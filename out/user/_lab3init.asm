
out/user/_lab3init:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <stat.h>
#include <user.h>

char *argv[] = {"lab3test", 0};

int main(void) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
  if (open("console", O_RDWR) < 0) {
   4:	be 02 00 00 00       	mov    $0x2,%esi
   9:	bf 25 0b 00 00       	mov    $0xb25,%edi
   e:	e8 00 08 00 00       	callq  813 <open>
  13:	85 c0                	test   %eax,%eax
  15:	79 23                	jns    3a <main+0x3a>
    mknod("console", 1, 1);
  17:	ba 01 00 00 00       	mov    $0x1,%edx
  1c:	be 01 00 00 00       	mov    $0x1,%esi
  21:	bf 25 0b 00 00       	mov    $0xb25,%edi
  26:	e8 f0 07 00 00       	callq  81b <mknod>
    open("console", O_RDWR);
  2b:	be 02 00 00 00       	mov    $0x2,%esi
  30:	bf 25 0b 00 00       	mov    $0xb25,%edi
  35:	e8 d9 07 00 00       	callq  813 <open>
  }
  dup(0); // stdout
  3a:	bf 00 00 00 00       	mov    $0x0,%edi
  3f:	e8 07 08 00 00       	callq  84b <dup>
  dup(0); // stderr
  44:	bf 00 00 00 00       	mov    $0x0,%edi
  49:	e8 fd 07 00 00       	callq  84b <dup>

  exec("lab3test", argv);
  4e:	be b0 0d 00 00       	mov    $0xdb0,%esi
  53:	bf 1c 0b 00 00       	mov    $0xb1c,%edi
  58:	e8 ae 07 00 00       	callq  80b <exec>
  printf(1, "lab3init: exec failed\n");
  5d:	be 2d 0b 00 00       	mov    $0xb2d,%esi
  62:	bf 01 00 00 00       	mov    $0x1,%edi
  67:	b8 00 00 00 00       	mov    $0x0,%eax
  6c:	e8 bf 01 00 00       	callq  230 <printf>
  exit();
  71:	e8 5d 07 00 00       	callq  7d3 <exit>

0000000000000076 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  76:	55                   	push   %rbp
  77:	48 89 e5             	mov    %rsp,%rbp
  7a:	48 83 ec 10          	sub    $0x10,%rsp
  7e:	89 7d fc             	mov    %edi,-0x4(%rbp)
  81:	89 f0                	mov    %esi,%eax
  83:	88 45 f8             	mov    %al,-0x8(%rbp)
  86:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  8a:	8b 45 fc             	mov    -0x4(%rbp),%eax
  8d:	ba 01 00 00 00       	mov    $0x1,%edx
  92:	48 89 ce             	mov    %rcx,%rsi
  95:	89 c7                	mov    %eax,%edi
  97:	e8 57 07 00 00       	callq  7f3 <write>
  9c:	90                   	nop
  9d:	c9                   	leaveq 
  9e:	c3                   	retq   

000000000000009f <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  9f:	55                   	push   %rbp
  a0:	48 89 e5             	mov    %rsp,%rbp
  a3:	48 83 ec 40          	sub    $0x40,%rsp
  a7:	89 7d cc             	mov    %edi,-0x34(%rbp)
  aa:	89 75 c8             	mov    %esi,-0x38(%rbp)
  ad:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  b0:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  b3:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  b7:	74 1f                	je     d8 <printint64+0x39>
  b9:	8b 45 c8             	mov    -0x38(%rbp),%eax
  bc:	c1 e8 1f             	shr    $0x1f,%eax
  bf:	0f b6 c0             	movzbl %al,%eax
  c2:	89 45 c0             	mov    %eax,-0x40(%rbp)
  c5:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  c9:	74 0d                	je     d8 <printint64+0x39>
    x = -xx;
  cb:	8b 45 c8             	mov    -0x38(%rbp),%eax
  ce:	f7 d8                	neg    %eax
  d0:	48 98                	cltq   
  d2:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  d6:	eb 09                	jmp    e1 <printint64+0x42>
  else
    x = xx;
  d8:	8b 45 c8             	mov    -0x38(%rbp),%eax
  db:	48 98                	cltq   
  dd:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
  e1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
  e8:	8b 4d fc             	mov    -0x4(%rbp),%ecx
  eb:	8d 41 01             	lea    0x1(%rcx),%eax
  ee:	89 45 fc             	mov    %eax,-0x4(%rbp)
  f1:	8b 45 c4             	mov    -0x3c(%rbp),%eax
  f4:	48 63 f0             	movslq %eax,%rsi
  f7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  fb:	ba 00 00 00 00       	mov    $0x0,%edx
 100:	48 f7 f6             	div    %rsi
 103:	48 89 d0             	mov    %rdx,%rax
 106:	0f b6 90 c0 0d 00 00 	movzbl 0xdc0(%rax),%edx
 10d:	48 63 c1             	movslq %ecx,%rax
 110:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 114:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 117:	48 63 f8             	movslq %eax,%rdi
 11a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 11e:	ba 00 00 00 00       	mov    $0x0,%edx
 123:	48 f7 f7             	div    %rdi
 126:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 12a:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 12f:	75 b7                	jne    e8 <printint64+0x49>

  if (sgn)
 131:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 135:	74 2b                	je     162 <printint64+0xc3>
    buf[i++] = '-';
 137:	8b 45 fc             	mov    -0x4(%rbp),%eax
 13a:	8d 50 01             	lea    0x1(%rax),%edx
 13d:	89 55 fc             	mov    %edx,-0x4(%rbp)
 140:	48 98                	cltq   
 142:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 147:	eb 19                	jmp    162 <printint64+0xc3>
    putc(fd, buf[i]);
 149:	8b 45 fc             	mov    -0x4(%rbp),%eax
 14c:	48 98                	cltq   
 14e:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 153:	0f be d0             	movsbl %al,%edx
 156:	8b 45 cc             	mov    -0x34(%rbp),%eax
 159:	89 d6                	mov    %edx,%esi
 15b:	89 c7                	mov    %eax,%edi
 15d:	e8 14 ff ff ff       	callq  76 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 162:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 166:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 16a:	79 dd                	jns    149 <printint64+0xaa>
    putc(fd, buf[i]);
}
 16c:	90                   	nop
 16d:	c9                   	leaveq 
 16e:	c3                   	retq   

000000000000016f <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 16f:	55                   	push   %rbp
 170:	48 89 e5             	mov    %rsp,%rbp
 173:	48 83 ec 30          	sub    $0x30,%rsp
 177:	89 7d dc             	mov    %edi,-0x24(%rbp)
 17a:	89 75 d8             	mov    %esi,-0x28(%rbp)
 17d:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 180:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 183:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 18a:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 18e:	74 17                	je     1a7 <printint+0x38>
 190:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 194:	79 11                	jns    1a7 <printint+0x38>
    neg = 1;
 196:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 19d:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1a0:	f7 d8                	neg    %eax
 1a2:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1a5:	eb 06                	jmp    1ad <printint+0x3e>
  } else {
    x = xx;
 1a7:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1aa:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 1ad:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1b4:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1b7:	8d 41 01             	lea    0x1(%rcx),%eax
 1ba:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1bd:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 1c0:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1c3:	ba 00 00 00 00       	mov    $0x0,%edx
 1c8:	f7 f6                	div    %esi
 1ca:	89 d0                	mov    %edx,%eax
 1cc:	89 c0                	mov    %eax,%eax
 1ce:	0f b6 90 e0 0d 00 00 	movzbl 0xde0(%rax),%edx
 1d5:	48 63 c1             	movslq %ecx,%rax
 1d8:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 1dc:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 1df:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1e2:	ba 00 00 00 00       	mov    $0x0,%edx
 1e7:	f7 f7                	div    %edi
 1e9:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1ec:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 1f0:	75 c2                	jne    1b4 <printint+0x45>
  if (neg)
 1f2:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 1f6:	74 2b                	je     223 <printint+0xb4>
    buf[i++] = '-';
 1f8:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1fb:	8d 50 01             	lea    0x1(%rax),%edx
 1fe:	89 55 fc             	mov    %edx,-0x4(%rbp)
 201:	48 98                	cltq   
 203:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 208:	eb 19                	jmp    223 <printint+0xb4>
    putc(fd, buf[i]);
 20a:	8b 45 fc             	mov    -0x4(%rbp),%eax
 20d:	48 98                	cltq   
 20f:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 214:	0f be d0             	movsbl %al,%edx
 217:	8b 45 dc             	mov    -0x24(%rbp),%eax
 21a:	89 d6                	mov    %edx,%esi
 21c:	89 c7                	mov    %eax,%edi
 21e:	e8 53 fe ff ff       	callq  76 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 223:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 227:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 22b:	79 dd                	jns    20a <printint+0x9b>
    putc(fd, buf[i]);
}
 22d:	90                   	nop
 22e:	c9                   	leaveq 
 22f:	c3                   	retq   

0000000000000230 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 230:	55                   	push   %rbp
 231:	48 89 e5             	mov    %rsp,%rbp
 234:	48 83 ec 70          	sub    $0x70,%rsp
 238:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 23b:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 23f:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 243:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 247:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 24b:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 24f:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 256:	48 8d 45 10          	lea    0x10(%rbp),%rax
 25a:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 25e:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 262:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 266:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 26d:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 274:	e9 68 02 00 00       	jmpq   4e1 <printf+0x2b1>
    c = fmt[i] & 0xff;
 279:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 27c:	48 63 d0             	movslq %eax,%rdx
 27f:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 283:	48 01 d0             	add    %rdx,%rax
 286:	0f b6 00             	movzbl (%rax),%eax
 289:	0f be c0             	movsbl %al,%eax
 28c:	25 ff 00 00 00       	and    $0xff,%eax
 291:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 294:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 298:	75 30                	jne    2ca <printf+0x9a>
      if (c == '%') {
 29a:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 29e:	75 13                	jne    2b3 <printf+0x83>
        state = '%';
 2a0:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 2a7:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 2ae:	e9 2a 02 00 00       	jmpq   4dd <printf+0x2ad>
      } else {
        putc(fd, c);
 2b3:	8b 45 b8             	mov    -0x48(%rbp),%eax
 2b6:	0f be d0             	movsbl %al,%edx
 2b9:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2bc:	89 d6                	mov    %edx,%esi
 2be:	89 c7                	mov    %eax,%edi
 2c0:	e8 b1 fd ff ff       	callq  76 <putc>
 2c5:	e9 13 02 00 00       	jmpq   4dd <printf+0x2ad>
      }
    } else if (state == '%') {
 2ca:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 2ce:	0f 85 09 02 00 00    	jne    4dd <printf+0x2ad>
      if (c == 'l') {
 2d4:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 2d8:	75 0c                	jne    2e6 <printf+0xb6>
        lflag = 1;
 2da:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 2e1:	e9 f7 01 00 00       	jmpq   4dd <printf+0x2ad>
      } else if (c == 'd') {
 2e6:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 2ea:	0f 85 95 00 00 00    	jne    385 <printf+0x155>
        if (lflag == 1)
 2f0:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 2f4:	75 49                	jne    33f <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 2f6:	8b 45 a0             	mov    -0x60(%rbp),%eax
 2f9:	83 f8 30             	cmp    $0x30,%eax
 2fc:	73 17                	jae    315 <printf+0xe5>
 2fe:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 302:	8b 55 a0             	mov    -0x60(%rbp),%edx
 305:	89 d2                	mov    %edx,%edx
 307:	48 01 d0             	add    %rdx,%rax
 30a:	8b 55 a0             	mov    -0x60(%rbp),%edx
 30d:	83 c2 08             	add    $0x8,%edx
 310:	89 55 a0             	mov    %edx,-0x60(%rbp)
 313:	eb 0c                	jmp    321 <printf+0xf1>
 315:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 319:	48 8d 50 08          	lea    0x8(%rax),%rdx
 31d:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 321:	48 8b 00             	mov    (%rax),%rax
 324:	89 c6                	mov    %eax,%esi
 326:	8b 45 9c             	mov    -0x64(%rbp),%eax
 329:	b9 01 00 00 00       	mov    $0x1,%ecx
 32e:	ba 0a 00 00 00       	mov    $0xa,%edx
 333:	89 c7                	mov    %eax,%edi
 335:	e8 65 fd ff ff       	callq  9f <printint64>
 33a:	e9 97 01 00 00       	jmpq   4d6 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 33f:	8b 45 a0             	mov    -0x60(%rbp),%eax
 342:	83 f8 30             	cmp    $0x30,%eax
 345:	73 17                	jae    35e <printf+0x12e>
 347:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 34b:	8b 55 a0             	mov    -0x60(%rbp),%edx
 34e:	89 d2                	mov    %edx,%edx
 350:	48 01 d0             	add    %rdx,%rax
 353:	8b 55 a0             	mov    -0x60(%rbp),%edx
 356:	83 c2 08             	add    $0x8,%edx
 359:	89 55 a0             	mov    %edx,-0x60(%rbp)
 35c:	eb 0c                	jmp    36a <printf+0x13a>
 35e:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 362:	48 8d 50 08          	lea    0x8(%rax),%rdx
 366:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 36a:	8b 30                	mov    (%rax),%esi
 36c:	8b 45 9c             	mov    -0x64(%rbp),%eax
 36f:	b9 01 00 00 00       	mov    $0x1,%ecx
 374:	ba 0a 00 00 00       	mov    $0xa,%edx
 379:	89 c7                	mov    %eax,%edi
 37b:	e8 ef fd ff ff       	callq  16f <printint>
 380:	e9 51 01 00 00       	jmpq   4d6 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 385:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 389:	74 0a                	je     395 <printf+0x165>
 38b:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 38f:	0f 85 95 00 00 00    	jne    42a <printf+0x1fa>
        if (lflag == 1)
 395:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 399:	75 49                	jne    3e4 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 39b:	8b 45 a0             	mov    -0x60(%rbp),%eax
 39e:	83 f8 30             	cmp    $0x30,%eax
 3a1:	73 17                	jae    3ba <printf+0x18a>
 3a3:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3a7:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3aa:	89 d2                	mov    %edx,%edx
 3ac:	48 01 d0             	add    %rdx,%rax
 3af:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3b2:	83 c2 08             	add    $0x8,%edx
 3b5:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3b8:	eb 0c                	jmp    3c6 <printf+0x196>
 3ba:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3be:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3c2:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3c6:	48 8b 00             	mov    (%rax),%rax
 3c9:	89 c6                	mov    %eax,%esi
 3cb:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3ce:	b9 00 00 00 00       	mov    $0x0,%ecx
 3d3:	ba 10 00 00 00       	mov    $0x10,%edx
 3d8:	89 c7                	mov    %eax,%edi
 3da:	e8 c0 fc ff ff       	callq  9f <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 3df:	e9 f2 00 00 00       	jmpq   4d6 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 3e4:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3e7:	83 f8 30             	cmp    $0x30,%eax
 3ea:	73 17                	jae    403 <printf+0x1d3>
 3ec:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3f0:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3f3:	89 d2                	mov    %edx,%edx
 3f5:	48 01 d0             	add    %rdx,%rax
 3f8:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3fb:	83 c2 08             	add    $0x8,%edx
 3fe:	89 55 a0             	mov    %edx,-0x60(%rbp)
 401:	eb 0c                	jmp    40f <printf+0x1df>
 403:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 407:	48 8d 50 08          	lea    0x8(%rax),%rdx
 40b:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 40f:	8b 30                	mov    (%rax),%esi
 411:	8b 45 9c             	mov    -0x64(%rbp),%eax
 414:	b9 00 00 00 00       	mov    $0x0,%ecx
 419:	ba 10 00 00 00       	mov    $0x10,%edx
 41e:	89 c7                	mov    %eax,%edi
 420:	e8 4a fd ff ff       	callq  16f <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 425:	e9 ac 00 00 00       	jmpq   4d6 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 42a:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 42e:	75 6b                	jne    49b <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 430:	8b 45 a0             	mov    -0x60(%rbp),%eax
 433:	83 f8 30             	cmp    $0x30,%eax
 436:	73 17                	jae    44f <printf+0x21f>
 438:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 43c:	8b 55 a0             	mov    -0x60(%rbp),%edx
 43f:	89 d2                	mov    %edx,%edx
 441:	48 01 d0             	add    %rdx,%rax
 444:	8b 55 a0             	mov    -0x60(%rbp),%edx
 447:	83 c2 08             	add    $0x8,%edx
 44a:	89 55 a0             	mov    %edx,-0x60(%rbp)
 44d:	eb 0c                	jmp    45b <printf+0x22b>
 44f:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 453:	48 8d 50 08          	lea    0x8(%rax),%rdx
 457:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 45b:	48 8b 00             	mov    (%rax),%rax
 45e:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 462:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 467:	75 25                	jne    48e <printf+0x25e>
          s = "(null)";
 469:	48 c7 45 c8 44 0b 00 	movq   $0xb44,-0x38(%rbp)
 470:	00 
        for (; *s; s++)
 471:	eb 1b                	jmp    48e <printf+0x25e>
          putc(fd, *s);
 473:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 477:	0f b6 00             	movzbl (%rax),%eax
 47a:	0f be d0             	movsbl %al,%edx
 47d:	8b 45 9c             	mov    -0x64(%rbp),%eax
 480:	89 d6                	mov    %edx,%esi
 482:	89 c7                	mov    %eax,%edi
 484:	e8 ed fb ff ff       	callq  76 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 489:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 48e:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 492:	0f b6 00             	movzbl (%rax),%eax
 495:	84 c0                	test   %al,%al
 497:	75 da                	jne    473 <printf+0x243>
 499:	eb 3b                	jmp    4d6 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 49b:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 49f:	75 14                	jne    4b5 <printf+0x285>
        putc(fd, c);
 4a1:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4a4:	0f be d0             	movsbl %al,%edx
 4a7:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4aa:	89 d6                	mov    %edx,%esi
 4ac:	89 c7                	mov    %eax,%edi
 4ae:	e8 c3 fb ff ff       	callq  76 <putc>
 4b3:	eb 21                	jmp    4d6 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 4b5:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4b8:	be 25 00 00 00       	mov    $0x25,%esi
 4bd:	89 c7                	mov    %eax,%edi
 4bf:	e8 b2 fb ff ff       	callq  76 <putc>
        putc(fd, c);
 4c4:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4c7:	0f be d0             	movsbl %al,%edx
 4ca:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4cd:	89 d6                	mov    %edx,%esi
 4cf:	89 c7                	mov    %eax,%edi
 4d1:	e8 a0 fb ff ff       	callq  76 <putc>
      }
      state = 0;
 4d6:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 4dd:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 4e1:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 4e4:	48 63 d0             	movslq %eax,%rdx
 4e7:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 4eb:	48 01 d0             	add    %rdx,%rax
 4ee:	0f b6 00             	movzbl (%rax),%eax
 4f1:	84 c0                	test   %al,%al
 4f3:	0f 85 80 fd ff ff    	jne    279 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 4f9:	90                   	nop
 4fa:	c9                   	leaveq 
 4fb:	c3                   	retq   

00000000000004fc <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 4fc:	55                   	push   %rbp
 4fd:	48 89 e5             	mov    %rsp,%rbp
 500:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 504:	89 75 f4             	mov    %esi,-0xc(%rbp)
 507:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 50a:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 50e:	8b 55 f0             	mov    -0x10(%rbp),%edx
 511:	8b 45 f4             	mov    -0xc(%rbp),%eax
 514:	48 89 ce             	mov    %rcx,%rsi
 517:	48 89 f7             	mov    %rsi,%rdi
 51a:	89 d1                	mov    %edx,%ecx
 51c:	fc                   	cld    
 51d:	f3 aa                	rep stos %al,%es:(%rdi)
 51f:	89 ca                	mov    %ecx,%edx
 521:	48 89 fe             	mov    %rdi,%rsi
 524:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 528:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 52b:	90                   	nop
 52c:	5d                   	pop    %rbp
 52d:	c3                   	retq   

000000000000052e <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 52e:	55                   	push   %rbp
 52f:	48 89 e5             	mov    %rsp,%rbp
 532:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 536:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 53a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 53e:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 542:	90                   	nop
 543:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 547:	48 8d 50 01          	lea    0x1(%rax),%rdx
 54b:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 54f:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 553:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 557:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 55b:	0f b6 12             	movzbl (%rdx),%edx
 55e:	88 10                	mov    %dl,(%rax)
 560:	0f b6 00             	movzbl (%rax),%eax
 563:	84 c0                	test   %al,%al
 565:	75 dc                	jne    543 <strcpy+0x15>
    ;
  return os;
 567:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 56b:	5d                   	pop    %rbp
 56c:	c3                   	retq   

000000000000056d <strcmp>:

int strcmp(const char *p, const char *q) {
 56d:	55                   	push   %rbp
 56e:	48 89 e5             	mov    %rsp,%rbp
 571:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 575:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 579:	eb 0a                	jmp    585 <strcmp+0x18>
    p++, q++;
 57b:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 580:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 585:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 589:	0f b6 00             	movzbl (%rax),%eax
 58c:	84 c0                	test   %al,%al
 58e:	74 12                	je     5a2 <strcmp+0x35>
 590:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 594:	0f b6 10             	movzbl (%rax),%edx
 597:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 59b:	0f b6 00             	movzbl (%rax),%eax
 59e:	38 c2                	cmp    %al,%dl
 5a0:	74 d9                	je     57b <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 5a2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5a6:	0f b6 00             	movzbl (%rax),%eax
 5a9:	0f b6 d0             	movzbl %al,%edx
 5ac:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5b0:	0f b6 00             	movzbl (%rax),%eax
 5b3:	0f b6 c0             	movzbl %al,%eax
 5b6:	29 c2                	sub    %eax,%edx
 5b8:	89 d0                	mov    %edx,%eax
}
 5ba:	5d                   	pop    %rbp
 5bb:	c3                   	retq   

00000000000005bc <strlen>:

uint strlen(char *s) {
 5bc:	55                   	push   %rbp
 5bd:	48 89 e5             	mov    %rsp,%rbp
 5c0:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 5c4:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 5cb:	eb 04                	jmp    5d1 <strlen+0x15>
 5cd:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 5d1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 5d4:	48 63 d0             	movslq %eax,%rdx
 5d7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5db:	48 01 d0             	add    %rdx,%rax
 5de:	0f b6 00             	movzbl (%rax),%eax
 5e1:	84 c0                	test   %al,%al
 5e3:	75 e8                	jne    5cd <strlen+0x11>
    ;
  return n;
 5e5:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 5e8:	5d                   	pop    %rbp
 5e9:	c3                   	retq   

00000000000005ea <memset>:

void *memset(void *dst, int c, uint n) {
 5ea:	55                   	push   %rbp
 5eb:	48 89 e5             	mov    %rsp,%rbp
 5ee:	48 83 ec 10          	sub    $0x10,%rsp
 5f2:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5f6:	89 75 f4             	mov    %esi,-0xc(%rbp)
 5f9:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 5fc:	8b 55 f0             	mov    -0x10(%rbp),%edx
 5ff:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 602:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 606:	89 ce                	mov    %ecx,%esi
 608:	48 89 c7             	mov    %rax,%rdi
 60b:	e8 ec fe ff ff       	callq  4fc <stosb>
  return dst;
 610:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 614:	c9                   	leaveq 
 615:	c3                   	retq   

0000000000000616 <strchr>:

char *strchr(const char *s, char c) {
 616:	55                   	push   %rbp
 617:	48 89 e5             	mov    %rsp,%rbp
 61a:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 61e:	89 f0                	mov    %esi,%eax
 620:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 623:	eb 17                	jmp    63c <strchr+0x26>
    if (*s == c)
 625:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 629:	0f b6 00             	movzbl (%rax),%eax
 62c:	3a 45 f4             	cmp    -0xc(%rbp),%al
 62f:	75 06                	jne    637 <strchr+0x21>
      return (char *)s;
 631:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 635:	eb 15                	jmp    64c <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 637:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 63c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 640:	0f b6 00             	movzbl (%rax),%eax
 643:	84 c0                	test   %al,%al
 645:	75 de                	jne    625 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 647:	b8 00 00 00 00       	mov    $0x0,%eax
}
 64c:	5d                   	pop    %rbp
 64d:	c3                   	retq   

000000000000064e <gets>:

char *gets(char *buf, int max) {
 64e:	55                   	push   %rbp
 64f:	48 89 e5             	mov    %rsp,%rbp
 652:	48 83 ec 20          	sub    $0x20,%rsp
 656:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 65a:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 65d:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 664:	eb 48                	jmp    6ae <gets+0x60>
    cc = read(0, &c, 1);
 666:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 66a:	ba 01 00 00 00       	mov    $0x1,%edx
 66f:	48 89 c6             	mov    %rax,%rsi
 672:	bf 00 00 00 00       	mov    $0x0,%edi
 677:	e8 6f 01 00 00       	callq  7eb <read>
 67c:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 67f:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 683:	7e 36                	jle    6bb <gets+0x6d>
      break;
    buf[i++] = c;
 685:	8b 45 fc             	mov    -0x4(%rbp),%eax
 688:	8d 50 01             	lea    0x1(%rax),%edx
 68b:	89 55 fc             	mov    %edx,-0x4(%rbp)
 68e:	48 63 d0             	movslq %eax,%rdx
 691:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 695:	48 01 c2             	add    %rax,%rdx
 698:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 69c:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 69e:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6a2:	3c 0a                	cmp    $0xa,%al
 6a4:	74 16                	je     6bc <gets+0x6e>
 6a6:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6aa:	3c 0d                	cmp    $0xd,%al
 6ac:	74 0e                	je     6bc <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6ae:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6b1:	83 c0 01             	add    $0x1,%eax
 6b4:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 6b7:	7c ad                	jl     666 <gets+0x18>
 6b9:	eb 01                	jmp    6bc <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 6bb:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 6bc:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6bf:	48 63 d0             	movslq %eax,%rdx
 6c2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6c6:	48 01 d0             	add    %rdx,%rax
 6c9:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 6cc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 6d0:	c9                   	leaveq 
 6d1:	c3                   	retq   

00000000000006d2 <stat>:

int stat(char *n, struct stat *st) {
 6d2:	55                   	push   %rbp
 6d3:	48 89 e5             	mov    %rsp,%rbp
 6d6:	48 83 ec 20          	sub    $0x20,%rsp
 6da:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 6de:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 6e2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6e6:	be 00 00 00 00       	mov    $0x0,%esi
 6eb:	48 89 c7             	mov    %rax,%rdi
 6ee:	e8 20 01 00 00       	callq  813 <open>
 6f3:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 6f6:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 6fa:	79 07                	jns    703 <stat+0x31>
    return -1;
 6fc:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 701:	eb 21                	jmp    724 <stat+0x52>
  r = fstat(fd, st);
 703:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 707:	8b 45 fc             	mov    -0x4(%rbp),%eax
 70a:	48 89 d6             	mov    %rdx,%rsi
 70d:	89 c7                	mov    %eax,%edi
 70f:	e8 17 01 00 00       	callq  82b <fstat>
 714:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 717:	8b 45 fc             	mov    -0x4(%rbp),%eax
 71a:	89 c7                	mov    %eax,%edi
 71c:	e8 da 00 00 00       	callq  7fb <close>
  return r;
 721:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 724:	c9                   	leaveq 
 725:	c3                   	retq   

0000000000000726 <atoi>:

int atoi(const char *s) {
 726:	55                   	push   %rbp
 727:	48 89 e5             	mov    %rsp,%rbp
 72a:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 72e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 735:	eb 28                	jmp    75f <atoi+0x39>
    n = n * 10 + *s++ - '0';
 737:	8b 55 fc             	mov    -0x4(%rbp),%edx
 73a:	89 d0                	mov    %edx,%eax
 73c:	c1 e0 02             	shl    $0x2,%eax
 73f:	01 d0                	add    %edx,%eax
 741:	01 c0                	add    %eax,%eax
 743:	89 c1                	mov    %eax,%ecx
 745:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 749:	48 8d 50 01          	lea    0x1(%rax),%rdx
 74d:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 751:	0f b6 00             	movzbl (%rax),%eax
 754:	0f be c0             	movsbl %al,%eax
 757:	01 c8                	add    %ecx,%eax
 759:	83 e8 30             	sub    $0x30,%eax
 75c:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 75f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 763:	0f b6 00             	movzbl (%rax),%eax
 766:	3c 2f                	cmp    $0x2f,%al
 768:	7e 0b                	jle    775 <atoi+0x4f>
 76a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 76e:	0f b6 00             	movzbl (%rax),%eax
 771:	3c 39                	cmp    $0x39,%al
 773:	7e c2                	jle    737 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 775:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 778:	5d                   	pop    %rbp
 779:	c3                   	retq   

000000000000077a <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 77a:	55                   	push   %rbp
 77b:	48 89 e5             	mov    %rsp,%rbp
 77e:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 782:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 786:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 789:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 78d:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 791:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 795:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 799:	eb 1d                	jmp    7b8 <memmove+0x3e>
    *dst++ = *src++;
 79b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 79f:	48 8d 50 01          	lea    0x1(%rax),%rdx
 7a3:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 7a7:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 7ab:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 7af:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 7b3:	0f b6 12             	movzbl (%rdx),%edx
 7b6:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 7b8:	8b 45 dc             	mov    -0x24(%rbp),%eax
 7bb:	8d 50 ff             	lea    -0x1(%rax),%edx
 7be:	89 55 dc             	mov    %edx,-0x24(%rbp)
 7c1:	85 c0                	test   %eax,%eax
 7c3:	7f d6                	jg     79b <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 7c5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7c9:	5d                   	pop    %rbp
 7ca:	c3                   	retq   

00000000000007cb <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 7cb:	b8 01 00 00 00       	mov    $0x1,%eax
 7d0:	cd 40                	int    $0x40
 7d2:	c3                   	retq   

00000000000007d3 <exit>:
SYSCALL(exit)
 7d3:	b8 02 00 00 00       	mov    $0x2,%eax
 7d8:	cd 40                	int    $0x40
 7da:	c3                   	retq   

00000000000007db <wait>:
SYSCALL(wait)
 7db:	b8 03 00 00 00       	mov    $0x3,%eax
 7e0:	cd 40                	int    $0x40
 7e2:	c3                   	retq   

00000000000007e3 <pipe>:
SYSCALL(pipe)
 7e3:	b8 04 00 00 00       	mov    $0x4,%eax
 7e8:	cd 40                	int    $0x40
 7ea:	c3                   	retq   

00000000000007eb <read>:
SYSCALL(read)
 7eb:	b8 05 00 00 00       	mov    $0x5,%eax
 7f0:	cd 40                	int    $0x40
 7f2:	c3                   	retq   

00000000000007f3 <write>:
SYSCALL(write)
 7f3:	b8 10 00 00 00       	mov    $0x10,%eax
 7f8:	cd 40                	int    $0x40
 7fa:	c3                   	retq   

00000000000007fb <close>:
SYSCALL(close)
 7fb:	b8 15 00 00 00       	mov    $0x15,%eax
 800:	cd 40                	int    $0x40
 802:	c3                   	retq   

0000000000000803 <kill>:
SYSCALL(kill)
 803:	b8 06 00 00 00       	mov    $0x6,%eax
 808:	cd 40                	int    $0x40
 80a:	c3                   	retq   

000000000000080b <exec>:
SYSCALL(exec)
 80b:	b8 07 00 00 00       	mov    $0x7,%eax
 810:	cd 40                	int    $0x40
 812:	c3                   	retq   

0000000000000813 <open>:
SYSCALL(open)
 813:	b8 0f 00 00 00       	mov    $0xf,%eax
 818:	cd 40                	int    $0x40
 81a:	c3                   	retq   

000000000000081b <mknod>:
SYSCALL(mknod)
 81b:	b8 11 00 00 00       	mov    $0x11,%eax
 820:	cd 40                	int    $0x40
 822:	c3                   	retq   

0000000000000823 <unlink>:
SYSCALL(unlink)
 823:	b8 12 00 00 00       	mov    $0x12,%eax
 828:	cd 40                	int    $0x40
 82a:	c3                   	retq   

000000000000082b <fstat>:
SYSCALL(fstat)
 82b:	b8 08 00 00 00       	mov    $0x8,%eax
 830:	cd 40                	int    $0x40
 832:	c3                   	retq   

0000000000000833 <link>:
SYSCALL(link)
 833:	b8 13 00 00 00       	mov    $0x13,%eax
 838:	cd 40                	int    $0x40
 83a:	c3                   	retq   

000000000000083b <mkdir>:
SYSCALL(mkdir)
 83b:	b8 14 00 00 00       	mov    $0x14,%eax
 840:	cd 40                	int    $0x40
 842:	c3                   	retq   

0000000000000843 <chdir>:
SYSCALL(chdir)
 843:	b8 09 00 00 00       	mov    $0x9,%eax
 848:	cd 40                	int    $0x40
 84a:	c3                   	retq   

000000000000084b <dup>:
SYSCALL(dup)
 84b:	b8 0a 00 00 00       	mov    $0xa,%eax
 850:	cd 40                	int    $0x40
 852:	c3                   	retq   

0000000000000853 <getpid>:
SYSCALL(getpid)
 853:	b8 0b 00 00 00       	mov    $0xb,%eax
 858:	cd 40                	int    $0x40
 85a:	c3                   	retq   

000000000000085b <sbrk>:
SYSCALL(sbrk)
 85b:	b8 0c 00 00 00       	mov    $0xc,%eax
 860:	cd 40                	int    $0x40
 862:	c3                   	retq   

0000000000000863 <sleep>:
SYSCALL(sleep)
 863:	b8 0d 00 00 00       	mov    $0xd,%eax
 868:	cd 40                	int    $0x40
 86a:	c3                   	retq   

000000000000086b <uptime>:
SYSCALL(uptime)
 86b:	b8 0e 00 00 00       	mov    $0xe,%eax
 870:	cd 40                	int    $0x40
 872:	c3                   	retq   

0000000000000873 <sysinfo>:
SYSCALL(sysinfo)
 873:	b8 16 00 00 00       	mov    $0x16,%eax
 878:	cd 40                	int    $0x40
 87a:	c3                   	retq   

000000000000087b <crashn>:
SYSCALL(crashn)
 87b:	b8 17 00 00 00       	mov    $0x17,%eax
 880:	cd 40                	int    $0x40
 882:	c3                   	retq   

0000000000000883 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 883:	55                   	push   %rbp
 884:	48 89 e5             	mov    %rsp,%rbp
 887:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 88b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 88f:	48 83 e8 10          	sub    $0x10,%rax
 893:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 897:	48 8b 05 72 05 00 00 	mov    0x572(%rip),%rax        # e10 <freep>
 89e:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8a2:	eb 2f                	jmp    8d3 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 8a4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8a8:	48 8b 00             	mov    (%rax),%rax
 8ab:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8af:	77 17                	ja     8c8 <free+0x45>
 8b1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8b5:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8b9:	77 2f                	ja     8ea <free+0x67>
 8bb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8bf:	48 8b 00             	mov    (%rax),%rax
 8c2:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8c6:	77 22                	ja     8ea <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8c8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8cc:	48 8b 00             	mov    (%rax),%rax
 8cf:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8d3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8d7:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8db:	76 c7                	jbe    8a4 <free+0x21>
 8dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8e1:	48 8b 00             	mov    (%rax),%rax
 8e4:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8e8:	76 ba                	jbe    8a4 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 8ea:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8ee:	8b 40 08             	mov    0x8(%rax),%eax
 8f1:	89 c0                	mov    %eax,%eax
 8f3:	48 c1 e0 04          	shl    $0x4,%rax
 8f7:	48 89 c2             	mov    %rax,%rdx
 8fa:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8fe:	48 01 c2             	add    %rax,%rdx
 901:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 905:	48 8b 00             	mov    (%rax),%rax
 908:	48 39 c2             	cmp    %rax,%rdx
 90b:	75 2d                	jne    93a <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 90d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 911:	8b 50 08             	mov    0x8(%rax),%edx
 914:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 918:	48 8b 00             	mov    (%rax),%rax
 91b:	8b 40 08             	mov    0x8(%rax),%eax
 91e:	01 c2                	add    %eax,%edx
 920:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 924:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 927:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 92b:	48 8b 00             	mov    (%rax),%rax
 92e:	48 8b 10             	mov    (%rax),%rdx
 931:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 935:	48 89 10             	mov    %rdx,(%rax)
 938:	eb 0e                	jmp    948 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 93a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 93e:	48 8b 10             	mov    (%rax),%rdx
 941:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 945:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 948:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 94c:	8b 40 08             	mov    0x8(%rax),%eax
 94f:	89 c0                	mov    %eax,%eax
 951:	48 c1 e0 04          	shl    $0x4,%rax
 955:	48 89 c2             	mov    %rax,%rdx
 958:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 95c:	48 01 d0             	add    %rdx,%rax
 95f:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 963:	75 27                	jne    98c <free+0x109>
    p->s.size += bp->s.size;
 965:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 969:	8b 50 08             	mov    0x8(%rax),%edx
 96c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 970:	8b 40 08             	mov    0x8(%rax),%eax
 973:	01 c2                	add    %eax,%edx
 975:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 979:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 97c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 980:	48 8b 10             	mov    (%rax),%rdx
 983:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 987:	48 89 10             	mov    %rdx,(%rax)
 98a:	eb 0b                	jmp    997 <free+0x114>
  } else
    p->s.ptr = bp;
 98c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 990:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 994:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 997:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 99b:	48 89 05 6e 04 00 00 	mov    %rax,0x46e(%rip)        # e10 <freep>
}
 9a2:	90                   	nop
 9a3:	5d                   	pop    %rbp
 9a4:	c3                   	retq   

00000000000009a5 <morecore>:

static Header *morecore(uint nu) {
 9a5:	55                   	push   %rbp
 9a6:	48 89 e5             	mov    %rsp,%rbp
 9a9:	48 83 ec 20          	sub    $0x20,%rsp
 9ad:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 9b0:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 9b7:	77 07                	ja     9c0 <morecore+0x1b>
    nu = 4096;
 9b9:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 9c0:	8b 45 ec             	mov    -0x14(%rbp),%eax
 9c3:	c1 e0 04             	shl    $0x4,%eax
 9c6:	89 c7                	mov    %eax,%edi
 9c8:	e8 8e fe ff ff       	callq  85b <sbrk>
 9cd:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 9d1:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 9d6:	75 07                	jne    9df <morecore+0x3a>
    return 0;
 9d8:	b8 00 00 00 00       	mov    $0x0,%eax
 9dd:	eb 29                	jmp    a08 <morecore+0x63>
  hp = (Header *)p;
 9df:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9e3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 9e7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9eb:	8b 55 ec             	mov    -0x14(%rbp),%edx
 9ee:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 9f1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9f5:	48 83 c0 10          	add    $0x10,%rax
 9f9:	48 89 c7             	mov    %rax,%rdi
 9fc:	e8 82 fe ff ff       	callq  883 <free>
  return freep;
 a01:	48 8b 05 08 04 00 00 	mov    0x408(%rip),%rax        # e10 <freep>
}
 a08:	c9                   	leaveq 
 a09:	c3                   	retq   

0000000000000a0a <malloc>:

void *malloc(uint nbytes) {
 a0a:	55                   	push   %rbp
 a0b:	48 89 e5             	mov    %rsp,%rbp
 a0e:	48 83 ec 30          	sub    $0x30,%rsp
 a12:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a15:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a18:	48 83 c0 0f          	add    $0xf,%rax
 a1c:	48 c1 e8 04          	shr    $0x4,%rax
 a20:	83 c0 01             	add    $0x1,%eax
 a23:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a26:	48 8b 05 e3 03 00 00 	mov    0x3e3(%rip),%rax        # e10 <freep>
 a2d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 a31:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 a36:	75 2b                	jne    a63 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 a38:	48 c7 45 f0 00 0e 00 	movq   $0xe00,-0x10(%rbp)
 a3f:	00 
 a40:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a44:	48 89 05 c5 03 00 00 	mov    %rax,0x3c5(%rip)        # e10 <freep>
 a4b:	48 8b 05 be 03 00 00 	mov    0x3be(%rip),%rax        # e10 <freep>
 a52:	48 89 05 a7 03 00 00 	mov    %rax,0x3a7(%rip)        # e00 <base>
    base.s.size = 0;
 a59:	c7 05 a5 03 00 00 00 	movl   $0x0,0x3a5(%rip)        # e08 <base+0x8>
 a60:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a63:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a67:	48 8b 00             	mov    (%rax),%rax
 a6a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a6e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a72:	8b 40 08             	mov    0x8(%rax),%eax
 a75:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a78:	72 5f                	jb     ad9 <malloc+0xcf>
      if (p->s.size == nunits)
 a7a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a7e:	8b 40 08             	mov    0x8(%rax),%eax
 a81:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a84:	75 10                	jne    a96 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 a86:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a8a:	48 8b 10             	mov    (%rax),%rdx
 a8d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a91:	48 89 10             	mov    %rdx,(%rax)
 a94:	eb 2e                	jmp    ac4 <malloc+0xba>
      else {
        p->s.size -= nunits;
 a96:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a9a:	8b 40 08             	mov    0x8(%rax),%eax
 a9d:	2b 45 ec             	sub    -0x14(%rbp),%eax
 aa0:	89 c2                	mov    %eax,%edx
 aa2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa6:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 aa9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aad:	8b 40 08             	mov    0x8(%rax),%eax
 ab0:	89 c0                	mov    %eax,%eax
 ab2:	48 c1 e0 04          	shl    $0x4,%rax
 ab6:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 aba:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 abe:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ac1:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 ac4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ac8:	48 89 05 41 03 00 00 	mov    %rax,0x341(%rip)        # e10 <freep>
      return (void *)(p + 1);
 acf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ad3:	48 83 c0 10          	add    $0x10,%rax
 ad7:	eb 41                	jmp    b1a <malloc+0x110>
    }
    if (p == freep)
 ad9:	48 8b 05 30 03 00 00 	mov    0x330(%rip),%rax        # e10 <freep>
 ae0:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 ae4:	75 1c                	jne    b02 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 ae6:	8b 45 ec             	mov    -0x14(%rbp),%eax
 ae9:	89 c7                	mov    %eax,%edi
 aeb:	e8 b5 fe ff ff       	callq  9a5 <morecore>
 af0:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 af4:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 af9:	75 07                	jne    b02 <malloc+0xf8>
        return 0;
 afb:	b8 00 00 00 00       	mov    $0x0,%eax
 b00:	eb 18                	jmp    b1a <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b02:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b06:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b0a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b0e:	48 8b 00             	mov    (%rax),%rax
 b11:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b15:	e9 54 ff ff ff       	jmpq   a6e <malloc+0x64>
 b1a:	c9                   	leaveq 
 b1b:	c3                   	retq   
