
out/user/_lab1test:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
void testinvalidargs(void);
void smallfilereadtest(void);
void duptest(void);
void nofilestest(void);

int main() {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
  if(open("console", O_RDWR) < 0){
       4:	be 02 00 00 00       	mov    $0x2,%esi
       9:	bf 98 1f 00 00       	mov    $0x1f98,%edi
       e:	e8 76 1c 00 00       	callq  1c89 <open>
      13:	85 c0                	test   %eax,%eax
      15:	79 07                	jns    1e <main+0x1e>
    return -1;
      17:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
      1c:	eb 58                	jmp    76 <main+0x76>
  }
  dup(0);     // stdout
      1e:	bf 00 00 00 00       	mov    $0x0,%edi
      23:	e8 99 1c 00 00       	callq  1cc1 <dup>
  dup(0);     // stderr
      28:	bf 00 00 00 00       	mov    $0x0,%edi
      2d:	e8 8f 1c 00 00       	callq  1cc1 <dup>

  printf(1, "hello world\n");
      32:	be a0 1f 00 00       	mov    $0x1fa0,%esi
      37:	bf 01 00 00 00       	mov    $0x1,%edi
      3c:	b8 00 00 00 00       	mov    $0x0,%eax
      41:	e8 60 16 00 00       	callq  16a6 <printf>

  testinvalidargs();
      46:	e8 2d 00 00 00       	callq  78 <testinvalidargs>

  smallfilereadtest();
      4b:	e8 d3 07 00 00       	callq  823 <smallfilereadtest>
  duptest();
      50:	e8 a5 0c 00 00       	callq  cfa <duptest>
  nofilestest();
      55:	e8 3f 12 00 00       	callq  1299 <nofilestest>

  printf(stdout, "lab1 tests passed!\n");
      5a:	8b 05 70 2c 00 00    	mov    0x2c70(%rip),%eax        # 2cd0 <stdout>
      60:	be ad 1f 00 00       	mov    $0x1fad,%esi
      65:	89 c7                	mov    %eax,%edi
      67:	b8 00 00 00 00       	mov    $0x0,%eax
      6c:	e8 35 16 00 00       	callq  16a6 <printf>

  exit();
      71:	e8 d3 1b 00 00       	callq  1c49 <exit>
  return 0;
}
      76:	5d                   	pop    %rbp
      77:	c3                   	retq   

0000000000000078 <testinvalidargs>:

void testinvalidargs(void) {
      78:	55                   	push   %rbp
      79:	48 89 e5             	mov    %rsp,%rbp
      7c:	48 83 ec 30          	sub    $0x30,%rsp
  int fd, i;
  char buf[11];
  struct stat st;

  // open
  if (open("/other.txt", O_CREATE) != -1)
      80:	be 00 02 00 00       	mov    $0x200,%esi
      85:	bf c1 1f 00 00       	mov    $0x1fc1,%edi
      8a:	e8 fa 1b 00 00       	callq  1c89 <open>
      8f:	83 f8 ff             	cmp    $0xffffffff,%eax
      92:	74 4c                	je     e0 <testinvalidargs+0x68>
    error("created file in read only file system");
      94:	8b 05 36 2c 00 00    	mov    0x2c36(%rip),%eax        # 2cd0 <stdout>
      9a:	ba 3f 00 00 00       	mov    $0x3f,%edx
      9f:	be cc 1f 00 00       	mov    $0x1fcc,%esi
      a4:	89 c7                	mov    %eax,%edi
      a6:	b8 00 00 00 00       	mov    $0x0,%eax
      ab:	e8 f6 15 00 00       	callq  16a6 <printf>
      b0:	8b 05 1a 2c 00 00    	mov    0x2c1a(%rip),%eax        # 2cd0 <stdout>
      b6:	be e0 1f 00 00       	mov    $0x1fe0,%esi
      bb:	89 c7                	mov    %eax,%edi
      bd:	b8 00 00 00 00       	mov    $0x0,%eax
      c2:	e8 df 15 00 00       	callq  16a6 <printf>
      c7:	8b 05 03 2c 00 00    	mov    0x2c03(%rip),%eax        # 2cd0 <stdout>
      cd:	be 06 20 00 00       	mov    $0x2006,%esi
      d2:	89 c7                	mov    %eax,%edi
      d4:	b8 00 00 00 00       	mov    $0x0,%eax
      d9:	e8 c8 15 00 00       	callq  16a6 <printf>
      de:	eb fe                	jmp    de <testinvalidargs+0x66>

  if (open("/small.txt", O_RDWR) != -1 || open("/small.txt", O_WRONLY) != -1)
      e0:	be 02 00 00 00       	mov    $0x2,%esi
      e5:	bf 08 20 00 00       	mov    $0x2008,%edi
      ea:	e8 9a 1b 00 00       	callq  1c89 <open>
      ef:	83 f8 ff             	cmp    $0xffffffff,%eax
      f2:	75 14                	jne    108 <testinvalidargs+0x90>
      f4:	be 01 00 00 00       	mov    $0x1,%esi
      f9:	bf 08 20 00 00       	mov    $0x2008,%edi
      fe:	e8 86 1b 00 00       	callq  1c89 <open>
     103:	83 f8 ff             	cmp    $0xffffffff,%eax
     106:	74 4c                	je     154 <testinvalidargs+0xdc>
    error("tried to open a file for writing in read only fs");
     108:	8b 05 c2 2b 00 00    	mov    0x2bc2(%rip),%eax        # 2cd0 <stdout>
     10e:	ba 42 00 00 00       	mov    $0x42,%edx
     113:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     118:	89 c7                	mov    %eax,%edi
     11a:	b8 00 00 00 00       	mov    $0x0,%eax
     11f:	e8 82 15 00 00       	callq  16a6 <printf>
     124:	8b 05 a6 2b 00 00    	mov    0x2ba6(%rip),%eax        # 2cd0 <stdout>
     12a:	be 18 20 00 00       	mov    $0x2018,%esi
     12f:	89 c7                	mov    %eax,%edi
     131:	b8 00 00 00 00       	mov    $0x0,%eax
     136:	e8 6b 15 00 00       	callq  16a6 <printf>
     13b:	8b 05 8f 2b 00 00    	mov    0x2b8f(%rip),%eax        # 2cd0 <stdout>
     141:	be 06 20 00 00       	mov    $0x2006,%esi
     146:	89 c7                	mov    %eax,%edi
     148:	b8 00 00 00 00       	mov    $0x0,%eax
     14d:	e8 54 15 00 00       	callq  16a6 <printf>
     152:	eb fe                	jmp    152 <testinvalidargs+0xda>

  if (open("/other.txt", O_RDONLY) != -1)
     154:	be 00 00 00 00       	mov    $0x0,%esi
     159:	bf c1 1f 00 00       	mov    $0x1fc1,%edi
     15e:	e8 26 1b 00 00       	callq  1c89 <open>
     163:	83 f8 ff             	cmp    $0xffffffff,%eax
     166:	74 4c                	je     1b4 <testinvalidargs+0x13c>
    error("opened a file that doesn't exist");
     168:	8b 05 62 2b 00 00    	mov    0x2b62(%rip),%eax        # 2cd0 <stdout>
     16e:	ba 45 00 00 00       	mov    $0x45,%edx
     173:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     178:	89 c7                	mov    %eax,%edi
     17a:	b8 00 00 00 00       	mov    $0x0,%eax
     17f:	e8 22 15 00 00       	callq  16a6 <printf>
     184:	8b 05 46 2b 00 00    	mov    0x2b46(%rip),%eax        # 2cd0 <stdout>
     18a:	be 50 20 00 00       	mov    $0x2050,%esi
     18f:	89 c7                	mov    %eax,%edi
     191:	b8 00 00 00 00       	mov    $0x0,%eax
     196:	e8 0b 15 00 00       	callq  16a6 <printf>
     19b:	8b 05 2f 2b 00 00    	mov    0x2b2f(%rip),%eax        # 2cd0 <stdout>
     1a1:	be 06 20 00 00       	mov    $0x2006,%esi
     1a6:	89 c7                	mov    %eax,%edi
     1a8:	b8 00 00 00 00       	mov    $0x0,%eax
     1ad:	e8 f4 14 00 00       	callq  16a6 <printf>
     1b2:	eb fe                	jmp    1b2 <testinvalidargs+0x13a>

  printf(stdout, "passed argument checking for open\n");
     1b4:	8b 05 16 2b 00 00    	mov    0x2b16(%rip),%eax        # 2cd0 <stdout>
     1ba:	be 78 20 00 00       	mov    $0x2078,%esi
     1bf:	89 c7                	mov    %eax,%edi
     1c1:	b8 00 00 00 00       	mov    $0x0,%eax
     1c6:	e8 db 14 00 00       	callq  16a6 <printf>

  // read
  if (read(15, buf, 11) != -1)
     1cb:	48 8d 45 ed          	lea    -0x13(%rbp),%rax
     1cf:	ba 0b 00 00 00       	mov    $0xb,%edx
     1d4:	48 89 c6             	mov    %rax,%rsi
     1d7:	bf 0f 00 00 00       	mov    $0xf,%edi
     1dc:	e8 80 1a 00 00       	callq  1c61 <read>
     1e1:	83 f8 ff             	cmp    $0xffffffff,%eax
     1e4:	74 4c                	je     232 <testinvalidargs+0x1ba>
    error("read on a non existent file descriptor");
     1e6:	8b 05 e4 2a 00 00    	mov    0x2ae4(%rip),%eax        # 2cd0 <stdout>
     1ec:	ba 4b 00 00 00       	mov    $0x4b,%edx
     1f1:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     1f6:	89 c7                	mov    %eax,%edi
     1f8:	b8 00 00 00 00       	mov    $0x0,%eax
     1fd:	e8 a4 14 00 00       	callq  16a6 <printf>
     202:	8b 05 c8 2a 00 00    	mov    0x2ac8(%rip),%eax        # 2cd0 <stdout>
     208:	be a0 20 00 00       	mov    $0x20a0,%esi
     20d:	89 c7                	mov    %eax,%edi
     20f:	b8 00 00 00 00       	mov    $0x0,%eax
     214:	e8 8d 14 00 00       	callq  16a6 <printf>
     219:	8b 05 b1 2a 00 00    	mov    0x2ab1(%rip),%eax        # 2cd0 <stdout>
     21f:	be 06 20 00 00       	mov    $0x2006,%esi
     224:	89 c7                	mov    %eax,%edi
     226:	b8 00 00 00 00       	mov    $0x0,%eax
     22b:	e8 76 14 00 00       	callq  16a6 <printf>
     230:	eb fe                	jmp    230 <testinvalidargs+0x1b8>

  fd = open("console", O_WRONLY);
     232:	be 01 00 00 00       	mov    $0x1,%esi
     237:	bf 98 1f 00 00       	mov    $0x1f98,%edi
     23c:	e8 48 1a 00 00       	callq  1c89 <open>
     241:	89 45 fc             	mov    %eax,-0x4(%rbp)
  assert(fd != -1);
     244:	83 7d fc ff          	cmpl   $0xffffffff,-0x4(%rbp)
     248:	75 23                	jne    26d <testinvalidargs+0x1f5>
     24a:	8b 05 80 2a 00 00    	mov    0x2a80(%rip),%eax        # 2cd0 <stdout>
     250:	b9 c7 20 00 00       	mov    $0x20c7,%ecx
     255:	ba 4e 00 00 00       	mov    $0x4e,%edx
     25a:	be d0 20 00 00       	mov    $0x20d0,%esi
     25f:	89 c7                	mov    %eax,%edi
     261:	b8 00 00 00 00       	mov    $0x0,%eax
     266:	e8 3b 14 00 00       	callq  16a6 <printf>
     26b:	eb fe                	jmp    26b <testinvalidargs+0x1f3>

  if (read(fd, buf, 10) != -1)
     26d:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     271:	8b 45 fc             	mov    -0x4(%rbp),%eax
     274:	ba 0a 00 00 00       	mov    $0xa,%edx
     279:	48 89 ce             	mov    %rcx,%rsi
     27c:	89 c7                	mov    %eax,%edi
     27e:	e8 de 19 00 00       	callq  1c61 <read>
     283:	83 f8 ff             	cmp    $0xffffffff,%eax
     286:	74 4c                	je     2d4 <testinvalidargs+0x25c>
    // NOTE: If the test hangs here it's because you're trying to read from
    // stdin (hint: shouldn't happen).
    error("able to read from a write only console");
     288:	8b 05 42 2a 00 00    	mov    0x2a42(%rip),%eax        # 2cd0 <stdout>
     28e:	ba 53 00 00 00       	mov    $0x53,%edx
     293:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     298:	89 c7                	mov    %eax,%edi
     29a:	b8 00 00 00 00       	mov    $0x0,%eax
     29f:	e8 02 14 00 00       	callq  16a6 <printf>
     2a4:	8b 05 26 2a 00 00    	mov    0x2a26(%rip),%eax        # 2cd0 <stdout>
     2aa:	be f0 20 00 00       	mov    $0x20f0,%esi
     2af:	89 c7                	mov    %eax,%edi
     2b1:	b8 00 00 00 00       	mov    $0x0,%eax
     2b6:	e8 eb 13 00 00       	callq  16a6 <printf>
     2bb:	8b 05 0f 2a 00 00    	mov    0x2a0f(%rip),%eax        # 2cd0 <stdout>
     2c1:	be 06 20 00 00       	mov    $0x2006,%esi
     2c6:	89 c7                	mov    %eax,%edi
     2c8:	b8 00 00 00 00       	mov    $0x0,%eax
     2cd:	e8 d4 13 00 00       	callq  16a6 <printf>
     2d2:	eb fe                	jmp    2d2 <testinvalidargs+0x25a>
  assert(close(fd) == 0);
     2d4:	8b 45 fc             	mov    -0x4(%rbp),%eax
     2d7:	89 c7                	mov    %eax,%edi
     2d9:	e8 93 19 00 00       	callq  1c71 <close>
     2de:	85 c0                	test   %eax,%eax
     2e0:	74 23                	je     305 <testinvalidargs+0x28d>
     2e2:	8b 05 e8 29 00 00    	mov    0x29e8(%rip),%eax        # 2cd0 <stdout>
     2e8:	b9 17 21 00 00       	mov    $0x2117,%ecx
     2ed:	ba 54 00 00 00       	mov    $0x54,%edx
     2f2:	be d0 20 00 00       	mov    $0x20d0,%esi
     2f7:	89 c7                	mov    %eax,%edi
     2f9:	b8 00 00 00 00       	mov    $0x0,%eax
     2fe:	e8 a3 13 00 00       	callq  16a6 <printf>
     303:	eb fe                	jmp    303 <testinvalidargs+0x28b>

  fd = open("/small.txt", O_RDONLY);
     305:	be 00 00 00 00       	mov    $0x0,%esi
     30a:	bf 08 20 00 00       	mov    $0x2008,%edi
     30f:	e8 75 19 00 00       	callq  1c89 <open>
     314:	89 45 fc             	mov    %eax,-0x4(%rbp)

  if ((i = read(fd, buf, -100)) != -1)
     317:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     31b:	8b 45 fc             	mov    -0x4(%rbp),%eax
     31e:	ba 9c ff ff ff       	mov    $0xffffff9c,%edx
     323:	48 89 ce             	mov    %rcx,%rsi
     326:	89 c7                	mov    %eax,%edi
     328:	e8 34 19 00 00       	callq  1c61 <read>
     32d:	89 45 f8             	mov    %eax,-0x8(%rbp)
     330:	83 7d f8 ff          	cmpl   $0xffffffff,-0x8(%rbp)
     334:	74 4f                	je     385 <testinvalidargs+0x30d>
    error("negative n didn't return error, return value was '%d'", i);
     336:	8b 05 94 29 00 00    	mov    0x2994(%rip),%eax        # 2cd0 <stdout>
     33c:	ba 59 00 00 00       	mov    $0x59,%edx
     341:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     346:	89 c7                	mov    %eax,%edi
     348:	b8 00 00 00 00       	mov    $0x0,%eax
     34d:	e8 54 13 00 00       	callq  16a6 <printf>
     352:	8b 05 78 29 00 00    	mov    0x2978(%rip),%eax        # 2cd0 <stdout>
     358:	8b 55 f8             	mov    -0x8(%rbp),%edx
     35b:	be 28 21 00 00       	mov    $0x2128,%esi
     360:	89 c7                	mov    %eax,%edi
     362:	b8 00 00 00 00       	mov    $0x0,%eax
     367:	e8 3a 13 00 00       	callq  16a6 <printf>
     36c:	8b 05 5e 29 00 00    	mov    0x295e(%rip),%eax        # 2cd0 <stdout>
     372:	be 06 20 00 00       	mov    $0x2006,%esi
     377:	89 c7                	mov    %eax,%edi
     379:	b8 00 00 00 00       	mov    $0x0,%eax
     37e:	e8 23 13 00 00       	callq  16a6 <printf>
     383:	eb fe                	jmp    383 <testinvalidargs+0x30b>

  if (read(fd, (char *)0xffffff00, 10) != -1)
     385:	8b 45 fc             	mov    -0x4(%rbp),%eax
     388:	ba 0a 00 00 00       	mov    $0xa,%edx
     38d:	be 00 ff ff ff       	mov    $0xffffff00,%esi
     392:	89 c7                	mov    %eax,%edi
     394:	e8 c8 18 00 00       	callq  1c61 <read>
     399:	83 f8 ff             	cmp    $0xffffffff,%eax
     39c:	74 4c                	je     3ea <testinvalidargs+0x372>
    error("able to read to a buffer not in my memory region");
     39e:	8b 05 2c 29 00 00    	mov    0x292c(%rip),%eax        # 2cd0 <stdout>
     3a4:	ba 5c 00 00 00       	mov    $0x5c,%edx
     3a9:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     3ae:	89 c7                	mov    %eax,%edi
     3b0:	b8 00 00 00 00       	mov    $0x0,%eax
     3b5:	e8 ec 12 00 00       	callq  16a6 <printf>
     3ba:	8b 05 10 29 00 00    	mov    0x2910(%rip),%eax        # 2cd0 <stdout>
     3c0:	be 60 21 00 00       	mov    $0x2160,%esi
     3c5:	89 c7                	mov    %eax,%edi
     3c7:	b8 00 00 00 00       	mov    $0x0,%eax
     3cc:	e8 d5 12 00 00       	callq  16a6 <printf>
     3d1:	8b 05 f9 28 00 00    	mov    0x28f9(%rip),%eax        # 2cd0 <stdout>
     3d7:	be 06 20 00 00       	mov    $0x2006,%esi
     3dc:	89 c7                	mov    %eax,%edi
     3de:	b8 00 00 00 00       	mov    $0x0,%eax
     3e3:	e8 be 12 00 00       	callq  16a6 <printf>
     3e8:	eb fe                	jmp    3e8 <testinvalidargs+0x370>

  printf(stdout, "passed argument checking for read\n");
     3ea:	8b 05 e0 28 00 00    	mov    0x28e0(%rip),%eax        # 2cd0 <stdout>
     3f0:	be 98 21 00 00       	mov    $0x2198,%esi
     3f5:	89 c7                	mov    %eax,%edi
     3f7:	b8 00 00 00 00       	mov    $0x0,%eax
     3fc:	e8 a5 12 00 00       	callq  16a6 <printf>

  // write
  if (write(15, buf, 11) != -1)
     401:	48 8d 45 ed          	lea    -0x13(%rbp),%rax
     405:	ba 0b 00 00 00       	mov    $0xb,%edx
     40a:	48 89 c6             	mov    %rax,%rsi
     40d:	bf 0f 00 00 00       	mov    $0xf,%edi
     412:	e8 52 18 00 00       	callq  1c69 <write>
     417:	83 f8 ff             	cmp    $0xffffffff,%eax
     41a:	74 4c                	je     468 <testinvalidargs+0x3f0>
    error("write on a non existent file descriptor");
     41c:	8b 05 ae 28 00 00    	mov    0x28ae(%rip),%eax        # 2cd0 <stdout>
     422:	ba 62 00 00 00       	mov    $0x62,%edx
     427:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     42c:	89 c7                	mov    %eax,%edi
     42e:	b8 00 00 00 00       	mov    $0x0,%eax
     433:	e8 6e 12 00 00       	callq  16a6 <printf>
     438:	8b 05 92 28 00 00    	mov    0x2892(%rip),%eax        # 2cd0 <stdout>
     43e:	be c0 21 00 00       	mov    $0x21c0,%esi
     443:	89 c7                	mov    %eax,%edi
     445:	b8 00 00 00 00       	mov    $0x0,%eax
     44a:	e8 57 12 00 00       	callq  16a6 <printf>
     44f:	8b 05 7b 28 00 00    	mov    0x287b(%rip),%eax        # 2cd0 <stdout>
     455:	be 06 20 00 00       	mov    $0x2006,%esi
     45a:	89 c7                	mov    %eax,%edi
     45c:	b8 00 00 00 00       	mov    $0x0,%eax
     461:	e8 40 12 00 00       	callq  16a6 <printf>
     466:	eb fe                	jmp    466 <testinvalidargs+0x3ee>

  if (write(fd, buf, 11) != -1)
     468:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     46c:	8b 45 fc             	mov    -0x4(%rbp),%eax
     46f:	ba 0b 00 00 00       	mov    $0xb,%edx
     474:	48 89 ce             	mov    %rcx,%rsi
     477:	89 c7                	mov    %eax,%edi
     479:	e8 eb 17 00 00       	callq  1c69 <write>
     47e:	83 f8 ff             	cmp    $0xffffffff,%eax
     481:	74 4c                	je     4cf <testinvalidargs+0x457>
    error("able to write to a file in read only fs");
     483:	8b 05 47 28 00 00    	mov    0x2847(%rip),%eax        # 2cd0 <stdout>
     489:	ba 65 00 00 00       	mov    $0x65,%edx
     48e:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     493:	89 c7                	mov    %eax,%edi
     495:	b8 00 00 00 00       	mov    $0x0,%eax
     49a:	e8 07 12 00 00       	callq  16a6 <printf>
     49f:	8b 05 2b 28 00 00    	mov    0x282b(%rip),%eax        # 2cd0 <stdout>
     4a5:	be e8 21 00 00       	mov    $0x21e8,%esi
     4aa:	89 c7                	mov    %eax,%edi
     4ac:	b8 00 00 00 00       	mov    $0x0,%eax
     4b1:	e8 f0 11 00 00       	callq  16a6 <printf>
     4b6:	8b 05 14 28 00 00    	mov    0x2814(%rip),%eax        # 2cd0 <stdout>
     4bc:	be 06 20 00 00       	mov    $0x2006,%esi
     4c1:	89 c7                	mov    %eax,%edi
     4c3:	b8 00 00 00 00       	mov    $0x0,%eax
     4c8:	e8 d9 11 00 00       	callq  16a6 <printf>
     4cd:	eb fe                	jmp    4cd <testinvalidargs+0x455>

  printf(stdout, "passed argument checking for write\n");
     4cf:	8b 05 fb 27 00 00    	mov    0x27fb(%rip),%eax        # 2cd0 <stdout>
     4d5:	be 10 22 00 00       	mov    $0x2210,%esi
     4da:	89 c7                	mov    %eax,%edi
     4dc:	b8 00 00 00 00       	mov    $0x0,%eax
     4e1:	e8 c0 11 00 00       	callq  16a6 <printf>

  // stat
  if (fstat(15, &st) != -1)
     4e6:	48 8d 45 dc          	lea    -0x24(%rbp),%rax
     4ea:	48 89 c6             	mov    %rax,%rsi
     4ed:	bf 0f 00 00 00       	mov    $0xf,%edi
     4f2:	e8 aa 17 00 00       	callq  1ca1 <fstat>
     4f7:	83 f8 ff             	cmp    $0xffffffff,%eax
     4fa:	74 4c                	je     548 <testinvalidargs+0x4d0>
    error("tried to fstat on a non existent file descriptor");
     4fc:	8b 05 ce 27 00 00    	mov    0x27ce(%rip),%eax        # 2cd0 <stdout>
     502:	ba 6b 00 00 00       	mov    $0x6b,%edx
     507:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     50c:	89 c7                	mov    %eax,%edi
     50e:	b8 00 00 00 00       	mov    $0x0,%eax
     513:	e8 8e 11 00 00       	callq  16a6 <printf>
     518:	8b 05 b2 27 00 00    	mov    0x27b2(%rip),%eax        # 2cd0 <stdout>
     51e:	be 38 22 00 00       	mov    $0x2238,%esi
     523:	89 c7                	mov    %eax,%edi
     525:	b8 00 00 00 00       	mov    $0x0,%eax
     52a:	e8 77 11 00 00       	callq  16a6 <printf>
     52f:	8b 05 9b 27 00 00    	mov    0x279b(%rip),%eax        # 2cd0 <stdout>
     535:	be 06 20 00 00       	mov    $0x2006,%esi
     53a:	89 c7                	mov    %eax,%edi
     53c:	b8 00 00 00 00       	mov    $0x0,%eax
     541:	e8 60 11 00 00       	callq  16a6 <printf>
     546:	eb fe                	jmp    546 <testinvalidargs+0x4ce>

  if (fstat(stdout, &st) != 0) error("couldn't fstat on stdout");
     548:	8b 05 82 27 00 00    	mov    0x2782(%rip),%eax        # 2cd0 <stdout>
     54e:	48 8d 55 dc          	lea    -0x24(%rbp),%rdx
     552:	48 89 d6             	mov    %rdx,%rsi
     555:	89 c7                	mov    %eax,%edi
     557:	e8 45 17 00 00       	callq  1ca1 <fstat>
     55c:	85 c0                	test   %eax,%eax
     55e:	74 4c                	je     5ac <testinvalidargs+0x534>
     560:	8b 05 6a 27 00 00    	mov    0x276a(%rip),%eax        # 2cd0 <stdout>
     566:	ba 6d 00 00 00       	mov    $0x6d,%edx
     56b:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     570:	89 c7                	mov    %eax,%edi
     572:	b8 00 00 00 00       	mov    $0x0,%eax
     577:	e8 2a 11 00 00       	callq  16a6 <printf>
     57c:	8b 05 4e 27 00 00    	mov    0x274e(%rip),%eax        # 2cd0 <stdout>
     582:	be 69 22 00 00       	mov    $0x2269,%esi
     587:	89 c7                	mov    %eax,%edi
     589:	b8 00 00 00 00       	mov    $0x0,%eax
     58e:	e8 13 11 00 00       	callq  16a6 <printf>
     593:	8b 05 37 27 00 00    	mov    0x2737(%rip),%eax        # 2cd0 <stdout>
     599:	be 06 20 00 00       	mov    $0x2006,%esi
     59e:	89 c7                	mov    %eax,%edi
     5a0:	b8 00 00 00 00       	mov    $0x0,%eax
     5a5:	e8 fc 10 00 00       	callq  16a6 <printf>
     5aa:	eb fe                	jmp    5aa <testinvalidargs+0x532>

  assert(st.type == T_DEV);
     5ac:	0f b7 45 dc          	movzwl -0x24(%rbp),%eax
     5b0:	66 83 f8 03          	cmp    $0x3,%ax
     5b4:	74 23                	je     5d9 <testinvalidargs+0x561>
     5b6:	8b 05 14 27 00 00    	mov    0x2714(%rip),%eax        # 2cd0 <stdout>
     5bc:	b9 82 22 00 00       	mov    $0x2282,%ecx
     5c1:	ba 6f 00 00 00       	mov    $0x6f,%edx
     5c6:	be d0 20 00 00       	mov    $0x20d0,%esi
     5cb:	89 c7                	mov    %eax,%edi
     5cd:	b8 00 00 00 00       	mov    $0x0,%eax
     5d2:	e8 cf 10 00 00       	callq  16a6 <printf>
     5d7:	eb fe                	jmp    5d7 <testinvalidargs+0x55f>
  assert(st.size == 0);
     5d9:	8b 45 e8             	mov    -0x18(%rbp),%eax
     5dc:	85 c0                	test   %eax,%eax
     5de:	74 23                	je     603 <testinvalidargs+0x58b>
     5e0:	8b 05 ea 26 00 00    	mov    0x26ea(%rip),%eax        # 2cd0 <stdout>
     5e6:	b9 93 22 00 00       	mov    $0x2293,%ecx
     5eb:	ba 70 00 00 00       	mov    $0x70,%edx
     5f0:	be d0 20 00 00       	mov    $0x20d0,%esi
     5f5:	89 c7                	mov    %eax,%edi
     5f7:	b8 00 00 00 00       	mov    $0x0,%eax
     5fc:	e8 a5 10 00 00       	callq  16a6 <printf>
     601:	eb fe                	jmp    601 <testinvalidargs+0x589>

  if (stat("/small.txt", &st) != 0) error("couldn't stat on '/small.txt'");
     603:	48 8d 45 dc          	lea    -0x24(%rbp),%rax
     607:	48 89 c6             	mov    %rax,%rsi
     60a:	bf 08 20 00 00       	mov    $0x2008,%edi
     60f:	e8 34 15 00 00       	callq  1b48 <stat>
     614:	85 c0                	test   %eax,%eax
     616:	74 4c                	je     664 <testinvalidargs+0x5ec>
     618:	8b 05 b2 26 00 00    	mov    0x26b2(%rip),%eax        # 2cd0 <stdout>
     61e:	ba 72 00 00 00       	mov    $0x72,%edx
     623:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     628:	89 c7                	mov    %eax,%edi
     62a:	b8 00 00 00 00       	mov    $0x0,%eax
     62f:	e8 72 10 00 00       	callq  16a6 <printf>
     634:	8b 05 96 26 00 00    	mov    0x2696(%rip),%eax        # 2cd0 <stdout>
     63a:	be a0 22 00 00       	mov    $0x22a0,%esi
     63f:	89 c7                	mov    %eax,%edi
     641:	b8 00 00 00 00       	mov    $0x0,%eax
     646:	e8 5b 10 00 00       	callq  16a6 <printf>
     64b:	8b 05 7f 26 00 00    	mov    0x267f(%rip),%eax        # 2cd0 <stdout>
     651:	be 06 20 00 00       	mov    $0x2006,%esi
     656:	89 c7                	mov    %eax,%edi
     658:	b8 00 00 00 00       	mov    $0x0,%eax
     65d:	e8 44 10 00 00       	callq  16a6 <printf>
     662:	eb fe                	jmp    662 <testinvalidargs+0x5ea>

  assert(st.type == T_FILE);
     664:	0f b7 45 dc          	movzwl -0x24(%rbp),%eax
     668:	66 83 f8 02          	cmp    $0x2,%ax
     66c:	74 23                	je     691 <testinvalidargs+0x619>
     66e:	8b 05 5c 26 00 00    	mov    0x265c(%rip),%eax        # 2cd0 <stdout>
     674:	b9 be 22 00 00       	mov    $0x22be,%ecx
     679:	ba 74 00 00 00       	mov    $0x74,%edx
     67e:	be d0 20 00 00       	mov    $0x20d0,%esi
     683:	89 c7                	mov    %eax,%edi
     685:	b8 00 00 00 00       	mov    $0x0,%eax
     68a:	e8 17 10 00 00       	callq  16a6 <printf>
     68f:	eb fe                	jmp    68f <testinvalidargs+0x617>
  assert(st.size == 26);
     691:	8b 45 e8             	mov    -0x18(%rbp),%eax
     694:	83 f8 1a             	cmp    $0x1a,%eax
     697:	74 23                	je     6bc <testinvalidargs+0x644>
     699:	8b 05 31 26 00 00    	mov    0x2631(%rip),%eax        # 2cd0 <stdout>
     69f:	b9 d0 22 00 00       	mov    $0x22d0,%ecx
     6a4:	ba 75 00 00 00       	mov    $0x75,%edx
     6a9:	be d0 20 00 00       	mov    $0x20d0,%esi
     6ae:	89 c7                	mov    %eax,%edi
     6b0:	b8 00 00 00 00       	mov    $0x0,%eax
     6b5:	e8 ec 0f 00 00       	callq  16a6 <printf>
     6ba:	eb fe                	jmp    6ba <testinvalidargs+0x642>

  printf(stdout, "passed argument checking for fstat\n");
     6bc:	8b 05 0e 26 00 00    	mov    0x260e(%rip),%eax        # 2cd0 <stdout>
     6c2:	be e0 22 00 00       	mov    $0x22e0,%esi
     6c7:	89 c7                	mov    %eax,%edi
     6c9:	b8 00 00 00 00       	mov    $0x0,%eax
     6ce:	e8 d3 0f 00 00       	callq  16a6 <printf>

  // dup
  if (dup(15) != -1)
     6d3:	bf 0f 00 00 00       	mov    $0xf,%edi
     6d8:	e8 e4 15 00 00       	callq  1cc1 <dup>
     6dd:	83 f8 ff             	cmp    $0xffffffff,%eax
     6e0:	74 4c                	je     72e <testinvalidargs+0x6b6>
    error("able to duplicated a non open file");
     6e2:	8b 05 e8 25 00 00    	mov    0x25e8(%rip),%eax        # 2cd0 <stdout>
     6e8:	ba 7b 00 00 00       	mov    $0x7b,%edx
     6ed:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     6f2:	89 c7                	mov    %eax,%edi
     6f4:	b8 00 00 00 00       	mov    $0x0,%eax
     6f9:	e8 a8 0f 00 00       	callq  16a6 <printf>
     6fe:	8b 05 cc 25 00 00    	mov    0x25cc(%rip),%eax        # 2cd0 <stdout>
     704:	be 08 23 00 00       	mov    $0x2308,%esi
     709:	89 c7                	mov    %eax,%edi
     70b:	b8 00 00 00 00       	mov    $0x0,%eax
     710:	e8 91 0f 00 00       	callq  16a6 <printf>
     715:	8b 05 b5 25 00 00    	mov    0x25b5(%rip),%eax        # 2cd0 <stdout>
     71b:	be 06 20 00 00       	mov    $0x2006,%esi
     720:	89 c7                	mov    %eax,%edi
     722:	b8 00 00 00 00       	mov    $0x0,%eax
     727:	e8 7a 0f 00 00       	callq  16a6 <printf>
     72c:	eb fe                	jmp    72c <testinvalidargs+0x6b4>

  printf(stdout, "passed argument checking for dup\n");
     72e:	8b 05 9c 25 00 00    	mov    0x259c(%rip),%eax        # 2cd0 <stdout>
     734:	be 30 23 00 00       	mov    $0x2330,%esi
     739:	89 c7                	mov    %eax,%edi
     73b:	b8 00 00 00 00       	mov    $0x0,%eax
     740:	e8 61 0f 00 00       	callq  16a6 <printf>

  // close
  if (close(15) != -1)
     745:	bf 0f 00 00 00       	mov    $0xf,%edi
     74a:	e8 22 15 00 00       	callq  1c71 <close>
     74f:	83 f8 ff             	cmp    $0xffffffff,%eax
     752:	74 4c                	je     7a0 <testinvalidargs+0x728>
    error("able to close non open file");
     754:	8b 05 76 25 00 00    	mov    0x2576(%rip),%eax        # 2cd0 <stdout>
     75a:	ba 81 00 00 00       	mov    $0x81,%edx
     75f:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     764:	89 c7                	mov    %eax,%edi
     766:	b8 00 00 00 00       	mov    $0x0,%eax
     76b:	e8 36 0f 00 00       	callq  16a6 <printf>
     770:	8b 05 5a 25 00 00    	mov    0x255a(%rip),%eax        # 2cd0 <stdout>
     776:	be 52 23 00 00       	mov    $0x2352,%esi
     77b:	89 c7                	mov    %eax,%edi
     77d:	b8 00 00 00 00       	mov    $0x0,%eax
     782:	e8 1f 0f 00 00       	callq  16a6 <printf>
     787:	8b 05 43 25 00 00    	mov    0x2543(%rip),%eax        # 2cd0 <stdout>
     78d:	be 06 20 00 00       	mov    $0x2006,%esi
     792:	89 c7                	mov    %eax,%edi
     794:	b8 00 00 00 00       	mov    $0x0,%eax
     799:	e8 08 0f 00 00       	callq  16a6 <printf>
     79e:	eb fe                	jmp    79e <testinvalidargs+0x726>

  if (close(fd) > 0 && close(fd) != -1)
     7a0:	8b 45 fc             	mov    -0x4(%rbp),%eax
     7a3:	89 c7                	mov    %eax,%edi
     7a5:	e8 c7 14 00 00       	callq  1c71 <close>
     7aa:	85 c0                	test   %eax,%eax
     7ac:	7e 5b                	jle    809 <testinvalidargs+0x791>
     7ae:	8b 45 fc             	mov    -0x4(%rbp),%eax
     7b1:	89 c7                	mov    %eax,%edi
     7b3:	e8 b9 14 00 00       	callq  1c71 <close>
     7b8:	83 f8 ff             	cmp    $0xffffffff,%eax
     7bb:	74 4c                	je     809 <testinvalidargs+0x791>
    error("able to close same file twice");
     7bd:	8b 05 0d 25 00 00    	mov    0x250d(%rip),%eax        # 2cd0 <stdout>
     7c3:	ba 84 00 00 00       	mov    $0x84,%edx
     7c8:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     7cd:	89 c7                	mov    %eax,%edi
     7cf:	b8 00 00 00 00       	mov    $0x0,%eax
     7d4:	e8 cd 0e 00 00       	callq  16a6 <printf>
     7d9:	8b 05 f1 24 00 00    	mov    0x24f1(%rip),%eax        # 2cd0 <stdout>
     7df:	be 6e 23 00 00       	mov    $0x236e,%esi
     7e4:	89 c7                	mov    %eax,%edi
     7e6:	b8 00 00 00 00       	mov    $0x0,%eax
     7eb:	e8 b6 0e 00 00       	callq  16a6 <printf>
     7f0:	8b 05 da 24 00 00    	mov    0x24da(%rip),%eax        # 2cd0 <stdout>
     7f6:	be 06 20 00 00       	mov    $0x2006,%esi
     7fb:	89 c7                	mov    %eax,%edi
     7fd:	b8 00 00 00 00       	mov    $0x0,%eax
     802:	e8 9f 0e 00 00       	callq  16a6 <printf>
     807:	eb fe                	jmp    807 <testinvalidargs+0x78f>

  printf(stdout, "passed argument checking for close\n");
     809:	8b 05 c1 24 00 00    	mov    0x24c1(%rip),%eax        # 2cd0 <stdout>
     80f:	be 90 23 00 00       	mov    $0x2390,%esi
     814:	89 c7                	mov    %eax,%edi
     816:	b8 00 00 00 00       	mov    $0x0,%eax
     81b:	e8 86 0e 00 00       	callq  16a6 <printf>
}
     820:	90                   	nop
     821:	c9                   	leaveq 
     822:	c3                   	retq   

0000000000000823 <smallfilereadtest>:

void smallfilereadtest(void) {
     823:	55                   	push   %rbp
     824:	48 89 e5             	mov    %rsp,%rbp
     827:	48 83 ec 20          	sub    $0x20,%rsp
  int fd, i;
  char buf[11];

  printf(stdout, "small file test\n");
     82b:	8b 05 9f 24 00 00    	mov    0x249f(%rip),%eax        # 2cd0 <stdout>
     831:	be b4 23 00 00       	mov    $0x23b4,%esi
     836:	89 c7                	mov    %eax,%edi
     838:	b8 00 00 00 00       	mov    $0x0,%eax
     83d:	e8 64 0e 00 00       	callq  16a6 <printf>
  // Test read only funcionality
  fd = open("/small.txt", O_RDONLY);
     842:	be 00 00 00 00       	mov    $0x0,%esi
     847:	bf 08 20 00 00       	mov    $0x2008,%edi
     84c:	e8 38 14 00 00       	callq  1c89 <open>
     851:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
     854:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     858:	79 4c                	jns    8a6 <smallfilereadtest+0x83>
    error("unable to open small file");
     85a:	8b 05 70 24 00 00    	mov    0x2470(%rip),%eax        # 2cd0 <stdout>
     860:	ba 91 00 00 00       	mov    $0x91,%edx
     865:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     86a:	89 c7                	mov    %eax,%edi
     86c:	b8 00 00 00 00       	mov    $0x0,%eax
     871:	e8 30 0e 00 00       	callq  16a6 <printf>
     876:	8b 05 54 24 00 00    	mov    0x2454(%rip),%eax        # 2cd0 <stdout>
     87c:	be c5 23 00 00       	mov    $0x23c5,%esi
     881:	89 c7                	mov    %eax,%edi
     883:	b8 00 00 00 00       	mov    $0x0,%eax
     888:	e8 19 0e 00 00       	callq  16a6 <printf>
     88d:	8b 05 3d 24 00 00    	mov    0x243d(%rip),%eax        # 2cd0 <stdout>
     893:	be 06 20 00 00       	mov    $0x2006,%esi
     898:	89 c7                	mov    %eax,%edi
     89a:	b8 00 00 00 00       	mov    $0x0,%eax
     89f:	e8 02 0e 00 00       	callq  16a6 <printf>
     8a4:	eb fe                	jmp    8a4 <smallfilereadtest+0x81>

  printf(stdout, "open small file succeeded; ok\n");
     8a6:	8b 05 24 24 00 00    	mov    0x2424(%rip),%eax        # 2cd0 <stdout>
     8ac:	be e0 23 00 00       	mov    $0x23e0,%esi
     8b1:	89 c7                	mov    %eax,%edi
     8b3:	b8 00 00 00 00       	mov    $0x0,%eax
     8b8:	e8 e9 0d 00 00       	callq  16a6 <printf>

  if ((i = read(fd, buf, 10)) != 10)
     8bd:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     8c1:	8b 45 fc             	mov    -0x4(%rbp),%eax
     8c4:	ba 0a 00 00 00       	mov    $0xa,%edx
     8c9:	48 89 ce             	mov    %rcx,%rsi
     8cc:	89 c7                	mov    %eax,%edi
     8ce:	e8 8e 13 00 00       	callq  1c61 <read>
     8d3:	89 45 f8             	mov    %eax,-0x8(%rbp)
     8d6:	83 7d f8 0a          	cmpl   $0xa,-0x8(%rbp)
     8da:	74 4f                	je     92b <smallfilereadtest+0x108>
    error("read of first 10 bytes unsucessful was %d bytes", i);
     8dc:	8b 05 ee 23 00 00    	mov    0x23ee(%rip),%eax        # 2cd0 <stdout>
     8e2:	ba 96 00 00 00       	mov    $0x96,%edx
     8e7:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     8ec:	89 c7                	mov    %eax,%edi
     8ee:	b8 00 00 00 00       	mov    $0x0,%eax
     8f3:	e8 ae 0d 00 00       	callq  16a6 <printf>
     8f8:	8b 05 d2 23 00 00    	mov    0x23d2(%rip),%eax        # 2cd0 <stdout>
     8fe:	8b 55 f8             	mov    -0x8(%rbp),%edx
     901:	be 00 24 00 00       	mov    $0x2400,%esi
     906:	89 c7                	mov    %eax,%edi
     908:	b8 00 00 00 00       	mov    $0x0,%eax
     90d:	e8 94 0d 00 00       	callq  16a6 <printf>
     912:	8b 05 b8 23 00 00    	mov    0x23b8(%rip),%eax        # 2cd0 <stdout>
     918:	be 06 20 00 00       	mov    $0x2006,%esi
     91d:	89 c7                	mov    %eax,%edi
     91f:	b8 00 00 00 00       	mov    $0x0,%eax
     924:	e8 7d 0d 00 00       	callq  16a6 <printf>
     929:	eb fe                	jmp    929 <smallfilereadtest+0x106>

  buf[10] = 0;
     92b:	c6 45 f7 00          	movb   $0x0,-0x9(%rbp)
  if (strcmp(buf, "aaaaaaaaaa") != 0)
     92f:	48 8d 45 ed          	lea    -0x13(%rbp),%rax
     933:	be 30 24 00 00       	mov    $0x2430,%esi
     938:	48 89 c7             	mov    %rax,%rdi
     93b:	e8 a3 10 00 00       	callq  19e3 <strcmp>
     940:	85 c0                	test   %eax,%eax
     942:	74 50                	je     994 <smallfilereadtest+0x171>
    error("buf was not 10 a's, was: '%s'", buf);
     944:	8b 05 86 23 00 00    	mov    0x2386(%rip),%eax        # 2cd0 <stdout>
     94a:	ba 9a 00 00 00       	mov    $0x9a,%edx
     94f:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     954:	89 c7                	mov    %eax,%edi
     956:	b8 00 00 00 00       	mov    $0x0,%eax
     95b:	e8 46 0d 00 00       	callq  16a6 <printf>
     960:	8b 05 6a 23 00 00    	mov    0x236a(%rip),%eax        # 2cd0 <stdout>
     966:	48 8d 55 ed          	lea    -0x13(%rbp),%rdx
     96a:	be 3b 24 00 00       	mov    $0x243b,%esi
     96f:	89 c7                	mov    %eax,%edi
     971:	b8 00 00 00 00       	mov    $0x0,%eax
     976:	e8 2b 0d 00 00       	callq  16a6 <printf>
     97b:	8b 05 4f 23 00 00    	mov    0x234f(%rip),%eax        # 2cd0 <stdout>
     981:	be 06 20 00 00       	mov    $0x2006,%esi
     986:	89 c7                	mov    %eax,%edi
     988:	b8 00 00 00 00       	mov    $0x0,%eax
     98d:	e8 14 0d 00 00       	callq  16a6 <printf>
     992:	eb fe                	jmp    992 <smallfilereadtest+0x16f>
  printf(stdout, "read of first 10 bytes sucessful\n");
     994:	8b 05 36 23 00 00    	mov    0x2336(%rip),%eax        # 2cd0 <stdout>
     99a:	be 60 24 00 00       	mov    $0x2460,%esi
     99f:	89 c7                	mov    %eax,%edi
     9a1:	b8 00 00 00 00       	mov    $0x0,%eax
     9a6:	e8 fb 0c 00 00       	callq  16a6 <printf>

  if ((i = read(fd, buf, 10)) != 10)
     9ab:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     9af:	8b 45 fc             	mov    -0x4(%rbp),%eax
     9b2:	ba 0a 00 00 00       	mov    $0xa,%edx
     9b7:	48 89 ce             	mov    %rcx,%rsi
     9ba:	89 c7                	mov    %eax,%edi
     9bc:	e8 a0 12 00 00       	callq  1c61 <read>
     9c1:	89 45 f8             	mov    %eax,-0x8(%rbp)
     9c4:	83 7d f8 0a          	cmpl   $0xa,-0x8(%rbp)
     9c8:	74 4f                	je     a19 <smallfilereadtest+0x1f6>
    error("read of second 10 bytes unsucessful was %d bytes", i);
     9ca:	8b 05 00 23 00 00    	mov    0x2300(%rip),%eax        # 2cd0 <stdout>
     9d0:	ba 9e 00 00 00       	mov    $0x9e,%edx
     9d5:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     9da:	89 c7                	mov    %eax,%edi
     9dc:	b8 00 00 00 00       	mov    $0x0,%eax
     9e1:	e8 c0 0c 00 00       	callq  16a6 <printf>
     9e6:	8b 05 e4 22 00 00    	mov    0x22e4(%rip),%eax        # 2cd0 <stdout>
     9ec:	8b 55 f8             	mov    -0x8(%rbp),%edx
     9ef:	be 88 24 00 00       	mov    $0x2488,%esi
     9f4:	89 c7                	mov    %eax,%edi
     9f6:	b8 00 00 00 00       	mov    $0x0,%eax
     9fb:	e8 a6 0c 00 00       	callq  16a6 <printf>
     a00:	8b 05 ca 22 00 00    	mov    0x22ca(%rip),%eax        # 2cd0 <stdout>
     a06:	be 06 20 00 00       	mov    $0x2006,%esi
     a0b:	89 c7                	mov    %eax,%edi
     a0d:	b8 00 00 00 00       	mov    $0x0,%eax
     a12:	e8 8f 0c 00 00       	callq  16a6 <printf>
     a17:	eb fe                	jmp    a17 <smallfilereadtest+0x1f4>

  buf[10] = 0;
     a19:	c6 45 f7 00          	movb   $0x0,-0x9(%rbp)
  if (strcmp(buf, "bbbbbbbbbb") != 0)
     a1d:	48 8d 45 ed          	lea    -0x13(%rbp),%rax
     a21:	be b9 24 00 00       	mov    $0x24b9,%esi
     a26:	48 89 c7             	mov    %rax,%rdi
     a29:	e8 b5 0f 00 00       	callq  19e3 <strcmp>
     a2e:	85 c0                	test   %eax,%eax
     a30:	74 50                	je     a82 <smallfilereadtest+0x25f>
    error("buf was not 10 b's, was: '%s'", buf);
     a32:	8b 05 98 22 00 00    	mov    0x2298(%rip),%eax        # 2cd0 <stdout>
     a38:	ba a2 00 00 00       	mov    $0xa2,%edx
     a3d:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     a42:	89 c7                	mov    %eax,%edi
     a44:	b8 00 00 00 00       	mov    $0x0,%eax
     a49:	e8 58 0c 00 00       	callq  16a6 <printf>
     a4e:	8b 05 7c 22 00 00    	mov    0x227c(%rip),%eax        # 2cd0 <stdout>
     a54:	48 8d 55 ed          	lea    -0x13(%rbp),%rdx
     a58:	be c4 24 00 00       	mov    $0x24c4,%esi
     a5d:	89 c7                	mov    %eax,%edi
     a5f:	b8 00 00 00 00       	mov    $0x0,%eax
     a64:	e8 3d 0c 00 00       	callq  16a6 <printf>
     a69:	8b 05 61 22 00 00    	mov    0x2261(%rip),%eax        # 2cd0 <stdout>
     a6f:	be 06 20 00 00       	mov    $0x2006,%esi
     a74:	89 c7                	mov    %eax,%edi
     a76:	b8 00 00 00 00       	mov    $0x0,%eax
     a7b:	e8 26 0c 00 00       	callq  16a6 <printf>
     a80:	eb fe                	jmp    a80 <smallfilereadtest+0x25d>
  printf(stdout, "read of second 10 bytes sucessful\n");
     a82:	8b 05 48 22 00 00    	mov    0x2248(%rip),%eax        # 2cd0 <stdout>
     a88:	be e8 24 00 00       	mov    $0x24e8,%esi
     a8d:	89 c7                	mov    %eax,%edi
     a8f:	b8 00 00 00 00       	mov    $0x0,%eax
     a94:	e8 0d 0c 00 00       	callq  16a6 <printf>

  // only 25 byte file
  if ((i = read(fd, buf, 10)) != 6)
     a99:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     a9d:	8b 45 fc             	mov    -0x4(%rbp),%eax
     aa0:	ba 0a 00 00 00       	mov    $0xa,%edx
     aa5:	48 89 ce             	mov    %rcx,%rsi
     aa8:	89 c7                	mov    %eax,%edi
     aaa:	e8 b2 11 00 00       	callq  1c61 <read>
     aaf:	89 45 f8             	mov    %eax,-0x8(%rbp)
     ab2:	83 7d f8 06          	cmpl   $0x6,-0x8(%rbp)
     ab6:	74 4f                	je     b07 <smallfilereadtest+0x2e4>
    error("read of last 6 bytes unsucessful was %d bytes", i);
     ab8:	8b 05 12 22 00 00    	mov    0x2212(%rip),%eax        # 2cd0 <stdout>
     abe:	ba a7 00 00 00       	mov    $0xa7,%edx
     ac3:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     ac8:	89 c7                	mov    %eax,%edi
     aca:	b8 00 00 00 00       	mov    $0x0,%eax
     acf:	e8 d2 0b 00 00       	callq  16a6 <printf>
     ad4:	8b 05 f6 21 00 00    	mov    0x21f6(%rip),%eax        # 2cd0 <stdout>
     ada:	8b 55 f8             	mov    -0x8(%rbp),%edx
     add:	be 10 25 00 00       	mov    $0x2510,%esi
     ae2:	89 c7                	mov    %eax,%edi
     ae4:	b8 00 00 00 00       	mov    $0x0,%eax
     ae9:	e8 b8 0b 00 00       	callq  16a6 <printf>
     aee:	8b 05 dc 21 00 00    	mov    0x21dc(%rip),%eax        # 2cd0 <stdout>
     af4:	be 06 20 00 00       	mov    $0x2006,%esi
     af9:	89 c7                	mov    %eax,%edi
     afb:	b8 00 00 00 00       	mov    $0x0,%eax
     b00:	e8 a1 0b 00 00       	callq  16a6 <printf>
     b05:	eb fe                	jmp    b05 <smallfilereadtest+0x2e2>

  buf[6] = 0;
     b07:	c6 45 f3 00          	movb   $0x0,-0xd(%rbp)
  if (strcmp(buf, "ccccc\n") != 0)
     b0b:	48 8d 45 ed          	lea    -0x13(%rbp),%rax
     b0f:	be 3e 25 00 00       	mov    $0x253e,%esi
     b14:	48 89 c7             	mov    %rax,%rdi
     b17:	e8 c7 0e 00 00       	callq  19e3 <strcmp>
     b1c:	85 c0                	test   %eax,%eax
     b1e:	74 50                	je     b70 <smallfilereadtest+0x34d>
    error("buf was not 5 c's (and a newline), was: '%s'", buf);
     b20:	8b 05 aa 21 00 00    	mov    0x21aa(%rip),%eax        # 2cd0 <stdout>
     b26:	ba ab 00 00 00       	mov    $0xab,%edx
     b2b:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     b30:	89 c7                	mov    %eax,%edi
     b32:	b8 00 00 00 00       	mov    $0x0,%eax
     b37:	e8 6a 0b 00 00       	callq  16a6 <printf>
     b3c:	8b 05 8e 21 00 00    	mov    0x218e(%rip),%eax        # 2cd0 <stdout>
     b42:	48 8d 55 ed          	lea    -0x13(%rbp),%rdx
     b46:	be 48 25 00 00       	mov    $0x2548,%esi
     b4b:	89 c7                	mov    %eax,%edi
     b4d:	b8 00 00 00 00       	mov    $0x0,%eax
     b52:	e8 4f 0b 00 00       	callq  16a6 <printf>
     b57:	8b 05 73 21 00 00    	mov    0x2173(%rip),%eax        # 2cd0 <stdout>
     b5d:	be 06 20 00 00       	mov    $0x2006,%esi
     b62:	89 c7                	mov    %eax,%edi
     b64:	b8 00 00 00 00       	mov    $0x0,%eax
     b69:	e8 38 0b 00 00       	callq  16a6 <printf>
     b6e:	eb fe                	jmp    b6e <smallfilereadtest+0x34b>

  printf(stdout, "read of last 5 bytes sucessful\n");
     b70:	8b 05 5a 21 00 00    	mov    0x215a(%rip),%eax        # 2cd0 <stdout>
     b76:	be 78 25 00 00       	mov    $0x2578,%esi
     b7b:	89 c7                	mov    %eax,%edi
     b7d:	b8 00 00 00 00       	mov    $0x0,%eax
     b82:	e8 1f 0b 00 00       	callq  16a6 <printf>

  if (read(fd, buf, 10) != 0)
     b87:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     b8b:	8b 45 fc             	mov    -0x4(%rbp),%eax
     b8e:	ba 0a 00 00 00       	mov    $0xa,%edx
     b93:	48 89 ce             	mov    %rcx,%rsi
     b96:	89 c7                	mov    %eax,%edi
     b98:	e8 c4 10 00 00       	callq  1c61 <read>
     b9d:	85 c0                	test   %eax,%eax
     b9f:	74 4c                	je     bed <smallfilereadtest+0x3ca>
    error("read more bytes than should be possible");
     ba1:	8b 05 29 21 00 00    	mov    0x2129(%rip),%eax        # 2cd0 <stdout>
     ba7:	ba b0 00 00 00       	mov    $0xb0,%edx
     bac:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     bb1:	89 c7                	mov    %eax,%edi
     bb3:	b8 00 00 00 00       	mov    $0x0,%eax
     bb8:	e8 e9 0a 00 00       	callq  16a6 <printf>
     bbd:	8b 05 0d 21 00 00    	mov    0x210d(%rip),%eax        # 2cd0 <stdout>
     bc3:	be 98 25 00 00       	mov    $0x2598,%esi
     bc8:	89 c7                	mov    %eax,%edi
     bca:	b8 00 00 00 00       	mov    $0x0,%eax
     bcf:	e8 d2 0a 00 00       	callq  16a6 <printf>
     bd4:	8b 05 f6 20 00 00    	mov    0x20f6(%rip),%eax        # 2cd0 <stdout>
     bda:	be 06 20 00 00       	mov    $0x2006,%esi
     bdf:	89 c7                	mov    %eax,%edi
     be1:	b8 00 00 00 00       	mov    $0x0,%eax
     be6:	e8 bb 0a 00 00       	callq  16a6 <printf>
     beb:	eb fe                	jmp    beb <smallfilereadtest+0x3c8>

  strcpy(buf, "stdout\n");
     bed:	48 8d 45 ed          	lea    -0x13(%rbp),%rax
     bf1:	be c0 25 00 00       	mov    $0x25c0,%esi
     bf6:	48 89 c7             	mov    %rax,%rdi
     bf9:	e8 a6 0d 00 00       	callq  19a4 <strcpy>

  if ((i = write(stdout, buf, 7)) != 7)
     bfe:	8b 05 cc 20 00 00    	mov    0x20cc(%rip),%eax        # 2cd0 <stdout>
     c04:	48 8d 4d ed          	lea    -0x13(%rbp),%rcx
     c08:	ba 07 00 00 00       	mov    $0x7,%edx
     c0d:	48 89 ce             	mov    %rcx,%rsi
     c10:	89 c7                	mov    %eax,%edi
     c12:	e8 52 10 00 00       	callq  1c69 <write>
     c17:	89 45 f8             	mov    %eax,-0x8(%rbp)
     c1a:	83 7d f8 07          	cmpl   $0x7,-0x8(%rbp)
     c1e:	74 4f                	je     c6f <smallfilereadtest+0x44c>
    error("wasn't able to write to stdout, return value was '%d'", i);
     c20:	8b 05 aa 20 00 00    	mov    0x20aa(%rip),%eax        # 2cd0 <stdout>
     c26:	ba b5 00 00 00       	mov    $0xb5,%edx
     c2b:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     c30:	89 c7                	mov    %eax,%edi
     c32:	b8 00 00 00 00       	mov    $0x0,%eax
     c37:	e8 6a 0a 00 00       	callq  16a6 <printf>
     c3c:	8b 05 8e 20 00 00    	mov    0x208e(%rip),%eax        # 2cd0 <stdout>
     c42:	8b 55 f8             	mov    -0x8(%rbp),%edx
     c45:	be c8 25 00 00       	mov    $0x25c8,%esi
     c4a:	89 c7                	mov    %eax,%edi
     c4c:	b8 00 00 00 00       	mov    $0x0,%eax
     c51:	e8 50 0a 00 00       	callq  16a6 <printf>
     c56:	8b 05 74 20 00 00    	mov    0x2074(%rip),%eax        # 2cd0 <stdout>
     c5c:	be 06 20 00 00       	mov    $0x2006,%esi
     c61:	89 c7                	mov    %eax,%edi
     c63:	b8 00 00 00 00       	mov    $0x0,%eax
     c68:	e8 39 0a 00 00       	callq  16a6 <printf>
     c6d:	eb fe                	jmp    c6d <smallfilereadtest+0x44a>

  printf(stdout, "write to stdout directly ok\n");
     c6f:	8b 05 5b 20 00 00    	mov    0x205b(%rip),%eax        # 2cd0 <stdout>
     c75:	be fe 25 00 00       	mov    $0x25fe,%esi
     c7a:	89 c7                	mov    %eax,%edi
     c7c:	b8 00 00 00 00       	mov    $0x0,%eax
     c81:	e8 20 0a 00 00       	callq  16a6 <printf>

  if (close(fd) != 0)
     c86:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c89:	89 c7                	mov    %eax,%edi
     c8b:	e8 e1 0f 00 00       	callq  1c71 <close>
     c90:	85 c0                	test   %eax,%eax
     c92:	74 4c                	je     ce0 <smallfilereadtest+0x4bd>
    error("error closing fd");
     c94:	8b 05 36 20 00 00    	mov    0x2036(%rip),%eax        # 2cd0 <stdout>
     c9a:	ba ba 00 00 00       	mov    $0xba,%edx
     c9f:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     ca4:	89 c7                	mov    %eax,%edi
     ca6:	b8 00 00 00 00       	mov    $0x0,%eax
     cab:	e8 f6 09 00 00       	callq  16a6 <printf>
     cb0:	8b 05 1a 20 00 00    	mov    0x201a(%rip),%eax        # 2cd0 <stdout>
     cb6:	be 1b 26 00 00       	mov    $0x261b,%esi
     cbb:	89 c7                	mov    %eax,%edi
     cbd:	b8 00 00 00 00       	mov    $0x0,%eax
     cc2:	e8 df 09 00 00       	callq  16a6 <printf>
     cc7:	8b 05 03 20 00 00    	mov    0x2003(%rip),%eax        # 2cd0 <stdout>
     ccd:	be 06 20 00 00       	mov    $0x2006,%esi
     cd2:	89 c7                	mov    %eax,%edi
     cd4:	b8 00 00 00 00       	mov    $0x0,%eax
     cd9:	e8 c8 09 00 00       	callq  16a6 <printf>
     cde:	eb fe                	jmp    cde <smallfilereadtest+0x4bb>

  printf(stdout, "small file test ok\n");
     ce0:	8b 05 ea 1f 00 00    	mov    0x1fea(%rip),%eax        # 2cd0 <stdout>
     ce6:	be 2c 26 00 00       	mov    $0x262c,%esi
     ceb:	89 c7                	mov    %eax,%edi
     ced:	b8 00 00 00 00       	mov    $0x0,%eax
     cf2:	e8 af 09 00 00       	callq  16a6 <printf>
}
     cf7:	90                   	nop
     cf8:	c9                   	leaveq 
     cf9:	c3                   	retq   

0000000000000cfa <duptest>:

void duptest(void) {
     cfa:	55                   	push   %rbp
     cfb:	48 89 e5             	mov    %rsp,%rbp
     cfe:	53                   	push   %rbx
     cff:	48 81 ec 88 00 00 00 	sub    $0x88,%rsp
  int fd1, fd2, stdout_cpy;
  char buf[100];

  printf(stdout, "dup test\n");
     d06:	8b 05 c4 1f 00 00    	mov    0x1fc4(%rip),%eax        # 2cd0 <stdout>
     d0c:	be 40 26 00 00       	mov    $0x2640,%esi
     d11:	89 c7                	mov    %eax,%edi
     d13:	b8 00 00 00 00       	mov    $0x0,%eax
     d18:	e8 89 09 00 00       	callq  16a6 <printf>
  // Test read only funcionality
  fd1 = open("/small.txt", O_RDONLY);
     d1d:	be 00 00 00 00       	mov    $0x0,%esi
     d22:	bf 08 20 00 00       	mov    $0x2008,%edi
     d27:	e8 5d 0f 00 00       	callq  1c89 <open>
     d2c:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if (fd1 < 0)
     d2f:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
     d33:	79 4c                	jns    d81 <duptest+0x87>
    error("unable to open small file");
     d35:	8b 05 95 1f 00 00    	mov    0x1f95(%rip),%eax        # 2cd0 <stdout>
     d3b:	ba c7 00 00 00       	mov    $0xc7,%edx
     d40:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     d45:	89 c7                	mov    %eax,%edi
     d47:	b8 00 00 00 00       	mov    $0x0,%eax
     d4c:	e8 55 09 00 00       	callq  16a6 <printf>
     d51:	8b 05 79 1f 00 00    	mov    0x1f79(%rip),%eax        # 2cd0 <stdout>
     d57:	be c5 23 00 00       	mov    $0x23c5,%esi
     d5c:	89 c7                	mov    %eax,%edi
     d5e:	b8 00 00 00 00       	mov    $0x0,%eax
     d63:	e8 3e 09 00 00       	callq  16a6 <printf>
     d68:	8b 05 62 1f 00 00    	mov    0x1f62(%rip),%eax        # 2cd0 <stdout>
     d6e:	be 06 20 00 00       	mov    $0x2006,%esi
     d73:	89 c7                	mov    %eax,%edi
     d75:	b8 00 00 00 00       	mov    $0x0,%eax
     d7a:	e8 27 09 00 00       	callq  16a6 <printf>
     d7f:	eb fe                	jmp    d7f <duptest+0x85>

  if ((fd2 = dup(fd1)) != fd1 + 1)
     d81:	8b 45 ec             	mov    -0x14(%rbp),%eax
     d84:	89 c7                	mov    %eax,%edi
     d86:	e8 36 0f 00 00       	callq  1cc1 <dup>
     d8b:	89 45 e8             	mov    %eax,-0x18(%rbp)
     d8e:	8b 45 ec             	mov    -0x14(%rbp),%eax
     d91:	83 c0 01             	add    $0x1,%eax
     d94:	39 45 e8             	cmp    %eax,-0x18(%rbp)
     d97:	74 4f                	je     de8 <duptest+0xee>
    error("returned fd from dup was not the smallest free fd, was '%d'", fd2);
     d99:	8b 05 31 1f 00 00    	mov    0x1f31(%rip),%eax        # 2cd0 <stdout>
     d9f:	ba ca 00 00 00       	mov    $0xca,%edx
     da4:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     da9:	89 c7                	mov    %eax,%edi
     dab:	b8 00 00 00 00       	mov    $0x0,%eax
     db0:	e8 f1 08 00 00       	callq  16a6 <printf>
     db5:	8b 05 15 1f 00 00    	mov    0x1f15(%rip),%eax        # 2cd0 <stdout>
     dbb:	8b 55 e8             	mov    -0x18(%rbp),%edx
     dbe:	be 50 26 00 00       	mov    $0x2650,%esi
     dc3:	89 c7                	mov    %eax,%edi
     dc5:	b8 00 00 00 00       	mov    $0x0,%eax
     dca:	e8 d7 08 00 00       	callq  16a6 <printf>
     dcf:	8b 05 fb 1e 00 00    	mov    0x1efb(%rip),%eax        # 2cd0 <stdout>
     dd5:	be 06 20 00 00       	mov    $0x2006,%esi
     dda:	89 c7                	mov    %eax,%edi
     ddc:	b8 00 00 00 00       	mov    $0x0,%eax
     de1:	e8 c0 08 00 00       	callq  16a6 <printf>
     de6:	eb fe                	jmp    de6 <duptest+0xec>

  // test offsets are respected in dupped files
  assert(read(fd1, buf, 10) == 10);
     de8:	48 8d 8d 74 ff ff ff 	lea    -0x8c(%rbp),%rcx
     def:	8b 45 ec             	mov    -0x14(%rbp),%eax
     df2:	ba 0a 00 00 00       	mov    $0xa,%edx
     df7:	48 89 ce             	mov    %rcx,%rsi
     dfa:	89 c7                	mov    %eax,%edi
     dfc:	e8 60 0e 00 00       	callq  1c61 <read>
     e01:	83 f8 0a             	cmp    $0xa,%eax
     e04:	74 23                	je     e29 <duptest+0x12f>
     e06:	8b 05 c4 1e 00 00    	mov    0x1ec4(%rip),%eax        # 2cd0 <stdout>
     e0c:	b9 8c 26 00 00       	mov    $0x268c,%ecx
     e11:	ba cd 00 00 00       	mov    $0xcd,%edx
     e16:	be d0 20 00 00       	mov    $0x20d0,%esi
     e1b:	89 c7                	mov    %eax,%edi
     e1d:	b8 00 00 00 00       	mov    $0x0,%eax
     e22:	e8 7f 08 00 00       	callq  16a6 <printf>
     e27:	eb fe                	jmp    e27 <duptest+0x12d>
  buf[10] = 0;
     e29:	c6 85 7e ff ff ff 00 	movb   $0x0,-0x82(%rbp)

  if (strcmp(buf, "aaaaaaaaaa") != 0)
     e30:	48 8d 85 74 ff ff ff 	lea    -0x8c(%rbp),%rax
     e37:	be 30 24 00 00       	mov    $0x2430,%esi
     e3c:	48 89 c7             	mov    %rax,%rdi
     e3f:	e8 9f 0b 00 00       	callq  19e3 <strcmp>
     e44:	85 c0                	test   %eax,%eax
     e46:	74 4c                	je     e94 <duptest+0x19a>
    error("couldn't read from original fd after dup");
     e48:	8b 05 82 1e 00 00    	mov    0x1e82(%rip),%eax        # 2cd0 <stdout>
     e4e:	ba d1 00 00 00       	mov    $0xd1,%edx
     e53:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     e58:	89 c7                	mov    %eax,%edi
     e5a:	b8 00 00 00 00       	mov    $0x0,%eax
     e5f:	e8 42 08 00 00       	callq  16a6 <printf>
     e64:	8b 05 66 1e 00 00    	mov    0x1e66(%rip),%eax        # 2cd0 <stdout>
     e6a:	be a8 26 00 00       	mov    $0x26a8,%esi
     e6f:	89 c7                	mov    %eax,%edi
     e71:	b8 00 00 00 00       	mov    $0x0,%eax
     e76:	e8 2b 08 00 00       	callq  16a6 <printf>
     e7b:	8b 05 4f 1e 00 00    	mov    0x1e4f(%rip),%eax        # 2cd0 <stdout>
     e81:	be 06 20 00 00       	mov    $0x2006,%esi
     e86:	89 c7                	mov    %eax,%edi
     e88:	b8 00 00 00 00       	mov    $0x0,%eax
     e8d:	e8 14 08 00 00       	callq  16a6 <printf>
     e92:	eb fe                	jmp    e92 <duptest+0x198>

  if (read(fd2, buf, 10) != 10)
     e94:	48 8d 8d 74 ff ff ff 	lea    -0x8c(%rbp),%rcx
     e9b:	8b 45 e8             	mov    -0x18(%rbp),%eax
     e9e:	ba 0a 00 00 00       	mov    $0xa,%edx
     ea3:	48 89 ce             	mov    %rcx,%rsi
     ea6:	89 c7                	mov    %eax,%edi
     ea8:	e8 b4 0d 00 00       	callq  1c61 <read>
     ead:	83 f8 0a             	cmp    $0xa,%eax
     eb0:	74 4c                	je     efe <duptest+0x204>
    error("coudn't read from the dupped fd");
     eb2:	8b 05 18 1e 00 00    	mov    0x1e18(%rip),%eax        # 2cd0 <stdout>
     eb8:	ba d4 00 00 00       	mov    $0xd4,%edx
     ebd:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     ec2:	89 c7                	mov    %eax,%edi
     ec4:	b8 00 00 00 00       	mov    $0x0,%eax
     ec9:	e8 d8 07 00 00       	callq  16a6 <printf>
     ece:	8b 05 fc 1d 00 00    	mov    0x1dfc(%rip),%eax        # 2cd0 <stdout>
     ed4:	be d8 26 00 00       	mov    $0x26d8,%esi
     ed9:	89 c7                	mov    %eax,%edi
     edb:	b8 00 00 00 00       	mov    $0x0,%eax
     ee0:	e8 c1 07 00 00       	callq  16a6 <printf>
     ee5:	8b 05 e5 1d 00 00    	mov    0x1de5(%rip),%eax        # 2cd0 <stdout>
     eeb:	be 06 20 00 00       	mov    $0x2006,%esi
     ef0:	89 c7                	mov    %eax,%edi
     ef2:	b8 00 00 00 00       	mov    $0x0,%eax
     ef7:	e8 aa 07 00 00       	callq  16a6 <printf>
     efc:	eb fe                	jmp    efc <duptest+0x202>
  buf[10] = 0;
     efe:	c6 85 7e ff ff ff 00 	movb   $0x0,-0x82(%rbp)

  if (strcmp(buf, "aaaaaaaaaa") == 0)
     f05:	48 8d 85 74 ff ff ff 	lea    -0x8c(%rbp),%rax
     f0c:	be 30 24 00 00       	mov    $0x2430,%esi
     f11:	48 89 c7             	mov    %rax,%rdi
     f14:	e8 ca 0a 00 00       	callq  19e3 <strcmp>
     f19:	85 c0                	test   %eax,%eax
     f1b:	75 4c                	jne    f69 <duptest+0x26f>
    error("the duped fd didn't respect the read offset from the other file.");
     f1d:	8b 05 ad 1d 00 00    	mov    0x1dad(%rip),%eax        # 2cd0 <stdout>
     f23:	ba d8 00 00 00       	mov    $0xd8,%edx
     f28:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     f2d:	89 c7                	mov    %eax,%edi
     f2f:	b8 00 00 00 00       	mov    $0x0,%eax
     f34:	e8 6d 07 00 00       	callq  16a6 <printf>
     f39:	8b 05 91 1d 00 00    	mov    0x1d91(%rip),%eax        # 2cd0 <stdout>
     f3f:	be f8 26 00 00       	mov    $0x26f8,%esi
     f44:	89 c7                	mov    %eax,%edi
     f46:	b8 00 00 00 00       	mov    $0x0,%eax
     f4b:	e8 56 07 00 00       	callq  16a6 <printf>
     f50:	8b 05 7a 1d 00 00    	mov    0x1d7a(%rip),%eax        # 2cd0 <stdout>
     f56:	be 06 20 00 00       	mov    $0x2006,%esi
     f5b:	89 c7                	mov    %eax,%edi
     f5d:	b8 00 00 00 00       	mov    $0x0,%eax
     f62:	e8 3f 07 00 00       	callq  16a6 <printf>
     f67:	eb fe                	jmp    f67 <duptest+0x26d>

  if (strcmp(buf, "bbbbbbbbbb") != 0)
     f69:	48 8d 85 74 ff ff ff 	lea    -0x8c(%rbp),%rax
     f70:	be b9 24 00 00       	mov    $0x24b9,%esi
     f75:	48 89 c7             	mov    %rax,%rdi
     f78:	e8 66 0a 00 00       	callq  19e3 <strcmp>
     f7d:	85 c0                	test   %eax,%eax
     f7f:	74 4c                	je     fcd <duptest+0x2d3>
    error(
     f81:	8b 05 49 1d 00 00    	mov    0x1d49(%rip),%eax        # 2cd0 <stdout>
     f87:	ba dc 00 00 00       	mov    $0xdc,%edx
     f8c:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     f91:	89 c7                	mov    %eax,%edi
     f93:	b8 00 00 00 00       	mov    $0x0,%eax
     f98:	e8 09 07 00 00       	callq  16a6 <printf>
     f9d:	8b 05 2d 1d 00 00    	mov    0x1d2d(%rip),%eax        # 2cd0 <stdout>
     fa3:	be 40 27 00 00       	mov    $0x2740,%esi
     fa8:	89 c7                	mov    %eax,%edi
     faa:	b8 00 00 00 00       	mov    $0x0,%eax
     faf:	e8 f2 06 00 00       	callq  16a6 <printf>
     fb4:	8b 05 16 1d 00 00    	mov    0x1d16(%rip),%eax        # 2cd0 <stdout>
     fba:	be 06 20 00 00       	mov    $0x2006,%esi
     fbf:	89 c7                	mov    %eax,%edi
     fc1:	b8 00 00 00 00       	mov    $0x0,%eax
     fc6:	e8 db 06 00 00       	callq  16a6 <printf>
     fcb:	eb fe                	jmp    fcb <duptest+0x2d1>
        "the duped fd didn't read the correct 10 bytes at the 10 byte offset");

  if (close(fd1) != 0)
     fcd:	8b 45 ec             	mov    -0x14(%rbp),%eax
     fd0:	89 c7                	mov    %eax,%edi
     fd2:	e8 9a 0c 00 00       	callq  1c71 <close>
     fd7:	85 c0                	test   %eax,%eax
     fd9:	74 4c                	je     1027 <duptest+0x32d>
    error("closing the original file");
     fdb:	8b 05 ef 1c 00 00    	mov    0x1cef(%rip),%eax        # 2cd0 <stdout>
     fe1:	ba df 00 00 00       	mov    $0xdf,%edx
     fe6:	be cc 1f 00 00       	mov    $0x1fcc,%esi
     feb:	89 c7                	mov    %eax,%edi
     fed:	b8 00 00 00 00       	mov    $0x0,%eax
     ff2:	e8 af 06 00 00       	callq  16a6 <printf>
     ff7:	8b 05 d3 1c 00 00    	mov    0x1cd3(%rip),%eax        # 2cd0 <stdout>
     ffd:	be 84 27 00 00       	mov    $0x2784,%esi
    1002:	89 c7                	mov    %eax,%edi
    1004:	b8 00 00 00 00       	mov    $0x0,%eax
    1009:	e8 98 06 00 00       	callq  16a6 <printf>
    100e:	8b 05 bc 1c 00 00    	mov    0x1cbc(%rip),%eax        # 2cd0 <stdout>
    1014:	be 06 20 00 00       	mov    $0x2006,%esi
    1019:	89 c7                	mov    %eax,%edi
    101b:	b8 00 00 00 00       	mov    $0x0,%eax
    1020:	e8 81 06 00 00       	callq  16a6 <printf>
    1025:	eb fe                	jmp    1025 <duptest+0x32b>

  if (read(fd2, buf, 5) != 5)
    1027:	48 8d 8d 74 ff ff ff 	lea    -0x8c(%rbp),%rcx
    102e:	8b 45 e8             	mov    -0x18(%rbp),%eax
    1031:	ba 05 00 00 00       	mov    $0x5,%edx
    1036:	48 89 ce             	mov    %rcx,%rsi
    1039:	89 c7                	mov    %eax,%edi
    103b:	e8 21 0c 00 00       	callq  1c61 <read>
    1040:	83 f8 05             	cmp    $0x5,%eax
    1043:	74 4c                	je     1091 <duptest+0x397>
    error("wasn't able to read from the duped file after the original file was "
    1045:	8b 05 85 1c 00 00    	mov    0x1c85(%rip),%eax        # 2cd0 <stdout>
    104b:	ba e3 00 00 00       	mov    $0xe3,%edx
    1050:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    1055:	89 c7                	mov    %eax,%edi
    1057:	b8 00 00 00 00       	mov    $0x0,%eax
    105c:	e8 45 06 00 00       	callq  16a6 <printf>
    1061:	8b 05 69 1c 00 00    	mov    0x1c69(%rip),%eax        # 2cd0 <stdout>
    1067:	be a0 27 00 00       	mov    $0x27a0,%esi
    106c:	89 c7                	mov    %eax,%edi
    106e:	b8 00 00 00 00       	mov    $0x0,%eax
    1073:	e8 2e 06 00 00       	callq  16a6 <printf>
    1078:	8b 05 52 1c 00 00    	mov    0x1c52(%rip),%eax        # 2cd0 <stdout>
    107e:	be 06 20 00 00       	mov    $0x2006,%esi
    1083:	89 c7                	mov    %eax,%edi
    1085:	b8 00 00 00 00       	mov    $0x0,%eax
    108a:	e8 17 06 00 00       	callq  16a6 <printf>
    108f:	eb fe                	jmp    108f <duptest+0x395>
          "closed");

  buf[5] = 0;
    1091:	c6 85 79 ff ff ff 00 	movb   $0x0,-0x87(%rbp)
  assert(strcmp(buf, "ccccc") == 0);
    1098:	48 8d 85 74 ff ff ff 	lea    -0x8c(%rbp),%rax
    109f:	be eb 27 00 00       	mov    $0x27eb,%esi
    10a4:	48 89 c7             	mov    %rax,%rdi
    10a7:	e8 37 09 00 00       	callq  19e3 <strcmp>
    10ac:	85 c0                	test   %eax,%eax
    10ae:	74 23                	je     10d3 <duptest+0x3d9>
    10b0:	8b 05 1a 1c 00 00    	mov    0x1c1a(%rip),%eax        # 2cd0 <stdout>
    10b6:	b9 f1 27 00 00       	mov    $0x27f1,%ecx
    10bb:	ba e6 00 00 00       	mov    $0xe6,%edx
    10c0:	be d0 20 00 00       	mov    $0x20d0,%esi
    10c5:	89 c7                	mov    %eax,%edi
    10c7:	b8 00 00 00 00       	mov    $0x0,%eax
    10cc:	e8 d5 05 00 00       	callq  16a6 <printf>
    10d1:	eb fe                	jmp    10d1 <duptest+0x3d7>

  printf(stdout, "dup read offsets ok\n");
    10d3:	8b 05 f7 1b 00 00    	mov    0x1bf7(%rip),%eax        # 2cd0 <stdout>
    10d9:	be 0b 28 00 00       	mov    $0x280b,%esi
    10de:	89 c7                	mov    %eax,%edi
    10e0:	b8 00 00 00 00       	mov    $0x0,%eax
    10e5:	e8 bc 05 00 00       	callq  16a6 <printf>

  if (close(fd2) != 0)
    10ea:	8b 45 e8             	mov    -0x18(%rbp),%eax
    10ed:	89 c7                	mov    %eax,%edi
    10ef:	e8 7d 0b 00 00       	callq  1c71 <close>
    10f4:	85 c0                	test   %eax,%eax
    10f6:	74 4c                	je     1144 <duptest+0x44a>
    error("closing the duped file");
    10f8:	8b 05 d2 1b 00 00    	mov    0x1bd2(%rip),%eax        # 2cd0 <stdout>
    10fe:	ba eb 00 00 00       	mov    $0xeb,%edx
    1103:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    1108:	89 c7                	mov    %eax,%edi
    110a:	b8 00 00 00 00       	mov    $0x0,%eax
    110f:	e8 92 05 00 00       	callq  16a6 <printf>
    1114:	8b 05 b6 1b 00 00    	mov    0x1bb6(%rip),%eax        # 2cd0 <stdout>
    111a:	be 20 28 00 00       	mov    $0x2820,%esi
    111f:	89 c7                	mov    %eax,%edi
    1121:	b8 00 00 00 00       	mov    $0x0,%eax
    1126:	e8 7b 05 00 00       	callq  16a6 <printf>
    112b:	8b 05 9f 1b 00 00    	mov    0x1b9f(%rip),%eax        # 2cd0 <stdout>
    1131:	be 06 20 00 00       	mov    $0x2006,%esi
    1136:	89 c7                	mov    %eax,%edi
    1138:	b8 00 00 00 00       	mov    $0x0,%eax
    113d:	e8 64 05 00 00       	callq  16a6 <printf>
    1142:	eb fe                	jmp    1142 <duptest+0x448>

  // test duping of stdout
  // should be fd1, because that is the first file I opened (and closed) earlier
  if ((stdout_cpy = dup(stdout)) != fd1)
    1144:	8b 05 86 1b 00 00    	mov    0x1b86(%rip),%eax        # 2cd0 <stdout>
    114a:	89 c7                	mov    %eax,%edi
    114c:	e8 70 0b 00 00       	callq  1cc1 <dup>
    1151:	89 45 e4             	mov    %eax,-0x1c(%rbp)
    1154:	8b 45 e4             	mov    -0x1c(%rbp),%eax
    1157:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    115a:	74 4f                	je     11ab <duptest+0x4b1>
    error("returned fd from dup that was not the smallest free fd, was '%d'",
    115c:	8b 05 6e 1b 00 00    	mov    0x1b6e(%rip),%eax        # 2cd0 <stdout>
    1162:	ba f1 00 00 00       	mov    $0xf1,%edx
    1167:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    116c:	89 c7                	mov    %eax,%edi
    116e:	b8 00 00 00 00       	mov    $0x0,%eax
    1173:	e8 2e 05 00 00       	callq  16a6 <printf>
    1178:	8b 05 52 1b 00 00    	mov    0x1b52(%rip),%eax        # 2cd0 <stdout>
    117e:	8b 55 e4             	mov    -0x1c(%rbp),%edx
    1181:	be 38 28 00 00       	mov    $0x2838,%esi
    1186:	89 c7                	mov    %eax,%edi
    1188:	b8 00 00 00 00       	mov    $0x0,%eax
    118d:	e8 14 05 00 00       	callq  16a6 <printf>
    1192:	8b 05 38 1b 00 00    	mov    0x1b38(%rip),%eax        # 2cd0 <stdout>
    1198:	be 06 20 00 00       	mov    $0x2006,%esi
    119d:	89 c7                	mov    %eax,%edi
    119f:	b8 00 00 00 00       	mov    $0x0,%eax
    11a4:	e8 fd 04 00 00       	callq  16a6 <printf>
    11a9:	eb fe                	jmp    11a9 <duptest+0x4af>
          stdout_cpy);

  char *consolestr = "print to console directly from write\n";
    11ab:	48 c7 45 d8 80 28 00 	movq   $0x2880,-0x28(%rbp)
    11b2:	00 
  strcpy(buf, consolestr);
    11b3:	48 8b 55 d8          	mov    -0x28(%rbp),%rdx
    11b7:	48 8d 85 74 ff ff ff 	lea    -0x8c(%rbp),%rax
    11be:	48 89 d6             	mov    %rdx,%rsi
    11c1:	48 89 c7             	mov    %rax,%rdi
    11c4:	e8 db 07 00 00       	callq  19a4 <strcpy>
  
  if (write(stdout_cpy, consolestr, strlen(consolestr)) != strlen(consolestr))
    11c9:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
    11cd:	48 89 c7             	mov    %rax,%rdi
    11d0:	e8 5d 08 00 00       	callq  1a32 <strlen>
    11d5:	89 c2                	mov    %eax,%edx
    11d7:	48 8b 4d d8          	mov    -0x28(%rbp),%rcx
    11db:	8b 45 e4             	mov    -0x1c(%rbp),%eax
    11de:	48 89 ce             	mov    %rcx,%rsi
    11e1:	89 c7                	mov    %eax,%edi
    11e3:	e8 81 0a 00 00       	callq  1c69 <write>
    11e8:	89 c3                	mov    %eax,%ebx
    11ea:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
    11ee:	48 89 c7             	mov    %rax,%rdi
    11f1:	e8 3c 08 00 00       	callq  1a32 <strlen>
    11f6:	39 c3                	cmp    %eax,%ebx
    11f8:	74 4c                	je     1246 <duptest+0x54c>
    error("couldn't write to console from duped fd");
    11fa:	8b 05 d0 1a 00 00    	mov    0x1ad0(%rip),%eax        # 2cd0 <stdout>
    1200:	ba f7 00 00 00       	mov    $0xf7,%edx
    1205:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    120a:	89 c7                	mov    %eax,%edi
    120c:	b8 00 00 00 00       	mov    $0x0,%eax
    1211:	e8 90 04 00 00       	callq  16a6 <printf>
    1216:	8b 05 b4 1a 00 00    	mov    0x1ab4(%rip),%eax        # 2cd0 <stdout>
    121c:	be a8 28 00 00       	mov    $0x28a8,%esi
    1221:	89 c7                	mov    %eax,%edi
    1223:	b8 00 00 00 00       	mov    $0x0,%eax
    1228:	e8 79 04 00 00       	callq  16a6 <printf>
    122d:	8b 05 9d 1a 00 00    	mov    0x1a9d(%rip),%eax        # 2cd0 <stdout>
    1233:	be 06 20 00 00       	mov    $0x2006,%esi
    1238:	89 c7                	mov    %eax,%edi
    123a:	b8 00 00 00 00       	mov    $0x0,%eax
    123f:	e8 62 04 00 00       	callq  16a6 <printf>
    1244:	eb fe                	jmp    1244 <duptest+0x54a>
  
  assert(close(stdout_cpy) == 0);
    1246:	8b 45 e4             	mov    -0x1c(%rbp),%eax
    1249:	89 c7                	mov    %eax,%edi
    124b:	e8 21 0a 00 00       	callq  1c71 <close>
    1250:	85 c0                	test   %eax,%eax
    1252:	74 23                	je     1277 <duptest+0x57d>
    1254:	8b 05 76 1a 00 00    	mov    0x1a76(%rip),%eax        # 2cd0 <stdout>
    125a:	b9 d0 28 00 00       	mov    $0x28d0,%ecx
    125f:	ba f9 00 00 00       	mov    $0xf9,%edx
    1264:	be d0 20 00 00       	mov    $0x20d0,%esi
    1269:	89 c7                	mov    %eax,%edi
    126b:	b8 00 00 00 00       	mov    $0x0,%eax
    1270:	e8 31 04 00 00       	callq  16a6 <printf>
    1275:	eb fe                	jmp    1275 <duptest+0x57b>
   
  printf(stdout, "dup test ok\n");
    1277:	8b 05 53 1a 00 00    	mov    0x1a53(%rip),%eax        # 2cd0 <stdout>
    127d:	be e7 28 00 00       	mov    $0x28e7,%esi
    1282:	89 c7                	mov    %eax,%edi
    1284:	b8 00 00 00 00       	mov    $0x0,%eax
    1289:	e8 18 04 00 00       	callq  16a6 <printf>
}
    128e:	90                   	nop
    128f:	48 81 c4 88 00 00 00 	add    $0x88,%rsp
    1296:	5b                   	pop    %rbx
    1297:	5d                   	pop    %rbp
    1298:	c3                   	retq   

0000000000001299 <nofilestest>:

void nofilestest() {
    1299:	55                   	push   %rbp
    129a:	48 89 e5             	mov    %rsp,%rbp
    129d:	48 83 ec 10          	sub    $0x10,%rsp
  int fd, tmpfd;

  printf(stdout, "nofiles test\n");
    12a1:	8b 05 29 1a 00 00    	mov    0x1a29(%rip),%eax        # 2cd0 <stdout>
    12a7:	be f4 28 00 00       	mov    $0x28f4,%esi
    12ac:	89 c7                	mov    %eax,%edi
    12ae:	b8 00 00 00 00       	mov    $0x0,%eax
    12b3:	e8 ee 03 00 00       	callq  16a6 <printf>
  fd = open("/small.txt", O_RDONLY);
    12b8:	be 00 00 00 00       	mov    $0x0,%esi
    12bd:	bf 08 20 00 00       	mov    $0x2008,%edi
    12c2:	e8 c2 09 00 00       	callq  1c89 <open>
    12c7:	89 45 f8             	mov    %eax,-0x8(%rbp)
  assert(fd != -1);
    12ca:	83 7d f8 ff          	cmpl   $0xffffffff,-0x8(%rbp)
    12ce:	75 23                	jne    12f3 <nofilestest+0x5a>
    12d0:	8b 05 fa 19 00 00    	mov    0x19fa(%rip),%eax        # 2cd0 <stdout>
    12d6:	b9 c7 20 00 00       	mov    $0x20c7,%ecx
    12db:	ba 03 01 00 00       	mov    $0x103,%edx
    12e0:	be d0 20 00 00       	mov    $0x20d0,%esi
    12e5:	89 c7                	mov    %eax,%edi
    12e7:	b8 00 00 00 00       	mov    $0x0,%eax
    12ec:	e8 b5 03 00 00       	callq  16a6 <printf>
    12f1:	eb fe                	jmp    12f1 <nofilestest+0x58>

  for (tmpfd = fd + 1; tmpfd < NOFILE; tmpfd++) {
    12f3:	8b 45 f8             	mov    -0x8(%rbp),%eax
    12f6:	83 c0 01             	add    $0x1,%eax
    12f9:	89 45 fc             	mov    %eax,-0x4(%rbp)
    12fc:	eb 6d                	jmp    136b <nofilestest+0xd2>
    int newfd = open("/small.txt", O_RDONLY);
    12fe:	be 00 00 00 00       	mov    $0x0,%esi
    1303:	bf 08 20 00 00       	mov    $0x2008,%edi
    1308:	e8 7c 09 00 00       	callq  1c89 <open>
    130d:	89 45 f4             	mov    %eax,-0xc(%rbp)
    if (newfd != tmpfd)
    1310:	8b 45 f4             	mov    -0xc(%rbp),%eax
    1313:	3b 45 fc             	cmp    -0x4(%rbp),%eax
    1316:	74 4f                	je     1367 <nofilestest+0xce>
      error("returned fd from open was not the smallest free fd, was '%d'",
    1318:	8b 05 b2 19 00 00    	mov    0x19b2(%rip),%eax        # 2cd0 <stdout>
    131e:	ba 09 01 00 00       	mov    $0x109,%edx
    1323:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    1328:	89 c7                	mov    %eax,%edi
    132a:	b8 00 00 00 00       	mov    $0x0,%eax
    132f:	e8 72 03 00 00       	callq  16a6 <printf>
    1334:	8b 05 96 19 00 00    	mov    0x1996(%rip),%eax        # 2cd0 <stdout>
    133a:	8b 55 f4             	mov    -0xc(%rbp),%edx
    133d:	be 08 29 00 00       	mov    $0x2908,%esi
    1342:	89 c7                	mov    %eax,%edi
    1344:	b8 00 00 00 00       	mov    $0x0,%eax
    1349:	e8 58 03 00 00       	callq  16a6 <printf>
    134e:	8b 05 7c 19 00 00    	mov    0x197c(%rip),%eax        # 2cd0 <stdout>
    1354:	be 06 20 00 00       	mov    $0x2006,%esi
    1359:	89 c7                	mov    %eax,%edi
    135b:	b8 00 00 00 00       	mov    $0x0,%eax
    1360:	e8 41 03 00 00       	callq  16a6 <printf>
    1365:	eb fe                	jmp    1365 <nofilestest+0xcc>

  printf(stdout, "nofiles test\n");
  fd = open("/small.txt", O_RDONLY);
  assert(fd != -1);

  for (tmpfd = fd + 1; tmpfd < NOFILE; tmpfd++) {
    1367:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    136b:	83 7d fc 0f          	cmpl   $0xf,-0x4(%rbp)
    136f:	7e 8d                	jle    12fe <nofilestest+0x65>
      error("returned fd from open was not the smallest free fd, was '%d'",
            newfd);
  }

  // Opening should fail as we have reached the NOFILE limit
  if (open("/small.txt", O_RDONLY) != -1)
    1371:	be 00 00 00 00       	mov    $0x0,%esi
    1376:	bf 08 20 00 00       	mov    $0x2008,%edi
    137b:	e8 09 09 00 00       	callq  1c89 <open>
    1380:	83 f8 ff             	cmp    $0xffffffff,%eax
    1383:	74 4c                	je     13d1 <nofilestest+0x138>
    error("opened more files than allowed");
    1385:	8b 05 45 19 00 00    	mov    0x1945(%rip),%eax        # 2cd0 <stdout>
    138b:	ba 0e 01 00 00       	mov    $0x10e,%edx
    1390:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    1395:	89 c7                	mov    %eax,%edi
    1397:	b8 00 00 00 00       	mov    $0x0,%eax
    139c:	e8 05 03 00 00       	callq  16a6 <printf>
    13a1:	8b 05 29 19 00 00    	mov    0x1929(%rip),%eax        # 2cd0 <stdout>
    13a7:	be 48 29 00 00       	mov    $0x2948,%esi
    13ac:	89 c7                	mov    %eax,%edi
    13ae:	b8 00 00 00 00       	mov    $0x0,%eax
    13b3:	e8 ee 02 00 00       	callq  16a6 <printf>
    13b8:	8b 05 12 19 00 00    	mov    0x1912(%rip),%eax        # 2cd0 <stdout>
    13be:	be 06 20 00 00       	mov    $0x2006,%esi
    13c3:	89 c7                	mov    %eax,%edi
    13c5:	b8 00 00 00 00       	mov    $0x0,%eax
    13ca:	e8 d7 02 00 00       	callq  16a6 <printf>
    13cf:	eb fe                	jmp    13cf <nofilestest+0x136>

  assert(close(NOFILE - 1) == 0);
    13d1:	bf 0f 00 00 00       	mov    $0xf,%edi
    13d6:	e8 96 08 00 00       	callq  1c71 <close>
    13db:	85 c0                	test   %eax,%eax
    13dd:	74 23                	je     1402 <nofilestest+0x169>
    13df:	8b 05 eb 18 00 00    	mov    0x18eb(%rip),%eax        # 2cd0 <stdout>
    13e5:	b9 67 29 00 00       	mov    $0x2967,%ecx
    13ea:	ba 10 01 00 00       	mov    $0x110,%edx
    13ef:	be d0 20 00 00       	mov    $0x20d0,%esi
    13f4:	89 c7                	mov    %eax,%edi
    13f6:	b8 00 00 00 00       	mov    $0x0,%eax
    13fb:	e8 a6 02 00 00       	callq  16a6 <printf>
    1400:	eb fe                	jmp    1400 <nofilestest+0x167>

  // Opening should work once there is a fd available
  int fd2 = open("/small.txt", O_RDONLY);
    1402:	be 00 00 00 00       	mov    $0x0,%esi
    1407:	bf 08 20 00 00       	mov    $0x2008,%edi
    140c:	e8 78 08 00 00       	callq  1c89 <open>
    1411:	89 45 f0             	mov    %eax,-0x10(%rbp)
  if (fd2 == -1) error("unable to open file after an fd is available");
    1414:	83 7d f0 ff          	cmpl   $0xffffffff,-0x10(%rbp)
    1418:	75 4c                	jne    1466 <nofilestest+0x1cd>
    141a:	8b 05 b0 18 00 00    	mov    0x18b0(%rip),%eax        # 2cd0 <stdout>
    1420:	ba 14 01 00 00       	mov    $0x114,%edx
    1425:	be cc 1f 00 00       	mov    $0x1fcc,%esi
    142a:	89 c7                	mov    %eax,%edi
    142c:	b8 00 00 00 00       	mov    $0x0,%eax
    1431:	e8 70 02 00 00       	callq  16a6 <printf>
    1436:	8b 05 94 18 00 00    	mov    0x1894(%rip),%eax        # 2cd0 <stdout>
    143c:	be 80 29 00 00       	mov    $0x2980,%esi
    1441:	89 c7                	mov    %eax,%edi
    1443:	b8 00 00 00 00       	mov    $0x0,%eax
    1448:	e8 59 02 00 00       	callq  16a6 <printf>
    144d:	8b 05 7d 18 00 00    	mov    0x187d(%rip),%eax        # 2cd0 <stdout>
    1453:	be 06 20 00 00       	mov    $0x2006,%esi
    1458:	89 c7                	mov    %eax,%edi
    145a:	b8 00 00 00 00       	mov    $0x0,%eax
    145f:	e8 42 02 00 00       	callq  16a6 <printf>
    1464:	eb fe                	jmp    1464 <nofilestest+0x1cb>

  assert(fd2 == NOFILE - 1);
    1466:	83 7d f0 0f          	cmpl   $0xf,-0x10(%rbp)
    146a:	74 23                	je     148f <nofilestest+0x1f6>
    146c:	8b 05 5e 18 00 00    	mov    0x185e(%rip),%eax        # 2cd0 <stdout>
    1472:	b9 ad 29 00 00       	mov    $0x29ad,%ecx
    1477:	ba 16 01 00 00       	mov    $0x116,%edx
    147c:	be d0 20 00 00       	mov    $0x20d0,%esi
    1481:	89 c7                	mov    %eax,%edi
    1483:	b8 00 00 00 00       	mov    $0x0,%eax
    1488:	e8 19 02 00 00       	callq  16a6 <printf>
    148d:	eb fe                	jmp    148d <nofilestest+0x1f4>

  for (tmpfd = fd; tmpfd < NOFILE; tmpfd++) {
    148f:	8b 45 f8             	mov    -0x8(%rbp),%eax
    1492:	89 45 fc             	mov    %eax,-0x4(%rbp)
    1495:	eb 35                	jmp    14cc <nofilestest+0x233>
    assert(close(tmpfd) == 0);
    1497:	8b 45 fc             	mov    -0x4(%rbp),%eax
    149a:	89 c7                	mov    %eax,%edi
    149c:	e8 d0 07 00 00       	callq  1c71 <close>
    14a1:	85 c0                	test   %eax,%eax
    14a3:	74 23                	je     14c8 <nofilestest+0x22f>
    14a5:	8b 05 25 18 00 00    	mov    0x1825(%rip),%eax        # 2cd0 <stdout>
    14ab:	b9 bf 29 00 00       	mov    $0x29bf,%ecx
    14b0:	ba 19 01 00 00       	mov    $0x119,%edx
    14b5:	be d0 20 00 00       	mov    $0x20d0,%esi
    14ba:	89 c7                	mov    %eax,%edi
    14bc:	b8 00 00 00 00       	mov    $0x0,%eax
    14c1:	e8 e0 01 00 00       	callq  16a6 <printf>
    14c6:	eb fe                	jmp    14c6 <nofilestest+0x22d>
  int fd2 = open("/small.txt", O_RDONLY);
  if (fd2 == -1) error("unable to open file after an fd is available");

  assert(fd2 == NOFILE - 1);

  for (tmpfd = fd; tmpfd < NOFILE; tmpfd++) {
    14c8:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    14cc:	83 7d fc 0f          	cmpl   $0xf,-0x4(%rbp)
    14d0:	7e c5                	jle    1497 <nofilestest+0x1fe>
    assert(close(tmpfd) == 0);
  }

  printf(stdout, "nofiles test ok\n");
    14d2:	8b 05 f8 17 00 00    	mov    0x17f8(%rip),%eax        # 2cd0 <stdout>
    14d8:	be d1 29 00 00       	mov    $0x29d1,%esi
    14dd:	89 c7                	mov    %eax,%edi
    14df:	b8 00 00 00 00       	mov    $0x0,%eax
    14e4:	e8 bd 01 00 00       	callq  16a6 <printf>
}
    14e9:	90                   	nop
    14ea:	c9                   	leaveq 
    14eb:	c3                   	retq   

00000000000014ec <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
    14ec:	55                   	push   %rbp
    14ed:	48 89 e5             	mov    %rsp,%rbp
    14f0:	48 83 ec 10          	sub    $0x10,%rsp
    14f4:	89 7d fc             	mov    %edi,-0x4(%rbp)
    14f7:	89 f0                	mov    %esi,%eax
    14f9:	88 45 f8             	mov    %al,-0x8(%rbp)
    14fc:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
    1500:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1503:	ba 01 00 00 00       	mov    $0x1,%edx
    1508:	48 89 ce             	mov    %rcx,%rsi
    150b:	89 c7                	mov    %eax,%edi
    150d:	e8 57 07 00 00       	callq  1c69 <write>
    1512:	90                   	nop
    1513:	c9                   	leaveq 
    1514:	c3                   	retq   

0000000000001515 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
    1515:	55                   	push   %rbp
    1516:	48 89 e5             	mov    %rsp,%rbp
    1519:	48 83 ec 40          	sub    $0x40,%rsp
    151d:	89 7d cc             	mov    %edi,-0x34(%rbp)
    1520:	89 75 c8             	mov    %esi,-0x38(%rbp)
    1523:	89 55 c4             	mov    %edx,-0x3c(%rbp)
    1526:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
    1529:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    152d:	74 1f                	je     154e <printint64+0x39>
    152f:	8b 45 c8             	mov    -0x38(%rbp),%eax
    1532:	c1 e8 1f             	shr    $0x1f,%eax
    1535:	0f b6 c0             	movzbl %al,%eax
    1538:	89 45 c0             	mov    %eax,-0x40(%rbp)
    153b:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    153f:	74 0d                	je     154e <printint64+0x39>
    x = -xx;
    1541:	8b 45 c8             	mov    -0x38(%rbp),%eax
    1544:	f7 d8                	neg    %eax
    1546:	48 98                	cltq   
    1548:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    154c:	eb 09                	jmp    1557 <printint64+0x42>
  else
    x = xx;
    154e:	8b 45 c8             	mov    -0x38(%rbp),%eax
    1551:	48 98                	cltq   
    1553:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
    1557:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
    155e:	8b 4d fc             	mov    -0x4(%rbp),%ecx
    1561:	8d 41 01             	lea    0x1(%rcx),%eax
    1564:	89 45 fc             	mov    %eax,-0x4(%rbp)
    1567:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    156a:	48 63 f0             	movslq %eax,%rsi
    156d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1571:	ba 00 00 00 00       	mov    $0x0,%edx
    1576:	48 f7 f6             	div    %rsi
    1579:	48 89 d0             	mov    %rdx,%rax
    157c:	0f b6 90 e0 2c 00 00 	movzbl 0x2ce0(%rax),%edx
    1583:	48 63 c1             	movslq %ecx,%rax
    1586:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
    158a:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    158d:	48 63 f8             	movslq %eax,%rdi
    1590:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1594:	ba 00 00 00 00       	mov    $0x0,%edx
    1599:	48 f7 f7             	div    %rdi
    159c:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    15a0:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    15a5:	75 b7                	jne    155e <printint64+0x49>

  if (sgn)
    15a7:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    15ab:	74 2b                	je     15d8 <printint64+0xc3>
    buf[i++] = '-';
    15ad:	8b 45 fc             	mov    -0x4(%rbp),%eax
    15b0:	8d 50 01             	lea    0x1(%rax),%edx
    15b3:	89 55 fc             	mov    %edx,-0x4(%rbp)
    15b6:	48 98                	cltq   
    15b8:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
    15bd:	eb 19                	jmp    15d8 <printint64+0xc3>
    putc(fd, buf[i]);
    15bf:	8b 45 fc             	mov    -0x4(%rbp),%eax
    15c2:	48 98                	cltq   
    15c4:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
    15c9:	0f be d0             	movsbl %al,%edx
    15cc:	8b 45 cc             	mov    -0x34(%rbp),%eax
    15cf:	89 d6                	mov    %edx,%esi
    15d1:	89 c7                	mov    %eax,%edi
    15d3:	e8 14 ff ff ff       	callq  14ec <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
    15d8:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
    15dc:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    15e0:	79 dd                	jns    15bf <printint64+0xaa>
    putc(fd, buf[i]);
}
    15e2:	90                   	nop
    15e3:	c9                   	leaveq 
    15e4:	c3                   	retq   

00000000000015e5 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
    15e5:	55                   	push   %rbp
    15e6:	48 89 e5             	mov    %rsp,%rbp
    15e9:	48 83 ec 30          	sub    $0x30,%rsp
    15ed:	89 7d dc             	mov    %edi,-0x24(%rbp)
    15f0:	89 75 d8             	mov    %esi,-0x28(%rbp)
    15f3:	89 55 d4             	mov    %edx,-0x2c(%rbp)
    15f6:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
    15f9:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
    1600:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
    1604:	74 17                	je     161d <printint+0x38>
    1606:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
    160a:	79 11                	jns    161d <printint+0x38>
    neg = 1;
    160c:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
    1613:	8b 45 d8             	mov    -0x28(%rbp),%eax
    1616:	f7 d8                	neg    %eax
    1618:	89 45 f4             	mov    %eax,-0xc(%rbp)
    161b:	eb 06                	jmp    1623 <printint+0x3e>
  } else {
    x = xx;
    161d:	8b 45 d8             	mov    -0x28(%rbp),%eax
    1620:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
    1623:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
    162a:	8b 4d fc             	mov    -0x4(%rbp),%ecx
    162d:	8d 41 01             	lea    0x1(%rcx),%eax
    1630:	89 45 fc             	mov    %eax,-0x4(%rbp)
    1633:	8b 75 d4             	mov    -0x2c(%rbp),%esi
    1636:	8b 45 f4             	mov    -0xc(%rbp),%eax
    1639:	ba 00 00 00 00       	mov    $0x0,%edx
    163e:	f7 f6                	div    %esi
    1640:	89 d0                	mov    %edx,%eax
    1642:	89 c0                	mov    %eax,%eax
    1644:	0f b6 90 00 2d 00 00 	movzbl 0x2d00(%rax),%edx
    164b:	48 63 c1             	movslq %ecx,%rax
    164e:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
    1652:	8b 7d d4             	mov    -0x2c(%rbp),%edi
    1655:	8b 45 f4             	mov    -0xc(%rbp),%eax
    1658:	ba 00 00 00 00       	mov    $0x0,%edx
    165d:	f7 f7                	div    %edi
    165f:	89 45 f4             	mov    %eax,-0xc(%rbp)
    1662:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
    1666:	75 c2                	jne    162a <printint+0x45>
  if (neg)
    1668:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
    166c:	74 2b                	je     1699 <printint+0xb4>
    buf[i++] = '-';
    166e:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1671:	8d 50 01             	lea    0x1(%rax),%edx
    1674:	89 55 fc             	mov    %edx,-0x4(%rbp)
    1677:	48 98                	cltq   
    1679:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
    167e:	eb 19                	jmp    1699 <printint+0xb4>
    putc(fd, buf[i]);
    1680:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1683:	48 98                	cltq   
    1685:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
    168a:	0f be d0             	movsbl %al,%edx
    168d:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1690:	89 d6                	mov    %edx,%esi
    1692:	89 c7                	mov    %eax,%edi
    1694:	e8 53 fe ff ff       	callq  14ec <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
    1699:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
    169d:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    16a1:	79 dd                	jns    1680 <printint+0x9b>
    putc(fd, buf[i]);
}
    16a3:	90                   	nop
    16a4:	c9                   	leaveq 
    16a5:	c3                   	retq   

00000000000016a6 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
    16a6:	55                   	push   %rbp
    16a7:	48 89 e5             	mov    %rsp,%rbp
    16aa:	48 83 ec 70          	sub    $0x70,%rsp
    16ae:	89 7d 9c             	mov    %edi,-0x64(%rbp)
    16b1:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
    16b5:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
    16b9:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
    16bd:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
    16c1:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
    16c5:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
    16cc:	48 8d 45 10          	lea    0x10(%rbp),%rax
    16d0:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
    16d4:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
    16d8:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
    16dc:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
    16e3:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
    16ea:	e9 68 02 00 00       	jmpq   1957 <printf+0x2b1>
    c = fmt[i] & 0xff;
    16ef:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    16f2:	48 63 d0             	movslq %eax,%rdx
    16f5:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    16f9:	48 01 d0             	add    %rdx,%rax
    16fc:	0f b6 00             	movzbl (%rax),%eax
    16ff:	0f be c0             	movsbl %al,%eax
    1702:	25 ff 00 00 00       	and    $0xff,%eax
    1707:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
    170a:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    170e:	75 30                	jne    1740 <printf+0x9a>
      if (c == '%') {
    1710:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    1714:	75 13                	jne    1729 <printf+0x83>
        state = '%';
    1716:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
    171d:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
    1724:	e9 2a 02 00 00       	jmpq   1953 <printf+0x2ad>
      } else {
        putc(fd, c);
    1729:	8b 45 b8             	mov    -0x48(%rbp),%eax
    172c:	0f be d0             	movsbl %al,%edx
    172f:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1732:	89 d6                	mov    %edx,%esi
    1734:	89 c7                	mov    %eax,%edi
    1736:	e8 b1 fd ff ff       	callq  14ec <putc>
    173b:	e9 13 02 00 00       	jmpq   1953 <printf+0x2ad>
      }
    } else if (state == '%') {
    1740:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
    1744:	0f 85 09 02 00 00    	jne    1953 <printf+0x2ad>
      if (c == 'l') {
    174a:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
    174e:	75 0c                	jne    175c <printf+0xb6>
        lflag = 1;
    1750:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
    1757:	e9 f7 01 00 00       	jmpq   1953 <printf+0x2ad>
      } else if (c == 'd') {
    175c:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
    1760:	0f 85 95 00 00 00    	jne    17fb <printf+0x155>
        if (lflag == 1)
    1766:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    176a:	75 49                	jne    17b5 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
    176c:	8b 45 a0             	mov    -0x60(%rbp),%eax
    176f:	83 f8 30             	cmp    $0x30,%eax
    1772:	73 17                	jae    178b <printf+0xe5>
    1774:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1778:	8b 55 a0             	mov    -0x60(%rbp),%edx
    177b:	89 d2                	mov    %edx,%edx
    177d:	48 01 d0             	add    %rdx,%rax
    1780:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1783:	83 c2 08             	add    $0x8,%edx
    1786:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1789:	eb 0c                	jmp    1797 <printf+0xf1>
    178b:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    178f:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1793:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1797:	48 8b 00             	mov    (%rax),%rax
    179a:	89 c6                	mov    %eax,%esi
    179c:	8b 45 9c             	mov    -0x64(%rbp),%eax
    179f:	b9 01 00 00 00       	mov    $0x1,%ecx
    17a4:	ba 0a 00 00 00       	mov    $0xa,%edx
    17a9:	89 c7                	mov    %eax,%edi
    17ab:	e8 65 fd ff ff       	callq  1515 <printint64>
    17b0:	e9 97 01 00 00       	jmpq   194c <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
    17b5:	8b 45 a0             	mov    -0x60(%rbp),%eax
    17b8:	83 f8 30             	cmp    $0x30,%eax
    17bb:	73 17                	jae    17d4 <printf+0x12e>
    17bd:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    17c1:	8b 55 a0             	mov    -0x60(%rbp),%edx
    17c4:	89 d2                	mov    %edx,%edx
    17c6:	48 01 d0             	add    %rdx,%rax
    17c9:	8b 55 a0             	mov    -0x60(%rbp),%edx
    17cc:	83 c2 08             	add    $0x8,%edx
    17cf:	89 55 a0             	mov    %edx,-0x60(%rbp)
    17d2:	eb 0c                	jmp    17e0 <printf+0x13a>
    17d4:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    17d8:	48 8d 50 08          	lea    0x8(%rax),%rdx
    17dc:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    17e0:	8b 30                	mov    (%rax),%esi
    17e2:	8b 45 9c             	mov    -0x64(%rbp),%eax
    17e5:	b9 01 00 00 00       	mov    $0x1,%ecx
    17ea:	ba 0a 00 00 00       	mov    $0xa,%edx
    17ef:	89 c7                	mov    %eax,%edi
    17f1:	e8 ef fd ff ff       	callq  15e5 <printint>
    17f6:	e9 51 01 00 00       	jmpq   194c <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
    17fb:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
    17ff:	74 0a                	je     180b <printf+0x165>
    1801:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
    1805:	0f 85 95 00 00 00    	jne    18a0 <printf+0x1fa>
        if (lflag == 1)
    180b:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    180f:	75 49                	jne    185a <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
    1811:	8b 45 a0             	mov    -0x60(%rbp),%eax
    1814:	83 f8 30             	cmp    $0x30,%eax
    1817:	73 17                	jae    1830 <printf+0x18a>
    1819:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    181d:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1820:	89 d2                	mov    %edx,%edx
    1822:	48 01 d0             	add    %rdx,%rax
    1825:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1828:	83 c2 08             	add    $0x8,%edx
    182b:	89 55 a0             	mov    %edx,-0x60(%rbp)
    182e:	eb 0c                	jmp    183c <printf+0x196>
    1830:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1834:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1838:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    183c:	48 8b 00             	mov    (%rax),%rax
    183f:	89 c6                	mov    %eax,%esi
    1841:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1844:	b9 00 00 00 00       	mov    $0x0,%ecx
    1849:	ba 10 00 00 00       	mov    $0x10,%edx
    184e:	89 c7                	mov    %eax,%edi
    1850:	e8 c0 fc ff ff       	callq  1515 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    1855:	e9 f2 00 00 00       	jmpq   194c <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
    185a:	8b 45 a0             	mov    -0x60(%rbp),%eax
    185d:	83 f8 30             	cmp    $0x30,%eax
    1860:	73 17                	jae    1879 <printf+0x1d3>
    1862:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1866:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1869:	89 d2                	mov    %edx,%edx
    186b:	48 01 d0             	add    %rdx,%rax
    186e:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1871:	83 c2 08             	add    $0x8,%edx
    1874:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1877:	eb 0c                	jmp    1885 <printf+0x1df>
    1879:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    187d:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1881:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1885:	8b 30                	mov    (%rax),%esi
    1887:	8b 45 9c             	mov    -0x64(%rbp),%eax
    188a:	b9 00 00 00 00       	mov    $0x0,%ecx
    188f:	ba 10 00 00 00       	mov    $0x10,%edx
    1894:	89 c7                	mov    %eax,%edi
    1896:	e8 4a fd ff ff       	callq  15e5 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    189b:	e9 ac 00 00 00       	jmpq   194c <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
    18a0:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
    18a4:	75 6b                	jne    1911 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
    18a6:	8b 45 a0             	mov    -0x60(%rbp),%eax
    18a9:	83 f8 30             	cmp    $0x30,%eax
    18ac:	73 17                	jae    18c5 <printf+0x21f>
    18ae:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    18b2:	8b 55 a0             	mov    -0x60(%rbp),%edx
    18b5:	89 d2                	mov    %edx,%edx
    18b7:	48 01 d0             	add    %rdx,%rax
    18ba:	8b 55 a0             	mov    -0x60(%rbp),%edx
    18bd:	83 c2 08             	add    $0x8,%edx
    18c0:	89 55 a0             	mov    %edx,-0x60(%rbp)
    18c3:	eb 0c                	jmp    18d1 <printf+0x22b>
    18c5:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    18c9:	48 8d 50 08          	lea    0x8(%rax),%rdx
    18cd:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    18d1:	48 8b 00             	mov    (%rax),%rax
    18d4:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
    18d8:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
    18dd:	75 25                	jne    1904 <printf+0x25e>
          s = "(null)";
    18df:	48 c7 45 c8 e2 29 00 	movq   $0x29e2,-0x38(%rbp)
    18e6:	00 
        for (; *s; s++)
    18e7:	eb 1b                	jmp    1904 <printf+0x25e>
          putc(fd, *s);
    18e9:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    18ed:	0f b6 00             	movzbl (%rax),%eax
    18f0:	0f be d0             	movsbl %al,%edx
    18f3:	8b 45 9c             	mov    -0x64(%rbp),%eax
    18f6:	89 d6                	mov    %edx,%esi
    18f8:	89 c7                	mov    %eax,%edi
    18fa:	e8 ed fb ff ff       	callq  14ec <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
    18ff:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
    1904:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    1908:	0f b6 00             	movzbl (%rax),%eax
    190b:	84 c0                	test   %al,%al
    190d:	75 da                	jne    18e9 <printf+0x243>
    190f:	eb 3b                	jmp    194c <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
    1911:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    1915:	75 14                	jne    192b <printf+0x285>
        putc(fd, c);
    1917:	8b 45 b8             	mov    -0x48(%rbp),%eax
    191a:	0f be d0             	movsbl %al,%edx
    191d:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1920:	89 d6                	mov    %edx,%esi
    1922:	89 c7                	mov    %eax,%edi
    1924:	e8 c3 fb ff ff       	callq  14ec <putc>
    1929:	eb 21                	jmp    194c <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
    192b:	8b 45 9c             	mov    -0x64(%rbp),%eax
    192e:	be 25 00 00 00       	mov    $0x25,%esi
    1933:	89 c7                	mov    %eax,%edi
    1935:	e8 b2 fb ff ff       	callq  14ec <putc>
        putc(fd, c);
    193a:	8b 45 b8             	mov    -0x48(%rbp),%eax
    193d:	0f be d0             	movsbl %al,%edx
    1940:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1943:	89 d6                	mov    %edx,%esi
    1945:	89 c7                	mov    %eax,%edi
    1947:	e8 a0 fb ff ff       	callq  14ec <putc>
      }
      state = 0;
    194c:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
    1953:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
    1957:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    195a:	48 63 d0             	movslq %eax,%rdx
    195d:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    1961:	48 01 d0             	add    %rdx,%rax
    1964:	0f b6 00             	movzbl (%rax),%eax
    1967:	84 c0                	test   %al,%al
    1969:	0f 85 80 fd ff ff    	jne    16ef <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
    196f:	90                   	nop
    1970:	c9                   	leaveq 
    1971:	c3                   	retq   

0000000000001972 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
    1972:	55                   	push   %rbp
    1973:	48 89 e5             	mov    %rsp,%rbp
    1976:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    197a:	89 75 f4             	mov    %esi,-0xc(%rbp)
    197d:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
    1980:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
    1984:	8b 55 f0             	mov    -0x10(%rbp),%edx
    1987:	8b 45 f4             	mov    -0xc(%rbp),%eax
    198a:	48 89 ce             	mov    %rcx,%rsi
    198d:	48 89 f7             	mov    %rsi,%rdi
    1990:	89 d1                	mov    %edx,%ecx
    1992:	fc                   	cld    
    1993:	f3 aa                	rep stos %al,%es:(%rdi)
    1995:	89 ca                	mov    %ecx,%edx
    1997:	48 89 fe             	mov    %rdi,%rsi
    199a:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
    199e:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
    19a1:	90                   	nop
    19a2:	5d                   	pop    %rbp
    19a3:	c3                   	retq   

00000000000019a4 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
    19a4:	55                   	push   %rbp
    19a5:	48 89 e5             	mov    %rsp,%rbp
    19a8:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    19ac:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
    19b0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    19b4:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
    19b8:	90                   	nop
    19b9:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    19bd:	48 8d 50 01          	lea    0x1(%rax),%rdx
    19c1:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    19c5:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    19c9:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    19cd:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
    19d1:	0f b6 12             	movzbl (%rdx),%edx
    19d4:	88 10                	mov    %dl,(%rax)
    19d6:	0f b6 00             	movzbl (%rax),%eax
    19d9:	84 c0                	test   %al,%al
    19db:	75 dc                	jne    19b9 <strcpy+0x15>
    ;
  return os;
    19dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    19e1:	5d                   	pop    %rbp
    19e2:	c3                   	retq   

00000000000019e3 <strcmp>:

int strcmp(const char *p, const char *q) {
    19e3:	55                   	push   %rbp
    19e4:	48 89 e5             	mov    %rsp,%rbp
    19e7:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    19eb:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
    19ef:	eb 0a                	jmp    19fb <strcmp+0x18>
    p++, q++;
    19f1:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    19f6:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
    19fb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    19ff:	0f b6 00             	movzbl (%rax),%eax
    1a02:	84 c0                	test   %al,%al
    1a04:	74 12                	je     1a18 <strcmp+0x35>
    1a06:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a0a:	0f b6 10             	movzbl (%rax),%edx
    1a0d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1a11:	0f b6 00             	movzbl (%rax),%eax
    1a14:	38 c2                	cmp    %al,%dl
    1a16:	74 d9                	je     19f1 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
    1a18:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a1c:	0f b6 00             	movzbl (%rax),%eax
    1a1f:	0f b6 d0             	movzbl %al,%edx
    1a22:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1a26:	0f b6 00             	movzbl (%rax),%eax
    1a29:	0f b6 c0             	movzbl %al,%eax
    1a2c:	29 c2                	sub    %eax,%edx
    1a2e:	89 d0                	mov    %edx,%eax
}
    1a30:	5d                   	pop    %rbp
    1a31:	c3                   	retq   

0000000000001a32 <strlen>:

uint strlen(char *s) {
    1a32:	55                   	push   %rbp
    1a33:	48 89 e5             	mov    %rsp,%rbp
    1a36:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
    1a3a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1a41:	eb 04                	jmp    1a47 <strlen+0x15>
    1a43:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    1a47:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1a4a:	48 63 d0             	movslq %eax,%rdx
    1a4d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1a51:	48 01 d0             	add    %rdx,%rax
    1a54:	0f b6 00             	movzbl (%rax),%eax
    1a57:	84 c0                	test   %al,%al
    1a59:	75 e8                	jne    1a43 <strlen+0x11>
    ;
  return n;
    1a5b:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    1a5e:	5d                   	pop    %rbp
    1a5f:	c3                   	retq   

0000000000001a60 <memset>:

void *memset(void *dst, int c, uint n) {
    1a60:	55                   	push   %rbp
    1a61:	48 89 e5             	mov    %rsp,%rbp
    1a64:	48 83 ec 10          	sub    $0x10,%rsp
    1a68:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    1a6c:	89 75 f4             	mov    %esi,-0xc(%rbp)
    1a6f:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
    1a72:	8b 55 f0             	mov    -0x10(%rbp),%edx
    1a75:	8b 4d f4             	mov    -0xc(%rbp),%ecx
    1a78:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a7c:	89 ce                	mov    %ecx,%esi
    1a7e:	48 89 c7             	mov    %rax,%rdi
    1a81:	e8 ec fe ff ff       	callq  1972 <stosb>
  return dst;
    1a86:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    1a8a:	c9                   	leaveq 
    1a8b:	c3                   	retq   

0000000000001a8c <strchr>:

char *strchr(const char *s, char c) {
    1a8c:	55                   	push   %rbp
    1a8d:	48 89 e5             	mov    %rsp,%rbp
    1a90:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    1a94:	89 f0                	mov    %esi,%eax
    1a96:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
    1a99:	eb 17                	jmp    1ab2 <strchr+0x26>
    if (*s == c)
    1a9b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a9f:	0f b6 00             	movzbl (%rax),%eax
    1aa2:	3a 45 f4             	cmp    -0xc(%rbp),%al
    1aa5:	75 06                	jne    1aad <strchr+0x21>
      return (char *)s;
    1aa7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1aab:	eb 15                	jmp    1ac2 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
    1aad:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    1ab2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1ab6:	0f b6 00             	movzbl (%rax),%eax
    1ab9:	84 c0                	test   %al,%al
    1abb:	75 de                	jne    1a9b <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
    1abd:	b8 00 00 00 00       	mov    $0x0,%eax
}
    1ac2:	5d                   	pop    %rbp
    1ac3:	c3                   	retq   

0000000000001ac4 <gets>:

char *gets(char *buf, int max) {
    1ac4:	55                   	push   %rbp
    1ac5:	48 89 e5             	mov    %rsp,%rbp
    1ac8:	48 83 ec 20          	sub    $0x20,%rsp
    1acc:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1ad0:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    1ad3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1ada:	eb 48                	jmp    1b24 <gets+0x60>
    cc = read(0, &c, 1);
    1adc:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
    1ae0:	ba 01 00 00 00       	mov    $0x1,%edx
    1ae5:	48 89 c6             	mov    %rax,%rsi
    1ae8:	bf 00 00 00 00       	mov    $0x0,%edi
    1aed:	e8 6f 01 00 00       	callq  1c61 <read>
    1af2:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
    1af5:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
    1af9:	7e 36                	jle    1b31 <gets+0x6d>
      break;
    buf[i++] = c;
    1afb:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1afe:	8d 50 01             	lea    0x1(%rax),%edx
    1b01:	89 55 fc             	mov    %edx,-0x4(%rbp)
    1b04:	48 63 d0             	movslq %eax,%rdx
    1b07:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1b0b:	48 01 c2             	add    %rax,%rdx
    1b0e:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1b12:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
    1b14:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1b18:	3c 0a                	cmp    $0xa,%al
    1b1a:	74 16                	je     1b32 <gets+0x6e>
    1b1c:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1b20:	3c 0d                	cmp    $0xd,%al
    1b22:	74 0e                	je     1b32 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    1b24:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1b27:	83 c0 01             	add    $0x1,%eax
    1b2a:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
    1b2d:	7c ad                	jl     1adc <gets+0x18>
    1b2f:	eb 01                	jmp    1b32 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
    1b31:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
    1b32:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1b35:	48 63 d0             	movslq %eax,%rdx
    1b38:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1b3c:	48 01 d0             	add    %rdx,%rax
    1b3f:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
    1b42:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
    1b46:	c9                   	leaveq 
    1b47:	c3                   	retq   

0000000000001b48 <stat>:

int stat(char *n, struct stat *st) {
    1b48:	55                   	push   %rbp
    1b49:	48 89 e5             	mov    %rsp,%rbp
    1b4c:	48 83 ec 20          	sub    $0x20,%rsp
    1b50:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1b54:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    1b58:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1b5c:	be 00 00 00 00       	mov    $0x0,%esi
    1b61:	48 89 c7             	mov    %rax,%rdi
    1b64:	e8 20 01 00 00       	callq  1c89 <open>
    1b69:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
    1b6c:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    1b70:	79 07                	jns    1b79 <stat+0x31>
    return -1;
    1b72:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    1b77:	eb 21                	jmp    1b9a <stat+0x52>
  r = fstat(fd, st);
    1b79:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    1b7d:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1b80:	48 89 d6             	mov    %rdx,%rsi
    1b83:	89 c7                	mov    %eax,%edi
    1b85:	e8 17 01 00 00       	callq  1ca1 <fstat>
    1b8a:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
    1b8d:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1b90:	89 c7                	mov    %eax,%edi
    1b92:	e8 da 00 00 00       	callq  1c71 <close>
  return r;
    1b97:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
    1b9a:	c9                   	leaveq 
    1b9b:	c3                   	retq   

0000000000001b9c <atoi>:

int atoi(const char *s) {
    1b9c:	55                   	push   %rbp
    1b9d:	48 89 e5             	mov    %rsp,%rbp
    1ba0:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
    1ba4:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
    1bab:	eb 28                	jmp    1bd5 <atoi+0x39>
    n = n * 10 + *s++ - '0';
    1bad:	8b 55 fc             	mov    -0x4(%rbp),%edx
    1bb0:	89 d0                	mov    %edx,%eax
    1bb2:	c1 e0 02             	shl    $0x2,%eax
    1bb5:	01 d0                	add    %edx,%eax
    1bb7:	01 c0                	add    %eax,%eax
    1bb9:	89 c1                	mov    %eax,%ecx
    1bbb:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1bbf:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1bc3:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    1bc7:	0f b6 00             	movzbl (%rax),%eax
    1bca:	0f be c0             	movsbl %al,%eax
    1bcd:	01 c8                	add    %ecx,%eax
    1bcf:	83 e8 30             	sub    $0x30,%eax
    1bd2:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
    1bd5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1bd9:	0f b6 00             	movzbl (%rax),%eax
    1bdc:	3c 2f                	cmp    $0x2f,%al
    1bde:	7e 0b                	jle    1beb <atoi+0x4f>
    1be0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1be4:	0f b6 00             	movzbl (%rax),%eax
    1be7:	3c 39                	cmp    $0x39,%al
    1be9:	7e c2                	jle    1bad <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
    1beb:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    1bee:	5d                   	pop    %rbp
    1bef:	c3                   	retq   

0000000000001bf0 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
    1bf0:	55                   	push   %rbp
    1bf1:	48 89 e5             	mov    %rsp,%rbp
    1bf4:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1bf8:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
    1bfc:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
    1bff:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1c03:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
    1c07:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
    1c0b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
    1c0f:	eb 1d                	jmp    1c2e <memmove+0x3e>
    *dst++ = *src++;
    1c11:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c15:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1c19:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
    1c1d:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1c21:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    1c25:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
    1c29:	0f b6 12             	movzbl (%rdx),%edx
    1c2c:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
    1c2e:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1c31:	8d 50 ff             	lea    -0x1(%rax),%edx
    1c34:	89 55 dc             	mov    %edx,-0x24(%rbp)
    1c37:	85 c0                	test   %eax,%eax
    1c39:	7f d6                	jg     1c11 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
    1c3b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1c3f:	5d                   	pop    %rbp
    1c40:	c3                   	retq   

0000000000001c41 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
    1c41:	b8 01 00 00 00       	mov    $0x1,%eax
    1c46:	cd 40                	int    $0x40
    1c48:	c3                   	retq   

0000000000001c49 <exit>:
SYSCALL(exit)
    1c49:	b8 02 00 00 00       	mov    $0x2,%eax
    1c4e:	cd 40                	int    $0x40
    1c50:	c3                   	retq   

0000000000001c51 <wait>:
SYSCALL(wait)
    1c51:	b8 03 00 00 00       	mov    $0x3,%eax
    1c56:	cd 40                	int    $0x40
    1c58:	c3                   	retq   

0000000000001c59 <pipe>:
SYSCALL(pipe)
    1c59:	b8 04 00 00 00       	mov    $0x4,%eax
    1c5e:	cd 40                	int    $0x40
    1c60:	c3                   	retq   

0000000000001c61 <read>:
SYSCALL(read)
    1c61:	b8 05 00 00 00       	mov    $0x5,%eax
    1c66:	cd 40                	int    $0x40
    1c68:	c3                   	retq   

0000000000001c69 <write>:
SYSCALL(write)
    1c69:	b8 10 00 00 00       	mov    $0x10,%eax
    1c6e:	cd 40                	int    $0x40
    1c70:	c3                   	retq   

0000000000001c71 <close>:
SYSCALL(close)
    1c71:	b8 15 00 00 00       	mov    $0x15,%eax
    1c76:	cd 40                	int    $0x40
    1c78:	c3                   	retq   

0000000000001c79 <kill>:
SYSCALL(kill)
    1c79:	b8 06 00 00 00       	mov    $0x6,%eax
    1c7e:	cd 40                	int    $0x40
    1c80:	c3                   	retq   

0000000000001c81 <exec>:
SYSCALL(exec)
    1c81:	b8 07 00 00 00       	mov    $0x7,%eax
    1c86:	cd 40                	int    $0x40
    1c88:	c3                   	retq   

0000000000001c89 <open>:
SYSCALL(open)
    1c89:	b8 0f 00 00 00       	mov    $0xf,%eax
    1c8e:	cd 40                	int    $0x40
    1c90:	c3                   	retq   

0000000000001c91 <mknod>:
SYSCALL(mknod)
    1c91:	b8 11 00 00 00       	mov    $0x11,%eax
    1c96:	cd 40                	int    $0x40
    1c98:	c3                   	retq   

0000000000001c99 <unlink>:
SYSCALL(unlink)
    1c99:	b8 12 00 00 00       	mov    $0x12,%eax
    1c9e:	cd 40                	int    $0x40
    1ca0:	c3                   	retq   

0000000000001ca1 <fstat>:
SYSCALL(fstat)
    1ca1:	b8 08 00 00 00       	mov    $0x8,%eax
    1ca6:	cd 40                	int    $0x40
    1ca8:	c3                   	retq   

0000000000001ca9 <link>:
SYSCALL(link)
    1ca9:	b8 13 00 00 00       	mov    $0x13,%eax
    1cae:	cd 40                	int    $0x40
    1cb0:	c3                   	retq   

0000000000001cb1 <mkdir>:
SYSCALL(mkdir)
    1cb1:	b8 14 00 00 00       	mov    $0x14,%eax
    1cb6:	cd 40                	int    $0x40
    1cb8:	c3                   	retq   

0000000000001cb9 <chdir>:
SYSCALL(chdir)
    1cb9:	b8 09 00 00 00       	mov    $0x9,%eax
    1cbe:	cd 40                	int    $0x40
    1cc0:	c3                   	retq   

0000000000001cc1 <dup>:
SYSCALL(dup)
    1cc1:	b8 0a 00 00 00       	mov    $0xa,%eax
    1cc6:	cd 40                	int    $0x40
    1cc8:	c3                   	retq   

0000000000001cc9 <getpid>:
SYSCALL(getpid)
    1cc9:	b8 0b 00 00 00       	mov    $0xb,%eax
    1cce:	cd 40                	int    $0x40
    1cd0:	c3                   	retq   

0000000000001cd1 <sbrk>:
SYSCALL(sbrk)
    1cd1:	b8 0c 00 00 00       	mov    $0xc,%eax
    1cd6:	cd 40                	int    $0x40
    1cd8:	c3                   	retq   

0000000000001cd9 <sleep>:
SYSCALL(sleep)
    1cd9:	b8 0d 00 00 00       	mov    $0xd,%eax
    1cde:	cd 40                	int    $0x40
    1ce0:	c3                   	retq   

0000000000001ce1 <uptime>:
SYSCALL(uptime)
    1ce1:	b8 0e 00 00 00       	mov    $0xe,%eax
    1ce6:	cd 40                	int    $0x40
    1ce8:	c3                   	retq   

0000000000001ce9 <sysinfo>:
SYSCALL(sysinfo)
    1ce9:	b8 16 00 00 00       	mov    $0x16,%eax
    1cee:	cd 40                	int    $0x40
    1cf0:	c3                   	retq   

0000000000001cf1 <crashn>:
SYSCALL(crashn)
    1cf1:	b8 17 00 00 00       	mov    $0x17,%eax
    1cf6:	cd 40                	int    $0x40
    1cf8:	c3                   	retq   

0000000000001cf9 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
    1cf9:	55                   	push   %rbp
    1cfa:	48 89 e5             	mov    %rsp,%rbp
    1cfd:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
    1d01:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1d05:	48 83 e8 10          	sub    $0x10,%rax
    1d09:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1d0d:	48 8b 05 1c 10 00 00 	mov    0x101c(%rip),%rax        # 2d30 <freep>
    1d14:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1d18:	eb 2f                	jmp    1d49 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    1d1a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1d1e:	48 8b 00             	mov    (%rax),%rax
    1d21:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1d25:	77 17                	ja     1d3e <free+0x45>
    1d27:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1d2b:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1d2f:	77 2f                	ja     1d60 <free+0x67>
    1d31:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1d35:	48 8b 00             	mov    (%rax),%rax
    1d38:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1d3c:	77 22                	ja     1d60 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1d3e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1d42:	48 8b 00             	mov    (%rax),%rax
    1d45:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1d49:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1d4d:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1d51:	76 c7                	jbe    1d1a <free+0x21>
    1d53:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1d57:	48 8b 00             	mov    (%rax),%rax
    1d5a:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1d5e:	76 ba                	jbe    1d1a <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
    1d60:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1d64:	8b 40 08             	mov    0x8(%rax),%eax
    1d67:	89 c0                	mov    %eax,%eax
    1d69:	48 c1 e0 04          	shl    $0x4,%rax
    1d6d:	48 89 c2             	mov    %rax,%rdx
    1d70:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1d74:	48 01 c2             	add    %rax,%rdx
    1d77:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1d7b:	48 8b 00             	mov    (%rax),%rax
    1d7e:	48 39 c2             	cmp    %rax,%rdx
    1d81:	75 2d                	jne    1db0 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
    1d83:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1d87:	8b 50 08             	mov    0x8(%rax),%edx
    1d8a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1d8e:	48 8b 00             	mov    (%rax),%rax
    1d91:	8b 40 08             	mov    0x8(%rax),%eax
    1d94:	01 c2                	add    %eax,%edx
    1d96:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1d9a:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
    1d9d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1da1:	48 8b 00             	mov    (%rax),%rax
    1da4:	48 8b 10             	mov    (%rax),%rdx
    1da7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1dab:	48 89 10             	mov    %rdx,(%rax)
    1dae:	eb 0e                	jmp    1dbe <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
    1db0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1db4:	48 8b 10             	mov    (%rax),%rdx
    1db7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1dbb:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
    1dbe:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1dc2:	8b 40 08             	mov    0x8(%rax),%eax
    1dc5:	89 c0                	mov    %eax,%eax
    1dc7:	48 c1 e0 04          	shl    $0x4,%rax
    1dcb:	48 89 c2             	mov    %rax,%rdx
    1dce:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1dd2:	48 01 d0             	add    %rdx,%rax
    1dd5:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1dd9:	75 27                	jne    1e02 <free+0x109>
    p->s.size += bp->s.size;
    1ddb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1ddf:	8b 50 08             	mov    0x8(%rax),%edx
    1de2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1de6:	8b 40 08             	mov    0x8(%rax),%eax
    1de9:	01 c2                	add    %eax,%edx
    1deb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1def:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
    1df2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1df6:	48 8b 10             	mov    (%rax),%rdx
    1df9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1dfd:	48 89 10             	mov    %rdx,(%rax)
    1e00:	eb 0b                	jmp    1e0d <free+0x114>
  } else
    p->s.ptr = bp;
    1e02:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1e06:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1e0a:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
    1e0d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1e11:	48 89 05 18 0f 00 00 	mov    %rax,0xf18(%rip)        # 2d30 <freep>
}
    1e18:	90                   	nop
    1e19:	5d                   	pop    %rbp
    1e1a:	c3                   	retq   

0000000000001e1b <morecore>:

static Header *morecore(uint nu) {
    1e1b:	55                   	push   %rbp
    1e1c:	48 89 e5             	mov    %rsp,%rbp
    1e1f:	48 83 ec 20          	sub    $0x20,%rsp
    1e23:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
    1e26:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
    1e2d:	77 07                	ja     1e36 <morecore+0x1b>
    nu = 4096;
    1e2f:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
    1e36:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1e39:	c1 e0 04             	shl    $0x4,%eax
    1e3c:	89 c7                	mov    %eax,%edi
    1e3e:	e8 8e fe ff ff       	callq  1cd1 <sbrk>
    1e43:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
    1e47:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
    1e4c:	75 07                	jne    1e55 <morecore+0x3a>
    return 0;
    1e4e:	b8 00 00 00 00       	mov    $0x0,%eax
    1e53:	eb 29                	jmp    1e7e <morecore+0x63>
  hp = (Header *)p;
    1e55:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1e59:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
    1e5d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1e61:	8b 55 ec             	mov    -0x14(%rbp),%edx
    1e64:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
    1e67:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1e6b:	48 83 c0 10          	add    $0x10,%rax
    1e6f:	48 89 c7             	mov    %rax,%rdi
    1e72:	e8 82 fe ff ff       	callq  1cf9 <free>
  return freep;
    1e77:	48 8b 05 b2 0e 00 00 	mov    0xeb2(%rip),%rax        # 2d30 <freep>
}
    1e7e:	c9                   	leaveq 
    1e7f:	c3                   	retq   

0000000000001e80 <malloc>:

void *malloc(uint nbytes) {
    1e80:	55                   	push   %rbp
    1e81:	48 89 e5             	mov    %rsp,%rbp
    1e84:	48 83 ec 30          	sub    $0x30,%rsp
    1e88:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
    1e8b:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1e8e:	48 83 c0 0f          	add    $0xf,%rax
    1e92:	48 c1 e8 04          	shr    $0x4,%rax
    1e96:	83 c0 01             	add    $0x1,%eax
    1e99:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
    1e9c:	48 8b 05 8d 0e 00 00 	mov    0xe8d(%rip),%rax        # 2d30 <freep>
    1ea3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    1ea7:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    1eac:	75 2b                	jne    1ed9 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
    1eae:	48 c7 45 f0 20 2d 00 	movq   $0x2d20,-0x10(%rbp)
    1eb5:	00 
    1eb6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1eba:	48 89 05 6f 0e 00 00 	mov    %rax,0xe6f(%rip)        # 2d30 <freep>
    1ec1:	48 8b 05 68 0e 00 00 	mov    0xe68(%rip),%rax        # 2d30 <freep>
    1ec8:	48 89 05 51 0e 00 00 	mov    %rax,0xe51(%rip)        # 2d20 <base>
    base.s.size = 0;
    1ecf:	c7 05 4f 0e 00 00 00 	movl   $0x0,0xe4f(%rip)        # 2d28 <base+0x8>
    1ed6:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1ed9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1edd:	48 8b 00             	mov    (%rax),%rax
    1ee0:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
    1ee4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1ee8:	8b 40 08             	mov    0x8(%rax),%eax
    1eeb:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1eee:	72 5f                	jb     1f4f <malloc+0xcf>
      if (p->s.size == nunits)
    1ef0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1ef4:	8b 40 08             	mov    0x8(%rax),%eax
    1ef7:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1efa:	75 10                	jne    1f0c <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
    1efc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f00:	48 8b 10             	mov    (%rax),%rdx
    1f03:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1f07:	48 89 10             	mov    %rdx,(%rax)
    1f0a:	eb 2e                	jmp    1f3a <malloc+0xba>
      else {
        p->s.size -= nunits;
    1f0c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f10:	8b 40 08             	mov    0x8(%rax),%eax
    1f13:	2b 45 ec             	sub    -0x14(%rbp),%eax
    1f16:	89 c2                	mov    %eax,%edx
    1f18:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f1c:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
    1f1f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f23:	8b 40 08             	mov    0x8(%rax),%eax
    1f26:	89 c0                	mov    %eax,%eax
    1f28:	48 c1 e0 04          	shl    $0x4,%rax
    1f2c:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
    1f30:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f34:	8b 55 ec             	mov    -0x14(%rbp),%edx
    1f37:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
    1f3a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1f3e:	48 89 05 eb 0d 00 00 	mov    %rax,0xdeb(%rip)        # 2d30 <freep>
      return (void *)(p + 1);
    1f45:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f49:	48 83 c0 10          	add    $0x10,%rax
    1f4d:	eb 41                	jmp    1f90 <malloc+0x110>
    }
    if (p == freep)
    1f4f:	48 8b 05 da 0d 00 00 	mov    0xdda(%rip),%rax        # 2d30 <freep>
    1f56:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    1f5a:	75 1c                	jne    1f78 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    1f5c:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1f5f:	89 c7                	mov    %eax,%edi
    1f61:	e8 b5 fe ff ff       	callq  1e1b <morecore>
    1f66:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1f6a:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    1f6f:	75 07                	jne    1f78 <malloc+0xf8>
        return 0;
    1f71:	b8 00 00 00 00       	mov    $0x0,%eax
    1f76:	eb 18                	jmp    1f90 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1f78:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f7c:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    1f80:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1f84:	48 8b 00             	mov    (%rax),%rax
    1f87:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    1f8b:	e9 54 ff ff ff       	jmpq   1ee4 <malloc+0x64>
    1f90:	c9                   	leaveq 
    1f91:	c3                   	retq   
