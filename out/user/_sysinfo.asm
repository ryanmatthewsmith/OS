
out/user/_sysinfo:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <fs.h>
#include <stat.h>
#include <sysinfo.h>
#include <user.h>

int main(int argc, char *argv[]) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 30          	sub    $0x30,%rsp
   8:	89 7d dc             	mov    %edi,-0x24(%rbp)
   b:	48 89 75 d0          	mov    %rsi,-0x30(%rbp)
  struct sys_info info;
  sysinfo(&info);
   f:	48 8d 45 ec          	lea    -0x14(%rbp),%rax
  13:	48 89 c7             	mov    %rax,%rdi
  16:	e8 7f 08 00 00       	callq  89a <sysinfo>

  printf(1, "pages_in_use = %d\n", info.pages_in_use);
  1b:	8b 45 ec             	mov    -0x14(%rbp),%eax
  1e:	89 c2                	mov    %eax,%edx
  20:	be 43 0b 00 00       	mov    $0xb43,%esi
  25:	bf 01 00 00 00       	mov    $0x1,%edi
  2a:	b8 00 00 00 00       	mov    $0x0,%eax
  2f:	e8 23 02 00 00       	callq  257 <printf>
  printf(1, "pages_in_swap = %d\n", info.pages_in_swap);
  34:	8b 45 f0             	mov    -0x10(%rbp),%eax
  37:	89 c2                	mov    %eax,%edx
  39:	be 56 0b 00 00       	mov    $0xb56,%esi
  3e:	bf 01 00 00 00       	mov    $0x1,%edi
  43:	b8 00 00 00 00       	mov    $0x0,%eax
  48:	e8 0a 02 00 00       	callq  257 <printf>
  printf(1, "free_pages = %d\n", info.free_pages);
  4d:	8b 45 f4             	mov    -0xc(%rbp),%eax
  50:	89 c2                	mov    %eax,%edx
  52:	be 6a 0b 00 00       	mov    $0xb6a,%esi
  57:	bf 01 00 00 00       	mov    $0x1,%edi
  5c:	b8 00 00 00 00       	mov    $0x0,%eax
  61:	e8 f1 01 00 00       	callq  257 <printf>
  printf(1, "num_page_faults = %d\n", info.num_page_faults);
  66:	8b 45 f8             	mov    -0x8(%rbp),%eax
  69:	89 c2                	mov    %eax,%edx
  6b:	be 7b 0b 00 00       	mov    $0xb7b,%esi
  70:	bf 01 00 00 00       	mov    $0x1,%edi
  75:	b8 00 00 00 00       	mov    $0x0,%eax
  7a:	e8 d8 01 00 00       	callq  257 <printf>
  printf(1, "num_disk_reads = %d\n", info.num_disk_reads);
  7f:	8b 45 fc             	mov    -0x4(%rbp),%eax
  82:	89 c2                	mov    %eax,%edx
  84:	be 91 0b 00 00       	mov    $0xb91,%esi
  89:	bf 01 00 00 00       	mov    $0x1,%edi
  8e:	b8 00 00 00 00       	mov    $0x0,%eax
  93:	e8 bf 01 00 00       	callq  257 <printf>

  exit();
  98:	e8 5d 07 00 00       	callq  7fa <exit>

000000000000009d <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  9d:	55                   	push   %rbp
  9e:	48 89 e5             	mov    %rsp,%rbp
  a1:	48 83 ec 10          	sub    $0x10,%rsp
  a5:	89 7d fc             	mov    %edi,-0x4(%rbp)
  a8:	89 f0                	mov    %esi,%eax
  aa:	88 45 f8             	mov    %al,-0x8(%rbp)
  ad:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  b1:	8b 45 fc             	mov    -0x4(%rbp),%eax
  b4:	ba 01 00 00 00       	mov    $0x1,%edx
  b9:	48 89 ce             	mov    %rcx,%rsi
  bc:	89 c7                	mov    %eax,%edi
  be:	e8 57 07 00 00       	callq  81a <write>
  c3:	90                   	nop
  c4:	c9                   	leaveq 
  c5:	c3                   	retq   

00000000000000c6 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  c6:	55                   	push   %rbp
  c7:	48 89 e5             	mov    %rsp,%rbp
  ca:	48 83 ec 40          	sub    $0x40,%rsp
  ce:	89 7d cc             	mov    %edi,-0x34(%rbp)
  d1:	89 75 c8             	mov    %esi,-0x38(%rbp)
  d4:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  d7:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  da:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  de:	74 1f                	je     ff <printint64+0x39>
  e0:	8b 45 c8             	mov    -0x38(%rbp),%eax
  e3:	c1 e8 1f             	shr    $0x1f,%eax
  e6:	0f b6 c0             	movzbl %al,%eax
  e9:	89 45 c0             	mov    %eax,-0x40(%rbp)
  ec:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  f0:	74 0d                	je     ff <printint64+0x39>
    x = -xx;
  f2:	8b 45 c8             	mov    -0x38(%rbp),%eax
  f5:	f7 d8                	neg    %eax
  f7:	48 98                	cltq   
  f9:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  fd:	eb 09                	jmp    108 <printint64+0x42>
  else
    x = xx;
  ff:	8b 45 c8             	mov    -0x38(%rbp),%eax
 102:	48 98                	cltq   
 104:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 108:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 10f:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 112:	8d 41 01             	lea    0x1(%rcx),%eax
 115:	89 45 fc             	mov    %eax,-0x4(%rbp)
 118:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 11b:	48 63 f0             	movslq %eax,%rsi
 11e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 122:	ba 00 00 00 00       	mov    $0x0,%edx
 127:	48 f7 f6             	div    %rsi
 12a:	48 89 d0             	mov    %rdx,%rax
 12d:	0f b6 90 10 0e 00 00 	movzbl 0xe10(%rax),%edx
 134:	48 63 c1             	movslq %ecx,%rax
 137:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 13b:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 13e:	48 63 f8             	movslq %eax,%rdi
 141:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 145:	ba 00 00 00 00       	mov    $0x0,%edx
 14a:	48 f7 f7             	div    %rdi
 14d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 151:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 156:	75 b7                	jne    10f <printint64+0x49>

  if (sgn)
 158:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 15c:	74 2b                	je     189 <printint64+0xc3>
    buf[i++] = '-';
 15e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 161:	8d 50 01             	lea    0x1(%rax),%edx
 164:	89 55 fc             	mov    %edx,-0x4(%rbp)
 167:	48 98                	cltq   
 169:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 16e:	eb 19                	jmp    189 <printint64+0xc3>
    putc(fd, buf[i]);
 170:	8b 45 fc             	mov    -0x4(%rbp),%eax
 173:	48 98                	cltq   
 175:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 17a:	0f be d0             	movsbl %al,%edx
 17d:	8b 45 cc             	mov    -0x34(%rbp),%eax
 180:	89 d6                	mov    %edx,%esi
 182:	89 c7                	mov    %eax,%edi
 184:	e8 14 ff ff ff       	callq  9d <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 189:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 18d:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 191:	79 dd                	jns    170 <printint64+0xaa>
    putc(fd, buf[i]);
}
 193:	90                   	nop
 194:	c9                   	leaveq 
 195:	c3                   	retq   

0000000000000196 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 196:	55                   	push   %rbp
 197:	48 89 e5             	mov    %rsp,%rbp
 19a:	48 83 ec 30          	sub    $0x30,%rsp
 19e:	89 7d dc             	mov    %edi,-0x24(%rbp)
 1a1:	89 75 d8             	mov    %esi,-0x28(%rbp)
 1a4:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 1a7:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 1aa:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 1b1:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 1b5:	74 17                	je     1ce <printint+0x38>
 1b7:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 1bb:	79 11                	jns    1ce <printint+0x38>
    neg = 1;
 1bd:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 1c4:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1c7:	f7 d8                	neg    %eax
 1c9:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1cc:	eb 06                	jmp    1d4 <printint+0x3e>
  } else {
    x = xx;
 1ce:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1d1:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 1d4:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1db:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1de:	8d 41 01             	lea    0x1(%rcx),%eax
 1e1:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1e4:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 1e7:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1ea:	ba 00 00 00 00       	mov    $0x0,%edx
 1ef:	f7 f6                	div    %esi
 1f1:	89 d0                	mov    %edx,%eax
 1f3:	89 c0                	mov    %eax,%eax
 1f5:	0f b6 90 30 0e 00 00 	movzbl 0xe30(%rax),%edx
 1fc:	48 63 c1             	movslq %ecx,%rax
 1ff:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 203:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 206:	8b 45 f4             	mov    -0xc(%rbp),%eax
 209:	ba 00 00 00 00       	mov    $0x0,%edx
 20e:	f7 f7                	div    %edi
 210:	89 45 f4             	mov    %eax,-0xc(%rbp)
 213:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 217:	75 c2                	jne    1db <printint+0x45>
  if (neg)
 219:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 21d:	74 2b                	je     24a <printint+0xb4>
    buf[i++] = '-';
 21f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 222:	8d 50 01             	lea    0x1(%rax),%edx
 225:	89 55 fc             	mov    %edx,-0x4(%rbp)
 228:	48 98                	cltq   
 22a:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 22f:	eb 19                	jmp    24a <printint+0xb4>
    putc(fd, buf[i]);
 231:	8b 45 fc             	mov    -0x4(%rbp),%eax
 234:	48 98                	cltq   
 236:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 23b:	0f be d0             	movsbl %al,%edx
 23e:	8b 45 dc             	mov    -0x24(%rbp),%eax
 241:	89 d6                	mov    %edx,%esi
 243:	89 c7                	mov    %eax,%edi
 245:	e8 53 fe ff ff       	callq  9d <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 24a:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 24e:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 252:	79 dd                	jns    231 <printint+0x9b>
    putc(fd, buf[i]);
}
 254:	90                   	nop
 255:	c9                   	leaveq 
 256:	c3                   	retq   

0000000000000257 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 257:	55                   	push   %rbp
 258:	48 89 e5             	mov    %rsp,%rbp
 25b:	48 83 ec 70          	sub    $0x70,%rsp
 25f:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 262:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 266:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 26a:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 26e:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 272:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 276:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 27d:	48 8d 45 10          	lea    0x10(%rbp),%rax
 281:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 285:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 289:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 28d:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 294:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 29b:	e9 68 02 00 00       	jmpq   508 <printf+0x2b1>
    c = fmt[i] & 0xff;
 2a0:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 2a3:	48 63 d0             	movslq %eax,%rdx
 2a6:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 2aa:	48 01 d0             	add    %rdx,%rax
 2ad:	0f b6 00             	movzbl (%rax),%eax
 2b0:	0f be c0             	movsbl %al,%eax
 2b3:	25 ff 00 00 00       	and    $0xff,%eax
 2b8:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 2bb:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 2bf:	75 30                	jne    2f1 <printf+0x9a>
      if (c == '%') {
 2c1:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 2c5:	75 13                	jne    2da <printf+0x83>
        state = '%';
 2c7:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 2ce:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 2d5:	e9 2a 02 00 00       	jmpq   504 <printf+0x2ad>
      } else {
        putc(fd, c);
 2da:	8b 45 b8             	mov    -0x48(%rbp),%eax
 2dd:	0f be d0             	movsbl %al,%edx
 2e0:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2e3:	89 d6                	mov    %edx,%esi
 2e5:	89 c7                	mov    %eax,%edi
 2e7:	e8 b1 fd ff ff       	callq  9d <putc>
 2ec:	e9 13 02 00 00       	jmpq   504 <printf+0x2ad>
      }
    } else if (state == '%') {
 2f1:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 2f5:	0f 85 09 02 00 00    	jne    504 <printf+0x2ad>
      if (c == 'l') {
 2fb:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 2ff:	75 0c                	jne    30d <printf+0xb6>
        lflag = 1;
 301:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 308:	e9 f7 01 00 00       	jmpq   504 <printf+0x2ad>
      } else if (c == 'd') {
 30d:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 311:	0f 85 95 00 00 00    	jne    3ac <printf+0x155>
        if (lflag == 1)
 317:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 31b:	75 49                	jne    366 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 31d:	8b 45 a0             	mov    -0x60(%rbp),%eax
 320:	83 f8 30             	cmp    $0x30,%eax
 323:	73 17                	jae    33c <printf+0xe5>
 325:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 329:	8b 55 a0             	mov    -0x60(%rbp),%edx
 32c:	89 d2                	mov    %edx,%edx
 32e:	48 01 d0             	add    %rdx,%rax
 331:	8b 55 a0             	mov    -0x60(%rbp),%edx
 334:	83 c2 08             	add    $0x8,%edx
 337:	89 55 a0             	mov    %edx,-0x60(%rbp)
 33a:	eb 0c                	jmp    348 <printf+0xf1>
 33c:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 340:	48 8d 50 08          	lea    0x8(%rax),%rdx
 344:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 348:	48 8b 00             	mov    (%rax),%rax
 34b:	89 c6                	mov    %eax,%esi
 34d:	8b 45 9c             	mov    -0x64(%rbp),%eax
 350:	b9 01 00 00 00       	mov    $0x1,%ecx
 355:	ba 0a 00 00 00       	mov    $0xa,%edx
 35a:	89 c7                	mov    %eax,%edi
 35c:	e8 65 fd ff ff       	callq  c6 <printint64>
 361:	e9 97 01 00 00       	jmpq   4fd <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 366:	8b 45 a0             	mov    -0x60(%rbp),%eax
 369:	83 f8 30             	cmp    $0x30,%eax
 36c:	73 17                	jae    385 <printf+0x12e>
 36e:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 372:	8b 55 a0             	mov    -0x60(%rbp),%edx
 375:	89 d2                	mov    %edx,%edx
 377:	48 01 d0             	add    %rdx,%rax
 37a:	8b 55 a0             	mov    -0x60(%rbp),%edx
 37d:	83 c2 08             	add    $0x8,%edx
 380:	89 55 a0             	mov    %edx,-0x60(%rbp)
 383:	eb 0c                	jmp    391 <printf+0x13a>
 385:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 389:	48 8d 50 08          	lea    0x8(%rax),%rdx
 38d:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 391:	8b 30                	mov    (%rax),%esi
 393:	8b 45 9c             	mov    -0x64(%rbp),%eax
 396:	b9 01 00 00 00       	mov    $0x1,%ecx
 39b:	ba 0a 00 00 00       	mov    $0xa,%edx
 3a0:	89 c7                	mov    %eax,%edi
 3a2:	e8 ef fd ff ff       	callq  196 <printint>
 3a7:	e9 51 01 00 00       	jmpq   4fd <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 3ac:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 3b0:	74 0a                	je     3bc <printf+0x165>
 3b2:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 3b6:	0f 85 95 00 00 00    	jne    451 <printf+0x1fa>
        if (lflag == 1)
 3bc:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 3c0:	75 49                	jne    40b <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 3c2:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3c5:	83 f8 30             	cmp    $0x30,%eax
 3c8:	73 17                	jae    3e1 <printf+0x18a>
 3ca:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3ce:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3d1:	89 d2                	mov    %edx,%edx
 3d3:	48 01 d0             	add    %rdx,%rax
 3d6:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3d9:	83 c2 08             	add    $0x8,%edx
 3dc:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3df:	eb 0c                	jmp    3ed <printf+0x196>
 3e1:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3e5:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3e9:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3ed:	48 8b 00             	mov    (%rax),%rax
 3f0:	89 c6                	mov    %eax,%esi
 3f2:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3f5:	b9 00 00 00 00       	mov    $0x0,%ecx
 3fa:	ba 10 00 00 00       	mov    $0x10,%edx
 3ff:	89 c7                	mov    %eax,%edi
 401:	e8 c0 fc ff ff       	callq  c6 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 406:	e9 f2 00 00 00       	jmpq   4fd <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 40b:	8b 45 a0             	mov    -0x60(%rbp),%eax
 40e:	83 f8 30             	cmp    $0x30,%eax
 411:	73 17                	jae    42a <printf+0x1d3>
 413:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 417:	8b 55 a0             	mov    -0x60(%rbp),%edx
 41a:	89 d2                	mov    %edx,%edx
 41c:	48 01 d0             	add    %rdx,%rax
 41f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 422:	83 c2 08             	add    $0x8,%edx
 425:	89 55 a0             	mov    %edx,-0x60(%rbp)
 428:	eb 0c                	jmp    436 <printf+0x1df>
 42a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 42e:	48 8d 50 08          	lea    0x8(%rax),%rdx
 432:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 436:	8b 30                	mov    (%rax),%esi
 438:	8b 45 9c             	mov    -0x64(%rbp),%eax
 43b:	b9 00 00 00 00       	mov    $0x0,%ecx
 440:	ba 10 00 00 00       	mov    $0x10,%edx
 445:	89 c7                	mov    %eax,%edi
 447:	e8 4a fd ff ff       	callq  196 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 44c:	e9 ac 00 00 00       	jmpq   4fd <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 451:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 455:	75 6b                	jne    4c2 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 457:	8b 45 a0             	mov    -0x60(%rbp),%eax
 45a:	83 f8 30             	cmp    $0x30,%eax
 45d:	73 17                	jae    476 <printf+0x21f>
 45f:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 463:	8b 55 a0             	mov    -0x60(%rbp),%edx
 466:	89 d2                	mov    %edx,%edx
 468:	48 01 d0             	add    %rdx,%rax
 46b:	8b 55 a0             	mov    -0x60(%rbp),%edx
 46e:	83 c2 08             	add    $0x8,%edx
 471:	89 55 a0             	mov    %edx,-0x60(%rbp)
 474:	eb 0c                	jmp    482 <printf+0x22b>
 476:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 47a:	48 8d 50 08          	lea    0x8(%rax),%rdx
 47e:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 482:	48 8b 00             	mov    (%rax),%rax
 485:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 489:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 48e:	75 25                	jne    4b5 <printf+0x25e>
          s = "(null)";
 490:	48 c7 45 c8 a6 0b 00 	movq   $0xba6,-0x38(%rbp)
 497:	00 
        for (; *s; s++)
 498:	eb 1b                	jmp    4b5 <printf+0x25e>
          putc(fd, *s);
 49a:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 49e:	0f b6 00             	movzbl (%rax),%eax
 4a1:	0f be d0             	movsbl %al,%edx
 4a4:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4a7:	89 d6                	mov    %edx,%esi
 4a9:	89 c7                	mov    %eax,%edi
 4ab:	e8 ed fb ff ff       	callq  9d <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 4b0:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 4b5:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 4b9:	0f b6 00             	movzbl (%rax),%eax
 4bc:	84 c0                	test   %al,%al
 4be:	75 da                	jne    49a <printf+0x243>
 4c0:	eb 3b                	jmp    4fd <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 4c2:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 4c6:	75 14                	jne    4dc <printf+0x285>
        putc(fd, c);
 4c8:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4cb:	0f be d0             	movsbl %al,%edx
 4ce:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4d1:	89 d6                	mov    %edx,%esi
 4d3:	89 c7                	mov    %eax,%edi
 4d5:	e8 c3 fb ff ff       	callq  9d <putc>
 4da:	eb 21                	jmp    4fd <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 4dc:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4df:	be 25 00 00 00       	mov    $0x25,%esi
 4e4:	89 c7                	mov    %eax,%edi
 4e6:	e8 b2 fb ff ff       	callq  9d <putc>
        putc(fd, c);
 4eb:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4ee:	0f be d0             	movsbl %al,%edx
 4f1:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4f4:	89 d6                	mov    %edx,%esi
 4f6:	89 c7                	mov    %eax,%edi
 4f8:	e8 a0 fb ff ff       	callq  9d <putc>
      }
      state = 0;
 4fd:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 504:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 508:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 50b:	48 63 d0             	movslq %eax,%rdx
 50e:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 512:	48 01 d0             	add    %rdx,%rax
 515:	0f b6 00             	movzbl (%rax),%eax
 518:	84 c0                	test   %al,%al
 51a:	0f 85 80 fd ff ff    	jne    2a0 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 520:	90                   	nop
 521:	c9                   	leaveq 
 522:	c3                   	retq   

0000000000000523 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 523:	55                   	push   %rbp
 524:	48 89 e5             	mov    %rsp,%rbp
 527:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 52b:	89 75 f4             	mov    %esi,-0xc(%rbp)
 52e:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 531:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 535:	8b 55 f0             	mov    -0x10(%rbp),%edx
 538:	8b 45 f4             	mov    -0xc(%rbp),%eax
 53b:	48 89 ce             	mov    %rcx,%rsi
 53e:	48 89 f7             	mov    %rsi,%rdi
 541:	89 d1                	mov    %edx,%ecx
 543:	fc                   	cld    
 544:	f3 aa                	rep stos %al,%es:(%rdi)
 546:	89 ca                	mov    %ecx,%edx
 548:	48 89 fe             	mov    %rdi,%rsi
 54b:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 54f:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 552:	90                   	nop
 553:	5d                   	pop    %rbp
 554:	c3                   	retq   

0000000000000555 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 555:	55                   	push   %rbp
 556:	48 89 e5             	mov    %rsp,%rbp
 559:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 55d:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 561:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 565:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 569:	90                   	nop
 56a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 56e:	48 8d 50 01          	lea    0x1(%rax),%rdx
 572:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 576:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 57a:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 57e:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 582:	0f b6 12             	movzbl (%rdx),%edx
 585:	88 10                	mov    %dl,(%rax)
 587:	0f b6 00             	movzbl (%rax),%eax
 58a:	84 c0                	test   %al,%al
 58c:	75 dc                	jne    56a <strcpy+0x15>
    ;
  return os;
 58e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 592:	5d                   	pop    %rbp
 593:	c3                   	retq   

0000000000000594 <strcmp>:

int strcmp(const char *p, const char *q) {
 594:	55                   	push   %rbp
 595:	48 89 e5             	mov    %rsp,%rbp
 598:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 59c:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 5a0:	eb 0a                	jmp    5ac <strcmp+0x18>
    p++, q++;
 5a2:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 5a7:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 5ac:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5b0:	0f b6 00             	movzbl (%rax),%eax
 5b3:	84 c0                	test   %al,%al
 5b5:	74 12                	je     5c9 <strcmp+0x35>
 5b7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5bb:	0f b6 10             	movzbl (%rax),%edx
 5be:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5c2:	0f b6 00             	movzbl (%rax),%eax
 5c5:	38 c2                	cmp    %al,%dl
 5c7:	74 d9                	je     5a2 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 5c9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5cd:	0f b6 00             	movzbl (%rax),%eax
 5d0:	0f b6 d0             	movzbl %al,%edx
 5d3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5d7:	0f b6 00             	movzbl (%rax),%eax
 5da:	0f b6 c0             	movzbl %al,%eax
 5dd:	29 c2                	sub    %eax,%edx
 5df:	89 d0                	mov    %edx,%eax
}
 5e1:	5d                   	pop    %rbp
 5e2:	c3                   	retq   

00000000000005e3 <strlen>:

uint strlen(char *s) {
 5e3:	55                   	push   %rbp
 5e4:	48 89 e5             	mov    %rsp,%rbp
 5e7:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 5eb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 5f2:	eb 04                	jmp    5f8 <strlen+0x15>
 5f4:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 5f8:	8b 45 fc             	mov    -0x4(%rbp),%eax
 5fb:	48 63 d0             	movslq %eax,%rdx
 5fe:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 602:	48 01 d0             	add    %rdx,%rax
 605:	0f b6 00             	movzbl (%rax),%eax
 608:	84 c0                	test   %al,%al
 60a:	75 e8                	jne    5f4 <strlen+0x11>
    ;
  return n;
 60c:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 60f:	5d                   	pop    %rbp
 610:	c3                   	retq   

0000000000000611 <memset>:

void *memset(void *dst, int c, uint n) {
 611:	55                   	push   %rbp
 612:	48 89 e5             	mov    %rsp,%rbp
 615:	48 83 ec 10          	sub    $0x10,%rsp
 619:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 61d:	89 75 f4             	mov    %esi,-0xc(%rbp)
 620:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 623:	8b 55 f0             	mov    -0x10(%rbp),%edx
 626:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 629:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 62d:	89 ce                	mov    %ecx,%esi
 62f:	48 89 c7             	mov    %rax,%rdi
 632:	e8 ec fe ff ff       	callq  523 <stosb>
  return dst;
 637:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 63b:	c9                   	leaveq 
 63c:	c3                   	retq   

000000000000063d <strchr>:

char *strchr(const char *s, char c) {
 63d:	55                   	push   %rbp
 63e:	48 89 e5             	mov    %rsp,%rbp
 641:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 645:	89 f0                	mov    %esi,%eax
 647:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 64a:	eb 17                	jmp    663 <strchr+0x26>
    if (*s == c)
 64c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 650:	0f b6 00             	movzbl (%rax),%eax
 653:	3a 45 f4             	cmp    -0xc(%rbp),%al
 656:	75 06                	jne    65e <strchr+0x21>
      return (char *)s;
 658:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 65c:	eb 15                	jmp    673 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 65e:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 663:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 667:	0f b6 00             	movzbl (%rax),%eax
 66a:	84 c0                	test   %al,%al
 66c:	75 de                	jne    64c <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 66e:	b8 00 00 00 00       	mov    $0x0,%eax
}
 673:	5d                   	pop    %rbp
 674:	c3                   	retq   

0000000000000675 <gets>:

char *gets(char *buf, int max) {
 675:	55                   	push   %rbp
 676:	48 89 e5             	mov    %rsp,%rbp
 679:	48 83 ec 20          	sub    $0x20,%rsp
 67d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 681:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 684:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 68b:	eb 48                	jmp    6d5 <gets+0x60>
    cc = read(0, &c, 1);
 68d:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 691:	ba 01 00 00 00       	mov    $0x1,%edx
 696:	48 89 c6             	mov    %rax,%rsi
 699:	bf 00 00 00 00       	mov    $0x0,%edi
 69e:	e8 6f 01 00 00       	callq  812 <read>
 6a3:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 6a6:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 6aa:	7e 36                	jle    6e2 <gets+0x6d>
      break;
    buf[i++] = c;
 6ac:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6af:	8d 50 01             	lea    0x1(%rax),%edx
 6b2:	89 55 fc             	mov    %edx,-0x4(%rbp)
 6b5:	48 63 d0             	movslq %eax,%rdx
 6b8:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6bc:	48 01 c2             	add    %rax,%rdx
 6bf:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6c3:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 6c5:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6c9:	3c 0a                	cmp    $0xa,%al
 6cb:	74 16                	je     6e3 <gets+0x6e>
 6cd:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6d1:	3c 0d                	cmp    $0xd,%al
 6d3:	74 0e                	je     6e3 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6d5:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6d8:	83 c0 01             	add    $0x1,%eax
 6db:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 6de:	7c ad                	jl     68d <gets+0x18>
 6e0:	eb 01                	jmp    6e3 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 6e2:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 6e3:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6e6:	48 63 d0             	movslq %eax,%rdx
 6e9:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6ed:	48 01 d0             	add    %rdx,%rax
 6f0:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 6f3:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 6f7:	c9                   	leaveq 
 6f8:	c3                   	retq   

00000000000006f9 <stat>:

int stat(char *n, struct stat *st) {
 6f9:	55                   	push   %rbp
 6fa:	48 89 e5             	mov    %rsp,%rbp
 6fd:	48 83 ec 20          	sub    $0x20,%rsp
 701:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 705:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 709:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 70d:	be 00 00 00 00       	mov    $0x0,%esi
 712:	48 89 c7             	mov    %rax,%rdi
 715:	e8 20 01 00 00       	callq  83a <open>
 71a:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 71d:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 721:	79 07                	jns    72a <stat+0x31>
    return -1;
 723:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 728:	eb 21                	jmp    74b <stat+0x52>
  r = fstat(fd, st);
 72a:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 72e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 731:	48 89 d6             	mov    %rdx,%rsi
 734:	89 c7                	mov    %eax,%edi
 736:	e8 17 01 00 00       	callq  852 <fstat>
 73b:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 73e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 741:	89 c7                	mov    %eax,%edi
 743:	e8 da 00 00 00       	callq  822 <close>
  return r;
 748:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 74b:	c9                   	leaveq 
 74c:	c3                   	retq   

000000000000074d <atoi>:

int atoi(const char *s) {
 74d:	55                   	push   %rbp
 74e:	48 89 e5             	mov    %rsp,%rbp
 751:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 755:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 75c:	eb 28                	jmp    786 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 75e:	8b 55 fc             	mov    -0x4(%rbp),%edx
 761:	89 d0                	mov    %edx,%eax
 763:	c1 e0 02             	shl    $0x2,%eax
 766:	01 d0                	add    %edx,%eax
 768:	01 c0                	add    %eax,%eax
 76a:	89 c1                	mov    %eax,%ecx
 76c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 770:	48 8d 50 01          	lea    0x1(%rax),%rdx
 774:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 778:	0f b6 00             	movzbl (%rax),%eax
 77b:	0f be c0             	movsbl %al,%eax
 77e:	01 c8                	add    %ecx,%eax
 780:	83 e8 30             	sub    $0x30,%eax
 783:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 786:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 78a:	0f b6 00             	movzbl (%rax),%eax
 78d:	3c 2f                	cmp    $0x2f,%al
 78f:	7e 0b                	jle    79c <atoi+0x4f>
 791:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 795:	0f b6 00             	movzbl (%rax),%eax
 798:	3c 39                	cmp    $0x39,%al
 79a:	7e c2                	jle    75e <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 79c:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 79f:	5d                   	pop    %rbp
 7a0:	c3                   	retq   

00000000000007a1 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 7a1:	55                   	push   %rbp
 7a2:	48 89 e5             	mov    %rsp,%rbp
 7a5:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 7a9:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 7ad:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 7b0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7b4:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 7b8:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 7bc:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 7c0:	eb 1d                	jmp    7df <memmove+0x3e>
    *dst++ = *src++;
 7c2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 7c6:	48 8d 50 01          	lea    0x1(%rax),%rdx
 7ca:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 7ce:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 7d2:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 7d6:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 7da:	0f b6 12             	movzbl (%rdx),%edx
 7dd:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 7df:	8b 45 dc             	mov    -0x24(%rbp),%eax
 7e2:	8d 50 ff             	lea    -0x1(%rax),%edx
 7e5:	89 55 dc             	mov    %edx,-0x24(%rbp)
 7e8:	85 c0                	test   %eax,%eax
 7ea:	7f d6                	jg     7c2 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 7ec:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7f0:	5d                   	pop    %rbp
 7f1:	c3                   	retq   

00000000000007f2 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 7f2:	b8 01 00 00 00       	mov    $0x1,%eax
 7f7:	cd 40                	int    $0x40
 7f9:	c3                   	retq   

00000000000007fa <exit>:
SYSCALL(exit)
 7fa:	b8 02 00 00 00       	mov    $0x2,%eax
 7ff:	cd 40                	int    $0x40
 801:	c3                   	retq   

0000000000000802 <wait>:
SYSCALL(wait)
 802:	b8 03 00 00 00       	mov    $0x3,%eax
 807:	cd 40                	int    $0x40
 809:	c3                   	retq   

000000000000080a <pipe>:
SYSCALL(pipe)
 80a:	b8 04 00 00 00       	mov    $0x4,%eax
 80f:	cd 40                	int    $0x40
 811:	c3                   	retq   

0000000000000812 <read>:
SYSCALL(read)
 812:	b8 05 00 00 00       	mov    $0x5,%eax
 817:	cd 40                	int    $0x40
 819:	c3                   	retq   

000000000000081a <write>:
SYSCALL(write)
 81a:	b8 10 00 00 00       	mov    $0x10,%eax
 81f:	cd 40                	int    $0x40
 821:	c3                   	retq   

0000000000000822 <close>:
SYSCALL(close)
 822:	b8 15 00 00 00       	mov    $0x15,%eax
 827:	cd 40                	int    $0x40
 829:	c3                   	retq   

000000000000082a <kill>:
SYSCALL(kill)
 82a:	b8 06 00 00 00       	mov    $0x6,%eax
 82f:	cd 40                	int    $0x40
 831:	c3                   	retq   

0000000000000832 <exec>:
SYSCALL(exec)
 832:	b8 07 00 00 00       	mov    $0x7,%eax
 837:	cd 40                	int    $0x40
 839:	c3                   	retq   

000000000000083a <open>:
SYSCALL(open)
 83a:	b8 0f 00 00 00       	mov    $0xf,%eax
 83f:	cd 40                	int    $0x40
 841:	c3                   	retq   

0000000000000842 <mknod>:
SYSCALL(mknod)
 842:	b8 11 00 00 00       	mov    $0x11,%eax
 847:	cd 40                	int    $0x40
 849:	c3                   	retq   

000000000000084a <unlink>:
SYSCALL(unlink)
 84a:	b8 12 00 00 00       	mov    $0x12,%eax
 84f:	cd 40                	int    $0x40
 851:	c3                   	retq   

0000000000000852 <fstat>:
SYSCALL(fstat)
 852:	b8 08 00 00 00       	mov    $0x8,%eax
 857:	cd 40                	int    $0x40
 859:	c3                   	retq   

000000000000085a <link>:
SYSCALL(link)
 85a:	b8 13 00 00 00       	mov    $0x13,%eax
 85f:	cd 40                	int    $0x40
 861:	c3                   	retq   

0000000000000862 <mkdir>:
SYSCALL(mkdir)
 862:	b8 14 00 00 00       	mov    $0x14,%eax
 867:	cd 40                	int    $0x40
 869:	c3                   	retq   

000000000000086a <chdir>:
SYSCALL(chdir)
 86a:	b8 09 00 00 00       	mov    $0x9,%eax
 86f:	cd 40                	int    $0x40
 871:	c3                   	retq   

0000000000000872 <dup>:
SYSCALL(dup)
 872:	b8 0a 00 00 00       	mov    $0xa,%eax
 877:	cd 40                	int    $0x40
 879:	c3                   	retq   

000000000000087a <getpid>:
SYSCALL(getpid)
 87a:	b8 0b 00 00 00       	mov    $0xb,%eax
 87f:	cd 40                	int    $0x40
 881:	c3                   	retq   

0000000000000882 <sbrk>:
SYSCALL(sbrk)
 882:	b8 0c 00 00 00       	mov    $0xc,%eax
 887:	cd 40                	int    $0x40
 889:	c3                   	retq   

000000000000088a <sleep>:
SYSCALL(sleep)
 88a:	b8 0d 00 00 00       	mov    $0xd,%eax
 88f:	cd 40                	int    $0x40
 891:	c3                   	retq   

0000000000000892 <uptime>:
SYSCALL(uptime)
 892:	b8 0e 00 00 00       	mov    $0xe,%eax
 897:	cd 40                	int    $0x40
 899:	c3                   	retq   

000000000000089a <sysinfo>:
SYSCALL(sysinfo)
 89a:	b8 16 00 00 00       	mov    $0x16,%eax
 89f:	cd 40                	int    $0x40
 8a1:	c3                   	retq   

00000000000008a2 <crashn>:
SYSCALL(crashn)
 8a2:	b8 17 00 00 00       	mov    $0x17,%eax
 8a7:	cd 40                	int    $0x40
 8a9:	c3                   	retq   

00000000000008aa <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 8aa:	55                   	push   %rbp
 8ab:	48 89 e5             	mov    %rsp,%rbp
 8ae:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 8b2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8b6:	48 83 e8 10          	sub    $0x10,%rax
 8ba:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8be:	48 8b 05 9b 05 00 00 	mov    0x59b(%rip),%rax        # e60 <freep>
 8c5:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8c9:	eb 2f                	jmp    8fa <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 8cb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8cf:	48 8b 00             	mov    (%rax),%rax
 8d2:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8d6:	77 17                	ja     8ef <free+0x45>
 8d8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8dc:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8e0:	77 2f                	ja     911 <free+0x67>
 8e2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8e6:	48 8b 00             	mov    (%rax),%rax
 8e9:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8ed:	77 22                	ja     911 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8ef:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8f3:	48 8b 00             	mov    (%rax),%rax
 8f6:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8fa:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8fe:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 902:	76 c7                	jbe    8cb <free+0x21>
 904:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 908:	48 8b 00             	mov    (%rax),%rax
 90b:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 90f:	76 ba                	jbe    8cb <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 911:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 915:	8b 40 08             	mov    0x8(%rax),%eax
 918:	89 c0                	mov    %eax,%eax
 91a:	48 c1 e0 04          	shl    $0x4,%rax
 91e:	48 89 c2             	mov    %rax,%rdx
 921:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 925:	48 01 c2             	add    %rax,%rdx
 928:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 92c:	48 8b 00             	mov    (%rax),%rax
 92f:	48 39 c2             	cmp    %rax,%rdx
 932:	75 2d                	jne    961 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 934:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 938:	8b 50 08             	mov    0x8(%rax),%edx
 93b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 93f:	48 8b 00             	mov    (%rax),%rax
 942:	8b 40 08             	mov    0x8(%rax),%eax
 945:	01 c2                	add    %eax,%edx
 947:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 94b:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 94e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 952:	48 8b 00             	mov    (%rax),%rax
 955:	48 8b 10             	mov    (%rax),%rdx
 958:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 95c:	48 89 10             	mov    %rdx,(%rax)
 95f:	eb 0e                	jmp    96f <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 961:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 965:	48 8b 10             	mov    (%rax),%rdx
 968:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 96c:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 96f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 973:	8b 40 08             	mov    0x8(%rax),%eax
 976:	89 c0                	mov    %eax,%eax
 978:	48 c1 e0 04          	shl    $0x4,%rax
 97c:	48 89 c2             	mov    %rax,%rdx
 97f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 983:	48 01 d0             	add    %rdx,%rax
 986:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 98a:	75 27                	jne    9b3 <free+0x109>
    p->s.size += bp->s.size;
 98c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 990:	8b 50 08             	mov    0x8(%rax),%edx
 993:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 997:	8b 40 08             	mov    0x8(%rax),%eax
 99a:	01 c2                	add    %eax,%edx
 99c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9a0:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 9a3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9a7:	48 8b 10             	mov    (%rax),%rdx
 9aa:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9ae:	48 89 10             	mov    %rdx,(%rax)
 9b1:	eb 0b                	jmp    9be <free+0x114>
  } else
    p->s.ptr = bp;
 9b3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9b7:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 9bb:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 9be:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9c2:	48 89 05 97 04 00 00 	mov    %rax,0x497(%rip)        # e60 <freep>
}
 9c9:	90                   	nop
 9ca:	5d                   	pop    %rbp
 9cb:	c3                   	retq   

00000000000009cc <morecore>:

static Header *morecore(uint nu) {
 9cc:	55                   	push   %rbp
 9cd:	48 89 e5             	mov    %rsp,%rbp
 9d0:	48 83 ec 20          	sub    $0x20,%rsp
 9d4:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 9d7:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 9de:	77 07                	ja     9e7 <morecore+0x1b>
    nu = 4096;
 9e0:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 9e7:	8b 45 ec             	mov    -0x14(%rbp),%eax
 9ea:	c1 e0 04             	shl    $0x4,%eax
 9ed:	89 c7                	mov    %eax,%edi
 9ef:	e8 8e fe ff ff       	callq  882 <sbrk>
 9f4:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 9f8:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 9fd:	75 07                	jne    a06 <morecore+0x3a>
    return 0;
 9ff:	b8 00 00 00 00       	mov    $0x0,%eax
 a04:	eb 29                	jmp    a2f <morecore+0x63>
  hp = (Header *)p;
 a06:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a0a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 a0e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a12:	8b 55 ec             	mov    -0x14(%rbp),%edx
 a15:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 a18:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a1c:	48 83 c0 10          	add    $0x10,%rax
 a20:	48 89 c7             	mov    %rax,%rdi
 a23:	e8 82 fe ff ff       	callq  8aa <free>
  return freep;
 a28:	48 8b 05 31 04 00 00 	mov    0x431(%rip),%rax        # e60 <freep>
}
 a2f:	c9                   	leaveq 
 a30:	c3                   	retq   

0000000000000a31 <malloc>:

void *malloc(uint nbytes) {
 a31:	55                   	push   %rbp
 a32:	48 89 e5             	mov    %rsp,%rbp
 a35:	48 83 ec 30          	sub    $0x30,%rsp
 a39:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a3c:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a3f:	48 83 c0 0f          	add    $0xf,%rax
 a43:	48 c1 e8 04          	shr    $0x4,%rax
 a47:	83 c0 01             	add    $0x1,%eax
 a4a:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a4d:	48 8b 05 0c 04 00 00 	mov    0x40c(%rip),%rax        # e60 <freep>
 a54:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 a58:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 a5d:	75 2b                	jne    a8a <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 a5f:	48 c7 45 f0 50 0e 00 	movq   $0xe50,-0x10(%rbp)
 a66:	00 
 a67:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a6b:	48 89 05 ee 03 00 00 	mov    %rax,0x3ee(%rip)        # e60 <freep>
 a72:	48 8b 05 e7 03 00 00 	mov    0x3e7(%rip),%rax        # e60 <freep>
 a79:	48 89 05 d0 03 00 00 	mov    %rax,0x3d0(%rip)        # e50 <base>
    base.s.size = 0;
 a80:	c7 05 ce 03 00 00 00 	movl   $0x0,0x3ce(%rip)        # e58 <base+0x8>
 a87:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a8a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a8e:	48 8b 00             	mov    (%rax),%rax
 a91:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a95:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a99:	8b 40 08             	mov    0x8(%rax),%eax
 a9c:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a9f:	72 5f                	jb     b00 <malloc+0xcf>
      if (p->s.size == nunits)
 aa1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa5:	8b 40 08             	mov    0x8(%rax),%eax
 aa8:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 aab:	75 10                	jne    abd <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 aad:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ab1:	48 8b 10             	mov    (%rax),%rdx
 ab4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ab8:	48 89 10             	mov    %rdx,(%rax)
 abb:	eb 2e                	jmp    aeb <malloc+0xba>
      else {
        p->s.size -= nunits;
 abd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ac1:	8b 40 08             	mov    0x8(%rax),%eax
 ac4:	2b 45 ec             	sub    -0x14(%rbp),%eax
 ac7:	89 c2                	mov    %eax,%edx
 ac9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 acd:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 ad0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ad4:	8b 40 08             	mov    0x8(%rax),%eax
 ad7:	89 c0                	mov    %eax,%eax
 ad9:	48 c1 e0 04          	shl    $0x4,%rax
 add:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 ae1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ae5:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ae8:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 aeb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 aef:	48 89 05 6a 03 00 00 	mov    %rax,0x36a(%rip)        # e60 <freep>
      return (void *)(p + 1);
 af6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 afa:	48 83 c0 10          	add    $0x10,%rax
 afe:	eb 41                	jmp    b41 <malloc+0x110>
    }
    if (p == freep)
 b00:	48 8b 05 59 03 00 00 	mov    0x359(%rip),%rax        # e60 <freep>
 b07:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 b0b:	75 1c                	jne    b29 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 b0d:	8b 45 ec             	mov    -0x14(%rbp),%eax
 b10:	89 c7                	mov    %eax,%edi
 b12:	e8 b5 fe ff ff       	callq  9cc <morecore>
 b17:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 b1b:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 b20:	75 07                	jne    b29 <malloc+0xf8>
        return 0;
 b22:	b8 00 00 00 00       	mov    $0x0,%eax
 b27:	eb 18                	jmp    b41 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b29:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b2d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b31:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b35:	48 8b 00             	mov    (%rax),%rax
 b38:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b3c:	e9 54 ff ff ff       	jmpq   a95 <malloc+0x64>
 b41:	c9                   	leaveq 
 b42:	c3                   	retq   
