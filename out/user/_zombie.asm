
out/user/_zombie:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:

#include <cdefs.h>
#include <stat.h>
#include <user.h>

int main(void) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
  if (fork() > 0)
   4:	e8 68 07 00 00       	callq  771 <fork>
   9:	85 c0                	test   %eax,%eax
   b:	7e 0a                	jle    17 <main+0x17>
    sleep(5); // Let child exit before parent.
   d:	bf 05 00 00 00       	mov    $0x5,%edi
  12:	e8 f2 07 00 00       	callq  809 <sleep>
  exit();
  17:	e8 5d 07 00 00       	callq  779 <exit>

000000000000001c <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  1c:	55                   	push   %rbp
  1d:	48 89 e5             	mov    %rsp,%rbp
  20:	48 83 ec 10          	sub    $0x10,%rsp
  24:	89 7d fc             	mov    %edi,-0x4(%rbp)
  27:	89 f0                	mov    %esi,%eax
  29:	88 45 f8             	mov    %al,-0x8(%rbp)
  2c:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  30:	8b 45 fc             	mov    -0x4(%rbp),%eax
  33:	ba 01 00 00 00       	mov    $0x1,%edx
  38:	48 89 ce             	mov    %rcx,%rsi
  3b:	89 c7                	mov    %eax,%edi
  3d:	e8 57 07 00 00       	callq  799 <write>
  42:	90                   	nop
  43:	c9                   	leaveq 
  44:	c3                   	retq   

0000000000000045 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  45:	55                   	push   %rbp
  46:	48 89 e5             	mov    %rsp,%rbp
  49:	48 83 ec 40          	sub    $0x40,%rsp
  4d:	89 7d cc             	mov    %edi,-0x34(%rbp)
  50:	89 75 c8             	mov    %esi,-0x38(%rbp)
  53:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  56:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  59:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  5d:	74 1f                	je     7e <printint64+0x39>
  5f:	8b 45 c8             	mov    -0x38(%rbp),%eax
  62:	c1 e8 1f             	shr    $0x1f,%eax
  65:	0f b6 c0             	movzbl %al,%eax
  68:	89 45 c0             	mov    %eax,-0x40(%rbp)
  6b:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  6f:	74 0d                	je     7e <printint64+0x39>
    x = -xx;
  71:	8b 45 c8             	mov    -0x38(%rbp),%eax
  74:	f7 d8                	neg    %eax
  76:	48 98                	cltq   
  78:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  7c:	eb 09                	jmp    87 <printint64+0x42>
  else
    x = xx;
  7e:	8b 45 c8             	mov    -0x38(%rbp),%eax
  81:	48 98                	cltq   
  83:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
  87:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
  8e:	8b 4d fc             	mov    -0x4(%rbp),%ecx
  91:	8d 41 01             	lea    0x1(%rcx),%eax
  94:	89 45 fc             	mov    %eax,-0x4(%rbp)
  97:	8b 45 c4             	mov    -0x3c(%rbp),%eax
  9a:	48 63 f0             	movslq %eax,%rsi
  9d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  a1:	ba 00 00 00 00       	mov    $0x0,%edx
  a6:	48 f7 f6             	div    %rsi
  a9:	48 89 d0             	mov    %rdx,%rax
  ac:	0f b6 90 30 0d 00 00 	movzbl 0xd30(%rax),%edx
  b3:	48 63 c1             	movslq %ecx,%rax
  b6:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
  ba:	8b 45 c4             	mov    -0x3c(%rbp),%eax
  bd:	48 63 f8             	movslq %eax,%rdi
  c0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  c4:	ba 00 00 00 00       	mov    $0x0,%edx
  c9:	48 f7 f7             	div    %rdi
  cc:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  d0:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
  d5:	75 b7                	jne    8e <printint64+0x49>

  if (sgn)
  d7:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  db:	74 2b                	je     108 <printint64+0xc3>
    buf[i++] = '-';
  dd:	8b 45 fc             	mov    -0x4(%rbp),%eax
  e0:	8d 50 01             	lea    0x1(%rax),%edx
  e3:	89 55 fc             	mov    %edx,-0x4(%rbp)
  e6:	48 98                	cltq   
  e8:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
  ed:	eb 19                	jmp    108 <printint64+0xc3>
    putc(fd, buf[i]);
  ef:	8b 45 fc             	mov    -0x4(%rbp),%eax
  f2:	48 98                	cltq   
  f4:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
  f9:	0f be d0             	movsbl %al,%edx
  fc:	8b 45 cc             	mov    -0x34(%rbp),%eax
  ff:	89 d6                	mov    %edx,%esi
 101:	89 c7                	mov    %eax,%edi
 103:	e8 14 ff ff ff       	callq  1c <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 108:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 10c:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 110:	79 dd                	jns    ef <printint64+0xaa>
    putc(fd, buf[i]);
}
 112:	90                   	nop
 113:	c9                   	leaveq 
 114:	c3                   	retq   

0000000000000115 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 115:	55                   	push   %rbp
 116:	48 89 e5             	mov    %rsp,%rbp
 119:	48 83 ec 30          	sub    $0x30,%rsp
 11d:	89 7d dc             	mov    %edi,-0x24(%rbp)
 120:	89 75 d8             	mov    %esi,-0x28(%rbp)
 123:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 126:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 129:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 130:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 134:	74 17                	je     14d <printint+0x38>
 136:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 13a:	79 11                	jns    14d <printint+0x38>
    neg = 1;
 13c:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 143:	8b 45 d8             	mov    -0x28(%rbp),%eax
 146:	f7 d8                	neg    %eax
 148:	89 45 f4             	mov    %eax,-0xc(%rbp)
 14b:	eb 06                	jmp    153 <printint+0x3e>
  } else {
    x = xx;
 14d:	8b 45 d8             	mov    -0x28(%rbp),%eax
 150:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 153:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 15a:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 15d:	8d 41 01             	lea    0x1(%rcx),%eax
 160:	89 45 fc             	mov    %eax,-0x4(%rbp)
 163:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 166:	8b 45 f4             	mov    -0xc(%rbp),%eax
 169:	ba 00 00 00 00       	mov    $0x0,%edx
 16e:	f7 f6                	div    %esi
 170:	89 d0                	mov    %edx,%eax
 172:	89 c0                	mov    %eax,%eax
 174:	0f b6 90 50 0d 00 00 	movzbl 0xd50(%rax),%edx
 17b:	48 63 c1             	movslq %ecx,%rax
 17e:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 182:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 185:	8b 45 f4             	mov    -0xc(%rbp),%eax
 188:	ba 00 00 00 00       	mov    $0x0,%edx
 18d:	f7 f7                	div    %edi
 18f:	89 45 f4             	mov    %eax,-0xc(%rbp)
 192:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 196:	75 c2                	jne    15a <printint+0x45>
  if (neg)
 198:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 19c:	74 2b                	je     1c9 <printint+0xb4>
    buf[i++] = '-';
 19e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1a1:	8d 50 01             	lea    0x1(%rax),%edx
 1a4:	89 55 fc             	mov    %edx,-0x4(%rbp)
 1a7:	48 98                	cltq   
 1a9:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 1ae:	eb 19                	jmp    1c9 <printint+0xb4>
    putc(fd, buf[i]);
 1b0:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1b3:	48 98                	cltq   
 1b5:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 1ba:	0f be d0             	movsbl %al,%edx
 1bd:	8b 45 dc             	mov    -0x24(%rbp),%eax
 1c0:	89 d6                	mov    %edx,%esi
 1c2:	89 c7                	mov    %eax,%edi
 1c4:	e8 53 fe ff ff       	callq  1c <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 1c9:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 1cd:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 1d1:	79 dd                	jns    1b0 <printint+0x9b>
    putc(fd, buf[i]);
}
 1d3:	90                   	nop
 1d4:	c9                   	leaveq 
 1d5:	c3                   	retq   

00000000000001d6 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 1d6:	55                   	push   %rbp
 1d7:	48 89 e5             	mov    %rsp,%rbp
 1da:	48 83 ec 70          	sub    $0x70,%rsp
 1de:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 1e1:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 1e5:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 1e9:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 1ed:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 1f1:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 1f5:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 1fc:	48 8d 45 10          	lea    0x10(%rbp),%rax
 200:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 204:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 208:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 20c:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 213:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 21a:	e9 68 02 00 00       	jmpq   487 <printf+0x2b1>
    c = fmt[i] & 0xff;
 21f:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 222:	48 63 d0             	movslq %eax,%rdx
 225:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 229:	48 01 d0             	add    %rdx,%rax
 22c:	0f b6 00             	movzbl (%rax),%eax
 22f:	0f be c0             	movsbl %al,%eax
 232:	25 ff 00 00 00       	and    $0xff,%eax
 237:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 23a:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 23e:	75 30                	jne    270 <printf+0x9a>
      if (c == '%') {
 240:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 244:	75 13                	jne    259 <printf+0x83>
        state = '%';
 246:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 24d:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 254:	e9 2a 02 00 00       	jmpq   483 <printf+0x2ad>
      } else {
        putc(fd, c);
 259:	8b 45 b8             	mov    -0x48(%rbp),%eax
 25c:	0f be d0             	movsbl %al,%edx
 25f:	8b 45 9c             	mov    -0x64(%rbp),%eax
 262:	89 d6                	mov    %edx,%esi
 264:	89 c7                	mov    %eax,%edi
 266:	e8 b1 fd ff ff       	callq  1c <putc>
 26b:	e9 13 02 00 00       	jmpq   483 <printf+0x2ad>
      }
    } else if (state == '%') {
 270:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 274:	0f 85 09 02 00 00    	jne    483 <printf+0x2ad>
      if (c == 'l') {
 27a:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 27e:	75 0c                	jne    28c <printf+0xb6>
        lflag = 1;
 280:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 287:	e9 f7 01 00 00       	jmpq   483 <printf+0x2ad>
      } else if (c == 'd') {
 28c:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 290:	0f 85 95 00 00 00    	jne    32b <printf+0x155>
        if (lflag == 1)
 296:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 29a:	75 49                	jne    2e5 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 29c:	8b 45 a0             	mov    -0x60(%rbp),%eax
 29f:	83 f8 30             	cmp    $0x30,%eax
 2a2:	73 17                	jae    2bb <printf+0xe5>
 2a4:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 2a8:	8b 55 a0             	mov    -0x60(%rbp),%edx
 2ab:	89 d2                	mov    %edx,%edx
 2ad:	48 01 d0             	add    %rdx,%rax
 2b0:	8b 55 a0             	mov    -0x60(%rbp),%edx
 2b3:	83 c2 08             	add    $0x8,%edx
 2b6:	89 55 a0             	mov    %edx,-0x60(%rbp)
 2b9:	eb 0c                	jmp    2c7 <printf+0xf1>
 2bb:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 2bf:	48 8d 50 08          	lea    0x8(%rax),%rdx
 2c3:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 2c7:	48 8b 00             	mov    (%rax),%rax
 2ca:	89 c6                	mov    %eax,%esi
 2cc:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2cf:	b9 01 00 00 00       	mov    $0x1,%ecx
 2d4:	ba 0a 00 00 00       	mov    $0xa,%edx
 2d9:	89 c7                	mov    %eax,%edi
 2db:	e8 65 fd ff ff       	callq  45 <printint64>
 2e0:	e9 97 01 00 00       	jmpq   47c <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 2e5:	8b 45 a0             	mov    -0x60(%rbp),%eax
 2e8:	83 f8 30             	cmp    $0x30,%eax
 2eb:	73 17                	jae    304 <printf+0x12e>
 2ed:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 2f1:	8b 55 a0             	mov    -0x60(%rbp),%edx
 2f4:	89 d2                	mov    %edx,%edx
 2f6:	48 01 d0             	add    %rdx,%rax
 2f9:	8b 55 a0             	mov    -0x60(%rbp),%edx
 2fc:	83 c2 08             	add    $0x8,%edx
 2ff:	89 55 a0             	mov    %edx,-0x60(%rbp)
 302:	eb 0c                	jmp    310 <printf+0x13a>
 304:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 308:	48 8d 50 08          	lea    0x8(%rax),%rdx
 30c:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 310:	8b 30                	mov    (%rax),%esi
 312:	8b 45 9c             	mov    -0x64(%rbp),%eax
 315:	b9 01 00 00 00       	mov    $0x1,%ecx
 31a:	ba 0a 00 00 00       	mov    $0xa,%edx
 31f:	89 c7                	mov    %eax,%edi
 321:	e8 ef fd ff ff       	callq  115 <printint>
 326:	e9 51 01 00 00       	jmpq   47c <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 32b:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 32f:	74 0a                	je     33b <printf+0x165>
 331:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 335:	0f 85 95 00 00 00    	jne    3d0 <printf+0x1fa>
        if (lflag == 1)
 33b:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 33f:	75 49                	jne    38a <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 341:	8b 45 a0             	mov    -0x60(%rbp),%eax
 344:	83 f8 30             	cmp    $0x30,%eax
 347:	73 17                	jae    360 <printf+0x18a>
 349:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 34d:	8b 55 a0             	mov    -0x60(%rbp),%edx
 350:	89 d2                	mov    %edx,%edx
 352:	48 01 d0             	add    %rdx,%rax
 355:	8b 55 a0             	mov    -0x60(%rbp),%edx
 358:	83 c2 08             	add    $0x8,%edx
 35b:	89 55 a0             	mov    %edx,-0x60(%rbp)
 35e:	eb 0c                	jmp    36c <printf+0x196>
 360:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 364:	48 8d 50 08          	lea    0x8(%rax),%rdx
 368:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 36c:	48 8b 00             	mov    (%rax),%rax
 36f:	89 c6                	mov    %eax,%esi
 371:	8b 45 9c             	mov    -0x64(%rbp),%eax
 374:	b9 00 00 00 00       	mov    $0x0,%ecx
 379:	ba 10 00 00 00       	mov    $0x10,%edx
 37e:	89 c7                	mov    %eax,%edi
 380:	e8 c0 fc ff ff       	callq  45 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 385:	e9 f2 00 00 00       	jmpq   47c <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 38a:	8b 45 a0             	mov    -0x60(%rbp),%eax
 38d:	83 f8 30             	cmp    $0x30,%eax
 390:	73 17                	jae    3a9 <printf+0x1d3>
 392:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 396:	8b 55 a0             	mov    -0x60(%rbp),%edx
 399:	89 d2                	mov    %edx,%edx
 39b:	48 01 d0             	add    %rdx,%rax
 39e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3a1:	83 c2 08             	add    $0x8,%edx
 3a4:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3a7:	eb 0c                	jmp    3b5 <printf+0x1df>
 3a9:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3ad:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3b1:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3b5:	8b 30                	mov    (%rax),%esi
 3b7:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3ba:	b9 00 00 00 00       	mov    $0x0,%ecx
 3bf:	ba 10 00 00 00       	mov    $0x10,%edx
 3c4:	89 c7                	mov    %eax,%edi
 3c6:	e8 4a fd ff ff       	callq  115 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 3cb:	e9 ac 00 00 00       	jmpq   47c <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 3d0:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 3d4:	75 6b                	jne    441 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 3d6:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3d9:	83 f8 30             	cmp    $0x30,%eax
 3dc:	73 17                	jae    3f5 <printf+0x21f>
 3de:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3e2:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3e5:	89 d2                	mov    %edx,%edx
 3e7:	48 01 d0             	add    %rdx,%rax
 3ea:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3ed:	83 c2 08             	add    $0x8,%edx
 3f0:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3f3:	eb 0c                	jmp    401 <printf+0x22b>
 3f5:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3f9:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3fd:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 401:	48 8b 00             	mov    (%rax),%rax
 404:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 408:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 40d:	75 25                	jne    434 <printf+0x25e>
          s = "(null)";
 40f:	48 c7 45 c8 c2 0a 00 	movq   $0xac2,-0x38(%rbp)
 416:	00 
        for (; *s; s++)
 417:	eb 1b                	jmp    434 <printf+0x25e>
          putc(fd, *s);
 419:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 41d:	0f b6 00             	movzbl (%rax),%eax
 420:	0f be d0             	movsbl %al,%edx
 423:	8b 45 9c             	mov    -0x64(%rbp),%eax
 426:	89 d6                	mov    %edx,%esi
 428:	89 c7                	mov    %eax,%edi
 42a:	e8 ed fb ff ff       	callq  1c <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 42f:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 434:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 438:	0f b6 00             	movzbl (%rax),%eax
 43b:	84 c0                	test   %al,%al
 43d:	75 da                	jne    419 <printf+0x243>
 43f:	eb 3b                	jmp    47c <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 441:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 445:	75 14                	jne    45b <printf+0x285>
        putc(fd, c);
 447:	8b 45 b8             	mov    -0x48(%rbp),%eax
 44a:	0f be d0             	movsbl %al,%edx
 44d:	8b 45 9c             	mov    -0x64(%rbp),%eax
 450:	89 d6                	mov    %edx,%esi
 452:	89 c7                	mov    %eax,%edi
 454:	e8 c3 fb ff ff       	callq  1c <putc>
 459:	eb 21                	jmp    47c <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 45b:	8b 45 9c             	mov    -0x64(%rbp),%eax
 45e:	be 25 00 00 00       	mov    $0x25,%esi
 463:	89 c7                	mov    %eax,%edi
 465:	e8 b2 fb ff ff       	callq  1c <putc>
        putc(fd, c);
 46a:	8b 45 b8             	mov    -0x48(%rbp),%eax
 46d:	0f be d0             	movsbl %al,%edx
 470:	8b 45 9c             	mov    -0x64(%rbp),%eax
 473:	89 d6                	mov    %edx,%esi
 475:	89 c7                	mov    %eax,%edi
 477:	e8 a0 fb ff ff       	callq  1c <putc>
      }
      state = 0;
 47c:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 483:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 487:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 48a:	48 63 d0             	movslq %eax,%rdx
 48d:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 491:	48 01 d0             	add    %rdx,%rax
 494:	0f b6 00             	movzbl (%rax),%eax
 497:	84 c0                	test   %al,%al
 499:	0f 85 80 fd ff ff    	jne    21f <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 49f:	90                   	nop
 4a0:	c9                   	leaveq 
 4a1:	c3                   	retq   

00000000000004a2 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 4a2:	55                   	push   %rbp
 4a3:	48 89 e5             	mov    %rsp,%rbp
 4a6:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 4aa:	89 75 f4             	mov    %esi,-0xc(%rbp)
 4ad:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 4b0:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 4b4:	8b 55 f0             	mov    -0x10(%rbp),%edx
 4b7:	8b 45 f4             	mov    -0xc(%rbp),%eax
 4ba:	48 89 ce             	mov    %rcx,%rsi
 4bd:	48 89 f7             	mov    %rsi,%rdi
 4c0:	89 d1                	mov    %edx,%ecx
 4c2:	fc                   	cld    
 4c3:	f3 aa                	rep stos %al,%es:(%rdi)
 4c5:	89 ca                	mov    %ecx,%edx
 4c7:	48 89 fe             	mov    %rdi,%rsi
 4ca:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 4ce:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 4d1:	90                   	nop
 4d2:	5d                   	pop    %rbp
 4d3:	c3                   	retq   

00000000000004d4 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 4d4:	55                   	push   %rbp
 4d5:	48 89 e5             	mov    %rsp,%rbp
 4d8:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 4dc:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 4e0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 4e4:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 4e8:	90                   	nop
 4e9:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 4ed:	48 8d 50 01          	lea    0x1(%rax),%rdx
 4f1:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 4f5:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 4f9:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 4fd:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 501:	0f b6 12             	movzbl (%rdx),%edx
 504:	88 10                	mov    %dl,(%rax)
 506:	0f b6 00             	movzbl (%rax),%eax
 509:	84 c0                	test   %al,%al
 50b:	75 dc                	jne    4e9 <strcpy+0x15>
    ;
  return os;
 50d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 511:	5d                   	pop    %rbp
 512:	c3                   	retq   

0000000000000513 <strcmp>:

int strcmp(const char *p, const char *q) {
 513:	55                   	push   %rbp
 514:	48 89 e5             	mov    %rsp,%rbp
 517:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 51b:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 51f:	eb 0a                	jmp    52b <strcmp+0x18>
    p++, q++;
 521:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 526:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 52b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 52f:	0f b6 00             	movzbl (%rax),%eax
 532:	84 c0                	test   %al,%al
 534:	74 12                	je     548 <strcmp+0x35>
 536:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 53a:	0f b6 10             	movzbl (%rax),%edx
 53d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 541:	0f b6 00             	movzbl (%rax),%eax
 544:	38 c2                	cmp    %al,%dl
 546:	74 d9                	je     521 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 548:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 54c:	0f b6 00             	movzbl (%rax),%eax
 54f:	0f b6 d0             	movzbl %al,%edx
 552:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 556:	0f b6 00             	movzbl (%rax),%eax
 559:	0f b6 c0             	movzbl %al,%eax
 55c:	29 c2                	sub    %eax,%edx
 55e:	89 d0                	mov    %edx,%eax
}
 560:	5d                   	pop    %rbp
 561:	c3                   	retq   

0000000000000562 <strlen>:

uint strlen(char *s) {
 562:	55                   	push   %rbp
 563:	48 89 e5             	mov    %rsp,%rbp
 566:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 56a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 571:	eb 04                	jmp    577 <strlen+0x15>
 573:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 577:	8b 45 fc             	mov    -0x4(%rbp),%eax
 57a:	48 63 d0             	movslq %eax,%rdx
 57d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 581:	48 01 d0             	add    %rdx,%rax
 584:	0f b6 00             	movzbl (%rax),%eax
 587:	84 c0                	test   %al,%al
 589:	75 e8                	jne    573 <strlen+0x11>
    ;
  return n;
 58b:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 58e:	5d                   	pop    %rbp
 58f:	c3                   	retq   

0000000000000590 <memset>:

void *memset(void *dst, int c, uint n) {
 590:	55                   	push   %rbp
 591:	48 89 e5             	mov    %rsp,%rbp
 594:	48 83 ec 10          	sub    $0x10,%rsp
 598:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 59c:	89 75 f4             	mov    %esi,-0xc(%rbp)
 59f:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 5a2:	8b 55 f0             	mov    -0x10(%rbp),%edx
 5a5:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 5a8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5ac:	89 ce                	mov    %ecx,%esi
 5ae:	48 89 c7             	mov    %rax,%rdi
 5b1:	e8 ec fe ff ff       	callq  4a2 <stosb>
  return dst;
 5b6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 5ba:	c9                   	leaveq 
 5bb:	c3                   	retq   

00000000000005bc <strchr>:

char *strchr(const char *s, char c) {
 5bc:	55                   	push   %rbp
 5bd:	48 89 e5             	mov    %rsp,%rbp
 5c0:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5c4:	89 f0                	mov    %esi,%eax
 5c6:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 5c9:	eb 17                	jmp    5e2 <strchr+0x26>
    if (*s == c)
 5cb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5cf:	0f b6 00             	movzbl (%rax),%eax
 5d2:	3a 45 f4             	cmp    -0xc(%rbp),%al
 5d5:	75 06                	jne    5dd <strchr+0x21>
      return (char *)s;
 5d7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5db:	eb 15                	jmp    5f2 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 5dd:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 5e2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5e6:	0f b6 00             	movzbl (%rax),%eax
 5e9:	84 c0                	test   %al,%al
 5eb:	75 de                	jne    5cb <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 5ed:	b8 00 00 00 00       	mov    $0x0,%eax
}
 5f2:	5d                   	pop    %rbp
 5f3:	c3                   	retq   

00000000000005f4 <gets>:

char *gets(char *buf, int max) {
 5f4:	55                   	push   %rbp
 5f5:	48 89 e5             	mov    %rsp,%rbp
 5f8:	48 83 ec 20          	sub    $0x20,%rsp
 5fc:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 600:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 603:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 60a:	eb 48                	jmp    654 <gets+0x60>
    cc = read(0, &c, 1);
 60c:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 610:	ba 01 00 00 00       	mov    $0x1,%edx
 615:	48 89 c6             	mov    %rax,%rsi
 618:	bf 00 00 00 00       	mov    $0x0,%edi
 61d:	e8 6f 01 00 00       	callq  791 <read>
 622:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 625:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 629:	7e 36                	jle    661 <gets+0x6d>
      break;
    buf[i++] = c;
 62b:	8b 45 fc             	mov    -0x4(%rbp),%eax
 62e:	8d 50 01             	lea    0x1(%rax),%edx
 631:	89 55 fc             	mov    %edx,-0x4(%rbp)
 634:	48 63 d0             	movslq %eax,%rdx
 637:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 63b:	48 01 c2             	add    %rax,%rdx
 63e:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 642:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 644:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 648:	3c 0a                	cmp    $0xa,%al
 64a:	74 16                	je     662 <gets+0x6e>
 64c:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 650:	3c 0d                	cmp    $0xd,%al
 652:	74 0e                	je     662 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 654:	8b 45 fc             	mov    -0x4(%rbp),%eax
 657:	83 c0 01             	add    $0x1,%eax
 65a:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 65d:	7c ad                	jl     60c <gets+0x18>
 65f:	eb 01                	jmp    662 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 661:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 662:	8b 45 fc             	mov    -0x4(%rbp),%eax
 665:	48 63 d0             	movslq %eax,%rdx
 668:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 66c:	48 01 d0             	add    %rdx,%rax
 66f:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 672:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 676:	c9                   	leaveq 
 677:	c3                   	retq   

0000000000000678 <stat>:

int stat(char *n, struct stat *st) {
 678:	55                   	push   %rbp
 679:	48 89 e5             	mov    %rsp,%rbp
 67c:	48 83 ec 20          	sub    $0x20,%rsp
 680:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 684:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 688:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 68c:	be 00 00 00 00       	mov    $0x0,%esi
 691:	48 89 c7             	mov    %rax,%rdi
 694:	e8 20 01 00 00       	callq  7b9 <open>
 699:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 69c:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 6a0:	79 07                	jns    6a9 <stat+0x31>
    return -1;
 6a2:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 6a7:	eb 21                	jmp    6ca <stat+0x52>
  r = fstat(fd, st);
 6a9:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 6ad:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6b0:	48 89 d6             	mov    %rdx,%rsi
 6b3:	89 c7                	mov    %eax,%edi
 6b5:	e8 17 01 00 00       	callq  7d1 <fstat>
 6ba:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 6bd:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6c0:	89 c7                	mov    %eax,%edi
 6c2:	e8 da 00 00 00       	callq  7a1 <close>
  return r;
 6c7:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 6ca:	c9                   	leaveq 
 6cb:	c3                   	retq   

00000000000006cc <atoi>:

int atoi(const char *s) {
 6cc:	55                   	push   %rbp
 6cd:	48 89 e5             	mov    %rsp,%rbp
 6d0:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 6d4:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 6db:	eb 28                	jmp    705 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 6dd:	8b 55 fc             	mov    -0x4(%rbp),%edx
 6e0:	89 d0                	mov    %edx,%eax
 6e2:	c1 e0 02             	shl    $0x2,%eax
 6e5:	01 d0                	add    %edx,%eax
 6e7:	01 c0                	add    %eax,%eax
 6e9:	89 c1                	mov    %eax,%ecx
 6eb:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6ef:	48 8d 50 01          	lea    0x1(%rax),%rdx
 6f3:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 6f7:	0f b6 00             	movzbl (%rax),%eax
 6fa:	0f be c0             	movsbl %al,%eax
 6fd:	01 c8                	add    %ecx,%eax
 6ff:	83 e8 30             	sub    $0x30,%eax
 702:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 705:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 709:	0f b6 00             	movzbl (%rax),%eax
 70c:	3c 2f                	cmp    $0x2f,%al
 70e:	7e 0b                	jle    71b <atoi+0x4f>
 710:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 714:	0f b6 00             	movzbl (%rax),%eax
 717:	3c 39                	cmp    $0x39,%al
 719:	7e c2                	jle    6dd <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 71b:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 71e:	5d                   	pop    %rbp
 71f:	c3                   	retq   

0000000000000720 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 720:	55                   	push   %rbp
 721:	48 89 e5             	mov    %rsp,%rbp
 724:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 728:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 72c:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 72f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 733:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 737:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 73b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 73f:	eb 1d                	jmp    75e <memmove+0x3e>
    *dst++ = *src++;
 741:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 745:	48 8d 50 01          	lea    0x1(%rax),%rdx
 749:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 74d:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 751:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 755:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 759:	0f b6 12             	movzbl (%rdx),%edx
 75c:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 75e:	8b 45 dc             	mov    -0x24(%rbp),%eax
 761:	8d 50 ff             	lea    -0x1(%rax),%edx
 764:	89 55 dc             	mov    %edx,-0x24(%rbp)
 767:	85 c0                	test   %eax,%eax
 769:	7f d6                	jg     741 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 76b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 76f:	5d                   	pop    %rbp
 770:	c3                   	retq   

0000000000000771 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 771:	b8 01 00 00 00       	mov    $0x1,%eax
 776:	cd 40                	int    $0x40
 778:	c3                   	retq   

0000000000000779 <exit>:
SYSCALL(exit)
 779:	b8 02 00 00 00       	mov    $0x2,%eax
 77e:	cd 40                	int    $0x40
 780:	c3                   	retq   

0000000000000781 <wait>:
SYSCALL(wait)
 781:	b8 03 00 00 00       	mov    $0x3,%eax
 786:	cd 40                	int    $0x40
 788:	c3                   	retq   

0000000000000789 <pipe>:
SYSCALL(pipe)
 789:	b8 04 00 00 00       	mov    $0x4,%eax
 78e:	cd 40                	int    $0x40
 790:	c3                   	retq   

0000000000000791 <read>:
SYSCALL(read)
 791:	b8 05 00 00 00       	mov    $0x5,%eax
 796:	cd 40                	int    $0x40
 798:	c3                   	retq   

0000000000000799 <write>:
SYSCALL(write)
 799:	b8 10 00 00 00       	mov    $0x10,%eax
 79e:	cd 40                	int    $0x40
 7a0:	c3                   	retq   

00000000000007a1 <close>:
SYSCALL(close)
 7a1:	b8 15 00 00 00       	mov    $0x15,%eax
 7a6:	cd 40                	int    $0x40
 7a8:	c3                   	retq   

00000000000007a9 <kill>:
SYSCALL(kill)
 7a9:	b8 06 00 00 00       	mov    $0x6,%eax
 7ae:	cd 40                	int    $0x40
 7b0:	c3                   	retq   

00000000000007b1 <exec>:
SYSCALL(exec)
 7b1:	b8 07 00 00 00       	mov    $0x7,%eax
 7b6:	cd 40                	int    $0x40
 7b8:	c3                   	retq   

00000000000007b9 <open>:
SYSCALL(open)
 7b9:	b8 0f 00 00 00       	mov    $0xf,%eax
 7be:	cd 40                	int    $0x40
 7c0:	c3                   	retq   

00000000000007c1 <mknod>:
SYSCALL(mknod)
 7c1:	b8 11 00 00 00       	mov    $0x11,%eax
 7c6:	cd 40                	int    $0x40
 7c8:	c3                   	retq   

00000000000007c9 <unlink>:
SYSCALL(unlink)
 7c9:	b8 12 00 00 00       	mov    $0x12,%eax
 7ce:	cd 40                	int    $0x40
 7d0:	c3                   	retq   

00000000000007d1 <fstat>:
SYSCALL(fstat)
 7d1:	b8 08 00 00 00       	mov    $0x8,%eax
 7d6:	cd 40                	int    $0x40
 7d8:	c3                   	retq   

00000000000007d9 <link>:
SYSCALL(link)
 7d9:	b8 13 00 00 00       	mov    $0x13,%eax
 7de:	cd 40                	int    $0x40
 7e0:	c3                   	retq   

00000000000007e1 <mkdir>:
SYSCALL(mkdir)
 7e1:	b8 14 00 00 00       	mov    $0x14,%eax
 7e6:	cd 40                	int    $0x40
 7e8:	c3                   	retq   

00000000000007e9 <chdir>:
SYSCALL(chdir)
 7e9:	b8 09 00 00 00       	mov    $0x9,%eax
 7ee:	cd 40                	int    $0x40
 7f0:	c3                   	retq   

00000000000007f1 <dup>:
SYSCALL(dup)
 7f1:	b8 0a 00 00 00       	mov    $0xa,%eax
 7f6:	cd 40                	int    $0x40
 7f8:	c3                   	retq   

00000000000007f9 <getpid>:
SYSCALL(getpid)
 7f9:	b8 0b 00 00 00       	mov    $0xb,%eax
 7fe:	cd 40                	int    $0x40
 800:	c3                   	retq   

0000000000000801 <sbrk>:
SYSCALL(sbrk)
 801:	b8 0c 00 00 00       	mov    $0xc,%eax
 806:	cd 40                	int    $0x40
 808:	c3                   	retq   

0000000000000809 <sleep>:
SYSCALL(sleep)
 809:	b8 0d 00 00 00       	mov    $0xd,%eax
 80e:	cd 40                	int    $0x40
 810:	c3                   	retq   

0000000000000811 <uptime>:
SYSCALL(uptime)
 811:	b8 0e 00 00 00       	mov    $0xe,%eax
 816:	cd 40                	int    $0x40
 818:	c3                   	retq   

0000000000000819 <sysinfo>:
SYSCALL(sysinfo)
 819:	b8 16 00 00 00       	mov    $0x16,%eax
 81e:	cd 40                	int    $0x40
 820:	c3                   	retq   

0000000000000821 <crashn>:
SYSCALL(crashn)
 821:	b8 17 00 00 00       	mov    $0x17,%eax
 826:	cd 40                	int    $0x40
 828:	c3                   	retq   

0000000000000829 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 829:	55                   	push   %rbp
 82a:	48 89 e5             	mov    %rsp,%rbp
 82d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 831:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 835:	48 83 e8 10          	sub    $0x10,%rax
 839:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 83d:	48 8b 05 3c 05 00 00 	mov    0x53c(%rip),%rax        # d80 <freep>
 844:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 848:	eb 2f                	jmp    879 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 84a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 84e:	48 8b 00             	mov    (%rax),%rax
 851:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 855:	77 17                	ja     86e <free+0x45>
 857:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 85b:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 85f:	77 2f                	ja     890 <free+0x67>
 861:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 865:	48 8b 00             	mov    (%rax),%rax
 868:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 86c:	77 22                	ja     890 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 86e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 872:	48 8b 00             	mov    (%rax),%rax
 875:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 879:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 87d:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 881:	76 c7                	jbe    84a <free+0x21>
 883:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 887:	48 8b 00             	mov    (%rax),%rax
 88a:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 88e:	76 ba                	jbe    84a <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 890:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 894:	8b 40 08             	mov    0x8(%rax),%eax
 897:	89 c0                	mov    %eax,%eax
 899:	48 c1 e0 04          	shl    $0x4,%rax
 89d:	48 89 c2             	mov    %rax,%rdx
 8a0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8a4:	48 01 c2             	add    %rax,%rdx
 8a7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8ab:	48 8b 00             	mov    (%rax),%rax
 8ae:	48 39 c2             	cmp    %rax,%rdx
 8b1:	75 2d                	jne    8e0 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 8b3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8b7:	8b 50 08             	mov    0x8(%rax),%edx
 8ba:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8be:	48 8b 00             	mov    (%rax),%rax
 8c1:	8b 40 08             	mov    0x8(%rax),%eax
 8c4:	01 c2                	add    %eax,%edx
 8c6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8ca:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 8cd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8d1:	48 8b 00             	mov    (%rax),%rax
 8d4:	48 8b 10             	mov    (%rax),%rdx
 8d7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8db:	48 89 10             	mov    %rdx,(%rax)
 8de:	eb 0e                	jmp    8ee <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 8e0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8e4:	48 8b 10             	mov    (%rax),%rdx
 8e7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8eb:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 8ee:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8f2:	8b 40 08             	mov    0x8(%rax),%eax
 8f5:	89 c0                	mov    %eax,%eax
 8f7:	48 c1 e0 04          	shl    $0x4,%rax
 8fb:	48 89 c2             	mov    %rax,%rdx
 8fe:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 902:	48 01 d0             	add    %rdx,%rax
 905:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 909:	75 27                	jne    932 <free+0x109>
    p->s.size += bp->s.size;
 90b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 90f:	8b 50 08             	mov    0x8(%rax),%edx
 912:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 916:	8b 40 08             	mov    0x8(%rax),%eax
 919:	01 c2                	add    %eax,%edx
 91b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 91f:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 922:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 926:	48 8b 10             	mov    (%rax),%rdx
 929:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 92d:	48 89 10             	mov    %rdx,(%rax)
 930:	eb 0b                	jmp    93d <free+0x114>
  } else
    p->s.ptr = bp;
 932:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 936:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 93a:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 93d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 941:	48 89 05 38 04 00 00 	mov    %rax,0x438(%rip)        # d80 <freep>
}
 948:	90                   	nop
 949:	5d                   	pop    %rbp
 94a:	c3                   	retq   

000000000000094b <morecore>:

static Header *morecore(uint nu) {
 94b:	55                   	push   %rbp
 94c:	48 89 e5             	mov    %rsp,%rbp
 94f:	48 83 ec 20          	sub    $0x20,%rsp
 953:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 956:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 95d:	77 07                	ja     966 <morecore+0x1b>
    nu = 4096;
 95f:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 966:	8b 45 ec             	mov    -0x14(%rbp),%eax
 969:	c1 e0 04             	shl    $0x4,%eax
 96c:	89 c7                	mov    %eax,%edi
 96e:	e8 8e fe ff ff       	callq  801 <sbrk>
 973:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 977:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 97c:	75 07                	jne    985 <morecore+0x3a>
    return 0;
 97e:	b8 00 00 00 00       	mov    $0x0,%eax
 983:	eb 29                	jmp    9ae <morecore+0x63>
  hp = (Header *)p;
 985:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 989:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 98d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 991:	8b 55 ec             	mov    -0x14(%rbp),%edx
 994:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 997:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 99b:	48 83 c0 10          	add    $0x10,%rax
 99f:	48 89 c7             	mov    %rax,%rdi
 9a2:	e8 82 fe ff ff       	callq  829 <free>
  return freep;
 9a7:	48 8b 05 d2 03 00 00 	mov    0x3d2(%rip),%rax        # d80 <freep>
}
 9ae:	c9                   	leaveq 
 9af:	c3                   	retq   

00000000000009b0 <malloc>:

void *malloc(uint nbytes) {
 9b0:	55                   	push   %rbp
 9b1:	48 89 e5             	mov    %rsp,%rbp
 9b4:	48 83 ec 30          	sub    $0x30,%rsp
 9b8:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 9bb:	8b 45 dc             	mov    -0x24(%rbp),%eax
 9be:	48 83 c0 0f          	add    $0xf,%rax
 9c2:	48 c1 e8 04          	shr    $0x4,%rax
 9c6:	83 c0 01             	add    $0x1,%eax
 9c9:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 9cc:	48 8b 05 ad 03 00 00 	mov    0x3ad(%rip),%rax        # d80 <freep>
 9d3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 9d7:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 9dc:	75 2b                	jne    a09 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 9de:	48 c7 45 f0 70 0d 00 	movq   $0xd70,-0x10(%rbp)
 9e5:	00 
 9e6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ea:	48 89 05 8f 03 00 00 	mov    %rax,0x38f(%rip)        # d80 <freep>
 9f1:	48 8b 05 88 03 00 00 	mov    0x388(%rip),%rax        # d80 <freep>
 9f8:	48 89 05 71 03 00 00 	mov    %rax,0x371(%rip)        # d70 <base>
    base.s.size = 0;
 9ff:	c7 05 6f 03 00 00 00 	movl   $0x0,0x36f(%rip)        # d78 <base+0x8>
 a06:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a09:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a0d:	48 8b 00             	mov    (%rax),%rax
 a10:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a14:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a18:	8b 40 08             	mov    0x8(%rax),%eax
 a1b:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a1e:	72 5f                	jb     a7f <malloc+0xcf>
      if (p->s.size == nunits)
 a20:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a24:	8b 40 08             	mov    0x8(%rax),%eax
 a27:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a2a:	75 10                	jne    a3c <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 a2c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a30:	48 8b 10             	mov    (%rax),%rdx
 a33:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a37:	48 89 10             	mov    %rdx,(%rax)
 a3a:	eb 2e                	jmp    a6a <malloc+0xba>
      else {
        p->s.size -= nunits;
 a3c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a40:	8b 40 08             	mov    0x8(%rax),%eax
 a43:	2b 45 ec             	sub    -0x14(%rbp),%eax
 a46:	89 c2                	mov    %eax,%edx
 a48:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a4c:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 a4f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a53:	8b 40 08             	mov    0x8(%rax),%eax
 a56:	89 c0                	mov    %eax,%eax
 a58:	48 c1 e0 04          	shl    $0x4,%rax
 a5c:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 a60:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a64:	8b 55 ec             	mov    -0x14(%rbp),%edx
 a67:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 a6a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a6e:	48 89 05 0b 03 00 00 	mov    %rax,0x30b(%rip)        # d80 <freep>
      return (void *)(p + 1);
 a75:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a79:	48 83 c0 10          	add    $0x10,%rax
 a7d:	eb 41                	jmp    ac0 <malloc+0x110>
    }
    if (p == freep)
 a7f:	48 8b 05 fa 02 00 00 	mov    0x2fa(%rip),%rax        # d80 <freep>
 a86:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 a8a:	75 1c                	jne    aa8 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 a8c:	8b 45 ec             	mov    -0x14(%rbp),%eax
 a8f:	89 c7                	mov    %eax,%edi
 a91:	e8 b5 fe ff ff       	callq  94b <morecore>
 a96:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 a9a:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 a9f:	75 07                	jne    aa8 <malloc+0xf8>
        return 0;
 aa1:	b8 00 00 00 00       	mov    $0x0,%eax
 aa6:	eb 18                	jmp    ac0 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 aa8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aac:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 ab0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ab4:	48 8b 00             	mov    (%rax),%rax
 ab7:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 abb:	e9 54 ff ff ff       	jmpq   a14 <malloc+0x64>
 ac0:	c9                   	leaveq 
 ac1:	c3                   	retq   
