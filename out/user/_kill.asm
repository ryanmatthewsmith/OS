
out/user/_kill:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <cdefs.h>
#include <stat.h>
#include <user.h>

int main(int argc, char **argv) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 20          	sub    $0x20,%rsp
   8:	89 7d ec             	mov    %edi,-0x14(%rbp)
   b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int i;

  if (argc < 2) {
   f:	83 7d ec 01          	cmpl   $0x1,-0x14(%rbp)
  13:	7f 19                	jg     2e <main+0x2e>
    printf(2, "usage: kill pid...\n");
  15:	be 14 0b 00 00       	mov    $0xb14,%esi
  1a:	bf 02 00 00 00       	mov    $0x2,%edi
  1f:	b8 00 00 00 00       	mov    $0x0,%eax
  24:	e8 ff 01 00 00       	callq  228 <printf>
    exit();
  29:	e8 9d 07 00 00       	callq  7cb <exit>
  }
  for (i = 1; i < argc; i++)
  2e:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%rbp)
  35:	eb 2a                	jmp    61 <main+0x61>
    kill(atoi(argv[i]));
  37:	8b 45 fc             	mov    -0x4(%rbp),%eax
  3a:	48 98                	cltq   
  3c:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
  43:	00 
  44:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  48:	48 01 d0             	add    %rdx,%rax
  4b:	48 8b 00             	mov    (%rax),%rax
  4e:	48 89 c7             	mov    %rax,%rdi
  51:	e8 c8 06 00 00       	callq  71e <atoi>
  56:	89 c7                	mov    %eax,%edi
  58:	e8 9e 07 00 00       	callq  7fb <kill>

  if (argc < 2) {
    printf(2, "usage: kill pid...\n");
    exit();
  }
  for (i = 1; i < argc; i++)
  5d:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  61:	8b 45 fc             	mov    -0x4(%rbp),%eax
  64:	3b 45 ec             	cmp    -0x14(%rbp),%eax
  67:	7c ce                	jl     37 <main+0x37>
    kill(atoi(argv[i]));
  exit();
  69:	e8 5d 07 00 00       	callq  7cb <exit>

000000000000006e <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  6e:	55                   	push   %rbp
  6f:	48 89 e5             	mov    %rsp,%rbp
  72:	48 83 ec 10          	sub    $0x10,%rsp
  76:	89 7d fc             	mov    %edi,-0x4(%rbp)
  79:	89 f0                	mov    %esi,%eax
  7b:	88 45 f8             	mov    %al,-0x8(%rbp)
  7e:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  82:	8b 45 fc             	mov    -0x4(%rbp),%eax
  85:	ba 01 00 00 00       	mov    $0x1,%edx
  8a:	48 89 ce             	mov    %rcx,%rsi
  8d:	89 c7                	mov    %eax,%edi
  8f:	e8 57 07 00 00       	callq  7eb <write>
  94:	90                   	nop
  95:	c9                   	leaveq 
  96:	c3                   	retq   

0000000000000097 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  97:	55                   	push   %rbp
  98:	48 89 e5             	mov    %rsp,%rbp
  9b:	48 83 ec 40          	sub    $0x40,%rsp
  9f:	89 7d cc             	mov    %edi,-0x34(%rbp)
  a2:	89 75 c8             	mov    %esi,-0x38(%rbp)
  a5:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  a8:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  ab:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  af:	74 1f                	je     d0 <printint64+0x39>
  b1:	8b 45 c8             	mov    -0x38(%rbp),%eax
  b4:	c1 e8 1f             	shr    $0x1f,%eax
  b7:	0f b6 c0             	movzbl %al,%eax
  ba:	89 45 c0             	mov    %eax,-0x40(%rbp)
  bd:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  c1:	74 0d                	je     d0 <printint64+0x39>
    x = -xx;
  c3:	8b 45 c8             	mov    -0x38(%rbp),%eax
  c6:	f7 d8                	neg    %eax
  c8:	48 98                	cltq   
  ca:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  ce:	eb 09                	jmp    d9 <printint64+0x42>
  else
    x = xx;
  d0:	8b 45 c8             	mov    -0x38(%rbp),%eax
  d3:	48 98                	cltq   
  d5:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
  d9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
  e0:	8b 4d fc             	mov    -0x4(%rbp),%ecx
  e3:	8d 41 01             	lea    0x1(%rcx),%eax
  e6:	89 45 fc             	mov    %eax,-0x4(%rbp)
  e9:	8b 45 c4             	mov    -0x3c(%rbp),%eax
  ec:	48 63 f0             	movslq %eax,%rsi
  ef:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  f3:	ba 00 00 00 00       	mov    $0x0,%edx
  f8:	48 f7 f6             	div    %rsi
  fb:	48 89 d0             	mov    %rdx,%rax
  fe:	0f b6 90 90 0d 00 00 	movzbl 0xd90(%rax),%edx
 105:	48 63 c1             	movslq %ecx,%rax
 108:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 10c:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 10f:	48 63 f8             	movslq %eax,%rdi
 112:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 116:	ba 00 00 00 00       	mov    $0x0,%edx
 11b:	48 f7 f7             	div    %rdi
 11e:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 122:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 127:	75 b7                	jne    e0 <printint64+0x49>

  if (sgn)
 129:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 12d:	74 2b                	je     15a <printint64+0xc3>
    buf[i++] = '-';
 12f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 132:	8d 50 01             	lea    0x1(%rax),%edx
 135:	89 55 fc             	mov    %edx,-0x4(%rbp)
 138:	48 98                	cltq   
 13a:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 13f:	eb 19                	jmp    15a <printint64+0xc3>
    putc(fd, buf[i]);
 141:	8b 45 fc             	mov    -0x4(%rbp),%eax
 144:	48 98                	cltq   
 146:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 14b:	0f be d0             	movsbl %al,%edx
 14e:	8b 45 cc             	mov    -0x34(%rbp),%eax
 151:	89 d6                	mov    %edx,%esi
 153:	89 c7                	mov    %eax,%edi
 155:	e8 14 ff ff ff       	callq  6e <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 15a:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 15e:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 162:	79 dd                	jns    141 <printint64+0xaa>
    putc(fd, buf[i]);
}
 164:	90                   	nop
 165:	c9                   	leaveq 
 166:	c3                   	retq   

0000000000000167 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 167:	55                   	push   %rbp
 168:	48 89 e5             	mov    %rsp,%rbp
 16b:	48 83 ec 30          	sub    $0x30,%rsp
 16f:	89 7d dc             	mov    %edi,-0x24(%rbp)
 172:	89 75 d8             	mov    %esi,-0x28(%rbp)
 175:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 178:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 17b:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 182:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 186:	74 17                	je     19f <printint+0x38>
 188:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 18c:	79 11                	jns    19f <printint+0x38>
    neg = 1;
 18e:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 195:	8b 45 d8             	mov    -0x28(%rbp),%eax
 198:	f7 d8                	neg    %eax
 19a:	89 45 f4             	mov    %eax,-0xc(%rbp)
 19d:	eb 06                	jmp    1a5 <printint+0x3e>
  } else {
    x = xx;
 19f:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1a2:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 1a5:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1ac:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1af:	8d 41 01             	lea    0x1(%rcx),%eax
 1b2:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1b5:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 1b8:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1bb:	ba 00 00 00 00       	mov    $0x0,%edx
 1c0:	f7 f6                	div    %esi
 1c2:	89 d0                	mov    %edx,%eax
 1c4:	89 c0                	mov    %eax,%eax
 1c6:	0f b6 90 b0 0d 00 00 	movzbl 0xdb0(%rax),%edx
 1cd:	48 63 c1             	movslq %ecx,%rax
 1d0:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 1d4:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 1d7:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1da:	ba 00 00 00 00       	mov    $0x0,%edx
 1df:	f7 f7                	div    %edi
 1e1:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1e4:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 1e8:	75 c2                	jne    1ac <printint+0x45>
  if (neg)
 1ea:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 1ee:	74 2b                	je     21b <printint+0xb4>
    buf[i++] = '-';
 1f0:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1f3:	8d 50 01             	lea    0x1(%rax),%edx
 1f6:	89 55 fc             	mov    %edx,-0x4(%rbp)
 1f9:	48 98                	cltq   
 1fb:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 200:	eb 19                	jmp    21b <printint+0xb4>
    putc(fd, buf[i]);
 202:	8b 45 fc             	mov    -0x4(%rbp),%eax
 205:	48 98                	cltq   
 207:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 20c:	0f be d0             	movsbl %al,%edx
 20f:	8b 45 dc             	mov    -0x24(%rbp),%eax
 212:	89 d6                	mov    %edx,%esi
 214:	89 c7                	mov    %eax,%edi
 216:	e8 53 fe ff ff       	callq  6e <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 21b:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 21f:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 223:	79 dd                	jns    202 <printint+0x9b>
    putc(fd, buf[i]);
}
 225:	90                   	nop
 226:	c9                   	leaveq 
 227:	c3                   	retq   

0000000000000228 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 228:	55                   	push   %rbp
 229:	48 89 e5             	mov    %rsp,%rbp
 22c:	48 83 ec 70          	sub    $0x70,%rsp
 230:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 233:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 237:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 23b:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 23f:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 243:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 247:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 24e:	48 8d 45 10          	lea    0x10(%rbp),%rax
 252:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 256:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 25a:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 25e:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 265:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 26c:	e9 68 02 00 00       	jmpq   4d9 <printf+0x2b1>
    c = fmt[i] & 0xff;
 271:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 274:	48 63 d0             	movslq %eax,%rdx
 277:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 27b:	48 01 d0             	add    %rdx,%rax
 27e:	0f b6 00             	movzbl (%rax),%eax
 281:	0f be c0             	movsbl %al,%eax
 284:	25 ff 00 00 00       	and    $0xff,%eax
 289:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 28c:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 290:	75 30                	jne    2c2 <printf+0x9a>
      if (c == '%') {
 292:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 296:	75 13                	jne    2ab <printf+0x83>
        state = '%';
 298:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 29f:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 2a6:	e9 2a 02 00 00       	jmpq   4d5 <printf+0x2ad>
      } else {
        putc(fd, c);
 2ab:	8b 45 b8             	mov    -0x48(%rbp),%eax
 2ae:	0f be d0             	movsbl %al,%edx
 2b1:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2b4:	89 d6                	mov    %edx,%esi
 2b6:	89 c7                	mov    %eax,%edi
 2b8:	e8 b1 fd ff ff       	callq  6e <putc>
 2bd:	e9 13 02 00 00       	jmpq   4d5 <printf+0x2ad>
      }
    } else if (state == '%') {
 2c2:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 2c6:	0f 85 09 02 00 00    	jne    4d5 <printf+0x2ad>
      if (c == 'l') {
 2cc:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 2d0:	75 0c                	jne    2de <printf+0xb6>
        lflag = 1;
 2d2:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 2d9:	e9 f7 01 00 00       	jmpq   4d5 <printf+0x2ad>
      } else if (c == 'd') {
 2de:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 2e2:	0f 85 95 00 00 00    	jne    37d <printf+0x155>
        if (lflag == 1)
 2e8:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 2ec:	75 49                	jne    337 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 2ee:	8b 45 a0             	mov    -0x60(%rbp),%eax
 2f1:	83 f8 30             	cmp    $0x30,%eax
 2f4:	73 17                	jae    30d <printf+0xe5>
 2f6:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 2fa:	8b 55 a0             	mov    -0x60(%rbp),%edx
 2fd:	89 d2                	mov    %edx,%edx
 2ff:	48 01 d0             	add    %rdx,%rax
 302:	8b 55 a0             	mov    -0x60(%rbp),%edx
 305:	83 c2 08             	add    $0x8,%edx
 308:	89 55 a0             	mov    %edx,-0x60(%rbp)
 30b:	eb 0c                	jmp    319 <printf+0xf1>
 30d:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 311:	48 8d 50 08          	lea    0x8(%rax),%rdx
 315:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 319:	48 8b 00             	mov    (%rax),%rax
 31c:	89 c6                	mov    %eax,%esi
 31e:	8b 45 9c             	mov    -0x64(%rbp),%eax
 321:	b9 01 00 00 00       	mov    $0x1,%ecx
 326:	ba 0a 00 00 00       	mov    $0xa,%edx
 32b:	89 c7                	mov    %eax,%edi
 32d:	e8 65 fd ff ff       	callq  97 <printint64>
 332:	e9 97 01 00 00       	jmpq   4ce <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 337:	8b 45 a0             	mov    -0x60(%rbp),%eax
 33a:	83 f8 30             	cmp    $0x30,%eax
 33d:	73 17                	jae    356 <printf+0x12e>
 33f:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 343:	8b 55 a0             	mov    -0x60(%rbp),%edx
 346:	89 d2                	mov    %edx,%edx
 348:	48 01 d0             	add    %rdx,%rax
 34b:	8b 55 a0             	mov    -0x60(%rbp),%edx
 34e:	83 c2 08             	add    $0x8,%edx
 351:	89 55 a0             	mov    %edx,-0x60(%rbp)
 354:	eb 0c                	jmp    362 <printf+0x13a>
 356:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 35a:	48 8d 50 08          	lea    0x8(%rax),%rdx
 35e:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 362:	8b 30                	mov    (%rax),%esi
 364:	8b 45 9c             	mov    -0x64(%rbp),%eax
 367:	b9 01 00 00 00       	mov    $0x1,%ecx
 36c:	ba 0a 00 00 00       	mov    $0xa,%edx
 371:	89 c7                	mov    %eax,%edi
 373:	e8 ef fd ff ff       	callq  167 <printint>
 378:	e9 51 01 00 00       	jmpq   4ce <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 37d:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 381:	74 0a                	je     38d <printf+0x165>
 383:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 387:	0f 85 95 00 00 00    	jne    422 <printf+0x1fa>
        if (lflag == 1)
 38d:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 391:	75 49                	jne    3dc <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 393:	8b 45 a0             	mov    -0x60(%rbp),%eax
 396:	83 f8 30             	cmp    $0x30,%eax
 399:	73 17                	jae    3b2 <printf+0x18a>
 39b:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 39f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3a2:	89 d2                	mov    %edx,%edx
 3a4:	48 01 d0             	add    %rdx,%rax
 3a7:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3aa:	83 c2 08             	add    $0x8,%edx
 3ad:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3b0:	eb 0c                	jmp    3be <printf+0x196>
 3b2:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3b6:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3ba:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3be:	48 8b 00             	mov    (%rax),%rax
 3c1:	89 c6                	mov    %eax,%esi
 3c3:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3c6:	b9 00 00 00 00       	mov    $0x0,%ecx
 3cb:	ba 10 00 00 00       	mov    $0x10,%edx
 3d0:	89 c7                	mov    %eax,%edi
 3d2:	e8 c0 fc ff ff       	callq  97 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 3d7:	e9 f2 00 00 00       	jmpq   4ce <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 3dc:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3df:	83 f8 30             	cmp    $0x30,%eax
 3e2:	73 17                	jae    3fb <printf+0x1d3>
 3e4:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3e8:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3eb:	89 d2                	mov    %edx,%edx
 3ed:	48 01 d0             	add    %rdx,%rax
 3f0:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3f3:	83 c2 08             	add    $0x8,%edx
 3f6:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3f9:	eb 0c                	jmp    407 <printf+0x1df>
 3fb:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3ff:	48 8d 50 08          	lea    0x8(%rax),%rdx
 403:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 407:	8b 30                	mov    (%rax),%esi
 409:	8b 45 9c             	mov    -0x64(%rbp),%eax
 40c:	b9 00 00 00 00       	mov    $0x0,%ecx
 411:	ba 10 00 00 00       	mov    $0x10,%edx
 416:	89 c7                	mov    %eax,%edi
 418:	e8 4a fd ff ff       	callq  167 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 41d:	e9 ac 00 00 00       	jmpq   4ce <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 422:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 426:	75 6b                	jne    493 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 428:	8b 45 a0             	mov    -0x60(%rbp),%eax
 42b:	83 f8 30             	cmp    $0x30,%eax
 42e:	73 17                	jae    447 <printf+0x21f>
 430:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 434:	8b 55 a0             	mov    -0x60(%rbp),%edx
 437:	89 d2                	mov    %edx,%edx
 439:	48 01 d0             	add    %rdx,%rax
 43c:	8b 55 a0             	mov    -0x60(%rbp),%edx
 43f:	83 c2 08             	add    $0x8,%edx
 442:	89 55 a0             	mov    %edx,-0x60(%rbp)
 445:	eb 0c                	jmp    453 <printf+0x22b>
 447:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 44b:	48 8d 50 08          	lea    0x8(%rax),%rdx
 44f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 453:	48 8b 00             	mov    (%rax),%rax
 456:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 45a:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 45f:	75 25                	jne    486 <printf+0x25e>
          s = "(null)";
 461:	48 c7 45 c8 28 0b 00 	movq   $0xb28,-0x38(%rbp)
 468:	00 
        for (; *s; s++)
 469:	eb 1b                	jmp    486 <printf+0x25e>
          putc(fd, *s);
 46b:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 46f:	0f b6 00             	movzbl (%rax),%eax
 472:	0f be d0             	movsbl %al,%edx
 475:	8b 45 9c             	mov    -0x64(%rbp),%eax
 478:	89 d6                	mov    %edx,%esi
 47a:	89 c7                	mov    %eax,%edi
 47c:	e8 ed fb ff ff       	callq  6e <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 481:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 486:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 48a:	0f b6 00             	movzbl (%rax),%eax
 48d:	84 c0                	test   %al,%al
 48f:	75 da                	jne    46b <printf+0x243>
 491:	eb 3b                	jmp    4ce <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 493:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 497:	75 14                	jne    4ad <printf+0x285>
        putc(fd, c);
 499:	8b 45 b8             	mov    -0x48(%rbp),%eax
 49c:	0f be d0             	movsbl %al,%edx
 49f:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4a2:	89 d6                	mov    %edx,%esi
 4a4:	89 c7                	mov    %eax,%edi
 4a6:	e8 c3 fb ff ff       	callq  6e <putc>
 4ab:	eb 21                	jmp    4ce <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 4ad:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4b0:	be 25 00 00 00       	mov    $0x25,%esi
 4b5:	89 c7                	mov    %eax,%edi
 4b7:	e8 b2 fb ff ff       	callq  6e <putc>
        putc(fd, c);
 4bc:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4bf:	0f be d0             	movsbl %al,%edx
 4c2:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4c5:	89 d6                	mov    %edx,%esi
 4c7:	89 c7                	mov    %eax,%edi
 4c9:	e8 a0 fb ff ff       	callq  6e <putc>
      }
      state = 0;
 4ce:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 4d5:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 4d9:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 4dc:	48 63 d0             	movslq %eax,%rdx
 4df:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 4e3:	48 01 d0             	add    %rdx,%rax
 4e6:	0f b6 00             	movzbl (%rax),%eax
 4e9:	84 c0                	test   %al,%al
 4eb:	0f 85 80 fd ff ff    	jne    271 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 4f1:	90                   	nop
 4f2:	c9                   	leaveq 
 4f3:	c3                   	retq   

00000000000004f4 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 4f4:	55                   	push   %rbp
 4f5:	48 89 e5             	mov    %rsp,%rbp
 4f8:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 4fc:	89 75 f4             	mov    %esi,-0xc(%rbp)
 4ff:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 502:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 506:	8b 55 f0             	mov    -0x10(%rbp),%edx
 509:	8b 45 f4             	mov    -0xc(%rbp),%eax
 50c:	48 89 ce             	mov    %rcx,%rsi
 50f:	48 89 f7             	mov    %rsi,%rdi
 512:	89 d1                	mov    %edx,%ecx
 514:	fc                   	cld    
 515:	f3 aa                	rep stos %al,%es:(%rdi)
 517:	89 ca                	mov    %ecx,%edx
 519:	48 89 fe             	mov    %rdi,%rsi
 51c:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 520:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 523:	90                   	nop
 524:	5d                   	pop    %rbp
 525:	c3                   	retq   

0000000000000526 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 526:	55                   	push   %rbp
 527:	48 89 e5             	mov    %rsp,%rbp
 52a:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 52e:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 532:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 536:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 53a:	90                   	nop
 53b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 53f:	48 8d 50 01          	lea    0x1(%rax),%rdx
 543:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 547:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 54b:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 54f:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 553:	0f b6 12             	movzbl (%rdx),%edx
 556:	88 10                	mov    %dl,(%rax)
 558:	0f b6 00             	movzbl (%rax),%eax
 55b:	84 c0                	test   %al,%al
 55d:	75 dc                	jne    53b <strcpy+0x15>
    ;
  return os;
 55f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 563:	5d                   	pop    %rbp
 564:	c3                   	retq   

0000000000000565 <strcmp>:

int strcmp(const char *p, const char *q) {
 565:	55                   	push   %rbp
 566:	48 89 e5             	mov    %rsp,%rbp
 569:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 56d:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 571:	eb 0a                	jmp    57d <strcmp+0x18>
    p++, q++;
 573:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 578:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 57d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 581:	0f b6 00             	movzbl (%rax),%eax
 584:	84 c0                	test   %al,%al
 586:	74 12                	je     59a <strcmp+0x35>
 588:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 58c:	0f b6 10             	movzbl (%rax),%edx
 58f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 593:	0f b6 00             	movzbl (%rax),%eax
 596:	38 c2                	cmp    %al,%dl
 598:	74 d9                	je     573 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 59a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 59e:	0f b6 00             	movzbl (%rax),%eax
 5a1:	0f b6 d0             	movzbl %al,%edx
 5a4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5a8:	0f b6 00             	movzbl (%rax),%eax
 5ab:	0f b6 c0             	movzbl %al,%eax
 5ae:	29 c2                	sub    %eax,%edx
 5b0:	89 d0                	mov    %edx,%eax
}
 5b2:	5d                   	pop    %rbp
 5b3:	c3                   	retq   

00000000000005b4 <strlen>:

uint strlen(char *s) {
 5b4:	55                   	push   %rbp
 5b5:	48 89 e5             	mov    %rsp,%rbp
 5b8:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 5bc:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 5c3:	eb 04                	jmp    5c9 <strlen+0x15>
 5c5:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 5c9:	8b 45 fc             	mov    -0x4(%rbp),%eax
 5cc:	48 63 d0             	movslq %eax,%rdx
 5cf:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5d3:	48 01 d0             	add    %rdx,%rax
 5d6:	0f b6 00             	movzbl (%rax),%eax
 5d9:	84 c0                	test   %al,%al
 5db:	75 e8                	jne    5c5 <strlen+0x11>
    ;
  return n;
 5dd:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 5e0:	5d                   	pop    %rbp
 5e1:	c3                   	retq   

00000000000005e2 <memset>:

void *memset(void *dst, int c, uint n) {
 5e2:	55                   	push   %rbp
 5e3:	48 89 e5             	mov    %rsp,%rbp
 5e6:	48 83 ec 10          	sub    $0x10,%rsp
 5ea:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5ee:	89 75 f4             	mov    %esi,-0xc(%rbp)
 5f1:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 5f4:	8b 55 f0             	mov    -0x10(%rbp),%edx
 5f7:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 5fa:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5fe:	89 ce                	mov    %ecx,%esi
 600:	48 89 c7             	mov    %rax,%rdi
 603:	e8 ec fe ff ff       	callq  4f4 <stosb>
  return dst;
 608:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 60c:	c9                   	leaveq 
 60d:	c3                   	retq   

000000000000060e <strchr>:

char *strchr(const char *s, char c) {
 60e:	55                   	push   %rbp
 60f:	48 89 e5             	mov    %rsp,%rbp
 612:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 616:	89 f0                	mov    %esi,%eax
 618:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 61b:	eb 17                	jmp    634 <strchr+0x26>
    if (*s == c)
 61d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 621:	0f b6 00             	movzbl (%rax),%eax
 624:	3a 45 f4             	cmp    -0xc(%rbp),%al
 627:	75 06                	jne    62f <strchr+0x21>
      return (char *)s;
 629:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 62d:	eb 15                	jmp    644 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 62f:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 634:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 638:	0f b6 00             	movzbl (%rax),%eax
 63b:	84 c0                	test   %al,%al
 63d:	75 de                	jne    61d <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 63f:	b8 00 00 00 00       	mov    $0x0,%eax
}
 644:	5d                   	pop    %rbp
 645:	c3                   	retq   

0000000000000646 <gets>:

char *gets(char *buf, int max) {
 646:	55                   	push   %rbp
 647:	48 89 e5             	mov    %rsp,%rbp
 64a:	48 83 ec 20          	sub    $0x20,%rsp
 64e:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 652:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 655:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 65c:	eb 48                	jmp    6a6 <gets+0x60>
    cc = read(0, &c, 1);
 65e:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 662:	ba 01 00 00 00       	mov    $0x1,%edx
 667:	48 89 c6             	mov    %rax,%rsi
 66a:	bf 00 00 00 00       	mov    $0x0,%edi
 66f:	e8 6f 01 00 00       	callq  7e3 <read>
 674:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 677:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 67b:	7e 36                	jle    6b3 <gets+0x6d>
      break;
    buf[i++] = c;
 67d:	8b 45 fc             	mov    -0x4(%rbp),%eax
 680:	8d 50 01             	lea    0x1(%rax),%edx
 683:	89 55 fc             	mov    %edx,-0x4(%rbp)
 686:	48 63 d0             	movslq %eax,%rdx
 689:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 68d:	48 01 c2             	add    %rax,%rdx
 690:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 694:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 696:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 69a:	3c 0a                	cmp    $0xa,%al
 69c:	74 16                	je     6b4 <gets+0x6e>
 69e:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6a2:	3c 0d                	cmp    $0xd,%al
 6a4:	74 0e                	je     6b4 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6a6:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6a9:	83 c0 01             	add    $0x1,%eax
 6ac:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 6af:	7c ad                	jl     65e <gets+0x18>
 6b1:	eb 01                	jmp    6b4 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 6b3:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 6b4:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6b7:	48 63 d0             	movslq %eax,%rdx
 6ba:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6be:	48 01 d0             	add    %rdx,%rax
 6c1:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 6c4:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 6c8:	c9                   	leaveq 
 6c9:	c3                   	retq   

00000000000006ca <stat>:

int stat(char *n, struct stat *st) {
 6ca:	55                   	push   %rbp
 6cb:	48 89 e5             	mov    %rsp,%rbp
 6ce:	48 83 ec 20          	sub    $0x20,%rsp
 6d2:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 6d6:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 6da:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6de:	be 00 00 00 00       	mov    $0x0,%esi
 6e3:	48 89 c7             	mov    %rax,%rdi
 6e6:	e8 20 01 00 00       	callq  80b <open>
 6eb:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 6ee:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 6f2:	79 07                	jns    6fb <stat+0x31>
    return -1;
 6f4:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 6f9:	eb 21                	jmp    71c <stat+0x52>
  r = fstat(fd, st);
 6fb:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 6ff:	8b 45 fc             	mov    -0x4(%rbp),%eax
 702:	48 89 d6             	mov    %rdx,%rsi
 705:	89 c7                	mov    %eax,%edi
 707:	e8 17 01 00 00       	callq  823 <fstat>
 70c:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 70f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 712:	89 c7                	mov    %eax,%edi
 714:	e8 da 00 00 00       	callq  7f3 <close>
  return r;
 719:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 71c:	c9                   	leaveq 
 71d:	c3                   	retq   

000000000000071e <atoi>:

int atoi(const char *s) {
 71e:	55                   	push   %rbp
 71f:	48 89 e5             	mov    %rsp,%rbp
 722:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 726:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 72d:	eb 28                	jmp    757 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 72f:	8b 55 fc             	mov    -0x4(%rbp),%edx
 732:	89 d0                	mov    %edx,%eax
 734:	c1 e0 02             	shl    $0x2,%eax
 737:	01 d0                	add    %edx,%eax
 739:	01 c0                	add    %eax,%eax
 73b:	89 c1                	mov    %eax,%ecx
 73d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 741:	48 8d 50 01          	lea    0x1(%rax),%rdx
 745:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 749:	0f b6 00             	movzbl (%rax),%eax
 74c:	0f be c0             	movsbl %al,%eax
 74f:	01 c8                	add    %ecx,%eax
 751:	83 e8 30             	sub    $0x30,%eax
 754:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 757:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 75b:	0f b6 00             	movzbl (%rax),%eax
 75e:	3c 2f                	cmp    $0x2f,%al
 760:	7e 0b                	jle    76d <atoi+0x4f>
 762:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 766:	0f b6 00             	movzbl (%rax),%eax
 769:	3c 39                	cmp    $0x39,%al
 76b:	7e c2                	jle    72f <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 76d:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 770:	5d                   	pop    %rbp
 771:	c3                   	retq   

0000000000000772 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 772:	55                   	push   %rbp
 773:	48 89 e5             	mov    %rsp,%rbp
 776:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 77a:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 77e:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 781:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 785:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 789:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 78d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 791:	eb 1d                	jmp    7b0 <memmove+0x3e>
    *dst++ = *src++;
 793:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 797:	48 8d 50 01          	lea    0x1(%rax),%rdx
 79b:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 79f:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 7a3:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 7a7:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 7ab:	0f b6 12             	movzbl (%rdx),%edx
 7ae:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 7b0:	8b 45 dc             	mov    -0x24(%rbp),%eax
 7b3:	8d 50 ff             	lea    -0x1(%rax),%edx
 7b6:	89 55 dc             	mov    %edx,-0x24(%rbp)
 7b9:	85 c0                	test   %eax,%eax
 7bb:	7f d6                	jg     793 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 7bd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7c1:	5d                   	pop    %rbp
 7c2:	c3                   	retq   

00000000000007c3 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 7c3:	b8 01 00 00 00       	mov    $0x1,%eax
 7c8:	cd 40                	int    $0x40
 7ca:	c3                   	retq   

00000000000007cb <exit>:
SYSCALL(exit)
 7cb:	b8 02 00 00 00       	mov    $0x2,%eax
 7d0:	cd 40                	int    $0x40
 7d2:	c3                   	retq   

00000000000007d3 <wait>:
SYSCALL(wait)
 7d3:	b8 03 00 00 00       	mov    $0x3,%eax
 7d8:	cd 40                	int    $0x40
 7da:	c3                   	retq   

00000000000007db <pipe>:
SYSCALL(pipe)
 7db:	b8 04 00 00 00       	mov    $0x4,%eax
 7e0:	cd 40                	int    $0x40
 7e2:	c3                   	retq   

00000000000007e3 <read>:
SYSCALL(read)
 7e3:	b8 05 00 00 00       	mov    $0x5,%eax
 7e8:	cd 40                	int    $0x40
 7ea:	c3                   	retq   

00000000000007eb <write>:
SYSCALL(write)
 7eb:	b8 10 00 00 00       	mov    $0x10,%eax
 7f0:	cd 40                	int    $0x40
 7f2:	c3                   	retq   

00000000000007f3 <close>:
SYSCALL(close)
 7f3:	b8 15 00 00 00       	mov    $0x15,%eax
 7f8:	cd 40                	int    $0x40
 7fa:	c3                   	retq   

00000000000007fb <kill>:
SYSCALL(kill)
 7fb:	b8 06 00 00 00       	mov    $0x6,%eax
 800:	cd 40                	int    $0x40
 802:	c3                   	retq   

0000000000000803 <exec>:
SYSCALL(exec)
 803:	b8 07 00 00 00       	mov    $0x7,%eax
 808:	cd 40                	int    $0x40
 80a:	c3                   	retq   

000000000000080b <open>:
SYSCALL(open)
 80b:	b8 0f 00 00 00       	mov    $0xf,%eax
 810:	cd 40                	int    $0x40
 812:	c3                   	retq   

0000000000000813 <mknod>:
SYSCALL(mknod)
 813:	b8 11 00 00 00       	mov    $0x11,%eax
 818:	cd 40                	int    $0x40
 81a:	c3                   	retq   

000000000000081b <unlink>:
SYSCALL(unlink)
 81b:	b8 12 00 00 00       	mov    $0x12,%eax
 820:	cd 40                	int    $0x40
 822:	c3                   	retq   

0000000000000823 <fstat>:
SYSCALL(fstat)
 823:	b8 08 00 00 00       	mov    $0x8,%eax
 828:	cd 40                	int    $0x40
 82a:	c3                   	retq   

000000000000082b <link>:
SYSCALL(link)
 82b:	b8 13 00 00 00       	mov    $0x13,%eax
 830:	cd 40                	int    $0x40
 832:	c3                   	retq   

0000000000000833 <mkdir>:
SYSCALL(mkdir)
 833:	b8 14 00 00 00       	mov    $0x14,%eax
 838:	cd 40                	int    $0x40
 83a:	c3                   	retq   

000000000000083b <chdir>:
SYSCALL(chdir)
 83b:	b8 09 00 00 00       	mov    $0x9,%eax
 840:	cd 40                	int    $0x40
 842:	c3                   	retq   

0000000000000843 <dup>:
SYSCALL(dup)
 843:	b8 0a 00 00 00       	mov    $0xa,%eax
 848:	cd 40                	int    $0x40
 84a:	c3                   	retq   

000000000000084b <getpid>:
SYSCALL(getpid)
 84b:	b8 0b 00 00 00       	mov    $0xb,%eax
 850:	cd 40                	int    $0x40
 852:	c3                   	retq   

0000000000000853 <sbrk>:
SYSCALL(sbrk)
 853:	b8 0c 00 00 00       	mov    $0xc,%eax
 858:	cd 40                	int    $0x40
 85a:	c3                   	retq   

000000000000085b <sleep>:
SYSCALL(sleep)
 85b:	b8 0d 00 00 00       	mov    $0xd,%eax
 860:	cd 40                	int    $0x40
 862:	c3                   	retq   

0000000000000863 <uptime>:
SYSCALL(uptime)
 863:	b8 0e 00 00 00       	mov    $0xe,%eax
 868:	cd 40                	int    $0x40
 86a:	c3                   	retq   

000000000000086b <sysinfo>:
SYSCALL(sysinfo)
 86b:	b8 16 00 00 00       	mov    $0x16,%eax
 870:	cd 40                	int    $0x40
 872:	c3                   	retq   

0000000000000873 <crashn>:
SYSCALL(crashn)
 873:	b8 17 00 00 00       	mov    $0x17,%eax
 878:	cd 40                	int    $0x40
 87a:	c3                   	retq   

000000000000087b <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 87b:	55                   	push   %rbp
 87c:	48 89 e5             	mov    %rsp,%rbp
 87f:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 883:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 887:	48 83 e8 10          	sub    $0x10,%rax
 88b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 88f:	48 8b 05 4a 05 00 00 	mov    0x54a(%rip),%rax        # de0 <freep>
 896:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 89a:	eb 2f                	jmp    8cb <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 89c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8a0:	48 8b 00             	mov    (%rax),%rax
 8a3:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8a7:	77 17                	ja     8c0 <free+0x45>
 8a9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8ad:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8b1:	77 2f                	ja     8e2 <free+0x67>
 8b3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8b7:	48 8b 00             	mov    (%rax),%rax
 8ba:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8be:	77 22                	ja     8e2 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8c0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8c4:	48 8b 00             	mov    (%rax),%rax
 8c7:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8cb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8cf:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8d3:	76 c7                	jbe    89c <free+0x21>
 8d5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8d9:	48 8b 00             	mov    (%rax),%rax
 8dc:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8e0:	76 ba                	jbe    89c <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 8e2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8e6:	8b 40 08             	mov    0x8(%rax),%eax
 8e9:	89 c0                	mov    %eax,%eax
 8eb:	48 c1 e0 04          	shl    $0x4,%rax
 8ef:	48 89 c2             	mov    %rax,%rdx
 8f2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8f6:	48 01 c2             	add    %rax,%rdx
 8f9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8fd:	48 8b 00             	mov    (%rax),%rax
 900:	48 39 c2             	cmp    %rax,%rdx
 903:	75 2d                	jne    932 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 905:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 909:	8b 50 08             	mov    0x8(%rax),%edx
 90c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 910:	48 8b 00             	mov    (%rax),%rax
 913:	8b 40 08             	mov    0x8(%rax),%eax
 916:	01 c2                	add    %eax,%edx
 918:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 91c:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 91f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 923:	48 8b 00             	mov    (%rax),%rax
 926:	48 8b 10             	mov    (%rax),%rdx
 929:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 92d:	48 89 10             	mov    %rdx,(%rax)
 930:	eb 0e                	jmp    940 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 932:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 936:	48 8b 10             	mov    (%rax),%rdx
 939:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 93d:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 940:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 944:	8b 40 08             	mov    0x8(%rax),%eax
 947:	89 c0                	mov    %eax,%eax
 949:	48 c1 e0 04          	shl    $0x4,%rax
 94d:	48 89 c2             	mov    %rax,%rdx
 950:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 954:	48 01 d0             	add    %rdx,%rax
 957:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 95b:	75 27                	jne    984 <free+0x109>
    p->s.size += bp->s.size;
 95d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 961:	8b 50 08             	mov    0x8(%rax),%edx
 964:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 968:	8b 40 08             	mov    0x8(%rax),%eax
 96b:	01 c2                	add    %eax,%edx
 96d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 971:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 974:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 978:	48 8b 10             	mov    (%rax),%rdx
 97b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 97f:	48 89 10             	mov    %rdx,(%rax)
 982:	eb 0b                	jmp    98f <free+0x114>
  } else
    p->s.ptr = bp;
 984:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 988:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 98c:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 98f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 993:	48 89 05 46 04 00 00 	mov    %rax,0x446(%rip)        # de0 <freep>
}
 99a:	90                   	nop
 99b:	5d                   	pop    %rbp
 99c:	c3                   	retq   

000000000000099d <morecore>:

static Header *morecore(uint nu) {
 99d:	55                   	push   %rbp
 99e:	48 89 e5             	mov    %rsp,%rbp
 9a1:	48 83 ec 20          	sub    $0x20,%rsp
 9a5:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 9a8:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 9af:	77 07                	ja     9b8 <morecore+0x1b>
    nu = 4096;
 9b1:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 9b8:	8b 45 ec             	mov    -0x14(%rbp),%eax
 9bb:	c1 e0 04             	shl    $0x4,%eax
 9be:	89 c7                	mov    %eax,%edi
 9c0:	e8 8e fe ff ff       	callq  853 <sbrk>
 9c5:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 9c9:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 9ce:	75 07                	jne    9d7 <morecore+0x3a>
    return 0;
 9d0:	b8 00 00 00 00       	mov    $0x0,%eax
 9d5:	eb 29                	jmp    a00 <morecore+0x63>
  hp = (Header *)p;
 9d7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9db:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 9df:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9e3:	8b 55 ec             	mov    -0x14(%rbp),%edx
 9e6:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 9e9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ed:	48 83 c0 10          	add    $0x10,%rax
 9f1:	48 89 c7             	mov    %rax,%rdi
 9f4:	e8 82 fe ff ff       	callq  87b <free>
  return freep;
 9f9:	48 8b 05 e0 03 00 00 	mov    0x3e0(%rip),%rax        # de0 <freep>
}
 a00:	c9                   	leaveq 
 a01:	c3                   	retq   

0000000000000a02 <malloc>:

void *malloc(uint nbytes) {
 a02:	55                   	push   %rbp
 a03:	48 89 e5             	mov    %rsp,%rbp
 a06:	48 83 ec 30          	sub    $0x30,%rsp
 a0a:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a0d:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a10:	48 83 c0 0f          	add    $0xf,%rax
 a14:	48 c1 e8 04          	shr    $0x4,%rax
 a18:	83 c0 01             	add    $0x1,%eax
 a1b:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a1e:	48 8b 05 bb 03 00 00 	mov    0x3bb(%rip),%rax        # de0 <freep>
 a25:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 a29:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 a2e:	75 2b                	jne    a5b <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 a30:	48 c7 45 f0 d0 0d 00 	movq   $0xdd0,-0x10(%rbp)
 a37:	00 
 a38:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a3c:	48 89 05 9d 03 00 00 	mov    %rax,0x39d(%rip)        # de0 <freep>
 a43:	48 8b 05 96 03 00 00 	mov    0x396(%rip),%rax        # de0 <freep>
 a4a:	48 89 05 7f 03 00 00 	mov    %rax,0x37f(%rip)        # dd0 <base>
    base.s.size = 0;
 a51:	c7 05 7d 03 00 00 00 	movl   $0x0,0x37d(%rip)        # dd8 <base+0x8>
 a58:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a5b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a5f:	48 8b 00             	mov    (%rax),%rax
 a62:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a66:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a6a:	8b 40 08             	mov    0x8(%rax),%eax
 a6d:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a70:	72 5f                	jb     ad1 <malloc+0xcf>
      if (p->s.size == nunits)
 a72:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a76:	8b 40 08             	mov    0x8(%rax),%eax
 a79:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a7c:	75 10                	jne    a8e <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 a7e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a82:	48 8b 10             	mov    (%rax),%rdx
 a85:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a89:	48 89 10             	mov    %rdx,(%rax)
 a8c:	eb 2e                	jmp    abc <malloc+0xba>
      else {
        p->s.size -= nunits;
 a8e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a92:	8b 40 08             	mov    0x8(%rax),%eax
 a95:	2b 45 ec             	sub    -0x14(%rbp),%eax
 a98:	89 c2                	mov    %eax,%edx
 a9a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a9e:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 aa1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa5:	8b 40 08             	mov    0x8(%rax),%eax
 aa8:	89 c0                	mov    %eax,%eax
 aaa:	48 c1 e0 04          	shl    $0x4,%rax
 aae:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 ab2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ab6:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ab9:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 abc:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ac0:	48 89 05 19 03 00 00 	mov    %rax,0x319(%rip)        # de0 <freep>
      return (void *)(p + 1);
 ac7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 acb:	48 83 c0 10          	add    $0x10,%rax
 acf:	eb 41                	jmp    b12 <malloc+0x110>
    }
    if (p == freep)
 ad1:	48 8b 05 08 03 00 00 	mov    0x308(%rip),%rax        # de0 <freep>
 ad8:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 adc:	75 1c                	jne    afa <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 ade:	8b 45 ec             	mov    -0x14(%rbp),%eax
 ae1:	89 c7                	mov    %eax,%edi
 ae3:	e8 b5 fe ff ff       	callq  99d <morecore>
 ae8:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 aec:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 af1:	75 07                	jne    afa <malloc+0xf8>
        return 0;
 af3:	b8 00 00 00 00       	mov    $0x0,%eax
 af8:	eb 18                	jmp    b12 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 afa:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 afe:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b02:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b06:	48 8b 00             	mov    (%rax),%rax
 b09:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b0d:	e9 54 ff ff ff       	jmpq   a66 <malloc+0x64>
 b12:	c9                   	leaveq 
 b13:	c3                   	retq   
