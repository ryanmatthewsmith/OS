
out/user/_rm:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <cdefs.h>
#include <stat.h>
#include <user.h>

int main(int argc, char *argv[]) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 20          	sub    $0x20,%rsp
   8:	89 7d ec             	mov    %edi,-0x14(%rbp)
   b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int i;

  if (argc < 2) {
   f:	83 7d ec 01          	cmpl   $0x1,-0x14(%rbp)
  13:	7f 19                	jg     2e <main+0x2e>
    printf(2, "Usage: rm files...\n");
  15:	be 41 0b 00 00       	mov    $0xb41,%esi
  1a:	bf 02 00 00 00       	mov    $0x2,%edi
  1f:	b8 00 00 00 00       	mov    $0x0,%eax
  24:	e8 2c 02 00 00       	callq  255 <printf>
    exit();
  29:	e8 ca 07 00 00       	callq  7f8 <exit>
  }

  for (i = 1; i < argc; i++) {
  2e:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%rbp)
  35:	eb 57                	jmp    8e <main+0x8e>
    if (unlink(argv[i]) < 0) {
  37:	8b 45 fc             	mov    -0x4(%rbp),%eax
  3a:	48 98                	cltq   
  3c:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
  43:	00 
  44:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  48:	48 01 d0             	add    %rdx,%rax
  4b:	48 8b 00             	mov    (%rax),%rax
  4e:	48 89 c7             	mov    %rax,%rdi
  51:	e8 f2 07 00 00       	callq  848 <unlink>
  56:	85 c0                	test   %eax,%eax
  58:	79 30                	jns    8a <main+0x8a>
      printf(2, "rm: %s failed to delete\n", argv[i]);
  5a:	8b 45 fc             	mov    -0x4(%rbp),%eax
  5d:	48 98                	cltq   
  5f:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
  66:	00 
  67:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  6b:	48 01 d0             	add    %rdx,%rax
  6e:	48 8b 00             	mov    (%rax),%rax
  71:	48 89 c2             	mov    %rax,%rdx
  74:	be 55 0b 00 00       	mov    $0xb55,%esi
  79:	bf 02 00 00 00       	mov    $0x2,%edi
  7e:	b8 00 00 00 00       	mov    $0x0,%eax
  83:	e8 cd 01 00 00       	callq  255 <printf>
      break;
  88:	eb 0c                	jmp    96 <main+0x96>
  if (argc < 2) {
    printf(2, "Usage: rm files...\n");
    exit();
  }

  for (i = 1; i < argc; i++) {
  8a:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  8e:	8b 45 fc             	mov    -0x4(%rbp),%eax
  91:	3b 45 ec             	cmp    -0x14(%rbp),%eax
  94:	7c a1                	jl     37 <main+0x37>
      printf(2, "rm: %s failed to delete\n", argv[i]);
      break;
    }
  }

  exit();
  96:	e8 5d 07 00 00       	callq  7f8 <exit>

000000000000009b <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  9b:	55                   	push   %rbp
  9c:	48 89 e5             	mov    %rsp,%rbp
  9f:	48 83 ec 10          	sub    $0x10,%rsp
  a3:	89 7d fc             	mov    %edi,-0x4(%rbp)
  a6:	89 f0                	mov    %esi,%eax
  a8:	88 45 f8             	mov    %al,-0x8(%rbp)
  ab:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  af:	8b 45 fc             	mov    -0x4(%rbp),%eax
  b2:	ba 01 00 00 00       	mov    $0x1,%edx
  b7:	48 89 ce             	mov    %rcx,%rsi
  ba:	89 c7                	mov    %eax,%edi
  bc:	e8 57 07 00 00       	callq  818 <write>
  c1:	90                   	nop
  c2:	c9                   	leaveq 
  c3:	c3                   	retq   

00000000000000c4 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  c4:	55                   	push   %rbp
  c5:	48 89 e5             	mov    %rsp,%rbp
  c8:	48 83 ec 40          	sub    $0x40,%rsp
  cc:	89 7d cc             	mov    %edi,-0x34(%rbp)
  cf:	89 75 c8             	mov    %esi,-0x38(%rbp)
  d2:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  d5:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  d8:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  dc:	74 1f                	je     fd <printint64+0x39>
  de:	8b 45 c8             	mov    -0x38(%rbp),%eax
  e1:	c1 e8 1f             	shr    $0x1f,%eax
  e4:	0f b6 c0             	movzbl %al,%eax
  e7:	89 45 c0             	mov    %eax,-0x40(%rbp)
  ea:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  ee:	74 0d                	je     fd <printint64+0x39>
    x = -xx;
  f0:	8b 45 c8             	mov    -0x38(%rbp),%eax
  f3:	f7 d8                	neg    %eax
  f5:	48 98                	cltq   
  f7:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  fb:	eb 09                	jmp    106 <printint64+0x42>
  else
    x = xx;
  fd:	8b 45 c8             	mov    -0x38(%rbp),%eax
 100:	48 98                	cltq   
 102:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 106:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 10d:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 110:	8d 41 01             	lea    0x1(%rcx),%eax
 113:	89 45 fc             	mov    %eax,-0x4(%rbp)
 116:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 119:	48 63 f0             	movslq %eax,%rsi
 11c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 120:	ba 00 00 00 00       	mov    $0x0,%edx
 125:	48 f7 f6             	div    %rsi
 128:	48 89 d0             	mov    %rdx,%rax
 12b:	0f b6 90 d0 0d 00 00 	movzbl 0xdd0(%rax),%edx
 132:	48 63 c1             	movslq %ecx,%rax
 135:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 139:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 13c:	48 63 f8             	movslq %eax,%rdi
 13f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 143:	ba 00 00 00 00       	mov    $0x0,%edx
 148:	48 f7 f7             	div    %rdi
 14b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 14f:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 154:	75 b7                	jne    10d <printint64+0x49>

  if (sgn)
 156:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 15a:	74 2b                	je     187 <printint64+0xc3>
    buf[i++] = '-';
 15c:	8b 45 fc             	mov    -0x4(%rbp),%eax
 15f:	8d 50 01             	lea    0x1(%rax),%edx
 162:	89 55 fc             	mov    %edx,-0x4(%rbp)
 165:	48 98                	cltq   
 167:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 16c:	eb 19                	jmp    187 <printint64+0xc3>
    putc(fd, buf[i]);
 16e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 171:	48 98                	cltq   
 173:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 178:	0f be d0             	movsbl %al,%edx
 17b:	8b 45 cc             	mov    -0x34(%rbp),%eax
 17e:	89 d6                	mov    %edx,%esi
 180:	89 c7                	mov    %eax,%edi
 182:	e8 14 ff ff ff       	callq  9b <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 187:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 18b:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 18f:	79 dd                	jns    16e <printint64+0xaa>
    putc(fd, buf[i]);
}
 191:	90                   	nop
 192:	c9                   	leaveq 
 193:	c3                   	retq   

0000000000000194 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 194:	55                   	push   %rbp
 195:	48 89 e5             	mov    %rsp,%rbp
 198:	48 83 ec 30          	sub    $0x30,%rsp
 19c:	89 7d dc             	mov    %edi,-0x24(%rbp)
 19f:	89 75 d8             	mov    %esi,-0x28(%rbp)
 1a2:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 1a5:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 1a8:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 1af:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 1b3:	74 17                	je     1cc <printint+0x38>
 1b5:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 1b9:	79 11                	jns    1cc <printint+0x38>
    neg = 1;
 1bb:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 1c2:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1c5:	f7 d8                	neg    %eax
 1c7:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1ca:	eb 06                	jmp    1d2 <printint+0x3e>
  } else {
    x = xx;
 1cc:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1cf:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 1d2:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1d9:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1dc:	8d 41 01             	lea    0x1(%rcx),%eax
 1df:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1e2:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 1e5:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1e8:	ba 00 00 00 00       	mov    $0x0,%edx
 1ed:	f7 f6                	div    %esi
 1ef:	89 d0                	mov    %edx,%eax
 1f1:	89 c0                	mov    %eax,%eax
 1f3:	0f b6 90 f0 0d 00 00 	movzbl 0xdf0(%rax),%edx
 1fa:	48 63 c1             	movslq %ecx,%rax
 1fd:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 201:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 204:	8b 45 f4             	mov    -0xc(%rbp),%eax
 207:	ba 00 00 00 00       	mov    $0x0,%edx
 20c:	f7 f7                	div    %edi
 20e:	89 45 f4             	mov    %eax,-0xc(%rbp)
 211:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 215:	75 c2                	jne    1d9 <printint+0x45>
  if (neg)
 217:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 21b:	74 2b                	je     248 <printint+0xb4>
    buf[i++] = '-';
 21d:	8b 45 fc             	mov    -0x4(%rbp),%eax
 220:	8d 50 01             	lea    0x1(%rax),%edx
 223:	89 55 fc             	mov    %edx,-0x4(%rbp)
 226:	48 98                	cltq   
 228:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 22d:	eb 19                	jmp    248 <printint+0xb4>
    putc(fd, buf[i]);
 22f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 232:	48 98                	cltq   
 234:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 239:	0f be d0             	movsbl %al,%edx
 23c:	8b 45 dc             	mov    -0x24(%rbp),%eax
 23f:	89 d6                	mov    %edx,%esi
 241:	89 c7                	mov    %eax,%edi
 243:	e8 53 fe ff ff       	callq  9b <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 248:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 24c:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 250:	79 dd                	jns    22f <printint+0x9b>
    putc(fd, buf[i]);
}
 252:	90                   	nop
 253:	c9                   	leaveq 
 254:	c3                   	retq   

0000000000000255 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 255:	55                   	push   %rbp
 256:	48 89 e5             	mov    %rsp,%rbp
 259:	48 83 ec 70          	sub    $0x70,%rsp
 25d:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 260:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 264:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 268:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 26c:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 270:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 274:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 27b:	48 8d 45 10          	lea    0x10(%rbp),%rax
 27f:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 283:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 287:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 28b:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 292:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 299:	e9 68 02 00 00       	jmpq   506 <printf+0x2b1>
    c = fmt[i] & 0xff;
 29e:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 2a1:	48 63 d0             	movslq %eax,%rdx
 2a4:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 2a8:	48 01 d0             	add    %rdx,%rax
 2ab:	0f b6 00             	movzbl (%rax),%eax
 2ae:	0f be c0             	movsbl %al,%eax
 2b1:	25 ff 00 00 00       	and    $0xff,%eax
 2b6:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 2b9:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 2bd:	75 30                	jne    2ef <printf+0x9a>
      if (c == '%') {
 2bf:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 2c3:	75 13                	jne    2d8 <printf+0x83>
        state = '%';
 2c5:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 2cc:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 2d3:	e9 2a 02 00 00       	jmpq   502 <printf+0x2ad>
      } else {
        putc(fd, c);
 2d8:	8b 45 b8             	mov    -0x48(%rbp),%eax
 2db:	0f be d0             	movsbl %al,%edx
 2de:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2e1:	89 d6                	mov    %edx,%esi
 2e3:	89 c7                	mov    %eax,%edi
 2e5:	e8 b1 fd ff ff       	callq  9b <putc>
 2ea:	e9 13 02 00 00       	jmpq   502 <printf+0x2ad>
      }
    } else if (state == '%') {
 2ef:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 2f3:	0f 85 09 02 00 00    	jne    502 <printf+0x2ad>
      if (c == 'l') {
 2f9:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 2fd:	75 0c                	jne    30b <printf+0xb6>
        lflag = 1;
 2ff:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 306:	e9 f7 01 00 00       	jmpq   502 <printf+0x2ad>
      } else if (c == 'd') {
 30b:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 30f:	0f 85 95 00 00 00    	jne    3aa <printf+0x155>
        if (lflag == 1)
 315:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 319:	75 49                	jne    364 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 31b:	8b 45 a0             	mov    -0x60(%rbp),%eax
 31e:	83 f8 30             	cmp    $0x30,%eax
 321:	73 17                	jae    33a <printf+0xe5>
 323:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 327:	8b 55 a0             	mov    -0x60(%rbp),%edx
 32a:	89 d2                	mov    %edx,%edx
 32c:	48 01 d0             	add    %rdx,%rax
 32f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 332:	83 c2 08             	add    $0x8,%edx
 335:	89 55 a0             	mov    %edx,-0x60(%rbp)
 338:	eb 0c                	jmp    346 <printf+0xf1>
 33a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 33e:	48 8d 50 08          	lea    0x8(%rax),%rdx
 342:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 346:	48 8b 00             	mov    (%rax),%rax
 349:	89 c6                	mov    %eax,%esi
 34b:	8b 45 9c             	mov    -0x64(%rbp),%eax
 34e:	b9 01 00 00 00       	mov    $0x1,%ecx
 353:	ba 0a 00 00 00       	mov    $0xa,%edx
 358:	89 c7                	mov    %eax,%edi
 35a:	e8 65 fd ff ff       	callq  c4 <printint64>
 35f:	e9 97 01 00 00       	jmpq   4fb <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 364:	8b 45 a0             	mov    -0x60(%rbp),%eax
 367:	83 f8 30             	cmp    $0x30,%eax
 36a:	73 17                	jae    383 <printf+0x12e>
 36c:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 370:	8b 55 a0             	mov    -0x60(%rbp),%edx
 373:	89 d2                	mov    %edx,%edx
 375:	48 01 d0             	add    %rdx,%rax
 378:	8b 55 a0             	mov    -0x60(%rbp),%edx
 37b:	83 c2 08             	add    $0x8,%edx
 37e:	89 55 a0             	mov    %edx,-0x60(%rbp)
 381:	eb 0c                	jmp    38f <printf+0x13a>
 383:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 387:	48 8d 50 08          	lea    0x8(%rax),%rdx
 38b:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 38f:	8b 30                	mov    (%rax),%esi
 391:	8b 45 9c             	mov    -0x64(%rbp),%eax
 394:	b9 01 00 00 00       	mov    $0x1,%ecx
 399:	ba 0a 00 00 00       	mov    $0xa,%edx
 39e:	89 c7                	mov    %eax,%edi
 3a0:	e8 ef fd ff ff       	callq  194 <printint>
 3a5:	e9 51 01 00 00       	jmpq   4fb <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 3aa:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 3ae:	74 0a                	je     3ba <printf+0x165>
 3b0:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 3b4:	0f 85 95 00 00 00    	jne    44f <printf+0x1fa>
        if (lflag == 1)
 3ba:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 3be:	75 49                	jne    409 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 3c0:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3c3:	83 f8 30             	cmp    $0x30,%eax
 3c6:	73 17                	jae    3df <printf+0x18a>
 3c8:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3cc:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3cf:	89 d2                	mov    %edx,%edx
 3d1:	48 01 d0             	add    %rdx,%rax
 3d4:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3d7:	83 c2 08             	add    $0x8,%edx
 3da:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3dd:	eb 0c                	jmp    3eb <printf+0x196>
 3df:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3e3:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3e7:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3eb:	48 8b 00             	mov    (%rax),%rax
 3ee:	89 c6                	mov    %eax,%esi
 3f0:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3f3:	b9 00 00 00 00       	mov    $0x0,%ecx
 3f8:	ba 10 00 00 00       	mov    $0x10,%edx
 3fd:	89 c7                	mov    %eax,%edi
 3ff:	e8 c0 fc ff ff       	callq  c4 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 404:	e9 f2 00 00 00       	jmpq   4fb <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 409:	8b 45 a0             	mov    -0x60(%rbp),%eax
 40c:	83 f8 30             	cmp    $0x30,%eax
 40f:	73 17                	jae    428 <printf+0x1d3>
 411:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 415:	8b 55 a0             	mov    -0x60(%rbp),%edx
 418:	89 d2                	mov    %edx,%edx
 41a:	48 01 d0             	add    %rdx,%rax
 41d:	8b 55 a0             	mov    -0x60(%rbp),%edx
 420:	83 c2 08             	add    $0x8,%edx
 423:	89 55 a0             	mov    %edx,-0x60(%rbp)
 426:	eb 0c                	jmp    434 <printf+0x1df>
 428:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 42c:	48 8d 50 08          	lea    0x8(%rax),%rdx
 430:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 434:	8b 30                	mov    (%rax),%esi
 436:	8b 45 9c             	mov    -0x64(%rbp),%eax
 439:	b9 00 00 00 00       	mov    $0x0,%ecx
 43e:	ba 10 00 00 00       	mov    $0x10,%edx
 443:	89 c7                	mov    %eax,%edi
 445:	e8 4a fd ff ff       	callq  194 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 44a:	e9 ac 00 00 00       	jmpq   4fb <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 44f:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 453:	75 6b                	jne    4c0 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 455:	8b 45 a0             	mov    -0x60(%rbp),%eax
 458:	83 f8 30             	cmp    $0x30,%eax
 45b:	73 17                	jae    474 <printf+0x21f>
 45d:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 461:	8b 55 a0             	mov    -0x60(%rbp),%edx
 464:	89 d2                	mov    %edx,%edx
 466:	48 01 d0             	add    %rdx,%rax
 469:	8b 55 a0             	mov    -0x60(%rbp),%edx
 46c:	83 c2 08             	add    $0x8,%edx
 46f:	89 55 a0             	mov    %edx,-0x60(%rbp)
 472:	eb 0c                	jmp    480 <printf+0x22b>
 474:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 478:	48 8d 50 08          	lea    0x8(%rax),%rdx
 47c:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 480:	48 8b 00             	mov    (%rax),%rax
 483:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 487:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 48c:	75 25                	jne    4b3 <printf+0x25e>
          s = "(null)";
 48e:	48 c7 45 c8 6e 0b 00 	movq   $0xb6e,-0x38(%rbp)
 495:	00 
        for (; *s; s++)
 496:	eb 1b                	jmp    4b3 <printf+0x25e>
          putc(fd, *s);
 498:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 49c:	0f b6 00             	movzbl (%rax),%eax
 49f:	0f be d0             	movsbl %al,%edx
 4a2:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4a5:	89 d6                	mov    %edx,%esi
 4a7:	89 c7                	mov    %eax,%edi
 4a9:	e8 ed fb ff ff       	callq  9b <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 4ae:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 4b3:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 4b7:	0f b6 00             	movzbl (%rax),%eax
 4ba:	84 c0                	test   %al,%al
 4bc:	75 da                	jne    498 <printf+0x243>
 4be:	eb 3b                	jmp    4fb <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 4c0:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 4c4:	75 14                	jne    4da <printf+0x285>
        putc(fd, c);
 4c6:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4c9:	0f be d0             	movsbl %al,%edx
 4cc:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4cf:	89 d6                	mov    %edx,%esi
 4d1:	89 c7                	mov    %eax,%edi
 4d3:	e8 c3 fb ff ff       	callq  9b <putc>
 4d8:	eb 21                	jmp    4fb <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 4da:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4dd:	be 25 00 00 00       	mov    $0x25,%esi
 4e2:	89 c7                	mov    %eax,%edi
 4e4:	e8 b2 fb ff ff       	callq  9b <putc>
        putc(fd, c);
 4e9:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4ec:	0f be d0             	movsbl %al,%edx
 4ef:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4f2:	89 d6                	mov    %edx,%esi
 4f4:	89 c7                	mov    %eax,%edi
 4f6:	e8 a0 fb ff ff       	callq  9b <putc>
      }
      state = 0;
 4fb:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 502:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 506:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 509:	48 63 d0             	movslq %eax,%rdx
 50c:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 510:	48 01 d0             	add    %rdx,%rax
 513:	0f b6 00             	movzbl (%rax),%eax
 516:	84 c0                	test   %al,%al
 518:	0f 85 80 fd ff ff    	jne    29e <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 51e:	90                   	nop
 51f:	c9                   	leaveq 
 520:	c3                   	retq   

0000000000000521 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 521:	55                   	push   %rbp
 522:	48 89 e5             	mov    %rsp,%rbp
 525:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 529:	89 75 f4             	mov    %esi,-0xc(%rbp)
 52c:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 52f:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 533:	8b 55 f0             	mov    -0x10(%rbp),%edx
 536:	8b 45 f4             	mov    -0xc(%rbp),%eax
 539:	48 89 ce             	mov    %rcx,%rsi
 53c:	48 89 f7             	mov    %rsi,%rdi
 53f:	89 d1                	mov    %edx,%ecx
 541:	fc                   	cld    
 542:	f3 aa                	rep stos %al,%es:(%rdi)
 544:	89 ca                	mov    %ecx,%edx
 546:	48 89 fe             	mov    %rdi,%rsi
 549:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 54d:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 550:	90                   	nop
 551:	5d                   	pop    %rbp
 552:	c3                   	retq   

0000000000000553 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 553:	55                   	push   %rbp
 554:	48 89 e5             	mov    %rsp,%rbp
 557:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 55b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 55f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 563:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 567:	90                   	nop
 568:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 56c:	48 8d 50 01          	lea    0x1(%rax),%rdx
 570:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 574:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 578:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 57c:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 580:	0f b6 12             	movzbl (%rdx),%edx
 583:	88 10                	mov    %dl,(%rax)
 585:	0f b6 00             	movzbl (%rax),%eax
 588:	84 c0                	test   %al,%al
 58a:	75 dc                	jne    568 <strcpy+0x15>
    ;
  return os;
 58c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 590:	5d                   	pop    %rbp
 591:	c3                   	retq   

0000000000000592 <strcmp>:

int strcmp(const char *p, const char *q) {
 592:	55                   	push   %rbp
 593:	48 89 e5             	mov    %rsp,%rbp
 596:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 59a:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 59e:	eb 0a                	jmp    5aa <strcmp+0x18>
    p++, q++;
 5a0:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 5a5:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 5aa:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5ae:	0f b6 00             	movzbl (%rax),%eax
 5b1:	84 c0                	test   %al,%al
 5b3:	74 12                	je     5c7 <strcmp+0x35>
 5b5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5b9:	0f b6 10             	movzbl (%rax),%edx
 5bc:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5c0:	0f b6 00             	movzbl (%rax),%eax
 5c3:	38 c2                	cmp    %al,%dl
 5c5:	74 d9                	je     5a0 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 5c7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5cb:	0f b6 00             	movzbl (%rax),%eax
 5ce:	0f b6 d0             	movzbl %al,%edx
 5d1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5d5:	0f b6 00             	movzbl (%rax),%eax
 5d8:	0f b6 c0             	movzbl %al,%eax
 5db:	29 c2                	sub    %eax,%edx
 5dd:	89 d0                	mov    %edx,%eax
}
 5df:	5d                   	pop    %rbp
 5e0:	c3                   	retq   

00000000000005e1 <strlen>:

uint strlen(char *s) {
 5e1:	55                   	push   %rbp
 5e2:	48 89 e5             	mov    %rsp,%rbp
 5e5:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 5e9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 5f0:	eb 04                	jmp    5f6 <strlen+0x15>
 5f2:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 5f6:	8b 45 fc             	mov    -0x4(%rbp),%eax
 5f9:	48 63 d0             	movslq %eax,%rdx
 5fc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 600:	48 01 d0             	add    %rdx,%rax
 603:	0f b6 00             	movzbl (%rax),%eax
 606:	84 c0                	test   %al,%al
 608:	75 e8                	jne    5f2 <strlen+0x11>
    ;
  return n;
 60a:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 60d:	5d                   	pop    %rbp
 60e:	c3                   	retq   

000000000000060f <memset>:

void *memset(void *dst, int c, uint n) {
 60f:	55                   	push   %rbp
 610:	48 89 e5             	mov    %rsp,%rbp
 613:	48 83 ec 10          	sub    $0x10,%rsp
 617:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 61b:	89 75 f4             	mov    %esi,-0xc(%rbp)
 61e:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 621:	8b 55 f0             	mov    -0x10(%rbp),%edx
 624:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 627:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 62b:	89 ce                	mov    %ecx,%esi
 62d:	48 89 c7             	mov    %rax,%rdi
 630:	e8 ec fe ff ff       	callq  521 <stosb>
  return dst;
 635:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 639:	c9                   	leaveq 
 63a:	c3                   	retq   

000000000000063b <strchr>:

char *strchr(const char *s, char c) {
 63b:	55                   	push   %rbp
 63c:	48 89 e5             	mov    %rsp,%rbp
 63f:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 643:	89 f0                	mov    %esi,%eax
 645:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 648:	eb 17                	jmp    661 <strchr+0x26>
    if (*s == c)
 64a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 64e:	0f b6 00             	movzbl (%rax),%eax
 651:	3a 45 f4             	cmp    -0xc(%rbp),%al
 654:	75 06                	jne    65c <strchr+0x21>
      return (char *)s;
 656:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 65a:	eb 15                	jmp    671 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 65c:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 661:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 665:	0f b6 00             	movzbl (%rax),%eax
 668:	84 c0                	test   %al,%al
 66a:	75 de                	jne    64a <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 66c:	b8 00 00 00 00       	mov    $0x0,%eax
}
 671:	5d                   	pop    %rbp
 672:	c3                   	retq   

0000000000000673 <gets>:

char *gets(char *buf, int max) {
 673:	55                   	push   %rbp
 674:	48 89 e5             	mov    %rsp,%rbp
 677:	48 83 ec 20          	sub    $0x20,%rsp
 67b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 67f:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 682:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 689:	eb 48                	jmp    6d3 <gets+0x60>
    cc = read(0, &c, 1);
 68b:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 68f:	ba 01 00 00 00       	mov    $0x1,%edx
 694:	48 89 c6             	mov    %rax,%rsi
 697:	bf 00 00 00 00       	mov    $0x0,%edi
 69c:	e8 6f 01 00 00       	callq  810 <read>
 6a1:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 6a4:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 6a8:	7e 36                	jle    6e0 <gets+0x6d>
      break;
    buf[i++] = c;
 6aa:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6ad:	8d 50 01             	lea    0x1(%rax),%edx
 6b0:	89 55 fc             	mov    %edx,-0x4(%rbp)
 6b3:	48 63 d0             	movslq %eax,%rdx
 6b6:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6ba:	48 01 c2             	add    %rax,%rdx
 6bd:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6c1:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 6c3:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6c7:	3c 0a                	cmp    $0xa,%al
 6c9:	74 16                	je     6e1 <gets+0x6e>
 6cb:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6cf:	3c 0d                	cmp    $0xd,%al
 6d1:	74 0e                	je     6e1 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6d3:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6d6:	83 c0 01             	add    $0x1,%eax
 6d9:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 6dc:	7c ad                	jl     68b <gets+0x18>
 6de:	eb 01                	jmp    6e1 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 6e0:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 6e1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6e4:	48 63 d0             	movslq %eax,%rdx
 6e7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6eb:	48 01 d0             	add    %rdx,%rax
 6ee:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 6f1:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 6f5:	c9                   	leaveq 
 6f6:	c3                   	retq   

00000000000006f7 <stat>:

int stat(char *n, struct stat *st) {
 6f7:	55                   	push   %rbp
 6f8:	48 89 e5             	mov    %rsp,%rbp
 6fb:	48 83 ec 20          	sub    $0x20,%rsp
 6ff:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 703:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 707:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 70b:	be 00 00 00 00       	mov    $0x0,%esi
 710:	48 89 c7             	mov    %rax,%rdi
 713:	e8 20 01 00 00       	callq  838 <open>
 718:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 71b:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 71f:	79 07                	jns    728 <stat+0x31>
    return -1;
 721:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 726:	eb 21                	jmp    749 <stat+0x52>
  r = fstat(fd, st);
 728:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 72c:	8b 45 fc             	mov    -0x4(%rbp),%eax
 72f:	48 89 d6             	mov    %rdx,%rsi
 732:	89 c7                	mov    %eax,%edi
 734:	e8 17 01 00 00       	callq  850 <fstat>
 739:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 73c:	8b 45 fc             	mov    -0x4(%rbp),%eax
 73f:	89 c7                	mov    %eax,%edi
 741:	e8 da 00 00 00       	callq  820 <close>
  return r;
 746:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 749:	c9                   	leaveq 
 74a:	c3                   	retq   

000000000000074b <atoi>:

int atoi(const char *s) {
 74b:	55                   	push   %rbp
 74c:	48 89 e5             	mov    %rsp,%rbp
 74f:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 753:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 75a:	eb 28                	jmp    784 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 75c:	8b 55 fc             	mov    -0x4(%rbp),%edx
 75f:	89 d0                	mov    %edx,%eax
 761:	c1 e0 02             	shl    $0x2,%eax
 764:	01 d0                	add    %edx,%eax
 766:	01 c0                	add    %eax,%eax
 768:	89 c1                	mov    %eax,%ecx
 76a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 76e:	48 8d 50 01          	lea    0x1(%rax),%rdx
 772:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 776:	0f b6 00             	movzbl (%rax),%eax
 779:	0f be c0             	movsbl %al,%eax
 77c:	01 c8                	add    %ecx,%eax
 77e:	83 e8 30             	sub    $0x30,%eax
 781:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 784:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 788:	0f b6 00             	movzbl (%rax),%eax
 78b:	3c 2f                	cmp    $0x2f,%al
 78d:	7e 0b                	jle    79a <atoi+0x4f>
 78f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 793:	0f b6 00             	movzbl (%rax),%eax
 796:	3c 39                	cmp    $0x39,%al
 798:	7e c2                	jle    75c <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 79a:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 79d:	5d                   	pop    %rbp
 79e:	c3                   	retq   

000000000000079f <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 79f:	55                   	push   %rbp
 7a0:	48 89 e5             	mov    %rsp,%rbp
 7a3:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 7a7:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 7ab:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 7ae:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7b2:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 7b6:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 7ba:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 7be:	eb 1d                	jmp    7dd <memmove+0x3e>
    *dst++ = *src++;
 7c0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 7c4:	48 8d 50 01          	lea    0x1(%rax),%rdx
 7c8:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 7cc:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 7d0:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 7d4:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 7d8:	0f b6 12             	movzbl (%rdx),%edx
 7db:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 7dd:	8b 45 dc             	mov    -0x24(%rbp),%eax
 7e0:	8d 50 ff             	lea    -0x1(%rax),%edx
 7e3:	89 55 dc             	mov    %edx,-0x24(%rbp)
 7e6:	85 c0                	test   %eax,%eax
 7e8:	7f d6                	jg     7c0 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 7ea:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7ee:	5d                   	pop    %rbp
 7ef:	c3                   	retq   

00000000000007f0 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 7f0:	b8 01 00 00 00       	mov    $0x1,%eax
 7f5:	cd 40                	int    $0x40
 7f7:	c3                   	retq   

00000000000007f8 <exit>:
SYSCALL(exit)
 7f8:	b8 02 00 00 00       	mov    $0x2,%eax
 7fd:	cd 40                	int    $0x40
 7ff:	c3                   	retq   

0000000000000800 <wait>:
SYSCALL(wait)
 800:	b8 03 00 00 00       	mov    $0x3,%eax
 805:	cd 40                	int    $0x40
 807:	c3                   	retq   

0000000000000808 <pipe>:
SYSCALL(pipe)
 808:	b8 04 00 00 00       	mov    $0x4,%eax
 80d:	cd 40                	int    $0x40
 80f:	c3                   	retq   

0000000000000810 <read>:
SYSCALL(read)
 810:	b8 05 00 00 00       	mov    $0x5,%eax
 815:	cd 40                	int    $0x40
 817:	c3                   	retq   

0000000000000818 <write>:
SYSCALL(write)
 818:	b8 10 00 00 00       	mov    $0x10,%eax
 81d:	cd 40                	int    $0x40
 81f:	c3                   	retq   

0000000000000820 <close>:
SYSCALL(close)
 820:	b8 15 00 00 00       	mov    $0x15,%eax
 825:	cd 40                	int    $0x40
 827:	c3                   	retq   

0000000000000828 <kill>:
SYSCALL(kill)
 828:	b8 06 00 00 00       	mov    $0x6,%eax
 82d:	cd 40                	int    $0x40
 82f:	c3                   	retq   

0000000000000830 <exec>:
SYSCALL(exec)
 830:	b8 07 00 00 00       	mov    $0x7,%eax
 835:	cd 40                	int    $0x40
 837:	c3                   	retq   

0000000000000838 <open>:
SYSCALL(open)
 838:	b8 0f 00 00 00       	mov    $0xf,%eax
 83d:	cd 40                	int    $0x40
 83f:	c3                   	retq   

0000000000000840 <mknod>:
SYSCALL(mknod)
 840:	b8 11 00 00 00       	mov    $0x11,%eax
 845:	cd 40                	int    $0x40
 847:	c3                   	retq   

0000000000000848 <unlink>:
SYSCALL(unlink)
 848:	b8 12 00 00 00       	mov    $0x12,%eax
 84d:	cd 40                	int    $0x40
 84f:	c3                   	retq   

0000000000000850 <fstat>:
SYSCALL(fstat)
 850:	b8 08 00 00 00       	mov    $0x8,%eax
 855:	cd 40                	int    $0x40
 857:	c3                   	retq   

0000000000000858 <link>:
SYSCALL(link)
 858:	b8 13 00 00 00       	mov    $0x13,%eax
 85d:	cd 40                	int    $0x40
 85f:	c3                   	retq   

0000000000000860 <mkdir>:
SYSCALL(mkdir)
 860:	b8 14 00 00 00       	mov    $0x14,%eax
 865:	cd 40                	int    $0x40
 867:	c3                   	retq   

0000000000000868 <chdir>:
SYSCALL(chdir)
 868:	b8 09 00 00 00       	mov    $0x9,%eax
 86d:	cd 40                	int    $0x40
 86f:	c3                   	retq   

0000000000000870 <dup>:
SYSCALL(dup)
 870:	b8 0a 00 00 00       	mov    $0xa,%eax
 875:	cd 40                	int    $0x40
 877:	c3                   	retq   

0000000000000878 <getpid>:
SYSCALL(getpid)
 878:	b8 0b 00 00 00       	mov    $0xb,%eax
 87d:	cd 40                	int    $0x40
 87f:	c3                   	retq   

0000000000000880 <sbrk>:
SYSCALL(sbrk)
 880:	b8 0c 00 00 00       	mov    $0xc,%eax
 885:	cd 40                	int    $0x40
 887:	c3                   	retq   

0000000000000888 <sleep>:
SYSCALL(sleep)
 888:	b8 0d 00 00 00       	mov    $0xd,%eax
 88d:	cd 40                	int    $0x40
 88f:	c3                   	retq   

0000000000000890 <uptime>:
SYSCALL(uptime)
 890:	b8 0e 00 00 00       	mov    $0xe,%eax
 895:	cd 40                	int    $0x40
 897:	c3                   	retq   

0000000000000898 <sysinfo>:
SYSCALL(sysinfo)
 898:	b8 16 00 00 00       	mov    $0x16,%eax
 89d:	cd 40                	int    $0x40
 89f:	c3                   	retq   

00000000000008a0 <crashn>:
SYSCALL(crashn)
 8a0:	b8 17 00 00 00       	mov    $0x17,%eax
 8a5:	cd 40                	int    $0x40
 8a7:	c3                   	retq   

00000000000008a8 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 8a8:	55                   	push   %rbp
 8a9:	48 89 e5             	mov    %rsp,%rbp
 8ac:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 8b0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8b4:	48 83 e8 10          	sub    $0x10,%rax
 8b8:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8bc:	48 8b 05 5d 05 00 00 	mov    0x55d(%rip),%rax        # e20 <freep>
 8c3:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8c7:	eb 2f                	jmp    8f8 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 8c9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8cd:	48 8b 00             	mov    (%rax),%rax
 8d0:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8d4:	77 17                	ja     8ed <free+0x45>
 8d6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8da:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8de:	77 2f                	ja     90f <free+0x67>
 8e0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8e4:	48 8b 00             	mov    (%rax),%rax
 8e7:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8eb:	77 22                	ja     90f <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8ed:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8f1:	48 8b 00             	mov    (%rax),%rax
 8f4:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8f8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8fc:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 900:	76 c7                	jbe    8c9 <free+0x21>
 902:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 906:	48 8b 00             	mov    (%rax),%rax
 909:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 90d:	76 ba                	jbe    8c9 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 90f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 913:	8b 40 08             	mov    0x8(%rax),%eax
 916:	89 c0                	mov    %eax,%eax
 918:	48 c1 e0 04          	shl    $0x4,%rax
 91c:	48 89 c2             	mov    %rax,%rdx
 91f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 923:	48 01 c2             	add    %rax,%rdx
 926:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 92a:	48 8b 00             	mov    (%rax),%rax
 92d:	48 39 c2             	cmp    %rax,%rdx
 930:	75 2d                	jne    95f <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 932:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 936:	8b 50 08             	mov    0x8(%rax),%edx
 939:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 93d:	48 8b 00             	mov    (%rax),%rax
 940:	8b 40 08             	mov    0x8(%rax),%eax
 943:	01 c2                	add    %eax,%edx
 945:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 949:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 94c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 950:	48 8b 00             	mov    (%rax),%rax
 953:	48 8b 10             	mov    (%rax),%rdx
 956:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 95a:	48 89 10             	mov    %rdx,(%rax)
 95d:	eb 0e                	jmp    96d <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 95f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 963:	48 8b 10             	mov    (%rax),%rdx
 966:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 96a:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 96d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 971:	8b 40 08             	mov    0x8(%rax),%eax
 974:	89 c0                	mov    %eax,%eax
 976:	48 c1 e0 04          	shl    $0x4,%rax
 97a:	48 89 c2             	mov    %rax,%rdx
 97d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 981:	48 01 d0             	add    %rdx,%rax
 984:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 988:	75 27                	jne    9b1 <free+0x109>
    p->s.size += bp->s.size;
 98a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 98e:	8b 50 08             	mov    0x8(%rax),%edx
 991:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 995:	8b 40 08             	mov    0x8(%rax),%eax
 998:	01 c2                	add    %eax,%edx
 99a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 99e:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 9a1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9a5:	48 8b 10             	mov    (%rax),%rdx
 9a8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9ac:	48 89 10             	mov    %rdx,(%rax)
 9af:	eb 0b                	jmp    9bc <free+0x114>
  } else
    p->s.ptr = bp;
 9b1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9b5:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 9b9:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 9bc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9c0:	48 89 05 59 04 00 00 	mov    %rax,0x459(%rip)        # e20 <freep>
}
 9c7:	90                   	nop
 9c8:	5d                   	pop    %rbp
 9c9:	c3                   	retq   

00000000000009ca <morecore>:

static Header *morecore(uint nu) {
 9ca:	55                   	push   %rbp
 9cb:	48 89 e5             	mov    %rsp,%rbp
 9ce:	48 83 ec 20          	sub    $0x20,%rsp
 9d2:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 9d5:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 9dc:	77 07                	ja     9e5 <morecore+0x1b>
    nu = 4096;
 9de:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 9e5:	8b 45 ec             	mov    -0x14(%rbp),%eax
 9e8:	c1 e0 04             	shl    $0x4,%eax
 9eb:	89 c7                	mov    %eax,%edi
 9ed:	e8 8e fe ff ff       	callq  880 <sbrk>
 9f2:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 9f6:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 9fb:	75 07                	jne    a04 <morecore+0x3a>
    return 0;
 9fd:	b8 00 00 00 00       	mov    $0x0,%eax
 a02:	eb 29                	jmp    a2d <morecore+0x63>
  hp = (Header *)p;
 a04:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a08:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 a0c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a10:	8b 55 ec             	mov    -0x14(%rbp),%edx
 a13:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 a16:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a1a:	48 83 c0 10          	add    $0x10,%rax
 a1e:	48 89 c7             	mov    %rax,%rdi
 a21:	e8 82 fe ff ff       	callq  8a8 <free>
  return freep;
 a26:	48 8b 05 f3 03 00 00 	mov    0x3f3(%rip),%rax        # e20 <freep>
}
 a2d:	c9                   	leaveq 
 a2e:	c3                   	retq   

0000000000000a2f <malloc>:

void *malloc(uint nbytes) {
 a2f:	55                   	push   %rbp
 a30:	48 89 e5             	mov    %rsp,%rbp
 a33:	48 83 ec 30          	sub    $0x30,%rsp
 a37:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a3a:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a3d:	48 83 c0 0f          	add    $0xf,%rax
 a41:	48 c1 e8 04          	shr    $0x4,%rax
 a45:	83 c0 01             	add    $0x1,%eax
 a48:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a4b:	48 8b 05 ce 03 00 00 	mov    0x3ce(%rip),%rax        # e20 <freep>
 a52:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 a56:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 a5b:	75 2b                	jne    a88 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 a5d:	48 c7 45 f0 10 0e 00 	movq   $0xe10,-0x10(%rbp)
 a64:	00 
 a65:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a69:	48 89 05 b0 03 00 00 	mov    %rax,0x3b0(%rip)        # e20 <freep>
 a70:	48 8b 05 a9 03 00 00 	mov    0x3a9(%rip),%rax        # e20 <freep>
 a77:	48 89 05 92 03 00 00 	mov    %rax,0x392(%rip)        # e10 <base>
    base.s.size = 0;
 a7e:	c7 05 90 03 00 00 00 	movl   $0x0,0x390(%rip)        # e18 <base+0x8>
 a85:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a88:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a8c:	48 8b 00             	mov    (%rax),%rax
 a8f:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a93:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a97:	8b 40 08             	mov    0x8(%rax),%eax
 a9a:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a9d:	72 5f                	jb     afe <malloc+0xcf>
      if (p->s.size == nunits)
 a9f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa3:	8b 40 08             	mov    0x8(%rax),%eax
 aa6:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 aa9:	75 10                	jne    abb <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 aab:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aaf:	48 8b 10             	mov    (%rax),%rdx
 ab2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ab6:	48 89 10             	mov    %rdx,(%rax)
 ab9:	eb 2e                	jmp    ae9 <malloc+0xba>
      else {
        p->s.size -= nunits;
 abb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 abf:	8b 40 08             	mov    0x8(%rax),%eax
 ac2:	2b 45 ec             	sub    -0x14(%rbp),%eax
 ac5:	89 c2                	mov    %eax,%edx
 ac7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 acb:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 ace:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ad2:	8b 40 08             	mov    0x8(%rax),%eax
 ad5:	89 c0                	mov    %eax,%eax
 ad7:	48 c1 e0 04          	shl    $0x4,%rax
 adb:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 adf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ae3:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ae6:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 ae9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 aed:	48 89 05 2c 03 00 00 	mov    %rax,0x32c(%rip)        # e20 <freep>
      return (void *)(p + 1);
 af4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 af8:	48 83 c0 10          	add    $0x10,%rax
 afc:	eb 41                	jmp    b3f <malloc+0x110>
    }
    if (p == freep)
 afe:	48 8b 05 1b 03 00 00 	mov    0x31b(%rip),%rax        # e20 <freep>
 b05:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 b09:	75 1c                	jne    b27 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 b0b:	8b 45 ec             	mov    -0x14(%rbp),%eax
 b0e:	89 c7                	mov    %eax,%edi
 b10:	e8 b5 fe ff ff       	callq  9ca <morecore>
 b15:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 b19:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 b1e:	75 07                	jne    b27 <malloc+0xf8>
        return 0;
 b20:	b8 00 00 00 00       	mov    $0x0,%eax
 b25:	eb 18                	jmp    b3f <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b27:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b2b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b2f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b33:	48 8b 00             	mov    (%rax),%rax
 b36:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b3a:	e9 54 ff ff ff       	jmpq   a93 <malloc+0x64>
 b3f:	c9                   	leaveq 
 b40:	c3                   	retq   
