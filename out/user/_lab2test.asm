
out/user/_lab2test:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
void pkilltest(void);
void fdesctest(void);
void childpidtest(void);
void exectest(void);

int main() {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
       4:	48 83 ec 10          	sub    $0x10,%rsp
  int pid, wpid;

  if (open("console", O_RDWR) < 0) {
       8:	be 02 00 00 00       	mov    $0x2,%esi
       d:	bf e8 1c 00 00       	mov    $0x1ce8,%edi
      12:	e8 c1 19 00 00       	callq  19d8 <open>
      17:	85 c0                	test   %eax,%eax
      19:	79 0a                	jns    25 <main+0x25>
    return -1;
      1b:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
      20:	e9 9f 00 00 00       	jmpq   c4 <main+0xc4>
  }
  dup(0); // stdout
      25:	bf 00 00 00 00       	mov    $0x0,%edi
      2a:	e8 e1 19 00 00       	callq  1a10 <dup>
  dup(0); // stderr
      2f:	bf 00 00 00 00       	mov    $0x0,%edi
      34:	e8 d7 19 00 00       	callq  1a10 <dup>

  pid = fork();
      39:	e8 52 19 00 00       	callq  1990 <fork>
      3e:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (pid < 0) {
      41:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
      45:	79 19                	jns    60 <main+0x60>
    printf(1, "fork failed\n");
      47:	be f0 1c 00 00       	mov    $0x1cf0,%esi
      4c:	bf 01 00 00 00       	mov    $0x1,%edi
      51:	b8 00 00 00 00       	mov    $0x0,%eax
      56:	e8 9a 13 00 00       	callq  13f5 <printf>
    exit();
      5b:	e8 38 19 00 00       	callq  1998 <exit>
  }

  if (pid == 0) {
      60:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
      64:	75 43                	jne    a9 <main+0xa9>
    forktest();
      66:	e8 5b 00 00 00       	callq  c6 <forktest>
    pipetest();
      6b:	e8 d4 01 00 00       	callq  244 <pipetest>
    fdesctest();
      70:	e8 7e 07 00 00       	callq  7f3 <fdesctest>
    //pkilltest();
    //racetest();
    childpidtest();
      75:	e8 aa 0d 00 00       	callq  e24 <childpidtest>
    exectest();
      7a:	e8 de 0d 00 00       	callq  e5d <exectest>

    printf(1, "lab2 tests passed!!\n");
      7f:	be fd 1c 00 00       	mov    $0x1cfd,%esi
      84:	bf 01 00 00 00       	mov    $0x1,%edi
      89:	b8 00 00 00 00       	mov    $0x0,%eax
      8e:	e8 62 13 00 00       	callq  13f5 <printf>

    while (1)
      ;
      93:	eb fe                	jmp    93 <main+0x93>
  }

  while ((wpid = wait()) >= 0 && wpid != pid)
    printf(1, "zombie!\n");
      95:	be 12 1d 00 00       	mov    $0x1d12,%esi
      9a:	bf 01 00 00 00       	mov    $0x1,%edi
      9f:	b8 00 00 00 00       	mov    $0x0,%eax
      a4:	e8 4c 13 00 00       	callq  13f5 <printf>

    while (1)
      ;
  }

  while ((wpid = wait()) >= 0 && wpid != pid)
      a9:	e8 f2 18 00 00       	callq  19a0 <wait>
      ae:	89 45 f8             	mov    %eax,-0x8(%rbp)
      b1:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
      b5:	78 08                	js     bf <main+0xbf>
      b7:	8b 45 f8             	mov    -0x8(%rbp),%eax
      ba:	3b 45 fc             	cmp    -0x4(%rbp),%eax
      bd:	75 d6                	jne    95 <main+0x95>
    printf(1, "zombie!\n");

  exit();
      bf:	e8 d4 18 00 00       	callq  1998 <exit>
  return 0;
}
      c4:	c9                   	leaveq 
      c5:	c3                   	retq   

00000000000000c6 <forktest>:

void forktest(void) {
      c6:	55                   	push   %rbp
      c7:	48 89 e5             	mov    %rsp,%rbp
      ca:	48 83 ec 10          	sub    $0x10,%rsp
  int n, pid;
  int nproc = 6;
      ce:	c7 45 f8 06 00 00 00 	movl   $0x6,-0x8(%rbp)

  printf(1, "forktest\n");
      d5:	be 1b 1d 00 00       	mov    $0x1d1b,%esi
      da:	bf 01 00 00 00       	mov    $0x1,%edi
      df:	b8 00 00 00 00       	mov    $0x0,%eax
      e4:	e8 0c 13 00 00       	callq  13f5 <printf>

  for (n = 0; n < nproc; n++) {
      e9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
      f0:	eb 1d                	jmp    10f <forktest+0x49>
    pid = fork();
      f2:	e8 99 18 00 00       	callq  1990 <fork>
      f7:	89 45 f4             	mov    %eax,-0xc(%rbp)
    if (pid < 0)
      fa:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
      fe:	78 19                	js     119 <forktest+0x53>
      break;
    if (pid == 0) {
     100:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     104:	75 05                	jne    10b <forktest+0x45>
      exit();
     106:	e8 8d 18 00 00       	callq  1998 <exit>
  int n, pid;
  int nproc = 6;

  printf(1, "forktest\n");

  for (n = 0; n < nproc; n++) {
     10b:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     10f:	8b 45 fc             	mov    -0x4(%rbp),%eax
     112:	3b 45 f8             	cmp    -0x8(%rbp),%eax
     115:	7c db                	jl     f2 <forktest+0x2c>
     117:	eb 01                	jmp    11a <forktest+0x54>
    pid = fork();
    if (pid < 0)
      break;
     119:	90                   	nop
      exit();
      error("forktest: exit failed to destroy this process");
    }
  }

  if (n != nproc) {
     11a:	8b 45 fc             	mov    -0x4(%rbp),%eax
     11d:	3b 45 f8             	cmp    -0x8(%rbp),%eax
     120:	0f 84 ab 00 00 00    	je     1d1 <forktest+0x10b>
    error("forktest: fork claimed to work %d times! but only %d\n", nproc, n);
     126:	8b 05 a4 24 00 00    	mov    0x24a4(%rip),%eax        # 25d0 <stdout>
     12c:	ba 59 00 00 00       	mov    $0x59,%edx
     131:	be 25 1d 00 00       	mov    $0x1d25,%esi
     136:	89 c7                	mov    %eax,%edi
     138:	b8 00 00 00 00       	mov    $0x0,%eax
     13d:	e8 b3 12 00 00       	callq  13f5 <printf>
     142:	8b 05 88 24 00 00    	mov    0x2488(%rip),%eax        # 25d0 <stdout>
     148:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     14b:	8b 55 f8             	mov    -0x8(%rbp),%edx
     14e:	be 38 1d 00 00       	mov    $0x1d38,%esi
     153:	89 c7                	mov    %eax,%edi
     155:	b8 00 00 00 00       	mov    $0x0,%eax
     15a:	e8 96 12 00 00       	callq  13f5 <printf>
     15f:	8b 05 6b 24 00 00    	mov    0x246b(%rip),%eax        # 25d0 <stdout>
     165:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     16a:	89 c7                	mov    %eax,%edi
     16c:	b8 00 00 00 00       	mov    $0x0,%eax
     171:	e8 7f 12 00 00       	callq  13f5 <printf>
     176:	eb fe                	jmp    176 <forktest+0xb0>
  }

  for (; n > 0; n--) {
    if (wait() < 0) {
     178:	e8 23 18 00 00       	callq  19a0 <wait>
     17d:	85 c0                	test   %eax,%eax
     17f:	79 4c                	jns    1cd <forktest+0x107>
      error("forktest: wait stopped early\n");
     181:	8b 05 49 24 00 00    	mov    0x2449(%rip),%eax        # 25d0 <stdout>
     187:	ba 5e 00 00 00       	mov    $0x5e,%edx
     18c:	be 25 1d 00 00       	mov    $0x1d25,%esi
     191:	89 c7                	mov    %eax,%edi
     193:	b8 00 00 00 00       	mov    $0x0,%eax
     198:	e8 58 12 00 00       	callq  13f5 <printf>
     19d:	8b 05 2d 24 00 00    	mov    0x242d(%rip),%eax        # 25d0 <stdout>
     1a3:	be 70 1d 00 00       	mov    $0x1d70,%esi
     1a8:	89 c7                	mov    %eax,%edi
     1aa:	b8 00 00 00 00       	mov    $0x0,%eax
     1af:	e8 41 12 00 00       	callq  13f5 <printf>
     1b4:	8b 05 16 24 00 00    	mov    0x2416(%rip),%eax        # 25d0 <stdout>
     1ba:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     1bf:	89 c7                	mov    %eax,%edi
     1c1:	b8 00 00 00 00       	mov    $0x0,%eax
     1c6:	e8 2a 12 00 00       	callq  13f5 <printf>
     1cb:	eb fe                	jmp    1cb <forktest+0x105>

  if (n != nproc) {
    error("forktest: fork claimed to work %d times! but only %d\n", nproc, n);
  }

  for (; n > 0; n--) {
     1cd:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     1d1:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     1d5:	7f a1                	jg     178 <forktest+0xb2>
    if (wait() < 0) {
      error("forktest: wait stopped early\n");
    }
  }

  if (wait() != -1) {
     1d7:	e8 c4 17 00 00       	callq  19a0 <wait>
     1dc:	83 f8 ff             	cmp    $0xffffffff,%eax
     1df:	74 4c                	je     22d <forktest+0x167>
    error("forktest: wait got too many\n");
     1e1:	8b 05 e9 23 00 00    	mov    0x23e9(%rip),%eax        # 25d0 <stdout>
     1e7:	ba 63 00 00 00       	mov    $0x63,%edx
     1ec:	be 25 1d 00 00       	mov    $0x1d25,%esi
     1f1:	89 c7                	mov    %eax,%edi
     1f3:	b8 00 00 00 00       	mov    $0x0,%eax
     1f8:	e8 f8 11 00 00       	callq  13f5 <printf>
     1fd:	8b 05 cd 23 00 00    	mov    0x23cd(%rip),%eax        # 25d0 <stdout>
     203:	be 8e 1d 00 00       	mov    $0x1d8e,%esi
     208:	89 c7                	mov    %eax,%edi
     20a:	b8 00 00 00 00       	mov    $0x0,%eax
     20f:	e8 e1 11 00 00       	callq  13f5 <printf>
     214:	8b 05 b6 23 00 00    	mov    0x23b6(%rip),%eax        # 25d0 <stdout>
     21a:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     21f:	89 c7                	mov    %eax,%edi
     221:	b8 00 00 00 00       	mov    $0x0,%eax
     226:	e8 ca 11 00 00       	callq  13f5 <printf>
     22b:	eb fe                	jmp    22b <forktest+0x165>
  }

  printf(1, "forktest: fork test OK\n");
     22d:	be ab 1d 00 00       	mov    $0x1dab,%esi
     232:	bf 01 00 00 00       	mov    $0x1,%edi
     237:	b8 00 00 00 00       	mov    $0x0,%eax
     23c:	e8 b4 11 00 00       	callq  13f5 <printf>
}
     241:	90                   	nop
     242:	c9                   	leaveq 
     243:	c3                   	retq   

0000000000000244 <pipetest>:

void pipetest(void) {
     244:	55                   	push   %rbp
     245:	48 89 e5             	mov    %rsp,%rbp
     248:	48 81 ec 20 02 00 00 	sub    $0x220,%rsp
  char buf[500];
  int fds[2], pid;
  int seq, i, n, cc, total;

  if (pipe(fds) != 0) {
     24f:	48 8d 85 ec fd ff ff 	lea    -0x214(%rbp),%rax
     256:	48 89 c7             	mov    %rax,%rdi
     259:	e8 4a 17 00 00       	callq  19a8 <pipe>
     25e:	85 c0                	test   %eax,%eax
     260:	74 4c                	je     2ae <pipetest+0x6a>
    error("pipetest: pipe() failed\n");
     262:	8b 05 68 23 00 00    	mov    0x2368(%rip),%eax        # 25d0 <stdout>
     268:	ba 6f 00 00 00       	mov    $0x6f,%edx
     26d:	be 25 1d 00 00       	mov    $0x1d25,%esi
     272:	89 c7                	mov    %eax,%edi
     274:	b8 00 00 00 00       	mov    $0x0,%eax
     279:	e8 77 11 00 00       	callq  13f5 <printf>
     27e:	8b 05 4c 23 00 00    	mov    0x234c(%rip),%eax        # 25d0 <stdout>
     284:	be c3 1d 00 00       	mov    $0x1dc3,%esi
     289:	89 c7                	mov    %eax,%edi
     28b:	b8 00 00 00 00       	mov    $0x0,%eax
     290:	e8 60 11 00 00       	callq  13f5 <printf>
     295:	8b 05 35 23 00 00    	mov    0x2335(%rip),%eax        # 25d0 <stdout>
     29b:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     2a0:	89 c7                	mov    %eax,%edi
     2a2:	b8 00 00 00 00       	mov    $0x0,%eax
     2a7:	e8 49 11 00 00       	callq  13f5 <printf>
     2ac:	eb fe                	jmp    2ac <pipetest+0x68>
  }
  pid = fork();
     2ae:	e8 dd 16 00 00       	callq  1990 <fork>
     2b3:	89 45 e8             	mov    %eax,-0x18(%rbp)
  seq = 0;
     2b6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  if (pid == 0) {
     2bd:	83 7d e8 00          	cmpl   $0x0,-0x18(%rbp)
     2c1:	0f 85 c3 00 00 00    	jne    38a <pipetest+0x146>
    close(fds[0]);
     2c7:	8b 85 ec fd ff ff    	mov    -0x214(%rbp),%eax
     2cd:	89 c7                	mov    %eax,%edi
     2cf:	e8 ec 16 00 00       	callq  19c0 <close>
    for (n = 0; n < 5; n++) {
     2d4:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%rbp)
     2db:	e9 9b 00 00 00       	jmpq   37b <pipetest+0x137>
      for (i = 0; i < 95; i++)
     2e0:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     2e7:	eb 1b                	jmp    304 <pipetest+0xc0>
        buf[i] = seq++;
     2e9:	8b 45 fc             	mov    -0x4(%rbp),%eax
     2ec:	8d 50 01             	lea    0x1(%rax),%edx
     2ef:	89 55 fc             	mov    %edx,-0x4(%rbp)
     2f2:	89 c2                	mov    %eax,%edx
     2f4:	8b 45 f8             	mov    -0x8(%rbp),%eax
     2f7:	48 98                	cltq   
     2f9:	88 94 05 f4 fd ff ff 	mov    %dl,-0x20c(%rbp,%rax,1)
  pid = fork();
  seq = 0;
  if (pid == 0) {
    close(fds[0]);
    for (n = 0; n < 5; n++) {
      for (i = 0; i < 95; i++)
     300:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     304:	83 7d f8 5e          	cmpl   $0x5e,-0x8(%rbp)
     308:	7e df                	jle    2e9 <pipetest+0xa5>
        buf[i] = seq++;
      if (write(fds[1], buf, 95) != 95) {
     30a:	8b 85 f0 fd ff ff    	mov    -0x210(%rbp),%eax
     310:	48 8d 8d f4 fd ff ff 	lea    -0x20c(%rbp),%rcx
     317:	ba 5f 00 00 00       	mov    $0x5f,%edx
     31c:	48 89 ce             	mov    %rcx,%rsi
     31f:	89 c7                	mov    %eax,%edi
     321:	e8 92 16 00 00       	callq  19b8 <write>
     326:	83 f8 5f             	cmp    $0x5f,%eax
     329:	74 4c                	je     377 <pipetest+0x133>
        error("pipetest: oops 1\n");
     32b:	8b 05 9f 22 00 00    	mov    0x229f(%rip),%eax        # 25d0 <stdout>
     331:	ba 79 00 00 00       	mov    $0x79,%edx
     336:	be 25 1d 00 00       	mov    $0x1d25,%esi
     33b:	89 c7                	mov    %eax,%edi
     33d:	b8 00 00 00 00       	mov    $0x0,%eax
     342:	e8 ae 10 00 00       	callq  13f5 <printf>
     347:	8b 05 83 22 00 00    	mov    0x2283(%rip),%eax        # 25d0 <stdout>
     34d:	be dc 1d 00 00       	mov    $0x1ddc,%esi
     352:	89 c7                	mov    %eax,%edi
     354:	b8 00 00 00 00       	mov    $0x0,%eax
     359:	e8 97 10 00 00       	callq  13f5 <printf>
     35e:	8b 05 6c 22 00 00    	mov    0x226c(%rip),%eax        # 25d0 <stdout>
     364:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     369:	89 c7                	mov    %eax,%edi
     36b:	b8 00 00 00 00       	mov    $0x0,%eax
     370:	e8 80 10 00 00       	callq  13f5 <printf>
     375:	eb fe                	jmp    375 <pipetest+0x131>
  }
  pid = fork();
  seq = 0;
  if (pid == 0) {
    close(fds[0]);
    for (n = 0; n < 5; n++) {
     377:	83 45 f4 01          	addl   $0x1,-0xc(%rbp)
     37b:	83 7d f4 04          	cmpl   $0x4,-0xc(%rbp)
     37f:	0f 8e 5b ff ff ff    	jle    2e0 <pipetest+0x9c>
        buf[i] = seq++;
      if (write(fds[1], buf, 95) != 95) {
        error("pipetest: oops 1\n");
      }
    }
    exit();
     385:	e8 0e 16 00 00       	callq  1998 <exit>
  } else if (pid > 0) {
     38a:	83 7d e8 00          	cmpl   $0x0,-0x18(%rbp)
     38e:	0f 8e 50 01 00 00    	jle    4e4 <pipetest+0x2a0>
    close(fds[1]);
     394:	8b 85 f0 fd ff ff    	mov    -0x210(%rbp),%eax
     39a:	89 c7                	mov    %eax,%edi
     39c:	e8 1f 16 00 00       	callq  19c0 <close>
    total = 0;
     3a1:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%rbp)
    cc = 1;
     3a8:	c7 45 f0 01 00 00 00 	movl   $0x1,-0x10(%rbp)
    while ((n = read(fds[0], buf, cc)) > 0) {
     3af:	e9 9d 00 00 00       	jmpq   451 <pipetest+0x20d>
      for (i = 0; i < n; i++) {
     3b4:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     3bb:	eb 72                	jmp    42f <pipetest+0x1eb>
        if ((buf[i] & 0xff) != (seq++ & 0xff)) {
     3bd:	8b 45 f8             	mov    -0x8(%rbp),%eax
     3c0:	48 98                	cltq   
     3c2:	0f b6 84 05 f4 fd ff 	movzbl -0x20c(%rbp,%rax,1),%eax
     3c9:	ff 
     3ca:	0f be c8             	movsbl %al,%ecx
     3cd:	8b 45 fc             	mov    -0x4(%rbp),%eax
     3d0:	8d 50 01             	lea    0x1(%rax),%edx
     3d3:	89 55 fc             	mov    %edx,-0x4(%rbp)
     3d6:	31 c8                	xor    %ecx,%eax
     3d8:	0f b6 c0             	movzbl %al,%eax
     3db:	85 c0                	test   %eax,%eax
     3dd:	74 4c                	je     42b <pipetest+0x1e7>
          error("pipetest: oops 2\n");
     3df:	8b 05 eb 21 00 00    	mov    0x21eb(%rip),%eax        # 25d0 <stdout>
     3e5:	ba 84 00 00 00       	mov    $0x84,%edx
     3ea:	be 25 1d 00 00       	mov    $0x1d25,%esi
     3ef:	89 c7                	mov    %eax,%edi
     3f1:	b8 00 00 00 00       	mov    $0x0,%eax
     3f6:	e8 fa 0f 00 00       	callq  13f5 <printf>
     3fb:	8b 05 cf 21 00 00    	mov    0x21cf(%rip),%eax        # 25d0 <stdout>
     401:	be ee 1d 00 00       	mov    $0x1dee,%esi
     406:	89 c7                	mov    %eax,%edi
     408:	b8 00 00 00 00       	mov    $0x0,%eax
     40d:	e8 e3 0f 00 00       	callq  13f5 <printf>
     412:	8b 05 b8 21 00 00    	mov    0x21b8(%rip),%eax        # 25d0 <stdout>
     418:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     41d:	89 c7                	mov    %eax,%edi
     41f:	b8 00 00 00 00       	mov    $0x0,%eax
     424:	e8 cc 0f 00 00       	callq  13f5 <printf>
     429:	eb fe                	jmp    429 <pipetest+0x1e5>
  } else if (pid > 0) {
    close(fds[1]);
    total = 0;
    cc = 1;
    while ((n = read(fds[0], buf, cc)) > 0) {
      for (i = 0; i < n; i++) {
     42b:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     42f:	8b 45 f8             	mov    -0x8(%rbp),%eax
     432:	3b 45 f4             	cmp    -0xc(%rbp),%eax
     435:	7c 86                	jl     3bd <pipetest+0x179>
        if ((buf[i] & 0xff) != (seq++ & 0xff)) {
          error("pipetest: oops 2\n");
        }
      }
      total += n;
     437:	8b 45 f4             	mov    -0xc(%rbp),%eax
     43a:	01 45 ec             	add    %eax,-0x14(%rbp)
      cc = cc * 2;
     43d:	d1 65 f0             	shll   -0x10(%rbp)
      if (cc > sizeof(buf))
     440:	8b 45 f0             	mov    -0x10(%rbp),%eax
     443:	3d f4 01 00 00       	cmp    $0x1f4,%eax
     448:	76 07                	jbe    451 <pipetest+0x20d>
        cc = sizeof(buf);
     44a:	c7 45 f0 f4 01 00 00 	movl   $0x1f4,-0x10(%rbp)
    exit();
  } else if (pid > 0) {
    close(fds[1]);
    total = 0;
    cc = 1;
    while ((n = read(fds[0], buf, cc)) > 0) {
     451:	8b 85 ec fd ff ff    	mov    -0x214(%rbp),%eax
     457:	8b 55 f0             	mov    -0x10(%rbp),%edx
     45a:	48 8d 8d f4 fd ff ff 	lea    -0x20c(%rbp),%rcx
     461:	48 89 ce             	mov    %rcx,%rsi
     464:	89 c7                	mov    %eax,%edi
     466:	e8 45 15 00 00       	callq  19b0 <read>
     46b:	89 45 f4             	mov    %eax,-0xc(%rbp)
     46e:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     472:	0f 8f 3c ff ff ff    	jg     3b4 <pipetest+0x170>
      total += n;
      cc = cc * 2;
      if (cc > sizeof(buf))
        cc = sizeof(buf);
    }
    if (total != 5 * 95) {
     478:	81 7d ec db 01 00 00 	cmpl   $0x1db,-0x14(%rbp)
     47f:	74 4f                	je     4d0 <pipetest+0x28c>
      error("pipetest: oops 3 total %d\n", total);
     481:	8b 05 49 21 00 00    	mov    0x2149(%rip),%eax        # 25d0 <stdout>
     487:	ba 8d 00 00 00       	mov    $0x8d,%edx
     48c:	be 25 1d 00 00       	mov    $0x1d25,%esi
     491:	89 c7                	mov    %eax,%edi
     493:	b8 00 00 00 00       	mov    $0x0,%eax
     498:	e8 58 0f 00 00       	callq  13f5 <printf>
     49d:	8b 05 2d 21 00 00    	mov    0x212d(%rip),%eax        # 25d0 <stdout>
     4a3:	8b 55 ec             	mov    -0x14(%rbp),%edx
     4a6:	be 00 1e 00 00       	mov    $0x1e00,%esi
     4ab:	89 c7                	mov    %eax,%edi
     4ad:	b8 00 00 00 00       	mov    $0x0,%eax
     4b2:	e8 3e 0f 00 00       	callq  13f5 <printf>
     4b7:	8b 05 13 21 00 00    	mov    0x2113(%rip),%eax        # 25d0 <stdout>
     4bd:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     4c2:	89 c7                	mov    %eax,%edi
     4c4:	b8 00 00 00 00       	mov    $0x0,%eax
     4c9:	e8 27 0f 00 00       	callq  13f5 <printf>
     4ce:	eb fe                	jmp    4ce <pipetest+0x28a>
    }
    close(fds[0]);
     4d0:	8b 85 ec fd ff ff    	mov    -0x214(%rbp),%eax
     4d6:	89 c7                	mov    %eax,%edi
     4d8:	e8 e3 14 00 00       	callq  19c0 <close>
    wait();
     4dd:	e8 be 14 00 00       	callq  19a0 <wait>
     4e2:	eb 4c                	jmp    530 <pipetest+0x2ec>
  } else {
    error("pipetest: fork() failed\n");
     4e4:	8b 05 e6 20 00 00    	mov    0x20e6(%rip),%eax        # 25d0 <stdout>
     4ea:	ba 92 00 00 00       	mov    $0x92,%edx
     4ef:	be 25 1d 00 00       	mov    $0x1d25,%esi
     4f4:	89 c7                	mov    %eax,%edi
     4f6:	b8 00 00 00 00       	mov    $0x0,%eax
     4fb:	e8 f5 0e 00 00       	callq  13f5 <printf>
     500:	8b 05 ca 20 00 00    	mov    0x20ca(%rip),%eax        # 25d0 <stdout>
     506:	be 1b 1e 00 00       	mov    $0x1e1b,%esi
     50b:	89 c7                	mov    %eax,%edi
     50d:	b8 00 00 00 00       	mov    $0x0,%eax
     512:	e8 de 0e 00 00       	callq  13f5 <printf>
     517:	8b 05 b3 20 00 00    	mov    0x20b3(%rip),%eax        # 25d0 <stdout>
     51d:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     522:	89 c7                	mov    %eax,%edi
     524:	b8 00 00 00 00       	mov    $0x0,%eax
     529:	e8 c7 0e 00 00       	callq  13f5 <printf>
     52e:	eb fe                	jmp    52e <pipetest+0x2ea>
  }
  printf(1, "pipetest ok\n");
     530:	be 34 1e 00 00       	mov    $0x1e34,%esi
     535:	bf 01 00 00 00       	mov    $0x1,%edi
     53a:	b8 00 00 00 00       	mov    $0x0,%eax
     53f:	e8 b1 0e 00 00       	callq  13f5 <printf>
}
     544:	90                   	nop
     545:	c9                   	leaveq 
     546:	c3                   	retq   

0000000000000547 <pkilltest>:

void pkilltest(void) {
     547:	55                   	push   %rbp
     548:	48 89 e5             	mov    %rsp,%rbp
     54b:	48 83 ec 20          	sub    $0x20,%rsp
  char buf[11];

  int pid1, pid2, pid3;
  int pfds[2];

  printf(1, "preempt: ");
     54f:	be 41 1e 00 00       	mov    $0x1e41,%esi
     554:	bf 01 00 00 00       	mov    $0x1,%edi
     559:	b8 00 00 00 00       	mov    $0x0,%eax
     55e:	e8 92 0e 00 00       	callq  13f5 <printf>
  pid1 = fork();
     563:	e8 28 14 00 00       	callq  1990 <fork>
     568:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (pid1 == 0)
     56b:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     56f:	75 02                	jne    573 <pkilltest+0x2c>
    for (;;)
      ;
     571:	eb fe                	jmp    571 <pkilltest+0x2a>

  pid2 = fork();
     573:	e8 18 14 00 00       	callq  1990 <fork>
     578:	89 45 f8             	mov    %eax,-0x8(%rbp)
  if (pid2 == 0)
     57b:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     57f:	75 02                	jne    583 <pkilltest+0x3c>
    for (;;)
      ;
     581:	eb fe                	jmp    581 <pkilltest+0x3a>

  pipe(pfds);
     583:	48 8d 45 e0          	lea    -0x20(%rbp),%rax
     587:	48 89 c7             	mov    %rax,%rdi
     58a:	e8 19 14 00 00       	callq  19a8 <pipe>
  pid3 = fork();
     58f:	e8 fc 13 00 00       	callq  1990 <fork>
     594:	89 45 f4             	mov    %eax,-0xc(%rbp)
  if (pid3 == 0) {
     597:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     59b:	75 7b                	jne    618 <pkilltest+0xd1>
    close(pfds[0]);
     59d:	8b 45 e0             	mov    -0x20(%rbp),%eax
     5a0:	89 c7                	mov    %eax,%edi
     5a2:	e8 19 14 00 00       	callq  19c0 <close>
    if (write(pfds[1], "x", 1) != 1)
     5a7:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     5aa:	ba 01 00 00 00       	mov    $0x1,%edx
     5af:	be 4b 1e 00 00       	mov    $0x1e4b,%esi
     5b4:	89 c7                	mov    %eax,%edi
     5b6:	e8 fd 13 00 00       	callq  19b8 <write>
     5bb:	83 f8 01             	cmp    $0x1,%eax
     5be:	74 4c                	je     60c <pkilltest+0xc5>
      error("pkilltest: write error");
     5c0:	8b 05 0a 20 00 00    	mov    0x200a(%rip),%eax        # 25d0 <stdout>
     5c6:	ba ad 00 00 00       	mov    $0xad,%edx
     5cb:	be 25 1d 00 00       	mov    $0x1d25,%esi
     5d0:	89 c7                	mov    %eax,%edi
     5d2:	b8 00 00 00 00       	mov    $0x0,%eax
     5d7:	e8 19 0e 00 00       	callq  13f5 <printf>
     5dc:	8b 05 ee 1f 00 00    	mov    0x1fee(%rip),%eax        # 25d0 <stdout>
     5e2:	be 4d 1e 00 00       	mov    $0x1e4d,%esi
     5e7:	89 c7                	mov    %eax,%edi
     5e9:	b8 00 00 00 00       	mov    $0x0,%eax
     5ee:	e8 02 0e 00 00       	callq  13f5 <printf>
     5f3:	8b 05 d7 1f 00 00    	mov    0x1fd7(%rip),%eax        # 25d0 <stdout>
     5f9:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     5fe:	89 c7                	mov    %eax,%edi
     600:	b8 00 00 00 00       	mov    $0x0,%eax
     605:	e8 eb 0d 00 00       	callq  13f5 <printf>
     60a:	eb fe                	jmp    60a <pkilltest+0xc3>
    close(pfds[1]);
     60c:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     60f:	89 c7                	mov    %eax,%edi
     611:	e8 aa 13 00 00       	callq  19c0 <close>
    for (;;)
      ;
     616:	eb fe                	jmp    616 <pkilltest+0xcf>
  }

  close(pfds[1]);
     618:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     61b:	89 c7                	mov    %eax,%edi
     61d:	e8 9e 13 00 00       	callq  19c0 <close>
  if (read(pfds[0], buf, sizeof(buf)) != 1) {
     622:	8b 45 e0             	mov    -0x20(%rbp),%eax
     625:	48 8d 4d e9          	lea    -0x17(%rbp),%rcx
     629:	ba 0b 00 00 00       	mov    $0xb,%edx
     62e:	48 89 ce             	mov    %rcx,%rsi
     631:	89 c7                	mov    %eax,%edi
     633:	e8 78 13 00 00       	callq  19b0 <read>
     638:	83 f8 01             	cmp    $0x1,%eax
     63b:	74 4c                	je     689 <pkilltest+0x142>
    error("pkilltest: read error");
     63d:	8b 05 8d 1f 00 00    	mov    0x1f8d(%rip),%eax        # 25d0 <stdout>
     643:	ba b5 00 00 00       	mov    $0xb5,%edx
     648:	be 25 1d 00 00       	mov    $0x1d25,%esi
     64d:	89 c7                	mov    %eax,%edi
     64f:	b8 00 00 00 00       	mov    $0x0,%eax
     654:	e8 9c 0d 00 00       	callq  13f5 <printf>
     659:	8b 05 71 1f 00 00    	mov    0x1f71(%rip),%eax        # 25d0 <stdout>
     65f:	be 64 1e 00 00       	mov    $0x1e64,%esi
     664:	89 c7                	mov    %eax,%edi
     666:	b8 00 00 00 00       	mov    $0x0,%eax
     66b:	e8 85 0d 00 00       	callq  13f5 <printf>
     670:	8b 05 5a 1f 00 00    	mov    0x1f5a(%rip),%eax        # 25d0 <stdout>
     676:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     67b:	89 c7                	mov    %eax,%edi
     67d:	b8 00 00 00 00       	mov    $0x0,%eax
     682:	e8 6e 0d 00 00       	callq  13f5 <printf>
     687:	eb fe                	jmp    687 <pkilltest+0x140>
  }
  close(pfds[0]);
     689:	8b 45 e0             	mov    -0x20(%rbp),%eax
     68c:	89 c7                	mov    %eax,%edi
     68e:	e8 2d 13 00 00       	callq  19c0 <close>
  printf(1, "kill... ");
     693:	be 7a 1e 00 00       	mov    $0x1e7a,%esi
     698:	bf 01 00 00 00       	mov    $0x1,%edi
     69d:	b8 00 00 00 00       	mov    $0x0,%eax
     6a2:	e8 4e 0d 00 00       	callq  13f5 <printf>
  kill(pid1);
     6a7:	8b 45 fc             	mov    -0x4(%rbp),%eax
     6aa:	89 c7                	mov    %eax,%edi
     6ac:	e8 17 13 00 00       	callq  19c8 <kill>
  kill(pid2);
     6b1:	8b 45 f8             	mov    -0x8(%rbp),%eax
     6b4:	89 c7                	mov    %eax,%edi
     6b6:	e8 0d 13 00 00       	callq  19c8 <kill>
  kill(pid3);
     6bb:	8b 45 f4             	mov    -0xc(%rbp),%eax
     6be:	89 c7                	mov    %eax,%edi
     6c0:	e8 03 13 00 00       	callq  19c8 <kill>
  printf(1, "wait... ");
     6c5:	be 83 1e 00 00       	mov    $0x1e83,%esi
     6ca:	bf 01 00 00 00       	mov    $0x1,%edi
     6cf:	b8 00 00 00 00       	mov    $0x0,%eax
     6d4:	e8 1c 0d 00 00       	callq  13f5 <printf>
  wait();
     6d9:	e8 c2 12 00 00       	callq  19a0 <wait>
  wait();
     6de:	e8 bd 12 00 00       	callq  19a0 <wait>
  wait();
     6e3:	e8 b8 12 00 00       	callq  19a0 <wait>
  printf(1, "pkilltest: ok\n");
     6e8:	be 8c 1e 00 00       	mov    $0x1e8c,%esi
     6ed:	bf 01 00 00 00       	mov    $0x1,%edi
     6f2:	b8 00 00 00 00       	mov    $0x0,%eax
     6f7:	e8 f9 0c 00 00       	callq  13f5 <printf>
}
     6fc:	90                   	nop
     6fd:	c9                   	leaveq 
     6fe:	c3                   	retq   

00000000000006ff <racetest>:

void racetest(void) {
     6ff:	55                   	push   %rbp
     700:	48 89 e5             	mov    %rsp,%rbp
     703:	48 83 ec 10          	sub    $0x10,%rsp
  int i, pid;

  for (i = 0; i < 100; i++) {
     707:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     70e:	e9 bf 00 00 00       	jmpq   7d2 <racetest+0xd3>
    pid = fork();
     713:	e8 78 12 00 00       	callq  1990 <fork>
     718:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (pid < 0) {
     71b:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     71f:	79 4c                	jns    76d <racetest+0x6e>
      error("racetest: fork failed\n");
     721:	8b 05 a9 1e 00 00    	mov    0x1ea9(%rip),%eax        # 25d0 <stdout>
     727:	ba c9 00 00 00       	mov    $0xc9,%edx
     72c:	be 25 1d 00 00       	mov    $0x1d25,%esi
     731:	89 c7                	mov    %eax,%edi
     733:	b8 00 00 00 00       	mov    $0x0,%eax
     738:	e8 b8 0c 00 00       	callq  13f5 <printf>
     73d:	8b 05 8d 1e 00 00    	mov    0x1e8d(%rip),%eax        # 25d0 <stdout>
     743:	be 9b 1e 00 00       	mov    $0x1e9b,%esi
     748:	89 c7                	mov    %eax,%edi
     74a:	b8 00 00 00 00       	mov    $0x0,%eax
     74f:	e8 a1 0c 00 00       	callq  13f5 <printf>
     754:	8b 05 76 1e 00 00    	mov    0x1e76(%rip),%eax        # 25d0 <stdout>
     75a:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     75f:	89 c7                	mov    %eax,%edi
     761:	b8 00 00 00 00       	mov    $0x0,%eax
     766:	e8 8a 0c 00 00       	callq  13f5 <printf>
     76b:	eb fe                	jmp    76b <racetest+0x6c>
    }
    if (pid) {
     76d:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     771:	74 56                	je     7c9 <racetest+0xca>
      if (wait() != pid) {
     773:	e8 28 12 00 00       	callq  19a0 <wait>
     778:	3b 45 f8             	cmp    -0x8(%rbp),%eax
     77b:	74 51                	je     7ce <racetest+0xcf>
        error("racetest: wait wrong pid\n");
     77d:	8b 05 4d 1e 00 00    	mov    0x1e4d(%rip),%eax        # 25d0 <stdout>
     783:	ba cd 00 00 00       	mov    $0xcd,%edx
     788:	be 25 1d 00 00       	mov    $0x1d25,%esi
     78d:	89 c7                	mov    %eax,%edi
     78f:	b8 00 00 00 00       	mov    $0x0,%eax
     794:	e8 5c 0c 00 00       	callq  13f5 <printf>
     799:	8b 05 31 1e 00 00    	mov    0x1e31(%rip),%eax        # 25d0 <stdout>
     79f:	be b2 1e 00 00       	mov    $0x1eb2,%esi
     7a4:	89 c7                	mov    %eax,%edi
     7a6:	b8 00 00 00 00       	mov    $0x0,%eax
     7ab:	e8 45 0c 00 00       	callq  13f5 <printf>
     7b0:	8b 05 1a 1e 00 00    	mov    0x1e1a(%rip),%eax        # 25d0 <stdout>
     7b6:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     7bb:	89 c7                	mov    %eax,%edi
     7bd:	b8 00 00 00 00       	mov    $0x0,%eax
     7c2:	e8 2e 0c 00 00       	callq  13f5 <printf>
     7c7:	eb fe                	jmp    7c7 <racetest+0xc8>
      }
    } else {
      exit();
     7c9:	e8 ca 11 00 00       	callq  1998 <exit>
}

void racetest(void) {
  int i, pid;

  for (i = 0; i < 100; i++) {
     7ce:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     7d2:	83 7d fc 63          	cmpl   $0x63,-0x4(%rbp)
     7d6:	0f 8e 37 ff ff ff    	jle    713 <racetest+0x14>
      }
    } else {
      exit();
    }
  }
  printf(1, "racetest ok\n");
     7dc:	be cc 1e 00 00       	mov    $0x1ecc,%esi
     7e1:	bf 01 00 00 00       	mov    $0x1,%edi
     7e6:	b8 00 00 00 00       	mov    $0x0,%eax
     7eb:	e8 05 0c 00 00       	callq  13f5 <printf>
}
     7f0:	90                   	nop
     7f1:	c9                   	leaveq 
     7f2:	c3                   	retq   

00000000000007f3 <fdesctest>:

void fdesctest(void) {
     7f3:	55                   	push   %rbp
     7f4:	48 89 e5             	mov    %rsp,%rbp
     7f7:	48 83 ec 10          	sub    $0x10,%rsp
  printf(1, "fdesctest\n");
     7fb:	be d9 1e 00 00       	mov    $0x1ed9,%esi
     800:	bf 01 00 00 00       	mov    $0x1,%edi
     805:	b8 00 00 00 00       	mov    $0x0,%eax
     80a:	e8 e6 0b 00 00       	callq  13f5 <printf>
  int fd1;
  char buf[11] = {0};
     80f:	48 c7 45 f1 00 00 00 	movq   $0x0,-0xf(%rbp)
     816:	00 
     817:	66 c7 45 f9 00 00    	movw   $0x0,-0x7(%rbp)
     81d:	c6 45 fb 00          	movb   $0x0,-0x5(%rbp)

  // Make sure offsets are shared
  assert((fd1 = open("l2_share.txt", O_RDONLY)) > -1);
     821:	be 00 00 00 00       	mov    $0x0,%esi
     826:	bf e4 1e 00 00       	mov    $0x1ee4,%edi
     82b:	e8 a8 11 00 00       	callq  19d8 <open>
     830:	89 45 fc             	mov    %eax,-0x4(%rbp)
     833:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     837:	79 23                	jns    85c <fdesctest+0x69>
     839:	8b 05 91 1d 00 00    	mov    0x1d91(%rip),%eax        # 25d0 <stdout>
     83f:	b9 f8 1e 00 00       	mov    $0x1ef8,%ecx
     844:	ba dc 00 00 00       	mov    $0xdc,%edx
     849:	be 28 1f 00 00       	mov    $0x1f28,%esi
     84e:	89 c7                	mov    %eax,%edi
     850:	b8 00 00 00 00       	mov    $0x0,%eax
     855:	e8 9b 0b 00 00       	callq  13f5 <printf>
     85a:	eb fe                	jmp    85a <fdesctest+0x67>

  if (!fork()) {
     85c:	e8 2f 11 00 00       	callq  1990 <fork>
     861:	85 c0                	test   %eax,%eax
     863:	0f 85 d1 00 00 00    	jne    93a <fdesctest+0x147>
    if (read(fd1, buf, 10) != 10)
     869:	48 8d 4d f1          	lea    -0xf(%rbp),%rcx
     86d:	8b 45 fc             	mov    -0x4(%rbp),%eax
     870:	ba 0a 00 00 00       	mov    $0xa,%edx
     875:	48 89 ce             	mov    %rcx,%rsi
     878:	89 c7                	mov    %eax,%edi
     87a:	e8 31 11 00 00       	callq  19b0 <read>
     87f:	83 f8 0a             	cmp    $0xa,%eax
     882:	74 4c                	je     8d0 <fdesctest+0xdd>
      error("tried to read 10 bytes from fd1");
     884:	8b 05 46 1d 00 00    	mov    0x1d46(%rip),%eax        # 25d0 <stdout>
     88a:	ba e0 00 00 00       	mov    $0xe0,%edx
     88f:	be 25 1d 00 00       	mov    $0x1d25,%esi
     894:	89 c7                	mov    %eax,%edi
     896:	b8 00 00 00 00       	mov    $0x0,%eax
     89b:	e8 55 0b 00 00       	callq  13f5 <printf>
     8a0:	8b 05 2a 1d 00 00    	mov    0x1d2a(%rip),%eax        # 25d0 <stdout>
     8a6:	be 48 1f 00 00       	mov    $0x1f48,%esi
     8ab:	89 c7                	mov    %eax,%edi
     8ad:	b8 00 00 00 00       	mov    $0x0,%eax
     8b2:	e8 3e 0b 00 00       	callq  13f5 <printf>
     8b7:	8b 05 13 1d 00 00    	mov    0x1d13(%rip),%eax        # 25d0 <stdout>
     8bd:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     8c2:	89 c7                	mov    %eax,%edi
     8c4:	b8 00 00 00 00       	mov    $0x0,%eax
     8c9:	e8 27 0b 00 00       	callq  13f5 <printf>
     8ce:	eb fe                	jmp    8ce <fdesctest+0xdb>

    if (strcmp("cccccccccc", buf))
     8d0:	48 8d 45 f1          	lea    -0xf(%rbp),%rax
     8d4:	48 89 c6             	mov    %rax,%rsi
     8d7:	bf 68 1f 00 00       	mov    $0x1f68,%edi
     8dc:	e8 51 0e 00 00       	callq  1732 <strcmp>
     8e1:	85 c0                	test   %eax,%eax
     8e3:	74 50                	je     935 <fdesctest+0x142>
      error("should have reead 10 c's, instead: '%s'", buf);
     8e5:	8b 05 e5 1c 00 00    	mov    0x1ce5(%rip),%eax        # 25d0 <stdout>
     8eb:	ba e3 00 00 00       	mov    $0xe3,%edx
     8f0:	be 25 1d 00 00       	mov    $0x1d25,%esi
     8f5:	89 c7                	mov    %eax,%edi
     8f7:	b8 00 00 00 00       	mov    $0x0,%eax
     8fc:	e8 f4 0a 00 00       	callq  13f5 <printf>
     901:	8b 05 c9 1c 00 00    	mov    0x1cc9(%rip),%eax        # 25d0 <stdout>
     907:	48 8d 55 f1          	lea    -0xf(%rbp),%rdx
     90b:	be 78 1f 00 00       	mov    $0x1f78,%esi
     910:	89 c7                	mov    %eax,%edi
     912:	b8 00 00 00 00       	mov    $0x0,%eax
     917:	e8 d9 0a 00 00       	callq  13f5 <printf>
     91c:	8b 05 ae 1c 00 00    	mov    0x1cae(%rip),%eax        # 25d0 <stdout>
     922:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     927:	89 c7                	mov    %eax,%edi
     929:	b8 00 00 00 00       	mov    $0x0,%eax
     92e:	e8 c2 0a 00 00       	callq  13f5 <printf>
     933:	eb fe                	jmp    933 <fdesctest+0x140>
    exit();
     935:	e8 5e 10 00 00       	callq  1998 <exit>
  } else {
    wait();
     93a:	e8 61 10 00 00       	callq  19a0 <wait>
  }

  if (read(fd1, buf, 10) != 10)
     93f:	48 8d 4d f1          	lea    -0xf(%rbp),%rcx
     943:	8b 45 fc             	mov    -0x4(%rbp),%eax
     946:	ba 0a 00 00 00       	mov    $0xa,%edx
     94b:	48 89 ce             	mov    %rcx,%rsi
     94e:	89 c7                	mov    %eax,%edi
     950:	e8 5b 10 00 00       	callq  19b0 <read>
     955:	83 f8 0a             	cmp    $0xa,%eax
     958:	74 4c                	je     9a6 <fdesctest+0x1b3>
    error("tried to read 10 bytes from fd1");
     95a:	8b 05 70 1c 00 00    	mov    0x1c70(%rip),%eax        # 25d0 <stdout>
     960:	ba ea 00 00 00       	mov    $0xea,%edx
     965:	be 25 1d 00 00       	mov    $0x1d25,%esi
     96a:	89 c7                	mov    %eax,%edi
     96c:	b8 00 00 00 00       	mov    $0x0,%eax
     971:	e8 7f 0a 00 00       	callq  13f5 <printf>
     976:	8b 05 54 1c 00 00    	mov    0x1c54(%rip),%eax        # 25d0 <stdout>
     97c:	be 48 1f 00 00       	mov    $0x1f48,%esi
     981:	89 c7                	mov    %eax,%edi
     983:	b8 00 00 00 00       	mov    $0x0,%eax
     988:	e8 68 0a 00 00       	callq  13f5 <printf>
     98d:	8b 05 3d 1c 00 00    	mov    0x1c3d(%rip),%eax        # 25d0 <stdout>
     993:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     998:	89 c7                	mov    %eax,%edi
     99a:	b8 00 00 00 00       	mov    $0x0,%eax
     99f:	e8 51 0a 00 00       	callq  13f5 <printf>
     9a4:	eb fe                	jmp    9a4 <fdesctest+0x1b1>

  if (strcmp("ppppppppp\n", buf))
     9a6:	48 8d 45 f1          	lea    -0xf(%rbp),%rax
     9aa:	48 89 c6             	mov    %rax,%rsi
     9ad:	bf a0 1f 00 00       	mov    $0x1fa0,%edi
     9b2:	e8 7b 0d 00 00       	callq  1732 <strcmp>
     9b7:	85 c0                	test   %eax,%eax
     9b9:	74 50                	je     a0b <fdesctest+0x218>
    error("should have reead 9 p's (and a newline), instead: '%s'", buf);
     9bb:	8b 05 0f 1c 00 00    	mov    0x1c0f(%rip),%eax        # 25d0 <stdout>
     9c1:	ba ed 00 00 00       	mov    $0xed,%edx
     9c6:	be 25 1d 00 00       	mov    $0x1d25,%esi
     9cb:	89 c7                	mov    %eax,%edi
     9cd:	b8 00 00 00 00       	mov    $0x0,%eax
     9d2:	e8 1e 0a 00 00       	callq  13f5 <printf>
     9d7:	8b 05 f3 1b 00 00    	mov    0x1bf3(%rip),%eax        # 25d0 <stdout>
     9dd:	48 8d 55 f1          	lea    -0xf(%rbp),%rdx
     9e1:	be b0 1f 00 00       	mov    $0x1fb0,%esi
     9e6:	89 c7                	mov    %eax,%edi
     9e8:	b8 00 00 00 00       	mov    $0x0,%eax
     9ed:	e8 03 0a 00 00       	callq  13f5 <printf>
     9f2:	8b 05 d8 1b 00 00    	mov    0x1bd8(%rip),%eax        # 25d0 <stdout>
     9f8:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     9fd:	89 c7                	mov    %eax,%edi
     9ff:	b8 00 00 00 00       	mov    $0x0,%eax
     a04:	e8 ec 09 00 00       	callq  13f5 <printf>
     a09:	eb fe                	jmp    a09 <fdesctest+0x216>

  close(fd1);
     a0b:	8b 45 fc             	mov    -0x4(%rbp),%eax
     a0e:	89 c7                	mov    %eax,%edi
     a10:	e8 ab 0f 00 00       	callq  19c0 <close>

  // Make sure the correct file descriptor ints are maintained

  assert(open("l2_share.txt", O_RDONLY) > -1);
     a15:	be 00 00 00 00       	mov    $0x0,%esi
     a1a:	bf e4 1e 00 00       	mov    $0x1ee4,%edi
     a1f:	e8 b4 0f 00 00       	callq  19d8 <open>
     a24:	85 c0                	test   %eax,%eax
     a26:	79 23                	jns    a4b <fdesctest+0x258>
     a28:	8b 05 a2 1b 00 00    	mov    0x1ba2(%rip),%eax        # 25d0 <stdout>
     a2e:	b9 e8 1f 00 00       	mov    $0x1fe8,%ecx
     a33:	ba f3 00 00 00       	mov    $0xf3,%edx
     a38:	be 28 1f 00 00       	mov    $0x1f28,%esi
     a3d:	89 c7                	mov    %eax,%edi
     a3f:	b8 00 00 00 00       	mov    $0x0,%eax
     a44:	e8 ac 09 00 00       	callq  13f5 <printf>
     a49:	eb fe                	jmp    a49 <fdesctest+0x256>
  assert(open("l2_share.txt", O_RDONLY) > -1);
     a4b:	be 00 00 00 00       	mov    $0x0,%esi
     a50:	bf e4 1e 00 00       	mov    $0x1ee4,%edi
     a55:	e8 7e 0f 00 00       	callq  19d8 <open>
     a5a:	85 c0                	test   %eax,%eax
     a5c:	79 23                	jns    a81 <fdesctest+0x28e>
     a5e:	8b 05 6c 1b 00 00    	mov    0x1b6c(%rip),%eax        # 25d0 <stdout>
     a64:	b9 e8 1f 00 00       	mov    $0x1fe8,%ecx
     a69:	ba f4 00 00 00       	mov    $0xf4,%edx
     a6e:	be 28 1f 00 00       	mov    $0x1f28,%esi
     a73:	89 c7                	mov    %eax,%edi
     a75:	b8 00 00 00 00       	mov    $0x0,%eax
     a7a:	e8 76 09 00 00       	callq  13f5 <printf>
     a7f:	eb fe                	jmp    a7f <fdesctest+0x28c>
  assert(open("l2_share.txt", O_RDONLY) > -1);
     a81:	be 00 00 00 00       	mov    $0x0,%esi
     a86:	bf e4 1e 00 00       	mov    $0x1ee4,%edi
     a8b:	e8 48 0f 00 00       	callq  19d8 <open>
     a90:	85 c0                	test   %eax,%eax
     a92:	79 23                	jns    ab7 <fdesctest+0x2c4>
     a94:	8b 05 36 1b 00 00    	mov    0x1b36(%rip),%eax        # 25d0 <stdout>
     a9a:	b9 e8 1f 00 00       	mov    $0x1fe8,%ecx
     a9f:	ba f5 00 00 00       	mov    $0xf5,%edx
     aa4:	be 28 1f 00 00       	mov    $0x1f28,%esi
     aa9:	89 c7                	mov    %eax,%edi
     aab:	b8 00 00 00 00       	mov    $0x0,%eax
     ab0:	e8 40 09 00 00       	callq  13f5 <printf>
     ab5:	eb fe                	jmp    ab5 <fdesctest+0x2c2>
  assert(open("l2_share.txt", O_RDONLY) > -1);
     ab7:	be 00 00 00 00       	mov    $0x0,%esi
     abc:	bf e4 1e 00 00       	mov    $0x1ee4,%edi
     ac1:	e8 12 0f 00 00       	callq  19d8 <open>
     ac6:	85 c0                	test   %eax,%eax
     ac8:	79 23                	jns    aed <fdesctest+0x2fa>
     aca:	8b 05 00 1b 00 00    	mov    0x1b00(%rip),%eax        # 25d0 <stdout>
     ad0:	b9 e8 1f 00 00       	mov    $0x1fe8,%ecx
     ad5:	ba f6 00 00 00       	mov    $0xf6,%edx
     ada:	be 28 1f 00 00       	mov    $0x1f28,%esi
     adf:	89 c7                	mov    %eax,%edi
     ae1:	b8 00 00 00 00       	mov    $0x0,%eax
     ae6:	e8 0a 09 00 00       	callq  13f5 <printf>
     aeb:	eb fe                	jmp    aeb <fdesctest+0x2f8>

  assert(close(4) != -1);
     aed:	bf 04 00 00 00       	mov    $0x4,%edi
     af2:	e8 c9 0e 00 00       	callq  19c0 <close>
     af7:	83 f8 ff             	cmp    $0xffffffff,%eax
     afa:	75 23                	jne    b1f <fdesctest+0x32c>
     afc:	8b 05 ce 1a 00 00    	mov    0x1ace(%rip),%eax        # 25d0 <stdout>
     b02:	b9 0c 20 00 00       	mov    $0x200c,%ecx
     b07:	ba f8 00 00 00       	mov    $0xf8,%edx
     b0c:	be 28 1f 00 00       	mov    $0x1f28,%esi
     b11:	89 c7                	mov    %eax,%edi
     b13:	b8 00 00 00 00       	mov    $0x0,%eax
     b18:	e8 d8 08 00 00       	callq  13f5 <printf>
     b1d:	eb fe                	jmp    b1d <fdesctest+0x32a>
  assert(close(5) != -1);
     b1f:	bf 05 00 00 00       	mov    $0x5,%edi
     b24:	e8 97 0e 00 00       	callq  19c0 <close>
     b29:	83 f8 ff             	cmp    $0xffffffff,%eax
     b2c:	75 23                	jne    b51 <fdesctest+0x35e>
     b2e:	8b 05 9c 1a 00 00    	mov    0x1a9c(%rip),%eax        # 25d0 <stdout>
     b34:	b9 1b 20 00 00       	mov    $0x201b,%ecx
     b39:	ba f9 00 00 00       	mov    $0xf9,%edx
     b3e:	be 28 1f 00 00       	mov    $0x1f28,%esi
     b43:	89 c7                	mov    %eax,%edi
     b45:	b8 00 00 00 00       	mov    $0x0,%eax
     b4a:	e8 a6 08 00 00       	callq  13f5 <printf>
     b4f:	eb fe                	jmp    b4f <fdesctest+0x35c>

  if (!fork()) {
     b51:	e8 3a 0e 00 00       	callq  1990 <fork>
     b56:	85 c0                	test   %eax,%eax
     b58:	0f 85 fd 00 00 00    	jne    c5b <fdesctest+0x468>
    assert(read(3, buf, 10) == 10);
     b5e:	48 8d 45 f1          	lea    -0xf(%rbp),%rax
     b62:	ba 0a 00 00 00       	mov    $0xa,%edx
     b67:	48 89 c6             	mov    %rax,%rsi
     b6a:	bf 03 00 00 00       	mov    $0x3,%edi
     b6f:	e8 3c 0e 00 00       	callq  19b0 <read>
     b74:	83 f8 0a             	cmp    $0xa,%eax
     b77:	74 23                	je     b9c <fdesctest+0x3a9>
     b79:	8b 05 51 1a 00 00    	mov    0x1a51(%rip),%eax        # 25d0 <stdout>
     b7f:	b9 2a 20 00 00       	mov    $0x202a,%ecx
     b84:	ba fc 00 00 00       	mov    $0xfc,%edx
     b89:	be 28 1f 00 00       	mov    $0x1f28,%esi
     b8e:	89 c7                	mov    %eax,%edi
     b90:	b8 00 00 00 00       	mov    $0x0,%eax
     b95:	e8 5b 08 00 00       	callq  13f5 <printf>
     b9a:	eb fe                	jmp    b9a <fdesctest+0x3a7>
    assert(read(4, buf, 10) == -1); // this fd shouldn't be open
     b9c:	48 8d 45 f1          	lea    -0xf(%rbp),%rax
     ba0:	ba 0a 00 00 00       	mov    $0xa,%edx
     ba5:	48 89 c6             	mov    %rax,%rsi
     ba8:	bf 04 00 00 00       	mov    $0x4,%edi
     bad:	e8 fe 0d 00 00       	callq  19b0 <read>
     bb2:	83 f8 ff             	cmp    $0xffffffff,%eax
     bb5:	74 23                	je     bda <fdesctest+0x3e7>
     bb7:	8b 05 13 1a 00 00    	mov    0x1a13(%rip),%eax        # 25d0 <stdout>
     bbd:	b9 41 20 00 00       	mov    $0x2041,%ecx
     bc2:	ba fd 00 00 00       	mov    $0xfd,%edx
     bc7:	be 28 1f 00 00       	mov    $0x1f28,%esi
     bcc:	89 c7                	mov    %eax,%edi
     bce:	b8 00 00 00 00       	mov    $0x0,%eax
     bd3:	e8 1d 08 00 00       	callq  13f5 <printf>
     bd8:	eb fe                	jmp    bd8 <fdesctest+0x3e5>
    assert(read(5, buf, 10) == -1); // this fd shouldn't be open
     bda:	48 8d 45 f1          	lea    -0xf(%rbp),%rax
     bde:	ba 0a 00 00 00       	mov    $0xa,%edx
     be3:	48 89 c6             	mov    %rax,%rsi
     be6:	bf 05 00 00 00       	mov    $0x5,%edi
     beb:	e8 c0 0d 00 00       	callq  19b0 <read>
     bf0:	83 f8 ff             	cmp    $0xffffffff,%eax
     bf3:	74 23                	je     c18 <fdesctest+0x425>
     bf5:	8b 05 d5 19 00 00    	mov    0x19d5(%rip),%eax        # 25d0 <stdout>
     bfb:	b9 58 20 00 00       	mov    $0x2058,%ecx
     c00:	ba fe 00 00 00       	mov    $0xfe,%edx
     c05:	be 28 1f 00 00       	mov    $0x1f28,%esi
     c0a:	89 c7                	mov    %eax,%edi
     c0c:	b8 00 00 00 00       	mov    $0x0,%eax
     c11:	e8 df 07 00 00       	callq  13f5 <printf>
     c16:	eb fe                	jmp    c16 <fdesctest+0x423>
    assert(read(6, buf, 10) == 10); // this fd shouldn't be open
     c18:	48 8d 45 f1          	lea    -0xf(%rbp),%rax
     c1c:	ba 0a 00 00 00       	mov    $0xa,%edx
     c21:	48 89 c6             	mov    %rax,%rsi
     c24:	bf 06 00 00 00       	mov    $0x6,%edi
     c29:	e8 82 0d 00 00       	callq  19b0 <read>
     c2e:	83 f8 0a             	cmp    $0xa,%eax
     c31:	74 23                	je     c56 <fdesctest+0x463>
     c33:	8b 05 97 19 00 00    	mov    0x1997(%rip),%eax        # 25d0 <stdout>
     c39:	b9 6f 20 00 00       	mov    $0x206f,%ecx
     c3e:	ba ff 00 00 00       	mov    $0xff,%edx
     c43:	be 28 1f 00 00       	mov    $0x1f28,%esi
     c48:	89 c7                	mov    %eax,%edi
     c4a:	b8 00 00 00 00       	mov    $0x0,%eax
     c4f:	e8 a1 07 00 00       	callq  13f5 <printf>
     c54:	eb fe                	jmp    c54 <fdesctest+0x461>
    exit();
     c56:	e8 3d 0d 00 00       	callq  1998 <exit>
  } else {
    wait();
     c5b:	e8 40 0d 00 00       	callq  19a0 <wait>
  }

  printf(1, "fdesctest: passed\n");
     c60:	be 86 20 00 00       	mov    $0x2086,%esi
     c65:	bf 01 00 00 00       	mov    $0x1,%edi
     c6a:	b8 00 00 00 00       	mov    $0x0,%eax
     c6f:	e8 81 07 00 00       	callq  13f5 <printf>
}
     c74:	90                   	nop
     c75:	c9                   	leaveq 
     c76:	c3                   	retq   

0000000000000c77 <childpidtesthelper>:

static void childpidtesthelper(int depth) {
     c77:	55                   	push   %rbp
     c78:	48 89 e5             	mov    %rsp,%rbp
     c7b:	48 83 ec 30          	sub    $0x30,%rsp
     c7f:	89 7d dc             	mov    %edi,-0x24(%rbp)
  int pids[3];
  int i, j, p;

  if (depth == 0)
     c82:	83 7d dc 00          	cmpl   $0x0,-0x24(%rbp)
     c86:	0f 84 95 01 00 00    	je     e21 <childpidtesthelper+0x1aa>
    return;

  for (i = 0; i < 3; i++) {
     c8c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     c93:	eb 63                	jmp    cf8 <childpidtesthelper+0x81>
    pids[i] = fork();
     c95:	e8 f6 0c 00 00       	callq  1990 <fork>
     c9a:	89 c2                	mov    %eax,%edx
     c9c:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c9f:	48 98                	cltq   
     ca1:	89 54 85 e8          	mov    %edx,-0x18(%rbp,%rax,4)
    assert(pids[i] >= 0);
     ca5:	8b 45 fc             	mov    -0x4(%rbp),%eax
     ca8:	48 98                	cltq   
     caa:	8b 44 85 e8          	mov    -0x18(%rbp,%rax,4),%eax
     cae:	85 c0                	test   %eax,%eax
     cb0:	79 23                	jns    cd5 <childpidtesthelper+0x5e>
     cb2:	8b 05 18 19 00 00    	mov    0x1918(%rip),%eax        # 25d0 <stdout>
     cb8:	b9 99 20 00 00       	mov    $0x2099,%ecx
     cbd:	ba 11 01 00 00       	mov    $0x111,%edx
     cc2:	be 28 1f 00 00       	mov    $0x1f28,%esi
     cc7:	89 c7                	mov    %eax,%edi
     cc9:	b8 00 00 00 00       	mov    $0x0,%eax
     cce:	e8 22 07 00 00       	callq  13f5 <printf>
     cd3:	eb fe                	jmp    cd3 <childpidtesthelper+0x5c>
    if (!pids[i]) {
     cd5:	8b 45 fc             	mov    -0x4(%rbp),%eax
     cd8:	48 98                	cltq   
     cda:	8b 44 85 e8          	mov    -0x18(%rbp,%rax,4),%eax
     cde:	85 c0                	test   %eax,%eax
     ce0:	75 12                	jne    cf4 <childpidtesthelper+0x7d>
      childpidtesthelper(depth - 1);
     ce2:	8b 45 dc             	mov    -0x24(%rbp),%eax
     ce5:	83 e8 01             	sub    $0x1,%eax
     ce8:	89 c7                	mov    %eax,%edi
     cea:	e8 88 ff ff ff       	callq  c77 <childpidtesthelper>
      exit();
     cef:	e8 a4 0c 00 00       	callq  1998 <exit>
  int i, j, p;

  if (depth == 0)
    return;

  for (i = 0; i < 3; i++) {
     cf4:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     cf8:	83 7d fc 02          	cmpl   $0x2,-0x4(%rbp)
     cfc:	7e 97                	jle    c95 <childpidtesthelper+0x1e>
      childpidtesthelper(depth - 1);
      exit();
    }
  }

  for (i = 0; i < 3; i++) {
     cfe:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     d05:	e9 b7 00 00 00       	jmpq   dc1 <childpidtesthelper+0x14a>
    p = wait();
     d0a:	e8 91 0c 00 00       	callq  19a0 <wait>
     d0f:	89 45 f4             	mov    %eax,-0xc(%rbp)
    //printf(2, "USER pid from wait: %d\n", p);
    assert(p > 0);
     d12:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     d16:	7f 23                	jg     d3b <childpidtesthelper+0xc4>
     d18:	8b 05 b2 18 00 00    	mov    0x18b2(%rip),%eax        # 25d0 <stdout>
     d1e:	b9 a6 20 00 00       	mov    $0x20a6,%ecx
     d23:	ba 1b 01 00 00       	mov    $0x11b,%edx
     d28:	be 28 1f 00 00       	mov    $0x1f28,%esi
     d2d:	89 c7                	mov    %eax,%edi
     d2f:	b8 00 00 00 00       	mov    $0x0,%eax
     d34:	e8 bc 06 00 00       	callq  13f5 <printf>
     d39:	eb fe                	jmp    d39 <childpidtesthelper+0xc2>
    for (j = 0; j < 3; j++) {
     d3b:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     d42:	eb 21                	jmp    d65 <childpidtesthelper+0xee>
      if (p == pids[j]) {
     d44:	8b 45 f8             	mov    -0x8(%rbp),%eax
     d47:	48 98                	cltq   
     d49:	8b 44 85 e8          	mov    -0x18(%rbp,%rax,4),%eax
     d4d:	3b 45 f4             	cmp    -0xc(%rbp),%eax
     d50:	75 0f                	jne    d61 <childpidtesthelper+0xea>
        pids[j] = -1; // make sure duplicates are not returned
     d52:	8b 45 f8             	mov    -0x8(%rbp),%eax
     d55:	48 98                	cltq   
     d57:	c7 44 85 e8 ff ff ff 	movl   $0xffffffff,-0x18(%rbp,%rax,4)
     d5e:	ff 
        break;
     d5f:	eb 0a                	jmp    d6b <childpidtesthelper+0xf4>

  for (i = 0; i < 3; i++) {
    p = wait();
    //printf(2, "USER pid from wait: %d\n", p);
    assert(p > 0);
    for (j = 0; j < 3; j++) {
     d61:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     d65:	83 7d f8 02          	cmpl   $0x2,-0x8(%rbp)
     d69:	7e d9                	jle    d44 <childpidtesthelper+0xcd>
      if (p == pids[j]) {
        pids[j] = -1; // make sure duplicates are not returned
        break;
      }
    }
    if (j == 3)
     d6b:	83 7d f8 03          	cmpl   $0x3,-0x8(%rbp)
     d6f:	75 4c                	jne    dbd <childpidtesthelper+0x146>
      error("returned pid was not a child");
     d71:	8b 05 59 18 00 00    	mov    0x1859(%rip),%eax        # 25d0 <stdout>
     d77:	ba 23 01 00 00       	mov    $0x123,%edx
     d7c:	be 25 1d 00 00       	mov    $0x1d25,%esi
     d81:	89 c7                	mov    %eax,%edi
     d83:	b8 00 00 00 00       	mov    $0x0,%eax
     d88:	e8 68 06 00 00       	callq  13f5 <printf>
     d8d:	8b 05 3d 18 00 00    	mov    0x183d(%rip),%eax        # 25d0 <stdout>
     d93:	be ac 20 00 00       	mov    $0x20ac,%esi
     d98:	89 c7                	mov    %eax,%edi
     d9a:	b8 00 00 00 00       	mov    $0x0,%eax
     d9f:	e8 51 06 00 00       	callq  13f5 <printf>
     da4:	8b 05 26 18 00 00    	mov    0x1826(%rip),%eax        # 25d0 <stdout>
     daa:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     daf:	89 c7                	mov    %eax,%edi
     db1:	b8 00 00 00 00       	mov    $0x0,%eax
     db6:	e8 3a 06 00 00       	callq  13f5 <printf>
     dbb:	eb fe                	jmp    dbb <childpidtesthelper+0x144>
      childpidtesthelper(depth - 1);
      exit();
    }
  }

  for (i = 0; i < 3; i++) {
     dbd:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     dc1:	83 7d fc 02          	cmpl   $0x2,-0x4(%rbp)
     dc5:	0f 8e 3f ff ff ff    	jle    d0a <childpidtesthelper+0x93>
    if (j == 3)
      error("returned pid was not a child");
  }

  // No more children
  if (wait() != -1)
     dcb:	e8 d0 0b 00 00       	callq  19a0 <wait>
     dd0:	83 f8 ff             	cmp    $0xffffffff,%eax
     dd3:	74 4d                	je     e22 <childpidtesthelper+0x1ab>
    error("there should be no more children");
     dd5:	8b 05 f5 17 00 00    	mov    0x17f5(%rip),%eax        # 25d0 <stdout>
     ddb:	ba 28 01 00 00       	mov    $0x128,%edx
     de0:	be 25 1d 00 00       	mov    $0x1d25,%esi
     de5:	89 c7                	mov    %eax,%edi
     de7:	b8 00 00 00 00       	mov    $0x0,%eax
     dec:	e8 04 06 00 00       	callq  13f5 <printf>
     df1:	8b 05 d9 17 00 00    	mov    0x17d9(%rip),%eax        # 25d0 <stdout>
     df7:	be d0 20 00 00       	mov    $0x20d0,%esi
     dfc:	89 c7                	mov    %eax,%edi
     dfe:	b8 00 00 00 00       	mov    $0x0,%eax
     e03:	e8 ed 05 00 00       	callq  13f5 <printf>
     e08:	8b 05 c2 17 00 00    	mov    0x17c2(%rip),%eax        # 25d0 <stdout>
     e0e:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     e13:	89 c7                	mov    %eax,%edi
     e15:	b8 00 00 00 00       	mov    $0x0,%eax
     e1a:	e8 d6 05 00 00       	callq  13f5 <printf>
     e1f:	eb fe                	jmp    e1f <childpidtesthelper+0x1a8>
static void childpidtesthelper(int depth) {
  int pids[3];
  int i, j, p;

  if (depth == 0)
    return;
     e21:	90                   	nop
  }

  // No more children
  if (wait() != -1)
    error("there should be no more children");
}
     e22:	c9                   	leaveq 
     e23:	c3                   	retq   

0000000000000e24 <childpidtest>:

void childpidtest() {
     e24:	55                   	push   %rbp
     e25:	48 89 e5             	mov    %rsp,%rbp
  printf(1, "childpidtest\n");
     e28:	be f1 20 00 00       	mov    $0x20f1,%esi
     e2d:	bf 01 00 00 00       	mov    $0x1,%edi
     e32:	b8 00 00 00 00       	mov    $0x0,%eax
     e37:	e8 b9 05 00 00       	callq  13f5 <printf>
  childpidtesthelper(2);
     e3c:	bf 02 00 00 00       	mov    $0x2,%edi
     e41:	e8 31 fe ff ff       	callq  c77 <childpidtesthelper>
  printf(1, "childpidtest: passed\n");
     e46:	be ff 20 00 00       	mov    $0x20ff,%esi
     e4b:	bf 01 00 00 00       	mov    $0x1,%edi
     e50:	b8 00 00 00 00       	mov    $0x0,%eax
     e55:	e8 9b 05 00 00       	callq  13f5 <printf>
}
     e5a:	90                   	nop
     e5b:	5d                   	pop    %rbp
     e5c:	c3                   	retq   

0000000000000e5d <exectest>:

void exectest(void) {
     e5d:	55                   	push   %rbp
     e5e:	48 89 e5             	mov    %rsp,%rbp
     e61:	48 83 ec 60          	sub    $0x60,%rsp
  printf(1, "exectest: starting ls\n");
     e65:	be 15 21 00 00       	mov    $0x2115,%esi
     e6a:	bf 01 00 00 00       	mov    $0x1,%edi
     e6f:	b8 00 00 00 00       	mov    $0x0,%eax
     e74:	e8 7c 05 00 00       	callq  13f5 <printf>
  int pid = fork();
     e79:	e8 12 0b 00 00       	callq  1990 <fork>
     e7e:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (pid < 0) {
     e81:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     e85:	79 4c                	jns    ed3 <exectest+0x76>
    error("exectest: fork failed\n");
     e87:	8b 05 43 17 00 00    	mov    0x1743(%rip),%eax        # 25d0 <stdout>
     e8d:	ba 35 01 00 00       	mov    $0x135,%edx
     e92:	be 25 1d 00 00       	mov    $0x1d25,%esi
     e97:	89 c7                	mov    %eax,%edi
     e99:	b8 00 00 00 00       	mov    $0x0,%eax
     e9e:	e8 52 05 00 00       	callq  13f5 <printf>
     ea3:	8b 05 27 17 00 00    	mov    0x1727(%rip),%eax        # 25d0 <stdout>
     ea9:	be 2c 21 00 00       	mov    $0x212c,%esi
     eae:	89 c7                	mov    %eax,%edi
     eb0:	b8 00 00 00 00       	mov    $0x0,%eax
     eb5:	e8 3b 05 00 00       	callq  13f5 <printf>
     eba:	8b 05 10 17 00 00    	mov    0x1710(%rip),%eax        # 25d0 <stdout>
     ec0:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     ec5:	89 c7                	mov    %eax,%edi
     ec7:	b8 00 00 00 00       	mov    $0x0,%eax
     ecc:	e8 24 05 00 00       	callq  13f5 <printf>
     ed1:	eb fe                	jmp    ed1 <exectest+0x74>
  }

  char *argv[] = {0};
     ed3:	48 c7 45 e0 00 00 00 	movq   $0x0,-0x20(%rbp)
     eda:	00 

  if (pid == 0) {
     edb:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     edf:	75 5d                	jne    f3e <exectest+0xe1>
    exec("ls", argv);
     ee1:	48 8d 45 e0          	lea    -0x20(%rbp),%rax
     ee5:	48 89 c6             	mov    %rax,%rsi
     ee8:	bf 43 21 00 00       	mov    $0x2143,%edi
     eed:	e8 de 0a 00 00       	callq  19d0 <exec>
    error("exectest: exec ls failed\n");
     ef2:	8b 05 d8 16 00 00    	mov    0x16d8(%rip),%eax        # 25d0 <stdout>
     ef8:	ba 3c 01 00 00       	mov    $0x13c,%edx
     efd:	be 25 1d 00 00       	mov    $0x1d25,%esi
     f02:	89 c7                	mov    %eax,%edi
     f04:	b8 00 00 00 00       	mov    $0x0,%eax
     f09:	e8 e7 04 00 00       	callq  13f5 <printf>
     f0e:	8b 05 bc 16 00 00    	mov    0x16bc(%rip),%eax        # 25d0 <stdout>
     f14:	be 46 21 00 00       	mov    $0x2146,%esi
     f19:	89 c7                	mov    %eax,%edi
     f1b:	b8 00 00 00 00       	mov    $0x0,%eax
     f20:	e8 d0 04 00 00       	callq  13f5 <printf>
     f25:	8b 05 a5 16 00 00    	mov    0x16a5(%rip),%eax        # 25d0 <stdout>
     f2b:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     f30:	89 c7                	mov    %eax,%edi
     f32:	b8 00 00 00 00       	mov    $0x0,%eax
     f37:	e8 b9 04 00 00       	callq  13f5 <printf>
     f3c:	eb fe                	jmp    f3c <exectest+0xdf>
  } else {
    pid = wait();
     f3e:	e8 5d 0a 00 00       	callq  19a0 <wait>
     f43:	89 45 fc             	mov    %eax,-0x4(%rbp)
  }

  char *echoargv[] = {"echo", "echotest", "ok", 0};
     f46:	48 c7 45 c0 60 21 00 	movq   $0x2160,-0x40(%rbp)
     f4d:	00 
     f4e:	48 c7 45 c8 65 21 00 	movq   $0x2165,-0x38(%rbp)
     f55:	00 
     f56:	48 c7 45 d0 6e 21 00 	movq   $0x216e,-0x30(%rbp)
     f5d:	00 
     f5e:	48 c7 45 d8 00 00 00 	movq   $0x0,-0x28(%rbp)
     f65:	00 
  printf(stdout, "exectest: test argument\n");
     f66:	8b 05 64 16 00 00    	mov    0x1664(%rip),%eax        # 25d0 <stdout>
     f6c:	be 71 21 00 00       	mov    $0x2171,%esi
     f71:	89 c7                	mov    %eax,%edi
     f73:	b8 00 00 00 00       	mov    $0x0,%eax
     f78:	e8 78 04 00 00       	callq  13f5 <printf>

  int fds[2];
  assert(pipe(fds) == 0);
     f7d:	48 8d 45 b8          	lea    -0x48(%rbp),%rax
     f81:	48 89 c7             	mov    %rax,%rdi
     f84:	e8 1f 0a 00 00       	callq  19a8 <pipe>
     f89:	85 c0                	test   %eax,%eax
     f8b:	74 23                	je     fb0 <exectest+0x153>
     f8d:	8b 05 3d 16 00 00    	mov    0x163d(%rip),%eax        # 25d0 <stdout>
     f93:	b9 8a 21 00 00       	mov    $0x218a,%ecx
     f98:	ba 45 01 00 00       	mov    $0x145,%edx
     f9d:	be 28 1f 00 00       	mov    $0x1f28,%esi
     fa2:	89 c7                	mov    %eax,%edi
     fa4:	b8 00 00 00 00       	mov    $0x0,%eax
     fa9:	e8 47 04 00 00       	callq  13f5 <printf>
     fae:	eb fe                	jmp    fae <exectest+0x151>

  pid = fork();
     fb0:	e8 db 09 00 00       	callq  1990 <fork>
     fb5:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (pid < 0) {
     fb8:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     fbc:	79 4c                	jns    100a <exectest+0x1ad>
    error("exectest: fork failed\n");
     fbe:	8b 05 0c 16 00 00    	mov    0x160c(%rip),%eax        # 25d0 <stdout>
     fc4:	ba 49 01 00 00       	mov    $0x149,%edx
     fc9:	be 25 1d 00 00       	mov    $0x1d25,%esi
     fce:	89 c7                	mov    %eax,%edi
     fd0:	b8 00 00 00 00       	mov    $0x0,%eax
     fd5:	e8 1b 04 00 00       	callq  13f5 <printf>
     fda:	8b 05 f0 15 00 00    	mov    0x15f0(%rip),%eax        # 25d0 <stdout>
     fe0:	be 2c 21 00 00       	mov    $0x212c,%esi
     fe5:	89 c7                	mov    %eax,%edi
     fe7:	b8 00 00 00 00       	mov    $0x0,%eax
     fec:	e8 04 04 00 00       	callq  13f5 <printf>
     ff1:	8b 05 d9 15 00 00    	mov    0x15d9(%rip),%eax        # 25d0 <stdout>
     ff7:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
     ffc:	89 c7                	mov    %eax,%edi
     ffe:	b8 00 00 00 00       	mov    $0x0,%eax
    1003:	e8 ed 03 00 00       	callq  13f5 <printf>
    1008:	eb fe                	jmp    1008 <exectest+0x1ab>
  }

  if (pid == 0) {
    100a:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    100e:	0f 85 a3 00 00 00    	jne    10b7 <exectest+0x25a>
    // send output to parent.
    close(stdout);
    1014:	8b 05 b6 15 00 00    	mov    0x15b6(%rip),%eax        # 25d0 <stdout>
    101a:	89 c7                	mov    %eax,%edi
    101c:	e8 9f 09 00 00       	callq  19c0 <close>
    assert(dup(fds[1]) == stdout);
    1021:	8b 45 bc             	mov    -0x44(%rbp),%eax
    1024:	89 c7                	mov    %eax,%edi
    1026:	e8 e5 09 00 00       	callq  1a10 <dup>
    102b:	89 c2                	mov    %eax,%edx
    102d:	8b 05 9d 15 00 00    	mov    0x159d(%rip),%eax        # 25d0 <stdout>
    1033:	39 c2                	cmp    %eax,%edx
    1035:	74 23                	je     105a <exectest+0x1fd>
    1037:	8b 05 93 15 00 00    	mov    0x1593(%rip),%eax        # 25d0 <stdout>
    103d:	b9 99 21 00 00       	mov    $0x2199,%ecx
    1042:	ba 4f 01 00 00       	mov    $0x14f,%edx
    1047:	be 28 1f 00 00       	mov    $0x1f28,%esi
    104c:	89 c7                	mov    %eax,%edi
    104e:	b8 00 00 00 00       	mov    $0x0,%eax
    1053:	e8 9d 03 00 00       	callq  13f5 <printf>
    1058:	eb fe                	jmp    1058 <exectest+0x1fb>
    exec("echo", echoargv);
    105a:	48 8d 45 c0          	lea    -0x40(%rbp),%rax
    105e:	48 89 c6             	mov    %rax,%rsi
    1061:	bf 60 21 00 00       	mov    $0x2160,%edi
    1066:	e8 65 09 00 00       	callq  19d0 <exec>
    error("exectest: exec echo failed\n");
    106b:	8b 05 5f 15 00 00    	mov    0x155f(%rip),%eax        # 25d0 <stdout>
    1071:	ba 51 01 00 00       	mov    $0x151,%edx
    1076:	be 25 1d 00 00       	mov    $0x1d25,%esi
    107b:	89 c7                	mov    %eax,%edi
    107d:	b8 00 00 00 00       	mov    $0x0,%eax
    1082:	e8 6e 03 00 00       	callq  13f5 <printf>
    1087:	8b 05 43 15 00 00    	mov    0x1543(%rip),%eax        # 25d0 <stdout>
    108d:	be af 21 00 00       	mov    $0x21af,%esi
    1092:	89 c7                	mov    %eax,%edi
    1094:	b8 00 00 00 00       	mov    $0x0,%eax
    1099:	e8 57 03 00 00       	callq  13f5 <printf>
    109e:	8b 05 2c 15 00 00    	mov    0x152c(%rip),%eax        # 25d0 <stdout>
    10a4:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
    10a9:	89 c7                	mov    %eax,%edi
    10ab:	b8 00 00 00 00       	mov    $0x0,%eax
    10b0:	e8 40 03 00 00       	callq  13f5 <printf>
    10b5:	eb fe                	jmp    10b5 <exectest+0x258>
  } else {
    assert(wait() == pid);
    10b7:	e8 e4 08 00 00       	callq  19a0 <wait>
    10bc:	3b 45 fc             	cmp    -0x4(%rbp),%eax
    10bf:	74 23                	je     10e4 <exectest+0x287>
    10c1:	8b 05 09 15 00 00    	mov    0x1509(%rip),%eax        # 25d0 <stdout>
    10c7:	b9 cb 21 00 00       	mov    $0x21cb,%ecx
    10cc:	ba 53 01 00 00       	mov    $0x153,%edx
    10d1:	be 28 1f 00 00       	mov    $0x1f28,%esi
    10d6:	89 c7                	mov    %eax,%edi
    10d8:	b8 00 00 00 00       	mov    $0x0,%eax
    10dd:	e8 13 03 00 00       	callq  13f5 <printf>
    10e2:	eb fe                	jmp    10e2 <exectest+0x285>

    // read output from child.
    printf(stdout, "exectest: test output\n");
    10e4:	8b 05 e6 14 00 00    	mov    0x14e6(%rip),%eax        # 25d0 <stdout>
    10ea:	be d9 21 00 00       	mov    $0x21d9,%esi
    10ef:	89 c7                	mov    %eax,%edi
    10f1:	b8 00 00 00 00       	mov    $0x0,%eax
    10f6:	e8 fa 02 00 00       	callq  13f5 <printf>
    char buf[16];
    char *expected = "echotest ok\n";
    10fb:	48 c7 45 f0 f0 21 00 	movq   $0x21f0,-0x10(%rbp)
    1102:	00 
    int count = read(fds[0], buf, 15);
    1103:	8b 45 b8             	mov    -0x48(%rbp),%eax
    1106:	48 8d 4d a8          	lea    -0x58(%rbp),%rcx
    110a:	ba 0f 00 00 00       	mov    $0xf,%edx
    110f:	48 89 ce             	mov    %rcx,%rsi
    1112:	89 c7                	mov    %eax,%edi
    1114:	e8 97 08 00 00       	callq  19b0 <read>
    1119:	89 45 ec             	mov    %eax,-0x14(%rbp)
    if (count == -1)
    111c:	83 7d ec ff          	cmpl   $0xffffffff,-0x14(%rbp)
    1120:	75 4c                	jne    116e <exectest+0x311>
      error("exectest: exec echotest no output");
    1122:	8b 05 a8 14 00 00    	mov    0x14a8(%rip),%eax        # 25d0 <stdout>
    1128:	ba 5b 01 00 00       	mov    $0x15b,%edx
    112d:	be 25 1d 00 00       	mov    $0x1d25,%esi
    1132:	89 c7                	mov    %eax,%edi
    1134:	b8 00 00 00 00       	mov    $0x0,%eax
    1139:	e8 b7 02 00 00       	callq  13f5 <printf>
    113e:	8b 05 8c 14 00 00    	mov    0x148c(%rip),%eax        # 25d0 <stdout>
    1144:	be 00 22 00 00       	mov    $0x2200,%esi
    1149:	89 c7                	mov    %eax,%edi
    114b:	b8 00 00 00 00       	mov    $0x0,%eax
    1150:	e8 a0 02 00 00       	callq  13f5 <printf>
    1155:	8b 05 75 14 00 00    	mov    0x1475(%rip),%eax        # 25d0 <stdout>
    115b:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
    1160:	89 c7                	mov    %eax,%edi
    1162:	b8 00 00 00 00       	mov    $0x0,%eax
    1167:	e8 89 02 00 00       	callq  13f5 <printf>
    116c:	eb fe                	jmp    116c <exectest+0x30f>

    buf[count] = 0;
    116e:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1171:	48 98                	cltq   
    1173:	c6 44 05 a8 00       	movb   $0x0,-0x58(%rbp,%rax,1)
    if (strcmp(buf, expected) != 0) {
    1178:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    117c:	48 8d 45 a8          	lea    -0x58(%rbp),%rax
    1180:	48 89 d6             	mov    %rdx,%rsi
    1183:	48 89 c7             	mov    %rax,%rdi
    1186:	e8 a7 05 00 00       	callq  1732 <strcmp>
    118b:	85 c0                	test   %eax,%eax
    118d:	74 7c                	je     120b <exectest+0x3ae>
      printf(stdout, "exectest: bad output:\n");
    118f:	8b 05 3b 14 00 00    	mov    0x143b(%rip),%eax        # 25d0 <stdout>
    1195:	be 22 22 00 00       	mov    $0x2222,%esi
    119a:	89 c7                	mov    %eax,%edi
    119c:	b8 00 00 00 00       	mov    $0x0,%eax
    11a1:	e8 4f 02 00 00       	callq  13f5 <printf>
      printf(stdout, buf);
    11a6:	8b 05 24 14 00 00    	mov    0x1424(%rip),%eax        # 25d0 <stdout>
    11ac:	48 8d 55 a8          	lea    -0x58(%rbp),%rdx
    11b0:	48 89 d6             	mov    %rdx,%rsi
    11b3:	89 c7                	mov    %eax,%edi
    11b5:	b8 00 00 00 00       	mov    $0x0,%eax
    11ba:	e8 36 02 00 00       	callq  13f5 <printf>
      error("exectest: output test failed");
    11bf:	8b 05 0b 14 00 00    	mov    0x140b(%rip),%eax        # 25d0 <stdout>
    11c5:	ba 61 01 00 00       	mov    $0x161,%edx
    11ca:	be 25 1d 00 00       	mov    $0x1d25,%esi
    11cf:	89 c7                	mov    %eax,%edi
    11d1:	b8 00 00 00 00       	mov    $0x0,%eax
    11d6:	e8 1a 02 00 00       	callq  13f5 <printf>
    11db:	8b 05 ef 13 00 00    	mov    0x13ef(%rip),%eax        # 25d0 <stdout>
    11e1:	be 39 22 00 00       	mov    $0x2239,%esi
    11e6:	89 c7                	mov    %eax,%edi
    11e8:	b8 00 00 00 00       	mov    $0x0,%eax
    11ed:	e8 03 02 00 00       	callq  13f5 <printf>
    11f2:	8b 05 d8 13 00 00    	mov    0x13d8(%rip),%eax        # 25d0 <stdout>
    11f8:	be 6e 1d 00 00       	mov    $0x1d6e,%esi
    11fd:	89 c7                	mov    %eax,%edi
    11ff:	b8 00 00 00 00       	mov    $0x0,%eax
    1204:	e8 ec 01 00 00       	callq  13f5 <printf>
    1209:	eb fe                	jmp    1209 <exectest+0x3ac>
    }
    printf(stdout, expected);
    120b:	8b 05 bf 13 00 00    	mov    0x13bf(%rip),%eax        # 25d0 <stdout>
    1211:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1215:	48 89 d6             	mov    %rdx,%rsi
    1218:	89 c7                	mov    %eax,%edi
    121a:	b8 00 00 00 00       	mov    $0x0,%eax
    121f:	e8 d1 01 00 00       	callq  13f5 <printf>
  }

  printf(1, "exectest passed: ok\n");
    1224:	be 56 22 00 00       	mov    $0x2256,%esi
    1229:	bf 01 00 00 00       	mov    $0x1,%edi
    122e:	b8 00 00 00 00       	mov    $0x0,%eax
    1233:	e8 bd 01 00 00       	callq  13f5 <printf>
}
    1238:	90                   	nop
    1239:	c9                   	leaveq 
    123a:	c3                   	retq   

000000000000123b <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
    123b:	55                   	push   %rbp
    123c:	48 89 e5             	mov    %rsp,%rbp
    123f:	48 83 ec 10          	sub    $0x10,%rsp
    1243:	89 7d fc             	mov    %edi,-0x4(%rbp)
    1246:	89 f0                	mov    %esi,%eax
    1248:	88 45 f8             	mov    %al,-0x8(%rbp)
    124b:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
    124f:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1252:	ba 01 00 00 00       	mov    $0x1,%edx
    1257:	48 89 ce             	mov    %rcx,%rsi
    125a:	89 c7                	mov    %eax,%edi
    125c:	e8 57 07 00 00       	callq  19b8 <write>
    1261:	90                   	nop
    1262:	c9                   	leaveq 
    1263:	c3                   	retq   

0000000000001264 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
    1264:	55                   	push   %rbp
    1265:	48 89 e5             	mov    %rsp,%rbp
    1268:	48 83 ec 40          	sub    $0x40,%rsp
    126c:	89 7d cc             	mov    %edi,-0x34(%rbp)
    126f:	89 75 c8             	mov    %esi,-0x38(%rbp)
    1272:	89 55 c4             	mov    %edx,-0x3c(%rbp)
    1275:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
    1278:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    127c:	74 1f                	je     129d <printint64+0x39>
    127e:	8b 45 c8             	mov    -0x38(%rbp),%eax
    1281:	c1 e8 1f             	shr    $0x1f,%eax
    1284:	0f b6 c0             	movzbl %al,%eax
    1287:	89 45 c0             	mov    %eax,-0x40(%rbp)
    128a:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    128e:	74 0d                	je     129d <printint64+0x39>
    x = -xx;
    1290:	8b 45 c8             	mov    -0x38(%rbp),%eax
    1293:	f7 d8                	neg    %eax
    1295:	48 98                	cltq   
    1297:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    129b:	eb 09                	jmp    12a6 <printint64+0x42>
  else
    x = xx;
    129d:	8b 45 c8             	mov    -0x38(%rbp),%eax
    12a0:	48 98                	cltq   
    12a2:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
    12a6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
    12ad:	8b 4d fc             	mov    -0x4(%rbp),%ecx
    12b0:	8d 41 01             	lea    0x1(%rcx),%eax
    12b3:	89 45 fc             	mov    %eax,-0x4(%rbp)
    12b6:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    12b9:	48 63 f0             	movslq %eax,%rsi
    12bc:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    12c0:	ba 00 00 00 00       	mov    $0x0,%edx
    12c5:	48 f7 f6             	div    %rsi
    12c8:	48 89 d0             	mov    %rdx,%rax
    12cb:	0f b6 90 e0 25 00 00 	movzbl 0x25e0(%rax),%edx
    12d2:	48 63 c1             	movslq %ecx,%rax
    12d5:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
    12d9:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    12dc:	48 63 f8             	movslq %eax,%rdi
    12df:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    12e3:	ba 00 00 00 00       	mov    $0x0,%edx
    12e8:	48 f7 f7             	div    %rdi
    12eb:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    12ef:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    12f4:	75 b7                	jne    12ad <printint64+0x49>

  if (sgn)
    12f6:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    12fa:	74 2b                	je     1327 <printint64+0xc3>
    buf[i++] = '-';
    12fc:	8b 45 fc             	mov    -0x4(%rbp),%eax
    12ff:	8d 50 01             	lea    0x1(%rax),%edx
    1302:	89 55 fc             	mov    %edx,-0x4(%rbp)
    1305:	48 98                	cltq   
    1307:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
    130c:	eb 19                	jmp    1327 <printint64+0xc3>
    putc(fd, buf[i]);
    130e:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1311:	48 98                	cltq   
    1313:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
    1318:	0f be d0             	movsbl %al,%edx
    131b:	8b 45 cc             	mov    -0x34(%rbp),%eax
    131e:	89 d6                	mov    %edx,%esi
    1320:	89 c7                	mov    %eax,%edi
    1322:	e8 14 ff ff ff       	callq  123b <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
    1327:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
    132b:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    132f:	79 dd                	jns    130e <printint64+0xaa>
    putc(fd, buf[i]);
}
    1331:	90                   	nop
    1332:	c9                   	leaveq 
    1333:	c3                   	retq   

0000000000001334 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
    1334:	55                   	push   %rbp
    1335:	48 89 e5             	mov    %rsp,%rbp
    1338:	48 83 ec 30          	sub    $0x30,%rsp
    133c:	89 7d dc             	mov    %edi,-0x24(%rbp)
    133f:	89 75 d8             	mov    %esi,-0x28(%rbp)
    1342:	89 55 d4             	mov    %edx,-0x2c(%rbp)
    1345:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
    1348:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
    134f:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
    1353:	74 17                	je     136c <printint+0x38>
    1355:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
    1359:	79 11                	jns    136c <printint+0x38>
    neg = 1;
    135b:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
    1362:	8b 45 d8             	mov    -0x28(%rbp),%eax
    1365:	f7 d8                	neg    %eax
    1367:	89 45 f4             	mov    %eax,-0xc(%rbp)
    136a:	eb 06                	jmp    1372 <printint+0x3e>
  } else {
    x = xx;
    136c:	8b 45 d8             	mov    -0x28(%rbp),%eax
    136f:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
    1372:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
    1379:	8b 4d fc             	mov    -0x4(%rbp),%ecx
    137c:	8d 41 01             	lea    0x1(%rcx),%eax
    137f:	89 45 fc             	mov    %eax,-0x4(%rbp)
    1382:	8b 75 d4             	mov    -0x2c(%rbp),%esi
    1385:	8b 45 f4             	mov    -0xc(%rbp),%eax
    1388:	ba 00 00 00 00       	mov    $0x0,%edx
    138d:	f7 f6                	div    %esi
    138f:	89 d0                	mov    %edx,%eax
    1391:	89 c0                	mov    %eax,%eax
    1393:	0f b6 90 00 26 00 00 	movzbl 0x2600(%rax),%edx
    139a:	48 63 c1             	movslq %ecx,%rax
    139d:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
    13a1:	8b 7d d4             	mov    -0x2c(%rbp),%edi
    13a4:	8b 45 f4             	mov    -0xc(%rbp),%eax
    13a7:	ba 00 00 00 00       	mov    $0x0,%edx
    13ac:	f7 f7                	div    %edi
    13ae:	89 45 f4             	mov    %eax,-0xc(%rbp)
    13b1:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
    13b5:	75 c2                	jne    1379 <printint+0x45>
  if (neg)
    13b7:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
    13bb:	74 2b                	je     13e8 <printint+0xb4>
    buf[i++] = '-';
    13bd:	8b 45 fc             	mov    -0x4(%rbp),%eax
    13c0:	8d 50 01             	lea    0x1(%rax),%edx
    13c3:	89 55 fc             	mov    %edx,-0x4(%rbp)
    13c6:	48 98                	cltq   
    13c8:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
    13cd:	eb 19                	jmp    13e8 <printint+0xb4>
    putc(fd, buf[i]);
    13cf:	8b 45 fc             	mov    -0x4(%rbp),%eax
    13d2:	48 98                	cltq   
    13d4:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
    13d9:	0f be d0             	movsbl %al,%edx
    13dc:	8b 45 dc             	mov    -0x24(%rbp),%eax
    13df:	89 d6                	mov    %edx,%esi
    13e1:	89 c7                	mov    %eax,%edi
    13e3:	e8 53 fe ff ff       	callq  123b <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
    13e8:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
    13ec:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    13f0:	79 dd                	jns    13cf <printint+0x9b>
    putc(fd, buf[i]);
}
    13f2:	90                   	nop
    13f3:	c9                   	leaveq 
    13f4:	c3                   	retq   

00000000000013f5 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
    13f5:	55                   	push   %rbp
    13f6:	48 89 e5             	mov    %rsp,%rbp
    13f9:	48 83 ec 70          	sub    $0x70,%rsp
    13fd:	89 7d 9c             	mov    %edi,-0x64(%rbp)
    1400:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
    1404:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
    1408:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
    140c:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
    1410:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
    1414:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
    141b:	48 8d 45 10          	lea    0x10(%rbp),%rax
    141f:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
    1423:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
    1427:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
    142b:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
    1432:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
    1439:	e9 68 02 00 00       	jmpq   16a6 <printf+0x2b1>
    c = fmt[i] & 0xff;
    143e:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    1441:	48 63 d0             	movslq %eax,%rdx
    1444:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    1448:	48 01 d0             	add    %rdx,%rax
    144b:	0f b6 00             	movzbl (%rax),%eax
    144e:	0f be c0             	movsbl %al,%eax
    1451:	25 ff 00 00 00       	and    $0xff,%eax
    1456:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
    1459:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    145d:	75 30                	jne    148f <printf+0x9a>
      if (c == '%') {
    145f:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    1463:	75 13                	jne    1478 <printf+0x83>
        state = '%';
    1465:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
    146c:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
    1473:	e9 2a 02 00 00       	jmpq   16a2 <printf+0x2ad>
      } else {
        putc(fd, c);
    1478:	8b 45 b8             	mov    -0x48(%rbp),%eax
    147b:	0f be d0             	movsbl %al,%edx
    147e:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1481:	89 d6                	mov    %edx,%esi
    1483:	89 c7                	mov    %eax,%edi
    1485:	e8 b1 fd ff ff       	callq  123b <putc>
    148a:	e9 13 02 00 00       	jmpq   16a2 <printf+0x2ad>
      }
    } else if (state == '%') {
    148f:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
    1493:	0f 85 09 02 00 00    	jne    16a2 <printf+0x2ad>
      if (c == 'l') {
    1499:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
    149d:	75 0c                	jne    14ab <printf+0xb6>
        lflag = 1;
    149f:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
    14a6:	e9 f7 01 00 00       	jmpq   16a2 <printf+0x2ad>
      } else if (c == 'd') {
    14ab:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
    14af:	0f 85 95 00 00 00    	jne    154a <printf+0x155>
        if (lflag == 1)
    14b5:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    14b9:	75 49                	jne    1504 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
    14bb:	8b 45 a0             	mov    -0x60(%rbp),%eax
    14be:	83 f8 30             	cmp    $0x30,%eax
    14c1:	73 17                	jae    14da <printf+0xe5>
    14c3:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    14c7:	8b 55 a0             	mov    -0x60(%rbp),%edx
    14ca:	89 d2                	mov    %edx,%edx
    14cc:	48 01 d0             	add    %rdx,%rax
    14cf:	8b 55 a0             	mov    -0x60(%rbp),%edx
    14d2:	83 c2 08             	add    $0x8,%edx
    14d5:	89 55 a0             	mov    %edx,-0x60(%rbp)
    14d8:	eb 0c                	jmp    14e6 <printf+0xf1>
    14da:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    14de:	48 8d 50 08          	lea    0x8(%rax),%rdx
    14e2:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    14e6:	48 8b 00             	mov    (%rax),%rax
    14e9:	89 c6                	mov    %eax,%esi
    14eb:	8b 45 9c             	mov    -0x64(%rbp),%eax
    14ee:	b9 01 00 00 00       	mov    $0x1,%ecx
    14f3:	ba 0a 00 00 00       	mov    $0xa,%edx
    14f8:	89 c7                	mov    %eax,%edi
    14fa:	e8 65 fd ff ff       	callq  1264 <printint64>
    14ff:	e9 97 01 00 00       	jmpq   169b <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
    1504:	8b 45 a0             	mov    -0x60(%rbp),%eax
    1507:	83 f8 30             	cmp    $0x30,%eax
    150a:	73 17                	jae    1523 <printf+0x12e>
    150c:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1510:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1513:	89 d2                	mov    %edx,%edx
    1515:	48 01 d0             	add    %rdx,%rax
    1518:	8b 55 a0             	mov    -0x60(%rbp),%edx
    151b:	83 c2 08             	add    $0x8,%edx
    151e:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1521:	eb 0c                	jmp    152f <printf+0x13a>
    1523:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1527:	48 8d 50 08          	lea    0x8(%rax),%rdx
    152b:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    152f:	8b 30                	mov    (%rax),%esi
    1531:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1534:	b9 01 00 00 00       	mov    $0x1,%ecx
    1539:	ba 0a 00 00 00       	mov    $0xa,%edx
    153e:	89 c7                	mov    %eax,%edi
    1540:	e8 ef fd ff ff       	callq  1334 <printint>
    1545:	e9 51 01 00 00       	jmpq   169b <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
    154a:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
    154e:	74 0a                	je     155a <printf+0x165>
    1550:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
    1554:	0f 85 95 00 00 00    	jne    15ef <printf+0x1fa>
        if (lflag == 1)
    155a:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    155e:	75 49                	jne    15a9 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
    1560:	8b 45 a0             	mov    -0x60(%rbp),%eax
    1563:	83 f8 30             	cmp    $0x30,%eax
    1566:	73 17                	jae    157f <printf+0x18a>
    1568:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    156c:	8b 55 a0             	mov    -0x60(%rbp),%edx
    156f:	89 d2                	mov    %edx,%edx
    1571:	48 01 d0             	add    %rdx,%rax
    1574:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1577:	83 c2 08             	add    $0x8,%edx
    157a:	89 55 a0             	mov    %edx,-0x60(%rbp)
    157d:	eb 0c                	jmp    158b <printf+0x196>
    157f:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1583:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1587:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    158b:	48 8b 00             	mov    (%rax),%rax
    158e:	89 c6                	mov    %eax,%esi
    1590:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1593:	b9 00 00 00 00       	mov    $0x0,%ecx
    1598:	ba 10 00 00 00       	mov    $0x10,%edx
    159d:	89 c7                	mov    %eax,%edi
    159f:	e8 c0 fc ff ff       	callq  1264 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    15a4:	e9 f2 00 00 00       	jmpq   169b <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
    15a9:	8b 45 a0             	mov    -0x60(%rbp),%eax
    15ac:	83 f8 30             	cmp    $0x30,%eax
    15af:	73 17                	jae    15c8 <printf+0x1d3>
    15b1:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    15b5:	8b 55 a0             	mov    -0x60(%rbp),%edx
    15b8:	89 d2                	mov    %edx,%edx
    15ba:	48 01 d0             	add    %rdx,%rax
    15bd:	8b 55 a0             	mov    -0x60(%rbp),%edx
    15c0:	83 c2 08             	add    $0x8,%edx
    15c3:	89 55 a0             	mov    %edx,-0x60(%rbp)
    15c6:	eb 0c                	jmp    15d4 <printf+0x1df>
    15c8:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    15cc:	48 8d 50 08          	lea    0x8(%rax),%rdx
    15d0:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    15d4:	8b 30                	mov    (%rax),%esi
    15d6:	8b 45 9c             	mov    -0x64(%rbp),%eax
    15d9:	b9 00 00 00 00       	mov    $0x0,%ecx
    15de:	ba 10 00 00 00       	mov    $0x10,%edx
    15e3:	89 c7                	mov    %eax,%edi
    15e5:	e8 4a fd ff ff       	callq  1334 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    15ea:	e9 ac 00 00 00       	jmpq   169b <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
    15ef:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
    15f3:	75 6b                	jne    1660 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
    15f5:	8b 45 a0             	mov    -0x60(%rbp),%eax
    15f8:	83 f8 30             	cmp    $0x30,%eax
    15fb:	73 17                	jae    1614 <printf+0x21f>
    15fd:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1601:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1604:	89 d2                	mov    %edx,%edx
    1606:	48 01 d0             	add    %rdx,%rax
    1609:	8b 55 a0             	mov    -0x60(%rbp),%edx
    160c:	83 c2 08             	add    $0x8,%edx
    160f:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1612:	eb 0c                	jmp    1620 <printf+0x22b>
    1614:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1618:	48 8d 50 08          	lea    0x8(%rax),%rdx
    161c:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1620:	48 8b 00             	mov    (%rax),%rax
    1623:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
    1627:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
    162c:	75 25                	jne    1653 <printf+0x25e>
          s = "(null)";
    162e:	48 c7 45 c8 6b 22 00 	movq   $0x226b,-0x38(%rbp)
    1635:	00 
        for (; *s; s++)
    1636:	eb 1b                	jmp    1653 <printf+0x25e>
          putc(fd, *s);
    1638:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    163c:	0f b6 00             	movzbl (%rax),%eax
    163f:	0f be d0             	movsbl %al,%edx
    1642:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1645:	89 d6                	mov    %edx,%esi
    1647:	89 c7                	mov    %eax,%edi
    1649:	e8 ed fb ff ff       	callq  123b <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
    164e:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
    1653:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    1657:	0f b6 00             	movzbl (%rax),%eax
    165a:	84 c0                	test   %al,%al
    165c:	75 da                	jne    1638 <printf+0x243>
    165e:	eb 3b                	jmp    169b <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
    1660:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    1664:	75 14                	jne    167a <printf+0x285>
        putc(fd, c);
    1666:	8b 45 b8             	mov    -0x48(%rbp),%eax
    1669:	0f be d0             	movsbl %al,%edx
    166c:	8b 45 9c             	mov    -0x64(%rbp),%eax
    166f:	89 d6                	mov    %edx,%esi
    1671:	89 c7                	mov    %eax,%edi
    1673:	e8 c3 fb ff ff       	callq  123b <putc>
    1678:	eb 21                	jmp    169b <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
    167a:	8b 45 9c             	mov    -0x64(%rbp),%eax
    167d:	be 25 00 00 00       	mov    $0x25,%esi
    1682:	89 c7                	mov    %eax,%edi
    1684:	e8 b2 fb ff ff       	callq  123b <putc>
        putc(fd, c);
    1689:	8b 45 b8             	mov    -0x48(%rbp),%eax
    168c:	0f be d0             	movsbl %al,%edx
    168f:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1692:	89 d6                	mov    %edx,%esi
    1694:	89 c7                	mov    %eax,%edi
    1696:	e8 a0 fb ff ff       	callq  123b <putc>
      }
      state = 0;
    169b:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
    16a2:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
    16a6:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    16a9:	48 63 d0             	movslq %eax,%rdx
    16ac:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    16b0:	48 01 d0             	add    %rdx,%rax
    16b3:	0f b6 00             	movzbl (%rax),%eax
    16b6:	84 c0                	test   %al,%al
    16b8:	0f 85 80 fd ff ff    	jne    143e <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
    16be:	90                   	nop
    16bf:	c9                   	leaveq 
    16c0:	c3                   	retq   

00000000000016c1 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
    16c1:	55                   	push   %rbp
    16c2:	48 89 e5             	mov    %rsp,%rbp
    16c5:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    16c9:	89 75 f4             	mov    %esi,-0xc(%rbp)
    16cc:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
    16cf:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
    16d3:	8b 55 f0             	mov    -0x10(%rbp),%edx
    16d6:	8b 45 f4             	mov    -0xc(%rbp),%eax
    16d9:	48 89 ce             	mov    %rcx,%rsi
    16dc:	48 89 f7             	mov    %rsi,%rdi
    16df:	89 d1                	mov    %edx,%ecx
    16e1:	fc                   	cld    
    16e2:	f3 aa                	rep stos %al,%es:(%rdi)
    16e4:	89 ca                	mov    %ecx,%edx
    16e6:	48 89 fe             	mov    %rdi,%rsi
    16e9:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
    16ed:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
    16f0:	90                   	nop
    16f1:	5d                   	pop    %rbp
    16f2:	c3                   	retq   

00000000000016f3 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
    16f3:	55                   	push   %rbp
    16f4:	48 89 e5             	mov    %rsp,%rbp
    16f7:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    16fb:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
    16ff:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1703:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
    1707:	90                   	nop
    1708:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    170c:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1710:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    1714:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    1718:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    171c:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
    1720:	0f b6 12             	movzbl (%rdx),%edx
    1723:	88 10                	mov    %dl,(%rax)
    1725:	0f b6 00             	movzbl (%rax),%eax
    1728:	84 c0                	test   %al,%al
    172a:	75 dc                	jne    1708 <strcpy+0x15>
    ;
  return os;
    172c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    1730:	5d                   	pop    %rbp
    1731:	c3                   	retq   

0000000000001732 <strcmp>:

int strcmp(const char *p, const char *q) {
    1732:	55                   	push   %rbp
    1733:	48 89 e5             	mov    %rsp,%rbp
    1736:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    173a:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
    173e:	eb 0a                	jmp    174a <strcmp+0x18>
    p++, q++;
    1740:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    1745:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
    174a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    174e:	0f b6 00             	movzbl (%rax),%eax
    1751:	84 c0                	test   %al,%al
    1753:	74 12                	je     1767 <strcmp+0x35>
    1755:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1759:	0f b6 10             	movzbl (%rax),%edx
    175c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1760:	0f b6 00             	movzbl (%rax),%eax
    1763:	38 c2                	cmp    %al,%dl
    1765:	74 d9                	je     1740 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
    1767:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    176b:	0f b6 00             	movzbl (%rax),%eax
    176e:	0f b6 d0             	movzbl %al,%edx
    1771:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1775:	0f b6 00             	movzbl (%rax),%eax
    1778:	0f b6 c0             	movzbl %al,%eax
    177b:	29 c2                	sub    %eax,%edx
    177d:	89 d0                	mov    %edx,%eax
}
    177f:	5d                   	pop    %rbp
    1780:	c3                   	retq   

0000000000001781 <strlen>:

uint strlen(char *s) {
    1781:	55                   	push   %rbp
    1782:	48 89 e5             	mov    %rsp,%rbp
    1785:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
    1789:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1790:	eb 04                	jmp    1796 <strlen+0x15>
    1792:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    1796:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1799:	48 63 d0             	movslq %eax,%rdx
    179c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    17a0:	48 01 d0             	add    %rdx,%rax
    17a3:	0f b6 00             	movzbl (%rax),%eax
    17a6:	84 c0                	test   %al,%al
    17a8:	75 e8                	jne    1792 <strlen+0x11>
    ;
  return n;
    17aa:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    17ad:	5d                   	pop    %rbp
    17ae:	c3                   	retq   

00000000000017af <memset>:

void *memset(void *dst, int c, uint n) {
    17af:	55                   	push   %rbp
    17b0:	48 89 e5             	mov    %rsp,%rbp
    17b3:	48 83 ec 10          	sub    $0x10,%rsp
    17b7:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    17bb:	89 75 f4             	mov    %esi,-0xc(%rbp)
    17be:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
    17c1:	8b 55 f0             	mov    -0x10(%rbp),%edx
    17c4:	8b 4d f4             	mov    -0xc(%rbp),%ecx
    17c7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17cb:	89 ce                	mov    %ecx,%esi
    17cd:	48 89 c7             	mov    %rax,%rdi
    17d0:	e8 ec fe ff ff       	callq  16c1 <stosb>
  return dst;
    17d5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    17d9:	c9                   	leaveq 
    17da:	c3                   	retq   

00000000000017db <strchr>:

char *strchr(const char *s, char c) {
    17db:	55                   	push   %rbp
    17dc:	48 89 e5             	mov    %rsp,%rbp
    17df:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    17e3:	89 f0                	mov    %esi,%eax
    17e5:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
    17e8:	eb 17                	jmp    1801 <strchr+0x26>
    if (*s == c)
    17ea:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17ee:	0f b6 00             	movzbl (%rax),%eax
    17f1:	3a 45 f4             	cmp    -0xc(%rbp),%al
    17f4:	75 06                	jne    17fc <strchr+0x21>
      return (char *)s;
    17f6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17fa:	eb 15                	jmp    1811 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
    17fc:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    1801:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1805:	0f b6 00             	movzbl (%rax),%eax
    1808:	84 c0                	test   %al,%al
    180a:	75 de                	jne    17ea <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
    180c:	b8 00 00 00 00       	mov    $0x0,%eax
}
    1811:	5d                   	pop    %rbp
    1812:	c3                   	retq   

0000000000001813 <gets>:

char *gets(char *buf, int max) {
    1813:	55                   	push   %rbp
    1814:	48 89 e5             	mov    %rsp,%rbp
    1817:	48 83 ec 20          	sub    $0x20,%rsp
    181b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    181f:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    1822:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1829:	eb 48                	jmp    1873 <gets+0x60>
    cc = read(0, &c, 1);
    182b:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
    182f:	ba 01 00 00 00       	mov    $0x1,%edx
    1834:	48 89 c6             	mov    %rax,%rsi
    1837:	bf 00 00 00 00       	mov    $0x0,%edi
    183c:	e8 6f 01 00 00       	callq  19b0 <read>
    1841:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
    1844:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
    1848:	7e 36                	jle    1880 <gets+0x6d>
      break;
    buf[i++] = c;
    184a:	8b 45 fc             	mov    -0x4(%rbp),%eax
    184d:	8d 50 01             	lea    0x1(%rax),%edx
    1850:	89 55 fc             	mov    %edx,-0x4(%rbp)
    1853:	48 63 d0             	movslq %eax,%rdx
    1856:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    185a:	48 01 c2             	add    %rax,%rdx
    185d:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1861:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
    1863:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1867:	3c 0a                	cmp    $0xa,%al
    1869:	74 16                	je     1881 <gets+0x6e>
    186b:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    186f:	3c 0d                	cmp    $0xd,%al
    1871:	74 0e                	je     1881 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    1873:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1876:	83 c0 01             	add    $0x1,%eax
    1879:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
    187c:	7c ad                	jl     182b <gets+0x18>
    187e:	eb 01                	jmp    1881 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
    1880:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
    1881:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1884:	48 63 d0             	movslq %eax,%rdx
    1887:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    188b:	48 01 d0             	add    %rdx,%rax
    188e:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
    1891:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
    1895:	c9                   	leaveq 
    1896:	c3                   	retq   

0000000000001897 <stat>:

int stat(char *n, struct stat *st) {
    1897:	55                   	push   %rbp
    1898:	48 89 e5             	mov    %rsp,%rbp
    189b:	48 83 ec 20          	sub    $0x20,%rsp
    189f:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    18a3:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    18a7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    18ab:	be 00 00 00 00       	mov    $0x0,%esi
    18b0:	48 89 c7             	mov    %rax,%rdi
    18b3:	e8 20 01 00 00       	callq  19d8 <open>
    18b8:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
    18bb:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    18bf:	79 07                	jns    18c8 <stat+0x31>
    return -1;
    18c1:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    18c6:	eb 21                	jmp    18e9 <stat+0x52>
  r = fstat(fd, st);
    18c8:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    18cc:	8b 45 fc             	mov    -0x4(%rbp),%eax
    18cf:	48 89 d6             	mov    %rdx,%rsi
    18d2:	89 c7                	mov    %eax,%edi
    18d4:	e8 17 01 00 00       	callq  19f0 <fstat>
    18d9:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
    18dc:	8b 45 fc             	mov    -0x4(%rbp),%eax
    18df:	89 c7                	mov    %eax,%edi
    18e1:	e8 da 00 00 00       	callq  19c0 <close>
  return r;
    18e6:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
    18e9:	c9                   	leaveq 
    18ea:	c3                   	retq   

00000000000018eb <atoi>:

int atoi(const char *s) {
    18eb:	55                   	push   %rbp
    18ec:	48 89 e5             	mov    %rsp,%rbp
    18ef:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
    18f3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
    18fa:	eb 28                	jmp    1924 <atoi+0x39>
    n = n * 10 + *s++ - '0';
    18fc:	8b 55 fc             	mov    -0x4(%rbp),%edx
    18ff:	89 d0                	mov    %edx,%eax
    1901:	c1 e0 02             	shl    $0x2,%eax
    1904:	01 d0                	add    %edx,%eax
    1906:	01 c0                	add    %eax,%eax
    1908:	89 c1                	mov    %eax,%ecx
    190a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    190e:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1912:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    1916:	0f b6 00             	movzbl (%rax),%eax
    1919:	0f be c0             	movsbl %al,%eax
    191c:	01 c8                	add    %ecx,%eax
    191e:	83 e8 30             	sub    $0x30,%eax
    1921:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
    1924:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1928:	0f b6 00             	movzbl (%rax),%eax
    192b:	3c 2f                	cmp    $0x2f,%al
    192d:	7e 0b                	jle    193a <atoi+0x4f>
    192f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1933:	0f b6 00             	movzbl (%rax),%eax
    1936:	3c 39                	cmp    $0x39,%al
    1938:	7e c2                	jle    18fc <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
    193a:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    193d:	5d                   	pop    %rbp
    193e:	c3                   	retq   

000000000000193f <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
    193f:	55                   	push   %rbp
    1940:	48 89 e5             	mov    %rsp,%rbp
    1943:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1947:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
    194b:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
    194e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1952:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
    1956:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
    195a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
    195e:	eb 1d                	jmp    197d <memmove+0x3e>
    *dst++ = *src++;
    1960:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1964:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1968:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
    196c:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1970:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    1974:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
    1978:	0f b6 12             	movzbl (%rdx),%edx
    197b:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
    197d:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1980:	8d 50 ff             	lea    -0x1(%rax),%edx
    1983:	89 55 dc             	mov    %edx,-0x24(%rbp)
    1986:	85 c0                	test   %eax,%eax
    1988:	7f d6                	jg     1960 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
    198a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    198e:	5d                   	pop    %rbp
    198f:	c3                   	retq   

0000000000001990 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
    1990:	b8 01 00 00 00       	mov    $0x1,%eax
    1995:	cd 40                	int    $0x40
    1997:	c3                   	retq   

0000000000001998 <exit>:
SYSCALL(exit)
    1998:	b8 02 00 00 00       	mov    $0x2,%eax
    199d:	cd 40                	int    $0x40
    199f:	c3                   	retq   

00000000000019a0 <wait>:
SYSCALL(wait)
    19a0:	b8 03 00 00 00       	mov    $0x3,%eax
    19a5:	cd 40                	int    $0x40
    19a7:	c3                   	retq   

00000000000019a8 <pipe>:
SYSCALL(pipe)
    19a8:	b8 04 00 00 00       	mov    $0x4,%eax
    19ad:	cd 40                	int    $0x40
    19af:	c3                   	retq   

00000000000019b0 <read>:
SYSCALL(read)
    19b0:	b8 05 00 00 00       	mov    $0x5,%eax
    19b5:	cd 40                	int    $0x40
    19b7:	c3                   	retq   

00000000000019b8 <write>:
SYSCALL(write)
    19b8:	b8 10 00 00 00       	mov    $0x10,%eax
    19bd:	cd 40                	int    $0x40
    19bf:	c3                   	retq   

00000000000019c0 <close>:
SYSCALL(close)
    19c0:	b8 15 00 00 00       	mov    $0x15,%eax
    19c5:	cd 40                	int    $0x40
    19c7:	c3                   	retq   

00000000000019c8 <kill>:
SYSCALL(kill)
    19c8:	b8 06 00 00 00       	mov    $0x6,%eax
    19cd:	cd 40                	int    $0x40
    19cf:	c3                   	retq   

00000000000019d0 <exec>:
SYSCALL(exec)
    19d0:	b8 07 00 00 00       	mov    $0x7,%eax
    19d5:	cd 40                	int    $0x40
    19d7:	c3                   	retq   

00000000000019d8 <open>:
SYSCALL(open)
    19d8:	b8 0f 00 00 00       	mov    $0xf,%eax
    19dd:	cd 40                	int    $0x40
    19df:	c3                   	retq   

00000000000019e0 <mknod>:
SYSCALL(mknod)
    19e0:	b8 11 00 00 00       	mov    $0x11,%eax
    19e5:	cd 40                	int    $0x40
    19e7:	c3                   	retq   

00000000000019e8 <unlink>:
SYSCALL(unlink)
    19e8:	b8 12 00 00 00       	mov    $0x12,%eax
    19ed:	cd 40                	int    $0x40
    19ef:	c3                   	retq   

00000000000019f0 <fstat>:
SYSCALL(fstat)
    19f0:	b8 08 00 00 00       	mov    $0x8,%eax
    19f5:	cd 40                	int    $0x40
    19f7:	c3                   	retq   

00000000000019f8 <link>:
SYSCALL(link)
    19f8:	b8 13 00 00 00       	mov    $0x13,%eax
    19fd:	cd 40                	int    $0x40
    19ff:	c3                   	retq   

0000000000001a00 <mkdir>:
SYSCALL(mkdir)
    1a00:	b8 14 00 00 00       	mov    $0x14,%eax
    1a05:	cd 40                	int    $0x40
    1a07:	c3                   	retq   

0000000000001a08 <chdir>:
SYSCALL(chdir)
    1a08:	b8 09 00 00 00       	mov    $0x9,%eax
    1a0d:	cd 40                	int    $0x40
    1a0f:	c3                   	retq   

0000000000001a10 <dup>:
SYSCALL(dup)
    1a10:	b8 0a 00 00 00       	mov    $0xa,%eax
    1a15:	cd 40                	int    $0x40
    1a17:	c3                   	retq   

0000000000001a18 <getpid>:
SYSCALL(getpid)
    1a18:	b8 0b 00 00 00       	mov    $0xb,%eax
    1a1d:	cd 40                	int    $0x40
    1a1f:	c3                   	retq   

0000000000001a20 <sbrk>:
SYSCALL(sbrk)
    1a20:	b8 0c 00 00 00       	mov    $0xc,%eax
    1a25:	cd 40                	int    $0x40
    1a27:	c3                   	retq   

0000000000001a28 <sleep>:
SYSCALL(sleep)
    1a28:	b8 0d 00 00 00       	mov    $0xd,%eax
    1a2d:	cd 40                	int    $0x40
    1a2f:	c3                   	retq   

0000000000001a30 <uptime>:
SYSCALL(uptime)
    1a30:	b8 0e 00 00 00       	mov    $0xe,%eax
    1a35:	cd 40                	int    $0x40
    1a37:	c3                   	retq   

0000000000001a38 <sysinfo>:
SYSCALL(sysinfo)
    1a38:	b8 16 00 00 00       	mov    $0x16,%eax
    1a3d:	cd 40                	int    $0x40
    1a3f:	c3                   	retq   

0000000000001a40 <crashn>:
SYSCALL(crashn)
    1a40:	b8 17 00 00 00       	mov    $0x17,%eax
    1a45:	cd 40                	int    $0x40
    1a47:	c3                   	retq   

0000000000001a48 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
    1a48:	55                   	push   %rbp
    1a49:	48 89 e5             	mov    %rsp,%rbp
    1a4c:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
    1a50:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1a54:	48 83 e8 10          	sub    $0x10,%rax
    1a58:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1a5c:	48 8b 05 cd 0b 00 00 	mov    0xbcd(%rip),%rax        # 2630 <freep>
    1a63:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1a67:	eb 2f                	jmp    1a98 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    1a69:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a6d:	48 8b 00             	mov    (%rax),%rax
    1a70:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1a74:	77 17                	ja     1a8d <free+0x45>
    1a76:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1a7a:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1a7e:	77 2f                	ja     1aaf <free+0x67>
    1a80:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a84:	48 8b 00             	mov    (%rax),%rax
    1a87:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1a8b:	77 22                	ja     1aaf <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1a8d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1a91:	48 8b 00             	mov    (%rax),%rax
    1a94:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1a98:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1a9c:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1aa0:	76 c7                	jbe    1a69 <free+0x21>
    1aa2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1aa6:	48 8b 00             	mov    (%rax),%rax
    1aa9:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1aad:	76 ba                	jbe    1a69 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
    1aaf:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1ab3:	8b 40 08             	mov    0x8(%rax),%eax
    1ab6:	89 c0                	mov    %eax,%eax
    1ab8:	48 c1 e0 04          	shl    $0x4,%rax
    1abc:	48 89 c2             	mov    %rax,%rdx
    1abf:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1ac3:	48 01 c2             	add    %rax,%rdx
    1ac6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1aca:	48 8b 00             	mov    (%rax),%rax
    1acd:	48 39 c2             	cmp    %rax,%rdx
    1ad0:	75 2d                	jne    1aff <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
    1ad2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1ad6:	8b 50 08             	mov    0x8(%rax),%edx
    1ad9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1add:	48 8b 00             	mov    (%rax),%rax
    1ae0:	8b 40 08             	mov    0x8(%rax),%eax
    1ae3:	01 c2                	add    %eax,%edx
    1ae5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1ae9:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
    1aec:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1af0:	48 8b 00             	mov    (%rax),%rax
    1af3:	48 8b 10             	mov    (%rax),%rdx
    1af6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1afa:	48 89 10             	mov    %rdx,(%rax)
    1afd:	eb 0e                	jmp    1b0d <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
    1aff:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b03:	48 8b 10             	mov    (%rax),%rdx
    1b06:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1b0a:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
    1b0d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b11:	8b 40 08             	mov    0x8(%rax),%eax
    1b14:	89 c0                	mov    %eax,%eax
    1b16:	48 c1 e0 04          	shl    $0x4,%rax
    1b1a:	48 89 c2             	mov    %rax,%rdx
    1b1d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b21:	48 01 d0             	add    %rdx,%rax
    1b24:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1b28:	75 27                	jne    1b51 <free+0x109>
    p->s.size += bp->s.size;
    1b2a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b2e:	8b 50 08             	mov    0x8(%rax),%edx
    1b31:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1b35:	8b 40 08             	mov    0x8(%rax),%eax
    1b38:	01 c2                	add    %eax,%edx
    1b3a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b3e:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
    1b41:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1b45:	48 8b 10             	mov    (%rax),%rdx
    1b48:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b4c:	48 89 10             	mov    %rdx,(%rax)
    1b4f:	eb 0b                	jmp    1b5c <free+0x114>
  } else
    p->s.ptr = bp;
    1b51:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b55:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1b59:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
    1b5c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1b60:	48 89 05 c9 0a 00 00 	mov    %rax,0xac9(%rip)        # 2630 <freep>
}
    1b67:	90                   	nop
    1b68:	5d                   	pop    %rbp
    1b69:	c3                   	retq   

0000000000001b6a <morecore>:

static Header *morecore(uint nu) {
    1b6a:	55                   	push   %rbp
    1b6b:	48 89 e5             	mov    %rsp,%rbp
    1b6e:	48 83 ec 20          	sub    $0x20,%rsp
    1b72:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
    1b75:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
    1b7c:	77 07                	ja     1b85 <morecore+0x1b>
    nu = 4096;
    1b7e:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
    1b85:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1b88:	c1 e0 04             	shl    $0x4,%eax
    1b8b:	89 c7                	mov    %eax,%edi
    1b8d:	e8 8e fe ff ff       	callq  1a20 <sbrk>
    1b92:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
    1b96:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
    1b9b:	75 07                	jne    1ba4 <morecore+0x3a>
    return 0;
    1b9d:	b8 00 00 00 00       	mov    $0x0,%eax
    1ba2:	eb 29                	jmp    1bcd <morecore+0x63>
  hp = (Header *)p;
    1ba4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1ba8:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
    1bac:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1bb0:	8b 55 ec             	mov    -0x14(%rbp),%edx
    1bb3:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
    1bb6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1bba:	48 83 c0 10          	add    $0x10,%rax
    1bbe:	48 89 c7             	mov    %rax,%rdi
    1bc1:	e8 82 fe ff ff       	callq  1a48 <free>
  return freep;
    1bc6:	48 8b 05 63 0a 00 00 	mov    0xa63(%rip),%rax        # 2630 <freep>
}
    1bcd:	c9                   	leaveq 
    1bce:	c3                   	retq   

0000000000001bcf <malloc>:

void *malloc(uint nbytes) {
    1bcf:	55                   	push   %rbp
    1bd0:	48 89 e5             	mov    %rsp,%rbp
    1bd3:	48 83 ec 30          	sub    $0x30,%rsp
    1bd7:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
    1bda:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1bdd:	48 83 c0 0f          	add    $0xf,%rax
    1be1:	48 c1 e8 04          	shr    $0x4,%rax
    1be5:	83 c0 01             	add    $0x1,%eax
    1be8:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
    1beb:	48 8b 05 3e 0a 00 00 	mov    0xa3e(%rip),%rax        # 2630 <freep>
    1bf2:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    1bf6:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    1bfb:	75 2b                	jne    1c28 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
    1bfd:	48 c7 45 f0 20 26 00 	movq   $0x2620,-0x10(%rbp)
    1c04:	00 
    1c05:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1c09:	48 89 05 20 0a 00 00 	mov    %rax,0xa20(%rip)        # 2630 <freep>
    1c10:	48 8b 05 19 0a 00 00 	mov    0xa19(%rip),%rax        # 2630 <freep>
    1c17:	48 89 05 02 0a 00 00 	mov    %rax,0xa02(%rip)        # 2620 <base>
    base.s.size = 0;
    1c1e:	c7 05 00 0a 00 00 00 	movl   $0x0,0xa00(%rip)        # 2628 <base+0x8>
    1c25:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1c28:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1c2c:	48 8b 00             	mov    (%rax),%rax
    1c2f:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
    1c33:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c37:	8b 40 08             	mov    0x8(%rax),%eax
    1c3a:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1c3d:	72 5f                	jb     1c9e <malloc+0xcf>
      if (p->s.size == nunits)
    1c3f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c43:	8b 40 08             	mov    0x8(%rax),%eax
    1c46:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1c49:	75 10                	jne    1c5b <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
    1c4b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c4f:	48 8b 10             	mov    (%rax),%rdx
    1c52:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1c56:	48 89 10             	mov    %rdx,(%rax)
    1c59:	eb 2e                	jmp    1c89 <malloc+0xba>
      else {
        p->s.size -= nunits;
    1c5b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c5f:	8b 40 08             	mov    0x8(%rax),%eax
    1c62:	2b 45 ec             	sub    -0x14(%rbp),%eax
    1c65:	89 c2                	mov    %eax,%edx
    1c67:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c6b:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
    1c6e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c72:	8b 40 08             	mov    0x8(%rax),%eax
    1c75:	89 c0                	mov    %eax,%eax
    1c77:	48 c1 e0 04          	shl    $0x4,%rax
    1c7b:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
    1c7f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c83:	8b 55 ec             	mov    -0x14(%rbp),%edx
    1c86:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
    1c89:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1c8d:	48 89 05 9c 09 00 00 	mov    %rax,0x99c(%rip)        # 2630 <freep>
      return (void *)(p + 1);
    1c94:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1c98:	48 83 c0 10          	add    $0x10,%rax
    1c9c:	eb 41                	jmp    1cdf <malloc+0x110>
    }
    if (p == freep)
    1c9e:	48 8b 05 8b 09 00 00 	mov    0x98b(%rip),%rax        # 2630 <freep>
    1ca5:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    1ca9:	75 1c                	jne    1cc7 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    1cab:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1cae:	89 c7                	mov    %eax,%edi
    1cb0:	e8 b5 fe ff ff       	callq  1b6a <morecore>
    1cb5:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1cb9:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    1cbe:	75 07                	jne    1cc7 <malloc+0xf8>
        return 0;
    1cc0:	b8 00 00 00 00       	mov    $0x0,%eax
    1cc5:	eb 18                	jmp    1cdf <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1cc7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1ccb:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    1ccf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1cd3:	48 8b 00             	mov    (%rax),%rax
    1cd6:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    1cda:	e9 54 ff ff ff       	jmpq   1c33 <malloc+0x64>
    1cdf:	c9                   	leaveq 
    1ce0:	c3                   	retq   
