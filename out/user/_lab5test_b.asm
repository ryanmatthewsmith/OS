
out/user/_lab5test_b:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <get_progress>:
    };                                                                         \
  } while (0)

int state;

void get_progress(void) {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
       4:	48 83 ec 10          	sub    $0x10,%rsp
  int fd;
  fd = open("progress.txt", O_RDONLY);
       8:	be 00 00 00 00       	mov    $0x0,%esi
       d:	bf e0 10 00 00       	mov    $0x10e0,%edi
      12:	e8 c0 0d 00 00       	callq  dd7 <open>
      17:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0) {
      1a:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
      1e:	79 3f                	jns    5f <get_progress+0x5f>
    // nothing is created yet
    state = 5;
      20:	c7 05 d6 15 00 00 05 	movl   $0x5,0x15d6(%rip)        # 1600 <state>
      27:	00 00 00 
    fd = open("progress.txt", O_CREATE | O_RDWR);
      2a:	be 02 02 00 00       	mov    $0x202,%esi
      2f:	bf e0 10 00 00       	mov    $0x10e0,%edi
      34:	e8 9e 0d 00 00       	callq  dd7 <open>
      39:	89 45 fc             	mov    %eax,-0x4(%rbp)
    write(fd, &state, sizeof(int));
      3c:	8b 45 fc             	mov    -0x4(%rbp),%eax
      3f:	ba 04 00 00 00       	mov    $0x4,%edx
      44:	be 00 16 00 00       	mov    $0x1600,%esi
      49:	89 c7                	mov    %eax,%edi
      4b:	e8 67 0d 00 00       	callq  db7 <write>
    close(fd);
      50:	8b 45 fc             	mov    -0x4(%rbp),%eax
      53:	89 c7                	mov    %eax,%edi
      55:	e8 65 0d 00 00       	callq  dbf <close>
    state += (state / 10) + 1;
    fd = open("progress.txt", O_RDWR);
    write(fd, &state, sizeof(int));
    close(fd);
  }
}
      5a:	e9 d3 00 00 00       	jmpq   132 <get_progress+0x132>
    state = 5;
    fd = open("progress.txt", O_CREATE | O_RDWR);
    write(fd, &state, sizeof(int));
    close(fd);
  } else {
    read(fd, &state, sizeof(int));
      5f:	8b 45 fc             	mov    -0x4(%rbp),%eax
      62:	ba 04 00 00 00       	mov    $0x4,%edx
      67:	be 00 16 00 00       	mov    $0x1600,%esi
      6c:	89 c7                	mov    %eax,%edi
      6e:	e8 3c 0d 00 00       	callq  daf <read>
    close(fd);
      73:	8b 45 fc             	mov    -0x4(%rbp),%eax
      76:	89 c7                	mov    %eax,%edi
      78:	e8 42 0d 00 00       	callq  dbf <close>

    if (state > 1000)
      7d:	8b 05 7d 15 00 00    	mov    0x157d(%rip),%eax        # 1600 <state>
      83:	3d e8 03 00 00       	cmp    $0x3e8,%eax
      88:	7e 4c                	jle    d6 <get_progress+0xd6>
      error("too many steps before operating is complete");
      8a:	8b 05 f0 14 00 00    	mov    0x14f0(%rip),%eax        # 1580 <stdout>
      90:	ba 27 00 00 00       	mov    $0x27,%edx
      95:	be ed 10 00 00       	mov    $0x10ed,%esi
      9a:	89 c7                	mov    %eax,%edi
      9c:	b8 00 00 00 00       	mov    $0x0,%eax
      a1:	e8 4e 07 00 00       	callq  7f4 <printf>
      a6:	8b 05 d4 14 00 00    	mov    0x14d4(%rip),%eax        # 1580 <stdout>
      ac:	be 00 11 00 00       	mov    $0x1100,%esi
      b1:	89 c7                	mov    %eax,%edi
      b3:	b8 00 00 00 00       	mov    $0x0,%eax
      b8:	e8 37 07 00 00       	callq  7f4 <printf>
      bd:	8b 05 bd 14 00 00    	mov    0x14bd(%rip),%eax        # 1580 <stdout>
      c3:	be 2c 11 00 00       	mov    $0x112c,%esi
      c8:	89 c7                	mov    %eax,%edi
      ca:	b8 00 00 00 00       	mov    $0x0,%eax
      cf:	e8 20 07 00 00       	callq  7f4 <printf>
      d4:	eb fe                	jmp    d4 <get_progress+0xd4>

    state += (state / 10) + 1;
      d6:	8b 0d 24 15 00 00    	mov    0x1524(%rip),%ecx        # 1600 <state>
      dc:	ba 67 66 66 66       	mov    $0x66666667,%edx
      e1:	89 c8                	mov    %ecx,%eax
      e3:	f7 ea                	imul   %edx
      e5:	c1 fa 02             	sar    $0x2,%edx
      e8:	89 c8                	mov    %ecx,%eax
      ea:	c1 f8 1f             	sar    $0x1f,%eax
      ed:	29 c2                	sub    %eax,%edx
      ef:	89 d0                	mov    %edx,%eax
      f1:	8d 50 01             	lea    0x1(%rax),%edx
      f4:	8b 05 06 15 00 00    	mov    0x1506(%rip),%eax        # 1600 <state>
      fa:	01 d0                	add    %edx,%eax
      fc:	89 05 fe 14 00 00    	mov    %eax,0x14fe(%rip)        # 1600 <state>
    fd = open("progress.txt", O_RDWR);
     102:	be 02 00 00 00       	mov    $0x2,%esi
     107:	bf e0 10 00 00       	mov    $0x10e0,%edi
     10c:	e8 c6 0c 00 00       	callq  dd7 <open>
     111:	89 45 fc             	mov    %eax,-0x4(%rbp)
    write(fd, &state, sizeof(int));
     114:	8b 45 fc             	mov    -0x4(%rbp),%eax
     117:	ba 04 00 00 00       	mov    $0x4,%edx
     11c:	be 00 16 00 00       	mov    $0x1600,%esi
     121:	89 c7                	mov    %eax,%edi
     123:	e8 8f 0c 00 00       	callq  db7 <write>
    close(fd);
     128:	8b 45 fc             	mov    -0x4(%rbp),%eax
     12b:	89 c7                	mov    %eax,%edi
     12d:	e8 8d 0c 00 00       	callq  dbf <close>
  }
}
     132:	90                   	nop
     133:	c9                   	leaveq 
     134:	c3                   	retq   

0000000000000135 <check1>:

void check1(void) {
     135:	55                   	push   %rbp
     136:	48 89 e5             	mov    %rsp,%rbp
     139:	48 83 ec 20          	sub    $0x20,%rsp
  int fd;
  int i, j;
  struct stat st;

  fd = open("big.txt", O_RDONLY);
     13d:	be 00 00 00 00       	mov    $0x0,%esi
     142:	bf 2e 11 00 00       	mov    $0x112e,%edi
     147:	e8 8b 0c 00 00       	callq  dd7 <open>
     14c:	89 45 f4             	mov    %eax,-0xc(%rbp)
  if (fd < 0)
     14f:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     153:	0f 88 04 02 00 00    	js     35d <check1+0x228>
    return;
  fstat(fd, &st);
     159:	48 8d 55 e0          	lea    -0x20(%rbp),%rdx
     15d:	8b 45 f4             	mov    -0xc(%rbp),%eax
     160:	48 89 d6             	mov    %rdx,%rsi
     163:	89 c7                	mov    %eax,%edi
     165:	e8 85 0c 00 00       	callq  def <fstat>

  // check if it is a zero-size file
  if (st.size == 0)
     16a:	8b 45 ec             	mov    -0x14(%rbp),%eax
     16d:	85 c0                	test   %eax,%eax
     16f:	0f 84 eb 01 00 00    	je     360 <check1+0x22b>
    return;

  if (st.size % 512 != 0)
     175:	8b 45 ec             	mov    -0x14(%rbp),%eax
     178:	25 ff 01 00 00       	and    $0x1ff,%eax
     17d:	85 c0                	test   %eax,%eax
     17f:	74 4c                	je     1cd <check1+0x98>
    error("write is in-complete, file system not in consistent state!");
     181:	8b 05 f9 13 00 00    	mov    0x13f9(%rip),%eax        # 1580 <stdout>
     187:	ba 3f 00 00 00       	mov    $0x3f,%edx
     18c:	be ed 10 00 00       	mov    $0x10ed,%esi
     191:	89 c7                	mov    %eax,%edi
     193:	b8 00 00 00 00       	mov    $0x0,%eax
     198:	e8 57 06 00 00       	callq  7f4 <printf>
     19d:	8b 05 dd 13 00 00    	mov    0x13dd(%rip),%eax        # 1580 <stdout>
     1a3:	be 38 11 00 00       	mov    $0x1138,%esi
     1a8:	89 c7                	mov    %eax,%edi
     1aa:	b8 00 00 00 00       	mov    $0x0,%eax
     1af:	e8 40 06 00 00       	callq  7f4 <printf>
     1b4:	8b 05 c6 13 00 00    	mov    0x13c6(%rip),%eax        # 1580 <stdout>
     1ba:	be 2c 11 00 00       	mov    $0x112c,%esi
     1bf:	89 c7                	mov    %eax,%edi
     1c1:	b8 00 00 00 00       	mov    $0x0,%eax
     1c6:	e8 29 06 00 00       	callq  7f4 <printf>
     1cb:	eb fe                	jmp    1cb <check1+0x96>

  // check if the size of the file is correct
  int progress = st.size / 512;
     1cd:	8b 45 ec             	mov    -0x14(%rbp),%eax
     1d0:	c1 e8 09             	shr    $0x9,%eax
     1d3:	89 45 f0             	mov    %eax,-0x10(%rbp)
  if (progress > 3)
     1d6:	83 7d f0 03          	cmpl   $0x3,-0x10(%rbp)
     1da:	7e 4c                	jle    228 <check1+0xf3>
    error("write is incorrect, file system not in consistent state!");
     1dc:	8b 05 9e 13 00 00    	mov    0x139e(%rip),%eax        # 1580 <stdout>
     1e2:	ba 44 00 00 00       	mov    $0x44,%edx
     1e7:	be ed 10 00 00       	mov    $0x10ed,%esi
     1ec:	89 c7                	mov    %eax,%edi
     1ee:	b8 00 00 00 00       	mov    $0x0,%eax
     1f3:	e8 fc 05 00 00       	callq  7f4 <printf>
     1f8:	8b 05 82 13 00 00    	mov    0x1382(%rip),%eax        # 1580 <stdout>
     1fe:	be 78 11 00 00       	mov    $0x1178,%esi
     203:	89 c7                	mov    %eax,%edi
     205:	b8 00 00 00 00       	mov    $0x0,%eax
     20a:	e8 e5 05 00 00       	callq  7f4 <printf>
     20f:	8b 05 6b 13 00 00    	mov    0x136b(%rip),%eax        # 1580 <stdout>
     215:	be 2c 11 00 00       	mov    $0x112c,%esi
     21a:	89 c7                	mov    %eax,%edi
     21c:	b8 00 00 00 00       	mov    $0x0,%eax
     221:	e8 ce 05 00 00       	callq  7f4 <printf>
     226:	eb fe                	jmp    226 <check1+0xf1>

  memset(buf, 0, sizeof(buf));
     228:	ba 00 20 00 00       	mov    $0x2000,%edx
     22d:	be 00 00 00 00       	mov    $0x0,%esi
     232:	bf 20 16 00 00       	mov    $0x1620,%edi
     237:	e8 72 09 00 00       	callq  bae <memset>
  read(fd, buf, 3 * 512);
     23c:	8b 45 f4             	mov    -0xc(%rbp),%eax
     23f:	ba 00 06 00 00       	mov    $0x600,%edx
     244:	be 20 16 00 00       	mov    $0x1620,%esi
     249:	89 c7                	mov    %eax,%edi
     24b:	e8 5f 0b 00 00       	callq  daf <read>
  for (i = 0; i < progress; i++) {
     250:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     257:	e9 b2 00 00 00       	jmpq   30e <check1+0x1d9>
    for (j = 0; j < 512; j++) {
     25c:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     263:	e9 95 00 00 00       	jmpq   2fd <check1+0x1c8>
      if (buf[i * 512 + j] != 'a' + i)
     268:	8b 45 fc             	mov    -0x4(%rbp),%eax
     26b:	c1 e0 09             	shl    $0x9,%eax
     26e:	89 c2                	mov    %eax,%edx
     270:	8b 45 f8             	mov    -0x8(%rbp),%eax
     273:	01 d0                	add    %edx,%eax
     275:	48 98                	cltq   
     277:	0f b6 80 20 16 00 00 	movzbl 0x1620(%rax),%eax
     27e:	0f be c0             	movsbl %al,%eax
     281:	8b 55 fc             	mov    -0x4(%rbp),%edx
     284:	83 c2 61             	add    $0x61,%edx
     287:	39 d0                	cmp    %edx,%eax
     289:	74 6e                	je     2f9 <check1+0x1c4>
        error("file system not in consistent state!, i = %d, j = %d, content = "
     28b:	8b 05 ef 12 00 00    	mov    0x12ef(%rip),%eax        # 1580 <stdout>
     291:	ba 4d 00 00 00       	mov    $0x4d,%edx
     296:	be ed 10 00 00       	mov    $0x10ed,%esi
     29b:	89 c7                	mov    %eax,%edi
     29d:	b8 00 00 00 00       	mov    $0x0,%eax
     2a2:	e8 4d 05 00 00       	callq  7f4 <printf>
     2a7:	8b 45 fc             	mov    -0x4(%rbp),%eax
     2aa:	c1 e0 09             	shl    $0x9,%eax
     2ad:	89 c2                	mov    %eax,%edx
     2af:	8b 45 f8             	mov    -0x8(%rbp),%eax
     2b2:	01 d0                	add    %edx,%eax
     2b4:	48 98                	cltq   
     2b6:	0f b6 80 20 16 00 00 	movzbl 0x1620(%rax),%eax
     2bd:	0f be f0             	movsbl %al,%esi
     2c0:	8b 05 ba 12 00 00    	mov    0x12ba(%rip),%eax        # 1580 <stdout>
     2c6:	8b 4d f8             	mov    -0x8(%rbp),%ecx
     2c9:	8b 55 fc             	mov    -0x4(%rbp),%edx
     2cc:	41 89 f0             	mov    %esi,%r8d
     2cf:	be b8 11 00 00       	mov    $0x11b8,%esi
     2d4:	89 c7                	mov    %eax,%edi
     2d6:	b8 00 00 00 00       	mov    $0x0,%eax
     2db:	e8 14 05 00 00       	callq  7f4 <printf>
     2e0:	8b 05 9a 12 00 00    	mov    0x129a(%rip),%eax        # 1580 <stdout>
     2e6:	be 2c 11 00 00       	mov    $0x112c,%esi
     2eb:	89 c7                	mov    %eax,%edi
     2ed:	b8 00 00 00 00       	mov    $0x0,%eax
     2f2:	e8 fd 04 00 00       	callq  7f4 <printf>
     2f7:	eb fe                	jmp    2f7 <check1+0x1c2>
    error("write is incorrect, file system not in consistent state!");

  memset(buf, 0, sizeof(buf));
  read(fd, buf, 3 * 512);
  for (i = 0; i < progress; i++) {
    for (j = 0; j < 512; j++) {
     2f9:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     2fd:	81 7d f8 ff 01 00 00 	cmpl   $0x1ff,-0x8(%rbp)
     304:	0f 8e 5e ff ff ff    	jle    268 <check1+0x133>
  if (progress > 3)
    error("write is incorrect, file system not in consistent state!");

  memset(buf, 0, sizeof(buf));
  read(fd, buf, 3 * 512);
  for (i = 0; i < progress; i++) {
     30a:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     30e:	8b 45 fc             	mov    -0x4(%rbp),%eax
     311:	3b 45 f0             	cmp    -0x10(%rbp),%eax
     314:	0f 8c 42 ff ff ff    	jl     25c <check1+0x127>
        error("file system not in consistent state!, i = %d, j = %d, content = "
              "%d",
              i, j, buf[i * 512 + j]);
    }
  }
  close(fd);
     31a:	8b 45 f4             	mov    -0xc(%rbp),%eax
     31d:	89 c7                	mov    %eax,%edi
     31f:	e8 9b 0a 00 00       	callq  dbf <close>

  if (progress == 3) {
     324:	83 7d f0 03          	cmpl   $0x3,-0x10(%rbp)
     328:	75 37                	jne    361 <check1+0x22c>
    printf(stdout, "big.txt is completely written\n");
     32a:	8b 05 50 12 00 00    	mov    0x1250(%rip),%eax        # 1580 <stdout>
     330:	be 00 12 00 00       	mov    $0x1200,%esi
     335:	89 c7                	mov    %eax,%edi
     337:	b8 00 00 00 00       	mov    $0x0,%eax
     33c:	e8 b3 04 00 00       	callq  7f4 <printf>
    printf(stdout, "lab5test_b passed!\n");
     341:	8b 05 39 12 00 00    	mov    0x1239(%rip),%eax        # 1580 <stdout>
     347:	be 1f 12 00 00       	mov    $0x121f,%esi
     34c:	89 c7                	mov    %eax,%edi
     34e:	b8 00 00 00 00       	mov    $0x0,%eax
     353:	e8 9c 04 00 00       	callq  7f4 <printf>
    exit();
     358:	e8 3a 0a 00 00       	callq  d97 <exit>
  int i, j;
  struct stat st;

  fd = open("big.txt", O_RDONLY);
  if (fd < 0)
    return;
     35d:	90                   	nop
     35e:	eb 01                	jmp    361 <check1+0x22c>
  fstat(fd, &st);

  // check if it is a zero-size file
  if (st.size == 0)
    return;
     360:	90                   	nop
  if (progress == 3) {
    printf(stdout, "big.txt is completely written\n");
    printf(stdout, "lab5test_b passed!\n");
    exit();
  }
}
     361:	c9                   	leaveq 
     362:	c3                   	retq   

0000000000000363 <check2>:

void check2(void) {
     363:	55                   	push   %rbp
     364:	48 89 e5             	mov    %rsp,%rbp
     367:	48 83 ec 20          	sub    $0x20,%rsp
  int fd;
  int i, j;
  struct stat st;

  fd = open("big.txt", O_RDONLY);
     36b:	be 00 00 00 00       	mov    $0x0,%esi
     370:	bf 2e 11 00 00       	mov    $0x112e,%edi
     375:	e8 5d 0a 00 00       	callq  dd7 <open>
     37a:	89 45 f4             	mov    %eax,-0xc(%rbp)
  if (fd < 0)
     37d:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     381:	79 4c                	jns    3cf <check2+0x6c>
    error("file is not generated");
     383:	8b 05 f7 11 00 00    	mov    0x11f7(%rip),%eax        # 1580 <stdout>
     389:	ba 60 00 00 00       	mov    $0x60,%edx
     38e:	be ed 10 00 00       	mov    $0x10ed,%esi
     393:	89 c7                	mov    %eax,%edi
     395:	b8 00 00 00 00       	mov    $0x0,%eax
     39a:	e8 55 04 00 00       	callq  7f4 <printf>
     39f:	8b 05 db 11 00 00    	mov    0x11db(%rip),%eax        # 1580 <stdout>
     3a5:	be 33 12 00 00       	mov    $0x1233,%esi
     3aa:	89 c7                	mov    %eax,%edi
     3ac:	b8 00 00 00 00       	mov    $0x0,%eax
     3b1:	e8 3e 04 00 00       	callq  7f4 <printf>
     3b6:	8b 05 c4 11 00 00    	mov    0x11c4(%rip),%eax        # 1580 <stdout>
     3bc:	be 2c 11 00 00       	mov    $0x112c,%esi
     3c1:	89 c7                	mov    %eax,%edi
     3c3:	b8 00 00 00 00       	mov    $0x0,%eax
     3c8:	e8 27 04 00 00       	callq  7f4 <printf>
     3cd:	eb fe                	jmp    3cd <check2+0x6a>

  fstat(fd, &st);
     3cf:	48 8d 55 e4          	lea    -0x1c(%rbp),%rdx
     3d3:	8b 45 f4             	mov    -0xc(%rbp),%eax
     3d6:	48 89 d6             	mov    %rdx,%rsi
     3d9:	89 c7                	mov    %eax,%edi
     3db:	e8 0f 0a 00 00       	callq  def <fstat>

  // check if the size of the file is correct
  if (st.size != 512 * 3)
     3e0:	8b 45 f0             	mov    -0x10(%rbp),%eax
     3e3:	3d 00 06 00 00       	cmp    $0x600,%eax
     3e8:	74 4c                	je     436 <check2+0xd3>
    error("write is in-complete, file system not in consistent state!");
     3ea:	8b 05 90 11 00 00    	mov    0x1190(%rip),%eax        # 1580 <stdout>
     3f0:	ba 66 00 00 00       	mov    $0x66,%edx
     3f5:	be ed 10 00 00       	mov    $0x10ed,%esi
     3fa:	89 c7                	mov    %eax,%edi
     3fc:	b8 00 00 00 00       	mov    $0x0,%eax
     401:	e8 ee 03 00 00       	callq  7f4 <printf>
     406:	8b 05 74 11 00 00    	mov    0x1174(%rip),%eax        # 1580 <stdout>
     40c:	be 38 11 00 00       	mov    $0x1138,%esi
     411:	89 c7                	mov    %eax,%edi
     413:	b8 00 00 00 00       	mov    $0x0,%eax
     418:	e8 d7 03 00 00       	callq  7f4 <printf>
     41d:	8b 05 5d 11 00 00    	mov    0x115d(%rip),%eax        # 1580 <stdout>
     423:	be 2c 11 00 00       	mov    $0x112c,%esi
     428:	89 c7                	mov    %eax,%edi
     42a:	b8 00 00 00 00       	mov    $0x0,%eax
     42f:	e8 c0 03 00 00       	callq  7f4 <printf>
     434:	eb fe                	jmp    434 <check2+0xd1>
  memset(buf, 0, sizeof(buf));
     436:	ba 00 20 00 00       	mov    $0x2000,%edx
     43b:	be 00 00 00 00       	mov    $0x0,%esi
     440:	bf 20 16 00 00       	mov    $0x1620,%edi
     445:	e8 64 07 00 00       	callq  bae <memset>
  read(fd, buf, 3 * 512);
     44a:	8b 45 f4             	mov    -0xc(%rbp),%eax
     44d:	ba 00 06 00 00       	mov    $0x600,%edx
     452:	be 20 16 00 00       	mov    $0x1620,%esi
     457:	89 c7                	mov    %eax,%edi
     459:	e8 51 09 00 00       	callq  daf <read>
  for (i = 0; i < 3; i++) {
     45e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     465:	e9 89 00 00 00       	jmpq   4f3 <check2+0x190>
    for (j = 0; j < 512; j++) {
     46a:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     471:	eb 73                	jmp    4e6 <check2+0x183>
      if (buf[i * 512 + j] != 'a' + i)
     473:	8b 45 fc             	mov    -0x4(%rbp),%eax
     476:	c1 e0 09             	shl    $0x9,%eax
     479:	89 c2                	mov    %eax,%edx
     47b:	8b 45 f8             	mov    -0x8(%rbp),%eax
     47e:	01 d0                	add    %edx,%eax
     480:	48 98                	cltq   
     482:	0f b6 80 20 16 00 00 	movzbl 0x1620(%rax),%eax
     489:	0f be c0             	movsbl %al,%eax
     48c:	8b 55 fc             	mov    -0x4(%rbp),%edx
     48f:	83 c2 61             	add    $0x61,%edx
     492:	39 d0                	cmp    %edx,%eax
     494:	74 4c                	je     4e2 <check2+0x17f>
        error("file system not in consistent state!");
     496:	8b 05 e4 10 00 00    	mov    0x10e4(%rip),%eax        # 1580 <stdout>
     49c:	ba 6c 00 00 00       	mov    $0x6c,%edx
     4a1:	be ed 10 00 00       	mov    $0x10ed,%esi
     4a6:	89 c7                	mov    %eax,%edi
     4a8:	b8 00 00 00 00       	mov    $0x0,%eax
     4ad:	e8 42 03 00 00       	callq  7f4 <printf>
     4b2:	8b 05 c8 10 00 00    	mov    0x10c8(%rip),%eax        # 1580 <stdout>
     4b8:	be 50 12 00 00       	mov    $0x1250,%esi
     4bd:	89 c7                	mov    %eax,%edi
     4bf:	b8 00 00 00 00       	mov    $0x0,%eax
     4c4:	e8 2b 03 00 00       	callq  7f4 <printf>
     4c9:	8b 05 b1 10 00 00    	mov    0x10b1(%rip),%eax        # 1580 <stdout>
     4cf:	be 2c 11 00 00       	mov    $0x112c,%esi
     4d4:	89 c7                	mov    %eax,%edi
     4d6:	b8 00 00 00 00       	mov    $0x0,%eax
     4db:	e8 14 03 00 00       	callq  7f4 <printf>
     4e0:	eb fe                	jmp    4e0 <check2+0x17d>
  if (st.size != 512 * 3)
    error("write is in-complete, file system not in consistent state!");
  memset(buf, 0, sizeof(buf));
  read(fd, buf, 3 * 512);
  for (i = 0; i < 3; i++) {
    for (j = 0; j < 512; j++) {
     4e2:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     4e6:	81 7d f8 ff 01 00 00 	cmpl   $0x1ff,-0x8(%rbp)
     4ed:	7e 84                	jle    473 <check2+0x110>
  // check if the size of the file is correct
  if (st.size != 512 * 3)
    error("write is in-complete, file system not in consistent state!");
  memset(buf, 0, sizeof(buf));
  read(fd, buf, 3 * 512);
  for (i = 0; i < 3; i++) {
     4ef:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     4f3:	83 7d fc 02          	cmpl   $0x2,-0x4(%rbp)
     4f7:	0f 8e 6d ff ff ff    	jle    46a <check2+0x107>
    for (j = 0; j < 512; j++) {
      if (buf[i * 512 + j] != 'a' + i)
        error("file system not in consistent state!");
    }
  }
  close(fd);
     4fd:	8b 45 f4             	mov    -0xc(%rbp),%eax
     500:	89 c7                	mov    %eax,%edi
     502:	e8 b8 08 00 00       	callq  dbf <close>
  return;
     507:	90                   	nop
}
     508:	c9                   	leaveq 
     509:	c3                   	retq   

000000000000050a <crashsafe>:

void crashsafe(int steps) {
     50a:	55                   	push   %rbp
     50b:	48 89 e5             	mov    %rsp,%rbp
     50e:	48 83 ec 20          	sub    $0x20,%rsp
     512:	89 7d ec             	mov    %edi,-0x14(%rbp)
  int fd;
  crashn(steps);
     515:	8b 45 ec             	mov    -0x14(%rbp),%eax
     518:	89 c7                	mov    %eax,%edi
     51a:	e8 20 09 00 00       	callq  e3f <crashn>
  printf(stdout, "crash after %d steps\n", steps);
     51f:	8b 05 5b 10 00 00    	mov    0x105b(%rip),%eax        # 1580 <stdout>
     525:	8b 55 ec             	mov    -0x14(%rbp),%edx
     528:	be 75 12 00 00       	mov    $0x1275,%esi
     52d:	89 c7                	mov    %eax,%edi
     52f:	b8 00 00 00 00       	mov    $0x0,%eax
     534:	e8 bb 02 00 00       	callq  7f4 <printf>
  fd = open("big.txt", O_CREATE | O_RDWR);
     539:	be 02 02 00 00       	mov    $0x202,%esi
     53e:	bf 2e 11 00 00       	mov    $0x112e,%edi
     543:	e8 8f 08 00 00       	callq  dd7 <open>
     548:	89 45 fc             	mov    %eax,-0x4(%rbp)
  memset(buf, 'a', 512);
     54b:	ba 00 02 00 00       	mov    $0x200,%edx
     550:	be 61 00 00 00       	mov    $0x61,%esi
     555:	bf 20 16 00 00       	mov    $0x1620,%edi
     55a:	e8 4f 06 00 00       	callq  bae <memset>
  memset(buf + 512 * 1, 'b', 512);
     55f:	b8 20 18 00 00       	mov    $0x1820,%eax
     564:	ba 00 02 00 00       	mov    $0x200,%edx
     569:	be 62 00 00 00       	mov    $0x62,%esi
     56e:	48 89 c7             	mov    %rax,%rdi
     571:	e8 38 06 00 00       	callq  bae <memset>
  memset(buf + 512 * 2, 'c', 512);
     576:	b8 20 1a 00 00       	mov    $0x1a20,%eax
     57b:	ba 00 02 00 00       	mov    $0x200,%edx
     580:	be 63 00 00 00       	mov    $0x63,%esi
     585:	48 89 c7             	mov    %rax,%rdi
     588:	e8 21 06 00 00       	callq  bae <memset>
  write(fd, buf, 512);
     58d:	8b 45 fc             	mov    -0x4(%rbp),%eax
     590:	ba 00 02 00 00       	mov    $0x200,%edx
     595:	be 20 16 00 00       	mov    $0x1620,%esi
     59a:	89 c7                	mov    %eax,%edi
     59c:	e8 16 08 00 00       	callq  db7 <write>
  write(fd, buf + 512, 512);
     5a1:	b9 20 18 00 00       	mov    $0x1820,%ecx
     5a6:	8b 45 fc             	mov    -0x4(%rbp),%eax
     5a9:	ba 00 02 00 00       	mov    $0x200,%edx
     5ae:	48 89 ce             	mov    %rcx,%rsi
     5b1:	89 c7                	mov    %eax,%edi
     5b3:	e8 ff 07 00 00       	callq  db7 <write>
  write(fd, buf + 1024, 512);
     5b8:	b9 20 1a 00 00       	mov    $0x1a20,%ecx
     5bd:	8b 45 fc             	mov    -0x4(%rbp),%eax
     5c0:	ba 00 02 00 00       	mov    $0x200,%edx
     5c5:	48 89 ce             	mov    %rcx,%rsi
     5c8:	89 c7                	mov    %eax,%edi
     5ca:	e8 e8 07 00 00       	callq  db7 <write>
  close(fd);
     5cf:	8b 45 fc             	mov    -0x4(%rbp),%eax
     5d2:	89 c7                	mov    %eax,%edi
     5d4:	e8 e6 07 00 00       	callq  dbf <close>
}
     5d9:	90                   	nop
     5da:	c9                   	leaveq 
     5db:	c3                   	retq   

00000000000005dc <main>:

int main(int argc, char *argv[]) {
     5dc:	55                   	push   %rbp
     5dd:	48 89 e5             	mov    %rsp,%rbp
     5e0:	48 83 ec 10          	sub    $0x10,%rsp
     5e4:	89 7d fc             	mov    %edi,-0x4(%rbp)
     5e7:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  printf(stdout, "lab5test_b starting\n");
     5eb:	8b 05 8f 0f 00 00    	mov    0xf8f(%rip),%eax        # 1580 <stdout>
     5f1:	be 8b 12 00 00       	mov    $0x128b,%esi
     5f6:	89 c7                	mov    %eax,%edi
     5f8:	b8 00 00 00 00       	mov    $0x0,%eax
     5fd:	e8 f2 01 00 00       	callq  7f4 <printf>
  check1();
     602:	e8 2e fb ff ff       	callq  135 <check1>
  get_progress();
     607:	e8 f4 f9 ff ff       	callq  0 <get_progress>
  crashsafe(state);
     60c:	8b 05 ee 0f 00 00    	mov    0xfee(%rip),%eax        # 1600 <state>
     612:	89 c7                	mov    %eax,%edi
     614:	e8 f1 fe ff ff       	callq  50a <crashsafe>
  check2();
     619:	e8 45 fd ff ff       	callq  363 <check2>
  printf(stdout, "lab5test_b passed!\n");
     61e:	8b 05 5c 0f 00 00    	mov    0xf5c(%rip),%eax        # 1580 <stdout>
     624:	be 1f 12 00 00       	mov    $0x121f,%esi
     629:	89 c7                	mov    %eax,%edi
     62b:	b8 00 00 00 00       	mov    $0x0,%eax
     630:	e8 bf 01 00 00       	callq  7f4 <printf>
  exit();
     635:	e8 5d 07 00 00       	callq  d97 <exit>

000000000000063a <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
     63a:	55                   	push   %rbp
     63b:	48 89 e5             	mov    %rsp,%rbp
     63e:	48 83 ec 10          	sub    $0x10,%rsp
     642:	89 7d fc             	mov    %edi,-0x4(%rbp)
     645:	89 f0                	mov    %esi,%eax
     647:	88 45 f8             	mov    %al,-0x8(%rbp)
     64a:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
     64e:	8b 45 fc             	mov    -0x4(%rbp),%eax
     651:	ba 01 00 00 00       	mov    $0x1,%edx
     656:	48 89 ce             	mov    %rcx,%rsi
     659:	89 c7                	mov    %eax,%edi
     65b:	e8 57 07 00 00       	callq  db7 <write>
     660:	90                   	nop
     661:	c9                   	leaveq 
     662:	c3                   	retq   

0000000000000663 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
     663:	55                   	push   %rbp
     664:	48 89 e5             	mov    %rsp,%rbp
     667:	48 83 ec 40          	sub    $0x40,%rsp
     66b:	89 7d cc             	mov    %edi,-0x34(%rbp)
     66e:	89 75 c8             	mov    %esi,-0x38(%rbp)
     671:	89 55 c4             	mov    %edx,-0x3c(%rbp)
     674:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
     677:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     67b:	74 1f                	je     69c <printint64+0x39>
     67d:	8b 45 c8             	mov    -0x38(%rbp),%eax
     680:	c1 e8 1f             	shr    $0x1f,%eax
     683:	0f b6 c0             	movzbl %al,%eax
     686:	89 45 c0             	mov    %eax,-0x40(%rbp)
     689:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     68d:	74 0d                	je     69c <printint64+0x39>
    x = -xx;
     68f:	8b 45 c8             	mov    -0x38(%rbp),%eax
     692:	f7 d8                	neg    %eax
     694:	48 98                	cltq   
     696:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     69a:	eb 09                	jmp    6a5 <printint64+0x42>
  else
    x = xx;
     69c:	8b 45 c8             	mov    -0x38(%rbp),%eax
     69f:	48 98                	cltq   
     6a1:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
     6a5:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     6ac:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     6af:	8d 41 01             	lea    0x1(%rcx),%eax
     6b2:	89 45 fc             	mov    %eax,-0x4(%rbp)
     6b5:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     6b8:	48 63 f0             	movslq %eax,%rsi
     6bb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     6bf:	ba 00 00 00 00       	mov    $0x0,%edx
     6c4:	48 f7 f6             	div    %rsi
     6c7:	48 89 d0             	mov    %rdx,%rax
     6ca:	0f b6 90 90 15 00 00 	movzbl 0x1590(%rax),%edx
     6d1:	48 63 c1             	movslq %ecx,%rax
     6d4:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
     6d8:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     6db:	48 63 f8             	movslq %eax,%rdi
     6de:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     6e2:	ba 00 00 00 00       	mov    $0x0,%edx
     6e7:	48 f7 f7             	div    %rdi
     6ea:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     6ee:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     6f3:	75 b7                	jne    6ac <printint64+0x49>

  if (sgn)
     6f5:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     6f9:	74 2b                	je     726 <printint64+0xc3>
    buf[i++] = '-';
     6fb:	8b 45 fc             	mov    -0x4(%rbp),%eax
     6fe:	8d 50 01             	lea    0x1(%rax),%edx
     701:	89 55 fc             	mov    %edx,-0x4(%rbp)
     704:	48 98                	cltq   
     706:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
     70b:	eb 19                	jmp    726 <printint64+0xc3>
    putc(fd, buf[i]);
     70d:	8b 45 fc             	mov    -0x4(%rbp),%eax
     710:	48 98                	cltq   
     712:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
     717:	0f be d0             	movsbl %al,%edx
     71a:	8b 45 cc             	mov    -0x34(%rbp),%eax
     71d:	89 d6                	mov    %edx,%esi
     71f:	89 c7                	mov    %eax,%edi
     721:	e8 14 ff ff ff       	callq  63a <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
     726:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     72a:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     72e:	79 dd                	jns    70d <printint64+0xaa>
    putc(fd, buf[i]);
}
     730:	90                   	nop
     731:	c9                   	leaveq 
     732:	c3                   	retq   

0000000000000733 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
     733:	55                   	push   %rbp
     734:	48 89 e5             	mov    %rsp,%rbp
     737:	48 83 ec 30          	sub    $0x30,%rsp
     73b:	89 7d dc             	mov    %edi,-0x24(%rbp)
     73e:	89 75 d8             	mov    %esi,-0x28(%rbp)
     741:	89 55 d4             	mov    %edx,-0x2c(%rbp)
     744:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
     747:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
     74e:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
     752:	74 17                	je     76b <printint+0x38>
     754:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
     758:	79 11                	jns    76b <printint+0x38>
    neg = 1;
     75a:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
     761:	8b 45 d8             	mov    -0x28(%rbp),%eax
     764:	f7 d8                	neg    %eax
     766:	89 45 f4             	mov    %eax,-0xc(%rbp)
     769:	eb 06                	jmp    771 <printint+0x3e>
  } else {
    x = xx;
     76b:	8b 45 d8             	mov    -0x28(%rbp),%eax
     76e:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
     771:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     778:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     77b:	8d 41 01             	lea    0x1(%rcx),%eax
     77e:	89 45 fc             	mov    %eax,-0x4(%rbp)
     781:	8b 75 d4             	mov    -0x2c(%rbp),%esi
     784:	8b 45 f4             	mov    -0xc(%rbp),%eax
     787:	ba 00 00 00 00       	mov    $0x0,%edx
     78c:	f7 f6                	div    %esi
     78e:	89 d0                	mov    %edx,%eax
     790:	89 c0                	mov    %eax,%eax
     792:	0f b6 90 b0 15 00 00 	movzbl 0x15b0(%rax),%edx
     799:	48 63 c1             	movslq %ecx,%rax
     79c:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
     7a0:	8b 7d d4             	mov    -0x2c(%rbp),%edi
     7a3:	8b 45 f4             	mov    -0xc(%rbp),%eax
     7a6:	ba 00 00 00 00       	mov    $0x0,%edx
     7ab:	f7 f7                	div    %edi
     7ad:	89 45 f4             	mov    %eax,-0xc(%rbp)
     7b0:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     7b4:	75 c2                	jne    778 <printint+0x45>
  if (neg)
     7b6:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     7ba:	74 2b                	je     7e7 <printint+0xb4>
    buf[i++] = '-';
     7bc:	8b 45 fc             	mov    -0x4(%rbp),%eax
     7bf:	8d 50 01             	lea    0x1(%rax),%edx
     7c2:	89 55 fc             	mov    %edx,-0x4(%rbp)
     7c5:	48 98                	cltq   
     7c7:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
     7cc:	eb 19                	jmp    7e7 <printint+0xb4>
    putc(fd, buf[i]);
     7ce:	8b 45 fc             	mov    -0x4(%rbp),%eax
     7d1:	48 98                	cltq   
     7d3:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
     7d8:	0f be d0             	movsbl %al,%edx
     7db:	8b 45 dc             	mov    -0x24(%rbp),%eax
     7de:	89 d6                	mov    %edx,%esi
     7e0:	89 c7                	mov    %eax,%edi
     7e2:	e8 53 fe ff ff       	callq  63a <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
     7e7:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     7eb:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     7ef:	79 dd                	jns    7ce <printint+0x9b>
    putc(fd, buf[i]);
}
     7f1:	90                   	nop
     7f2:	c9                   	leaveq 
     7f3:	c3                   	retq   

00000000000007f4 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
     7f4:	55                   	push   %rbp
     7f5:	48 89 e5             	mov    %rsp,%rbp
     7f8:	48 83 ec 70          	sub    $0x70,%rsp
     7fc:	89 7d 9c             	mov    %edi,-0x64(%rbp)
     7ff:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
     803:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
     807:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
     80b:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
     80f:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
     813:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
     81a:	48 8d 45 10          	lea    0x10(%rbp),%rax
     81e:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
     822:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
     826:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
     82a:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
     831:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
     838:	e9 68 02 00 00       	jmpq   aa5 <printf+0x2b1>
    c = fmt[i] & 0xff;
     83d:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     840:	48 63 d0             	movslq %eax,%rdx
     843:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     847:	48 01 d0             	add    %rdx,%rax
     84a:	0f b6 00             	movzbl (%rax),%eax
     84d:	0f be c0             	movsbl %al,%eax
     850:	25 ff 00 00 00       	and    $0xff,%eax
     855:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
     858:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     85c:	75 30                	jne    88e <printf+0x9a>
      if (c == '%') {
     85e:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     862:	75 13                	jne    877 <printf+0x83>
        state = '%';
     864:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
     86b:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
     872:	e9 2a 02 00 00       	jmpq   aa1 <printf+0x2ad>
      } else {
        putc(fd, c);
     877:	8b 45 b8             	mov    -0x48(%rbp),%eax
     87a:	0f be d0             	movsbl %al,%edx
     87d:	8b 45 9c             	mov    -0x64(%rbp),%eax
     880:	89 d6                	mov    %edx,%esi
     882:	89 c7                	mov    %eax,%edi
     884:	e8 b1 fd ff ff       	callq  63a <putc>
     889:	e9 13 02 00 00       	jmpq   aa1 <printf+0x2ad>
      }
    } else if (state == '%') {
     88e:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
     892:	0f 85 09 02 00 00    	jne    aa1 <printf+0x2ad>
      if (c == 'l') {
     898:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
     89c:	75 0c                	jne    8aa <printf+0xb6>
        lflag = 1;
     89e:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
     8a5:	e9 f7 01 00 00       	jmpq   aa1 <printf+0x2ad>
      } else if (c == 'd') {
     8aa:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
     8ae:	0f 85 95 00 00 00    	jne    949 <printf+0x155>
        if (lflag == 1)
     8b4:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
     8b8:	75 49                	jne    903 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
     8ba:	8b 45 a0             	mov    -0x60(%rbp),%eax
     8bd:	83 f8 30             	cmp    $0x30,%eax
     8c0:	73 17                	jae    8d9 <printf+0xe5>
     8c2:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     8c6:	8b 55 a0             	mov    -0x60(%rbp),%edx
     8c9:	89 d2                	mov    %edx,%edx
     8cb:	48 01 d0             	add    %rdx,%rax
     8ce:	8b 55 a0             	mov    -0x60(%rbp),%edx
     8d1:	83 c2 08             	add    $0x8,%edx
     8d4:	89 55 a0             	mov    %edx,-0x60(%rbp)
     8d7:	eb 0c                	jmp    8e5 <printf+0xf1>
     8d9:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     8dd:	48 8d 50 08          	lea    0x8(%rax),%rdx
     8e1:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     8e5:	48 8b 00             	mov    (%rax),%rax
     8e8:	89 c6                	mov    %eax,%esi
     8ea:	8b 45 9c             	mov    -0x64(%rbp),%eax
     8ed:	b9 01 00 00 00       	mov    $0x1,%ecx
     8f2:	ba 0a 00 00 00       	mov    $0xa,%edx
     8f7:	89 c7                	mov    %eax,%edi
     8f9:	e8 65 fd ff ff       	callq  663 <printint64>
     8fe:	e9 97 01 00 00       	jmpq   a9a <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
     903:	8b 45 a0             	mov    -0x60(%rbp),%eax
     906:	83 f8 30             	cmp    $0x30,%eax
     909:	73 17                	jae    922 <printf+0x12e>
     90b:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     90f:	8b 55 a0             	mov    -0x60(%rbp),%edx
     912:	89 d2                	mov    %edx,%edx
     914:	48 01 d0             	add    %rdx,%rax
     917:	8b 55 a0             	mov    -0x60(%rbp),%edx
     91a:	83 c2 08             	add    $0x8,%edx
     91d:	89 55 a0             	mov    %edx,-0x60(%rbp)
     920:	eb 0c                	jmp    92e <printf+0x13a>
     922:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     926:	48 8d 50 08          	lea    0x8(%rax),%rdx
     92a:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     92e:	8b 30                	mov    (%rax),%esi
     930:	8b 45 9c             	mov    -0x64(%rbp),%eax
     933:	b9 01 00 00 00       	mov    $0x1,%ecx
     938:	ba 0a 00 00 00       	mov    $0xa,%edx
     93d:	89 c7                	mov    %eax,%edi
     93f:	e8 ef fd ff ff       	callq  733 <printint>
     944:	e9 51 01 00 00       	jmpq   a9a <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
     949:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
     94d:	74 0a                	je     959 <printf+0x165>
     94f:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
     953:	0f 85 95 00 00 00    	jne    9ee <printf+0x1fa>
        if (lflag == 1)
     959:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
     95d:	75 49                	jne    9a8 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
     95f:	8b 45 a0             	mov    -0x60(%rbp),%eax
     962:	83 f8 30             	cmp    $0x30,%eax
     965:	73 17                	jae    97e <printf+0x18a>
     967:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     96b:	8b 55 a0             	mov    -0x60(%rbp),%edx
     96e:	89 d2                	mov    %edx,%edx
     970:	48 01 d0             	add    %rdx,%rax
     973:	8b 55 a0             	mov    -0x60(%rbp),%edx
     976:	83 c2 08             	add    $0x8,%edx
     979:	89 55 a0             	mov    %edx,-0x60(%rbp)
     97c:	eb 0c                	jmp    98a <printf+0x196>
     97e:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     982:	48 8d 50 08          	lea    0x8(%rax),%rdx
     986:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     98a:	48 8b 00             	mov    (%rax),%rax
     98d:	89 c6                	mov    %eax,%esi
     98f:	8b 45 9c             	mov    -0x64(%rbp),%eax
     992:	b9 00 00 00 00       	mov    $0x0,%ecx
     997:	ba 10 00 00 00       	mov    $0x10,%edx
     99c:	89 c7                	mov    %eax,%edi
     99e:	e8 c0 fc ff ff       	callq  663 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
     9a3:	e9 f2 00 00 00       	jmpq   a9a <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
     9a8:	8b 45 a0             	mov    -0x60(%rbp),%eax
     9ab:	83 f8 30             	cmp    $0x30,%eax
     9ae:	73 17                	jae    9c7 <printf+0x1d3>
     9b0:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     9b4:	8b 55 a0             	mov    -0x60(%rbp),%edx
     9b7:	89 d2                	mov    %edx,%edx
     9b9:	48 01 d0             	add    %rdx,%rax
     9bc:	8b 55 a0             	mov    -0x60(%rbp),%edx
     9bf:	83 c2 08             	add    $0x8,%edx
     9c2:	89 55 a0             	mov    %edx,-0x60(%rbp)
     9c5:	eb 0c                	jmp    9d3 <printf+0x1df>
     9c7:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     9cb:	48 8d 50 08          	lea    0x8(%rax),%rdx
     9cf:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     9d3:	8b 30                	mov    (%rax),%esi
     9d5:	8b 45 9c             	mov    -0x64(%rbp),%eax
     9d8:	b9 00 00 00 00       	mov    $0x0,%ecx
     9dd:	ba 10 00 00 00       	mov    $0x10,%edx
     9e2:	89 c7                	mov    %eax,%edi
     9e4:	e8 4a fd ff ff       	callq  733 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
     9e9:	e9 ac 00 00 00       	jmpq   a9a <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
     9ee:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
     9f2:	75 6b                	jne    a5f <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
     9f4:	8b 45 a0             	mov    -0x60(%rbp),%eax
     9f7:	83 f8 30             	cmp    $0x30,%eax
     9fa:	73 17                	jae    a13 <printf+0x21f>
     9fc:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     a00:	8b 55 a0             	mov    -0x60(%rbp),%edx
     a03:	89 d2                	mov    %edx,%edx
     a05:	48 01 d0             	add    %rdx,%rax
     a08:	8b 55 a0             	mov    -0x60(%rbp),%edx
     a0b:	83 c2 08             	add    $0x8,%edx
     a0e:	89 55 a0             	mov    %edx,-0x60(%rbp)
     a11:	eb 0c                	jmp    a1f <printf+0x22b>
     a13:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     a17:	48 8d 50 08          	lea    0x8(%rax),%rdx
     a1b:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     a1f:	48 8b 00             	mov    (%rax),%rax
     a22:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
     a26:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
     a2b:	75 25                	jne    a52 <printf+0x25e>
          s = "(null)";
     a2d:	48 c7 45 c8 a0 12 00 	movq   $0x12a0,-0x38(%rbp)
     a34:	00 
        for (; *s; s++)
     a35:	eb 1b                	jmp    a52 <printf+0x25e>
          putc(fd, *s);
     a37:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     a3b:	0f b6 00             	movzbl (%rax),%eax
     a3e:	0f be d0             	movsbl %al,%edx
     a41:	8b 45 9c             	mov    -0x64(%rbp),%eax
     a44:	89 d6                	mov    %edx,%esi
     a46:	89 c7                	mov    %eax,%edi
     a48:	e8 ed fb ff ff       	callq  63a <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
     a4d:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
     a52:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     a56:	0f b6 00             	movzbl (%rax),%eax
     a59:	84 c0                	test   %al,%al
     a5b:	75 da                	jne    a37 <printf+0x243>
     a5d:	eb 3b                	jmp    a9a <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
     a5f:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     a63:	75 14                	jne    a79 <printf+0x285>
        putc(fd, c);
     a65:	8b 45 b8             	mov    -0x48(%rbp),%eax
     a68:	0f be d0             	movsbl %al,%edx
     a6b:	8b 45 9c             	mov    -0x64(%rbp),%eax
     a6e:	89 d6                	mov    %edx,%esi
     a70:	89 c7                	mov    %eax,%edi
     a72:	e8 c3 fb ff ff       	callq  63a <putc>
     a77:	eb 21                	jmp    a9a <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
     a79:	8b 45 9c             	mov    -0x64(%rbp),%eax
     a7c:	be 25 00 00 00       	mov    $0x25,%esi
     a81:	89 c7                	mov    %eax,%edi
     a83:	e8 b2 fb ff ff       	callq  63a <putc>
        putc(fd, c);
     a88:	8b 45 b8             	mov    -0x48(%rbp),%eax
     a8b:	0f be d0             	movsbl %al,%edx
     a8e:	8b 45 9c             	mov    -0x64(%rbp),%eax
     a91:	89 d6                	mov    %edx,%esi
     a93:	89 c7                	mov    %eax,%edi
     a95:	e8 a0 fb ff ff       	callq  63a <putc>
      }
      state = 0;
     a9a:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
     aa1:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
     aa5:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     aa8:	48 63 d0             	movslq %eax,%rdx
     aab:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     aaf:	48 01 d0             	add    %rdx,%rax
     ab2:	0f b6 00             	movzbl (%rax),%eax
     ab5:	84 c0                	test   %al,%al
     ab7:	0f 85 80 fd ff ff    	jne    83d <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
     abd:	90                   	nop
     abe:	c9                   	leaveq 
     abf:	c3                   	retq   

0000000000000ac0 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
     ac0:	55                   	push   %rbp
     ac1:	48 89 e5             	mov    %rsp,%rbp
     ac4:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     ac8:	89 75 f4             	mov    %esi,-0xc(%rbp)
     acb:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
     ace:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
     ad2:	8b 55 f0             	mov    -0x10(%rbp),%edx
     ad5:	8b 45 f4             	mov    -0xc(%rbp),%eax
     ad8:	48 89 ce             	mov    %rcx,%rsi
     adb:	48 89 f7             	mov    %rsi,%rdi
     ade:	89 d1                	mov    %edx,%ecx
     ae0:	fc                   	cld    
     ae1:	f3 aa                	rep stos %al,%es:(%rdi)
     ae3:	89 ca                	mov    %ecx,%edx
     ae5:	48 89 fe             	mov    %rdi,%rsi
     ae8:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
     aec:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
     aef:	90                   	nop
     af0:	5d                   	pop    %rbp
     af1:	c3                   	retq   

0000000000000af2 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
     af2:	55                   	push   %rbp
     af3:	48 89 e5             	mov    %rsp,%rbp
     af6:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     afa:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
     afe:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     b02:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
     b06:	90                   	nop
     b07:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     b0b:	48 8d 50 01          	lea    0x1(%rax),%rdx
     b0f:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
     b13:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     b17:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
     b1b:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
     b1f:	0f b6 12             	movzbl (%rdx),%edx
     b22:	88 10                	mov    %dl,(%rax)
     b24:	0f b6 00             	movzbl (%rax),%eax
     b27:	84 c0                	test   %al,%al
     b29:	75 dc                	jne    b07 <strcpy+0x15>
    ;
  return os;
     b2b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     b2f:	5d                   	pop    %rbp
     b30:	c3                   	retq   

0000000000000b31 <strcmp>:

int strcmp(const char *p, const char *q) {
     b31:	55                   	push   %rbp
     b32:	48 89 e5             	mov    %rsp,%rbp
     b35:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     b39:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
     b3d:	eb 0a                	jmp    b49 <strcmp+0x18>
    p++, q++;
     b3f:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
     b44:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
     b49:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b4d:	0f b6 00             	movzbl (%rax),%eax
     b50:	84 c0                	test   %al,%al
     b52:	74 12                	je     b66 <strcmp+0x35>
     b54:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b58:	0f b6 10             	movzbl (%rax),%edx
     b5b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     b5f:	0f b6 00             	movzbl (%rax),%eax
     b62:	38 c2                	cmp    %al,%dl
     b64:	74 d9                	je     b3f <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
     b66:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b6a:	0f b6 00             	movzbl (%rax),%eax
     b6d:	0f b6 d0             	movzbl %al,%edx
     b70:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     b74:	0f b6 00             	movzbl (%rax),%eax
     b77:	0f b6 c0             	movzbl %al,%eax
     b7a:	29 c2                	sub    %eax,%edx
     b7c:	89 d0                	mov    %edx,%eax
}
     b7e:	5d                   	pop    %rbp
     b7f:	c3                   	retq   

0000000000000b80 <strlen>:

uint strlen(char *s) {
     b80:	55                   	push   %rbp
     b81:	48 89 e5             	mov    %rsp,%rbp
     b84:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
     b88:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     b8f:	eb 04                	jmp    b95 <strlen+0x15>
     b91:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     b95:	8b 45 fc             	mov    -0x4(%rbp),%eax
     b98:	48 63 d0             	movslq %eax,%rdx
     b9b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     b9f:	48 01 d0             	add    %rdx,%rax
     ba2:	0f b6 00             	movzbl (%rax),%eax
     ba5:	84 c0                	test   %al,%al
     ba7:	75 e8                	jne    b91 <strlen+0x11>
    ;
  return n;
     ba9:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     bac:	5d                   	pop    %rbp
     bad:	c3                   	retq   

0000000000000bae <memset>:

void *memset(void *dst, int c, uint n) {
     bae:	55                   	push   %rbp
     baf:	48 89 e5             	mov    %rsp,%rbp
     bb2:	48 83 ec 10          	sub    $0x10,%rsp
     bb6:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     bba:	89 75 f4             	mov    %esi,-0xc(%rbp)
     bbd:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
     bc0:	8b 55 f0             	mov    -0x10(%rbp),%edx
     bc3:	8b 4d f4             	mov    -0xc(%rbp),%ecx
     bc6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     bca:	89 ce                	mov    %ecx,%esi
     bcc:	48 89 c7             	mov    %rax,%rdi
     bcf:	e8 ec fe ff ff       	callq  ac0 <stosb>
  return dst;
     bd4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     bd8:	c9                   	leaveq 
     bd9:	c3                   	retq   

0000000000000bda <strchr>:

char *strchr(const char *s, char c) {
     bda:	55                   	push   %rbp
     bdb:	48 89 e5             	mov    %rsp,%rbp
     bde:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     be2:	89 f0                	mov    %esi,%eax
     be4:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
     be7:	eb 17                	jmp    c00 <strchr+0x26>
    if (*s == c)
     be9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     bed:	0f b6 00             	movzbl (%rax),%eax
     bf0:	3a 45 f4             	cmp    -0xc(%rbp),%al
     bf3:	75 06                	jne    bfb <strchr+0x21>
      return (char *)s;
     bf5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     bf9:	eb 15                	jmp    c10 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
     bfb:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
     c00:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     c04:	0f b6 00             	movzbl (%rax),%eax
     c07:	84 c0                	test   %al,%al
     c09:	75 de                	jne    be9 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
     c0b:	b8 00 00 00 00       	mov    $0x0,%eax
}
     c10:	5d                   	pop    %rbp
     c11:	c3                   	retq   

0000000000000c12 <gets>:

char *gets(char *buf, int max) {
     c12:	55                   	push   %rbp
     c13:	48 89 e5             	mov    %rsp,%rbp
     c16:	48 83 ec 20          	sub    $0x20,%rsp
     c1a:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     c1e:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
     c21:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     c28:	eb 48                	jmp    c72 <gets+0x60>
    cc = read(0, &c, 1);
     c2a:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
     c2e:	ba 01 00 00 00       	mov    $0x1,%edx
     c33:	48 89 c6             	mov    %rax,%rsi
     c36:	bf 00 00 00 00       	mov    $0x0,%edi
     c3b:	e8 6f 01 00 00       	callq  daf <read>
     c40:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
     c43:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     c47:	7e 36                	jle    c7f <gets+0x6d>
      break;
    buf[i++] = c;
     c49:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c4c:	8d 50 01             	lea    0x1(%rax),%edx
     c4f:	89 55 fc             	mov    %edx,-0x4(%rbp)
     c52:	48 63 d0             	movslq %eax,%rdx
     c55:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c59:	48 01 c2             	add    %rax,%rdx
     c5c:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     c60:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
     c62:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     c66:	3c 0a                	cmp    $0xa,%al
     c68:	74 16                	je     c80 <gets+0x6e>
     c6a:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     c6e:	3c 0d                	cmp    $0xd,%al
     c70:	74 0e                	je     c80 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
     c72:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c75:	83 c0 01             	add    $0x1,%eax
     c78:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
     c7b:	7c ad                	jl     c2a <gets+0x18>
     c7d:	eb 01                	jmp    c80 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
     c7f:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
     c80:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c83:	48 63 d0             	movslq %eax,%rdx
     c86:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c8a:	48 01 d0             	add    %rdx,%rax
     c8d:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
     c90:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
     c94:	c9                   	leaveq 
     c95:	c3                   	retq   

0000000000000c96 <stat>:

int stat(char *n, struct stat *st) {
     c96:	55                   	push   %rbp
     c97:	48 89 e5             	mov    %rsp,%rbp
     c9a:	48 83 ec 20          	sub    $0x20,%rsp
     c9e:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     ca2:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
     ca6:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     caa:	be 00 00 00 00       	mov    $0x0,%esi
     caf:	48 89 c7             	mov    %rax,%rdi
     cb2:	e8 20 01 00 00       	callq  dd7 <open>
     cb7:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
     cba:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     cbe:	79 07                	jns    cc7 <stat+0x31>
    return -1;
     cc0:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
     cc5:	eb 21                	jmp    ce8 <stat+0x52>
  r = fstat(fd, st);
     cc7:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     ccb:	8b 45 fc             	mov    -0x4(%rbp),%eax
     cce:	48 89 d6             	mov    %rdx,%rsi
     cd1:	89 c7                	mov    %eax,%edi
     cd3:	e8 17 01 00 00       	callq  def <fstat>
     cd8:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
     cdb:	8b 45 fc             	mov    -0x4(%rbp),%eax
     cde:	89 c7                	mov    %eax,%edi
     ce0:	e8 da 00 00 00       	callq  dbf <close>
  return r;
     ce5:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
     ce8:	c9                   	leaveq 
     ce9:	c3                   	retq   

0000000000000cea <atoi>:

int atoi(const char *s) {
     cea:	55                   	push   %rbp
     ceb:	48 89 e5             	mov    %rsp,%rbp
     cee:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
     cf2:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
     cf9:	eb 28                	jmp    d23 <atoi+0x39>
    n = n * 10 + *s++ - '0';
     cfb:	8b 55 fc             	mov    -0x4(%rbp),%edx
     cfe:	89 d0                	mov    %edx,%eax
     d00:	c1 e0 02             	shl    $0x2,%eax
     d03:	01 d0                	add    %edx,%eax
     d05:	01 c0                	add    %eax,%eax
     d07:	89 c1                	mov    %eax,%ecx
     d09:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d0d:	48 8d 50 01          	lea    0x1(%rax),%rdx
     d11:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
     d15:	0f b6 00             	movzbl (%rax),%eax
     d18:	0f be c0             	movsbl %al,%eax
     d1b:	01 c8                	add    %ecx,%eax
     d1d:	83 e8 30             	sub    $0x30,%eax
     d20:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
     d23:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d27:	0f b6 00             	movzbl (%rax),%eax
     d2a:	3c 2f                	cmp    $0x2f,%al
     d2c:	7e 0b                	jle    d39 <atoi+0x4f>
     d2e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d32:	0f b6 00             	movzbl (%rax),%eax
     d35:	3c 39                	cmp    $0x39,%al
     d37:	7e c2                	jle    cfb <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
     d39:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     d3c:	5d                   	pop    %rbp
     d3d:	c3                   	retq   

0000000000000d3e <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
     d3e:	55                   	push   %rbp
     d3f:	48 89 e5             	mov    %rsp,%rbp
     d42:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     d46:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
     d4a:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
     d4d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d51:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
     d55:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     d59:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
     d5d:	eb 1d                	jmp    d7c <memmove+0x3e>
    *dst++ = *src++;
     d5f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     d63:	48 8d 50 01          	lea    0x1(%rax),%rdx
     d67:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
     d6b:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
     d6f:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
     d73:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
     d77:	0f b6 12             	movzbl (%rdx),%edx
     d7a:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
     d7c:	8b 45 dc             	mov    -0x24(%rbp),%eax
     d7f:	8d 50 ff             	lea    -0x1(%rax),%edx
     d82:	89 55 dc             	mov    %edx,-0x24(%rbp)
     d85:	85 c0                	test   %eax,%eax
     d87:	7f d6                	jg     d5f <memmove+0x21>
    *dst++ = *src++;
  return vdst;
     d89:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d8d:	5d                   	pop    %rbp
     d8e:	c3                   	retq   

0000000000000d8f <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
     d8f:	b8 01 00 00 00       	mov    $0x1,%eax
     d94:	cd 40                	int    $0x40
     d96:	c3                   	retq   

0000000000000d97 <exit>:
SYSCALL(exit)
     d97:	b8 02 00 00 00       	mov    $0x2,%eax
     d9c:	cd 40                	int    $0x40
     d9e:	c3                   	retq   

0000000000000d9f <wait>:
SYSCALL(wait)
     d9f:	b8 03 00 00 00       	mov    $0x3,%eax
     da4:	cd 40                	int    $0x40
     da6:	c3                   	retq   

0000000000000da7 <pipe>:
SYSCALL(pipe)
     da7:	b8 04 00 00 00       	mov    $0x4,%eax
     dac:	cd 40                	int    $0x40
     dae:	c3                   	retq   

0000000000000daf <read>:
SYSCALL(read)
     daf:	b8 05 00 00 00       	mov    $0x5,%eax
     db4:	cd 40                	int    $0x40
     db6:	c3                   	retq   

0000000000000db7 <write>:
SYSCALL(write)
     db7:	b8 10 00 00 00       	mov    $0x10,%eax
     dbc:	cd 40                	int    $0x40
     dbe:	c3                   	retq   

0000000000000dbf <close>:
SYSCALL(close)
     dbf:	b8 15 00 00 00       	mov    $0x15,%eax
     dc4:	cd 40                	int    $0x40
     dc6:	c3                   	retq   

0000000000000dc7 <kill>:
SYSCALL(kill)
     dc7:	b8 06 00 00 00       	mov    $0x6,%eax
     dcc:	cd 40                	int    $0x40
     dce:	c3                   	retq   

0000000000000dcf <exec>:
SYSCALL(exec)
     dcf:	b8 07 00 00 00       	mov    $0x7,%eax
     dd4:	cd 40                	int    $0x40
     dd6:	c3                   	retq   

0000000000000dd7 <open>:
SYSCALL(open)
     dd7:	b8 0f 00 00 00       	mov    $0xf,%eax
     ddc:	cd 40                	int    $0x40
     dde:	c3                   	retq   

0000000000000ddf <mknod>:
SYSCALL(mknod)
     ddf:	b8 11 00 00 00       	mov    $0x11,%eax
     de4:	cd 40                	int    $0x40
     de6:	c3                   	retq   

0000000000000de7 <unlink>:
SYSCALL(unlink)
     de7:	b8 12 00 00 00       	mov    $0x12,%eax
     dec:	cd 40                	int    $0x40
     dee:	c3                   	retq   

0000000000000def <fstat>:
SYSCALL(fstat)
     def:	b8 08 00 00 00       	mov    $0x8,%eax
     df4:	cd 40                	int    $0x40
     df6:	c3                   	retq   

0000000000000df7 <link>:
SYSCALL(link)
     df7:	b8 13 00 00 00       	mov    $0x13,%eax
     dfc:	cd 40                	int    $0x40
     dfe:	c3                   	retq   

0000000000000dff <mkdir>:
SYSCALL(mkdir)
     dff:	b8 14 00 00 00       	mov    $0x14,%eax
     e04:	cd 40                	int    $0x40
     e06:	c3                   	retq   

0000000000000e07 <chdir>:
SYSCALL(chdir)
     e07:	b8 09 00 00 00       	mov    $0x9,%eax
     e0c:	cd 40                	int    $0x40
     e0e:	c3                   	retq   

0000000000000e0f <dup>:
SYSCALL(dup)
     e0f:	b8 0a 00 00 00       	mov    $0xa,%eax
     e14:	cd 40                	int    $0x40
     e16:	c3                   	retq   

0000000000000e17 <getpid>:
SYSCALL(getpid)
     e17:	b8 0b 00 00 00       	mov    $0xb,%eax
     e1c:	cd 40                	int    $0x40
     e1e:	c3                   	retq   

0000000000000e1f <sbrk>:
SYSCALL(sbrk)
     e1f:	b8 0c 00 00 00       	mov    $0xc,%eax
     e24:	cd 40                	int    $0x40
     e26:	c3                   	retq   

0000000000000e27 <sleep>:
SYSCALL(sleep)
     e27:	b8 0d 00 00 00       	mov    $0xd,%eax
     e2c:	cd 40                	int    $0x40
     e2e:	c3                   	retq   

0000000000000e2f <uptime>:
SYSCALL(uptime)
     e2f:	b8 0e 00 00 00       	mov    $0xe,%eax
     e34:	cd 40                	int    $0x40
     e36:	c3                   	retq   

0000000000000e37 <sysinfo>:
SYSCALL(sysinfo)
     e37:	b8 16 00 00 00       	mov    $0x16,%eax
     e3c:	cd 40                	int    $0x40
     e3e:	c3                   	retq   

0000000000000e3f <crashn>:
SYSCALL(crashn)
     e3f:	b8 17 00 00 00       	mov    $0x17,%eax
     e44:	cd 40                	int    $0x40
     e46:	c3                   	retq   

0000000000000e47 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
     e47:	55                   	push   %rbp
     e48:	48 89 e5             	mov    %rsp,%rbp
     e4b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
     e4f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     e53:	48 83 e8 10          	sub    $0x10,%rax
     e57:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
     e5b:	48 8b 05 8e 07 00 00 	mov    0x78e(%rip),%rax        # 15f0 <freep>
     e62:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
     e66:	eb 2f                	jmp    e97 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
     e68:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e6c:	48 8b 00             	mov    (%rax),%rax
     e6f:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     e73:	77 17                	ja     e8c <free+0x45>
     e75:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e79:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     e7d:	77 2f                	ja     eae <free+0x67>
     e7f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e83:	48 8b 00             	mov    (%rax),%rax
     e86:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     e8a:	77 22                	ja     eae <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
     e8c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e90:	48 8b 00             	mov    (%rax),%rax
     e93:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
     e97:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e9b:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     e9f:	76 c7                	jbe    e68 <free+0x21>
     ea1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ea5:	48 8b 00             	mov    (%rax),%rax
     ea8:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     eac:	76 ba                	jbe    e68 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
     eae:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     eb2:	8b 40 08             	mov    0x8(%rax),%eax
     eb5:	89 c0                	mov    %eax,%eax
     eb7:	48 c1 e0 04          	shl    $0x4,%rax
     ebb:	48 89 c2             	mov    %rax,%rdx
     ebe:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ec2:	48 01 c2             	add    %rax,%rdx
     ec5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ec9:	48 8b 00             	mov    (%rax),%rax
     ecc:	48 39 c2             	cmp    %rax,%rdx
     ecf:	75 2d                	jne    efe <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
     ed1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ed5:	8b 50 08             	mov    0x8(%rax),%edx
     ed8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     edc:	48 8b 00             	mov    (%rax),%rax
     edf:	8b 40 08             	mov    0x8(%rax),%eax
     ee2:	01 c2                	add    %eax,%edx
     ee4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ee8:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
     eeb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     eef:	48 8b 00             	mov    (%rax),%rax
     ef2:	48 8b 10             	mov    (%rax),%rdx
     ef5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ef9:	48 89 10             	mov    %rdx,(%rax)
     efc:	eb 0e                	jmp    f0c <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
     efe:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f02:	48 8b 10             	mov    (%rax),%rdx
     f05:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f09:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
     f0c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f10:	8b 40 08             	mov    0x8(%rax),%eax
     f13:	89 c0                	mov    %eax,%eax
     f15:	48 c1 e0 04          	shl    $0x4,%rax
     f19:	48 89 c2             	mov    %rax,%rdx
     f1c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f20:	48 01 d0             	add    %rdx,%rax
     f23:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     f27:	75 27                	jne    f50 <free+0x109>
    p->s.size += bp->s.size;
     f29:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f2d:	8b 50 08             	mov    0x8(%rax),%edx
     f30:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f34:	8b 40 08             	mov    0x8(%rax),%eax
     f37:	01 c2                	add    %eax,%edx
     f39:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f3d:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
     f40:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f44:	48 8b 10             	mov    (%rax),%rdx
     f47:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f4b:	48 89 10             	mov    %rdx,(%rax)
     f4e:	eb 0b                	jmp    f5b <free+0x114>
  } else
    p->s.ptr = bp;
     f50:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f54:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
     f58:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
     f5b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f5f:	48 89 05 8a 06 00 00 	mov    %rax,0x68a(%rip)        # 15f0 <freep>
}
     f66:	90                   	nop
     f67:	5d                   	pop    %rbp
     f68:	c3                   	retq   

0000000000000f69 <morecore>:

static Header *morecore(uint nu) {
     f69:	55                   	push   %rbp
     f6a:	48 89 e5             	mov    %rsp,%rbp
     f6d:	48 83 ec 20          	sub    $0x20,%rsp
     f71:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
     f74:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
     f7b:	77 07                	ja     f84 <morecore+0x1b>
    nu = 4096;
     f7d:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
     f84:	8b 45 ec             	mov    -0x14(%rbp),%eax
     f87:	c1 e0 04             	shl    $0x4,%eax
     f8a:	89 c7                	mov    %eax,%edi
     f8c:	e8 8e fe ff ff       	callq  e1f <sbrk>
     f91:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
     f95:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
     f9a:	75 07                	jne    fa3 <morecore+0x3a>
    return 0;
     f9c:	b8 00 00 00 00       	mov    $0x0,%eax
     fa1:	eb 29                	jmp    fcc <morecore+0x63>
  hp = (Header *)p;
     fa3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fa7:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
     fab:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     faf:	8b 55 ec             	mov    -0x14(%rbp),%edx
     fb2:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
     fb5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     fb9:	48 83 c0 10          	add    $0x10,%rax
     fbd:	48 89 c7             	mov    %rax,%rdi
     fc0:	e8 82 fe ff ff       	callq  e47 <free>
  return freep;
     fc5:	48 8b 05 24 06 00 00 	mov    0x624(%rip),%rax        # 15f0 <freep>
}
     fcc:	c9                   	leaveq 
     fcd:	c3                   	retq   

0000000000000fce <malloc>:

void *malloc(uint nbytes) {
     fce:	55                   	push   %rbp
     fcf:	48 89 e5             	mov    %rsp,%rbp
     fd2:	48 83 ec 30          	sub    $0x30,%rsp
     fd6:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
     fd9:	8b 45 dc             	mov    -0x24(%rbp),%eax
     fdc:	48 83 c0 0f          	add    $0xf,%rax
     fe0:	48 c1 e8 04          	shr    $0x4,%rax
     fe4:	83 c0 01             	add    $0x1,%eax
     fe7:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
     fea:	48 8b 05 ff 05 00 00 	mov    0x5ff(%rip),%rax        # 15f0 <freep>
     ff1:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     ff5:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     ffa:	75 2b                	jne    1027 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
     ffc:	48 c7 45 f0 e0 15 00 	movq   $0x15e0,-0x10(%rbp)
    1003:	00 
    1004:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1008:	48 89 05 e1 05 00 00 	mov    %rax,0x5e1(%rip)        # 15f0 <freep>
    100f:	48 8b 05 da 05 00 00 	mov    0x5da(%rip),%rax        # 15f0 <freep>
    1016:	48 89 05 c3 05 00 00 	mov    %rax,0x5c3(%rip)        # 15e0 <base>
    base.s.size = 0;
    101d:	c7 05 c1 05 00 00 00 	movl   $0x0,0x5c1(%rip)        # 15e8 <base+0x8>
    1024:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1027:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    102b:	48 8b 00             	mov    (%rax),%rax
    102e:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
    1032:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1036:	8b 40 08             	mov    0x8(%rax),%eax
    1039:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    103c:	72 5f                	jb     109d <malloc+0xcf>
      if (p->s.size == nunits)
    103e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1042:	8b 40 08             	mov    0x8(%rax),%eax
    1045:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1048:	75 10                	jne    105a <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
    104a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    104e:	48 8b 10             	mov    (%rax),%rdx
    1051:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1055:	48 89 10             	mov    %rdx,(%rax)
    1058:	eb 2e                	jmp    1088 <malloc+0xba>
      else {
        p->s.size -= nunits;
    105a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    105e:	8b 40 08             	mov    0x8(%rax),%eax
    1061:	2b 45 ec             	sub    -0x14(%rbp),%eax
    1064:	89 c2                	mov    %eax,%edx
    1066:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    106a:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
    106d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1071:	8b 40 08             	mov    0x8(%rax),%eax
    1074:	89 c0                	mov    %eax,%eax
    1076:	48 c1 e0 04          	shl    $0x4,%rax
    107a:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
    107e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1082:	8b 55 ec             	mov    -0x14(%rbp),%edx
    1085:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
    1088:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    108c:	48 89 05 5d 05 00 00 	mov    %rax,0x55d(%rip)        # 15f0 <freep>
      return (void *)(p + 1);
    1093:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1097:	48 83 c0 10          	add    $0x10,%rax
    109b:	eb 41                	jmp    10de <malloc+0x110>
    }
    if (p == freep)
    109d:	48 8b 05 4c 05 00 00 	mov    0x54c(%rip),%rax        # 15f0 <freep>
    10a4:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    10a8:	75 1c                	jne    10c6 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    10aa:	8b 45 ec             	mov    -0x14(%rbp),%eax
    10ad:	89 c7                	mov    %eax,%edi
    10af:	e8 b5 fe ff ff       	callq  f69 <morecore>
    10b4:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    10b8:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    10bd:	75 07                	jne    10c6 <malloc+0xf8>
        return 0;
    10bf:	b8 00 00 00 00       	mov    $0x0,%eax
    10c4:	eb 18                	jmp    10de <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    10c6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    10ca:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    10ce:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    10d2:	48 8b 00             	mov    (%rax),%rax
    10d5:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    10d9:	e9 54 ff ff ff       	jmpq   1032 <malloc+0x64>
    10de:	c9                   	leaveq 
    10df:	c3                   	retq   
