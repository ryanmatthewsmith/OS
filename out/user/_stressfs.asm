
out/user/_stressfs:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <fcntl.h>
#include <fs.h>
#include <stat.h>
#include <user.h>

int main(int argc, char *argv[]) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 81 ec 30 02 00 00 	sub    $0x230,%rsp
   b:	89 bd dc fd ff ff    	mov    %edi,-0x224(%rbp)
  11:	48 89 b5 d0 fd ff ff 	mov    %rsi,-0x230(%rbp)
  int fd, i;
  char path[] = "stressfs0";
  18:	48 b8 73 74 72 65 73 	movabs $0x7366737365727473,%rax
  1f:	73 66 73 
  22:	48 89 45 ee          	mov    %rax,-0x12(%rbp)
  26:	66 c7 45 f6 30 00    	movw   $0x30,-0xa(%rbp)
  char data[512];

  printf(1, "stressfs starting\n");
  2c:	be f7 0b 00 00       	mov    $0xbf7,%esi
  31:	bf 01 00 00 00       	mov    $0x1,%edi
  36:	b8 00 00 00 00       	mov    $0x0,%eax
  3b:	e8 cb 02 00 00       	callq  30b <printf>
  memset(data, 'a', sizeof(data));
  40:	48 8d 85 ee fd ff ff 	lea    -0x212(%rbp),%rax
  47:	ba 00 02 00 00       	mov    $0x200,%edx
  4c:	be 61 00 00 00       	mov    $0x61,%esi
  51:	48 89 c7             	mov    %rax,%rdi
  54:	e8 6c 06 00 00       	callq  6c5 <memset>

  for (i = 0; i < 4; i++)
  59:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  60:	eb 0d                	jmp    6f <main+0x6f>
    if (fork() > 0)
  62:	e8 3f 08 00 00       	callq  8a6 <fork>
  67:	85 c0                	test   %eax,%eax
  69:	7f 0c                	jg     77 <main+0x77>
  char data[512];

  printf(1, "stressfs starting\n");
  memset(data, 'a', sizeof(data));

  for (i = 0; i < 4; i++)
  6b:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  6f:	83 7d fc 03          	cmpl   $0x3,-0x4(%rbp)
  73:	7e ed                	jle    62 <main+0x62>
  75:	eb 01                	jmp    78 <main+0x78>
    if (fork() > 0)
      break;
  77:	90                   	nop

  printf(1, "write %d\n", i);
  78:	8b 45 fc             	mov    -0x4(%rbp),%eax
  7b:	89 c2                	mov    %eax,%edx
  7d:	be 0a 0c 00 00       	mov    $0xc0a,%esi
  82:	bf 01 00 00 00       	mov    $0x1,%edi
  87:	b8 00 00 00 00       	mov    $0x0,%eax
  8c:	e8 7a 02 00 00       	callq  30b <printf>

  path[8] += i;
  91:	0f b6 45 f6          	movzbl -0xa(%rbp),%eax
  95:	89 c2                	mov    %eax,%edx
  97:	8b 45 fc             	mov    -0x4(%rbp),%eax
  9a:	01 d0                	add    %edx,%eax
  9c:	88 45 f6             	mov    %al,-0xa(%rbp)
  fd = open(path, O_CREATE | O_RDWR);
  9f:	48 8d 45 ee          	lea    -0x12(%rbp),%rax
  a3:	be 02 02 00 00       	mov    $0x202,%esi
  a8:	48 89 c7             	mov    %rax,%rdi
  ab:	e8 3e 08 00 00       	callq  8ee <open>
  b0:	89 45 f8             	mov    %eax,-0x8(%rbp)
  for (i = 0; i < 20; i++)
  b3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  ba:	eb 1d                	jmp    d9 <main+0xd9>
    //    printf(fd, "%d\n", i);
    write(fd, data, sizeof(data));
  bc:	48 8d 8d ee fd ff ff 	lea    -0x212(%rbp),%rcx
  c3:	8b 45 f8             	mov    -0x8(%rbp),%eax
  c6:	ba 00 02 00 00       	mov    $0x200,%edx
  cb:	48 89 ce             	mov    %rcx,%rsi
  ce:	89 c7                	mov    %eax,%edi
  d0:	e8 f9 07 00 00       	callq  8ce <write>

  printf(1, "write %d\n", i);

  path[8] += i;
  fd = open(path, O_CREATE | O_RDWR);
  for (i = 0; i < 20; i++)
  d5:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  d9:	83 7d fc 13          	cmpl   $0x13,-0x4(%rbp)
  dd:	7e dd                	jle    bc <main+0xbc>
    //    printf(fd, "%d\n", i);
    write(fd, data, sizeof(data));
  close(fd);
  df:	8b 45 f8             	mov    -0x8(%rbp),%eax
  e2:	89 c7                	mov    %eax,%edi
  e4:	e8 ed 07 00 00       	callq  8d6 <close>

  printf(1, "read\n");
  e9:	be 14 0c 00 00       	mov    $0xc14,%esi
  ee:	bf 01 00 00 00       	mov    $0x1,%edi
  f3:	b8 00 00 00 00       	mov    $0x0,%eax
  f8:	e8 0e 02 00 00       	callq  30b <printf>

  fd = open(path, O_RDONLY);
  fd:	48 8d 45 ee          	lea    -0x12(%rbp),%rax
 101:	be 00 00 00 00       	mov    $0x0,%esi
 106:	48 89 c7             	mov    %rax,%rdi
 109:	e8 e0 07 00 00       	callq  8ee <open>
 10e:	89 45 f8             	mov    %eax,-0x8(%rbp)
  for (i = 0; i < 20; i++)
 111:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 118:	eb 1d                	jmp    137 <main+0x137>
    read(fd, data, sizeof(data));
 11a:	48 8d 8d ee fd ff ff 	lea    -0x212(%rbp),%rcx
 121:	8b 45 f8             	mov    -0x8(%rbp),%eax
 124:	ba 00 02 00 00       	mov    $0x200,%edx
 129:	48 89 ce             	mov    %rcx,%rsi
 12c:	89 c7                	mov    %eax,%edi
 12e:	e8 93 07 00 00       	callq  8c6 <read>
  close(fd);

  printf(1, "read\n");

  fd = open(path, O_RDONLY);
  for (i = 0; i < 20; i++)
 133:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 137:	83 7d fc 13          	cmpl   $0x13,-0x4(%rbp)
 13b:	7e dd                	jle    11a <main+0x11a>
    read(fd, data, sizeof(data));
  close(fd);
 13d:	8b 45 f8             	mov    -0x8(%rbp),%eax
 140:	89 c7                	mov    %eax,%edi
 142:	e8 8f 07 00 00       	callq  8d6 <close>

  wait();
 147:	e8 6a 07 00 00       	callq  8b6 <wait>

  exit();
 14c:	e8 5d 07 00 00       	callq  8ae <exit>

0000000000000151 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
 151:	55                   	push   %rbp
 152:	48 89 e5             	mov    %rsp,%rbp
 155:	48 83 ec 10          	sub    $0x10,%rsp
 159:	89 7d fc             	mov    %edi,-0x4(%rbp)
 15c:	89 f0                	mov    %esi,%eax
 15e:	88 45 f8             	mov    %al,-0x8(%rbp)
 161:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
 165:	8b 45 fc             	mov    -0x4(%rbp),%eax
 168:	ba 01 00 00 00       	mov    $0x1,%edx
 16d:	48 89 ce             	mov    %rcx,%rsi
 170:	89 c7                	mov    %eax,%edi
 172:	e8 57 07 00 00       	callq  8ce <write>
 177:	90                   	nop
 178:	c9                   	leaveq 
 179:	c3                   	retq   

000000000000017a <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
 17a:	55                   	push   %rbp
 17b:	48 89 e5             	mov    %rsp,%rbp
 17e:	48 83 ec 40          	sub    $0x40,%rsp
 182:	89 7d cc             	mov    %edi,-0x34(%rbp)
 185:	89 75 c8             	mov    %esi,-0x38(%rbp)
 188:	89 55 c4             	mov    %edx,-0x3c(%rbp)
 18b:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
 18e:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 192:	74 1f                	je     1b3 <printint64+0x39>
 194:	8b 45 c8             	mov    -0x38(%rbp),%eax
 197:	c1 e8 1f             	shr    $0x1f,%eax
 19a:	0f b6 c0             	movzbl %al,%eax
 19d:	89 45 c0             	mov    %eax,-0x40(%rbp)
 1a0:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 1a4:	74 0d                	je     1b3 <printint64+0x39>
    x = -xx;
 1a6:	8b 45 c8             	mov    -0x38(%rbp),%eax
 1a9:	f7 d8                	neg    %eax
 1ab:	48 98                	cltq   
 1ad:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 1b1:	eb 09                	jmp    1bc <printint64+0x42>
  else
    x = xx;
 1b3:	8b 45 c8             	mov    -0x38(%rbp),%eax
 1b6:	48 98                	cltq   
 1b8:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 1bc:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1c3:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1c6:	8d 41 01             	lea    0x1(%rcx),%eax
 1c9:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1cc:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 1cf:	48 63 f0             	movslq %eax,%rsi
 1d2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 1d6:	ba 00 00 00 00       	mov    $0x0,%edx
 1db:	48 f7 f6             	div    %rsi
 1de:	48 89 d0             	mov    %rdx,%rax
 1e1:	0f b6 90 80 0e 00 00 	movzbl 0xe80(%rax),%edx
 1e8:	48 63 c1             	movslq %ecx,%rax
 1eb:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 1ef:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 1f2:	48 63 f8             	movslq %eax,%rdi
 1f5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 1f9:	ba 00 00 00 00       	mov    $0x0,%edx
 1fe:	48 f7 f7             	div    %rdi
 201:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 205:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 20a:	75 b7                	jne    1c3 <printint64+0x49>

  if (sgn)
 20c:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 210:	74 2b                	je     23d <printint64+0xc3>
    buf[i++] = '-';
 212:	8b 45 fc             	mov    -0x4(%rbp),%eax
 215:	8d 50 01             	lea    0x1(%rax),%edx
 218:	89 55 fc             	mov    %edx,-0x4(%rbp)
 21b:	48 98                	cltq   
 21d:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 222:	eb 19                	jmp    23d <printint64+0xc3>
    putc(fd, buf[i]);
 224:	8b 45 fc             	mov    -0x4(%rbp),%eax
 227:	48 98                	cltq   
 229:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 22e:	0f be d0             	movsbl %al,%edx
 231:	8b 45 cc             	mov    -0x34(%rbp),%eax
 234:	89 d6                	mov    %edx,%esi
 236:	89 c7                	mov    %eax,%edi
 238:	e8 14 ff ff ff       	callq  151 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 23d:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 241:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 245:	79 dd                	jns    224 <printint64+0xaa>
    putc(fd, buf[i]);
}
 247:	90                   	nop
 248:	c9                   	leaveq 
 249:	c3                   	retq   

000000000000024a <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 24a:	55                   	push   %rbp
 24b:	48 89 e5             	mov    %rsp,%rbp
 24e:	48 83 ec 30          	sub    $0x30,%rsp
 252:	89 7d dc             	mov    %edi,-0x24(%rbp)
 255:	89 75 d8             	mov    %esi,-0x28(%rbp)
 258:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 25b:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 25e:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 265:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 269:	74 17                	je     282 <printint+0x38>
 26b:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 26f:	79 11                	jns    282 <printint+0x38>
    neg = 1;
 271:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 278:	8b 45 d8             	mov    -0x28(%rbp),%eax
 27b:	f7 d8                	neg    %eax
 27d:	89 45 f4             	mov    %eax,-0xc(%rbp)
 280:	eb 06                	jmp    288 <printint+0x3e>
  } else {
    x = xx;
 282:	8b 45 d8             	mov    -0x28(%rbp),%eax
 285:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 288:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 28f:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 292:	8d 41 01             	lea    0x1(%rcx),%eax
 295:	89 45 fc             	mov    %eax,-0x4(%rbp)
 298:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 29b:	8b 45 f4             	mov    -0xc(%rbp),%eax
 29e:	ba 00 00 00 00       	mov    $0x0,%edx
 2a3:	f7 f6                	div    %esi
 2a5:	89 d0                	mov    %edx,%eax
 2a7:	89 c0                	mov    %eax,%eax
 2a9:	0f b6 90 a0 0e 00 00 	movzbl 0xea0(%rax),%edx
 2b0:	48 63 c1             	movslq %ecx,%rax
 2b3:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 2b7:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 2ba:	8b 45 f4             	mov    -0xc(%rbp),%eax
 2bd:	ba 00 00 00 00       	mov    $0x0,%edx
 2c2:	f7 f7                	div    %edi
 2c4:	89 45 f4             	mov    %eax,-0xc(%rbp)
 2c7:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 2cb:	75 c2                	jne    28f <printint+0x45>
  if (neg)
 2cd:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 2d1:	74 2b                	je     2fe <printint+0xb4>
    buf[i++] = '-';
 2d3:	8b 45 fc             	mov    -0x4(%rbp),%eax
 2d6:	8d 50 01             	lea    0x1(%rax),%edx
 2d9:	89 55 fc             	mov    %edx,-0x4(%rbp)
 2dc:	48 98                	cltq   
 2de:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 2e3:	eb 19                	jmp    2fe <printint+0xb4>
    putc(fd, buf[i]);
 2e5:	8b 45 fc             	mov    -0x4(%rbp),%eax
 2e8:	48 98                	cltq   
 2ea:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 2ef:	0f be d0             	movsbl %al,%edx
 2f2:	8b 45 dc             	mov    -0x24(%rbp),%eax
 2f5:	89 d6                	mov    %edx,%esi
 2f7:	89 c7                	mov    %eax,%edi
 2f9:	e8 53 fe ff ff       	callq  151 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 2fe:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 302:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 306:	79 dd                	jns    2e5 <printint+0x9b>
    putc(fd, buf[i]);
}
 308:	90                   	nop
 309:	c9                   	leaveq 
 30a:	c3                   	retq   

000000000000030b <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 30b:	55                   	push   %rbp
 30c:	48 89 e5             	mov    %rsp,%rbp
 30f:	48 83 ec 70          	sub    $0x70,%rsp
 313:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 316:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 31a:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 31e:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 322:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 326:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 32a:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 331:	48 8d 45 10          	lea    0x10(%rbp),%rax
 335:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 339:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 33d:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 341:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 348:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 34f:	e9 68 02 00 00       	jmpq   5bc <printf+0x2b1>
    c = fmt[i] & 0xff;
 354:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 357:	48 63 d0             	movslq %eax,%rdx
 35a:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 35e:	48 01 d0             	add    %rdx,%rax
 361:	0f b6 00             	movzbl (%rax),%eax
 364:	0f be c0             	movsbl %al,%eax
 367:	25 ff 00 00 00       	and    $0xff,%eax
 36c:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 36f:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 373:	75 30                	jne    3a5 <printf+0x9a>
      if (c == '%') {
 375:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 379:	75 13                	jne    38e <printf+0x83>
        state = '%';
 37b:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 382:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 389:	e9 2a 02 00 00       	jmpq   5b8 <printf+0x2ad>
      } else {
        putc(fd, c);
 38e:	8b 45 b8             	mov    -0x48(%rbp),%eax
 391:	0f be d0             	movsbl %al,%edx
 394:	8b 45 9c             	mov    -0x64(%rbp),%eax
 397:	89 d6                	mov    %edx,%esi
 399:	89 c7                	mov    %eax,%edi
 39b:	e8 b1 fd ff ff       	callq  151 <putc>
 3a0:	e9 13 02 00 00       	jmpq   5b8 <printf+0x2ad>
      }
    } else if (state == '%') {
 3a5:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 3a9:	0f 85 09 02 00 00    	jne    5b8 <printf+0x2ad>
      if (c == 'l') {
 3af:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 3b3:	75 0c                	jne    3c1 <printf+0xb6>
        lflag = 1;
 3b5:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 3bc:	e9 f7 01 00 00       	jmpq   5b8 <printf+0x2ad>
      } else if (c == 'd') {
 3c1:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 3c5:	0f 85 95 00 00 00    	jne    460 <printf+0x155>
        if (lflag == 1)
 3cb:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 3cf:	75 49                	jne    41a <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 3d1:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3d4:	83 f8 30             	cmp    $0x30,%eax
 3d7:	73 17                	jae    3f0 <printf+0xe5>
 3d9:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3dd:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3e0:	89 d2                	mov    %edx,%edx
 3e2:	48 01 d0             	add    %rdx,%rax
 3e5:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3e8:	83 c2 08             	add    $0x8,%edx
 3eb:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3ee:	eb 0c                	jmp    3fc <printf+0xf1>
 3f0:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3f4:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3f8:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3fc:	48 8b 00             	mov    (%rax),%rax
 3ff:	89 c6                	mov    %eax,%esi
 401:	8b 45 9c             	mov    -0x64(%rbp),%eax
 404:	b9 01 00 00 00       	mov    $0x1,%ecx
 409:	ba 0a 00 00 00       	mov    $0xa,%edx
 40e:	89 c7                	mov    %eax,%edi
 410:	e8 65 fd ff ff       	callq  17a <printint64>
 415:	e9 97 01 00 00       	jmpq   5b1 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 41a:	8b 45 a0             	mov    -0x60(%rbp),%eax
 41d:	83 f8 30             	cmp    $0x30,%eax
 420:	73 17                	jae    439 <printf+0x12e>
 422:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 426:	8b 55 a0             	mov    -0x60(%rbp),%edx
 429:	89 d2                	mov    %edx,%edx
 42b:	48 01 d0             	add    %rdx,%rax
 42e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 431:	83 c2 08             	add    $0x8,%edx
 434:	89 55 a0             	mov    %edx,-0x60(%rbp)
 437:	eb 0c                	jmp    445 <printf+0x13a>
 439:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 43d:	48 8d 50 08          	lea    0x8(%rax),%rdx
 441:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 445:	8b 30                	mov    (%rax),%esi
 447:	8b 45 9c             	mov    -0x64(%rbp),%eax
 44a:	b9 01 00 00 00       	mov    $0x1,%ecx
 44f:	ba 0a 00 00 00       	mov    $0xa,%edx
 454:	89 c7                	mov    %eax,%edi
 456:	e8 ef fd ff ff       	callq  24a <printint>
 45b:	e9 51 01 00 00       	jmpq   5b1 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 460:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 464:	74 0a                	je     470 <printf+0x165>
 466:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 46a:	0f 85 95 00 00 00    	jne    505 <printf+0x1fa>
        if (lflag == 1)
 470:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 474:	75 49                	jne    4bf <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 476:	8b 45 a0             	mov    -0x60(%rbp),%eax
 479:	83 f8 30             	cmp    $0x30,%eax
 47c:	73 17                	jae    495 <printf+0x18a>
 47e:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 482:	8b 55 a0             	mov    -0x60(%rbp),%edx
 485:	89 d2                	mov    %edx,%edx
 487:	48 01 d0             	add    %rdx,%rax
 48a:	8b 55 a0             	mov    -0x60(%rbp),%edx
 48d:	83 c2 08             	add    $0x8,%edx
 490:	89 55 a0             	mov    %edx,-0x60(%rbp)
 493:	eb 0c                	jmp    4a1 <printf+0x196>
 495:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 499:	48 8d 50 08          	lea    0x8(%rax),%rdx
 49d:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 4a1:	48 8b 00             	mov    (%rax),%rax
 4a4:	89 c6                	mov    %eax,%esi
 4a6:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4a9:	b9 00 00 00 00       	mov    $0x0,%ecx
 4ae:	ba 10 00 00 00       	mov    $0x10,%edx
 4b3:	89 c7                	mov    %eax,%edi
 4b5:	e8 c0 fc ff ff       	callq  17a <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 4ba:	e9 f2 00 00 00       	jmpq   5b1 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 4bf:	8b 45 a0             	mov    -0x60(%rbp),%eax
 4c2:	83 f8 30             	cmp    $0x30,%eax
 4c5:	73 17                	jae    4de <printf+0x1d3>
 4c7:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 4cb:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4ce:	89 d2                	mov    %edx,%edx
 4d0:	48 01 d0             	add    %rdx,%rax
 4d3:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4d6:	83 c2 08             	add    $0x8,%edx
 4d9:	89 55 a0             	mov    %edx,-0x60(%rbp)
 4dc:	eb 0c                	jmp    4ea <printf+0x1df>
 4de:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 4e2:	48 8d 50 08          	lea    0x8(%rax),%rdx
 4e6:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 4ea:	8b 30                	mov    (%rax),%esi
 4ec:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4ef:	b9 00 00 00 00       	mov    $0x0,%ecx
 4f4:	ba 10 00 00 00       	mov    $0x10,%edx
 4f9:	89 c7                	mov    %eax,%edi
 4fb:	e8 4a fd ff ff       	callq  24a <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 500:	e9 ac 00 00 00       	jmpq   5b1 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 505:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 509:	75 6b                	jne    576 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 50b:	8b 45 a0             	mov    -0x60(%rbp),%eax
 50e:	83 f8 30             	cmp    $0x30,%eax
 511:	73 17                	jae    52a <printf+0x21f>
 513:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 517:	8b 55 a0             	mov    -0x60(%rbp),%edx
 51a:	89 d2                	mov    %edx,%edx
 51c:	48 01 d0             	add    %rdx,%rax
 51f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 522:	83 c2 08             	add    $0x8,%edx
 525:	89 55 a0             	mov    %edx,-0x60(%rbp)
 528:	eb 0c                	jmp    536 <printf+0x22b>
 52a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 52e:	48 8d 50 08          	lea    0x8(%rax),%rdx
 532:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 536:	48 8b 00             	mov    (%rax),%rax
 539:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 53d:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 542:	75 25                	jne    569 <printf+0x25e>
          s = "(null)";
 544:	48 c7 45 c8 1a 0c 00 	movq   $0xc1a,-0x38(%rbp)
 54b:	00 
        for (; *s; s++)
 54c:	eb 1b                	jmp    569 <printf+0x25e>
          putc(fd, *s);
 54e:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 552:	0f b6 00             	movzbl (%rax),%eax
 555:	0f be d0             	movsbl %al,%edx
 558:	8b 45 9c             	mov    -0x64(%rbp),%eax
 55b:	89 d6                	mov    %edx,%esi
 55d:	89 c7                	mov    %eax,%edi
 55f:	e8 ed fb ff ff       	callq  151 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 564:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 569:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 56d:	0f b6 00             	movzbl (%rax),%eax
 570:	84 c0                	test   %al,%al
 572:	75 da                	jne    54e <printf+0x243>
 574:	eb 3b                	jmp    5b1 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 576:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 57a:	75 14                	jne    590 <printf+0x285>
        putc(fd, c);
 57c:	8b 45 b8             	mov    -0x48(%rbp),%eax
 57f:	0f be d0             	movsbl %al,%edx
 582:	8b 45 9c             	mov    -0x64(%rbp),%eax
 585:	89 d6                	mov    %edx,%esi
 587:	89 c7                	mov    %eax,%edi
 589:	e8 c3 fb ff ff       	callq  151 <putc>
 58e:	eb 21                	jmp    5b1 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 590:	8b 45 9c             	mov    -0x64(%rbp),%eax
 593:	be 25 00 00 00       	mov    $0x25,%esi
 598:	89 c7                	mov    %eax,%edi
 59a:	e8 b2 fb ff ff       	callq  151 <putc>
        putc(fd, c);
 59f:	8b 45 b8             	mov    -0x48(%rbp),%eax
 5a2:	0f be d0             	movsbl %al,%edx
 5a5:	8b 45 9c             	mov    -0x64(%rbp),%eax
 5a8:	89 d6                	mov    %edx,%esi
 5aa:	89 c7                	mov    %eax,%edi
 5ac:	e8 a0 fb ff ff       	callq  151 <putc>
      }
      state = 0;
 5b1:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 5b8:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 5bc:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 5bf:	48 63 d0             	movslq %eax,%rdx
 5c2:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 5c6:	48 01 d0             	add    %rdx,%rax
 5c9:	0f b6 00             	movzbl (%rax),%eax
 5cc:	84 c0                	test   %al,%al
 5ce:	0f 85 80 fd ff ff    	jne    354 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 5d4:	90                   	nop
 5d5:	c9                   	leaveq 
 5d6:	c3                   	retq   

00000000000005d7 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 5d7:	55                   	push   %rbp
 5d8:	48 89 e5             	mov    %rsp,%rbp
 5db:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5df:	89 75 f4             	mov    %esi,-0xc(%rbp)
 5e2:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 5e5:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 5e9:	8b 55 f0             	mov    -0x10(%rbp),%edx
 5ec:	8b 45 f4             	mov    -0xc(%rbp),%eax
 5ef:	48 89 ce             	mov    %rcx,%rsi
 5f2:	48 89 f7             	mov    %rsi,%rdi
 5f5:	89 d1                	mov    %edx,%ecx
 5f7:	fc                   	cld    
 5f8:	f3 aa                	rep stos %al,%es:(%rdi)
 5fa:	89 ca                	mov    %ecx,%edx
 5fc:	48 89 fe             	mov    %rdi,%rsi
 5ff:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 603:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 606:	90                   	nop
 607:	5d                   	pop    %rbp
 608:	c3                   	retq   

0000000000000609 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 609:	55                   	push   %rbp
 60a:	48 89 e5             	mov    %rsp,%rbp
 60d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 611:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 615:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 619:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 61d:	90                   	nop
 61e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 622:	48 8d 50 01          	lea    0x1(%rax),%rdx
 626:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 62a:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 62e:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 632:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 636:	0f b6 12             	movzbl (%rdx),%edx
 639:	88 10                	mov    %dl,(%rax)
 63b:	0f b6 00             	movzbl (%rax),%eax
 63e:	84 c0                	test   %al,%al
 640:	75 dc                	jne    61e <strcpy+0x15>
    ;
  return os;
 642:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 646:	5d                   	pop    %rbp
 647:	c3                   	retq   

0000000000000648 <strcmp>:

int strcmp(const char *p, const char *q) {
 648:	55                   	push   %rbp
 649:	48 89 e5             	mov    %rsp,%rbp
 64c:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 650:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 654:	eb 0a                	jmp    660 <strcmp+0x18>
    p++, q++;
 656:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 65b:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 660:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 664:	0f b6 00             	movzbl (%rax),%eax
 667:	84 c0                	test   %al,%al
 669:	74 12                	je     67d <strcmp+0x35>
 66b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 66f:	0f b6 10             	movzbl (%rax),%edx
 672:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 676:	0f b6 00             	movzbl (%rax),%eax
 679:	38 c2                	cmp    %al,%dl
 67b:	74 d9                	je     656 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 67d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 681:	0f b6 00             	movzbl (%rax),%eax
 684:	0f b6 d0             	movzbl %al,%edx
 687:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 68b:	0f b6 00             	movzbl (%rax),%eax
 68e:	0f b6 c0             	movzbl %al,%eax
 691:	29 c2                	sub    %eax,%edx
 693:	89 d0                	mov    %edx,%eax
}
 695:	5d                   	pop    %rbp
 696:	c3                   	retq   

0000000000000697 <strlen>:

uint strlen(char *s) {
 697:	55                   	push   %rbp
 698:	48 89 e5             	mov    %rsp,%rbp
 69b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 69f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 6a6:	eb 04                	jmp    6ac <strlen+0x15>
 6a8:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 6ac:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6af:	48 63 d0             	movslq %eax,%rdx
 6b2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6b6:	48 01 d0             	add    %rdx,%rax
 6b9:	0f b6 00             	movzbl (%rax),%eax
 6bc:	84 c0                	test   %al,%al
 6be:	75 e8                	jne    6a8 <strlen+0x11>
    ;
  return n;
 6c0:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 6c3:	5d                   	pop    %rbp
 6c4:	c3                   	retq   

00000000000006c5 <memset>:

void *memset(void *dst, int c, uint n) {
 6c5:	55                   	push   %rbp
 6c6:	48 89 e5             	mov    %rsp,%rbp
 6c9:	48 83 ec 10          	sub    $0x10,%rsp
 6cd:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 6d1:	89 75 f4             	mov    %esi,-0xc(%rbp)
 6d4:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 6d7:	8b 55 f0             	mov    -0x10(%rbp),%edx
 6da:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 6dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6e1:	89 ce                	mov    %ecx,%esi
 6e3:	48 89 c7             	mov    %rax,%rdi
 6e6:	e8 ec fe ff ff       	callq  5d7 <stosb>
  return dst;
 6eb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 6ef:	c9                   	leaveq 
 6f0:	c3                   	retq   

00000000000006f1 <strchr>:

char *strchr(const char *s, char c) {
 6f1:	55                   	push   %rbp
 6f2:	48 89 e5             	mov    %rsp,%rbp
 6f5:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 6f9:	89 f0                	mov    %esi,%eax
 6fb:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 6fe:	eb 17                	jmp    717 <strchr+0x26>
    if (*s == c)
 700:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 704:	0f b6 00             	movzbl (%rax),%eax
 707:	3a 45 f4             	cmp    -0xc(%rbp),%al
 70a:	75 06                	jne    712 <strchr+0x21>
      return (char *)s;
 70c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 710:	eb 15                	jmp    727 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 712:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 717:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 71b:	0f b6 00             	movzbl (%rax),%eax
 71e:	84 c0                	test   %al,%al
 720:	75 de                	jne    700 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 722:	b8 00 00 00 00       	mov    $0x0,%eax
}
 727:	5d                   	pop    %rbp
 728:	c3                   	retq   

0000000000000729 <gets>:

char *gets(char *buf, int max) {
 729:	55                   	push   %rbp
 72a:	48 89 e5             	mov    %rsp,%rbp
 72d:	48 83 ec 20          	sub    $0x20,%rsp
 731:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 735:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 738:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 73f:	eb 48                	jmp    789 <gets+0x60>
    cc = read(0, &c, 1);
 741:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 745:	ba 01 00 00 00       	mov    $0x1,%edx
 74a:	48 89 c6             	mov    %rax,%rsi
 74d:	bf 00 00 00 00       	mov    $0x0,%edi
 752:	e8 6f 01 00 00       	callq  8c6 <read>
 757:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 75a:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 75e:	7e 36                	jle    796 <gets+0x6d>
      break;
    buf[i++] = c;
 760:	8b 45 fc             	mov    -0x4(%rbp),%eax
 763:	8d 50 01             	lea    0x1(%rax),%edx
 766:	89 55 fc             	mov    %edx,-0x4(%rbp)
 769:	48 63 d0             	movslq %eax,%rdx
 76c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 770:	48 01 c2             	add    %rax,%rdx
 773:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 777:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 779:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 77d:	3c 0a                	cmp    $0xa,%al
 77f:	74 16                	je     797 <gets+0x6e>
 781:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 785:	3c 0d                	cmp    $0xd,%al
 787:	74 0e                	je     797 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 789:	8b 45 fc             	mov    -0x4(%rbp),%eax
 78c:	83 c0 01             	add    $0x1,%eax
 78f:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 792:	7c ad                	jl     741 <gets+0x18>
 794:	eb 01                	jmp    797 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 796:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 797:	8b 45 fc             	mov    -0x4(%rbp),%eax
 79a:	48 63 d0             	movslq %eax,%rdx
 79d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7a1:	48 01 d0             	add    %rdx,%rax
 7a4:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 7a7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 7ab:	c9                   	leaveq 
 7ac:	c3                   	retq   

00000000000007ad <stat>:

int stat(char *n, struct stat *st) {
 7ad:	55                   	push   %rbp
 7ae:	48 89 e5             	mov    %rsp,%rbp
 7b1:	48 83 ec 20          	sub    $0x20,%rsp
 7b5:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 7b9:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 7bd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7c1:	be 00 00 00 00       	mov    $0x0,%esi
 7c6:	48 89 c7             	mov    %rax,%rdi
 7c9:	e8 20 01 00 00       	callq  8ee <open>
 7ce:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 7d1:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 7d5:	79 07                	jns    7de <stat+0x31>
    return -1;
 7d7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 7dc:	eb 21                	jmp    7ff <stat+0x52>
  r = fstat(fd, st);
 7de:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 7e2:	8b 45 fc             	mov    -0x4(%rbp),%eax
 7e5:	48 89 d6             	mov    %rdx,%rsi
 7e8:	89 c7                	mov    %eax,%edi
 7ea:	e8 17 01 00 00       	callq  906 <fstat>
 7ef:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 7f2:	8b 45 fc             	mov    -0x4(%rbp),%eax
 7f5:	89 c7                	mov    %eax,%edi
 7f7:	e8 da 00 00 00       	callq  8d6 <close>
  return r;
 7fc:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 7ff:	c9                   	leaveq 
 800:	c3                   	retq   

0000000000000801 <atoi>:

int atoi(const char *s) {
 801:	55                   	push   %rbp
 802:	48 89 e5             	mov    %rsp,%rbp
 805:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 809:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 810:	eb 28                	jmp    83a <atoi+0x39>
    n = n * 10 + *s++ - '0';
 812:	8b 55 fc             	mov    -0x4(%rbp),%edx
 815:	89 d0                	mov    %edx,%eax
 817:	c1 e0 02             	shl    $0x2,%eax
 81a:	01 d0                	add    %edx,%eax
 81c:	01 c0                	add    %eax,%eax
 81e:	89 c1                	mov    %eax,%ecx
 820:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 824:	48 8d 50 01          	lea    0x1(%rax),%rdx
 828:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 82c:	0f b6 00             	movzbl (%rax),%eax
 82f:	0f be c0             	movsbl %al,%eax
 832:	01 c8                	add    %ecx,%eax
 834:	83 e8 30             	sub    $0x30,%eax
 837:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 83a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 83e:	0f b6 00             	movzbl (%rax),%eax
 841:	3c 2f                	cmp    $0x2f,%al
 843:	7e 0b                	jle    850 <atoi+0x4f>
 845:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 849:	0f b6 00             	movzbl (%rax),%eax
 84c:	3c 39                	cmp    $0x39,%al
 84e:	7e c2                	jle    812 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 850:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 853:	5d                   	pop    %rbp
 854:	c3                   	retq   

0000000000000855 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 855:	55                   	push   %rbp
 856:	48 89 e5             	mov    %rsp,%rbp
 859:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 85d:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 861:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 864:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 868:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 86c:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 870:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 874:	eb 1d                	jmp    893 <memmove+0x3e>
    *dst++ = *src++;
 876:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 87a:	48 8d 50 01          	lea    0x1(%rax),%rdx
 87e:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 882:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 886:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 88a:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 88e:	0f b6 12             	movzbl (%rdx),%edx
 891:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 893:	8b 45 dc             	mov    -0x24(%rbp),%eax
 896:	8d 50 ff             	lea    -0x1(%rax),%edx
 899:	89 55 dc             	mov    %edx,-0x24(%rbp)
 89c:	85 c0                	test   %eax,%eax
 89e:	7f d6                	jg     876 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 8a0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8a4:	5d                   	pop    %rbp
 8a5:	c3                   	retq   

00000000000008a6 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 8a6:	b8 01 00 00 00       	mov    $0x1,%eax
 8ab:	cd 40                	int    $0x40
 8ad:	c3                   	retq   

00000000000008ae <exit>:
SYSCALL(exit)
 8ae:	b8 02 00 00 00       	mov    $0x2,%eax
 8b3:	cd 40                	int    $0x40
 8b5:	c3                   	retq   

00000000000008b6 <wait>:
SYSCALL(wait)
 8b6:	b8 03 00 00 00       	mov    $0x3,%eax
 8bb:	cd 40                	int    $0x40
 8bd:	c3                   	retq   

00000000000008be <pipe>:
SYSCALL(pipe)
 8be:	b8 04 00 00 00       	mov    $0x4,%eax
 8c3:	cd 40                	int    $0x40
 8c5:	c3                   	retq   

00000000000008c6 <read>:
SYSCALL(read)
 8c6:	b8 05 00 00 00       	mov    $0x5,%eax
 8cb:	cd 40                	int    $0x40
 8cd:	c3                   	retq   

00000000000008ce <write>:
SYSCALL(write)
 8ce:	b8 10 00 00 00       	mov    $0x10,%eax
 8d3:	cd 40                	int    $0x40
 8d5:	c3                   	retq   

00000000000008d6 <close>:
SYSCALL(close)
 8d6:	b8 15 00 00 00       	mov    $0x15,%eax
 8db:	cd 40                	int    $0x40
 8dd:	c3                   	retq   

00000000000008de <kill>:
SYSCALL(kill)
 8de:	b8 06 00 00 00       	mov    $0x6,%eax
 8e3:	cd 40                	int    $0x40
 8e5:	c3                   	retq   

00000000000008e6 <exec>:
SYSCALL(exec)
 8e6:	b8 07 00 00 00       	mov    $0x7,%eax
 8eb:	cd 40                	int    $0x40
 8ed:	c3                   	retq   

00000000000008ee <open>:
SYSCALL(open)
 8ee:	b8 0f 00 00 00       	mov    $0xf,%eax
 8f3:	cd 40                	int    $0x40
 8f5:	c3                   	retq   

00000000000008f6 <mknod>:
SYSCALL(mknod)
 8f6:	b8 11 00 00 00       	mov    $0x11,%eax
 8fb:	cd 40                	int    $0x40
 8fd:	c3                   	retq   

00000000000008fe <unlink>:
SYSCALL(unlink)
 8fe:	b8 12 00 00 00       	mov    $0x12,%eax
 903:	cd 40                	int    $0x40
 905:	c3                   	retq   

0000000000000906 <fstat>:
SYSCALL(fstat)
 906:	b8 08 00 00 00       	mov    $0x8,%eax
 90b:	cd 40                	int    $0x40
 90d:	c3                   	retq   

000000000000090e <link>:
SYSCALL(link)
 90e:	b8 13 00 00 00       	mov    $0x13,%eax
 913:	cd 40                	int    $0x40
 915:	c3                   	retq   

0000000000000916 <mkdir>:
SYSCALL(mkdir)
 916:	b8 14 00 00 00       	mov    $0x14,%eax
 91b:	cd 40                	int    $0x40
 91d:	c3                   	retq   

000000000000091e <chdir>:
SYSCALL(chdir)
 91e:	b8 09 00 00 00       	mov    $0x9,%eax
 923:	cd 40                	int    $0x40
 925:	c3                   	retq   

0000000000000926 <dup>:
SYSCALL(dup)
 926:	b8 0a 00 00 00       	mov    $0xa,%eax
 92b:	cd 40                	int    $0x40
 92d:	c3                   	retq   

000000000000092e <getpid>:
SYSCALL(getpid)
 92e:	b8 0b 00 00 00       	mov    $0xb,%eax
 933:	cd 40                	int    $0x40
 935:	c3                   	retq   

0000000000000936 <sbrk>:
SYSCALL(sbrk)
 936:	b8 0c 00 00 00       	mov    $0xc,%eax
 93b:	cd 40                	int    $0x40
 93d:	c3                   	retq   

000000000000093e <sleep>:
SYSCALL(sleep)
 93e:	b8 0d 00 00 00       	mov    $0xd,%eax
 943:	cd 40                	int    $0x40
 945:	c3                   	retq   

0000000000000946 <uptime>:
SYSCALL(uptime)
 946:	b8 0e 00 00 00       	mov    $0xe,%eax
 94b:	cd 40                	int    $0x40
 94d:	c3                   	retq   

000000000000094e <sysinfo>:
SYSCALL(sysinfo)
 94e:	b8 16 00 00 00       	mov    $0x16,%eax
 953:	cd 40                	int    $0x40
 955:	c3                   	retq   

0000000000000956 <crashn>:
SYSCALL(crashn)
 956:	b8 17 00 00 00       	mov    $0x17,%eax
 95b:	cd 40                	int    $0x40
 95d:	c3                   	retq   

000000000000095e <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 95e:	55                   	push   %rbp
 95f:	48 89 e5             	mov    %rsp,%rbp
 962:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 966:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 96a:	48 83 e8 10          	sub    $0x10,%rax
 96e:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 972:	48 8b 05 57 05 00 00 	mov    0x557(%rip),%rax        # ed0 <freep>
 979:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 97d:	eb 2f                	jmp    9ae <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 97f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 983:	48 8b 00             	mov    (%rax),%rax
 986:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 98a:	77 17                	ja     9a3 <free+0x45>
 98c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 990:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 994:	77 2f                	ja     9c5 <free+0x67>
 996:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 99a:	48 8b 00             	mov    (%rax),%rax
 99d:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 9a1:	77 22                	ja     9c5 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9a3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9a7:	48 8b 00             	mov    (%rax),%rax
 9aa:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 9ae:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9b2:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 9b6:	76 c7                	jbe    97f <free+0x21>
 9b8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9bc:	48 8b 00             	mov    (%rax),%rax
 9bf:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 9c3:	76 ba                	jbe    97f <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 9c5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9c9:	8b 40 08             	mov    0x8(%rax),%eax
 9cc:	89 c0                	mov    %eax,%eax
 9ce:	48 c1 e0 04          	shl    $0x4,%rax
 9d2:	48 89 c2             	mov    %rax,%rdx
 9d5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9d9:	48 01 c2             	add    %rax,%rdx
 9dc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9e0:	48 8b 00             	mov    (%rax),%rax
 9e3:	48 39 c2             	cmp    %rax,%rdx
 9e6:	75 2d                	jne    a15 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 9e8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ec:	8b 50 08             	mov    0x8(%rax),%edx
 9ef:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9f3:	48 8b 00             	mov    (%rax),%rax
 9f6:	8b 40 08             	mov    0x8(%rax),%eax
 9f9:	01 c2                	add    %eax,%edx
 9fb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ff:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 a02:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a06:	48 8b 00             	mov    (%rax),%rax
 a09:	48 8b 10             	mov    (%rax),%rdx
 a0c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a10:	48 89 10             	mov    %rdx,(%rax)
 a13:	eb 0e                	jmp    a23 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 a15:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a19:	48 8b 10             	mov    (%rax),%rdx
 a1c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a20:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 a23:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a27:	8b 40 08             	mov    0x8(%rax),%eax
 a2a:	89 c0                	mov    %eax,%eax
 a2c:	48 c1 e0 04          	shl    $0x4,%rax
 a30:	48 89 c2             	mov    %rax,%rdx
 a33:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a37:	48 01 d0             	add    %rdx,%rax
 a3a:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 a3e:	75 27                	jne    a67 <free+0x109>
    p->s.size += bp->s.size;
 a40:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a44:	8b 50 08             	mov    0x8(%rax),%edx
 a47:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a4b:	8b 40 08             	mov    0x8(%rax),%eax
 a4e:	01 c2                	add    %eax,%edx
 a50:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a54:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 a57:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a5b:	48 8b 10             	mov    (%rax),%rdx
 a5e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a62:	48 89 10             	mov    %rdx,(%rax)
 a65:	eb 0b                	jmp    a72 <free+0x114>
  } else
    p->s.ptr = bp;
 a67:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a6b:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 a6f:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 a72:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a76:	48 89 05 53 04 00 00 	mov    %rax,0x453(%rip)        # ed0 <freep>
}
 a7d:	90                   	nop
 a7e:	5d                   	pop    %rbp
 a7f:	c3                   	retq   

0000000000000a80 <morecore>:

static Header *morecore(uint nu) {
 a80:	55                   	push   %rbp
 a81:	48 89 e5             	mov    %rsp,%rbp
 a84:	48 83 ec 20          	sub    $0x20,%rsp
 a88:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 a8b:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 a92:	77 07                	ja     a9b <morecore+0x1b>
    nu = 4096;
 a94:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 a9b:	8b 45 ec             	mov    -0x14(%rbp),%eax
 a9e:	c1 e0 04             	shl    $0x4,%eax
 aa1:	89 c7                	mov    %eax,%edi
 aa3:	e8 8e fe ff ff       	callq  936 <sbrk>
 aa8:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 aac:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 ab1:	75 07                	jne    aba <morecore+0x3a>
    return 0;
 ab3:	b8 00 00 00 00       	mov    $0x0,%eax
 ab8:	eb 29                	jmp    ae3 <morecore+0x63>
  hp = (Header *)p;
 aba:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 abe:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 ac2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ac6:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ac9:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 acc:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ad0:	48 83 c0 10          	add    $0x10,%rax
 ad4:	48 89 c7             	mov    %rax,%rdi
 ad7:	e8 82 fe ff ff       	callq  95e <free>
  return freep;
 adc:	48 8b 05 ed 03 00 00 	mov    0x3ed(%rip),%rax        # ed0 <freep>
}
 ae3:	c9                   	leaveq 
 ae4:	c3                   	retq   

0000000000000ae5 <malloc>:

void *malloc(uint nbytes) {
 ae5:	55                   	push   %rbp
 ae6:	48 89 e5             	mov    %rsp,%rbp
 ae9:	48 83 ec 30          	sub    $0x30,%rsp
 aed:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 af0:	8b 45 dc             	mov    -0x24(%rbp),%eax
 af3:	48 83 c0 0f          	add    $0xf,%rax
 af7:	48 c1 e8 04          	shr    $0x4,%rax
 afb:	83 c0 01             	add    $0x1,%eax
 afe:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 b01:	48 8b 05 c8 03 00 00 	mov    0x3c8(%rip),%rax        # ed0 <freep>
 b08:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b0c:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 b11:	75 2b                	jne    b3e <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 b13:	48 c7 45 f0 c0 0e 00 	movq   $0xec0,-0x10(%rbp)
 b1a:	00 
 b1b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b1f:	48 89 05 aa 03 00 00 	mov    %rax,0x3aa(%rip)        # ed0 <freep>
 b26:	48 8b 05 a3 03 00 00 	mov    0x3a3(%rip),%rax        # ed0 <freep>
 b2d:	48 89 05 8c 03 00 00 	mov    %rax,0x38c(%rip)        # ec0 <base>
    base.s.size = 0;
 b34:	c7 05 8a 03 00 00 00 	movl   $0x0,0x38a(%rip)        # ec8 <base+0x8>
 b3b:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b3e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b42:	48 8b 00             	mov    (%rax),%rax
 b45:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 b49:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b4d:	8b 40 08             	mov    0x8(%rax),%eax
 b50:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 b53:	72 5f                	jb     bb4 <malloc+0xcf>
      if (p->s.size == nunits)
 b55:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b59:	8b 40 08             	mov    0x8(%rax),%eax
 b5c:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 b5f:	75 10                	jne    b71 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 b61:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b65:	48 8b 10             	mov    (%rax),%rdx
 b68:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b6c:	48 89 10             	mov    %rdx,(%rax)
 b6f:	eb 2e                	jmp    b9f <malloc+0xba>
      else {
        p->s.size -= nunits;
 b71:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b75:	8b 40 08             	mov    0x8(%rax),%eax
 b78:	2b 45 ec             	sub    -0x14(%rbp),%eax
 b7b:	89 c2                	mov    %eax,%edx
 b7d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b81:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 b84:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b88:	8b 40 08             	mov    0x8(%rax),%eax
 b8b:	89 c0                	mov    %eax,%eax
 b8d:	48 c1 e0 04          	shl    $0x4,%rax
 b91:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 b95:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b99:	8b 55 ec             	mov    -0x14(%rbp),%edx
 b9c:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 b9f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ba3:	48 89 05 26 03 00 00 	mov    %rax,0x326(%rip)        # ed0 <freep>
      return (void *)(p + 1);
 baa:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bae:	48 83 c0 10          	add    $0x10,%rax
 bb2:	eb 41                	jmp    bf5 <malloc+0x110>
    }
    if (p == freep)
 bb4:	48 8b 05 15 03 00 00 	mov    0x315(%rip),%rax        # ed0 <freep>
 bbb:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 bbf:	75 1c                	jne    bdd <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 bc1:	8b 45 ec             	mov    -0x14(%rbp),%eax
 bc4:	89 c7                	mov    %eax,%edi
 bc6:	e8 b5 fe ff ff       	callq  a80 <morecore>
 bcb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 bcf:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 bd4:	75 07                	jne    bdd <malloc+0xf8>
        return 0;
 bd6:	b8 00 00 00 00       	mov    $0x0,%eax
 bdb:	eb 18                	jmp    bf5 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 bdd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 be1:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 be5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 be9:	48 8b 00             	mov    (%rax),%rax
 bec:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 bf0:	e9 54 ff ff ff       	jmpq   b49 <malloc+0x64>
 bf5:	c9                   	leaveq 
 bf6:	c3                   	retq   
