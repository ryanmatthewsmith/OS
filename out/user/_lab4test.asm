
out/user/_lab4test:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <swaptest>:
  } while (0)

#define START_PAGES (600)
#define SWAP_TEST_PAGES (START_PAGES * 2)

void swaptest(void) {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
       4:	48 83 ec 60          	sub    $0x60,%rsp
  char *start = sbrk(0);
       8:	bf 00 00 00 00       	mov    $0x0,%edi
       d:	e8 72 0d 00 00       	callq  d84 <sbrk>
      12:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  char *a;
  int i;
  int b = 4096;
      16:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  int num_pages_to_alloc = SWAP_TEST_PAGES;
      1d:	c7 45 e8 b0 04 00 00 	movl   $0x4b0,-0x18(%rbp)
  struct sys_info info1, info2, info3;

  if (!fork()) {
      24:	e8 cb 0c 00 00       	callq  cf4 <fork>
      29:	85 c0                	test   %eax,%eax
      2b:	0f 85 f0 01 00 00    	jne    221 <swaptest+0x221>
    for (i = 0; i < num_pages_to_alloc; i++) {
      31:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
      38:	e9 8c 00 00 00       	jmpq   c9 <swaptest+0xc9>
      a = sbrk(b);
      3d:	8b 45 ec             	mov    -0x14(%rbp),%eax
      40:	89 c7                	mov    %eax,%edi
      42:	e8 3d 0d 00 00       	callq  d84 <sbrk>
      47:	48 89 45 e0          	mov    %rax,-0x20(%rbp)
      if (a == (char *)-1) {
      4b:	48 83 7d e0 ff       	cmpq   $0xffffffffffffffff,-0x20(%rbp)
      50:	75 19                	jne    6b <swaptest+0x6b>
        printf(stdout, "no more memory\n");
      52:	8b 05 88 14 00 00    	mov    0x1488(%rip),%eax        # 14e0 <stdout>
      58:	be 48 10 00 00       	mov    $0x1048,%esi
      5d:	89 c7                	mov    %eax,%edi
      5f:	b8 00 00 00 00       	mov    $0x0,%eax
      64:	e8 f0 06 00 00       	callq  759 <printf>
        break;
      69:	eb 6a                	jmp    d5 <swaptest+0xd5>
      }
      memset(a, 0, b);
      6b:	8b 55 ec             	mov    -0x14(%rbp),%edx
      6e:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
      72:	be 00 00 00 00       	mov    $0x0,%esi
      77:	48 89 c7             	mov    %rax,%rdi
      7a:	e8 94 0a 00 00       	callq  b13 <memset>
      *(int *)a = i;
      7f:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
      83:	8b 55 fc             	mov    -0x4(%rbp),%edx
      86:	89 10                	mov    %edx,(%rax)
      if (i % 100 == 0)
      88:	8b 4d fc             	mov    -0x4(%rbp),%ecx
      8b:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
      90:	89 c8                	mov    %ecx,%eax
      92:	f7 ea                	imul   %edx
      94:	c1 fa 05             	sar    $0x5,%edx
      97:	89 c8                	mov    %ecx,%eax
      99:	c1 f8 1f             	sar    $0x1f,%eax
      9c:	29 c2                	sub    %eax,%edx
      9e:	89 d0                	mov    %edx,%eax
      a0:	6b c0 64             	imul   $0x64,%eax,%eax
      a3:	29 c1                	sub    %eax,%ecx
      a5:	89 c8                	mov    %ecx,%eax
      a7:	85 c0                	test   %eax,%eax
      a9:	75 1a                	jne    c5 <swaptest+0xc5>
        printf(stdout, "%d pages allocated\n", i);
      ab:	8b 05 2f 14 00 00    	mov    0x142f(%rip),%eax        # 14e0 <stdout>
      b1:	8b 55 fc             	mov    -0x4(%rbp),%edx
      b4:	be 58 10 00 00       	mov    $0x1058,%esi
      b9:	89 c7                	mov    %eax,%edi
      bb:	b8 00 00 00 00       	mov    $0x0,%eax
      c0:	e8 94 06 00 00       	callq  759 <printf>
  int b = 4096;
  int num_pages_to_alloc = SWAP_TEST_PAGES;
  struct sys_info info1, info2, info3;

  if (!fork()) {
    for (i = 0; i < num_pages_to_alloc; i++) {
      c5:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
      c9:	8b 45 fc             	mov    -0x4(%rbp),%eax
      cc:	3b 45 e8             	cmp    -0x18(%rbp),%eax
      cf:	0f 8c 68 ff ff ff    	jl     3d <swaptest+0x3d>
      *(int *)a = i;
      if (i % 100 == 0)
        printf(stdout, "%d pages allocated\n", i);
    }

    sysinfo(&info1);
      d5:	48 8d 45 cc          	lea    -0x34(%rbp),%rax
      d9:	48 89 c7             	mov    %rax,%rdi
      dc:	e8 bb 0c 00 00       	callq  d9c <sysinfo>

    // check whether memory data is consistent
    for (i = 0; i < num_pages_to_alloc; i++) {
      e1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
      e8:	e9 bb 00 00 00       	jmpq   1a8 <swaptest+0x1a8>
      if (i % 100 == 0)
      ed:	8b 4d fc             	mov    -0x4(%rbp),%ecx
      f0:	ba 1f 85 eb 51       	mov    $0x51eb851f,%edx
      f5:	89 c8                	mov    %ecx,%eax
      f7:	f7 ea                	imul   %edx
      f9:	c1 fa 05             	sar    $0x5,%edx
      fc:	89 c8                	mov    %ecx,%eax
      fe:	c1 f8 1f             	sar    $0x1f,%eax
     101:	29 c2                	sub    %eax,%edx
     103:	89 d0                	mov    %edx,%eax
     105:	6b c0 64             	imul   $0x64,%eax,%eax
     108:	29 c1                	sub    %eax,%ecx
     10a:	89 c8                	mov    %ecx,%eax
     10c:	85 c0                	test   %eax,%eax
     10e:	75 1a                	jne    12a <swaptest+0x12a>
        printf(stdout, "checking i %d\n", i);
     110:	8b 05 ca 13 00 00    	mov    0x13ca(%rip),%eax        # 14e0 <stdout>
     116:	8b 55 fc             	mov    -0x4(%rbp),%edx
     119:	be 6c 10 00 00       	mov    $0x106c,%esi
     11e:	89 c7                	mov    %eax,%edi
     120:	b8 00 00 00 00       	mov    $0x0,%eax
     125:	e8 2f 06 00 00       	callq  759 <printf>
      if (*(int *)(start + i * b) != i) {
     12a:	8b 45 fc             	mov    -0x4(%rbp),%eax
     12d:	0f af 45 ec          	imul   -0x14(%rbp),%eax
     131:	48 63 d0             	movslq %eax,%rdx
     134:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     138:	48 01 d0             	add    %rdx,%rax
     13b:	8b 00                	mov    (%rax),%eax
     13d:	3b 45 fc             	cmp    -0x4(%rbp),%eax
     140:	74 62                	je     1a4 <swaptest+0x1a4>
        error("data is incorrect, should be %d, but %d\n", i,
     142:	8b 05 98 13 00 00    	mov    0x1398(%rip),%eax        # 14e0 <stdout>
     148:	ba 34 00 00 00       	mov    $0x34,%edx
     14d:	be 7b 10 00 00       	mov    $0x107b,%esi
     152:	89 c7                	mov    %eax,%edi
     154:	b8 00 00 00 00       	mov    $0x0,%eax
     159:	e8 fb 05 00 00       	callq  759 <printf>
     15e:	8b 45 fc             	mov    -0x4(%rbp),%eax
     161:	0f af 45 ec          	imul   -0x14(%rbp),%eax
     165:	48 63 d0             	movslq %eax,%rdx
     168:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     16c:	48 01 d0             	add    %rdx,%rax
     16f:	8b 08                	mov    (%rax),%ecx
     171:	8b 05 69 13 00 00    	mov    0x1369(%rip),%eax        # 14e0 <stdout>
     177:	8b 55 fc             	mov    -0x4(%rbp),%edx
     17a:	be 90 10 00 00       	mov    $0x1090,%esi
     17f:	89 c7                	mov    %eax,%edi
     181:	b8 00 00 00 00       	mov    $0x0,%eax
     186:	e8 ce 05 00 00       	callq  759 <printf>
     18b:	8b 05 4f 13 00 00    	mov    0x134f(%rip),%eax        # 14e0 <stdout>
     191:	be b9 10 00 00       	mov    $0x10b9,%esi
     196:	89 c7                	mov    %eax,%edi
     198:	b8 00 00 00 00       	mov    $0x0,%eax
     19d:	e8 b7 05 00 00       	callq  759 <printf>
     1a2:	eb fe                	jmp    1a2 <swaptest+0x1a2>
    }

    sysinfo(&info1);

    // check whether memory data is consistent
    for (i = 0; i < num_pages_to_alloc; i++) {
     1a4:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     1a8:	8b 45 fc             	mov    -0x4(%rbp),%eax
     1ab:	3b 45 e8             	cmp    -0x18(%rbp),%eax
     1ae:	0f 8c 39 ff ff ff    	jl     ed <swaptest+0xed>
        error("data is incorrect, should be %d, but %d\n", i,
              *(int *)(start + i * b));
      }
    }

    sysinfo(&info2);
     1b4:	48 8d 45 b8          	lea    -0x48(%rbp),%rax
     1b8:	48 89 c7             	mov    %rax,%rdi
     1bb:	e8 dc 0b 00 00       	callq  d9c <sysinfo>

    printf(stdout, "number of disk reads = %d\n",
           info2.num_disk_reads - info1.num_disk_reads);
     1c0:	8b 55 c8             	mov    -0x38(%rbp),%edx
     1c3:	8b 45 dc             	mov    -0x24(%rbp),%eax
      }
    }

    sysinfo(&info2);

    printf(stdout, "number of disk reads = %d\n",
     1c6:	29 c2                	sub    %eax,%edx
     1c8:	8b 05 12 13 00 00    	mov    0x1312(%rip),%eax        # 14e0 <stdout>
     1ce:	be bb 10 00 00       	mov    $0x10bb,%esi
     1d3:	89 c7                	mov    %eax,%edi
     1d5:	b8 00 00 00 00       	mov    $0x0,%eax
     1da:	e8 7a 05 00 00       	callq  759 <printf>
           info2.num_disk_reads - info1.num_disk_reads);

    sysinfo(&info3);
     1df:	48 8d 45 a4          	lea    -0x5c(%rbp),%rax
     1e3:	48 89 c7             	mov    %rax,%rdi
     1e6:	e8 b1 0b 00 00       	callq  d9c <sysinfo>
    printf(stdout, "number of pages in swap = %d\n", info3.pages_in_swap);
     1eb:	8b 55 a8             	mov    -0x58(%rbp),%edx
     1ee:	8b 05 ec 12 00 00    	mov    0x12ec(%rip),%eax        # 14e0 <stdout>
     1f4:	be d6 10 00 00       	mov    $0x10d6,%esi
     1f9:	89 c7                	mov    %eax,%edi
     1fb:	b8 00 00 00 00       	mov    $0x0,%eax
     200:	e8 54 05 00 00       	callq  759 <printf>

    printf(stdout, "swaptest OK\n");
     205:	8b 05 d5 12 00 00    	mov    0x12d5(%rip),%eax        # 14e0 <stdout>
     20b:	be f4 10 00 00       	mov    $0x10f4,%esi
     210:	89 c7                	mov    %eax,%edi
     212:	b8 00 00 00 00       	mov    $0x0,%eax
     217:	e8 3d 05 00 00       	callq  759 <printf>
    exit();
     21c:	e8 db 0a 00 00       	callq  cfc <exit>
  } else {
    wait();
     221:	e8 de 0a 00 00       	callq  d04 <wait>
  }
}
     226:	90                   	nop
     227:	c9                   	leaveq 
     228:	c3                   	retq   

0000000000000229 <localitytest>:

void localitytest(void) {
     229:	55                   	push   %rbp
     22a:	48 89 e5             	mov    %rsp,%rbp
     22d:	48 81 ec 90 00 00 00 	sub    $0x90,%rsp
  char *start = sbrk(0);
     234:	bf 00 00 00 00       	mov    $0x0,%edi
     239:	e8 46 0b 00 00       	callq  d84 <sbrk>
     23e:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  char *a;
  int i, j, k;
  int b = 4096;
     242:	c7 45 e4 00 10 00 00 	movl   $0x1000,-0x1c(%rbp)
  int groups = 6;
     249:	c7 45 e0 06 00 00 00 	movl   $0x6,-0x20(%rbp)
  int pages_per_group = SWAP_TEST_PAGES / groups;
     250:	b8 b0 04 00 00       	mov    $0x4b0,%eax
     255:	99                   	cltd   
     256:	f7 7d e0             	idivl  -0x20(%rbp)
     259:	89 45 dc             	mov    %eax,-0x24(%rbp)
  struct sys_info info1, info2, curinfo, previnfo;

  sysinfo(&info1);
     25c:	48 8d 45 b4          	lea    -0x4c(%rbp),%rax
     260:	48 89 c7             	mov    %rax,%rdi
     263:	e8 34 0b 00 00       	callq  d9c <sysinfo>
  for (i = 0; i < groups * pages_per_group; i++) {
     268:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     26f:	eb 2f                	jmp    2a0 <localitytest+0x77>
    a = sbrk(b);
     271:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     274:	89 c7                	mov    %eax,%edi
     276:	e8 09 0b 00 00       	callq  d84 <sbrk>
     27b:	48 89 45 d0          	mov    %rax,-0x30(%rbp)
    memset(a, 0, b);
     27f:	8b 55 e4             	mov    -0x1c(%rbp),%edx
     282:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     286:	be 00 00 00 00       	mov    $0x0,%esi
     28b:	48 89 c7             	mov    %rax,%rdi
     28e:	e8 80 08 00 00       	callq  b13 <memset>
    *(int *)a = i;
     293:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     297:	8b 55 fc             	mov    -0x4(%rbp),%edx
     29a:	89 10                	mov    %edx,(%rax)
  int groups = 6;
  int pages_per_group = SWAP_TEST_PAGES / groups;
  struct sys_info info1, info2, curinfo, previnfo;

  sysinfo(&info1);
  for (i = 0; i < groups * pages_per_group; i++) {
     29c:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     2a0:	8b 45 e0             	mov    -0x20(%rbp),%eax
     2a3:	0f af 45 dc          	imul   -0x24(%rbp),%eax
     2a7:	3b 45 fc             	cmp    -0x4(%rbp),%eax
     2aa:	7f c5                	jg     271 <localitytest+0x48>
    a = sbrk(b);
    memset(a, 0, b);
    *(int *)a = i;
  }

  printf(stdout, "%d pages allocated\n", groups * pages_per_group);
     2ac:	8b 45 e0             	mov    -0x20(%rbp),%eax
     2af:	0f af 45 dc          	imul   -0x24(%rbp),%eax
     2b3:	89 c2                	mov    %eax,%edx
     2b5:	8b 05 25 12 00 00    	mov    0x1225(%rip),%eax        # 14e0 <stdout>
     2bb:	be 58 10 00 00       	mov    $0x1058,%esi
     2c0:	89 c7                	mov    %eax,%edi
     2c2:	b8 00 00 00 00       	mov    $0x0,%eax
     2c7:	e8 8d 04 00 00       	callq  759 <printf>

  sysinfo(&info1);
     2cc:	48 8d 45 b4          	lea    -0x4c(%rbp),%rax
     2d0:	48 89 c7             	mov    %rax,%rdi
     2d3:	e8 c4 0a 00 00       	callq  d9c <sysinfo>
  printf(stdout, "number of pages in swap = %d\n", info1.pages_in_swap);
     2d8:	8b 55 b8             	mov    -0x48(%rbp),%edx
     2db:	8b 05 ff 11 00 00    	mov    0x11ff(%rip),%eax        # 14e0 <stdout>
     2e1:	be d6 10 00 00       	mov    $0x10d6,%esi
     2e6:	89 c7                	mov    %eax,%edi
     2e8:	b8 00 00 00 00       	mov    $0x0,%eax
     2ed:	e8 67 04 00 00       	callq  759 <printf>

  int reads = 0;
     2f2:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%rbp)
  int prevreads;
  int curdiskreads;
  // test whether a rough approximation of LRU is implemented
  for (i = 0; i < groups; i++) {
     2f9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     300:	e9 d6 01 00 00       	jmpq   4db <localitytest+0x2b2>
    sysinfo(&previnfo);
     305:	48 8d 85 78 ff ff ff 	lea    -0x88(%rbp),%rax
     30c:	48 89 c7             	mov    %rax,%rdi
     30f:	e8 88 0a 00 00       	callq  d9c <sysinfo>
    prevreads = reads;
     314:	8b 45 f0             	mov    -0x10(%rbp),%eax
     317:	89 45 cc             	mov    %eax,-0x34(%rbp)
    for (j = groups - 1; j >= i; j--) {
     31a:	8b 45 e0             	mov    -0x20(%rbp),%eax
     31d:	83 e8 01             	sub    $0x1,%eax
     320:	89 45 f8             	mov    %eax,-0x8(%rbp)
     323:	e9 a0 00 00 00       	jmpq   3c8 <localitytest+0x19f>
      for (k = pages_per_group - 1; k >= 0; k--) {
     328:	8b 45 dc             	mov    -0x24(%rbp),%eax
     32b:	83 e8 01             	sub    $0x1,%eax
     32e:	89 45 f4             	mov    %eax,-0xc(%rbp)
     331:	e9 84 00 00 00       	jmpq   3ba <localitytest+0x191>
        reads++;
     336:	83 45 f0 01          	addl   $0x1,-0x10(%rbp)
        if (*(int *)(start + (j * pages_per_group + k) * b) !=
     33a:	8b 45 f8             	mov    -0x8(%rbp),%eax
     33d:	0f af 45 dc          	imul   -0x24(%rbp),%eax
     341:	89 c2                	mov    %eax,%edx
     343:	8b 45 f4             	mov    -0xc(%rbp),%eax
     346:	01 d0                	add    %edx,%eax
     348:	0f af 45 e4          	imul   -0x1c(%rbp),%eax
     34c:	48 63 d0             	movslq %eax,%rdx
     34f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     353:	48 01 d0             	add    %rdx,%rax
     356:	8b 10                	mov    (%rax),%edx
            j * pages_per_group + k) {
     358:	8b 45 f8             	mov    -0x8(%rbp),%eax
     35b:	0f af 45 dc          	imul   -0x24(%rbp),%eax
     35f:	89 c1                	mov    %eax,%ecx
     361:	8b 45 f4             	mov    -0xc(%rbp),%eax
     364:	01 c8                	add    %ecx,%eax
    sysinfo(&previnfo);
    prevreads = reads;
    for (j = groups - 1; j >= i; j--) {
      for (k = pages_per_group - 1; k >= 0; k--) {
        reads++;
        if (*(int *)(start + (j * pages_per_group + k) * b) !=
     366:	39 c2                	cmp    %eax,%edx
     368:	74 4c                	je     3b6 <localitytest+0x18d>
            j * pages_per_group + k) {
          error("data is incorrect");
     36a:	8b 05 70 11 00 00    	mov    0x1170(%rip),%eax        # 14e0 <stdout>
     370:	ba 68 00 00 00       	mov    $0x68,%edx
     375:	be 7b 10 00 00       	mov    $0x107b,%esi
     37a:	89 c7                	mov    %eax,%edi
     37c:	b8 00 00 00 00       	mov    $0x0,%eax
     381:	e8 d3 03 00 00       	callq  759 <printf>
     386:	8b 05 54 11 00 00    	mov    0x1154(%rip),%eax        # 14e0 <stdout>
     38c:	be 01 11 00 00       	mov    $0x1101,%esi
     391:	89 c7                	mov    %eax,%edi
     393:	b8 00 00 00 00       	mov    $0x0,%eax
     398:	e8 bc 03 00 00       	callq  759 <printf>
     39d:	8b 05 3d 11 00 00    	mov    0x113d(%rip),%eax        # 14e0 <stdout>
     3a3:	be b9 10 00 00       	mov    $0x10b9,%esi
     3a8:	89 c7                	mov    %eax,%edi
     3aa:	b8 00 00 00 00       	mov    $0x0,%eax
     3af:	e8 a5 03 00 00       	callq  759 <printf>
     3b4:	eb fe                	jmp    3b4 <localitytest+0x18b>
  // test whether a rough approximation of LRU is implemented
  for (i = 0; i < groups; i++) {
    sysinfo(&previnfo);
    prevreads = reads;
    for (j = groups - 1; j >= i; j--) {
      for (k = pages_per_group - 1; k >= 0; k--) {
     3b6:	83 6d f4 01          	subl   $0x1,-0xc(%rbp)
     3ba:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     3be:	0f 89 72 ff ff ff    	jns    336 <localitytest+0x10d>
  int curdiskreads;
  // test whether a rough approximation of LRU is implemented
  for (i = 0; i < groups; i++) {
    sysinfo(&previnfo);
    prevreads = reads;
    for (j = groups - 1; j >= i; j--) {
     3c4:	83 6d f8 01          	subl   $0x1,-0x8(%rbp)
     3c8:	8b 45 f8             	mov    -0x8(%rbp),%eax
     3cb:	3b 45 fc             	cmp    -0x4(%rbp),%eax
     3ce:	0f 8d 54 ff ff ff    	jge    328 <localitytest+0xff>
            j * pages_per_group + k) {
          error("data is incorrect");
        }
      }
    }
    sysinfo(&curinfo);
     3d4:	48 8d 45 8c          	lea    -0x74(%rbp),%rax
     3d8:	48 89 c7             	mov    %rax,%rdi
     3db:	e8 bc 09 00 00       	callq  d9c <sysinfo>
    curdiskreads = curinfo.num_disk_reads - previnfo.num_disk_reads;
     3e0:	8b 55 9c             	mov    -0x64(%rbp),%edx
     3e3:	8b 45 88             	mov    -0x78(%rbp),%eax
     3e6:	29 c2                	sub    %eax,%edx
     3e8:	89 d0                	mov    %edx,%eax
     3ea:	89 45 c8             	mov    %eax,-0x38(%rbp)
    if (i == 0) {
     3ed:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     3f1:	75 5c                	jne    44f <localitytest+0x226>
      // On the first iteration, we should only incur about half the page faults
      // (the lower portion of the groups should be in swap)
      if (curdiskreads >= (SWAP_TEST_PAGES - 2 * pages_per_group) * 8 * 2)
     3f3:	b8 58 02 00 00       	mov    $0x258,%eax
     3f8:	2b 45 dc             	sub    -0x24(%rbp),%eax
     3fb:	c1 e0 05             	shl    $0x5,%eax
     3fe:	3b 45 c8             	cmp    -0x38(%rbp),%eax
     401:	7f 4c                	jg     44f <localitytest+0x226>
        error("On first iteration, there should have been fewer disk reads");
     403:	8b 05 d7 10 00 00    	mov    0x10d7(%rip),%eax        # 14e0 <stdout>
     409:	ba 72 00 00 00       	mov    $0x72,%edx
     40e:	be 7b 10 00 00       	mov    $0x107b,%esi
     413:	89 c7                	mov    %eax,%edi
     415:	b8 00 00 00 00       	mov    $0x0,%eax
     41a:	e8 3a 03 00 00       	callq  759 <printf>
     41f:	8b 05 bb 10 00 00    	mov    0x10bb(%rip),%eax        # 14e0 <stdout>
     425:	be 18 11 00 00       	mov    $0x1118,%esi
     42a:	89 c7                	mov    %eax,%edi
     42c:	b8 00 00 00 00       	mov    $0x0,%eax
     431:	e8 23 03 00 00       	callq  759 <printf>
     436:	8b 05 a4 10 00 00    	mov    0x10a4(%rip),%eax        # 14e0 <stdout>
     43c:	be b9 10 00 00       	mov    $0x10b9,%esi
     441:	89 c7                	mov    %eax,%edi
     443:	b8 00 00 00 00       	mov    $0x0,%eax
     448:	e8 0c 03 00 00       	callq  759 <printf>
     44d:	eb fe                	jmp    44d <localitytest+0x224>
    }
    if (i == groups-1) {
     44f:	8b 45 e0             	mov    -0x20(%rbp),%eax
     452:	83 e8 01             	sub    $0x1,%eax
     455:	3b 45 fc             	cmp    -0x4(%rbp),%eax
     458:	75 52                	jne    4ac <localitytest+0x283>
      // There should be no disk reads on the last one as the other groups have
      // been well least recently used
      if (curdiskreads != 0)
     45a:	83 7d c8 00          	cmpl   $0x0,-0x38(%rbp)
     45e:	74 4c                	je     4ac <localitytest+0x283>
        error("The pages accessed on the last iteration were not all resident in memory");
     460:	8b 05 7a 10 00 00    	mov    0x107a(%rip),%eax        # 14e0 <stdout>
     466:	ba 78 00 00 00       	mov    $0x78,%edx
     46b:	be 7b 10 00 00       	mov    $0x107b,%esi
     470:	89 c7                	mov    %eax,%edi
     472:	b8 00 00 00 00       	mov    $0x0,%eax
     477:	e8 dd 02 00 00       	callq  759 <printf>
     47c:	8b 05 5e 10 00 00    	mov    0x105e(%rip),%eax        # 14e0 <stdout>
     482:	be 58 11 00 00       	mov    $0x1158,%esi
     487:	89 c7                	mov    %eax,%edi
     489:	b8 00 00 00 00       	mov    $0x0,%eax
     48e:	e8 c6 02 00 00       	callq  759 <printf>
     493:	8b 05 47 10 00 00    	mov    0x1047(%rip),%eax        # 14e0 <stdout>
     499:	be b9 10 00 00       	mov    $0x10b9,%esi
     49e:	89 c7                	mov    %eax,%edi
     4a0:	b8 00 00 00 00       	mov    $0x0,%eax
     4a5:	e8 af 02 00 00       	callq  759 <printf>
     4aa:	eb fe                	jmp    4aa <localitytest+0x281>
    }
    printf(1, "iteration %d, total disk reads %d, this iteration disk reads %d\n",
        i, curinfo.num_disk_reads, curinfo.num_disk_reads - previnfo.num_disk_reads);
     4ac:	8b 55 9c             	mov    -0x64(%rbp),%edx
     4af:	8b 45 88             	mov    -0x78(%rbp),%eax
      // There should be no disk reads on the last one as the other groups have
      // been well least recently used
      if (curdiskreads != 0)
        error("The pages accessed on the last iteration were not all resident in memory");
    }
    printf(1, "iteration %d, total disk reads %d, this iteration disk reads %d\n",
     4b2:	89 d1                	mov    %edx,%ecx
     4b4:	29 c1                	sub    %eax,%ecx
     4b6:	8b 55 9c             	mov    -0x64(%rbp),%edx
     4b9:	8b 45 fc             	mov    -0x4(%rbp),%eax
     4bc:	41 89 c8             	mov    %ecx,%r8d
     4bf:	89 d1                	mov    %edx,%ecx
     4c1:	89 c2                	mov    %eax,%edx
     4c3:	be a8 11 00 00       	mov    $0x11a8,%esi
     4c8:	bf 01 00 00 00       	mov    $0x1,%edi
     4cd:	b8 00 00 00 00       	mov    $0x0,%eax
     4d2:	e8 82 02 00 00       	callq  759 <printf>

  int reads = 0;
  int prevreads;
  int curdiskreads;
  // test whether a rough approximation of LRU is implemented
  for (i = 0; i < groups; i++) {
     4d7:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     4db:	8b 45 fc             	mov    -0x4(%rbp),%eax
     4de:	3b 45 e0             	cmp    -0x20(%rbp),%eax
     4e1:	0f 8c 1e fe ff ff    	jl     305 <localitytest+0xdc>

  // we set threshold to be 57600 so any LRU-like implementation can pass our
  // test


  sysinfo(&info2);
     4e7:	48 8d 45 a0          	lea    -0x60(%rbp),%rax
     4eb:	48 89 c7             	mov    %rax,%rdi
     4ee:	e8 a9 08 00 00       	callq  d9c <sysinfo>

  if (info2.num_disk_reads - info1.num_disk_reads > 60000)
     4f3:	8b 55 b0             	mov    -0x50(%rbp),%edx
     4f6:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     4f9:	29 c2                	sub    %eax,%edx
     4fb:	89 d0                	mov    %edx,%eax
     4fd:	3d 60 ea 00 00       	cmp    $0xea60,%eax
     502:	7e 4c                	jle    550 <localitytest+0x327>
    error("LRU function incurs too many swaps.");
     504:	8b 05 d6 0f 00 00    	mov    0xfd6(%rip),%eax        # 14e0 <stdout>
     50a:	ba 8f 00 00 00       	mov    $0x8f,%edx
     50f:	be 7b 10 00 00       	mov    $0x107b,%esi
     514:	89 c7                	mov    %eax,%edi
     516:	b8 00 00 00 00       	mov    $0x0,%eax
     51b:	e8 39 02 00 00       	callq  759 <printf>
     520:	8b 05 ba 0f 00 00    	mov    0xfba(%rip),%eax        # 14e0 <stdout>
     526:	be f0 11 00 00       	mov    $0x11f0,%esi
     52b:	89 c7                	mov    %eax,%edi
     52d:	b8 00 00 00 00       	mov    $0x0,%eax
     532:	e8 22 02 00 00       	callq  759 <printf>
     537:	8b 05 a3 0f 00 00    	mov    0xfa3(%rip),%eax        # 14e0 <stdout>
     53d:	be b9 10 00 00       	mov    $0x10b9,%esi
     542:	89 c7                	mov    %eax,%edi
     544:	b8 00 00 00 00       	mov    $0x0,%eax
     549:	e8 0b 02 00 00       	callq  759 <printf>
     54e:	eb fe                	jmp    54e <localitytest+0x325>

  printf(stdout, "localitytest OK\n");
     550:	8b 05 8a 0f 00 00    	mov    0xf8a(%rip),%eax        # 14e0 <stdout>
     556:	be 14 12 00 00       	mov    $0x1214,%esi
     55b:	89 c7                	mov    %eax,%edi
     55d:	b8 00 00 00 00       	mov    $0x0,%eax
     562:	e8 f2 01 00 00       	callq  759 <printf>
}
     567:	90                   	nop
     568:	c9                   	leaveq 
     569:	c3                   	retq   

000000000000056a <main>:

int main(int argc, char *argv[]) {
     56a:	55                   	push   %rbp
     56b:	48 89 e5             	mov    %rsp,%rbp
     56e:	48 83 ec 10          	sub    $0x10,%rsp
     572:	89 7d fc             	mov    %edi,-0x4(%rbp)
     575:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  swaptest();
     579:	e8 82 fa ff ff       	callq  0 <swaptest>
  localitytest();
     57e:	e8 a6 fc ff ff       	callq  229 <localitytest>
  printf(stdout, "lab4 tests passed!!\n");
     583:	8b 05 57 0f 00 00    	mov    0xf57(%rip),%eax        # 14e0 <stdout>
     589:	be 25 12 00 00       	mov    $0x1225,%esi
     58e:	89 c7                	mov    %eax,%edi
     590:	b8 00 00 00 00       	mov    $0x0,%eax
     595:	e8 bf 01 00 00       	callq  759 <printf>
  exit();
     59a:	e8 5d 07 00 00       	callq  cfc <exit>

000000000000059f <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
     59f:	55                   	push   %rbp
     5a0:	48 89 e5             	mov    %rsp,%rbp
     5a3:	48 83 ec 10          	sub    $0x10,%rsp
     5a7:	89 7d fc             	mov    %edi,-0x4(%rbp)
     5aa:	89 f0                	mov    %esi,%eax
     5ac:	88 45 f8             	mov    %al,-0x8(%rbp)
     5af:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
     5b3:	8b 45 fc             	mov    -0x4(%rbp),%eax
     5b6:	ba 01 00 00 00       	mov    $0x1,%edx
     5bb:	48 89 ce             	mov    %rcx,%rsi
     5be:	89 c7                	mov    %eax,%edi
     5c0:	e8 57 07 00 00       	callq  d1c <write>
     5c5:	90                   	nop
     5c6:	c9                   	leaveq 
     5c7:	c3                   	retq   

00000000000005c8 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
     5c8:	55                   	push   %rbp
     5c9:	48 89 e5             	mov    %rsp,%rbp
     5cc:	48 83 ec 40          	sub    $0x40,%rsp
     5d0:	89 7d cc             	mov    %edi,-0x34(%rbp)
     5d3:	89 75 c8             	mov    %esi,-0x38(%rbp)
     5d6:	89 55 c4             	mov    %edx,-0x3c(%rbp)
     5d9:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
     5dc:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     5e0:	74 1f                	je     601 <printint64+0x39>
     5e2:	8b 45 c8             	mov    -0x38(%rbp),%eax
     5e5:	c1 e8 1f             	shr    $0x1f,%eax
     5e8:	0f b6 c0             	movzbl %al,%eax
     5eb:	89 45 c0             	mov    %eax,-0x40(%rbp)
     5ee:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     5f2:	74 0d                	je     601 <printint64+0x39>
    x = -xx;
     5f4:	8b 45 c8             	mov    -0x38(%rbp),%eax
     5f7:	f7 d8                	neg    %eax
     5f9:	48 98                	cltq   
     5fb:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     5ff:	eb 09                	jmp    60a <printint64+0x42>
  else
    x = xx;
     601:	8b 45 c8             	mov    -0x38(%rbp),%eax
     604:	48 98                	cltq   
     606:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
     60a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     611:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     614:	8d 41 01             	lea    0x1(%rcx),%eax
     617:	89 45 fc             	mov    %eax,-0x4(%rbp)
     61a:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     61d:	48 63 f0             	movslq %eax,%rsi
     620:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     624:	ba 00 00 00 00       	mov    $0x0,%edx
     629:	48 f7 f6             	div    %rsi
     62c:	48 89 d0             	mov    %rdx,%rax
     62f:	0f b6 90 f0 14 00 00 	movzbl 0x14f0(%rax),%edx
     636:	48 63 c1             	movslq %ecx,%rax
     639:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
     63d:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     640:	48 63 f8             	movslq %eax,%rdi
     643:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     647:	ba 00 00 00 00       	mov    $0x0,%edx
     64c:	48 f7 f7             	div    %rdi
     64f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     653:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     658:	75 b7                	jne    611 <printint64+0x49>

  if (sgn)
     65a:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     65e:	74 2b                	je     68b <printint64+0xc3>
    buf[i++] = '-';
     660:	8b 45 fc             	mov    -0x4(%rbp),%eax
     663:	8d 50 01             	lea    0x1(%rax),%edx
     666:	89 55 fc             	mov    %edx,-0x4(%rbp)
     669:	48 98                	cltq   
     66b:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
     670:	eb 19                	jmp    68b <printint64+0xc3>
    putc(fd, buf[i]);
     672:	8b 45 fc             	mov    -0x4(%rbp),%eax
     675:	48 98                	cltq   
     677:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
     67c:	0f be d0             	movsbl %al,%edx
     67f:	8b 45 cc             	mov    -0x34(%rbp),%eax
     682:	89 d6                	mov    %edx,%esi
     684:	89 c7                	mov    %eax,%edi
     686:	e8 14 ff ff ff       	callq  59f <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
     68b:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     68f:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     693:	79 dd                	jns    672 <printint64+0xaa>
    putc(fd, buf[i]);
}
     695:	90                   	nop
     696:	c9                   	leaveq 
     697:	c3                   	retq   

0000000000000698 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
     698:	55                   	push   %rbp
     699:	48 89 e5             	mov    %rsp,%rbp
     69c:	48 83 ec 30          	sub    $0x30,%rsp
     6a0:	89 7d dc             	mov    %edi,-0x24(%rbp)
     6a3:	89 75 d8             	mov    %esi,-0x28(%rbp)
     6a6:	89 55 d4             	mov    %edx,-0x2c(%rbp)
     6a9:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
     6ac:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
     6b3:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
     6b7:	74 17                	je     6d0 <printint+0x38>
     6b9:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
     6bd:	79 11                	jns    6d0 <printint+0x38>
    neg = 1;
     6bf:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
     6c6:	8b 45 d8             	mov    -0x28(%rbp),%eax
     6c9:	f7 d8                	neg    %eax
     6cb:	89 45 f4             	mov    %eax,-0xc(%rbp)
     6ce:	eb 06                	jmp    6d6 <printint+0x3e>
  } else {
    x = xx;
     6d0:	8b 45 d8             	mov    -0x28(%rbp),%eax
     6d3:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
     6d6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     6dd:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     6e0:	8d 41 01             	lea    0x1(%rcx),%eax
     6e3:	89 45 fc             	mov    %eax,-0x4(%rbp)
     6e6:	8b 75 d4             	mov    -0x2c(%rbp),%esi
     6e9:	8b 45 f4             	mov    -0xc(%rbp),%eax
     6ec:	ba 00 00 00 00       	mov    $0x0,%edx
     6f1:	f7 f6                	div    %esi
     6f3:	89 d0                	mov    %edx,%eax
     6f5:	89 c0                	mov    %eax,%eax
     6f7:	0f b6 90 10 15 00 00 	movzbl 0x1510(%rax),%edx
     6fe:	48 63 c1             	movslq %ecx,%rax
     701:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
     705:	8b 7d d4             	mov    -0x2c(%rbp),%edi
     708:	8b 45 f4             	mov    -0xc(%rbp),%eax
     70b:	ba 00 00 00 00       	mov    $0x0,%edx
     710:	f7 f7                	div    %edi
     712:	89 45 f4             	mov    %eax,-0xc(%rbp)
     715:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     719:	75 c2                	jne    6dd <printint+0x45>
  if (neg)
     71b:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     71f:	74 2b                	je     74c <printint+0xb4>
    buf[i++] = '-';
     721:	8b 45 fc             	mov    -0x4(%rbp),%eax
     724:	8d 50 01             	lea    0x1(%rax),%edx
     727:	89 55 fc             	mov    %edx,-0x4(%rbp)
     72a:	48 98                	cltq   
     72c:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
     731:	eb 19                	jmp    74c <printint+0xb4>
    putc(fd, buf[i]);
     733:	8b 45 fc             	mov    -0x4(%rbp),%eax
     736:	48 98                	cltq   
     738:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
     73d:	0f be d0             	movsbl %al,%edx
     740:	8b 45 dc             	mov    -0x24(%rbp),%eax
     743:	89 d6                	mov    %edx,%esi
     745:	89 c7                	mov    %eax,%edi
     747:	e8 53 fe ff ff       	callq  59f <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
     74c:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     750:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     754:	79 dd                	jns    733 <printint+0x9b>
    putc(fd, buf[i]);
}
     756:	90                   	nop
     757:	c9                   	leaveq 
     758:	c3                   	retq   

0000000000000759 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
     759:	55                   	push   %rbp
     75a:	48 89 e5             	mov    %rsp,%rbp
     75d:	48 83 ec 70          	sub    $0x70,%rsp
     761:	89 7d 9c             	mov    %edi,-0x64(%rbp)
     764:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
     768:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
     76c:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
     770:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
     774:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
     778:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
     77f:	48 8d 45 10          	lea    0x10(%rbp),%rax
     783:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
     787:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
     78b:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
     78f:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
     796:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
     79d:	e9 68 02 00 00       	jmpq   a0a <printf+0x2b1>
    c = fmt[i] & 0xff;
     7a2:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     7a5:	48 63 d0             	movslq %eax,%rdx
     7a8:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     7ac:	48 01 d0             	add    %rdx,%rax
     7af:	0f b6 00             	movzbl (%rax),%eax
     7b2:	0f be c0             	movsbl %al,%eax
     7b5:	25 ff 00 00 00       	and    $0xff,%eax
     7ba:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
     7bd:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     7c1:	75 30                	jne    7f3 <printf+0x9a>
      if (c == '%') {
     7c3:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     7c7:	75 13                	jne    7dc <printf+0x83>
        state = '%';
     7c9:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
     7d0:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
     7d7:	e9 2a 02 00 00       	jmpq   a06 <printf+0x2ad>
      } else {
        putc(fd, c);
     7dc:	8b 45 b8             	mov    -0x48(%rbp),%eax
     7df:	0f be d0             	movsbl %al,%edx
     7e2:	8b 45 9c             	mov    -0x64(%rbp),%eax
     7e5:	89 d6                	mov    %edx,%esi
     7e7:	89 c7                	mov    %eax,%edi
     7e9:	e8 b1 fd ff ff       	callq  59f <putc>
     7ee:	e9 13 02 00 00       	jmpq   a06 <printf+0x2ad>
      }
    } else if (state == '%') {
     7f3:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
     7f7:	0f 85 09 02 00 00    	jne    a06 <printf+0x2ad>
      if (c == 'l') {
     7fd:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
     801:	75 0c                	jne    80f <printf+0xb6>
        lflag = 1;
     803:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
     80a:	e9 f7 01 00 00       	jmpq   a06 <printf+0x2ad>
      } else if (c == 'd') {
     80f:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
     813:	0f 85 95 00 00 00    	jne    8ae <printf+0x155>
        if (lflag == 1)
     819:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
     81d:	75 49                	jne    868 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
     81f:	8b 45 a0             	mov    -0x60(%rbp),%eax
     822:	83 f8 30             	cmp    $0x30,%eax
     825:	73 17                	jae    83e <printf+0xe5>
     827:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     82b:	8b 55 a0             	mov    -0x60(%rbp),%edx
     82e:	89 d2                	mov    %edx,%edx
     830:	48 01 d0             	add    %rdx,%rax
     833:	8b 55 a0             	mov    -0x60(%rbp),%edx
     836:	83 c2 08             	add    $0x8,%edx
     839:	89 55 a0             	mov    %edx,-0x60(%rbp)
     83c:	eb 0c                	jmp    84a <printf+0xf1>
     83e:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     842:	48 8d 50 08          	lea    0x8(%rax),%rdx
     846:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     84a:	48 8b 00             	mov    (%rax),%rax
     84d:	89 c6                	mov    %eax,%esi
     84f:	8b 45 9c             	mov    -0x64(%rbp),%eax
     852:	b9 01 00 00 00       	mov    $0x1,%ecx
     857:	ba 0a 00 00 00       	mov    $0xa,%edx
     85c:	89 c7                	mov    %eax,%edi
     85e:	e8 65 fd ff ff       	callq  5c8 <printint64>
     863:	e9 97 01 00 00       	jmpq   9ff <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
     868:	8b 45 a0             	mov    -0x60(%rbp),%eax
     86b:	83 f8 30             	cmp    $0x30,%eax
     86e:	73 17                	jae    887 <printf+0x12e>
     870:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     874:	8b 55 a0             	mov    -0x60(%rbp),%edx
     877:	89 d2                	mov    %edx,%edx
     879:	48 01 d0             	add    %rdx,%rax
     87c:	8b 55 a0             	mov    -0x60(%rbp),%edx
     87f:	83 c2 08             	add    $0x8,%edx
     882:	89 55 a0             	mov    %edx,-0x60(%rbp)
     885:	eb 0c                	jmp    893 <printf+0x13a>
     887:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     88b:	48 8d 50 08          	lea    0x8(%rax),%rdx
     88f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     893:	8b 30                	mov    (%rax),%esi
     895:	8b 45 9c             	mov    -0x64(%rbp),%eax
     898:	b9 01 00 00 00       	mov    $0x1,%ecx
     89d:	ba 0a 00 00 00       	mov    $0xa,%edx
     8a2:	89 c7                	mov    %eax,%edi
     8a4:	e8 ef fd ff ff       	callq  698 <printint>
     8a9:	e9 51 01 00 00       	jmpq   9ff <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
     8ae:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
     8b2:	74 0a                	je     8be <printf+0x165>
     8b4:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
     8b8:	0f 85 95 00 00 00    	jne    953 <printf+0x1fa>
        if (lflag == 1)
     8be:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
     8c2:	75 49                	jne    90d <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
     8c4:	8b 45 a0             	mov    -0x60(%rbp),%eax
     8c7:	83 f8 30             	cmp    $0x30,%eax
     8ca:	73 17                	jae    8e3 <printf+0x18a>
     8cc:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     8d0:	8b 55 a0             	mov    -0x60(%rbp),%edx
     8d3:	89 d2                	mov    %edx,%edx
     8d5:	48 01 d0             	add    %rdx,%rax
     8d8:	8b 55 a0             	mov    -0x60(%rbp),%edx
     8db:	83 c2 08             	add    $0x8,%edx
     8de:	89 55 a0             	mov    %edx,-0x60(%rbp)
     8e1:	eb 0c                	jmp    8ef <printf+0x196>
     8e3:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     8e7:	48 8d 50 08          	lea    0x8(%rax),%rdx
     8eb:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     8ef:	48 8b 00             	mov    (%rax),%rax
     8f2:	89 c6                	mov    %eax,%esi
     8f4:	8b 45 9c             	mov    -0x64(%rbp),%eax
     8f7:	b9 00 00 00 00       	mov    $0x0,%ecx
     8fc:	ba 10 00 00 00       	mov    $0x10,%edx
     901:	89 c7                	mov    %eax,%edi
     903:	e8 c0 fc ff ff       	callq  5c8 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
     908:	e9 f2 00 00 00       	jmpq   9ff <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
     90d:	8b 45 a0             	mov    -0x60(%rbp),%eax
     910:	83 f8 30             	cmp    $0x30,%eax
     913:	73 17                	jae    92c <printf+0x1d3>
     915:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     919:	8b 55 a0             	mov    -0x60(%rbp),%edx
     91c:	89 d2                	mov    %edx,%edx
     91e:	48 01 d0             	add    %rdx,%rax
     921:	8b 55 a0             	mov    -0x60(%rbp),%edx
     924:	83 c2 08             	add    $0x8,%edx
     927:	89 55 a0             	mov    %edx,-0x60(%rbp)
     92a:	eb 0c                	jmp    938 <printf+0x1df>
     92c:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     930:	48 8d 50 08          	lea    0x8(%rax),%rdx
     934:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     938:	8b 30                	mov    (%rax),%esi
     93a:	8b 45 9c             	mov    -0x64(%rbp),%eax
     93d:	b9 00 00 00 00       	mov    $0x0,%ecx
     942:	ba 10 00 00 00       	mov    $0x10,%edx
     947:	89 c7                	mov    %eax,%edi
     949:	e8 4a fd ff ff       	callq  698 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
     94e:	e9 ac 00 00 00       	jmpq   9ff <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
     953:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
     957:	75 6b                	jne    9c4 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
     959:	8b 45 a0             	mov    -0x60(%rbp),%eax
     95c:	83 f8 30             	cmp    $0x30,%eax
     95f:	73 17                	jae    978 <printf+0x21f>
     961:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     965:	8b 55 a0             	mov    -0x60(%rbp),%edx
     968:	89 d2                	mov    %edx,%edx
     96a:	48 01 d0             	add    %rdx,%rax
     96d:	8b 55 a0             	mov    -0x60(%rbp),%edx
     970:	83 c2 08             	add    $0x8,%edx
     973:	89 55 a0             	mov    %edx,-0x60(%rbp)
     976:	eb 0c                	jmp    984 <printf+0x22b>
     978:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     97c:	48 8d 50 08          	lea    0x8(%rax),%rdx
     980:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     984:	48 8b 00             	mov    (%rax),%rax
     987:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
     98b:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
     990:	75 25                	jne    9b7 <printf+0x25e>
          s = "(null)";
     992:	48 c7 45 c8 3a 12 00 	movq   $0x123a,-0x38(%rbp)
     999:	00 
        for (; *s; s++)
     99a:	eb 1b                	jmp    9b7 <printf+0x25e>
          putc(fd, *s);
     99c:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     9a0:	0f b6 00             	movzbl (%rax),%eax
     9a3:	0f be d0             	movsbl %al,%edx
     9a6:	8b 45 9c             	mov    -0x64(%rbp),%eax
     9a9:	89 d6                	mov    %edx,%esi
     9ab:	89 c7                	mov    %eax,%edi
     9ad:	e8 ed fb ff ff       	callq  59f <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
     9b2:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
     9b7:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     9bb:	0f b6 00             	movzbl (%rax),%eax
     9be:	84 c0                	test   %al,%al
     9c0:	75 da                	jne    99c <printf+0x243>
     9c2:	eb 3b                	jmp    9ff <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
     9c4:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     9c8:	75 14                	jne    9de <printf+0x285>
        putc(fd, c);
     9ca:	8b 45 b8             	mov    -0x48(%rbp),%eax
     9cd:	0f be d0             	movsbl %al,%edx
     9d0:	8b 45 9c             	mov    -0x64(%rbp),%eax
     9d3:	89 d6                	mov    %edx,%esi
     9d5:	89 c7                	mov    %eax,%edi
     9d7:	e8 c3 fb ff ff       	callq  59f <putc>
     9dc:	eb 21                	jmp    9ff <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
     9de:	8b 45 9c             	mov    -0x64(%rbp),%eax
     9e1:	be 25 00 00 00       	mov    $0x25,%esi
     9e6:	89 c7                	mov    %eax,%edi
     9e8:	e8 b2 fb ff ff       	callq  59f <putc>
        putc(fd, c);
     9ed:	8b 45 b8             	mov    -0x48(%rbp),%eax
     9f0:	0f be d0             	movsbl %al,%edx
     9f3:	8b 45 9c             	mov    -0x64(%rbp),%eax
     9f6:	89 d6                	mov    %edx,%esi
     9f8:	89 c7                	mov    %eax,%edi
     9fa:	e8 a0 fb ff ff       	callq  59f <putc>
      }
      state = 0;
     9ff:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
     a06:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
     a0a:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     a0d:	48 63 d0             	movslq %eax,%rdx
     a10:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     a14:	48 01 d0             	add    %rdx,%rax
     a17:	0f b6 00             	movzbl (%rax),%eax
     a1a:	84 c0                	test   %al,%al
     a1c:	0f 85 80 fd ff ff    	jne    7a2 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
     a22:	90                   	nop
     a23:	c9                   	leaveq 
     a24:	c3                   	retq   

0000000000000a25 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
     a25:	55                   	push   %rbp
     a26:	48 89 e5             	mov    %rsp,%rbp
     a29:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     a2d:	89 75 f4             	mov    %esi,-0xc(%rbp)
     a30:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
     a33:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
     a37:	8b 55 f0             	mov    -0x10(%rbp),%edx
     a3a:	8b 45 f4             	mov    -0xc(%rbp),%eax
     a3d:	48 89 ce             	mov    %rcx,%rsi
     a40:	48 89 f7             	mov    %rsi,%rdi
     a43:	89 d1                	mov    %edx,%ecx
     a45:	fc                   	cld    
     a46:	f3 aa                	rep stos %al,%es:(%rdi)
     a48:	89 ca                	mov    %ecx,%edx
     a4a:	48 89 fe             	mov    %rdi,%rsi
     a4d:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
     a51:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
     a54:	90                   	nop
     a55:	5d                   	pop    %rbp
     a56:	c3                   	retq   

0000000000000a57 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
     a57:	55                   	push   %rbp
     a58:	48 89 e5             	mov    %rsp,%rbp
     a5b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     a5f:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
     a63:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     a67:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
     a6b:	90                   	nop
     a6c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     a70:	48 8d 50 01          	lea    0x1(%rax),%rdx
     a74:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
     a78:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     a7c:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
     a80:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
     a84:	0f b6 12             	movzbl (%rdx),%edx
     a87:	88 10                	mov    %dl,(%rax)
     a89:	0f b6 00             	movzbl (%rax),%eax
     a8c:	84 c0                	test   %al,%al
     a8e:	75 dc                	jne    a6c <strcpy+0x15>
    ;
  return os;
     a90:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     a94:	5d                   	pop    %rbp
     a95:	c3                   	retq   

0000000000000a96 <strcmp>:

int strcmp(const char *p, const char *q) {
     a96:	55                   	push   %rbp
     a97:	48 89 e5             	mov    %rsp,%rbp
     a9a:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     a9e:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
     aa2:	eb 0a                	jmp    aae <strcmp+0x18>
    p++, q++;
     aa4:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
     aa9:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
     aae:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ab2:	0f b6 00             	movzbl (%rax),%eax
     ab5:	84 c0                	test   %al,%al
     ab7:	74 12                	je     acb <strcmp+0x35>
     ab9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     abd:	0f b6 10             	movzbl (%rax),%edx
     ac0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ac4:	0f b6 00             	movzbl (%rax),%eax
     ac7:	38 c2                	cmp    %al,%dl
     ac9:	74 d9                	je     aa4 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
     acb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     acf:	0f b6 00             	movzbl (%rax),%eax
     ad2:	0f b6 d0             	movzbl %al,%edx
     ad5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ad9:	0f b6 00             	movzbl (%rax),%eax
     adc:	0f b6 c0             	movzbl %al,%eax
     adf:	29 c2                	sub    %eax,%edx
     ae1:	89 d0                	mov    %edx,%eax
}
     ae3:	5d                   	pop    %rbp
     ae4:	c3                   	retq   

0000000000000ae5 <strlen>:

uint strlen(char *s) {
     ae5:	55                   	push   %rbp
     ae6:	48 89 e5             	mov    %rsp,%rbp
     ae9:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
     aed:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     af4:	eb 04                	jmp    afa <strlen+0x15>
     af6:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     afa:	8b 45 fc             	mov    -0x4(%rbp),%eax
     afd:	48 63 d0             	movslq %eax,%rdx
     b00:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     b04:	48 01 d0             	add    %rdx,%rax
     b07:	0f b6 00             	movzbl (%rax),%eax
     b0a:	84 c0                	test   %al,%al
     b0c:	75 e8                	jne    af6 <strlen+0x11>
    ;
  return n;
     b0e:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     b11:	5d                   	pop    %rbp
     b12:	c3                   	retq   

0000000000000b13 <memset>:

void *memset(void *dst, int c, uint n) {
     b13:	55                   	push   %rbp
     b14:	48 89 e5             	mov    %rsp,%rbp
     b17:	48 83 ec 10          	sub    $0x10,%rsp
     b1b:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     b1f:	89 75 f4             	mov    %esi,-0xc(%rbp)
     b22:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
     b25:	8b 55 f0             	mov    -0x10(%rbp),%edx
     b28:	8b 4d f4             	mov    -0xc(%rbp),%ecx
     b2b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b2f:	89 ce                	mov    %ecx,%esi
     b31:	48 89 c7             	mov    %rax,%rdi
     b34:	e8 ec fe ff ff       	callq  a25 <stosb>
  return dst;
     b39:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     b3d:	c9                   	leaveq 
     b3e:	c3                   	retq   

0000000000000b3f <strchr>:

char *strchr(const char *s, char c) {
     b3f:	55                   	push   %rbp
     b40:	48 89 e5             	mov    %rsp,%rbp
     b43:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     b47:	89 f0                	mov    %esi,%eax
     b49:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
     b4c:	eb 17                	jmp    b65 <strchr+0x26>
    if (*s == c)
     b4e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b52:	0f b6 00             	movzbl (%rax),%eax
     b55:	3a 45 f4             	cmp    -0xc(%rbp),%al
     b58:	75 06                	jne    b60 <strchr+0x21>
      return (char *)s;
     b5a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b5e:	eb 15                	jmp    b75 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
     b60:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
     b65:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b69:	0f b6 00             	movzbl (%rax),%eax
     b6c:	84 c0                	test   %al,%al
     b6e:	75 de                	jne    b4e <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
     b70:	b8 00 00 00 00       	mov    $0x0,%eax
}
     b75:	5d                   	pop    %rbp
     b76:	c3                   	retq   

0000000000000b77 <gets>:

char *gets(char *buf, int max) {
     b77:	55                   	push   %rbp
     b78:	48 89 e5             	mov    %rsp,%rbp
     b7b:	48 83 ec 20          	sub    $0x20,%rsp
     b7f:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     b83:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
     b86:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     b8d:	eb 48                	jmp    bd7 <gets+0x60>
    cc = read(0, &c, 1);
     b8f:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
     b93:	ba 01 00 00 00       	mov    $0x1,%edx
     b98:	48 89 c6             	mov    %rax,%rsi
     b9b:	bf 00 00 00 00       	mov    $0x0,%edi
     ba0:	e8 6f 01 00 00       	callq  d14 <read>
     ba5:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
     ba8:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     bac:	7e 36                	jle    be4 <gets+0x6d>
      break;
    buf[i++] = c;
     bae:	8b 45 fc             	mov    -0x4(%rbp),%eax
     bb1:	8d 50 01             	lea    0x1(%rax),%edx
     bb4:	89 55 fc             	mov    %edx,-0x4(%rbp)
     bb7:	48 63 d0             	movslq %eax,%rdx
     bba:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     bbe:	48 01 c2             	add    %rax,%rdx
     bc1:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     bc5:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
     bc7:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     bcb:	3c 0a                	cmp    $0xa,%al
     bcd:	74 16                	je     be5 <gets+0x6e>
     bcf:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     bd3:	3c 0d                	cmp    $0xd,%al
     bd5:	74 0e                	je     be5 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
     bd7:	8b 45 fc             	mov    -0x4(%rbp),%eax
     bda:	83 c0 01             	add    $0x1,%eax
     bdd:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
     be0:	7c ad                	jl     b8f <gets+0x18>
     be2:	eb 01                	jmp    be5 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
     be4:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
     be5:	8b 45 fc             	mov    -0x4(%rbp),%eax
     be8:	48 63 d0             	movslq %eax,%rdx
     beb:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     bef:	48 01 d0             	add    %rdx,%rax
     bf2:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
     bf5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
     bf9:	c9                   	leaveq 
     bfa:	c3                   	retq   

0000000000000bfb <stat>:

int stat(char *n, struct stat *st) {
     bfb:	55                   	push   %rbp
     bfc:	48 89 e5             	mov    %rsp,%rbp
     bff:	48 83 ec 20          	sub    $0x20,%rsp
     c03:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     c07:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
     c0b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c0f:	be 00 00 00 00       	mov    $0x0,%esi
     c14:	48 89 c7             	mov    %rax,%rdi
     c17:	e8 20 01 00 00       	callq  d3c <open>
     c1c:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
     c1f:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     c23:	79 07                	jns    c2c <stat+0x31>
    return -1;
     c25:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
     c2a:	eb 21                	jmp    c4d <stat+0x52>
  r = fstat(fd, st);
     c2c:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     c30:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c33:	48 89 d6             	mov    %rdx,%rsi
     c36:	89 c7                	mov    %eax,%edi
     c38:	e8 17 01 00 00       	callq  d54 <fstat>
     c3d:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
     c40:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c43:	89 c7                	mov    %eax,%edi
     c45:	e8 da 00 00 00       	callq  d24 <close>
  return r;
     c4a:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
     c4d:	c9                   	leaveq 
     c4e:	c3                   	retq   

0000000000000c4f <atoi>:

int atoi(const char *s) {
     c4f:	55                   	push   %rbp
     c50:	48 89 e5             	mov    %rsp,%rbp
     c53:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
     c57:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
     c5e:	eb 28                	jmp    c88 <atoi+0x39>
    n = n * 10 + *s++ - '0';
     c60:	8b 55 fc             	mov    -0x4(%rbp),%edx
     c63:	89 d0                	mov    %edx,%eax
     c65:	c1 e0 02             	shl    $0x2,%eax
     c68:	01 d0                	add    %edx,%eax
     c6a:	01 c0                	add    %eax,%eax
     c6c:	89 c1                	mov    %eax,%ecx
     c6e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c72:	48 8d 50 01          	lea    0x1(%rax),%rdx
     c76:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
     c7a:	0f b6 00             	movzbl (%rax),%eax
     c7d:	0f be c0             	movsbl %al,%eax
     c80:	01 c8                	add    %ecx,%eax
     c82:	83 e8 30             	sub    $0x30,%eax
     c85:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
     c88:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c8c:	0f b6 00             	movzbl (%rax),%eax
     c8f:	3c 2f                	cmp    $0x2f,%al
     c91:	7e 0b                	jle    c9e <atoi+0x4f>
     c93:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c97:	0f b6 00             	movzbl (%rax),%eax
     c9a:	3c 39                	cmp    $0x39,%al
     c9c:	7e c2                	jle    c60 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
     c9e:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     ca1:	5d                   	pop    %rbp
     ca2:	c3                   	retq   

0000000000000ca3 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
     ca3:	55                   	push   %rbp
     ca4:	48 89 e5             	mov    %rsp,%rbp
     ca7:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     cab:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
     caf:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
     cb2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     cb6:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
     cba:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     cbe:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
     cc2:	eb 1d                	jmp    ce1 <memmove+0x3e>
    *dst++ = *src++;
     cc4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     cc8:	48 8d 50 01          	lea    0x1(%rax),%rdx
     ccc:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
     cd0:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
     cd4:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
     cd8:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
     cdc:	0f b6 12             	movzbl (%rdx),%edx
     cdf:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
     ce1:	8b 45 dc             	mov    -0x24(%rbp),%eax
     ce4:	8d 50 ff             	lea    -0x1(%rax),%edx
     ce7:	89 55 dc             	mov    %edx,-0x24(%rbp)
     cea:	85 c0                	test   %eax,%eax
     cec:	7f d6                	jg     cc4 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
     cee:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     cf2:	5d                   	pop    %rbp
     cf3:	c3                   	retq   

0000000000000cf4 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
     cf4:	b8 01 00 00 00       	mov    $0x1,%eax
     cf9:	cd 40                	int    $0x40
     cfb:	c3                   	retq   

0000000000000cfc <exit>:
SYSCALL(exit)
     cfc:	b8 02 00 00 00       	mov    $0x2,%eax
     d01:	cd 40                	int    $0x40
     d03:	c3                   	retq   

0000000000000d04 <wait>:
SYSCALL(wait)
     d04:	b8 03 00 00 00       	mov    $0x3,%eax
     d09:	cd 40                	int    $0x40
     d0b:	c3                   	retq   

0000000000000d0c <pipe>:
SYSCALL(pipe)
     d0c:	b8 04 00 00 00       	mov    $0x4,%eax
     d11:	cd 40                	int    $0x40
     d13:	c3                   	retq   

0000000000000d14 <read>:
SYSCALL(read)
     d14:	b8 05 00 00 00       	mov    $0x5,%eax
     d19:	cd 40                	int    $0x40
     d1b:	c3                   	retq   

0000000000000d1c <write>:
SYSCALL(write)
     d1c:	b8 10 00 00 00       	mov    $0x10,%eax
     d21:	cd 40                	int    $0x40
     d23:	c3                   	retq   

0000000000000d24 <close>:
SYSCALL(close)
     d24:	b8 15 00 00 00       	mov    $0x15,%eax
     d29:	cd 40                	int    $0x40
     d2b:	c3                   	retq   

0000000000000d2c <kill>:
SYSCALL(kill)
     d2c:	b8 06 00 00 00       	mov    $0x6,%eax
     d31:	cd 40                	int    $0x40
     d33:	c3                   	retq   

0000000000000d34 <exec>:
SYSCALL(exec)
     d34:	b8 07 00 00 00       	mov    $0x7,%eax
     d39:	cd 40                	int    $0x40
     d3b:	c3                   	retq   

0000000000000d3c <open>:
SYSCALL(open)
     d3c:	b8 0f 00 00 00       	mov    $0xf,%eax
     d41:	cd 40                	int    $0x40
     d43:	c3                   	retq   

0000000000000d44 <mknod>:
SYSCALL(mknod)
     d44:	b8 11 00 00 00       	mov    $0x11,%eax
     d49:	cd 40                	int    $0x40
     d4b:	c3                   	retq   

0000000000000d4c <unlink>:
SYSCALL(unlink)
     d4c:	b8 12 00 00 00       	mov    $0x12,%eax
     d51:	cd 40                	int    $0x40
     d53:	c3                   	retq   

0000000000000d54 <fstat>:
SYSCALL(fstat)
     d54:	b8 08 00 00 00       	mov    $0x8,%eax
     d59:	cd 40                	int    $0x40
     d5b:	c3                   	retq   

0000000000000d5c <link>:
SYSCALL(link)
     d5c:	b8 13 00 00 00       	mov    $0x13,%eax
     d61:	cd 40                	int    $0x40
     d63:	c3                   	retq   

0000000000000d64 <mkdir>:
SYSCALL(mkdir)
     d64:	b8 14 00 00 00       	mov    $0x14,%eax
     d69:	cd 40                	int    $0x40
     d6b:	c3                   	retq   

0000000000000d6c <chdir>:
SYSCALL(chdir)
     d6c:	b8 09 00 00 00       	mov    $0x9,%eax
     d71:	cd 40                	int    $0x40
     d73:	c3                   	retq   

0000000000000d74 <dup>:
SYSCALL(dup)
     d74:	b8 0a 00 00 00       	mov    $0xa,%eax
     d79:	cd 40                	int    $0x40
     d7b:	c3                   	retq   

0000000000000d7c <getpid>:
SYSCALL(getpid)
     d7c:	b8 0b 00 00 00       	mov    $0xb,%eax
     d81:	cd 40                	int    $0x40
     d83:	c3                   	retq   

0000000000000d84 <sbrk>:
SYSCALL(sbrk)
     d84:	b8 0c 00 00 00       	mov    $0xc,%eax
     d89:	cd 40                	int    $0x40
     d8b:	c3                   	retq   

0000000000000d8c <sleep>:
SYSCALL(sleep)
     d8c:	b8 0d 00 00 00       	mov    $0xd,%eax
     d91:	cd 40                	int    $0x40
     d93:	c3                   	retq   

0000000000000d94 <uptime>:
SYSCALL(uptime)
     d94:	b8 0e 00 00 00       	mov    $0xe,%eax
     d99:	cd 40                	int    $0x40
     d9b:	c3                   	retq   

0000000000000d9c <sysinfo>:
SYSCALL(sysinfo)
     d9c:	b8 16 00 00 00       	mov    $0x16,%eax
     da1:	cd 40                	int    $0x40
     da3:	c3                   	retq   

0000000000000da4 <crashn>:
SYSCALL(crashn)
     da4:	b8 17 00 00 00       	mov    $0x17,%eax
     da9:	cd 40                	int    $0x40
     dab:	c3                   	retq   

0000000000000dac <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
     dac:	55                   	push   %rbp
     dad:	48 89 e5             	mov    %rsp,%rbp
     db0:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
     db4:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     db8:	48 83 e8 10          	sub    $0x10,%rax
     dbc:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
     dc0:	48 8b 05 79 07 00 00 	mov    0x779(%rip),%rax        # 1540 <freep>
     dc7:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
     dcb:	eb 2f                	jmp    dfc <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
     dcd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     dd1:	48 8b 00             	mov    (%rax),%rax
     dd4:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     dd8:	77 17                	ja     df1 <free+0x45>
     dda:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     dde:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     de2:	77 2f                	ja     e13 <free+0x67>
     de4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     de8:	48 8b 00             	mov    (%rax),%rax
     deb:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     def:	77 22                	ja     e13 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
     df1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     df5:	48 8b 00             	mov    (%rax),%rax
     df8:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
     dfc:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e00:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     e04:	76 c7                	jbe    dcd <free+0x21>
     e06:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e0a:	48 8b 00             	mov    (%rax),%rax
     e0d:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     e11:	76 ba                	jbe    dcd <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
     e13:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e17:	8b 40 08             	mov    0x8(%rax),%eax
     e1a:	89 c0                	mov    %eax,%eax
     e1c:	48 c1 e0 04          	shl    $0x4,%rax
     e20:	48 89 c2             	mov    %rax,%rdx
     e23:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e27:	48 01 c2             	add    %rax,%rdx
     e2a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e2e:	48 8b 00             	mov    (%rax),%rax
     e31:	48 39 c2             	cmp    %rax,%rdx
     e34:	75 2d                	jne    e63 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
     e36:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e3a:	8b 50 08             	mov    0x8(%rax),%edx
     e3d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e41:	48 8b 00             	mov    (%rax),%rax
     e44:	8b 40 08             	mov    0x8(%rax),%eax
     e47:	01 c2                	add    %eax,%edx
     e49:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e4d:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
     e50:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e54:	48 8b 00             	mov    (%rax),%rax
     e57:	48 8b 10             	mov    (%rax),%rdx
     e5a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e5e:	48 89 10             	mov    %rdx,(%rax)
     e61:	eb 0e                	jmp    e71 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
     e63:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e67:	48 8b 10             	mov    (%rax),%rdx
     e6a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e6e:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
     e71:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e75:	8b 40 08             	mov    0x8(%rax),%eax
     e78:	89 c0                	mov    %eax,%eax
     e7a:	48 c1 e0 04          	shl    $0x4,%rax
     e7e:	48 89 c2             	mov    %rax,%rdx
     e81:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e85:	48 01 d0             	add    %rdx,%rax
     e88:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     e8c:	75 27                	jne    eb5 <free+0x109>
    p->s.size += bp->s.size;
     e8e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e92:	8b 50 08             	mov    0x8(%rax),%edx
     e95:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e99:	8b 40 08             	mov    0x8(%rax),%eax
     e9c:	01 c2                	add    %eax,%edx
     e9e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ea2:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
     ea5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ea9:	48 8b 10             	mov    (%rax),%rdx
     eac:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     eb0:	48 89 10             	mov    %rdx,(%rax)
     eb3:	eb 0b                	jmp    ec0 <free+0x114>
  } else
    p->s.ptr = bp;
     eb5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     eb9:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
     ebd:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
     ec0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ec4:	48 89 05 75 06 00 00 	mov    %rax,0x675(%rip)        # 1540 <freep>
}
     ecb:	90                   	nop
     ecc:	5d                   	pop    %rbp
     ecd:	c3                   	retq   

0000000000000ece <morecore>:

static Header *morecore(uint nu) {
     ece:	55                   	push   %rbp
     ecf:	48 89 e5             	mov    %rsp,%rbp
     ed2:	48 83 ec 20          	sub    $0x20,%rsp
     ed6:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
     ed9:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
     ee0:	77 07                	ja     ee9 <morecore+0x1b>
    nu = 4096;
     ee2:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
     ee9:	8b 45 ec             	mov    -0x14(%rbp),%eax
     eec:	c1 e0 04             	shl    $0x4,%eax
     eef:	89 c7                	mov    %eax,%edi
     ef1:	e8 8e fe ff ff       	callq  d84 <sbrk>
     ef6:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
     efa:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
     eff:	75 07                	jne    f08 <morecore+0x3a>
    return 0;
     f01:	b8 00 00 00 00       	mov    $0x0,%eax
     f06:	eb 29                	jmp    f31 <morecore+0x63>
  hp = (Header *)p;
     f08:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f0c:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
     f10:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f14:	8b 55 ec             	mov    -0x14(%rbp),%edx
     f17:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
     f1a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f1e:	48 83 c0 10          	add    $0x10,%rax
     f22:	48 89 c7             	mov    %rax,%rdi
     f25:	e8 82 fe ff ff       	callq  dac <free>
  return freep;
     f2a:	48 8b 05 0f 06 00 00 	mov    0x60f(%rip),%rax        # 1540 <freep>
}
     f31:	c9                   	leaveq 
     f32:	c3                   	retq   

0000000000000f33 <malloc>:

void *malloc(uint nbytes) {
     f33:	55                   	push   %rbp
     f34:	48 89 e5             	mov    %rsp,%rbp
     f37:	48 83 ec 30          	sub    $0x30,%rsp
     f3b:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
     f3e:	8b 45 dc             	mov    -0x24(%rbp),%eax
     f41:	48 83 c0 0f          	add    $0xf,%rax
     f45:	48 c1 e8 04          	shr    $0x4,%rax
     f49:	83 c0 01             	add    $0x1,%eax
     f4c:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
     f4f:	48 8b 05 ea 05 00 00 	mov    0x5ea(%rip),%rax        # 1540 <freep>
     f56:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     f5a:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     f5f:	75 2b                	jne    f8c <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
     f61:	48 c7 45 f0 30 15 00 	movq   $0x1530,-0x10(%rbp)
     f68:	00 
     f69:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f6d:	48 89 05 cc 05 00 00 	mov    %rax,0x5cc(%rip)        # 1540 <freep>
     f74:	48 8b 05 c5 05 00 00 	mov    0x5c5(%rip),%rax        # 1540 <freep>
     f7b:	48 89 05 ae 05 00 00 	mov    %rax,0x5ae(%rip)        # 1530 <base>
    base.s.size = 0;
     f82:	c7 05 ac 05 00 00 00 	movl   $0x0,0x5ac(%rip)        # 1538 <base+0x8>
     f89:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
     f8c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f90:	48 8b 00             	mov    (%rax),%rax
     f93:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
     f97:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f9b:	8b 40 08             	mov    0x8(%rax),%eax
     f9e:	3b 45 ec             	cmp    -0x14(%rbp),%eax
     fa1:	72 5f                	jb     1002 <malloc+0xcf>
      if (p->s.size == nunits)
     fa3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fa7:	8b 40 08             	mov    0x8(%rax),%eax
     faa:	3b 45 ec             	cmp    -0x14(%rbp),%eax
     fad:	75 10                	jne    fbf <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
     faf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fb3:	48 8b 10             	mov    (%rax),%rdx
     fb6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     fba:	48 89 10             	mov    %rdx,(%rax)
     fbd:	eb 2e                	jmp    fed <malloc+0xba>
      else {
        p->s.size -= nunits;
     fbf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fc3:	8b 40 08             	mov    0x8(%rax),%eax
     fc6:	2b 45 ec             	sub    -0x14(%rbp),%eax
     fc9:	89 c2                	mov    %eax,%edx
     fcb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fcf:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
     fd2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fd6:	8b 40 08             	mov    0x8(%rax),%eax
     fd9:	89 c0                	mov    %eax,%eax
     fdb:	48 c1 e0 04          	shl    $0x4,%rax
     fdf:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
     fe3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fe7:	8b 55 ec             	mov    -0x14(%rbp),%edx
     fea:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
     fed:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ff1:	48 89 05 48 05 00 00 	mov    %rax,0x548(%rip)        # 1540 <freep>
      return (void *)(p + 1);
     ff8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ffc:	48 83 c0 10          	add    $0x10,%rax
    1000:	eb 41                	jmp    1043 <malloc+0x110>
    }
    if (p == freep)
    1002:	48 8b 05 37 05 00 00 	mov    0x537(%rip),%rax        # 1540 <freep>
    1009:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    100d:	75 1c                	jne    102b <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    100f:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1012:	89 c7                	mov    %eax,%edi
    1014:	e8 b5 fe ff ff       	callq  ece <morecore>
    1019:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    101d:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    1022:	75 07                	jne    102b <malloc+0xf8>
        return 0;
    1024:	b8 00 00 00 00       	mov    $0x0,%eax
    1029:	eb 18                	jmp    1043 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    102b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    102f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    1033:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1037:	48 8b 00             	mov    (%rax),%rax
    103a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    103e:	e9 54 ff ff ff       	jmpq   f97 <malloc+0x64>
    1043:	c9                   	leaveq 
    1044:	c3                   	retq   
