
out/user/_lab3test:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
void memtest(void);
void sbrktest(void);
void growstacktest(void);
void copyonwriteforktest(void);

int main(int argc, char *argv[]) {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
       4:	48 83 ec 10          	sub    $0x10,%rsp
       8:	89 7d fc             	mov    %edi,-0x4(%rbp)
       b:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  racetest();
       f:	e8 35 00 00 00       	callq  49 <racetest>
  pkilltest();
      14:	e8 24 01 00 00       	callq  13d <pkilltest>
  memtest();
      19:	e8 d7 02 00 00       	callq  2f5 <memtest>
  sbrktest();
      1e:	e8 0f 04 00 00       	callq  432 <sbrktest>
  growstacktest();
      23:	e8 30 09 00 00       	callq  958 <growstacktest>
  copyonwriteforktest();
      28:	e8 08 0b 00 00       	callq  b35 <copyonwriteforktest>

  printf(stdout, "lab3 tests passed!!\n");
      2d:	8b 05 cd 20 00 00    	mov    0x20cd(%rip),%eax        # 2100 <stdout>
      33:	be e0 18 00 00       	mov    $0x18e0,%esi
      38:	89 c7                	mov    %eax,%edi
      3a:	b8 00 00 00 00       	mov    $0x0,%eax
      3f:	e8 a9 0f 00 00       	callq  fed <printf>

  exit();
      44:	e8 47 15 00 00       	callq  1590 <exit>

0000000000000049 <racetest>:
  return 0;
}

void racetest(void) {
      49:	55                   	push   %rbp
      4a:	48 89 e5             	mov    %rsp,%rbp
      4d:	48 83 ec 10          	sub    $0x10,%rsp
  int i, pid;

  for (i = 0; i < 100; i++) {
      51:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
      58:	e9 bf 00 00 00       	jmpq   11c <racetest+0xd3>
    pid = fork();
      5d:	e8 26 15 00 00       	callq  1588 <fork>
      62:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (pid < 0) {
      65:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
      69:	79 4c                	jns    b7 <racetest+0x6e>
      error("racetest: fork failed\n");
      6b:	8b 05 8f 20 00 00    	mov    0x208f(%rip),%eax        # 2100 <stdout>
      71:	ba 30 00 00 00       	mov    $0x30,%edx
      76:	be f5 18 00 00       	mov    $0x18f5,%esi
      7b:	89 c7                	mov    %eax,%edi
      7d:	b8 00 00 00 00       	mov    $0x0,%eax
      82:	e8 66 0f 00 00       	callq  fed <printf>
      87:	8b 05 73 20 00 00    	mov    0x2073(%rip),%eax        # 2100 <stdout>
      8d:	be 07 19 00 00       	mov    $0x1907,%esi
      92:	89 c7                	mov    %eax,%edi
      94:	b8 00 00 00 00       	mov    $0x0,%eax
      99:	e8 4f 0f 00 00       	callq  fed <printf>
      9e:	8b 05 5c 20 00 00    	mov    0x205c(%rip),%eax        # 2100 <stdout>
      a4:	be 1e 19 00 00       	mov    $0x191e,%esi
      a9:	89 c7                	mov    %eax,%edi
      ab:	b8 00 00 00 00       	mov    $0x0,%eax
      b0:	e8 38 0f 00 00       	callq  fed <printf>
      b5:	eb fe                	jmp    b5 <racetest+0x6c>
    }
    if (pid) {
      b7:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
      bb:	74 56                	je     113 <racetest+0xca>
      if (wait() != pid) {
      bd:	e8 d6 14 00 00       	callq  1598 <wait>
      c2:	3b 45 f8             	cmp    -0x8(%rbp),%eax
      c5:	74 51                	je     118 <racetest+0xcf>
        error("racetest: wait wrong pid\n");
      c7:	8b 05 33 20 00 00    	mov    0x2033(%rip),%eax        # 2100 <stdout>
      cd:	ba 34 00 00 00       	mov    $0x34,%edx
      d2:	be f5 18 00 00       	mov    $0x18f5,%esi
      d7:	89 c7                	mov    %eax,%edi
      d9:	b8 00 00 00 00       	mov    $0x0,%eax
      de:	e8 0a 0f 00 00       	callq  fed <printf>
      e3:	8b 05 17 20 00 00    	mov    0x2017(%rip),%eax        # 2100 <stdout>
      e9:	be 20 19 00 00       	mov    $0x1920,%esi
      ee:	89 c7                	mov    %eax,%edi
      f0:	b8 00 00 00 00       	mov    $0x0,%eax
      f5:	e8 f3 0e 00 00       	callq  fed <printf>
      fa:	8b 05 00 20 00 00    	mov    0x2000(%rip),%eax        # 2100 <stdout>
     100:	be 1e 19 00 00       	mov    $0x191e,%esi
     105:	89 c7                	mov    %eax,%edi
     107:	b8 00 00 00 00       	mov    $0x0,%eax
     10c:	e8 dc 0e 00 00       	callq  fed <printf>
     111:	eb fe                	jmp    111 <racetest+0xc8>
      }
    } else {
      exit();
     113:	e8 78 14 00 00       	callq  1590 <exit>
}

void racetest(void) {
  int i, pid;

  for (i = 0; i < 100; i++) {
     118:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     11c:	83 7d fc 63          	cmpl   $0x63,-0x4(%rbp)
     120:	0f 8e 37 ff ff ff    	jle    5d <racetest+0x14>
      }
    } else {
      exit();
    }
  }
  printf(1, "racetest ok\n");
     126:	be 3a 19 00 00       	mov    $0x193a,%esi
     12b:	bf 01 00 00 00       	mov    $0x1,%edi
     130:	b8 00 00 00 00       	mov    $0x0,%eax
     135:	e8 b3 0e 00 00       	callq  fed <printf>
}
     13a:	90                   	nop
     13b:	c9                   	leaveq 
     13c:	c3                   	retq   

000000000000013d <pkilltest>:

void pkilltest(void) {
     13d:	55                   	push   %rbp
     13e:	48 89 e5             	mov    %rsp,%rbp
     141:	48 83 ec 20          	sub    $0x20,%rsp
  char buf[11];

  int pid1, pid2, pid3;
  int pfds[2];

  printf(1, "preempt: ");
     145:	be 47 19 00 00       	mov    $0x1947,%esi
     14a:	bf 01 00 00 00       	mov    $0x1,%edi
     14f:	b8 00 00 00 00       	mov    $0x0,%eax
     154:	e8 94 0e 00 00       	callq  fed <printf>
  pid1 = fork();
     159:	e8 2a 14 00 00       	callq  1588 <fork>
     15e:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (pid1 == 0)
     161:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     165:	75 02                	jne    169 <pkilltest+0x2c>
    for (;;)
      ;
     167:	eb fe                	jmp    167 <pkilltest+0x2a>

  pid2 = fork();
     169:	e8 1a 14 00 00       	callq  1588 <fork>
     16e:	89 45 f8             	mov    %eax,-0x8(%rbp)
  if (pid2 == 0)
     171:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     175:	75 02                	jne    179 <pkilltest+0x3c>
    for (;;)
      ;
     177:	eb fe                	jmp    177 <pkilltest+0x3a>

  pipe(pfds);
     179:	48 8d 45 e0          	lea    -0x20(%rbp),%rax
     17d:	48 89 c7             	mov    %rax,%rdi
     180:	e8 1b 14 00 00       	callq  15a0 <pipe>
  pid3 = fork();
     185:	e8 fe 13 00 00       	callq  1588 <fork>
     18a:	89 45 f4             	mov    %eax,-0xc(%rbp)
  if (pid3 == 0) {
     18d:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     191:	75 7b                	jne    20e <pkilltest+0xd1>
    close(pfds[0]);
     193:	8b 45 e0             	mov    -0x20(%rbp),%eax
     196:	89 c7                	mov    %eax,%edi
     198:	e8 1b 14 00 00       	callq  15b8 <close>
    if (write(pfds[1], "x", 1) != 1)
     19d:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     1a0:	ba 01 00 00 00       	mov    $0x1,%edx
     1a5:	be 51 19 00 00       	mov    $0x1951,%esi
     1aa:	89 c7                	mov    %eax,%edi
     1ac:	e8 ff 13 00 00       	callq  15b0 <write>
     1b1:	83 f8 01             	cmp    $0x1,%eax
     1b4:	74 4c                	je     202 <pkilltest+0xc5>
      error("pkilltest: write error");
     1b6:	8b 05 44 1f 00 00    	mov    0x1f44(%rip),%eax        # 2100 <stdout>
     1bc:	ba 53 00 00 00       	mov    $0x53,%edx
     1c1:	be f5 18 00 00       	mov    $0x18f5,%esi
     1c6:	89 c7                	mov    %eax,%edi
     1c8:	b8 00 00 00 00       	mov    $0x0,%eax
     1cd:	e8 1b 0e 00 00       	callq  fed <printf>
     1d2:	8b 05 28 1f 00 00    	mov    0x1f28(%rip),%eax        # 2100 <stdout>
     1d8:	be 53 19 00 00       	mov    $0x1953,%esi
     1dd:	89 c7                	mov    %eax,%edi
     1df:	b8 00 00 00 00       	mov    $0x0,%eax
     1e4:	e8 04 0e 00 00       	callq  fed <printf>
     1e9:	8b 05 11 1f 00 00    	mov    0x1f11(%rip),%eax        # 2100 <stdout>
     1ef:	be 1e 19 00 00       	mov    $0x191e,%esi
     1f4:	89 c7                	mov    %eax,%edi
     1f6:	b8 00 00 00 00       	mov    $0x0,%eax
     1fb:	e8 ed 0d 00 00       	callq  fed <printf>
     200:	eb fe                	jmp    200 <pkilltest+0xc3>
    close(pfds[1]);
     202:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     205:	89 c7                	mov    %eax,%edi
     207:	e8 ac 13 00 00       	callq  15b8 <close>
    for (;;)
      ;
     20c:	eb fe                	jmp    20c <pkilltest+0xcf>
  }

  close(pfds[1]);
     20e:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     211:	89 c7                	mov    %eax,%edi
     213:	e8 a0 13 00 00       	callq  15b8 <close>
  if (read(pfds[0], buf, sizeof(buf)) != 1) {
     218:	8b 45 e0             	mov    -0x20(%rbp),%eax
     21b:	48 8d 4d e9          	lea    -0x17(%rbp),%rcx
     21f:	ba 0b 00 00 00       	mov    $0xb,%edx
     224:	48 89 ce             	mov    %rcx,%rsi
     227:	89 c7                	mov    %eax,%edi
     229:	e8 7a 13 00 00       	callq  15a8 <read>
     22e:	83 f8 01             	cmp    $0x1,%eax
     231:	74 4c                	je     27f <pkilltest+0x142>
    error("pkilltest: read error");
     233:	8b 05 c7 1e 00 00    	mov    0x1ec7(%rip),%eax        # 2100 <stdout>
     239:	ba 5b 00 00 00       	mov    $0x5b,%edx
     23e:	be f5 18 00 00       	mov    $0x18f5,%esi
     243:	89 c7                	mov    %eax,%edi
     245:	b8 00 00 00 00       	mov    $0x0,%eax
     24a:	e8 9e 0d 00 00       	callq  fed <printf>
     24f:	8b 05 ab 1e 00 00    	mov    0x1eab(%rip),%eax        # 2100 <stdout>
     255:	be 6a 19 00 00       	mov    $0x196a,%esi
     25a:	89 c7                	mov    %eax,%edi
     25c:	b8 00 00 00 00       	mov    $0x0,%eax
     261:	e8 87 0d 00 00       	callq  fed <printf>
     266:	8b 05 94 1e 00 00    	mov    0x1e94(%rip),%eax        # 2100 <stdout>
     26c:	be 1e 19 00 00       	mov    $0x191e,%esi
     271:	89 c7                	mov    %eax,%edi
     273:	b8 00 00 00 00       	mov    $0x0,%eax
     278:	e8 70 0d 00 00       	callq  fed <printf>
     27d:	eb fe                	jmp    27d <pkilltest+0x140>
  }
  close(pfds[0]);
     27f:	8b 45 e0             	mov    -0x20(%rbp),%eax
     282:	89 c7                	mov    %eax,%edi
     284:	e8 2f 13 00 00       	callq  15b8 <close>
  printf(1, "kill... ");
     289:	be 80 19 00 00       	mov    $0x1980,%esi
     28e:	bf 01 00 00 00       	mov    $0x1,%edi
     293:	b8 00 00 00 00       	mov    $0x0,%eax
     298:	e8 50 0d 00 00       	callq  fed <printf>
  kill(pid1);
     29d:	8b 45 fc             	mov    -0x4(%rbp),%eax
     2a0:	89 c7                	mov    %eax,%edi
     2a2:	e8 19 13 00 00       	callq  15c0 <kill>
  kill(pid2);
     2a7:	8b 45 f8             	mov    -0x8(%rbp),%eax
     2aa:	89 c7                	mov    %eax,%edi
     2ac:	e8 0f 13 00 00       	callq  15c0 <kill>
  kill(pid3);
     2b1:	8b 45 f4             	mov    -0xc(%rbp),%eax
     2b4:	89 c7                	mov    %eax,%edi
     2b6:	e8 05 13 00 00       	callq  15c0 <kill>
  printf(1, "wait... ");
     2bb:	be 89 19 00 00       	mov    $0x1989,%esi
     2c0:	bf 01 00 00 00       	mov    $0x1,%edi
     2c5:	b8 00 00 00 00       	mov    $0x0,%eax
     2ca:	e8 1e 0d 00 00       	callq  fed <printf>
  wait();
     2cf:	e8 c4 12 00 00       	callq  1598 <wait>
  wait();
     2d4:	e8 bf 12 00 00       	callq  1598 <wait>
  wait();
     2d9:	e8 ba 12 00 00       	callq  1598 <wait>
  printf(1, "pkilltest: ok\n");
     2de:	be 92 19 00 00       	mov    $0x1992,%esi
     2e3:	bf 01 00 00 00       	mov    $0x1,%edi
     2e8:	b8 00 00 00 00       	mov    $0x0,%eax
     2ed:	e8 fb 0c 00 00       	callq  fed <printf>
}
     2f2:	90                   	nop
     2f3:	c9                   	leaveq 
     2f4:	c3                   	retq   

00000000000002f5 <memtest>:

void memtest() {
     2f5:	55                   	push   %rbp
     2f6:	48 89 e5             	mov    %rsp,%rbp
     2f9:	48 83 ec 20          	sub    $0x20,%rsp
  void *m1, *m2;
  int pid;
  int i;

  printf(stdout, "memtest\n");
     2fd:	8b 05 fd 1d 00 00    	mov    0x1dfd(%rip),%eax        # 2100 <stdout>
     303:	be a1 19 00 00       	mov    $0x19a1,%esi
     308:	89 c7                	mov    %eax,%edi
     30a:	b8 00 00 00 00       	mov    $0x0,%eax
     30f:	e8 d9 0c 00 00       	callq  fed <printf>
  if ((pid = fork()) == 0) {
     314:	e8 6f 12 00 00       	callq  1588 <fork>
     319:	89 45 f0             	mov    %eax,-0x10(%rbp)
     31c:	83 7d f0 00          	cmpl   $0x0,-0x10(%rbp)
     320:	0f 85 ed 00 00 00    	jne    413 <memtest+0x11e>
    m1 = 0;
     326:	48 c7 45 f8 00 00 00 	movq   $0x0,-0x8(%rbp)
     32d:	00 
    for (i = 0; i < 10; i++) {
     32e:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%rbp)
     335:	eb 25                	jmp    35c <memtest+0x67>
      m2 = malloc(10001);
     337:	bf 11 27 00 00       	mov    $0x2711,%edi
     33c:	e8 86 14 00 00       	callq  17c7 <malloc>
     341:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
      *(char **)m2 = m1;
     345:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     349:	48 8b 55 f8          	mov    -0x8(%rbp),%rdx
     34d:	48 89 10             	mov    %rdx,(%rax)
      m1 = m2;
     350:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     354:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  int i;

  printf(stdout, "memtest\n");
  if ((pid = fork()) == 0) {
    m1 = 0;
    for (i = 0; i < 10; i++) {
     358:	83 45 f4 01          	addl   $0x1,-0xc(%rbp)
     35c:	83 7d f4 09          	cmpl   $0x9,-0xc(%rbp)
     360:	7e d5                	jle    337 <memtest+0x42>
      m2 = malloc(10001);
      *(char **)m2 = m1;
      m1 = m2;
    }
    while (m1) {
     362:	eb 1f                	jmp    383 <memtest+0x8e>
      m2 = *(char **)m1;
     364:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     368:	48 8b 00             	mov    (%rax),%rax
     36b:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
      free(m1);
     36f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     373:	48 89 c7             	mov    %rax,%rdi
     376:	e8 c5 12 00 00       	callq  1640 <free>
      m1 = m2;
     37b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     37f:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    for (i = 0; i < 10; i++) {
      m2 = malloc(10001);
      *(char **)m2 = m1;
      m1 = m2;
    }
    while (m1) {
     383:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
     388:	75 da                	jne    364 <memtest+0x6f>
      m2 = *(char **)m1;
      free(m1);
      m1 = m2;
    }
    m1 = malloc(1024 * 20);
     38a:	bf 00 50 00 00       	mov    $0x5000,%edi
     38f:	e8 33 14 00 00       	callq  17c7 <malloc>
     394:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (m1 == 0) {
     398:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
     39d:	75 4c                	jne    3eb <memtest+0xf6>
      error("couldn't allocate mem?!!\n");
     39f:	8b 05 5b 1d 00 00    	mov    0x1d5b(%rip),%eax        # 2100 <stdout>
     3a5:	ba 7d 00 00 00       	mov    $0x7d,%edx
     3aa:	be f5 18 00 00       	mov    $0x18f5,%esi
     3af:	89 c7                	mov    %eax,%edi
     3b1:	b8 00 00 00 00       	mov    $0x0,%eax
     3b6:	e8 32 0c 00 00       	callq  fed <printf>
     3bb:	8b 05 3f 1d 00 00    	mov    0x1d3f(%rip),%eax        # 2100 <stdout>
     3c1:	be aa 19 00 00       	mov    $0x19aa,%esi
     3c6:	89 c7                	mov    %eax,%edi
     3c8:	b8 00 00 00 00       	mov    $0x0,%eax
     3cd:	e8 1b 0c 00 00       	callq  fed <printf>
     3d2:	8b 05 28 1d 00 00    	mov    0x1d28(%rip),%eax        # 2100 <stdout>
     3d8:	be 1e 19 00 00       	mov    $0x191e,%esi
     3dd:	89 c7                	mov    %eax,%edi
     3df:	b8 00 00 00 00       	mov    $0x0,%eax
     3e4:	e8 04 0c 00 00       	callq  fed <printf>
     3e9:	eb fe                	jmp    3e9 <memtest+0xf4>
    }
    free(m1);
     3eb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     3ef:	48 89 c7             	mov    %rax,%rdi
     3f2:	e8 49 12 00 00       	callq  1640 <free>
    printf(stdout, "mem ok\n");
     3f7:	8b 05 03 1d 00 00    	mov    0x1d03(%rip),%eax        # 2100 <stdout>
     3fd:	be c4 19 00 00       	mov    $0x19c4,%esi
     402:	89 c7                	mov    %eax,%edi
     404:	b8 00 00 00 00       	mov    $0x0,%eax
     409:	e8 df 0b 00 00       	callq  fed <printf>
    exit();
     40e:	e8 7d 11 00 00       	callq  1590 <exit>
  } else {
    wait();
     413:	e8 80 11 00 00       	callq  1598 <wait>
  }
  printf(stdout, "memtest passed\n");
     418:	8b 05 e2 1c 00 00    	mov    0x1ce2(%rip),%eax        # 2100 <stdout>
     41e:	be cc 19 00 00       	mov    $0x19cc,%esi
     423:	89 c7                	mov    %eax,%edi
     425:	b8 00 00 00 00       	mov    $0x0,%eax
     42a:	e8 be 0b 00 00       	callq  fed <printf>
}
     42f:	90                   	nop
     430:	c9                   	leaveq 
     431:	c3                   	retq   

0000000000000432 <sbrktest>:

void sbrktest(void) {
     432:	55                   	push   %rbp
     433:	48 89 e5             	mov    %rsp,%rbp
     436:	53                   	push   %rbx
     437:	48 83 ec 78          	sub    $0x78,%rsp
  int fds[2], pid, pids[7];
  char *a, *b, *c, *lastaddr, *oldbrk, *p, scratch;
  uint64_t amt;

  printf(stdout, "sbrktest\n");
     43b:	8b 05 bf 1c 00 00    	mov    0x1cbf(%rip),%eax        # 2100 <stdout>
     441:	be dc 19 00 00       	mov    $0x19dc,%esi
     446:	89 c7                	mov    %eax,%edi
     448:	b8 00 00 00 00       	mov    $0x0,%eax
     44d:	e8 9b 0b 00 00       	callq  fed <printf>
  oldbrk = sbrk(0);
     452:	bf 00 00 00 00       	mov    $0x0,%edi
     457:	e8 bc 11 00 00       	callq  1618 <sbrk>
     45c:	48 89 45 d8          	mov    %rax,-0x28(%rbp)

  // can one sbrk() less than a page?
  a = sbrk(0);
     460:	bf 00 00 00 00       	mov    $0x0,%edi
     465:	e8 ae 11 00 00       	callq  1618 <sbrk>
     46a:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  int i;
  for (i = 0; i < 5000; i++) {
     46e:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%rbp)
     475:	e9 89 00 00 00       	jmpq   503 <sbrktest+0xd1>
    b = sbrk(1);
     47a:	bf 01 00 00 00       	mov    $0x1,%edi
     47f:	e8 94 11 00 00       	callq  1618 <sbrk>
     484:	48 89 45 d0          	mov    %rax,-0x30(%rbp)
    if (b != a) {
     488:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     48c:	48 3b 45 e8          	cmp    -0x18(%rbp),%rax
     490:	74 5a                	je     4ec <sbrktest+0xba>
      error("sbrk test failed %d %x %x\n", i, a, b);
     492:	8b 05 68 1c 00 00    	mov    0x1c68(%rip),%eax        # 2100 <stdout>
     498:	ba 96 00 00 00       	mov    $0x96,%edx
     49d:	be f5 18 00 00       	mov    $0x18f5,%esi
     4a2:	89 c7                	mov    %eax,%edi
     4a4:	b8 00 00 00 00       	mov    $0x0,%eax
     4a9:	e8 3f 0b 00 00       	callq  fed <printf>
     4ae:	8b 05 4c 1c 00 00    	mov    0x1c4c(%rip),%eax        # 2100 <stdout>
     4b4:	48 8b 75 d0          	mov    -0x30(%rbp),%rsi
     4b8:	48 8b 4d e8          	mov    -0x18(%rbp),%rcx
     4bc:	8b 55 e4             	mov    -0x1c(%rbp),%edx
     4bf:	49 89 f0             	mov    %rsi,%r8
     4c2:	be e6 19 00 00       	mov    $0x19e6,%esi
     4c7:	89 c7                	mov    %eax,%edi
     4c9:	b8 00 00 00 00       	mov    $0x0,%eax
     4ce:	e8 1a 0b 00 00       	callq  fed <printf>
     4d3:	8b 05 27 1c 00 00    	mov    0x1c27(%rip),%eax        # 2100 <stdout>
     4d9:	be 1e 19 00 00       	mov    $0x191e,%esi
     4de:	89 c7                	mov    %eax,%edi
     4e0:	b8 00 00 00 00       	mov    $0x0,%eax
     4e5:	e8 03 0b 00 00       	callq  fed <printf>
     4ea:	eb fe                	jmp    4ea <sbrktest+0xb8>
    }
    *b = 1;
     4ec:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     4f0:	c6 00 01             	movb   $0x1,(%rax)
    a = b + 1;
     4f3:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     4f7:	48 83 c0 01          	add    $0x1,%rax
     4fb:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  oldbrk = sbrk(0);

  // can one sbrk() less than a page?
  a = sbrk(0);
  int i;
  for (i = 0; i < 5000; i++) {
     4ff:	83 45 e4 01          	addl   $0x1,-0x1c(%rbp)
     503:	81 7d e4 87 13 00 00 	cmpl   $0x1387,-0x1c(%rbp)
     50a:	0f 8e 6a ff ff ff    	jle    47a <sbrktest+0x48>
      error("sbrk test failed %d %x %x\n", i, a, b);
    }
    *b = 1;
    a = b + 1;
  }
  pid = fork();
     510:	e8 73 10 00 00       	callq  1588 <fork>
     515:	89 45 cc             	mov    %eax,-0x34(%rbp)
  if (pid < 0) {
     518:	83 7d cc 00          	cmpl   $0x0,-0x34(%rbp)
     51c:	79 4c                	jns    56a <sbrktest+0x138>
    error("sbrk test fork failed\n");
     51e:	8b 05 dc 1b 00 00    	mov    0x1bdc(%rip),%eax        # 2100 <stdout>
     524:	ba 9d 00 00 00       	mov    $0x9d,%edx
     529:	be f5 18 00 00       	mov    $0x18f5,%esi
     52e:	89 c7                	mov    %eax,%edi
     530:	b8 00 00 00 00       	mov    $0x0,%eax
     535:	e8 b3 0a 00 00       	callq  fed <printf>
     53a:	8b 05 c0 1b 00 00    	mov    0x1bc0(%rip),%eax        # 2100 <stdout>
     540:	be 01 1a 00 00       	mov    $0x1a01,%esi
     545:	89 c7                	mov    %eax,%edi
     547:	b8 00 00 00 00       	mov    $0x0,%eax
     54c:	e8 9c 0a 00 00       	callq  fed <printf>
     551:	8b 05 a9 1b 00 00    	mov    0x1ba9(%rip),%eax        # 2100 <stdout>
     557:	be 1e 19 00 00       	mov    $0x191e,%esi
     55c:	89 c7                	mov    %eax,%edi
     55e:	b8 00 00 00 00       	mov    $0x0,%eax
     563:	e8 85 0a 00 00       	callq  fed <printf>
     568:	eb fe                	jmp    568 <sbrktest+0x136>
  }
  c = sbrk(1);
     56a:	bf 01 00 00 00       	mov    $0x1,%edi
     56f:	e8 a4 10 00 00       	callq  1618 <sbrk>
     574:	48 89 45 c0          	mov    %rax,-0x40(%rbp)
  c = sbrk(1);
     578:	bf 01 00 00 00       	mov    $0x1,%edi
     57d:	e8 96 10 00 00       	callq  1618 <sbrk>
     582:	48 89 45 c0          	mov    %rax,-0x40(%rbp)
  if (c != a + 1) {
     586:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     58a:	48 83 c0 01          	add    $0x1,%rax
     58e:	48 3b 45 c0          	cmp    -0x40(%rbp),%rax
     592:	74 4c                	je     5e0 <sbrktest+0x1ae>
    error("sbrk test failed post-fork\n");
     594:	8b 05 66 1b 00 00    	mov    0x1b66(%rip),%eax        # 2100 <stdout>
     59a:	ba a2 00 00 00       	mov    $0xa2,%edx
     59f:	be f5 18 00 00       	mov    $0x18f5,%esi
     5a4:	89 c7                	mov    %eax,%edi
     5a6:	b8 00 00 00 00       	mov    $0x0,%eax
     5ab:	e8 3d 0a 00 00       	callq  fed <printf>
     5b0:	8b 05 4a 1b 00 00    	mov    0x1b4a(%rip),%eax        # 2100 <stdout>
     5b6:	be 18 1a 00 00       	mov    $0x1a18,%esi
     5bb:	89 c7                	mov    %eax,%edi
     5bd:	b8 00 00 00 00       	mov    $0x0,%eax
     5c2:	e8 26 0a 00 00       	callq  fed <printf>
     5c7:	8b 05 33 1b 00 00    	mov    0x1b33(%rip),%eax        # 2100 <stdout>
     5cd:	be 1e 19 00 00       	mov    $0x191e,%esi
     5d2:	89 c7                	mov    %eax,%edi
     5d4:	b8 00 00 00 00       	mov    $0x0,%eax
     5d9:	e8 0f 0a 00 00       	callq  fed <printf>
     5de:	eb fe                	jmp    5de <sbrktest+0x1ac>
  }
  if (pid == 0)
     5e0:	83 7d cc 00          	cmpl   $0x0,-0x34(%rbp)
     5e4:	75 05                	jne    5eb <sbrktest+0x1b9>
    exit();
     5e6:	e8 a5 0f 00 00       	callq  1590 <exit>
  wait();
     5eb:	e8 a8 0f 00 00       	callq  1598 <wait>

  // can one grow address space to something big?
#define BIG (1 * 1024 * 1024)
  a = sbrk(0);
     5f0:	bf 00 00 00 00       	mov    $0x0,%edi
     5f5:	e8 1e 10 00 00       	callq  1618 <sbrk>
     5fa:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  amt = (BIG) - (uint64_t)a;
     5fe:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     602:	ba 00 00 10 00       	mov    $0x100000,%edx
     607:	48 29 c2             	sub    %rax,%rdx
     60a:	48 89 d0             	mov    %rdx,%rax
     60d:	48 89 45 b8          	mov    %rax,-0x48(%rbp)
  p = sbrk(amt);
     611:	48 8b 45 b8          	mov    -0x48(%rbp),%rax
     615:	89 c7                	mov    %eax,%edi
     617:	e8 fc 0f 00 00       	callq  1618 <sbrk>
     61c:	48 89 45 b0          	mov    %rax,-0x50(%rbp)
  if (p != a) {
     620:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     624:	48 3b 45 e8          	cmp    -0x18(%rbp),%rax
     628:	74 4c                	je     676 <sbrktest+0x244>
    error("sbrk test failed to grow big address space; enough phys mem?\n");
     62a:	8b 05 d0 1a 00 00    	mov    0x1ad0(%rip),%eax        # 2100 <stdout>
     630:	ba ae 00 00 00       	mov    $0xae,%edx
     635:	be f5 18 00 00       	mov    $0x18f5,%esi
     63a:	89 c7                	mov    %eax,%edi
     63c:	b8 00 00 00 00       	mov    $0x0,%eax
     641:	e8 a7 09 00 00       	callq  fed <printf>
     646:	8b 05 b4 1a 00 00    	mov    0x1ab4(%rip),%eax        # 2100 <stdout>
     64c:	be 38 1a 00 00       	mov    $0x1a38,%esi
     651:	89 c7                	mov    %eax,%edi
     653:	b8 00 00 00 00       	mov    $0x0,%eax
     658:	e8 90 09 00 00       	callq  fed <printf>
     65d:	8b 05 9d 1a 00 00    	mov    0x1a9d(%rip),%eax        # 2100 <stdout>
     663:	be 1e 19 00 00       	mov    $0x191e,%esi
     668:	89 c7                	mov    %eax,%edi
     66a:	b8 00 00 00 00       	mov    $0x0,%eax
     66f:	e8 79 09 00 00       	callq  fed <printf>
     674:	eb fe                	jmp    674 <sbrktest+0x242>
  }
  lastaddr = (char *)(BIG - 1);
     676:	48 c7 45 a8 ff ff 0f 	movq   $0xfffff,-0x58(%rbp)
     67d:	00 
  *lastaddr = 99;
     67e:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     682:	c6 00 63             	movb   $0x63,(%rax)

  // can we read the kernel's memory?
  for (a = (char *)(KERNBASE); a < (char *)(KERNBASE + 2000000); a += 50000) {
     685:	48 c7 45 e8 00 00 00 	movq   $0xffffffff80000000,-0x18(%rbp)
     68c:	80 
     68d:	e9 de 00 00 00       	jmpq   770 <sbrktest+0x33e>
    pid = fork();
     692:	e8 f1 0e 00 00       	callq  1588 <fork>
     697:	89 45 cc             	mov    %eax,-0x34(%rbp)
    if (pid < 0) {
     69a:	83 7d cc 00          	cmpl   $0x0,-0x34(%rbp)
     69e:	79 4c                	jns    6ec <sbrktest+0x2ba>
      error("fork failed\n");
     6a0:	8b 05 5a 1a 00 00    	mov    0x1a5a(%rip),%eax        # 2100 <stdout>
     6a6:	ba b7 00 00 00       	mov    $0xb7,%edx
     6ab:	be f5 18 00 00       	mov    $0x18f5,%esi
     6b0:	89 c7                	mov    %eax,%edi
     6b2:	b8 00 00 00 00       	mov    $0x0,%eax
     6b7:	e8 31 09 00 00       	callq  fed <printf>
     6bc:	8b 05 3e 1a 00 00    	mov    0x1a3e(%rip),%eax        # 2100 <stdout>
     6c2:	be 76 1a 00 00       	mov    $0x1a76,%esi
     6c7:	89 c7                	mov    %eax,%edi
     6c9:	b8 00 00 00 00       	mov    $0x0,%eax
     6ce:	e8 1a 09 00 00       	callq  fed <printf>
     6d3:	8b 05 27 1a 00 00    	mov    0x1a27(%rip),%eax        # 2100 <stdout>
     6d9:	be 1e 19 00 00       	mov    $0x191e,%esi
     6de:	89 c7                	mov    %eax,%edi
     6e0:	b8 00 00 00 00       	mov    $0x0,%eax
     6e5:	e8 03 09 00 00       	callq  fed <printf>
     6ea:	eb fe                	jmp    6ea <sbrktest+0x2b8>
    }
    if (pid == 0) {
     6ec:	83 7d cc 00          	cmpl   $0x0,-0x34(%rbp)
     6f0:	75 71                	jne    763 <sbrktest+0x331>
      printf(stdout, "oops could read %x = %x\n", a, *a);
     6f2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     6f6:	0f b6 00             	movzbl (%rax),%eax
     6f9:	0f be c8             	movsbl %al,%ecx
     6fc:	8b 05 fe 19 00 00    	mov    0x19fe(%rip),%eax        # 2100 <stdout>
     702:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     706:	be 83 1a 00 00       	mov    $0x1a83,%esi
     70b:	89 c7                	mov    %eax,%edi
     70d:	b8 00 00 00 00       	mov    $0x0,%eax
     712:	e8 d6 08 00 00       	callq  fed <printf>
      error("a bad process is not killed!");
     717:	8b 05 e3 19 00 00    	mov    0x19e3(%rip),%eax        # 2100 <stdout>
     71d:	ba bb 00 00 00       	mov    $0xbb,%edx
     722:	be f5 18 00 00       	mov    $0x18f5,%esi
     727:	89 c7                	mov    %eax,%edi
     729:	b8 00 00 00 00       	mov    $0x0,%eax
     72e:	e8 ba 08 00 00       	callq  fed <printf>
     733:	8b 05 c7 19 00 00    	mov    0x19c7(%rip),%eax        # 2100 <stdout>
     739:	be 9c 1a 00 00       	mov    $0x1a9c,%esi
     73e:	89 c7                	mov    %eax,%edi
     740:	b8 00 00 00 00       	mov    $0x0,%eax
     745:	e8 a3 08 00 00       	callq  fed <printf>
     74a:	8b 05 b0 19 00 00    	mov    0x19b0(%rip),%eax        # 2100 <stdout>
     750:	be 1e 19 00 00       	mov    $0x191e,%esi
     755:	89 c7                	mov    %eax,%edi
     757:	b8 00 00 00 00       	mov    $0x0,%eax
     75c:	e8 8c 08 00 00       	callq  fed <printf>
     761:	eb fe                	jmp    761 <sbrktest+0x32f>
    }
    wait();
     763:	e8 30 0e 00 00       	callq  1598 <wait>
  }
  lastaddr = (char *)(BIG - 1);
  *lastaddr = 99;

  // can we read the kernel's memory?
  for (a = (char *)(KERNBASE); a < (char *)(KERNBASE + 2000000); a += 50000) {
     768:	48 81 45 e8 50 c3 00 	addq   $0xc350,-0x18(%rbp)
     76f:	00 
     770:	48 81 7d e8 7f 84 1e 	cmpq   $0xffffffff801e847f,-0x18(%rbp)
     777:	80 
     778:	0f 86 14 ff ff ff    	jbe    692 <sbrktest+0x260>
    wait();
  }

  // if we run the system out of memory, does it clean up the last
  // failed allocation?
  if (pipe(fds) != 0) {
     77e:	48 8d 45 a0          	lea    -0x60(%rbp),%rax
     782:	48 89 c7             	mov    %rax,%rdi
     785:	e8 16 0e 00 00       	callq  15a0 <pipe>
     78a:	85 c0                	test   %eax,%eax
     78c:	74 4c                	je     7da <sbrktest+0x3a8>
    error("pipe() failed\n");
     78e:	8b 05 6c 19 00 00    	mov    0x196c(%rip),%eax        # 2100 <stdout>
     794:	ba c3 00 00 00       	mov    $0xc3,%edx
     799:	be f5 18 00 00       	mov    $0x18f5,%esi
     79e:	89 c7                	mov    %eax,%edi
     7a0:	b8 00 00 00 00       	mov    $0x0,%eax
     7a5:	e8 43 08 00 00       	callq  fed <printf>
     7aa:	8b 05 50 19 00 00    	mov    0x1950(%rip),%eax        # 2100 <stdout>
     7b0:	be b9 1a 00 00       	mov    $0x1ab9,%esi
     7b5:	89 c7                	mov    %eax,%edi
     7b7:	b8 00 00 00 00       	mov    $0x0,%eax
     7bc:	e8 2c 08 00 00       	callq  fed <printf>
     7c1:	8b 05 39 19 00 00    	mov    0x1939(%rip),%eax        # 2100 <stdout>
     7c7:	be 1e 19 00 00       	mov    $0x191e,%esi
     7cc:	89 c7                	mov    %eax,%edi
     7ce:	b8 00 00 00 00       	mov    $0x0,%eax
     7d3:	e8 15 08 00 00       	callq  fed <printf>
     7d8:	eb fe                	jmp    7d8 <sbrktest+0x3a6>
  }
  for (i = 0; i < sizeof(pids) / sizeof(pids[0]); i++) {
     7da:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%rbp)
     7e1:	eb 7f                	jmp    862 <sbrktest+0x430>
    if ((pids[i] = fork()) == 0) {
     7e3:	e8 a0 0d 00 00       	callq  1588 <fork>
     7e8:	89 c2                	mov    %eax,%edx
     7ea:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     7ed:	48 98                	cltq   
     7ef:	89 54 85 84          	mov    %edx,-0x7c(%rbp,%rax,4)
     7f3:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     7f6:	48 98                	cltq   
     7f8:	8b 44 85 84          	mov    -0x7c(%rbp,%rax,4),%eax
     7fc:	85 c0                	test   %eax,%eax
     7fe:	75 3a                	jne    83a <sbrktest+0x408>
      // allocate a lot of memory
      sbrk(BIG - (uint64_t)sbrk(0));
     800:	bf 00 00 00 00       	mov    $0x0,%edi
     805:	e8 0e 0e 00 00       	callq  1618 <sbrk>
     80a:	ba 00 00 10 00       	mov    $0x100000,%edx
     80f:	29 c2                	sub    %eax,%edx
     811:	89 d0                	mov    %edx,%eax
     813:	89 c7                	mov    %eax,%edi
     815:	e8 fe 0d 00 00       	callq  1618 <sbrk>
      write(fds[1], "x", 1);
     81a:	8b 45 a4             	mov    -0x5c(%rbp),%eax
     81d:	ba 01 00 00 00       	mov    $0x1,%edx
     822:	be 51 19 00 00       	mov    $0x1951,%esi
     827:	89 c7                	mov    %eax,%edi
     829:	e8 82 0d 00 00       	callq  15b0 <write>
      // sit around until killed
      for (;;)
        sleep(1000);
     82e:	bf e8 03 00 00       	mov    $0x3e8,%edi
     833:	e8 e8 0d 00 00       	callq  1620 <sleep>
     838:	eb f4                	jmp    82e <sbrktest+0x3fc>
    }
    if (pids[i] != -1)
     83a:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     83d:	48 98                	cltq   
     83f:	8b 44 85 84          	mov    -0x7c(%rbp,%rax,4),%eax
     843:	83 f8 ff             	cmp    $0xffffffff,%eax
     846:	74 16                	je     85e <sbrktest+0x42c>
      read(fds[0], &scratch, 1);
     848:	8b 45 a0             	mov    -0x60(%rbp),%eax
     84b:	48 8d 4d 83          	lea    -0x7d(%rbp),%rcx
     84f:	ba 01 00 00 00       	mov    $0x1,%edx
     854:	48 89 ce             	mov    %rcx,%rsi
     857:	89 c7                	mov    %eax,%edi
     859:	e8 4a 0d 00 00       	callq  15a8 <read>
  // if we run the system out of memory, does it clean up the last
  // failed allocation?
  if (pipe(fds) != 0) {
    error("pipe() failed\n");
  }
  for (i = 0; i < sizeof(pids) / sizeof(pids[0]); i++) {
     85e:	83 45 e4 01          	addl   $0x1,-0x1c(%rbp)
     862:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     865:	83 f8 06             	cmp    $0x6,%eax
     868:	0f 86 75 ff ff ff    	jbe    7e3 <sbrktest+0x3b1>
      read(fds[0], &scratch, 1);
  }

  // if those failed allocations freed up the pages they did allocate,
  // we'll be able to allocate here
  c = sbrk(4096);
     86e:	bf 00 10 00 00       	mov    $0x1000,%edi
     873:	e8 a0 0d 00 00       	callq  1618 <sbrk>
     878:	48 89 45 c0          	mov    %rax,-0x40(%rbp)
  for (i = 0; i < sizeof(pids) / sizeof(pids[0]); i++) {
     87c:	c7 45 e4 00 00 00 00 	movl   $0x0,-0x1c(%rbp)
     883:	eb 2a                	jmp    8af <sbrktest+0x47d>
    if (pids[i] == -1)
     885:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     888:	48 98                	cltq   
     88a:	8b 44 85 84          	mov    -0x7c(%rbp,%rax,4),%eax
     88e:	83 f8 ff             	cmp    $0xffffffff,%eax
     891:	74 17                	je     8aa <sbrktest+0x478>
      continue;
    kill(pids[i]);
     893:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     896:	48 98                	cltq   
     898:	8b 44 85 84          	mov    -0x7c(%rbp,%rax,4),%eax
     89c:	89 c7                	mov    %eax,%edi
     89e:	e8 1d 0d 00 00       	callq  15c0 <kill>
    wait();
     8a3:	e8 f0 0c 00 00       	callq  1598 <wait>
     8a8:	eb 01                	jmp    8ab <sbrktest+0x479>
  // if those failed allocations freed up the pages they did allocate,
  // we'll be able to allocate here
  c = sbrk(4096);
  for (i = 0; i < sizeof(pids) / sizeof(pids[0]); i++) {
    if (pids[i] == -1)
      continue;
     8aa:	90                   	nop
  }

  // if those failed allocations freed up the pages they did allocate,
  // we'll be able to allocate here
  c = sbrk(4096);
  for (i = 0; i < sizeof(pids) / sizeof(pids[0]); i++) {
     8ab:	83 45 e4 01          	addl   $0x1,-0x1c(%rbp)
     8af:	8b 45 e4             	mov    -0x1c(%rbp),%eax
     8b2:	83 f8 06             	cmp    $0x6,%eax
     8b5:	76 ce                	jbe    885 <sbrktest+0x453>
    if (pids[i] == -1)
      continue;
    kill(pids[i]);
    wait();
  }
  if (c == (char *)0xffffffff) {
     8b7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
     8bc:	48 39 45 c0          	cmp    %rax,-0x40(%rbp)
     8c0:	75 4c                	jne    90e <sbrktest+0x4dc>
    error("failed sbrk leaked memory\n");
     8c2:	8b 05 38 18 00 00    	mov    0x1838(%rip),%eax        # 2100 <stdout>
     8c8:	ba dc 00 00 00       	mov    $0xdc,%edx
     8cd:	be f5 18 00 00       	mov    $0x18f5,%esi
     8d2:	89 c7                	mov    %eax,%edi
     8d4:	b8 00 00 00 00       	mov    $0x0,%eax
     8d9:	e8 0f 07 00 00       	callq  fed <printf>
     8de:	8b 05 1c 18 00 00    	mov    0x181c(%rip),%eax        # 2100 <stdout>
     8e4:	be c8 1a 00 00       	mov    $0x1ac8,%esi
     8e9:	89 c7                	mov    %eax,%edi
     8eb:	b8 00 00 00 00       	mov    $0x0,%eax
     8f0:	e8 f8 06 00 00       	callq  fed <printf>
     8f5:	8b 05 05 18 00 00    	mov    0x1805(%rip),%eax        # 2100 <stdout>
     8fb:	be 1e 19 00 00       	mov    $0x191e,%esi
     900:	89 c7                	mov    %eax,%edi
     902:	b8 00 00 00 00       	mov    $0x0,%eax
     907:	e8 e1 06 00 00       	callq  fed <printf>
     90c:	eb fe                	jmp    90c <sbrktest+0x4da>
  }

  if (sbrk(0) > oldbrk)
     90e:	bf 00 00 00 00       	mov    $0x0,%edi
     913:	e8 00 0d 00 00       	callq  1618 <sbrk>
     918:	48 3b 45 d8          	cmp    -0x28(%rbp),%rax
     91c:	76 1b                	jbe    939 <sbrktest+0x507>
    sbrk(-(sbrk(0) - oldbrk));
     91e:	48 8b 5d d8          	mov    -0x28(%rbp),%rbx
     922:	bf 00 00 00 00       	mov    $0x0,%edi
     927:	e8 ec 0c 00 00       	callq  1618 <sbrk>
     92c:	48 29 c3             	sub    %rax,%rbx
     92f:	48 89 d8             	mov    %rbx,%rax
     932:	89 c7                	mov    %eax,%edi
     934:	e8 df 0c 00 00       	callq  1618 <sbrk>

  printf(stdout, "sbrktest OK\n");
     939:	8b 05 c1 17 00 00    	mov    0x17c1(%rip),%eax        # 2100 <stdout>
     93f:	be e3 1a 00 00       	mov    $0x1ae3,%esi
     944:	89 c7                	mov    %eax,%edi
     946:	b8 00 00 00 00       	mov    $0x0,%eax
     94b:	e8 9d 06 00 00       	callq  fed <printf>
}
     950:	90                   	nop
     951:	48 83 c4 78          	add    $0x78,%rsp
     955:	5b                   	pop    %rbx
     956:	5d                   	pop    %rbp
     957:	c3                   	retq   

0000000000000958 <growstacktest>:

void growstacktest() {
     958:	55                   	push   %rbp
     959:	48 89 e5             	mov    %rsp,%rbp
     95c:	41 57                	push   %r15
     95e:	41 56                	push   %r14
     960:	41 55                	push   %r13
     962:	41 54                	push   %r12
     964:	53                   	push   %rbx
     965:	48 83 ec 48          	sub    $0x48,%rsp
     969:	48 89 e0             	mov    %rsp,%rax
     96c:	48 89 c3             	mov    %rax,%rbx
  int i;
  struct sys_info info1, info2;
  printf(stdout, "growstacktest\n");
     96f:	8b 05 8b 17 00 00    	mov    0x178b(%rip),%eax        # 2100 <stdout>
     975:	be f0 1a 00 00       	mov    $0x1af0,%esi
     97a:	89 c7                	mov    %eax,%edi
     97c:	b8 00 00 00 00       	mov    $0x0,%eax
     981:	e8 67 06 00 00       	callq  fed <printf>
  sysinfo(&info1);
     986:	48 8d 45 a4          	lea    -0x5c(%rbp),%rax
     98a:	48 89 c7             	mov    %rax,%rdi
     98d:	e8 9e 0c 00 00       	callq  1630 <sysinfo>

  printf(stdout, "pages_in_use before stack allocation = %d\n",
     992:	8b 55 a4             	mov    -0x5c(%rbp),%edx
     995:	8b 05 65 17 00 00    	mov    0x1765(%rip),%eax        # 2100 <stdout>
     99b:	be 00 1b 00 00       	mov    $0x1b00,%esi
     9a0:	89 c7                	mov    %eax,%edi
     9a2:	b8 00 00 00 00       	mov    $0x0,%eax
     9a7:	e8 41 06 00 00       	callq  fed <printf>
         info1.pages_in_use);
  printf(stdout, "pages_in_swap before stack allocation = %d\n",
     9ac:	8b 55 a8             	mov    -0x58(%rbp),%edx
     9af:	8b 05 4b 17 00 00    	mov    0x174b(%rip),%eax        # 2100 <stdout>
     9b5:	be 30 1b 00 00       	mov    $0x1b30,%esi
     9ba:	89 c7                	mov    %eax,%edi
     9bc:	b8 00 00 00 00       	mov    $0x0,%eax
     9c1:	e8 27 06 00 00       	callq  fed <printf>
         info1.pages_in_swap);

  int page8 = 8 * 4096;
     9c6:	c7 45 c8 00 80 00 00 	movl   $0x8000,-0x38(%rbp)
  char buf[page8];
     9cd:	8b 45 c8             	mov    -0x38(%rbp),%eax
     9d0:	48 63 d0             	movslq %eax,%rdx
     9d3:	48 83 ea 01          	sub    $0x1,%rdx
     9d7:	48 89 55 c0          	mov    %rdx,-0x40(%rbp)
     9db:	48 63 d0             	movslq %eax,%rdx
     9de:	49 89 d6             	mov    %rdx,%r14
     9e1:	41 bf 00 00 00 00    	mov    $0x0,%r15d
     9e7:	48 63 d0             	movslq %eax,%rdx
     9ea:	49 89 d4             	mov    %rdx,%r12
     9ed:	41 bd 00 00 00 00    	mov    $0x0,%r13d
     9f3:	48 98                	cltq   
     9f5:	ba 10 00 00 00       	mov    $0x10,%edx
     9fa:	48 83 ea 01          	sub    $0x1,%rdx
     9fe:	48 01 d0             	add    %rdx,%rax
     a01:	b9 10 00 00 00       	mov    $0x10,%ecx
     a06:	ba 00 00 00 00       	mov    $0x0,%edx
     a0b:	48 f7 f1             	div    %rcx
     a0e:	48 6b c0 10          	imul   $0x10,%rax,%rax
     a12:	48 29 c4             	sub    %rax,%rsp
     a15:	48 89 e0             	mov    %rsp,%rax
     a18:	48 83 c0 00          	add    $0x0,%rax
     a1c:	48 89 45 b8          	mov    %rax,-0x48(%rbp)
  for (i = 0; i < 8; i++) {
     a20:	c7 45 cc 00 00 00 00 	movl   $0x0,-0x34(%rbp)
     a27:	eb 37                	jmp    a60 <growstacktest+0x108>
    buf[i * 4096] = 'a';
     a29:	8b 45 cc             	mov    -0x34(%rbp),%eax
     a2c:	c1 e0 0c             	shl    $0xc,%eax
     a2f:	48 8b 55 b8          	mov    -0x48(%rbp),%rdx
     a33:	48 98                	cltq   
     a35:	c6 04 02 61          	movb   $0x61,(%rdx,%rax,1)
    buf[i * 4096 - 1] = buf[i * 4096];
     a39:	8b 45 cc             	mov    -0x34(%rbp),%eax
     a3c:	c1 e0 0c             	shl    $0xc,%eax
     a3f:	8d 70 ff             	lea    -0x1(%rax),%esi
     a42:	8b 45 cc             	mov    -0x34(%rbp),%eax
     a45:	c1 e0 0c             	shl    $0xc,%eax
     a48:	48 8b 55 b8          	mov    -0x48(%rbp),%rdx
     a4c:	48 98                	cltq   
     a4e:	0f b6 0c 02          	movzbl (%rdx,%rax,1),%ecx
     a52:	48 8b 55 b8          	mov    -0x48(%rbp),%rdx
     a56:	48 63 c6             	movslq %esi,%rax
     a59:	88 0c 02             	mov    %cl,(%rdx,%rax,1)
  printf(stdout, "pages_in_swap before stack allocation = %d\n",
         info1.pages_in_swap);

  int page8 = 8 * 4096;
  char buf[page8];
  for (i = 0; i < 8; i++) {
     a5c:	83 45 cc 01          	addl   $0x1,-0x34(%rbp)
     a60:	83 7d cc 07          	cmpl   $0x7,-0x34(%rbp)
     a64:	7e c3                	jle    a29 <growstacktest+0xd1>
    buf[i * 4096] = 'a';
    buf[i * 4096 - 1] = buf[i * 4096];
  }
  sysinfo(&info2);
     a66:	48 8d 45 90          	lea    -0x70(%rbp),%rax
     a6a:	48 89 c7             	mov    %rax,%rdi
     a6d:	e8 be 0b 00 00       	callq  1630 <sysinfo>

  printf(stdout, "pages_in_use after stack allocation = %d\n",
     a72:	8b 55 90             	mov    -0x70(%rbp),%edx
     a75:	8b 05 85 16 00 00    	mov    0x1685(%rip),%eax        # 2100 <stdout>
     a7b:	be 60 1b 00 00       	mov    $0x1b60,%esi
     a80:	89 c7                	mov    %eax,%edi
     a82:	b8 00 00 00 00       	mov    $0x0,%eax
     a87:	e8 61 05 00 00       	callq  fed <printf>
         info2.pages_in_use);
  printf(stdout, "pages_in_swap after stack allocation = %d\n",
     a8c:	8b 55 94             	mov    -0x6c(%rbp),%edx
     a8f:	8b 05 6b 16 00 00    	mov    0x166b(%rip),%eax        # 2100 <stdout>
     a95:	be 90 1b 00 00       	mov    $0x1b90,%esi
     a9a:	89 c7                	mov    %eax,%edi
     a9c:	b8 00 00 00 00       	mov    $0x0,%eax
     aa1:	e8 47 05 00 00       	callq  fed <printf>
         info2.pages_in_swap);

  // if grow ustack on-demand is implemented, then the 8 pages are allocated at
  // run-time
  if (info2.pages_in_use - info1.pages_in_use + info1.pages_in_swap -
     aa6:	8b 55 90             	mov    -0x70(%rbp),%edx
     aa9:	8b 45 a4             	mov    -0x5c(%rbp),%eax
     aac:	29 c2                	sub    %eax,%edx
     aae:	8b 45 a8             	mov    -0x58(%rbp),%eax
     ab1:	01 c2                	add    %eax,%edx
          info2.pages_in_swap !=
     ab3:	8b 45 94             	mov    -0x6c(%rbp),%eax
  printf(stdout, "pages_in_swap after stack allocation = %d\n",
         info2.pages_in_swap);

  // if grow ustack on-demand is implemented, then the 8 pages are allocated at
  // run-time
  if (info2.pages_in_use - info1.pages_in_use + info1.pages_in_swap -
     ab6:	29 c2                	sub    %eax,%edx
     ab8:	89 d0                	mov    %edx,%eax
     aba:	83 f8 08             	cmp    $0x8,%eax
     abd:	74 4c                	je     b0b <growstacktest+0x1b3>
          info2.pages_in_swap !=
      8)
    error("user stack is not growing");
     abf:	8b 05 3b 16 00 00    	mov    0x163b(%rip),%eax        # 2100 <stdout>
     ac5:	ba 02 01 00 00       	mov    $0x102,%edx
     aca:	be f5 18 00 00       	mov    $0x18f5,%esi
     acf:	89 c7                	mov    %eax,%edi
     ad1:	b8 00 00 00 00       	mov    $0x0,%eax
     ad6:	e8 12 05 00 00       	callq  fed <printf>
     adb:	8b 05 1f 16 00 00    	mov    0x161f(%rip),%eax        # 2100 <stdout>
     ae1:	be bb 1b 00 00       	mov    $0x1bbb,%esi
     ae6:	89 c7                	mov    %eax,%edi
     ae8:	b8 00 00 00 00       	mov    $0x0,%eax
     aed:	e8 fb 04 00 00       	callq  fed <printf>
     af2:	8b 05 08 16 00 00    	mov    0x1608(%rip),%eax        # 2100 <stdout>
     af8:	be 1e 19 00 00       	mov    $0x191e,%esi
     afd:	89 c7                	mov    %eax,%edi
     aff:	b8 00 00 00 00       	mov    $0x0,%eax
     b04:	e8 e4 04 00 00       	callq  fed <printf>
     b09:	eb fe                	jmp    b09 <growstacktest+0x1b1>

  printf(stdout, "growstacktest passed\n");
     b0b:	8b 05 ef 15 00 00    	mov    0x15ef(%rip),%eax        # 2100 <stdout>
     b11:	be d5 1b 00 00       	mov    $0x1bd5,%esi
     b16:	89 c7                	mov    %eax,%edi
     b18:	b8 00 00 00 00       	mov    $0x0,%eax
     b1d:	e8 cb 04 00 00       	callq  fed <printf>
     b22:	48 89 dc             	mov    %rbx,%rsp
}
     b25:	90                   	nop
     b26:	48 8d 65 d8          	lea    -0x28(%rbp),%rsp
     b2a:	5b                   	pop    %rbx
     b2b:	41 5c                	pop    %r12
     b2d:	41 5d                	pop    %r13
     b2f:	41 5e                	pop    %r14
     b31:	41 5f                	pop    %r15
     b33:	5d                   	pop    %rbp
     b34:	c3                   	retq   

0000000000000b35 <copyonwriteforktest>:

void copyonwriteforktest() {
     b35:	55                   	push   %rbp
     b36:	48 89 e5             	mov    %rsp,%rbp
     b39:	48 83 ec 70          	sub    $0x70,%rsp
  struct sys_info info1, info2, info3, info4;
  int page200 = 200 * 4096;
     b3d:	c7 45 f4 00 80 0c 00 	movl   $0xc8000,-0xc(%rbp)
  char *a = sbrk(page200);
     b44:	8b 45 f4             	mov    -0xc(%rbp),%eax
     b47:	89 c7                	mov    %eax,%edi
     b49:	e8 ca 0a 00 00       	callq  1618 <sbrk>
     b4e:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  char b;
  int j;

  a[0] = 0;
     b52:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     b56:	c6 00 00             	movb   $0x0,(%rax)
  printf(stdout, "copyonwriteforktest\n");
     b59:	8b 05 a1 15 00 00    	mov    0x15a1(%rip),%eax        # 2100 <stdout>
     b5f:	be eb 1b 00 00       	mov    $0x1beb,%esi
     b64:	89 c7                	mov    %eax,%edi
     b66:	b8 00 00 00 00       	mov    $0x0,%eax
     b6b:	e8 7d 04 00 00       	callq  fed <printf>
  sysinfo(&info1);
     b70:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
     b74:	48 89 c7             	mov    %rax,%rdi
     b77:	e8 b4 0a 00 00       	callq  1630 <sysinfo>
  printf(stdout, "pages_in_use before copy-on-write fork = %d\n",
     b7c:	8b 55 d0             	mov    -0x30(%rbp),%edx
     b7f:	8b 05 7b 15 00 00    	mov    0x157b(%rip),%eax        # 2100 <stdout>
     b85:	be 00 1c 00 00       	mov    $0x1c00,%esi
     b8a:	89 c7                	mov    %eax,%edi
     b8c:	b8 00 00 00 00       	mov    $0x0,%eax
     b91:	e8 57 04 00 00       	callq  fed <printf>
         info1.pages_in_use);
  printf(stdout, "pages_in_swap before copy-on-write fork = %d\n",
     b96:	8b 55 d4             	mov    -0x2c(%rbp),%edx
     b99:	8b 05 61 15 00 00    	mov    0x1561(%rip),%eax        # 2100 <stdout>
     b9f:	be 30 1c 00 00       	mov    $0x1c30,%esi
     ba4:	89 c7                	mov    %eax,%edi
     ba6:	b8 00 00 00 00       	mov    $0x0,%eax
     bab:	e8 3d 04 00 00       	callq  fed <printf>
         info1.pages_in_swap);
  int pid = fork();
     bb0:	e8 d3 09 00 00       	callq  1588 <fork>
     bb5:	89 45 e4             	mov    %eax,-0x1c(%rbp)
  if (pid == 0) {
     bb8:	83 7d e4 00          	cmpl   $0x0,-0x1c(%rbp)
     bbc:	75 0f                	jne    bcd <copyonwriteforktest+0x98>
    sleep(100);
     bbe:	bf 64 00 00 00       	mov    $0x64,%edi
     bc3:	e8 58 0a 00 00       	callq  1620 <sleep>
    exit();
     bc8:	e8 c3 09 00 00       	callq  1590 <exit>
  } else {
    sysinfo(&info2);
     bcd:	48 8d 45 bc          	lea    -0x44(%rbp),%rax
     bd1:	48 89 c7             	mov    %rax,%rdi
     bd4:	e8 57 0a 00 00       	callq  1630 <sysinfo>
    printf(stdout, "pages_in_use after copy-on-write fork = %d\n",
     bd9:	8b 55 bc             	mov    -0x44(%rbp),%edx
     bdc:	8b 05 1e 15 00 00    	mov    0x151e(%rip),%eax        # 2100 <stdout>
     be2:	be 60 1c 00 00       	mov    $0x1c60,%esi
     be7:	89 c7                	mov    %eax,%edi
     be9:	b8 00 00 00 00       	mov    $0x0,%eax
     bee:	e8 fa 03 00 00       	callq  fed <printf>
           info2.pages_in_use);
    printf(stdout, "pages_in_swap after copy-on-write fork = %d\n",
     bf3:	8b 55 c0             	mov    -0x40(%rbp),%edx
     bf6:	8b 05 04 15 00 00    	mov    0x1504(%rip),%eax        # 2100 <stdout>
     bfc:	be 90 1c 00 00       	mov    $0x1c90,%esi
     c01:	89 c7                	mov    %eax,%edi
     c03:	b8 00 00 00 00       	mov    $0x0,%eax
     c08:	e8 e0 03 00 00       	callq  fed <printf>
           info2.pages_in_swap);

    // if copy-on-write is implemented, there is no way a new process can take
    // more than 100 pages
    if (info2.pages_in_use - info1.pages_in_use + info1.pages_in_swap -
     c0d:	8b 55 bc             	mov    -0x44(%rbp),%edx
     c10:	8b 45 d0             	mov    -0x30(%rbp),%eax
     c13:	29 c2                	sub    %eax,%edx
     c15:	8b 45 d4             	mov    -0x2c(%rbp),%eax
     c18:	01 c2                	add    %eax,%edx
            info2.pages_in_swap >
     c1a:	8b 45 c0             	mov    -0x40(%rbp),%eax
    printf(stdout, "pages_in_swap after copy-on-write fork = %d\n",
           info2.pages_in_swap);

    // if copy-on-write is implemented, there is no way a new process can take
    // more than 100 pages
    if (info2.pages_in_use - info1.pages_in_use + info1.pages_in_swap -
     c1d:	29 c2                	sub    %eax,%edx
     c1f:	89 d0                	mov    %edx,%eax
     c21:	83 f8 64             	cmp    $0x64,%eax
     c24:	7e 4c                	jle    c72 <copyonwriteforktest+0x13d>
            info2.pages_in_swap >
        100)
      error("too much memory is used for fork");
     c26:	8b 05 d4 14 00 00    	mov    0x14d4(%rip),%eax        # 2100 <stdout>
     c2c:	ba 25 01 00 00       	mov    $0x125,%edx
     c31:	be f5 18 00 00       	mov    $0x18f5,%esi
     c36:	89 c7                	mov    %eax,%edi
     c38:	b8 00 00 00 00       	mov    $0x0,%eax
     c3d:	e8 ab 03 00 00       	callq  fed <printf>
     c42:	8b 05 b8 14 00 00    	mov    0x14b8(%rip),%eax        # 2100 <stdout>
     c48:	be c0 1c 00 00       	mov    $0x1cc0,%esi
     c4d:	89 c7                	mov    %eax,%edi
     c4f:	b8 00 00 00 00       	mov    $0x0,%eax
     c54:	e8 94 03 00 00       	callq  fed <printf>
     c59:	8b 05 a1 14 00 00    	mov    0x14a1(%rip),%eax        # 2100 <stdout>
     c5f:	be 1e 19 00 00       	mov    $0x191e,%esi
     c64:	89 c7                	mov    %eax,%edi
     c66:	b8 00 00 00 00       	mov    $0x0,%eax
     c6b:	e8 7d 03 00 00       	callq  fed <printf>
     c70:	eb fe                	jmp    c70 <copyonwriteforktest+0x13b>

    for (j = 0; j < 200; j++) {
     c72:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     c79:	eb 1a                	jmp    c95 <copyonwriteforktest+0x160>
      b = a[j * 4096];
     c7b:	8b 45 f8             	mov    -0x8(%rbp),%eax
     c7e:	c1 e0 0c             	shl    $0xc,%eax
     c81:	48 63 d0             	movslq %eax,%rdx
     c84:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c88:	48 01 d0             	add    %rdx,%rax
     c8b:	0f b6 00             	movzbl (%rax),%eax
     c8e:	88 45 ff             	mov    %al,-0x1(%rbp)
    if (info2.pages_in_use - info1.pages_in_use + info1.pages_in_swap -
            info2.pages_in_swap >
        100)
      error("too much memory is used for fork");

    for (j = 0; j < 200; j++) {
     c91:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     c95:	81 7d f8 c7 00 00 00 	cmpl   $0xc7,-0x8(%rbp)
     c9c:	7e dd                	jle    c7b <copyonwriteforktest+0x146>
      b = a[j * 4096];
    }

    sysinfo(&info3);
     c9e:	48 8d 45 a8          	lea    -0x58(%rbp),%rax
     ca2:	48 89 c7             	mov    %rax,%rdi
     ca5:	e8 86 09 00 00       	callq  1630 <sysinfo>
    printf(stdout, "pages_in_use after read = %d\n", info3.pages_in_use);
     caa:	8b 55 a8             	mov    -0x58(%rbp),%edx
     cad:	8b 05 4d 14 00 00    	mov    0x144d(%rip),%eax        # 2100 <stdout>
     cb3:	be e1 1c 00 00       	mov    $0x1ce1,%esi
     cb8:	89 c7                	mov    %eax,%edi
     cba:	b8 00 00 00 00       	mov    $0x0,%eax
     cbf:	e8 29 03 00 00       	callq  fed <printf>
    printf(stdout, "pages_in_swap after read = %d\n", info3.pages_in_swap);
     cc4:	8b 55 ac             	mov    -0x54(%rbp),%edx
     cc7:	8b 05 33 14 00 00    	mov    0x1433(%rip),%eax        # 2100 <stdout>
     ccd:	be 00 1d 00 00       	mov    $0x1d00,%esi
     cd2:	89 c7                	mov    %eax,%edi
     cd4:	b8 00 00 00 00       	mov    $0x0,%eax
     cd9:	e8 0f 03 00 00       	callq  fed <printf>

    // Read should not increase the amount of memory allocated
    if (info3.pages_in_use - info2.pages_in_use + info3.pages_in_swap -
     cde:	8b 55 a8             	mov    -0x58(%rbp),%edx
     ce1:	8b 45 bc             	mov    -0x44(%rbp),%eax
     ce4:	29 c2                	sub    %eax,%edx
     ce6:	8b 45 ac             	mov    -0x54(%rbp),%eax
     ce9:	01 c2                	add    %eax,%edx
            info2.pages_in_swap >
     ceb:	8b 45 c0             	mov    -0x40(%rbp),%eax
    sysinfo(&info3);
    printf(stdout, "pages_in_use after read = %d\n", info3.pages_in_use);
    printf(stdout, "pages_in_swap after read = %d\n", info3.pages_in_swap);

    // Read should not increase the amount of memory allocated
    if (info3.pages_in_use - info2.pages_in_use + info3.pages_in_swap -
     cee:	29 c2                	sub    %eax,%edx
     cf0:	89 d0                	mov    %edx,%eax
     cf2:	83 f8 64             	cmp    $0x64,%eax
     cf5:	7e 4c                	jle    d43 <copyonwriteforktest+0x20e>
            info2.pages_in_swap >
        100)
      error("too much memory is used for read");
     cf7:	8b 05 03 14 00 00    	mov    0x1403(%rip),%eax        # 2100 <stdout>
     cfd:	ba 33 01 00 00       	mov    $0x133,%edx
     d02:	be f5 18 00 00       	mov    $0x18f5,%esi
     d07:	89 c7                	mov    %eax,%edi
     d09:	b8 00 00 00 00       	mov    $0x0,%eax
     d0e:	e8 da 02 00 00       	callq  fed <printf>
     d13:	8b 05 e7 13 00 00    	mov    0x13e7(%rip),%eax        # 2100 <stdout>
     d19:	be 20 1d 00 00       	mov    $0x1d20,%esi
     d1e:	89 c7                	mov    %eax,%edi
     d20:	b8 00 00 00 00       	mov    $0x0,%eax
     d25:	e8 c3 02 00 00       	callq  fed <printf>
     d2a:	8b 05 d0 13 00 00    	mov    0x13d0(%rip),%eax        # 2100 <stdout>
     d30:	be 1e 19 00 00       	mov    $0x191e,%esi
     d35:	89 c7                	mov    %eax,%edi
     d37:	b8 00 00 00 00       	mov    $0x0,%eax
     d3c:	e8 ac 02 00 00       	callq  fed <printf>
     d41:	eb fe                	jmp    d41 <copyonwriteforktest+0x20c>

    for (j = 0; j < 200; j++) {
     d43:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     d4a:	eb 1a                	jmp    d66 <copyonwriteforktest+0x231>
      a[j * 4096] = b;
     d4c:	8b 45 f8             	mov    -0x8(%rbp),%eax
     d4f:	c1 e0 0c             	shl    $0xc,%eax
     d52:	48 63 d0             	movslq %eax,%rdx
     d55:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d59:	48 01 c2             	add    %rax,%rdx
     d5c:	0f b6 45 ff          	movzbl -0x1(%rbp),%eax
     d60:	88 02                	mov    %al,(%rdx)
    if (info3.pages_in_use - info2.pages_in_use + info3.pages_in_swap -
            info2.pages_in_swap >
        100)
      error("too much memory is used for read");

    for (j = 0; j < 200; j++) {
     d62:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     d66:	81 7d f8 c7 00 00 00 	cmpl   $0xc7,-0x8(%rbp)
     d6d:	7e dd                	jle    d4c <copyonwriteforktest+0x217>
      a[j * 4096] = b;
    }

    sysinfo(&info4);
     d6f:	48 8d 45 94          	lea    -0x6c(%rbp),%rax
     d73:	48 89 c7             	mov    %rax,%rdi
     d76:	e8 b5 08 00 00       	callq  1630 <sysinfo>
    printf(stdout, "pages_in_use after write = %d\n", info4.pages_in_use);
     d7b:	8b 55 94             	mov    -0x6c(%rbp),%edx
     d7e:	8b 05 7c 13 00 00    	mov    0x137c(%rip),%eax        # 2100 <stdout>
     d84:	be 48 1d 00 00       	mov    $0x1d48,%esi
     d89:	89 c7                	mov    %eax,%edi
     d8b:	b8 00 00 00 00       	mov    $0x0,%eax
     d90:	e8 58 02 00 00       	callq  fed <printf>
    printf(stdout, "pages_in_swap after write = %d\n", info4.pages_in_swap);
     d95:	8b 55 98             	mov    -0x68(%rbp),%edx
     d98:	8b 05 62 13 00 00    	mov    0x1362(%rip),%eax        # 2100 <stdout>
     d9e:	be 68 1d 00 00       	mov    $0x1d68,%esi
     da3:	89 c7                	mov    %eax,%edi
     da5:	b8 00 00 00 00       	mov    $0x0,%eax
     daa:	e8 3e 02 00 00       	callq  fed <printf>

    // Write should allocate the 200 pages of memory
    if (info4.pages_in_use - info3.pages_in_use + info4.pages_in_swap -
     daf:	8b 55 94             	mov    -0x6c(%rbp),%edx
     db2:	8b 45 a8             	mov    -0x58(%rbp),%eax
     db5:	29 c2                	sub    %eax,%edx
     db7:	8b 45 98             	mov    -0x68(%rbp),%eax
     dba:	01 c2                	add    %eax,%edx
            info3.pages_in_swap <
     dbc:	8b 45 ac             	mov    -0x54(%rbp),%eax
    sysinfo(&info4);
    printf(stdout, "pages_in_use after write = %d\n", info4.pages_in_use);
    printf(stdout, "pages_in_swap after write = %d\n", info4.pages_in_swap);

    // Write should allocate the 200 pages of memory
    if (info4.pages_in_use - info3.pages_in_use + info4.pages_in_swap -
     dbf:	29 c2                	sub    %eax,%edx
     dc1:	89 d0                	mov    %edx,%eax
     dc3:	83 f8 63             	cmp    $0x63,%eax
     dc6:	7f 4c                	jg     e14 <copyonwriteforktest+0x2df>
            info3.pages_in_swap <
        100)
      error("too less memory is used for write");
     dc8:	8b 05 32 13 00 00    	mov    0x1332(%rip),%eax        # 2100 <stdout>
     dce:	ba 41 01 00 00       	mov    $0x141,%edx
     dd3:	be f5 18 00 00       	mov    $0x18f5,%esi
     dd8:	89 c7                	mov    %eax,%edi
     dda:	b8 00 00 00 00       	mov    $0x0,%eax
     ddf:	e8 09 02 00 00       	callq  fed <printf>
     de4:	8b 05 16 13 00 00    	mov    0x1316(%rip),%eax        # 2100 <stdout>
     dea:	be 88 1d 00 00       	mov    $0x1d88,%esi
     def:	89 c7                	mov    %eax,%edi
     df1:	b8 00 00 00 00       	mov    $0x0,%eax
     df6:	e8 f2 01 00 00       	callq  fed <printf>
     dfb:	8b 05 ff 12 00 00    	mov    0x12ff(%rip),%eax        # 2100 <stdout>
     e01:	be 1e 19 00 00       	mov    $0x191e,%esi
     e06:	89 c7                	mov    %eax,%edi
     e08:	b8 00 00 00 00       	mov    $0x0,%eax
     e0d:	e8 db 01 00 00       	callq  fed <printf>
     e12:	eb fe                	jmp    e12 <copyonwriteforktest+0x2dd>

    wait();
     e14:	e8 7f 07 00 00       	callq  1598 <wait>
  }

  printf(stdout, "copyonwriteforktest passed\n");
     e19:	8b 05 e1 12 00 00    	mov    0x12e1(%rip),%eax        # 2100 <stdout>
     e1f:	be aa 1d 00 00       	mov    $0x1daa,%esi
     e24:	89 c7                	mov    %eax,%edi
     e26:	b8 00 00 00 00       	mov    $0x0,%eax
     e2b:	e8 bd 01 00 00       	callq  fed <printf>
}
     e30:	90                   	nop
     e31:	c9                   	leaveq 
     e32:	c3                   	retq   

0000000000000e33 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
     e33:	55                   	push   %rbp
     e34:	48 89 e5             	mov    %rsp,%rbp
     e37:	48 83 ec 10          	sub    $0x10,%rsp
     e3b:	89 7d fc             	mov    %edi,-0x4(%rbp)
     e3e:	89 f0                	mov    %esi,%eax
     e40:	88 45 f8             	mov    %al,-0x8(%rbp)
     e43:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
     e47:	8b 45 fc             	mov    -0x4(%rbp),%eax
     e4a:	ba 01 00 00 00       	mov    $0x1,%edx
     e4f:	48 89 ce             	mov    %rcx,%rsi
     e52:	89 c7                	mov    %eax,%edi
     e54:	e8 57 07 00 00       	callq  15b0 <write>
     e59:	90                   	nop
     e5a:	c9                   	leaveq 
     e5b:	c3                   	retq   

0000000000000e5c <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
     e5c:	55                   	push   %rbp
     e5d:	48 89 e5             	mov    %rsp,%rbp
     e60:	48 83 ec 40          	sub    $0x40,%rsp
     e64:	89 7d cc             	mov    %edi,-0x34(%rbp)
     e67:	89 75 c8             	mov    %esi,-0x38(%rbp)
     e6a:	89 55 c4             	mov    %edx,-0x3c(%rbp)
     e6d:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
     e70:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     e74:	74 1f                	je     e95 <printint64+0x39>
     e76:	8b 45 c8             	mov    -0x38(%rbp),%eax
     e79:	c1 e8 1f             	shr    $0x1f,%eax
     e7c:	0f b6 c0             	movzbl %al,%eax
     e7f:	89 45 c0             	mov    %eax,-0x40(%rbp)
     e82:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     e86:	74 0d                	je     e95 <printint64+0x39>
    x = -xx;
     e88:	8b 45 c8             	mov    -0x38(%rbp),%eax
     e8b:	f7 d8                	neg    %eax
     e8d:	48 98                	cltq   
     e8f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     e93:	eb 09                	jmp    e9e <printint64+0x42>
  else
    x = xx;
     e95:	8b 45 c8             	mov    -0x38(%rbp),%eax
     e98:	48 98                	cltq   
     e9a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
     e9e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     ea5:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     ea8:	8d 41 01             	lea    0x1(%rcx),%eax
     eab:	89 45 fc             	mov    %eax,-0x4(%rbp)
     eae:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     eb1:	48 63 f0             	movslq %eax,%rsi
     eb4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     eb8:	ba 00 00 00 00       	mov    $0x0,%edx
     ebd:	48 f7 f6             	div    %rsi
     ec0:	48 89 d0             	mov    %rdx,%rax
     ec3:	0f b6 90 10 21 00 00 	movzbl 0x2110(%rax),%edx
     eca:	48 63 c1             	movslq %ecx,%rax
     ecd:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
     ed1:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     ed4:	48 63 f8             	movslq %eax,%rdi
     ed7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     edb:	ba 00 00 00 00       	mov    $0x0,%edx
     ee0:	48 f7 f7             	div    %rdi
     ee3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     ee7:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     eec:	75 b7                	jne    ea5 <printint64+0x49>

  if (sgn)
     eee:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     ef2:	74 2b                	je     f1f <printint64+0xc3>
    buf[i++] = '-';
     ef4:	8b 45 fc             	mov    -0x4(%rbp),%eax
     ef7:	8d 50 01             	lea    0x1(%rax),%edx
     efa:	89 55 fc             	mov    %edx,-0x4(%rbp)
     efd:	48 98                	cltq   
     eff:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
     f04:	eb 19                	jmp    f1f <printint64+0xc3>
    putc(fd, buf[i]);
     f06:	8b 45 fc             	mov    -0x4(%rbp),%eax
     f09:	48 98                	cltq   
     f0b:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
     f10:	0f be d0             	movsbl %al,%edx
     f13:	8b 45 cc             	mov    -0x34(%rbp),%eax
     f16:	89 d6                	mov    %edx,%esi
     f18:	89 c7                	mov    %eax,%edi
     f1a:	e8 14 ff ff ff       	callq  e33 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
     f1f:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     f23:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     f27:	79 dd                	jns    f06 <printint64+0xaa>
    putc(fd, buf[i]);
}
     f29:	90                   	nop
     f2a:	c9                   	leaveq 
     f2b:	c3                   	retq   

0000000000000f2c <printint>:

static void printint(int fd, int xx, int base, int sgn) {
     f2c:	55                   	push   %rbp
     f2d:	48 89 e5             	mov    %rsp,%rbp
     f30:	48 83 ec 30          	sub    $0x30,%rsp
     f34:	89 7d dc             	mov    %edi,-0x24(%rbp)
     f37:	89 75 d8             	mov    %esi,-0x28(%rbp)
     f3a:	89 55 d4             	mov    %edx,-0x2c(%rbp)
     f3d:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
     f40:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
     f47:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
     f4b:	74 17                	je     f64 <printint+0x38>
     f4d:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
     f51:	79 11                	jns    f64 <printint+0x38>
    neg = 1;
     f53:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
     f5a:	8b 45 d8             	mov    -0x28(%rbp),%eax
     f5d:	f7 d8                	neg    %eax
     f5f:	89 45 f4             	mov    %eax,-0xc(%rbp)
     f62:	eb 06                	jmp    f6a <printint+0x3e>
  } else {
    x = xx;
     f64:	8b 45 d8             	mov    -0x28(%rbp),%eax
     f67:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
     f6a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     f71:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     f74:	8d 41 01             	lea    0x1(%rcx),%eax
     f77:	89 45 fc             	mov    %eax,-0x4(%rbp)
     f7a:	8b 75 d4             	mov    -0x2c(%rbp),%esi
     f7d:	8b 45 f4             	mov    -0xc(%rbp),%eax
     f80:	ba 00 00 00 00       	mov    $0x0,%edx
     f85:	f7 f6                	div    %esi
     f87:	89 d0                	mov    %edx,%eax
     f89:	89 c0                	mov    %eax,%eax
     f8b:	0f b6 90 30 21 00 00 	movzbl 0x2130(%rax),%edx
     f92:	48 63 c1             	movslq %ecx,%rax
     f95:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
     f99:	8b 7d d4             	mov    -0x2c(%rbp),%edi
     f9c:	8b 45 f4             	mov    -0xc(%rbp),%eax
     f9f:	ba 00 00 00 00       	mov    $0x0,%edx
     fa4:	f7 f7                	div    %edi
     fa6:	89 45 f4             	mov    %eax,-0xc(%rbp)
     fa9:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     fad:	75 c2                	jne    f71 <printint+0x45>
  if (neg)
     faf:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     fb3:	74 2b                	je     fe0 <printint+0xb4>
    buf[i++] = '-';
     fb5:	8b 45 fc             	mov    -0x4(%rbp),%eax
     fb8:	8d 50 01             	lea    0x1(%rax),%edx
     fbb:	89 55 fc             	mov    %edx,-0x4(%rbp)
     fbe:	48 98                	cltq   
     fc0:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
     fc5:	eb 19                	jmp    fe0 <printint+0xb4>
    putc(fd, buf[i]);
     fc7:	8b 45 fc             	mov    -0x4(%rbp),%eax
     fca:	48 98                	cltq   
     fcc:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
     fd1:	0f be d0             	movsbl %al,%edx
     fd4:	8b 45 dc             	mov    -0x24(%rbp),%eax
     fd7:	89 d6                	mov    %edx,%esi
     fd9:	89 c7                	mov    %eax,%edi
     fdb:	e8 53 fe ff ff       	callq  e33 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
     fe0:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     fe4:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     fe8:	79 dd                	jns    fc7 <printint+0x9b>
    putc(fd, buf[i]);
}
     fea:	90                   	nop
     feb:	c9                   	leaveq 
     fec:	c3                   	retq   

0000000000000fed <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
     fed:	55                   	push   %rbp
     fee:	48 89 e5             	mov    %rsp,%rbp
     ff1:	48 83 ec 70          	sub    $0x70,%rsp
     ff5:	89 7d 9c             	mov    %edi,-0x64(%rbp)
     ff8:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
     ffc:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
    1000:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
    1004:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
    1008:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
    100c:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
    1013:	48 8d 45 10          	lea    0x10(%rbp),%rax
    1017:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
    101b:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
    101f:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
    1023:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
    102a:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
    1031:	e9 68 02 00 00       	jmpq   129e <printf+0x2b1>
    c = fmt[i] & 0xff;
    1036:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    1039:	48 63 d0             	movslq %eax,%rdx
    103c:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    1040:	48 01 d0             	add    %rdx,%rax
    1043:	0f b6 00             	movzbl (%rax),%eax
    1046:	0f be c0             	movsbl %al,%eax
    1049:	25 ff 00 00 00       	and    $0xff,%eax
    104e:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
    1051:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
    1055:	75 30                	jne    1087 <printf+0x9a>
      if (c == '%') {
    1057:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    105b:	75 13                	jne    1070 <printf+0x83>
        state = '%';
    105d:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
    1064:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
    106b:	e9 2a 02 00 00       	jmpq   129a <printf+0x2ad>
      } else {
        putc(fd, c);
    1070:	8b 45 b8             	mov    -0x48(%rbp),%eax
    1073:	0f be d0             	movsbl %al,%edx
    1076:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1079:	89 d6                	mov    %edx,%esi
    107b:	89 c7                	mov    %eax,%edi
    107d:	e8 b1 fd ff ff       	callq  e33 <putc>
    1082:	e9 13 02 00 00       	jmpq   129a <printf+0x2ad>
      }
    } else if (state == '%') {
    1087:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
    108b:	0f 85 09 02 00 00    	jne    129a <printf+0x2ad>
      if (c == 'l') {
    1091:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
    1095:	75 0c                	jne    10a3 <printf+0xb6>
        lflag = 1;
    1097:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
    109e:	e9 f7 01 00 00       	jmpq   129a <printf+0x2ad>
      } else if (c == 'd') {
    10a3:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
    10a7:	0f 85 95 00 00 00    	jne    1142 <printf+0x155>
        if (lflag == 1)
    10ad:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    10b1:	75 49                	jne    10fc <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
    10b3:	8b 45 a0             	mov    -0x60(%rbp),%eax
    10b6:	83 f8 30             	cmp    $0x30,%eax
    10b9:	73 17                	jae    10d2 <printf+0xe5>
    10bb:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    10bf:	8b 55 a0             	mov    -0x60(%rbp),%edx
    10c2:	89 d2                	mov    %edx,%edx
    10c4:	48 01 d0             	add    %rdx,%rax
    10c7:	8b 55 a0             	mov    -0x60(%rbp),%edx
    10ca:	83 c2 08             	add    $0x8,%edx
    10cd:	89 55 a0             	mov    %edx,-0x60(%rbp)
    10d0:	eb 0c                	jmp    10de <printf+0xf1>
    10d2:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    10d6:	48 8d 50 08          	lea    0x8(%rax),%rdx
    10da:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    10de:	48 8b 00             	mov    (%rax),%rax
    10e1:	89 c6                	mov    %eax,%esi
    10e3:	8b 45 9c             	mov    -0x64(%rbp),%eax
    10e6:	b9 01 00 00 00       	mov    $0x1,%ecx
    10eb:	ba 0a 00 00 00       	mov    $0xa,%edx
    10f0:	89 c7                	mov    %eax,%edi
    10f2:	e8 65 fd ff ff       	callq  e5c <printint64>
    10f7:	e9 97 01 00 00       	jmpq   1293 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
    10fc:	8b 45 a0             	mov    -0x60(%rbp),%eax
    10ff:	83 f8 30             	cmp    $0x30,%eax
    1102:	73 17                	jae    111b <printf+0x12e>
    1104:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1108:	8b 55 a0             	mov    -0x60(%rbp),%edx
    110b:	89 d2                	mov    %edx,%edx
    110d:	48 01 d0             	add    %rdx,%rax
    1110:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1113:	83 c2 08             	add    $0x8,%edx
    1116:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1119:	eb 0c                	jmp    1127 <printf+0x13a>
    111b:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    111f:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1123:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1127:	8b 30                	mov    (%rax),%esi
    1129:	8b 45 9c             	mov    -0x64(%rbp),%eax
    112c:	b9 01 00 00 00       	mov    $0x1,%ecx
    1131:	ba 0a 00 00 00       	mov    $0xa,%edx
    1136:	89 c7                	mov    %eax,%edi
    1138:	e8 ef fd ff ff       	callq  f2c <printint>
    113d:	e9 51 01 00 00       	jmpq   1293 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
    1142:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
    1146:	74 0a                	je     1152 <printf+0x165>
    1148:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
    114c:	0f 85 95 00 00 00    	jne    11e7 <printf+0x1fa>
        if (lflag == 1)
    1152:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    1156:	75 49                	jne    11a1 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
    1158:	8b 45 a0             	mov    -0x60(%rbp),%eax
    115b:	83 f8 30             	cmp    $0x30,%eax
    115e:	73 17                	jae    1177 <printf+0x18a>
    1160:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1164:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1167:	89 d2                	mov    %edx,%edx
    1169:	48 01 d0             	add    %rdx,%rax
    116c:	8b 55 a0             	mov    -0x60(%rbp),%edx
    116f:	83 c2 08             	add    $0x8,%edx
    1172:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1175:	eb 0c                	jmp    1183 <printf+0x196>
    1177:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    117b:	48 8d 50 08          	lea    0x8(%rax),%rdx
    117f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1183:	48 8b 00             	mov    (%rax),%rax
    1186:	89 c6                	mov    %eax,%esi
    1188:	8b 45 9c             	mov    -0x64(%rbp),%eax
    118b:	b9 00 00 00 00       	mov    $0x0,%ecx
    1190:	ba 10 00 00 00       	mov    $0x10,%edx
    1195:	89 c7                	mov    %eax,%edi
    1197:	e8 c0 fc ff ff       	callq  e5c <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    119c:	e9 f2 00 00 00       	jmpq   1293 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
    11a1:	8b 45 a0             	mov    -0x60(%rbp),%eax
    11a4:	83 f8 30             	cmp    $0x30,%eax
    11a7:	73 17                	jae    11c0 <printf+0x1d3>
    11a9:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    11ad:	8b 55 a0             	mov    -0x60(%rbp),%edx
    11b0:	89 d2                	mov    %edx,%edx
    11b2:	48 01 d0             	add    %rdx,%rax
    11b5:	8b 55 a0             	mov    -0x60(%rbp),%edx
    11b8:	83 c2 08             	add    $0x8,%edx
    11bb:	89 55 a0             	mov    %edx,-0x60(%rbp)
    11be:	eb 0c                	jmp    11cc <printf+0x1df>
    11c0:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    11c4:	48 8d 50 08          	lea    0x8(%rax),%rdx
    11c8:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    11cc:	8b 30                	mov    (%rax),%esi
    11ce:	8b 45 9c             	mov    -0x64(%rbp),%eax
    11d1:	b9 00 00 00 00       	mov    $0x0,%ecx
    11d6:	ba 10 00 00 00       	mov    $0x10,%edx
    11db:	89 c7                	mov    %eax,%edi
    11dd:	e8 4a fd ff ff       	callq  f2c <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    11e2:	e9 ac 00 00 00       	jmpq   1293 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
    11e7:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
    11eb:	75 6b                	jne    1258 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
    11ed:	8b 45 a0             	mov    -0x60(%rbp),%eax
    11f0:	83 f8 30             	cmp    $0x30,%eax
    11f3:	73 17                	jae    120c <printf+0x21f>
    11f5:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    11f9:	8b 55 a0             	mov    -0x60(%rbp),%edx
    11fc:	89 d2                	mov    %edx,%edx
    11fe:	48 01 d0             	add    %rdx,%rax
    1201:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1204:	83 c2 08             	add    $0x8,%edx
    1207:	89 55 a0             	mov    %edx,-0x60(%rbp)
    120a:	eb 0c                	jmp    1218 <printf+0x22b>
    120c:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1210:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1214:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1218:	48 8b 00             	mov    (%rax),%rax
    121b:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
    121f:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
    1224:	75 25                	jne    124b <printf+0x25e>
          s = "(null)";
    1226:	48 c7 45 c8 c6 1d 00 	movq   $0x1dc6,-0x38(%rbp)
    122d:	00 
        for (; *s; s++)
    122e:	eb 1b                	jmp    124b <printf+0x25e>
          putc(fd, *s);
    1230:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    1234:	0f b6 00             	movzbl (%rax),%eax
    1237:	0f be d0             	movsbl %al,%edx
    123a:	8b 45 9c             	mov    -0x64(%rbp),%eax
    123d:	89 d6                	mov    %edx,%esi
    123f:	89 c7                	mov    %eax,%edi
    1241:	e8 ed fb ff ff       	callq  e33 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
    1246:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
    124b:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    124f:	0f b6 00             	movzbl (%rax),%eax
    1252:	84 c0                	test   %al,%al
    1254:	75 da                	jne    1230 <printf+0x243>
    1256:	eb 3b                	jmp    1293 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
    1258:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    125c:	75 14                	jne    1272 <printf+0x285>
        putc(fd, c);
    125e:	8b 45 b8             	mov    -0x48(%rbp),%eax
    1261:	0f be d0             	movsbl %al,%edx
    1264:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1267:	89 d6                	mov    %edx,%esi
    1269:	89 c7                	mov    %eax,%edi
    126b:	e8 c3 fb ff ff       	callq  e33 <putc>
    1270:	eb 21                	jmp    1293 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
    1272:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1275:	be 25 00 00 00       	mov    $0x25,%esi
    127a:	89 c7                	mov    %eax,%edi
    127c:	e8 b2 fb ff ff       	callq  e33 <putc>
        putc(fd, c);
    1281:	8b 45 b8             	mov    -0x48(%rbp),%eax
    1284:	0f be d0             	movsbl %al,%edx
    1287:	8b 45 9c             	mov    -0x64(%rbp),%eax
    128a:	89 d6                	mov    %edx,%esi
    128c:	89 c7                	mov    %eax,%edi
    128e:	e8 a0 fb ff ff       	callq  e33 <putc>
      }
      state = 0;
    1293:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
    129a:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
    129e:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    12a1:	48 63 d0             	movslq %eax,%rdx
    12a4:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    12a8:	48 01 d0             	add    %rdx,%rax
    12ab:	0f b6 00             	movzbl (%rax),%eax
    12ae:	84 c0                	test   %al,%al
    12b0:	0f 85 80 fd ff ff    	jne    1036 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
    12b6:	90                   	nop
    12b7:	c9                   	leaveq 
    12b8:	c3                   	retq   

00000000000012b9 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
    12b9:	55                   	push   %rbp
    12ba:	48 89 e5             	mov    %rsp,%rbp
    12bd:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    12c1:	89 75 f4             	mov    %esi,-0xc(%rbp)
    12c4:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
    12c7:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
    12cb:	8b 55 f0             	mov    -0x10(%rbp),%edx
    12ce:	8b 45 f4             	mov    -0xc(%rbp),%eax
    12d1:	48 89 ce             	mov    %rcx,%rsi
    12d4:	48 89 f7             	mov    %rsi,%rdi
    12d7:	89 d1                	mov    %edx,%ecx
    12d9:	fc                   	cld    
    12da:	f3 aa                	rep stos %al,%es:(%rdi)
    12dc:	89 ca                	mov    %ecx,%edx
    12de:	48 89 fe             	mov    %rdi,%rsi
    12e1:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
    12e5:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
    12e8:	90                   	nop
    12e9:	5d                   	pop    %rbp
    12ea:	c3                   	retq   

00000000000012eb <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
    12eb:	55                   	push   %rbp
    12ec:	48 89 e5             	mov    %rsp,%rbp
    12ef:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    12f3:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
    12f7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    12fb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
    12ff:	90                   	nop
    1300:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1304:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1308:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    130c:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    1310:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    1314:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
    1318:	0f b6 12             	movzbl (%rdx),%edx
    131b:	88 10                	mov    %dl,(%rax)
    131d:	0f b6 00             	movzbl (%rax),%eax
    1320:	84 c0                	test   %al,%al
    1322:	75 dc                	jne    1300 <strcpy+0x15>
    ;
  return os;
    1324:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    1328:	5d                   	pop    %rbp
    1329:	c3                   	retq   

000000000000132a <strcmp>:

int strcmp(const char *p, const char *q) {
    132a:	55                   	push   %rbp
    132b:	48 89 e5             	mov    %rsp,%rbp
    132e:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    1332:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
    1336:	eb 0a                	jmp    1342 <strcmp+0x18>
    p++, q++;
    1338:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    133d:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
    1342:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1346:	0f b6 00             	movzbl (%rax),%eax
    1349:	84 c0                	test   %al,%al
    134b:	74 12                	je     135f <strcmp+0x35>
    134d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1351:	0f b6 10             	movzbl (%rax),%edx
    1354:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1358:	0f b6 00             	movzbl (%rax),%eax
    135b:	38 c2                	cmp    %al,%dl
    135d:	74 d9                	je     1338 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
    135f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1363:	0f b6 00             	movzbl (%rax),%eax
    1366:	0f b6 d0             	movzbl %al,%edx
    1369:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    136d:	0f b6 00             	movzbl (%rax),%eax
    1370:	0f b6 c0             	movzbl %al,%eax
    1373:	29 c2                	sub    %eax,%edx
    1375:	89 d0                	mov    %edx,%eax
}
    1377:	5d                   	pop    %rbp
    1378:	c3                   	retq   

0000000000001379 <strlen>:

uint strlen(char *s) {
    1379:	55                   	push   %rbp
    137a:	48 89 e5             	mov    %rsp,%rbp
    137d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
    1381:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1388:	eb 04                	jmp    138e <strlen+0x15>
    138a:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    138e:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1391:	48 63 d0             	movslq %eax,%rdx
    1394:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1398:	48 01 d0             	add    %rdx,%rax
    139b:	0f b6 00             	movzbl (%rax),%eax
    139e:	84 c0                	test   %al,%al
    13a0:	75 e8                	jne    138a <strlen+0x11>
    ;
  return n;
    13a2:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    13a5:	5d                   	pop    %rbp
    13a6:	c3                   	retq   

00000000000013a7 <memset>:

void *memset(void *dst, int c, uint n) {
    13a7:	55                   	push   %rbp
    13a8:	48 89 e5             	mov    %rsp,%rbp
    13ab:	48 83 ec 10          	sub    $0x10,%rsp
    13af:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    13b3:	89 75 f4             	mov    %esi,-0xc(%rbp)
    13b6:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
    13b9:	8b 55 f0             	mov    -0x10(%rbp),%edx
    13bc:	8b 4d f4             	mov    -0xc(%rbp),%ecx
    13bf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    13c3:	89 ce                	mov    %ecx,%esi
    13c5:	48 89 c7             	mov    %rax,%rdi
    13c8:	e8 ec fe ff ff       	callq  12b9 <stosb>
  return dst;
    13cd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    13d1:	c9                   	leaveq 
    13d2:	c3                   	retq   

00000000000013d3 <strchr>:

char *strchr(const char *s, char c) {
    13d3:	55                   	push   %rbp
    13d4:	48 89 e5             	mov    %rsp,%rbp
    13d7:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    13db:	89 f0                	mov    %esi,%eax
    13dd:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
    13e0:	eb 17                	jmp    13f9 <strchr+0x26>
    if (*s == c)
    13e2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    13e6:	0f b6 00             	movzbl (%rax),%eax
    13e9:	3a 45 f4             	cmp    -0xc(%rbp),%al
    13ec:	75 06                	jne    13f4 <strchr+0x21>
      return (char *)s;
    13ee:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    13f2:	eb 15                	jmp    1409 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
    13f4:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    13f9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    13fd:	0f b6 00             	movzbl (%rax),%eax
    1400:	84 c0                	test   %al,%al
    1402:	75 de                	jne    13e2 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
    1404:	b8 00 00 00 00       	mov    $0x0,%eax
}
    1409:	5d                   	pop    %rbp
    140a:	c3                   	retq   

000000000000140b <gets>:

char *gets(char *buf, int max) {
    140b:	55                   	push   %rbp
    140c:	48 89 e5             	mov    %rsp,%rbp
    140f:	48 83 ec 20          	sub    $0x20,%rsp
    1413:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1417:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    141a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1421:	eb 48                	jmp    146b <gets+0x60>
    cc = read(0, &c, 1);
    1423:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
    1427:	ba 01 00 00 00       	mov    $0x1,%edx
    142c:	48 89 c6             	mov    %rax,%rsi
    142f:	bf 00 00 00 00       	mov    $0x0,%edi
    1434:	e8 6f 01 00 00       	callq  15a8 <read>
    1439:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
    143c:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
    1440:	7e 36                	jle    1478 <gets+0x6d>
      break;
    buf[i++] = c;
    1442:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1445:	8d 50 01             	lea    0x1(%rax),%edx
    1448:	89 55 fc             	mov    %edx,-0x4(%rbp)
    144b:	48 63 d0             	movslq %eax,%rdx
    144e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1452:	48 01 c2             	add    %rax,%rdx
    1455:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1459:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
    145b:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    145f:	3c 0a                	cmp    $0xa,%al
    1461:	74 16                	je     1479 <gets+0x6e>
    1463:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    1467:	3c 0d                	cmp    $0xd,%al
    1469:	74 0e                	je     1479 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    146b:	8b 45 fc             	mov    -0x4(%rbp),%eax
    146e:	83 c0 01             	add    $0x1,%eax
    1471:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
    1474:	7c ad                	jl     1423 <gets+0x18>
    1476:	eb 01                	jmp    1479 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
    1478:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
    1479:	8b 45 fc             	mov    -0x4(%rbp),%eax
    147c:	48 63 d0             	movslq %eax,%rdx
    147f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1483:	48 01 d0             	add    %rdx,%rax
    1486:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
    1489:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
    148d:	c9                   	leaveq 
    148e:	c3                   	retq   

000000000000148f <stat>:

int stat(char *n, struct stat *st) {
    148f:	55                   	push   %rbp
    1490:	48 89 e5             	mov    %rsp,%rbp
    1493:	48 83 ec 20          	sub    $0x20,%rsp
    1497:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    149b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    149f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    14a3:	be 00 00 00 00       	mov    $0x0,%esi
    14a8:	48 89 c7             	mov    %rax,%rdi
    14ab:	e8 20 01 00 00       	callq  15d0 <open>
    14b0:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
    14b3:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    14b7:	79 07                	jns    14c0 <stat+0x31>
    return -1;
    14b9:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    14be:	eb 21                	jmp    14e1 <stat+0x52>
  r = fstat(fd, st);
    14c0:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    14c4:	8b 45 fc             	mov    -0x4(%rbp),%eax
    14c7:	48 89 d6             	mov    %rdx,%rsi
    14ca:	89 c7                	mov    %eax,%edi
    14cc:	e8 17 01 00 00       	callq  15e8 <fstat>
    14d1:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
    14d4:	8b 45 fc             	mov    -0x4(%rbp),%eax
    14d7:	89 c7                	mov    %eax,%edi
    14d9:	e8 da 00 00 00       	callq  15b8 <close>
  return r;
    14de:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
    14e1:	c9                   	leaveq 
    14e2:	c3                   	retq   

00000000000014e3 <atoi>:

int atoi(const char *s) {
    14e3:	55                   	push   %rbp
    14e4:	48 89 e5             	mov    %rsp,%rbp
    14e7:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
    14eb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
    14f2:	eb 28                	jmp    151c <atoi+0x39>
    n = n * 10 + *s++ - '0';
    14f4:	8b 55 fc             	mov    -0x4(%rbp),%edx
    14f7:	89 d0                	mov    %edx,%eax
    14f9:	c1 e0 02             	shl    $0x2,%eax
    14fc:	01 d0                	add    %edx,%eax
    14fe:	01 c0                	add    %eax,%eax
    1500:	89 c1                	mov    %eax,%ecx
    1502:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1506:	48 8d 50 01          	lea    0x1(%rax),%rdx
    150a:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    150e:	0f b6 00             	movzbl (%rax),%eax
    1511:	0f be c0             	movsbl %al,%eax
    1514:	01 c8                	add    %ecx,%eax
    1516:	83 e8 30             	sub    $0x30,%eax
    1519:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
    151c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1520:	0f b6 00             	movzbl (%rax),%eax
    1523:	3c 2f                	cmp    $0x2f,%al
    1525:	7e 0b                	jle    1532 <atoi+0x4f>
    1527:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    152b:	0f b6 00             	movzbl (%rax),%eax
    152e:	3c 39                	cmp    $0x39,%al
    1530:	7e c2                	jle    14f4 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
    1532:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    1535:	5d                   	pop    %rbp
    1536:	c3                   	retq   

0000000000001537 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
    1537:	55                   	push   %rbp
    1538:	48 89 e5             	mov    %rsp,%rbp
    153b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    153f:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
    1543:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
    1546:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    154a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
    154e:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
    1552:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
    1556:	eb 1d                	jmp    1575 <memmove+0x3e>
    *dst++ = *src++;
    1558:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    155c:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1560:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
    1564:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1568:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    156c:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
    1570:	0f b6 12             	movzbl (%rdx),%edx
    1573:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
    1575:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1578:	8d 50 ff             	lea    -0x1(%rax),%edx
    157b:	89 55 dc             	mov    %edx,-0x24(%rbp)
    157e:	85 c0                	test   %eax,%eax
    1580:	7f d6                	jg     1558 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
    1582:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1586:	5d                   	pop    %rbp
    1587:	c3                   	retq   

0000000000001588 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
    1588:	b8 01 00 00 00       	mov    $0x1,%eax
    158d:	cd 40                	int    $0x40
    158f:	c3                   	retq   

0000000000001590 <exit>:
SYSCALL(exit)
    1590:	b8 02 00 00 00       	mov    $0x2,%eax
    1595:	cd 40                	int    $0x40
    1597:	c3                   	retq   

0000000000001598 <wait>:
SYSCALL(wait)
    1598:	b8 03 00 00 00       	mov    $0x3,%eax
    159d:	cd 40                	int    $0x40
    159f:	c3                   	retq   

00000000000015a0 <pipe>:
SYSCALL(pipe)
    15a0:	b8 04 00 00 00       	mov    $0x4,%eax
    15a5:	cd 40                	int    $0x40
    15a7:	c3                   	retq   

00000000000015a8 <read>:
SYSCALL(read)
    15a8:	b8 05 00 00 00       	mov    $0x5,%eax
    15ad:	cd 40                	int    $0x40
    15af:	c3                   	retq   

00000000000015b0 <write>:
SYSCALL(write)
    15b0:	b8 10 00 00 00       	mov    $0x10,%eax
    15b5:	cd 40                	int    $0x40
    15b7:	c3                   	retq   

00000000000015b8 <close>:
SYSCALL(close)
    15b8:	b8 15 00 00 00       	mov    $0x15,%eax
    15bd:	cd 40                	int    $0x40
    15bf:	c3                   	retq   

00000000000015c0 <kill>:
SYSCALL(kill)
    15c0:	b8 06 00 00 00       	mov    $0x6,%eax
    15c5:	cd 40                	int    $0x40
    15c7:	c3                   	retq   

00000000000015c8 <exec>:
SYSCALL(exec)
    15c8:	b8 07 00 00 00       	mov    $0x7,%eax
    15cd:	cd 40                	int    $0x40
    15cf:	c3                   	retq   

00000000000015d0 <open>:
SYSCALL(open)
    15d0:	b8 0f 00 00 00       	mov    $0xf,%eax
    15d5:	cd 40                	int    $0x40
    15d7:	c3                   	retq   

00000000000015d8 <mknod>:
SYSCALL(mknod)
    15d8:	b8 11 00 00 00       	mov    $0x11,%eax
    15dd:	cd 40                	int    $0x40
    15df:	c3                   	retq   

00000000000015e0 <unlink>:
SYSCALL(unlink)
    15e0:	b8 12 00 00 00       	mov    $0x12,%eax
    15e5:	cd 40                	int    $0x40
    15e7:	c3                   	retq   

00000000000015e8 <fstat>:
SYSCALL(fstat)
    15e8:	b8 08 00 00 00       	mov    $0x8,%eax
    15ed:	cd 40                	int    $0x40
    15ef:	c3                   	retq   

00000000000015f0 <link>:
SYSCALL(link)
    15f0:	b8 13 00 00 00       	mov    $0x13,%eax
    15f5:	cd 40                	int    $0x40
    15f7:	c3                   	retq   

00000000000015f8 <mkdir>:
SYSCALL(mkdir)
    15f8:	b8 14 00 00 00       	mov    $0x14,%eax
    15fd:	cd 40                	int    $0x40
    15ff:	c3                   	retq   

0000000000001600 <chdir>:
SYSCALL(chdir)
    1600:	b8 09 00 00 00       	mov    $0x9,%eax
    1605:	cd 40                	int    $0x40
    1607:	c3                   	retq   

0000000000001608 <dup>:
SYSCALL(dup)
    1608:	b8 0a 00 00 00       	mov    $0xa,%eax
    160d:	cd 40                	int    $0x40
    160f:	c3                   	retq   

0000000000001610 <getpid>:
SYSCALL(getpid)
    1610:	b8 0b 00 00 00       	mov    $0xb,%eax
    1615:	cd 40                	int    $0x40
    1617:	c3                   	retq   

0000000000001618 <sbrk>:
SYSCALL(sbrk)
    1618:	b8 0c 00 00 00       	mov    $0xc,%eax
    161d:	cd 40                	int    $0x40
    161f:	c3                   	retq   

0000000000001620 <sleep>:
SYSCALL(sleep)
    1620:	b8 0d 00 00 00       	mov    $0xd,%eax
    1625:	cd 40                	int    $0x40
    1627:	c3                   	retq   

0000000000001628 <uptime>:
SYSCALL(uptime)
    1628:	b8 0e 00 00 00       	mov    $0xe,%eax
    162d:	cd 40                	int    $0x40
    162f:	c3                   	retq   

0000000000001630 <sysinfo>:
SYSCALL(sysinfo)
    1630:	b8 16 00 00 00       	mov    $0x16,%eax
    1635:	cd 40                	int    $0x40
    1637:	c3                   	retq   

0000000000001638 <crashn>:
SYSCALL(crashn)
    1638:	b8 17 00 00 00       	mov    $0x17,%eax
    163d:	cd 40                	int    $0x40
    163f:	c3                   	retq   

0000000000001640 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
    1640:	55                   	push   %rbp
    1641:	48 89 e5             	mov    %rsp,%rbp
    1644:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
    1648:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    164c:	48 83 e8 10          	sub    $0x10,%rax
    1650:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1654:	48 8b 05 05 0b 00 00 	mov    0xb05(%rip),%rax        # 2160 <freep>
    165b:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    165f:	eb 2f                	jmp    1690 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    1661:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1665:	48 8b 00             	mov    (%rax),%rax
    1668:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    166c:	77 17                	ja     1685 <free+0x45>
    166e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1672:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1676:	77 2f                	ja     16a7 <free+0x67>
    1678:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    167c:	48 8b 00             	mov    (%rax),%rax
    167f:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1683:	77 22                	ja     16a7 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1685:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1689:	48 8b 00             	mov    (%rax),%rax
    168c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1690:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1694:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1698:	76 c7                	jbe    1661 <free+0x21>
    169a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    169e:	48 8b 00             	mov    (%rax),%rax
    16a1:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    16a5:	76 ba                	jbe    1661 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
    16a7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16ab:	8b 40 08             	mov    0x8(%rax),%eax
    16ae:	89 c0                	mov    %eax,%eax
    16b0:	48 c1 e0 04          	shl    $0x4,%rax
    16b4:	48 89 c2             	mov    %rax,%rdx
    16b7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16bb:	48 01 c2             	add    %rax,%rdx
    16be:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16c2:	48 8b 00             	mov    (%rax),%rax
    16c5:	48 39 c2             	cmp    %rax,%rdx
    16c8:	75 2d                	jne    16f7 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
    16ca:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16ce:	8b 50 08             	mov    0x8(%rax),%edx
    16d1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16d5:	48 8b 00             	mov    (%rax),%rax
    16d8:	8b 40 08             	mov    0x8(%rax),%eax
    16db:	01 c2                	add    %eax,%edx
    16dd:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16e1:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
    16e4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16e8:	48 8b 00             	mov    (%rax),%rax
    16eb:	48 8b 10             	mov    (%rax),%rdx
    16ee:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16f2:	48 89 10             	mov    %rdx,(%rax)
    16f5:	eb 0e                	jmp    1705 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
    16f7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16fb:	48 8b 10             	mov    (%rax),%rdx
    16fe:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1702:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
    1705:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1709:	8b 40 08             	mov    0x8(%rax),%eax
    170c:	89 c0                	mov    %eax,%eax
    170e:	48 c1 e0 04          	shl    $0x4,%rax
    1712:	48 89 c2             	mov    %rax,%rdx
    1715:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1719:	48 01 d0             	add    %rdx,%rax
    171c:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1720:	75 27                	jne    1749 <free+0x109>
    p->s.size += bp->s.size;
    1722:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1726:	8b 50 08             	mov    0x8(%rax),%edx
    1729:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    172d:	8b 40 08             	mov    0x8(%rax),%eax
    1730:	01 c2                	add    %eax,%edx
    1732:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1736:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
    1739:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    173d:	48 8b 10             	mov    (%rax),%rdx
    1740:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1744:	48 89 10             	mov    %rdx,(%rax)
    1747:	eb 0b                	jmp    1754 <free+0x114>
  } else
    p->s.ptr = bp;
    1749:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    174d:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1751:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
    1754:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1758:	48 89 05 01 0a 00 00 	mov    %rax,0xa01(%rip)        # 2160 <freep>
}
    175f:	90                   	nop
    1760:	5d                   	pop    %rbp
    1761:	c3                   	retq   

0000000000001762 <morecore>:

static Header *morecore(uint nu) {
    1762:	55                   	push   %rbp
    1763:	48 89 e5             	mov    %rsp,%rbp
    1766:	48 83 ec 20          	sub    $0x20,%rsp
    176a:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
    176d:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
    1774:	77 07                	ja     177d <morecore+0x1b>
    nu = 4096;
    1776:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
    177d:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1780:	c1 e0 04             	shl    $0x4,%eax
    1783:	89 c7                	mov    %eax,%edi
    1785:	e8 8e fe ff ff       	callq  1618 <sbrk>
    178a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
    178e:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
    1793:	75 07                	jne    179c <morecore+0x3a>
    return 0;
    1795:	b8 00 00 00 00       	mov    $0x0,%eax
    179a:	eb 29                	jmp    17c5 <morecore+0x63>
  hp = (Header *)p;
    179c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17a0:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
    17a4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    17a8:	8b 55 ec             	mov    -0x14(%rbp),%edx
    17ab:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
    17ae:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    17b2:	48 83 c0 10          	add    $0x10,%rax
    17b6:	48 89 c7             	mov    %rax,%rdi
    17b9:	e8 82 fe ff ff       	callq  1640 <free>
  return freep;
    17be:	48 8b 05 9b 09 00 00 	mov    0x99b(%rip),%rax        # 2160 <freep>
}
    17c5:	c9                   	leaveq 
    17c6:	c3                   	retq   

00000000000017c7 <malloc>:

void *malloc(uint nbytes) {
    17c7:	55                   	push   %rbp
    17c8:	48 89 e5             	mov    %rsp,%rbp
    17cb:	48 83 ec 30          	sub    $0x30,%rsp
    17cf:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
    17d2:	8b 45 dc             	mov    -0x24(%rbp),%eax
    17d5:	48 83 c0 0f          	add    $0xf,%rax
    17d9:	48 c1 e8 04          	shr    $0x4,%rax
    17dd:	83 c0 01             	add    $0x1,%eax
    17e0:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
    17e3:	48 8b 05 76 09 00 00 	mov    0x976(%rip),%rax        # 2160 <freep>
    17ea:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    17ee:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    17f3:	75 2b                	jne    1820 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
    17f5:	48 c7 45 f0 50 21 00 	movq   $0x2150,-0x10(%rbp)
    17fc:	00 
    17fd:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1801:	48 89 05 58 09 00 00 	mov    %rax,0x958(%rip)        # 2160 <freep>
    1808:	48 8b 05 51 09 00 00 	mov    0x951(%rip),%rax        # 2160 <freep>
    180f:	48 89 05 3a 09 00 00 	mov    %rax,0x93a(%rip)        # 2150 <base>
    base.s.size = 0;
    1816:	c7 05 38 09 00 00 00 	movl   $0x0,0x938(%rip)        # 2158 <base+0x8>
    181d:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1820:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1824:	48 8b 00             	mov    (%rax),%rax
    1827:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
    182b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    182f:	8b 40 08             	mov    0x8(%rax),%eax
    1832:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1835:	72 5f                	jb     1896 <malloc+0xcf>
      if (p->s.size == nunits)
    1837:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    183b:	8b 40 08             	mov    0x8(%rax),%eax
    183e:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1841:	75 10                	jne    1853 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
    1843:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1847:	48 8b 10             	mov    (%rax),%rdx
    184a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    184e:	48 89 10             	mov    %rdx,(%rax)
    1851:	eb 2e                	jmp    1881 <malloc+0xba>
      else {
        p->s.size -= nunits;
    1853:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1857:	8b 40 08             	mov    0x8(%rax),%eax
    185a:	2b 45 ec             	sub    -0x14(%rbp),%eax
    185d:	89 c2                	mov    %eax,%edx
    185f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1863:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
    1866:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    186a:	8b 40 08             	mov    0x8(%rax),%eax
    186d:	89 c0                	mov    %eax,%eax
    186f:	48 c1 e0 04          	shl    $0x4,%rax
    1873:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
    1877:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    187b:	8b 55 ec             	mov    -0x14(%rbp),%edx
    187e:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
    1881:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1885:	48 89 05 d4 08 00 00 	mov    %rax,0x8d4(%rip)        # 2160 <freep>
      return (void *)(p + 1);
    188c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1890:	48 83 c0 10          	add    $0x10,%rax
    1894:	eb 41                	jmp    18d7 <malloc+0x110>
    }
    if (p == freep)
    1896:	48 8b 05 c3 08 00 00 	mov    0x8c3(%rip),%rax        # 2160 <freep>
    189d:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    18a1:	75 1c                	jne    18bf <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    18a3:	8b 45 ec             	mov    -0x14(%rbp),%eax
    18a6:	89 c7                	mov    %eax,%edi
    18a8:	e8 b5 fe ff ff       	callq  1762 <morecore>
    18ad:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    18b1:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    18b6:	75 07                	jne    18bf <malloc+0xf8>
        return 0;
    18b8:	b8 00 00 00 00       	mov    $0x0,%eax
    18bd:	eb 18                	jmp    18d7 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    18bf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    18c3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    18c7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    18cb:	48 8b 00             	mov    (%rax),%rax
    18ce:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    18d2:	e9 54 ff ff ff       	jmpq   182b <malloc+0x64>
    18d7:	c9                   	leaveq 
    18d8:	c3                   	retq   
