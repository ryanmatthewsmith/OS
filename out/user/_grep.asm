
out/user/_grep:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <grep>:
#include <user.h>

char buf[1024];
int match(char *, char *);

void grep(char *pattern, int fd) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 30          	sub    $0x30,%rsp
   8:	48 89 7d d8          	mov    %rdi,-0x28(%rbp)
   c:	89 75 d4             	mov    %esi,-0x2c(%rbp)
  int n, m;
  char *p, *q;

  m = 0;
   f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ((n = read(fd, buf + m, sizeof(buf) - m - 1)) > 0) {
  16:	e9 d0 00 00 00       	jmpq   eb <grep+0xeb>
    m += n;
  1b:	8b 45 ec             	mov    -0x14(%rbp),%eax
  1e:	01 45 fc             	add    %eax,-0x4(%rbp)
    buf[m] = '\0';
  21:	8b 45 fc             	mov    -0x4(%rbp),%eax
  24:	48 98                	cltq   
  26:	c6 80 e0 11 00 00 00 	movb   $0x0,0x11e0(%rax)
    p = buf;
  2d:	48 c7 45 f0 e0 11 00 	movq   $0x11e0,-0x10(%rbp)
  34:	00 
    while ((q = strchr(p, '\n')) != 0) {
  35:	eb 59                	jmp    90 <grep+0x90>
      *q = 0;
  37:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  3b:	c6 00 00             	movb   $0x0,(%rax)
      if (match(pattern, p)) {
  3e:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
  42:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
  46:	48 89 d6             	mov    %rdx,%rsi
  49:	48 89 c7             	mov    %rax,%rdi
  4c:	e8 c0 01 00 00       	callq  211 <match>
  51:	85 c0                	test   %eax,%eax
  53:	74 2f                	je     84 <grep+0x84>
        *q = '\n';
  55:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  59:	c6 00 0a             	movb   $0xa,(%rax)
        write(1, p, q + 1 - p);
  5c:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  60:	48 83 c0 01          	add    $0x1,%rax
  64:	48 89 c2             	mov    %rax,%rdx
  67:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  6b:	48 29 c2             	sub    %rax,%rdx
  6e:	48 89 d0             	mov    %rdx,%rax
  71:	89 c2                	mov    %eax,%edx
  73:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  77:	48 89 c6             	mov    %rax,%rsi
  7a:	bf 01 00 00 00       	mov    $0x1,%edi
  7f:	e8 a3 0a 00 00       	callq  b27 <write>
      }
      p = q + 1;
  84:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  88:	48 83 c0 01          	add    $0x1,%rax
  8c:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  m = 0;
  while ((n = read(fd, buf + m, sizeof(buf) - m - 1)) > 0) {
    m += n;
    buf[m] = '\0';
    p = buf;
    while ((q = strchr(p, '\n')) != 0) {
  90:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  94:	be 0a 00 00 00       	mov    $0xa,%esi
  99:	48 89 c7             	mov    %rax,%rdi
  9c:	e8 a9 08 00 00       	callq  94a <strchr>
  a1:	48 89 45 e0          	mov    %rax,-0x20(%rbp)
  a5:	48 83 7d e0 00       	cmpq   $0x0,-0x20(%rbp)
  aa:	75 8b                	jne    37 <grep+0x37>
        *q = '\n';
        write(1, p, q + 1 - p);
      }
      p = q + 1;
    }
    if (p == buf)
  ac:	48 81 7d f0 e0 11 00 	cmpq   $0x11e0,-0x10(%rbp)
  b3:	00 
  b4:	75 07                	jne    bd <grep+0xbd>
      m = 0;
  b6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    if (m > 0) {
  bd:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
  c1:	7e 28                	jle    eb <grep+0xeb>
      m -= p - buf;
  c3:	8b 45 fc             	mov    -0x4(%rbp),%eax
  c6:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
  ca:	b9 e0 11 00 00       	mov    $0x11e0,%ecx
  cf:	48 29 ca             	sub    %rcx,%rdx
  d2:	29 d0                	sub    %edx,%eax
  d4:	89 45 fc             	mov    %eax,-0x4(%rbp)
      memmove(buf, p, m);
  d7:	8b 55 fc             	mov    -0x4(%rbp),%edx
  da:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  de:	48 89 c6             	mov    %rax,%rsi
  e1:	bf e0 11 00 00       	mov    $0x11e0,%edi
  e6:	e8 c3 09 00 00       	callq  aae <memmove>
void grep(char *pattern, int fd) {
  int n, m;
  char *p, *q;

  m = 0;
  while ((n = read(fd, buf + m, sizeof(buf) - m - 1)) > 0) {
  eb:	8b 45 fc             	mov    -0x4(%rbp),%eax
  ee:	ba ff 03 00 00       	mov    $0x3ff,%edx
  f3:	29 c2                	sub    %eax,%edx
  f5:	89 d0                	mov    %edx,%eax
  f7:	89 c2                	mov    %eax,%edx
  f9:	8b 45 fc             	mov    -0x4(%rbp),%eax
  fc:	48 98                	cltq   
  fe:	48 8d 88 e0 11 00 00 	lea    0x11e0(%rax),%rcx
 105:	8b 45 d4             	mov    -0x2c(%rbp),%eax
 108:	48 89 ce             	mov    %rcx,%rsi
 10b:	89 c7                	mov    %eax,%edi
 10d:	e8 0d 0a 00 00       	callq  b1f <read>
 112:	89 45 ec             	mov    %eax,-0x14(%rbp)
 115:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
 119:	0f 8f fc fe ff ff    	jg     1b <grep+0x1b>
    if (m > 0) {
      m -= p - buf;
      memmove(buf, p, m);
    }
  }
}
 11f:	90                   	nop
 120:	c9                   	leaveq 
 121:	c3                   	retq   

0000000000000122 <main>:

int main(int argc, char *argv[]) {
 122:	55                   	push   %rbp
 123:	48 89 e5             	mov    %rsp,%rbp
 126:	48 83 ec 30          	sub    $0x30,%rsp
 12a:	89 7d dc             	mov    %edi,-0x24(%rbp)
 12d:	48 89 75 d0          	mov    %rsi,-0x30(%rbp)
  int fd, i;
  char *pattern;

  if (argc <= 1) {
 131:	83 7d dc 01          	cmpl   $0x1,-0x24(%rbp)
 135:	7f 19                	jg     150 <main+0x2e>
    printf(2, "usage: grep pattern [file ...]\n");
 137:	be 50 0e 00 00       	mov    $0xe50,%esi
 13c:	bf 02 00 00 00       	mov    $0x2,%edi
 141:	b8 00 00 00 00       	mov    $0x0,%eax
 146:	e8 19 04 00 00       	callq  564 <printf>
    exit();
 14b:	e8 b7 09 00 00       	callq  b07 <exit>
  }
  pattern = argv[1];
 150:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
 154:	48 8b 40 08          	mov    0x8(%rax),%rax
 158:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  if (argc <= 2) {
 15c:	83 7d dc 02          	cmpl   $0x2,-0x24(%rbp)
 160:	7f 16                	jg     178 <main+0x56>
    grep(pattern, 0);
 162:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 166:	be 00 00 00 00       	mov    $0x0,%esi
 16b:	48 89 c7             	mov    %rax,%rdi
 16e:	e8 8d fe ff ff       	callq  0 <grep>
    exit();
 173:	e8 8f 09 00 00       	callq  b07 <exit>
  }

  for (i = 2; i < argc; i++) {
 178:	c7 45 fc 02 00 00 00 	movl   $0x2,-0x4(%rbp)
 17f:	eb 7f                	jmp    200 <main+0xde>
    if ((fd = open(argv[i], 0)) < 0) {
 181:	8b 45 fc             	mov    -0x4(%rbp),%eax
 184:	48 98                	cltq   
 186:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
 18d:	00 
 18e:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
 192:	48 01 d0             	add    %rdx,%rax
 195:	48 8b 00             	mov    (%rax),%rax
 198:	be 00 00 00 00       	mov    $0x0,%esi
 19d:	48 89 c7             	mov    %rax,%rdi
 1a0:	e8 a2 09 00 00       	callq  b47 <open>
 1a5:	89 45 ec             	mov    %eax,-0x14(%rbp)
 1a8:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
 1ac:	79 33                	jns    1e1 <main+0xbf>
      printf(1, "grep: cannot open %s\n", argv[i]);
 1ae:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1b1:	48 98                	cltq   
 1b3:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
 1ba:	00 
 1bb:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
 1bf:	48 01 d0             	add    %rdx,%rax
 1c2:	48 8b 00             	mov    (%rax),%rax
 1c5:	48 89 c2             	mov    %rax,%rdx
 1c8:	be 70 0e 00 00       	mov    $0xe70,%esi
 1cd:	bf 01 00 00 00       	mov    $0x1,%edi
 1d2:	b8 00 00 00 00       	mov    $0x0,%eax
 1d7:	e8 88 03 00 00       	callq  564 <printf>
      exit();
 1dc:	e8 26 09 00 00       	callq  b07 <exit>
    }
    grep(pattern, fd);
 1e1:	8b 55 ec             	mov    -0x14(%rbp),%edx
 1e4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 1e8:	89 d6                	mov    %edx,%esi
 1ea:	48 89 c7             	mov    %rax,%rdi
 1ed:	e8 0e fe ff ff       	callq  0 <grep>
    close(fd);
 1f2:	8b 45 ec             	mov    -0x14(%rbp),%eax
 1f5:	89 c7                	mov    %eax,%edi
 1f7:	e8 33 09 00 00       	callq  b2f <close>
  if (argc <= 2) {
    grep(pattern, 0);
    exit();
  }

  for (i = 2; i < argc; i++) {
 1fc:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 200:	8b 45 fc             	mov    -0x4(%rbp),%eax
 203:	3b 45 dc             	cmp    -0x24(%rbp),%eax
 206:	0f 8c 75 ff ff ff    	jl     181 <main+0x5f>
      exit();
    }
    grep(pattern, fd);
    close(fd);
  }
  exit();
 20c:	e8 f6 08 00 00       	callq  b07 <exit>

0000000000000211 <match>:
// The Practice of Programming, Chapter 9.

int matchhere(char *, char *);
int matchstar(int, char *, char *);

int match(char *re, char *text) {
 211:	55                   	push   %rbp
 212:	48 89 e5             	mov    %rsp,%rbp
 215:	48 83 ec 10          	sub    $0x10,%rsp
 219:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 21d:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  if (re[0] == '^')
 221:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 225:	0f b6 00             	movzbl (%rax),%eax
 228:	3c 5e                	cmp    $0x5e,%al
 22a:	75 19                	jne    245 <match+0x34>
    return matchhere(re + 1, text);
 22c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 230:	48 8d 50 01          	lea    0x1(%rax),%rdx
 234:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 238:	48 89 c6             	mov    %rax,%rsi
 23b:	48 89 d7             	mov    %rdx,%rdi
 23e:	e8 3a 00 00 00       	callq  27d <matchhere>
 243:	eb 36                	jmp    27b <match+0x6a>
  do { // must look at empty string
    if (matchhere(re, text))
 245:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 249:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 24d:	48 89 d6             	mov    %rdx,%rsi
 250:	48 89 c7             	mov    %rax,%rdi
 253:	e8 25 00 00 00       	callq  27d <matchhere>
 258:	85 c0                	test   %eax,%eax
 25a:	74 07                	je     263 <match+0x52>
      return 1;
 25c:	b8 01 00 00 00       	mov    $0x1,%eax
 261:	eb 18                	jmp    27b <match+0x6a>
  } while (*text++ != '\0');
 263:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 267:	48 8d 50 01          	lea    0x1(%rax),%rdx
 26b:	48 89 55 f0          	mov    %rdx,-0x10(%rbp)
 26f:	0f b6 00             	movzbl (%rax),%eax
 272:	84 c0                	test   %al,%al
 274:	75 cf                	jne    245 <match+0x34>
  return 0;
 276:	b8 00 00 00 00       	mov    $0x0,%eax
}
 27b:	c9                   	leaveq 
 27c:	c3                   	retq   

000000000000027d <matchhere>:

// matchhere: search for re at beginning of text
int matchhere(char *re, char *text) {
 27d:	55                   	push   %rbp
 27e:	48 89 e5             	mov    %rsp,%rbp
 281:	48 83 ec 10          	sub    $0x10,%rsp
 285:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 289:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  if (re[0] == '\0')
 28d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 291:	0f b6 00             	movzbl (%rax),%eax
 294:	84 c0                	test   %al,%al
 296:	75 0a                	jne    2a2 <matchhere+0x25>
    return 1;
 298:	b8 01 00 00 00       	mov    $0x1,%eax
 29d:	e9 a6 00 00 00       	jmpq   348 <matchhere+0xcb>
  if (re[1] == '*')
 2a2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 2a6:	48 83 c0 01          	add    $0x1,%rax
 2aa:	0f b6 00             	movzbl (%rax),%eax
 2ad:	3c 2a                	cmp    $0x2a,%al
 2af:	75 22                	jne    2d3 <matchhere+0x56>
    return matchstar(re[0], re + 2, text);
 2b1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 2b5:	48 8d 48 02          	lea    0x2(%rax),%rcx
 2b9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 2bd:	0f b6 00             	movzbl (%rax),%eax
 2c0:	0f be c0             	movsbl %al,%eax
 2c3:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 2c7:	48 89 ce             	mov    %rcx,%rsi
 2ca:	89 c7                	mov    %eax,%edi
 2cc:	e8 79 00 00 00       	callq  34a <matchstar>
 2d1:	eb 75                	jmp    348 <matchhere+0xcb>
  if (re[0] == '$' && re[1] == '\0')
 2d3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 2d7:	0f b6 00             	movzbl (%rax),%eax
 2da:	3c 24                	cmp    $0x24,%al
 2dc:	75 20                	jne    2fe <matchhere+0x81>
 2de:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 2e2:	48 83 c0 01          	add    $0x1,%rax
 2e6:	0f b6 00             	movzbl (%rax),%eax
 2e9:	84 c0                	test   %al,%al
 2eb:	75 11                	jne    2fe <matchhere+0x81>
    return *text == '\0';
 2ed:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 2f1:	0f b6 00             	movzbl (%rax),%eax
 2f4:	84 c0                	test   %al,%al
 2f6:	0f 94 c0             	sete   %al
 2f9:	0f b6 c0             	movzbl %al,%eax
 2fc:	eb 4a                	jmp    348 <matchhere+0xcb>
  if (*text != '\0' && (re[0] == '.' || re[0] == *text))
 2fe:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 302:	0f b6 00             	movzbl (%rax),%eax
 305:	84 c0                	test   %al,%al
 307:	74 3a                	je     343 <matchhere+0xc6>
 309:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 30d:	0f b6 00             	movzbl (%rax),%eax
 310:	3c 2e                	cmp    $0x2e,%al
 312:	74 12                	je     326 <matchhere+0xa9>
 314:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 318:	0f b6 10             	movzbl (%rax),%edx
 31b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 31f:	0f b6 00             	movzbl (%rax),%eax
 322:	38 c2                	cmp    %al,%dl
 324:	75 1d                	jne    343 <matchhere+0xc6>
    return matchhere(re + 1, text + 1);
 326:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 32a:	48 8d 50 01          	lea    0x1(%rax),%rdx
 32e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 332:	48 83 c0 01          	add    $0x1,%rax
 336:	48 89 d6             	mov    %rdx,%rsi
 339:	48 89 c7             	mov    %rax,%rdi
 33c:	e8 3c ff ff ff       	callq  27d <matchhere>
 341:	eb 05                	jmp    348 <matchhere+0xcb>
  return 0;
 343:	b8 00 00 00 00       	mov    $0x0,%eax
}
 348:	c9                   	leaveq 
 349:	c3                   	retq   

000000000000034a <matchstar>:

// matchstar: search for c*re at beginning of text
int matchstar(int c, char *re, char *text) {
 34a:	55                   	push   %rbp
 34b:	48 89 e5             	mov    %rsp,%rbp
 34e:	48 83 ec 20          	sub    $0x20,%rsp
 352:	89 7d fc             	mov    %edi,-0x4(%rbp)
 355:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
 359:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
  do { // a * matches zero or more instances
    if (matchhere(re, text))
 35d:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
 361:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 365:	48 89 d6             	mov    %rdx,%rsi
 368:	48 89 c7             	mov    %rax,%rdi
 36b:	e8 0d ff ff ff       	callq  27d <matchhere>
 370:	85 c0                	test   %eax,%eax
 372:	74 07                	je     37b <matchstar+0x31>
      return 1;
 374:	b8 01 00 00 00       	mov    $0x1,%eax
 379:	eb 2d                	jmp    3a8 <matchstar+0x5e>
  } while (*text != '\0' && (*text++ == c || c == '.'));
 37b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 37f:	0f b6 00             	movzbl (%rax),%eax
 382:	84 c0                	test   %al,%al
 384:	74 1d                	je     3a3 <matchstar+0x59>
 386:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 38a:	48 8d 50 01          	lea    0x1(%rax),%rdx
 38e:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 392:	0f b6 00             	movzbl (%rax),%eax
 395:	0f be c0             	movsbl %al,%eax
 398:	3b 45 fc             	cmp    -0x4(%rbp),%eax
 39b:	74 c0                	je     35d <matchstar+0x13>
 39d:	83 7d fc 2e          	cmpl   $0x2e,-0x4(%rbp)
 3a1:	74 ba                	je     35d <matchstar+0x13>
  return 0;
 3a3:	b8 00 00 00 00       	mov    $0x0,%eax
}
 3a8:	c9                   	leaveq 
 3a9:	c3                   	retq   

00000000000003aa <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
 3aa:	55                   	push   %rbp
 3ab:	48 89 e5             	mov    %rsp,%rbp
 3ae:	48 83 ec 10          	sub    $0x10,%rsp
 3b2:	89 7d fc             	mov    %edi,-0x4(%rbp)
 3b5:	89 f0                	mov    %esi,%eax
 3b7:	88 45 f8             	mov    %al,-0x8(%rbp)
 3ba:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
 3be:	8b 45 fc             	mov    -0x4(%rbp),%eax
 3c1:	ba 01 00 00 00       	mov    $0x1,%edx
 3c6:	48 89 ce             	mov    %rcx,%rsi
 3c9:	89 c7                	mov    %eax,%edi
 3cb:	e8 57 07 00 00       	callq  b27 <write>
 3d0:	90                   	nop
 3d1:	c9                   	leaveq 
 3d2:	c3                   	retq   

00000000000003d3 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
 3d3:	55                   	push   %rbp
 3d4:	48 89 e5             	mov    %rsp,%rbp
 3d7:	48 83 ec 40          	sub    $0x40,%rsp
 3db:	89 7d cc             	mov    %edi,-0x34(%rbp)
 3de:	89 75 c8             	mov    %esi,-0x38(%rbp)
 3e1:	89 55 c4             	mov    %edx,-0x3c(%rbp)
 3e4:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
 3e7:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 3eb:	74 1f                	je     40c <printint64+0x39>
 3ed:	8b 45 c8             	mov    -0x38(%rbp),%eax
 3f0:	c1 e8 1f             	shr    $0x1f,%eax
 3f3:	0f b6 c0             	movzbl %al,%eax
 3f6:	89 45 c0             	mov    %eax,-0x40(%rbp)
 3f9:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 3fd:	74 0d                	je     40c <printint64+0x39>
    x = -xx;
 3ff:	8b 45 c8             	mov    -0x38(%rbp),%eax
 402:	f7 d8                	neg    %eax
 404:	48 98                	cltq   
 406:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 40a:	eb 09                	jmp    415 <printint64+0x42>
  else
    x = xx;
 40c:	8b 45 c8             	mov    -0x38(%rbp),%eax
 40f:	48 98                	cltq   
 411:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 415:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 41c:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 41f:	8d 41 01             	lea    0x1(%rcx),%eax
 422:	89 45 fc             	mov    %eax,-0x4(%rbp)
 425:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 428:	48 63 f0             	movslq %eax,%rsi
 42b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 42f:	ba 00 00 00 00       	mov    $0x0,%edx
 434:	48 f7 f6             	div    %rsi
 437:	48 89 d0             	mov    %rdx,%rax
 43a:	0f b6 90 70 11 00 00 	movzbl 0x1170(%rax),%edx
 441:	48 63 c1             	movslq %ecx,%rax
 444:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 448:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 44b:	48 63 f8             	movslq %eax,%rdi
 44e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 452:	ba 00 00 00 00       	mov    $0x0,%edx
 457:	48 f7 f7             	div    %rdi
 45a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 45e:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 463:	75 b7                	jne    41c <printint64+0x49>

  if (sgn)
 465:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 469:	74 2b                	je     496 <printint64+0xc3>
    buf[i++] = '-';
 46b:	8b 45 fc             	mov    -0x4(%rbp),%eax
 46e:	8d 50 01             	lea    0x1(%rax),%edx
 471:	89 55 fc             	mov    %edx,-0x4(%rbp)
 474:	48 98                	cltq   
 476:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 47b:	eb 19                	jmp    496 <printint64+0xc3>
    putc(fd, buf[i]);
 47d:	8b 45 fc             	mov    -0x4(%rbp),%eax
 480:	48 98                	cltq   
 482:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 487:	0f be d0             	movsbl %al,%edx
 48a:	8b 45 cc             	mov    -0x34(%rbp),%eax
 48d:	89 d6                	mov    %edx,%esi
 48f:	89 c7                	mov    %eax,%edi
 491:	e8 14 ff ff ff       	callq  3aa <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 496:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 49a:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 49e:	79 dd                	jns    47d <printint64+0xaa>
    putc(fd, buf[i]);
}
 4a0:	90                   	nop
 4a1:	c9                   	leaveq 
 4a2:	c3                   	retq   

00000000000004a3 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 4a3:	55                   	push   %rbp
 4a4:	48 89 e5             	mov    %rsp,%rbp
 4a7:	48 83 ec 30          	sub    $0x30,%rsp
 4ab:	89 7d dc             	mov    %edi,-0x24(%rbp)
 4ae:	89 75 d8             	mov    %esi,-0x28(%rbp)
 4b1:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 4b4:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 4b7:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 4be:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 4c2:	74 17                	je     4db <printint+0x38>
 4c4:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 4c8:	79 11                	jns    4db <printint+0x38>
    neg = 1;
 4ca:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 4d1:	8b 45 d8             	mov    -0x28(%rbp),%eax
 4d4:	f7 d8                	neg    %eax
 4d6:	89 45 f4             	mov    %eax,-0xc(%rbp)
 4d9:	eb 06                	jmp    4e1 <printint+0x3e>
  } else {
    x = xx;
 4db:	8b 45 d8             	mov    -0x28(%rbp),%eax
 4de:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 4e1:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 4e8:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 4eb:	8d 41 01             	lea    0x1(%rcx),%eax
 4ee:	89 45 fc             	mov    %eax,-0x4(%rbp)
 4f1:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 4f4:	8b 45 f4             	mov    -0xc(%rbp),%eax
 4f7:	ba 00 00 00 00       	mov    $0x0,%edx
 4fc:	f7 f6                	div    %esi
 4fe:	89 d0                	mov    %edx,%eax
 500:	89 c0                	mov    %eax,%eax
 502:	0f b6 90 90 11 00 00 	movzbl 0x1190(%rax),%edx
 509:	48 63 c1             	movslq %ecx,%rax
 50c:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 510:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 513:	8b 45 f4             	mov    -0xc(%rbp),%eax
 516:	ba 00 00 00 00       	mov    $0x0,%edx
 51b:	f7 f7                	div    %edi
 51d:	89 45 f4             	mov    %eax,-0xc(%rbp)
 520:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 524:	75 c2                	jne    4e8 <printint+0x45>
  if (neg)
 526:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 52a:	74 2b                	je     557 <printint+0xb4>
    buf[i++] = '-';
 52c:	8b 45 fc             	mov    -0x4(%rbp),%eax
 52f:	8d 50 01             	lea    0x1(%rax),%edx
 532:	89 55 fc             	mov    %edx,-0x4(%rbp)
 535:	48 98                	cltq   
 537:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 53c:	eb 19                	jmp    557 <printint+0xb4>
    putc(fd, buf[i]);
 53e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 541:	48 98                	cltq   
 543:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 548:	0f be d0             	movsbl %al,%edx
 54b:	8b 45 dc             	mov    -0x24(%rbp),%eax
 54e:	89 d6                	mov    %edx,%esi
 550:	89 c7                	mov    %eax,%edi
 552:	e8 53 fe ff ff       	callq  3aa <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 557:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 55b:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 55f:	79 dd                	jns    53e <printint+0x9b>
    putc(fd, buf[i]);
}
 561:	90                   	nop
 562:	c9                   	leaveq 
 563:	c3                   	retq   

0000000000000564 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 564:	55                   	push   %rbp
 565:	48 89 e5             	mov    %rsp,%rbp
 568:	48 83 ec 70          	sub    $0x70,%rsp
 56c:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 56f:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 573:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 577:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 57b:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 57f:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 583:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 58a:	48 8d 45 10          	lea    0x10(%rbp),%rax
 58e:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 592:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 596:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 59a:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 5a1:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 5a8:	e9 68 02 00 00       	jmpq   815 <printf+0x2b1>
    c = fmt[i] & 0xff;
 5ad:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 5b0:	48 63 d0             	movslq %eax,%rdx
 5b3:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 5b7:	48 01 d0             	add    %rdx,%rax
 5ba:	0f b6 00             	movzbl (%rax),%eax
 5bd:	0f be c0             	movsbl %al,%eax
 5c0:	25 ff 00 00 00       	and    $0xff,%eax
 5c5:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 5c8:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 5cc:	75 30                	jne    5fe <printf+0x9a>
      if (c == '%') {
 5ce:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 5d2:	75 13                	jne    5e7 <printf+0x83>
        state = '%';
 5d4:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 5db:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 5e2:	e9 2a 02 00 00       	jmpq   811 <printf+0x2ad>
      } else {
        putc(fd, c);
 5e7:	8b 45 b8             	mov    -0x48(%rbp),%eax
 5ea:	0f be d0             	movsbl %al,%edx
 5ed:	8b 45 9c             	mov    -0x64(%rbp),%eax
 5f0:	89 d6                	mov    %edx,%esi
 5f2:	89 c7                	mov    %eax,%edi
 5f4:	e8 b1 fd ff ff       	callq  3aa <putc>
 5f9:	e9 13 02 00 00       	jmpq   811 <printf+0x2ad>
      }
    } else if (state == '%') {
 5fe:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 602:	0f 85 09 02 00 00    	jne    811 <printf+0x2ad>
      if (c == 'l') {
 608:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 60c:	75 0c                	jne    61a <printf+0xb6>
        lflag = 1;
 60e:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 615:	e9 f7 01 00 00       	jmpq   811 <printf+0x2ad>
      } else if (c == 'd') {
 61a:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 61e:	0f 85 95 00 00 00    	jne    6b9 <printf+0x155>
        if (lflag == 1)
 624:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 628:	75 49                	jne    673 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 62a:	8b 45 a0             	mov    -0x60(%rbp),%eax
 62d:	83 f8 30             	cmp    $0x30,%eax
 630:	73 17                	jae    649 <printf+0xe5>
 632:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 636:	8b 55 a0             	mov    -0x60(%rbp),%edx
 639:	89 d2                	mov    %edx,%edx
 63b:	48 01 d0             	add    %rdx,%rax
 63e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 641:	83 c2 08             	add    $0x8,%edx
 644:	89 55 a0             	mov    %edx,-0x60(%rbp)
 647:	eb 0c                	jmp    655 <printf+0xf1>
 649:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 64d:	48 8d 50 08          	lea    0x8(%rax),%rdx
 651:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 655:	48 8b 00             	mov    (%rax),%rax
 658:	89 c6                	mov    %eax,%esi
 65a:	8b 45 9c             	mov    -0x64(%rbp),%eax
 65d:	b9 01 00 00 00       	mov    $0x1,%ecx
 662:	ba 0a 00 00 00       	mov    $0xa,%edx
 667:	89 c7                	mov    %eax,%edi
 669:	e8 65 fd ff ff       	callq  3d3 <printint64>
 66e:	e9 97 01 00 00       	jmpq   80a <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 673:	8b 45 a0             	mov    -0x60(%rbp),%eax
 676:	83 f8 30             	cmp    $0x30,%eax
 679:	73 17                	jae    692 <printf+0x12e>
 67b:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 67f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 682:	89 d2                	mov    %edx,%edx
 684:	48 01 d0             	add    %rdx,%rax
 687:	8b 55 a0             	mov    -0x60(%rbp),%edx
 68a:	83 c2 08             	add    $0x8,%edx
 68d:	89 55 a0             	mov    %edx,-0x60(%rbp)
 690:	eb 0c                	jmp    69e <printf+0x13a>
 692:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 696:	48 8d 50 08          	lea    0x8(%rax),%rdx
 69a:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 69e:	8b 30                	mov    (%rax),%esi
 6a0:	8b 45 9c             	mov    -0x64(%rbp),%eax
 6a3:	b9 01 00 00 00       	mov    $0x1,%ecx
 6a8:	ba 0a 00 00 00       	mov    $0xa,%edx
 6ad:	89 c7                	mov    %eax,%edi
 6af:	e8 ef fd ff ff       	callq  4a3 <printint>
 6b4:	e9 51 01 00 00       	jmpq   80a <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 6b9:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 6bd:	74 0a                	je     6c9 <printf+0x165>
 6bf:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 6c3:	0f 85 95 00 00 00    	jne    75e <printf+0x1fa>
        if (lflag == 1)
 6c9:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 6cd:	75 49                	jne    718 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 6cf:	8b 45 a0             	mov    -0x60(%rbp),%eax
 6d2:	83 f8 30             	cmp    $0x30,%eax
 6d5:	73 17                	jae    6ee <printf+0x18a>
 6d7:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 6db:	8b 55 a0             	mov    -0x60(%rbp),%edx
 6de:	89 d2                	mov    %edx,%edx
 6e0:	48 01 d0             	add    %rdx,%rax
 6e3:	8b 55 a0             	mov    -0x60(%rbp),%edx
 6e6:	83 c2 08             	add    $0x8,%edx
 6e9:	89 55 a0             	mov    %edx,-0x60(%rbp)
 6ec:	eb 0c                	jmp    6fa <printf+0x196>
 6ee:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 6f2:	48 8d 50 08          	lea    0x8(%rax),%rdx
 6f6:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 6fa:	48 8b 00             	mov    (%rax),%rax
 6fd:	89 c6                	mov    %eax,%esi
 6ff:	8b 45 9c             	mov    -0x64(%rbp),%eax
 702:	b9 00 00 00 00       	mov    $0x0,%ecx
 707:	ba 10 00 00 00       	mov    $0x10,%edx
 70c:	89 c7                	mov    %eax,%edi
 70e:	e8 c0 fc ff ff       	callq  3d3 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 713:	e9 f2 00 00 00       	jmpq   80a <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 718:	8b 45 a0             	mov    -0x60(%rbp),%eax
 71b:	83 f8 30             	cmp    $0x30,%eax
 71e:	73 17                	jae    737 <printf+0x1d3>
 720:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 724:	8b 55 a0             	mov    -0x60(%rbp),%edx
 727:	89 d2                	mov    %edx,%edx
 729:	48 01 d0             	add    %rdx,%rax
 72c:	8b 55 a0             	mov    -0x60(%rbp),%edx
 72f:	83 c2 08             	add    $0x8,%edx
 732:	89 55 a0             	mov    %edx,-0x60(%rbp)
 735:	eb 0c                	jmp    743 <printf+0x1df>
 737:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 73b:	48 8d 50 08          	lea    0x8(%rax),%rdx
 73f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 743:	8b 30                	mov    (%rax),%esi
 745:	8b 45 9c             	mov    -0x64(%rbp),%eax
 748:	b9 00 00 00 00       	mov    $0x0,%ecx
 74d:	ba 10 00 00 00       	mov    $0x10,%edx
 752:	89 c7                	mov    %eax,%edi
 754:	e8 4a fd ff ff       	callq  4a3 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 759:	e9 ac 00 00 00       	jmpq   80a <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 75e:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 762:	75 6b                	jne    7cf <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 764:	8b 45 a0             	mov    -0x60(%rbp),%eax
 767:	83 f8 30             	cmp    $0x30,%eax
 76a:	73 17                	jae    783 <printf+0x21f>
 76c:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 770:	8b 55 a0             	mov    -0x60(%rbp),%edx
 773:	89 d2                	mov    %edx,%edx
 775:	48 01 d0             	add    %rdx,%rax
 778:	8b 55 a0             	mov    -0x60(%rbp),%edx
 77b:	83 c2 08             	add    $0x8,%edx
 77e:	89 55 a0             	mov    %edx,-0x60(%rbp)
 781:	eb 0c                	jmp    78f <printf+0x22b>
 783:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 787:	48 8d 50 08          	lea    0x8(%rax),%rdx
 78b:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 78f:	48 8b 00             	mov    (%rax),%rax
 792:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 796:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 79b:	75 25                	jne    7c2 <printf+0x25e>
          s = "(null)";
 79d:	48 c7 45 c8 86 0e 00 	movq   $0xe86,-0x38(%rbp)
 7a4:	00 
        for (; *s; s++)
 7a5:	eb 1b                	jmp    7c2 <printf+0x25e>
          putc(fd, *s);
 7a7:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 7ab:	0f b6 00             	movzbl (%rax),%eax
 7ae:	0f be d0             	movsbl %al,%edx
 7b1:	8b 45 9c             	mov    -0x64(%rbp),%eax
 7b4:	89 d6                	mov    %edx,%esi
 7b6:	89 c7                	mov    %eax,%edi
 7b8:	e8 ed fb ff ff       	callq  3aa <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 7bd:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 7c2:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 7c6:	0f b6 00             	movzbl (%rax),%eax
 7c9:	84 c0                	test   %al,%al
 7cb:	75 da                	jne    7a7 <printf+0x243>
 7cd:	eb 3b                	jmp    80a <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 7cf:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 7d3:	75 14                	jne    7e9 <printf+0x285>
        putc(fd, c);
 7d5:	8b 45 b8             	mov    -0x48(%rbp),%eax
 7d8:	0f be d0             	movsbl %al,%edx
 7db:	8b 45 9c             	mov    -0x64(%rbp),%eax
 7de:	89 d6                	mov    %edx,%esi
 7e0:	89 c7                	mov    %eax,%edi
 7e2:	e8 c3 fb ff ff       	callq  3aa <putc>
 7e7:	eb 21                	jmp    80a <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 7e9:	8b 45 9c             	mov    -0x64(%rbp),%eax
 7ec:	be 25 00 00 00       	mov    $0x25,%esi
 7f1:	89 c7                	mov    %eax,%edi
 7f3:	e8 b2 fb ff ff       	callq  3aa <putc>
        putc(fd, c);
 7f8:	8b 45 b8             	mov    -0x48(%rbp),%eax
 7fb:	0f be d0             	movsbl %al,%edx
 7fe:	8b 45 9c             	mov    -0x64(%rbp),%eax
 801:	89 d6                	mov    %edx,%esi
 803:	89 c7                	mov    %eax,%edi
 805:	e8 a0 fb ff ff       	callq  3aa <putc>
      }
      state = 0;
 80a:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 811:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 815:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 818:	48 63 d0             	movslq %eax,%rdx
 81b:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 81f:	48 01 d0             	add    %rdx,%rax
 822:	0f b6 00             	movzbl (%rax),%eax
 825:	84 c0                	test   %al,%al
 827:	0f 85 80 fd ff ff    	jne    5ad <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 82d:	90                   	nop
 82e:	c9                   	leaveq 
 82f:	c3                   	retq   

0000000000000830 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 830:	55                   	push   %rbp
 831:	48 89 e5             	mov    %rsp,%rbp
 834:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 838:	89 75 f4             	mov    %esi,-0xc(%rbp)
 83b:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 83e:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 842:	8b 55 f0             	mov    -0x10(%rbp),%edx
 845:	8b 45 f4             	mov    -0xc(%rbp),%eax
 848:	48 89 ce             	mov    %rcx,%rsi
 84b:	48 89 f7             	mov    %rsi,%rdi
 84e:	89 d1                	mov    %edx,%ecx
 850:	fc                   	cld    
 851:	f3 aa                	rep stos %al,%es:(%rdi)
 853:	89 ca                	mov    %ecx,%edx
 855:	48 89 fe             	mov    %rdi,%rsi
 858:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 85c:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 85f:	90                   	nop
 860:	5d                   	pop    %rbp
 861:	c3                   	retq   

0000000000000862 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 862:	55                   	push   %rbp
 863:	48 89 e5             	mov    %rsp,%rbp
 866:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 86a:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 86e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 872:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 876:	90                   	nop
 877:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 87b:	48 8d 50 01          	lea    0x1(%rax),%rdx
 87f:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 883:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 887:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 88b:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 88f:	0f b6 12             	movzbl (%rdx),%edx
 892:	88 10                	mov    %dl,(%rax)
 894:	0f b6 00             	movzbl (%rax),%eax
 897:	84 c0                	test   %al,%al
 899:	75 dc                	jne    877 <strcpy+0x15>
    ;
  return os;
 89b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 89f:	5d                   	pop    %rbp
 8a0:	c3                   	retq   

00000000000008a1 <strcmp>:

int strcmp(const char *p, const char *q) {
 8a1:	55                   	push   %rbp
 8a2:	48 89 e5             	mov    %rsp,%rbp
 8a5:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 8a9:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 8ad:	eb 0a                	jmp    8b9 <strcmp+0x18>
    p++, q++;
 8af:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 8b4:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 8b9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8bd:	0f b6 00             	movzbl (%rax),%eax
 8c0:	84 c0                	test   %al,%al
 8c2:	74 12                	je     8d6 <strcmp+0x35>
 8c4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8c8:	0f b6 10             	movzbl (%rax),%edx
 8cb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8cf:	0f b6 00             	movzbl (%rax),%eax
 8d2:	38 c2                	cmp    %al,%dl
 8d4:	74 d9                	je     8af <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 8d6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8da:	0f b6 00             	movzbl (%rax),%eax
 8dd:	0f b6 d0             	movzbl %al,%edx
 8e0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8e4:	0f b6 00             	movzbl (%rax),%eax
 8e7:	0f b6 c0             	movzbl %al,%eax
 8ea:	29 c2                	sub    %eax,%edx
 8ec:	89 d0                	mov    %edx,%eax
}
 8ee:	5d                   	pop    %rbp
 8ef:	c3                   	retq   

00000000000008f0 <strlen>:

uint strlen(char *s) {
 8f0:	55                   	push   %rbp
 8f1:	48 89 e5             	mov    %rsp,%rbp
 8f4:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 8f8:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 8ff:	eb 04                	jmp    905 <strlen+0x15>
 901:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 905:	8b 45 fc             	mov    -0x4(%rbp),%eax
 908:	48 63 d0             	movslq %eax,%rdx
 90b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 90f:	48 01 d0             	add    %rdx,%rax
 912:	0f b6 00             	movzbl (%rax),%eax
 915:	84 c0                	test   %al,%al
 917:	75 e8                	jne    901 <strlen+0x11>
    ;
  return n;
 919:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 91c:	5d                   	pop    %rbp
 91d:	c3                   	retq   

000000000000091e <memset>:

void *memset(void *dst, int c, uint n) {
 91e:	55                   	push   %rbp
 91f:	48 89 e5             	mov    %rsp,%rbp
 922:	48 83 ec 10          	sub    $0x10,%rsp
 926:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 92a:	89 75 f4             	mov    %esi,-0xc(%rbp)
 92d:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 930:	8b 55 f0             	mov    -0x10(%rbp),%edx
 933:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 936:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 93a:	89 ce                	mov    %ecx,%esi
 93c:	48 89 c7             	mov    %rax,%rdi
 93f:	e8 ec fe ff ff       	callq  830 <stosb>
  return dst;
 944:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 948:	c9                   	leaveq 
 949:	c3                   	retq   

000000000000094a <strchr>:

char *strchr(const char *s, char c) {
 94a:	55                   	push   %rbp
 94b:	48 89 e5             	mov    %rsp,%rbp
 94e:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 952:	89 f0                	mov    %esi,%eax
 954:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 957:	eb 17                	jmp    970 <strchr+0x26>
    if (*s == c)
 959:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 95d:	0f b6 00             	movzbl (%rax),%eax
 960:	3a 45 f4             	cmp    -0xc(%rbp),%al
 963:	75 06                	jne    96b <strchr+0x21>
      return (char *)s;
 965:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 969:	eb 15                	jmp    980 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 96b:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 970:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 974:	0f b6 00             	movzbl (%rax),%eax
 977:	84 c0                	test   %al,%al
 979:	75 de                	jne    959 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 97b:	b8 00 00 00 00       	mov    $0x0,%eax
}
 980:	5d                   	pop    %rbp
 981:	c3                   	retq   

0000000000000982 <gets>:

char *gets(char *buf, int max) {
 982:	55                   	push   %rbp
 983:	48 89 e5             	mov    %rsp,%rbp
 986:	48 83 ec 20          	sub    $0x20,%rsp
 98a:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 98e:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 991:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 998:	eb 48                	jmp    9e2 <gets+0x60>
    cc = read(0, &c, 1);
 99a:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 99e:	ba 01 00 00 00       	mov    $0x1,%edx
 9a3:	48 89 c6             	mov    %rax,%rsi
 9a6:	bf 00 00 00 00       	mov    $0x0,%edi
 9ab:	e8 6f 01 00 00       	callq  b1f <read>
 9b0:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 9b3:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 9b7:	7e 36                	jle    9ef <gets+0x6d>
      break;
    buf[i++] = c;
 9b9:	8b 45 fc             	mov    -0x4(%rbp),%eax
 9bc:	8d 50 01             	lea    0x1(%rax),%edx
 9bf:	89 55 fc             	mov    %edx,-0x4(%rbp)
 9c2:	48 63 d0             	movslq %eax,%rdx
 9c5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 9c9:	48 01 c2             	add    %rax,%rdx
 9cc:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 9d0:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 9d2:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 9d6:	3c 0a                	cmp    $0xa,%al
 9d8:	74 16                	je     9f0 <gets+0x6e>
 9da:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 9de:	3c 0d                	cmp    $0xd,%al
 9e0:	74 0e                	je     9f0 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 9e2:	8b 45 fc             	mov    -0x4(%rbp),%eax
 9e5:	83 c0 01             	add    $0x1,%eax
 9e8:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 9eb:	7c ad                	jl     99a <gets+0x18>
 9ed:	eb 01                	jmp    9f0 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 9ef:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 9f0:	8b 45 fc             	mov    -0x4(%rbp),%eax
 9f3:	48 63 d0             	movslq %eax,%rdx
 9f6:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 9fa:	48 01 d0             	add    %rdx,%rax
 9fd:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 a00:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 a04:	c9                   	leaveq 
 a05:	c3                   	retq   

0000000000000a06 <stat>:

int stat(char *n, struct stat *st) {
 a06:	55                   	push   %rbp
 a07:	48 89 e5             	mov    %rsp,%rbp
 a0a:	48 83 ec 20          	sub    $0x20,%rsp
 a0e:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 a12:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 a16:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a1a:	be 00 00 00 00       	mov    $0x0,%esi
 a1f:	48 89 c7             	mov    %rax,%rdi
 a22:	e8 20 01 00 00       	callq  b47 <open>
 a27:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 a2a:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 a2e:	79 07                	jns    a37 <stat+0x31>
    return -1;
 a30:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 a35:	eb 21                	jmp    a58 <stat+0x52>
  r = fstat(fd, st);
 a37:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 a3b:	8b 45 fc             	mov    -0x4(%rbp),%eax
 a3e:	48 89 d6             	mov    %rdx,%rsi
 a41:	89 c7                	mov    %eax,%edi
 a43:	e8 17 01 00 00       	callq  b5f <fstat>
 a48:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 a4b:	8b 45 fc             	mov    -0x4(%rbp),%eax
 a4e:	89 c7                	mov    %eax,%edi
 a50:	e8 da 00 00 00       	callq  b2f <close>
  return r;
 a55:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 a58:	c9                   	leaveq 
 a59:	c3                   	retq   

0000000000000a5a <atoi>:

int atoi(const char *s) {
 a5a:	55                   	push   %rbp
 a5b:	48 89 e5             	mov    %rsp,%rbp
 a5e:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 a62:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 a69:	eb 28                	jmp    a93 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 a6b:	8b 55 fc             	mov    -0x4(%rbp),%edx
 a6e:	89 d0                	mov    %edx,%eax
 a70:	c1 e0 02             	shl    $0x2,%eax
 a73:	01 d0                	add    %edx,%eax
 a75:	01 c0                	add    %eax,%eax
 a77:	89 c1                	mov    %eax,%ecx
 a79:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a7d:	48 8d 50 01          	lea    0x1(%rax),%rdx
 a81:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 a85:	0f b6 00             	movzbl (%rax),%eax
 a88:	0f be c0             	movsbl %al,%eax
 a8b:	01 c8                	add    %ecx,%eax
 a8d:	83 e8 30             	sub    $0x30,%eax
 a90:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 a93:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a97:	0f b6 00             	movzbl (%rax),%eax
 a9a:	3c 2f                	cmp    $0x2f,%al
 a9c:	7e 0b                	jle    aa9 <atoi+0x4f>
 a9e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 aa2:	0f b6 00             	movzbl (%rax),%eax
 aa5:	3c 39                	cmp    $0x39,%al
 aa7:	7e c2                	jle    a6b <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 aa9:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 aac:	5d                   	pop    %rbp
 aad:	c3                   	retq   

0000000000000aae <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 aae:	55                   	push   %rbp
 aaf:	48 89 e5             	mov    %rsp,%rbp
 ab2:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 ab6:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 aba:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 abd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 ac1:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 ac5:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 ac9:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 acd:	eb 1d                	jmp    aec <memmove+0x3e>
    *dst++ = *src++;
 acf:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ad3:	48 8d 50 01          	lea    0x1(%rax),%rdx
 ad7:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 adb:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 adf:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 ae3:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 ae7:	0f b6 12             	movzbl (%rdx),%edx
 aea:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 aec:	8b 45 dc             	mov    -0x24(%rbp),%eax
 aef:	8d 50 ff             	lea    -0x1(%rax),%edx
 af2:	89 55 dc             	mov    %edx,-0x24(%rbp)
 af5:	85 c0                	test   %eax,%eax
 af7:	7f d6                	jg     acf <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 af9:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 afd:	5d                   	pop    %rbp
 afe:	c3                   	retq   

0000000000000aff <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 aff:	b8 01 00 00 00       	mov    $0x1,%eax
 b04:	cd 40                	int    $0x40
 b06:	c3                   	retq   

0000000000000b07 <exit>:
SYSCALL(exit)
 b07:	b8 02 00 00 00       	mov    $0x2,%eax
 b0c:	cd 40                	int    $0x40
 b0e:	c3                   	retq   

0000000000000b0f <wait>:
SYSCALL(wait)
 b0f:	b8 03 00 00 00       	mov    $0x3,%eax
 b14:	cd 40                	int    $0x40
 b16:	c3                   	retq   

0000000000000b17 <pipe>:
SYSCALL(pipe)
 b17:	b8 04 00 00 00       	mov    $0x4,%eax
 b1c:	cd 40                	int    $0x40
 b1e:	c3                   	retq   

0000000000000b1f <read>:
SYSCALL(read)
 b1f:	b8 05 00 00 00       	mov    $0x5,%eax
 b24:	cd 40                	int    $0x40
 b26:	c3                   	retq   

0000000000000b27 <write>:
SYSCALL(write)
 b27:	b8 10 00 00 00       	mov    $0x10,%eax
 b2c:	cd 40                	int    $0x40
 b2e:	c3                   	retq   

0000000000000b2f <close>:
SYSCALL(close)
 b2f:	b8 15 00 00 00       	mov    $0x15,%eax
 b34:	cd 40                	int    $0x40
 b36:	c3                   	retq   

0000000000000b37 <kill>:
SYSCALL(kill)
 b37:	b8 06 00 00 00       	mov    $0x6,%eax
 b3c:	cd 40                	int    $0x40
 b3e:	c3                   	retq   

0000000000000b3f <exec>:
SYSCALL(exec)
 b3f:	b8 07 00 00 00       	mov    $0x7,%eax
 b44:	cd 40                	int    $0x40
 b46:	c3                   	retq   

0000000000000b47 <open>:
SYSCALL(open)
 b47:	b8 0f 00 00 00       	mov    $0xf,%eax
 b4c:	cd 40                	int    $0x40
 b4e:	c3                   	retq   

0000000000000b4f <mknod>:
SYSCALL(mknod)
 b4f:	b8 11 00 00 00       	mov    $0x11,%eax
 b54:	cd 40                	int    $0x40
 b56:	c3                   	retq   

0000000000000b57 <unlink>:
SYSCALL(unlink)
 b57:	b8 12 00 00 00       	mov    $0x12,%eax
 b5c:	cd 40                	int    $0x40
 b5e:	c3                   	retq   

0000000000000b5f <fstat>:
SYSCALL(fstat)
 b5f:	b8 08 00 00 00       	mov    $0x8,%eax
 b64:	cd 40                	int    $0x40
 b66:	c3                   	retq   

0000000000000b67 <link>:
SYSCALL(link)
 b67:	b8 13 00 00 00       	mov    $0x13,%eax
 b6c:	cd 40                	int    $0x40
 b6e:	c3                   	retq   

0000000000000b6f <mkdir>:
SYSCALL(mkdir)
 b6f:	b8 14 00 00 00       	mov    $0x14,%eax
 b74:	cd 40                	int    $0x40
 b76:	c3                   	retq   

0000000000000b77 <chdir>:
SYSCALL(chdir)
 b77:	b8 09 00 00 00       	mov    $0x9,%eax
 b7c:	cd 40                	int    $0x40
 b7e:	c3                   	retq   

0000000000000b7f <dup>:
SYSCALL(dup)
 b7f:	b8 0a 00 00 00       	mov    $0xa,%eax
 b84:	cd 40                	int    $0x40
 b86:	c3                   	retq   

0000000000000b87 <getpid>:
SYSCALL(getpid)
 b87:	b8 0b 00 00 00       	mov    $0xb,%eax
 b8c:	cd 40                	int    $0x40
 b8e:	c3                   	retq   

0000000000000b8f <sbrk>:
SYSCALL(sbrk)
 b8f:	b8 0c 00 00 00       	mov    $0xc,%eax
 b94:	cd 40                	int    $0x40
 b96:	c3                   	retq   

0000000000000b97 <sleep>:
SYSCALL(sleep)
 b97:	b8 0d 00 00 00       	mov    $0xd,%eax
 b9c:	cd 40                	int    $0x40
 b9e:	c3                   	retq   

0000000000000b9f <uptime>:
SYSCALL(uptime)
 b9f:	b8 0e 00 00 00       	mov    $0xe,%eax
 ba4:	cd 40                	int    $0x40
 ba6:	c3                   	retq   

0000000000000ba7 <sysinfo>:
SYSCALL(sysinfo)
 ba7:	b8 16 00 00 00       	mov    $0x16,%eax
 bac:	cd 40                	int    $0x40
 bae:	c3                   	retq   

0000000000000baf <crashn>:
SYSCALL(crashn)
 baf:	b8 17 00 00 00       	mov    $0x17,%eax
 bb4:	cd 40                	int    $0x40
 bb6:	c3                   	retq   

0000000000000bb7 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 bb7:	55                   	push   %rbp
 bb8:	48 89 e5             	mov    %rsp,%rbp
 bbb:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 bbf:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 bc3:	48 83 e8 10          	sub    $0x10,%rax
 bc7:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 bcb:	48 8b 05 fe 05 00 00 	mov    0x5fe(%rip),%rax        # 11d0 <freep>
 bd2:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 bd6:	eb 2f                	jmp    c07 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 bd8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bdc:	48 8b 00             	mov    (%rax),%rax
 bdf:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 be3:	77 17                	ja     bfc <free+0x45>
 be5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 be9:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 bed:	77 2f                	ja     c1e <free+0x67>
 bef:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bf3:	48 8b 00             	mov    (%rax),%rax
 bf6:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 bfa:	77 22                	ja     c1e <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 bfc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c00:	48 8b 00             	mov    (%rax),%rax
 c03:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 c07:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c0b:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 c0f:	76 c7                	jbe    bd8 <free+0x21>
 c11:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c15:	48 8b 00             	mov    (%rax),%rax
 c18:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 c1c:	76 ba                	jbe    bd8 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 c1e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c22:	8b 40 08             	mov    0x8(%rax),%eax
 c25:	89 c0                	mov    %eax,%eax
 c27:	48 c1 e0 04          	shl    $0x4,%rax
 c2b:	48 89 c2             	mov    %rax,%rdx
 c2e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c32:	48 01 c2             	add    %rax,%rdx
 c35:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c39:	48 8b 00             	mov    (%rax),%rax
 c3c:	48 39 c2             	cmp    %rax,%rdx
 c3f:	75 2d                	jne    c6e <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 c41:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c45:	8b 50 08             	mov    0x8(%rax),%edx
 c48:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c4c:	48 8b 00             	mov    (%rax),%rax
 c4f:	8b 40 08             	mov    0x8(%rax),%eax
 c52:	01 c2                	add    %eax,%edx
 c54:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c58:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 c5b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c5f:	48 8b 00             	mov    (%rax),%rax
 c62:	48 8b 10             	mov    (%rax),%rdx
 c65:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c69:	48 89 10             	mov    %rdx,(%rax)
 c6c:	eb 0e                	jmp    c7c <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 c6e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c72:	48 8b 10             	mov    (%rax),%rdx
 c75:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c79:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 c7c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c80:	8b 40 08             	mov    0x8(%rax),%eax
 c83:	89 c0                	mov    %eax,%eax
 c85:	48 c1 e0 04          	shl    $0x4,%rax
 c89:	48 89 c2             	mov    %rax,%rdx
 c8c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c90:	48 01 d0             	add    %rdx,%rax
 c93:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 c97:	75 27                	jne    cc0 <free+0x109>
    p->s.size += bp->s.size;
 c99:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c9d:	8b 50 08             	mov    0x8(%rax),%edx
 ca0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ca4:	8b 40 08             	mov    0x8(%rax),%eax
 ca7:	01 c2                	add    %eax,%edx
 ca9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 cad:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 cb0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 cb4:	48 8b 10             	mov    (%rax),%rdx
 cb7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 cbb:	48 89 10             	mov    %rdx,(%rax)
 cbe:	eb 0b                	jmp    ccb <free+0x114>
  } else
    p->s.ptr = bp;
 cc0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 cc4:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 cc8:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 ccb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ccf:	48 89 05 fa 04 00 00 	mov    %rax,0x4fa(%rip)        # 11d0 <freep>
}
 cd6:	90                   	nop
 cd7:	5d                   	pop    %rbp
 cd8:	c3                   	retq   

0000000000000cd9 <morecore>:

static Header *morecore(uint nu) {
 cd9:	55                   	push   %rbp
 cda:	48 89 e5             	mov    %rsp,%rbp
 cdd:	48 83 ec 20          	sub    $0x20,%rsp
 ce1:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 ce4:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 ceb:	77 07                	ja     cf4 <morecore+0x1b>
    nu = 4096;
 ced:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 cf4:	8b 45 ec             	mov    -0x14(%rbp),%eax
 cf7:	c1 e0 04             	shl    $0x4,%eax
 cfa:	89 c7                	mov    %eax,%edi
 cfc:	e8 8e fe ff ff       	callq  b8f <sbrk>
 d01:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 d05:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 d0a:	75 07                	jne    d13 <morecore+0x3a>
    return 0;
 d0c:	b8 00 00 00 00       	mov    $0x0,%eax
 d11:	eb 29                	jmp    d3c <morecore+0x63>
  hp = (Header *)p;
 d13:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 d17:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 d1b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d1f:	8b 55 ec             	mov    -0x14(%rbp),%edx
 d22:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 d25:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d29:	48 83 c0 10          	add    $0x10,%rax
 d2d:	48 89 c7             	mov    %rax,%rdi
 d30:	e8 82 fe ff ff       	callq  bb7 <free>
  return freep;
 d35:	48 8b 05 94 04 00 00 	mov    0x494(%rip),%rax        # 11d0 <freep>
}
 d3c:	c9                   	leaveq 
 d3d:	c3                   	retq   

0000000000000d3e <malloc>:

void *malloc(uint nbytes) {
 d3e:	55                   	push   %rbp
 d3f:	48 89 e5             	mov    %rsp,%rbp
 d42:	48 83 ec 30          	sub    $0x30,%rsp
 d46:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 d49:	8b 45 dc             	mov    -0x24(%rbp),%eax
 d4c:	48 83 c0 0f          	add    $0xf,%rax
 d50:	48 c1 e8 04          	shr    $0x4,%rax
 d54:	83 c0 01             	add    $0x1,%eax
 d57:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 d5a:	48 8b 05 6f 04 00 00 	mov    0x46f(%rip),%rax        # 11d0 <freep>
 d61:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 d65:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 d6a:	75 2b                	jne    d97 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 d6c:	48 c7 45 f0 c0 11 00 	movq   $0x11c0,-0x10(%rbp)
 d73:	00 
 d74:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d78:	48 89 05 51 04 00 00 	mov    %rax,0x451(%rip)        # 11d0 <freep>
 d7f:	48 8b 05 4a 04 00 00 	mov    0x44a(%rip),%rax        # 11d0 <freep>
 d86:	48 89 05 33 04 00 00 	mov    %rax,0x433(%rip)        # 11c0 <base>
    base.s.size = 0;
 d8d:	c7 05 31 04 00 00 00 	movl   $0x0,0x431(%rip)        # 11c8 <base+0x8>
 d94:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 d97:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d9b:	48 8b 00             	mov    (%rax),%rax
 d9e:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 da2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 da6:	8b 40 08             	mov    0x8(%rax),%eax
 da9:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 dac:	72 5f                	jb     e0d <malloc+0xcf>
      if (p->s.size == nunits)
 dae:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 db2:	8b 40 08             	mov    0x8(%rax),%eax
 db5:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 db8:	75 10                	jne    dca <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 dba:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 dbe:	48 8b 10             	mov    (%rax),%rdx
 dc1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 dc5:	48 89 10             	mov    %rdx,(%rax)
 dc8:	eb 2e                	jmp    df8 <malloc+0xba>
      else {
        p->s.size -= nunits;
 dca:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 dce:	8b 40 08             	mov    0x8(%rax),%eax
 dd1:	2b 45 ec             	sub    -0x14(%rbp),%eax
 dd4:	89 c2                	mov    %eax,%edx
 dd6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 dda:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 ddd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 de1:	8b 40 08             	mov    0x8(%rax),%eax
 de4:	89 c0                	mov    %eax,%eax
 de6:	48 c1 e0 04          	shl    $0x4,%rax
 dea:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 dee:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 df2:	8b 55 ec             	mov    -0x14(%rbp),%edx
 df5:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 df8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 dfc:	48 89 05 cd 03 00 00 	mov    %rax,0x3cd(%rip)        # 11d0 <freep>
      return (void *)(p + 1);
 e03:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 e07:	48 83 c0 10          	add    $0x10,%rax
 e0b:	eb 41                	jmp    e4e <malloc+0x110>
    }
    if (p == freep)
 e0d:	48 8b 05 bc 03 00 00 	mov    0x3bc(%rip),%rax        # 11d0 <freep>
 e14:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 e18:	75 1c                	jne    e36 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 e1a:	8b 45 ec             	mov    -0x14(%rbp),%eax
 e1d:	89 c7                	mov    %eax,%edi
 e1f:	e8 b5 fe ff ff       	callq  cd9 <morecore>
 e24:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 e28:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 e2d:	75 07                	jne    e36 <malloc+0xf8>
        return 0;
 e2f:	b8 00 00 00 00       	mov    $0x0,%eax
 e34:	eb 18                	jmp    e4e <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 e36:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 e3a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 e3e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 e42:	48 8b 00             	mov    (%rax),%rax
 e45:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 e49:	e9 54 ff ff ff       	jmpq   da2 <malloc+0x64>
 e4e:	c9                   	leaveq 
 e4f:	c3                   	retq   
