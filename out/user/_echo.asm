
out/user/_echo:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <cdefs.h>
#include <stat.h>
#include <user.h>

int main(int argc, char *argv[]) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 20          	sub    $0x20,%rsp
   8:	89 7d ec             	mov    %edi,-0x14(%rbp)
   b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int i;

  for (i = 1; i < argc; i++)
   f:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%rbp)
  16:	eb 4c                	jmp    64 <main+0x64>
    printf(1, "%s%s", argv[i], i + 1 < argc ? " " : "\n");
  18:	8b 45 fc             	mov    -0x4(%rbp),%eax
  1b:	83 c0 01             	add    $0x1,%eax
  1e:	3b 45 ec             	cmp    -0x14(%rbp),%eax
  21:	7d 07                	jge    2a <main+0x2a>
  23:	ba 17 0b 00 00       	mov    $0xb17,%edx
  28:	eb 05                	jmp    2f <main+0x2f>
  2a:	ba 19 0b 00 00       	mov    $0xb19,%edx
  2f:	8b 45 fc             	mov    -0x4(%rbp),%eax
  32:	48 98                	cltq   
  34:	48 8d 0c c5 00 00 00 	lea    0x0(,%rax,8),%rcx
  3b:	00 
  3c:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  40:	48 01 c8             	add    %rcx,%rax
  43:	48 8b 00             	mov    (%rax),%rax
  46:	48 89 d1             	mov    %rdx,%rcx
  49:	48 89 c2             	mov    %rax,%rdx
  4c:	be 1b 0b 00 00       	mov    $0xb1b,%esi
  51:	bf 01 00 00 00       	mov    $0x1,%edi
  56:	b8 00 00 00 00       	mov    $0x0,%eax
  5b:	e8 cb 01 00 00       	callq  22b <printf>
#include <user.h>

int main(int argc, char *argv[]) {
  int i;

  for (i = 1; i < argc; i++)
  60:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  64:	8b 45 fc             	mov    -0x4(%rbp),%eax
  67:	3b 45 ec             	cmp    -0x14(%rbp),%eax
  6a:	7c ac                	jl     18 <main+0x18>
    printf(1, "%s%s", argv[i], i + 1 < argc ? " " : "\n");
  exit();
  6c:	e8 5d 07 00 00       	callq  7ce <exit>

0000000000000071 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  71:	55                   	push   %rbp
  72:	48 89 e5             	mov    %rsp,%rbp
  75:	48 83 ec 10          	sub    $0x10,%rsp
  79:	89 7d fc             	mov    %edi,-0x4(%rbp)
  7c:	89 f0                	mov    %esi,%eax
  7e:	88 45 f8             	mov    %al,-0x8(%rbp)
  81:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  85:	8b 45 fc             	mov    -0x4(%rbp),%eax
  88:	ba 01 00 00 00       	mov    $0x1,%edx
  8d:	48 89 ce             	mov    %rcx,%rsi
  90:	89 c7                	mov    %eax,%edi
  92:	e8 57 07 00 00       	callq  7ee <write>
  97:	90                   	nop
  98:	c9                   	leaveq 
  99:	c3                   	retq   

000000000000009a <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  9a:	55                   	push   %rbp
  9b:	48 89 e5             	mov    %rsp,%rbp
  9e:	48 83 ec 40          	sub    $0x40,%rsp
  a2:	89 7d cc             	mov    %edi,-0x34(%rbp)
  a5:	89 75 c8             	mov    %esi,-0x38(%rbp)
  a8:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  ab:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  ae:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  b2:	74 1f                	je     d3 <printint64+0x39>
  b4:	8b 45 c8             	mov    -0x38(%rbp),%eax
  b7:	c1 e8 1f             	shr    $0x1f,%eax
  ba:	0f b6 c0             	movzbl %al,%eax
  bd:	89 45 c0             	mov    %eax,-0x40(%rbp)
  c0:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  c4:	74 0d                	je     d3 <printint64+0x39>
    x = -xx;
  c6:	8b 45 c8             	mov    -0x38(%rbp),%eax
  c9:	f7 d8                	neg    %eax
  cb:	48 98                	cltq   
  cd:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  d1:	eb 09                	jmp    dc <printint64+0x42>
  else
    x = xx;
  d3:	8b 45 c8             	mov    -0x38(%rbp),%eax
  d6:	48 98                	cltq   
  d8:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
  dc:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
  e3:	8b 4d fc             	mov    -0x4(%rbp),%ecx
  e6:	8d 41 01             	lea    0x1(%rcx),%eax
  e9:	89 45 fc             	mov    %eax,-0x4(%rbp)
  ec:	8b 45 c4             	mov    -0x3c(%rbp),%eax
  ef:	48 63 f0             	movslq %eax,%rsi
  f2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  f6:	ba 00 00 00 00       	mov    $0x0,%edx
  fb:	48 f7 f6             	div    %rsi
  fe:	48 89 d0             	mov    %rdx,%rax
 101:	0f b6 90 80 0d 00 00 	movzbl 0xd80(%rax),%edx
 108:	48 63 c1             	movslq %ecx,%rax
 10b:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 10f:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 112:	48 63 f8             	movslq %eax,%rdi
 115:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 119:	ba 00 00 00 00       	mov    $0x0,%edx
 11e:	48 f7 f7             	div    %rdi
 121:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 125:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 12a:	75 b7                	jne    e3 <printint64+0x49>

  if (sgn)
 12c:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 130:	74 2b                	je     15d <printint64+0xc3>
    buf[i++] = '-';
 132:	8b 45 fc             	mov    -0x4(%rbp),%eax
 135:	8d 50 01             	lea    0x1(%rax),%edx
 138:	89 55 fc             	mov    %edx,-0x4(%rbp)
 13b:	48 98                	cltq   
 13d:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 142:	eb 19                	jmp    15d <printint64+0xc3>
    putc(fd, buf[i]);
 144:	8b 45 fc             	mov    -0x4(%rbp),%eax
 147:	48 98                	cltq   
 149:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 14e:	0f be d0             	movsbl %al,%edx
 151:	8b 45 cc             	mov    -0x34(%rbp),%eax
 154:	89 d6                	mov    %edx,%esi
 156:	89 c7                	mov    %eax,%edi
 158:	e8 14 ff ff ff       	callq  71 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 15d:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 161:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 165:	79 dd                	jns    144 <printint64+0xaa>
    putc(fd, buf[i]);
}
 167:	90                   	nop
 168:	c9                   	leaveq 
 169:	c3                   	retq   

000000000000016a <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 16a:	55                   	push   %rbp
 16b:	48 89 e5             	mov    %rsp,%rbp
 16e:	48 83 ec 30          	sub    $0x30,%rsp
 172:	89 7d dc             	mov    %edi,-0x24(%rbp)
 175:	89 75 d8             	mov    %esi,-0x28(%rbp)
 178:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 17b:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 17e:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 185:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 189:	74 17                	je     1a2 <printint+0x38>
 18b:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 18f:	79 11                	jns    1a2 <printint+0x38>
    neg = 1;
 191:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 198:	8b 45 d8             	mov    -0x28(%rbp),%eax
 19b:	f7 d8                	neg    %eax
 19d:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1a0:	eb 06                	jmp    1a8 <printint+0x3e>
  } else {
    x = xx;
 1a2:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1a5:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 1a8:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1af:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1b2:	8d 41 01             	lea    0x1(%rcx),%eax
 1b5:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1b8:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 1bb:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1be:	ba 00 00 00 00       	mov    $0x0,%edx
 1c3:	f7 f6                	div    %esi
 1c5:	89 d0                	mov    %edx,%eax
 1c7:	89 c0                	mov    %eax,%eax
 1c9:	0f b6 90 a0 0d 00 00 	movzbl 0xda0(%rax),%edx
 1d0:	48 63 c1             	movslq %ecx,%rax
 1d3:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 1d7:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 1da:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1dd:	ba 00 00 00 00       	mov    $0x0,%edx
 1e2:	f7 f7                	div    %edi
 1e4:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1e7:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 1eb:	75 c2                	jne    1af <printint+0x45>
  if (neg)
 1ed:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 1f1:	74 2b                	je     21e <printint+0xb4>
    buf[i++] = '-';
 1f3:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1f6:	8d 50 01             	lea    0x1(%rax),%edx
 1f9:	89 55 fc             	mov    %edx,-0x4(%rbp)
 1fc:	48 98                	cltq   
 1fe:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 203:	eb 19                	jmp    21e <printint+0xb4>
    putc(fd, buf[i]);
 205:	8b 45 fc             	mov    -0x4(%rbp),%eax
 208:	48 98                	cltq   
 20a:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 20f:	0f be d0             	movsbl %al,%edx
 212:	8b 45 dc             	mov    -0x24(%rbp),%eax
 215:	89 d6                	mov    %edx,%esi
 217:	89 c7                	mov    %eax,%edi
 219:	e8 53 fe ff ff       	callq  71 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 21e:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 222:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 226:	79 dd                	jns    205 <printint+0x9b>
    putc(fd, buf[i]);
}
 228:	90                   	nop
 229:	c9                   	leaveq 
 22a:	c3                   	retq   

000000000000022b <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 22b:	55                   	push   %rbp
 22c:	48 89 e5             	mov    %rsp,%rbp
 22f:	48 83 ec 70          	sub    $0x70,%rsp
 233:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 236:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 23a:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 23e:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 242:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 246:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 24a:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 251:	48 8d 45 10          	lea    0x10(%rbp),%rax
 255:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 259:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 25d:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 261:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 268:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 26f:	e9 68 02 00 00       	jmpq   4dc <printf+0x2b1>
    c = fmt[i] & 0xff;
 274:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 277:	48 63 d0             	movslq %eax,%rdx
 27a:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 27e:	48 01 d0             	add    %rdx,%rax
 281:	0f b6 00             	movzbl (%rax),%eax
 284:	0f be c0             	movsbl %al,%eax
 287:	25 ff 00 00 00       	and    $0xff,%eax
 28c:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 28f:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 293:	75 30                	jne    2c5 <printf+0x9a>
      if (c == '%') {
 295:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 299:	75 13                	jne    2ae <printf+0x83>
        state = '%';
 29b:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 2a2:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 2a9:	e9 2a 02 00 00       	jmpq   4d8 <printf+0x2ad>
      } else {
        putc(fd, c);
 2ae:	8b 45 b8             	mov    -0x48(%rbp),%eax
 2b1:	0f be d0             	movsbl %al,%edx
 2b4:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2b7:	89 d6                	mov    %edx,%esi
 2b9:	89 c7                	mov    %eax,%edi
 2bb:	e8 b1 fd ff ff       	callq  71 <putc>
 2c0:	e9 13 02 00 00       	jmpq   4d8 <printf+0x2ad>
      }
    } else if (state == '%') {
 2c5:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 2c9:	0f 85 09 02 00 00    	jne    4d8 <printf+0x2ad>
      if (c == 'l') {
 2cf:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 2d3:	75 0c                	jne    2e1 <printf+0xb6>
        lflag = 1;
 2d5:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 2dc:	e9 f7 01 00 00       	jmpq   4d8 <printf+0x2ad>
      } else if (c == 'd') {
 2e1:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 2e5:	0f 85 95 00 00 00    	jne    380 <printf+0x155>
        if (lflag == 1)
 2eb:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 2ef:	75 49                	jne    33a <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 2f1:	8b 45 a0             	mov    -0x60(%rbp),%eax
 2f4:	83 f8 30             	cmp    $0x30,%eax
 2f7:	73 17                	jae    310 <printf+0xe5>
 2f9:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 2fd:	8b 55 a0             	mov    -0x60(%rbp),%edx
 300:	89 d2                	mov    %edx,%edx
 302:	48 01 d0             	add    %rdx,%rax
 305:	8b 55 a0             	mov    -0x60(%rbp),%edx
 308:	83 c2 08             	add    $0x8,%edx
 30b:	89 55 a0             	mov    %edx,-0x60(%rbp)
 30e:	eb 0c                	jmp    31c <printf+0xf1>
 310:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 314:	48 8d 50 08          	lea    0x8(%rax),%rdx
 318:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 31c:	48 8b 00             	mov    (%rax),%rax
 31f:	89 c6                	mov    %eax,%esi
 321:	8b 45 9c             	mov    -0x64(%rbp),%eax
 324:	b9 01 00 00 00       	mov    $0x1,%ecx
 329:	ba 0a 00 00 00       	mov    $0xa,%edx
 32e:	89 c7                	mov    %eax,%edi
 330:	e8 65 fd ff ff       	callq  9a <printint64>
 335:	e9 97 01 00 00       	jmpq   4d1 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 33a:	8b 45 a0             	mov    -0x60(%rbp),%eax
 33d:	83 f8 30             	cmp    $0x30,%eax
 340:	73 17                	jae    359 <printf+0x12e>
 342:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 346:	8b 55 a0             	mov    -0x60(%rbp),%edx
 349:	89 d2                	mov    %edx,%edx
 34b:	48 01 d0             	add    %rdx,%rax
 34e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 351:	83 c2 08             	add    $0x8,%edx
 354:	89 55 a0             	mov    %edx,-0x60(%rbp)
 357:	eb 0c                	jmp    365 <printf+0x13a>
 359:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 35d:	48 8d 50 08          	lea    0x8(%rax),%rdx
 361:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 365:	8b 30                	mov    (%rax),%esi
 367:	8b 45 9c             	mov    -0x64(%rbp),%eax
 36a:	b9 01 00 00 00       	mov    $0x1,%ecx
 36f:	ba 0a 00 00 00       	mov    $0xa,%edx
 374:	89 c7                	mov    %eax,%edi
 376:	e8 ef fd ff ff       	callq  16a <printint>
 37b:	e9 51 01 00 00       	jmpq   4d1 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 380:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 384:	74 0a                	je     390 <printf+0x165>
 386:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 38a:	0f 85 95 00 00 00    	jne    425 <printf+0x1fa>
        if (lflag == 1)
 390:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 394:	75 49                	jne    3df <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 396:	8b 45 a0             	mov    -0x60(%rbp),%eax
 399:	83 f8 30             	cmp    $0x30,%eax
 39c:	73 17                	jae    3b5 <printf+0x18a>
 39e:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3a2:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3a5:	89 d2                	mov    %edx,%edx
 3a7:	48 01 d0             	add    %rdx,%rax
 3aa:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3ad:	83 c2 08             	add    $0x8,%edx
 3b0:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3b3:	eb 0c                	jmp    3c1 <printf+0x196>
 3b5:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3b9:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3bd:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3c1:	48 8b 00             	mov    (%rax),%rax
 3c4:	89 c6                	mov    %eax,%esi
 3c6:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3c9:	b9 00 00 00 00       	mov    $0x0,%ecx
 3ce:	ba 10 00 00 00       	mov    $0x10,%edx
 3d3:	89 c7                	mov    %eax,%edi
 3d5:	e8 c0 fc ff ff       	callq  9a <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 3da:	e9 f2 00 00 00       	jmpq   4d1 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 3df:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3e2:	83 f8 30             	cmp    $0x30,%eax
 3e5:	73 17                	jae    3fe <printf+0x1d3>
 3e7:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3eb:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3ee:	89 d2                	mov    %edx,%edx
 3f0:	48 01 d0             	add    %rdx,%rax
 3f3:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3f6:	83 c2 08             	add    $0x8,%edx
 3f9:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3fc:	eb 0c                	jmp    40a <printf+0x1df>
 3fe:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 402:	48 8d 50 08          	lea    0x8(%rax),%rdx
 406:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 40a:	8b 30                	mov    (%rax),%esi
 40c:	8b 45 9c             	mov    -0x64(%rbp),%eax
 40f:	b9 00 00 00 00       	mov    $0x0,%ecx
 414:	ba 10 00 00 00       	mov    $0x10,%edx
 419:	89 c7                	mov    %eax,%edi
 41b:	e8 4a fd ff ff       	callq  16a <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 420:	e9 ac 00 00 00       	jmpq   4d1 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 425:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 429:	75 6b                	jne    496 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 42b:	8b 45 a0             	mov    -0x60(%rbp),%eax
 42e:	83 f8 30             	cmp    $0x30,%eax
 431:	73 17                	jae    44a <printf+0x21f>
 433:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 437:	8b 55 a0             	mov    -0x60(%rbp),%edx
 43a:	89 d2                	mov    %edx,%edx
 43c:	48 01 d0             	add    %rdx,%rax
 43f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 442:	83 c2 08             	add    $0x8,%edx
 445:	89 55 a0             	mov    %edx,-0x60(%rbp)
 448:	eb 0c                	jmp    456 <printf+0x22b>
 44a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 44e:	48 8d 50 08          	lea    0x8(%rax),%rdx
 452:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 456:	48 8b 00             	mov    (%rax),%rax
 459:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 45d:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 462:	75 25                	jne    489 <printf+0x25e>
          s = "(null)";
 464:	48 c7 45 c8 20 0b 00 	movq   $0xb20,-0x38(%rbp)
 46b:	00 
        for (; *s; s++)
 46c:	eb 1b                	jmp    489 <printf+0x25e>
          putc(fd, *s);
 46e:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 472:	0f b6 00             	movzbl (%rax),%eax
 475:	0f be d0             	movsbl %al,%edx
 478:	8b 45 9c             	mov    -0x64(%rbp),%eax
 47b:	89 d6                	mov    %edx,%esi
 47d:	89 c7                	mov    %eax,%edi
 47f:	e8 ed fb ff ff       	callq  71 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 484:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 489:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 48d:	0f b6 00             	movzbl (%rax),%eax
 490:	84 c0                	test   %al,%al
 492:	75 da                	jne    46e <printf+0x243>
 494:	eb 3b                	jmp    4d1 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 496:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 49a:	75 14                	jne    4b0 <printf+0x285>
        putc(fd, c);
 49c:	8b 45 b8             	mov    -0x48(%rbp),%eax
 49f:	0f be d0             	movsbl %al,%edx
 4a2:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4a5:	89 d6                	mov    %edx,%esi
 4a7:	89 c7                	mov    %eax,%edi
 4a9:	e8 c3 fb ff ff       	callq  71 <putc>
 4ae:	eb 21                	jmp    4d1 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 4b0:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4b3:	be 25 00 00 00       	mov    $0x25,%esi
 4b8:	89 c7                	mov    %eax,%edi
 4ba:	e8 b2 fb ff ff       	callq  71 <putc>
        putc(fd, c);
 4bf:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4c2:	0f be d0             	movsbl %al,%edx
 4c5:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4c8:	89 d6                	mov    %edx,%esi
 4ca:	89 c7                	mov    %eax,%edi
 4cc:	e8 a0 fb ff ff       	callq  71 <putc>
      }
      state = 0;
 4d1:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 4d8:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 4dc:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 4df:	48 63 d0             	movslq %eax,%rdx
 4e2:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 4e6:	48 01 d0             	add    %rdx,%rax
 4e9:	0f b6 00             	movzbl (%rax),%eax
 4ec:	84 c0                	test   %al,%al
 4ee:	0f 85 80 fd ff ff    	jne    274 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 4f4:	90                   	nop
 4f5:	c9                   	leaveq 
 4f6:	c3                   	retq   

00000000000004f7 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 4f7:	55                   	push   %rbp
 4f8:	48 89 e5             	mov    %rsp,%rbp
 4fb:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 4ff:	89 75 f4             	mov    %esi,-0xc(%rbp)
 502:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 505:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 509:	8b 55 f0             	mov    -0x10(%rbp),%edx
 50c:	8b 45 f4             	mov    -0xc(%rbp),%eax
 50f:	48 89 ce             	mov    %rcx,%rsi
 512:	48 89 f7             	mov    %rsi,%rdi
 515:	89 d1                	mov    %edx,%ecx
 517:	fc                   	cld    
 518:	f3 aa                	rep stos %al,%es:(%rdi)
 51a:	89 ca                	mov    %ecx,%edx
 51c:	48 89 fe             	mov    %rdi,%rsi
 51f:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 523:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 526:	90                   	nop
 527:	5d                   	pop    %rbp
 528:	c3                   	retq   

0000000000000529 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 529:	55                   	push   %rbp
 52a:	48 89 e5             	mov    %rsp,%rbp
 52d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 531:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 535:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 539:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 53d:	90                   	nop
 53e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 542:	48 8d 50 01          	lea    0x1(%rax),%rdx
 546:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 54a:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 54e:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 552:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 556:	0f b6 12             	movzbl (%rdx),%edx
 559:	88 10                	mov    %dl,(%rax)
 55b:	0f b6 00             	movzbl (%rax),%eax
 55e:	84 c0                	test   %al,%al
 560:	75 dc                	jne    53e <strcpy+0x15>
    ;
  return os;
 562:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 566:	5d                   	pop    %rbp
 567:	c3                   	retq   

0000000000000568 <strcmp>:

int strcmp(const char *p, const char *q) {
 568:	55                   	push   %rbp
 569:	48 89 e5             	mov    %rsp,%rbp
 56c:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 570:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 574:	eb 0a                	jmp    580 <strcmp+0x18>
    p++, q++;
 576:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 57b:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 580:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 584:	0f b6 00             	movzbl (%rax),%eax
 587:	84 c0                	test   %al,%al
 589:	74 12                	je     59d <strcmp+0x35>
 58b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 58f:	0f b6 10             	movzbl (%rax),%edx
 592:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 596:	0f b6 00             	movzbl (%rax),%eax
 599:	38 c2                	cmp    %al,%dl
 59b:	74 d9                	je     576 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 59d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5a1:	0f b6 00             	movzbl (%rax),%eax
 5a4:	0f b6 d0             	movzbl %al,%edx
 5a7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5ab:	0f b6 00             	movzbl (%rax),%eax
 5ae:	0f b6 c0             	movzbl %al,%eax
 5b1:	29 c2                	sub    %eax,%edx
 5b3:	89 d0                	mov    %edx,%eax
}
 5b5:	5d                   	pop    %rbp
 5b6:	c3                   	retq   

00000000000005b7 <strlen>:

uint strlen(char *s) {
 5b7:	55                   	push   %rbp
 5b8:	48 89 e5             	mov    %rsp,%rbp
 5bb:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 5bf:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 5c6:	eb 04                	jmp    5cc <strlen+0x15>
 5c8:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 5cc:	8b 45 fc             	mov    -0x4(%rbp),%eax
 5cf:	48 63 d0             	movslq %eax,%rdx
 5d2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5d6:	48 01 d0             	add    %rdx,%rax
 5d9:	0f b6 00             	movzbl (%rax),%eax
 5dc:	84 c0                	test   %al,%al
 5de:	75 e8                	jne    5c8 <strlen+0x11>
    ;
  return n;
 5e0:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 5e3:	5d                   	pop    %rbp
 5e4:	c3                   	retq   

00000000000005e5 <memset>:

void *memset(void *dst, int c, uint n) {
 5e5:	55                   	push   %rbp
 5e6:	48 89 e5             	mov    %rsp,%rbp
 5e9:	48 83 ec 10          	sub    $0x10,%rsp
 5ed:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5f1:	89 75 f4             	mov    %esi,-0xc(%rbp)
 5f4:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 5f7:	8b 55 f0             	mov    -0x10(%rbp),%edx
 5fa:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 5fd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 601:	89 ce                	mov    %ecx,%esi
 603:	48 89 c7             	mov    %rax,%rdi
 606:	e8 ec fe ff ff       	callq  4f7 <stosb>
  return dst;
 60b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 60f:	c9                   	leaveq 
 610:	c3                   	retq   

0000000000000611 <strchr>:

char *strchr(const char *s, char c) {
 611:	55                   	push   %rbp
 612:	48 89 e5             	mov    %rsp,%rbp
 615:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 619:	89 f0                	mov    %esi,%eax
 61b:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 61e:	eb 17                	jmp    637 <strchr+0x26>
    if (*s == c)
 620:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 624:	0f b6 00             	movzbl (%rax),%eax
 627:	3a 45 f4             	cmp    -0xc(%rbp),%al
 62a:	75 06                	jne    632 <strchr+0x21>
      return (char *)s;
 62c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 630:	eb 15                	jmp    647 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 632:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 637:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 63b:	0f b6 00             	movzbl (%rax),%eax
 63e:	84 c0                	test   %al,%al
 640:	75 de                	jne    620 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 642:	b8 00 00 00 00       	mov    $0x0,%eax
}
 647:	5d                   	pop    %rbp
 648:	c3                   	retq   

0000000000000649 <gets>:

char *gets(char *buf, int max) {
 649:	55                   	push   %rbp
 64a:	48 89 e5             	mov    %rsp,%rbp
 64d:	48 83 ec 20          	sub    $0x20,%rsp
 651:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 655:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 658:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 65f:	eb 48                	jmp    6a9 <gets+0x60>
    cc = read(0, &c, 1);
 661:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 665:	ba 01 00 00 00       	mov    $0x1,%edx
 66a:	48 89 c6             	mov    %rax,%rsi
 66d:	bf 00 00 00 00       	mov    $0x0,%edi
 672:	e8 6f 01 00 00       	callq  7e6 <read>
 677:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 67a:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 67e:	7e 36                	jle    6b6 <gets+0x6d>
      break;
    buf[i++] = c;
 680:	8b 45 fc             	mov    -0x4(%rbp),%eax
 683:	8d 50 01             	lea    0x1(%rax),%edx
 686:	89 55 fc             	mov    %edx,-0x4(%rbp)
 689:	48 63 d0             	movslq %eax,%rdx
 68c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 690:	48 01 c2             	add    %rax,%rdx
 693:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 697:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 699:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 69d:	3c 0a                	cmp    $0xa,%al
 69f:	74 16                	je     6b7 <gets+0x6e>
 6a1:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6a5:	3c 0d                	cmp    $0xd,%al
 6a7:	74 0e                	je     6b7 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6a9:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6ac:	83 c0 01             	add    $0x1,%eax
 6af:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 6b2:	7c ad                	jl     661 <gets+0x18>
 6b4:	eb 01                	jmp    6b7 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 6b6:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 6b7:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6ba:	48 63 d0             	movslq %eax,%rdx
 6bd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6c1:	48 01 d0             	add    %rdx,%rax
 6c4:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 6c7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 6cb:	c9                   	leaveq 
 6cc:	c3                   	retq   

00000000000006cd <stat>:

int stat(char *n, struct stat *st) {
 6cd:	55                   	push   %rbp
 6ce:	48 89 e5             	mov    %rsp,%rbp
 6d1:	48 83 ec 20          	sub    $0x20,%rsp
 6d5:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 6d9:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 6dd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6e1:	be 00 00 00 00       	mov    $0x0,%esi
 6e6:	48 89 c7             	mov    %rax,%rdi
 6e9:	e8 20 01 00 00       	callq  80e <open>
 6ee:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 6f1:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 6f5:	79 07                	jns    6fe <stat+0x31>
    return -1;
 6f7:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 6fc:	eb 21                	jmp    71f <stat+0x52>
  r = fstat(fd, st);
 6fe:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 702:	8b 45 fc             	mov    -0x4(%rbp),%eax
 705:	48 89 d6             	mov    %rdx,%rsi
 708:	89 c7                	mov    %eax,%edi
 70a:	e8 17 01 00 00       	callq  826 <fstat>
 70f:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 712:	8b 45 fc             	mov    -0x4(%rbp),%eax
 715:	89 c7                	mov    %eax,%edi
 717:	e8 da 00 00 00       	callq  7f6 <close>
  return r;
 71c:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 71f:	c9                   	leaveq 
 720:	c3                   	retq   

0000000000000721 <atoi>:

int atoi(const char *s) {
 721:	55                   	push   %rbp
 722:	48 89 e5             	mov    %rsp,%rbp
 725:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 729:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 730:	eb 28                	jmp    75a <atoi+0x39>
    n = n * 10 + *s++ - '0';
 732:	8b 55 fc             	mov    -0x4(%rbp),%edx
 735:	89 d0                	mov    %edx,%eax
 737:	c1 e0 02             	shl    $0x2,%eax
 73a:	01 d0                	add    %edx,%eax
 73c:	01 c0                	add    %eax,%eax
 73e:	89 c1                	mov    %eax,%ecx
 740:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 744:	48 8d 50 01          	lea    0x1(%rax),%rdx
 748:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 74c:	0f b6 00             	movzbl (%rax),%eax
 74f:	0f be c0             	movsbl %al,%eax
 752:	01 c8                	add    %ecx,%eax
 754:	83 e8 30             	sub    $0x30,%eax
 757:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 75a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 75e:	0f b6 00             	movzbl (%rax),%eax
 761:	3c 2f                	cmp    $0x2f,%al
 763:	7e 0b                	jle    770 <atoi+0x4f>
 765:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 769:	0f b6 00             	movzbl (%rax),%eax
 76c:	3c 39                	cmp    $0x39,%al
 76e:	7e c2                	jle    732 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 770:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 773:	5d                   	pop    %rbp
 774:	c3                   	retq   

0000000000000775 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 775:	55                   	push   %rbp
 776:	48 89 e5             	mov    %rsp,%rbp
 779:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 77d:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 781:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 784:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 788:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 78c:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 790:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 794:	eb 1d                	jmp    7b3 <memmove+0x3e>
    *dst++ = *src++;
 796:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 79a:	48 8d 50 01          	lea    0x1(%rax),%rdx
 79e:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 7a2:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 7a6:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 7aa:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 7ae:	0f b6 12             	movzbl (%rdx),%edx
 7b1:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 7b3:	8b 45 dc             	mov    -0x24(%rbp),%eax
 7b6:	8d 50 ff             	lea    -0x1(%rax),%edx
 7b9:	89 55 dc             	mov    %edx,-0x24(%rbp)
 7bc:	85 c0                	test   %eax,%eax
 7be:	7f d6                	jg     796 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 7c0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7c4:	5d                   	pop    %rbp
 7c5:	c3                   	retq   

00000000000007c6 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 7c6:	b8 01 00 00 00       	mov    $0x1,%eax
 7cb:	cd 40                	int    $0x40
 7cd:	c3                   	retq   

00000000000007ce <exit>:
SYSCALL(exit)
 7ce:	b8 02 00 00 00       	mov    $0x2,%eax
 7d3:	cd 40                	int    $0x40
 7d5:	c3                   	retq   

00000000000007d6 <wait>:
SYSCALL(wait)
 7d6:	b8 03 00 00 00       	mov    $0x3,%eax
 7db:	cd 40                	int    $0x40
 7dd:	c3                   	retq   

00000000000007de <pipe>:
SYSCALL(pipe)
 7de:	b8 04 00 00 00       	mov    $0x4,%eax
 7e3:	cd 40                	int    $0x40
 7e5:	c3                   	retq   

00000000000007e6 <read>:
SYSCALL(read)
 7e6:	b8 05 00 00 00       	mov    $0x5,%eax
 7eb:	cd 40                	int    $0x40
 7ed:	c3                   	retq   

00000000000007ee <write>:
SYSCALL(write)
 7ee:	b8 10 00 00 00       	mov    $0x10,%eax
 7f3:	cd 40                	int    $0x40
 7f5:	c3                   	retq   

00000000000007f6 <close>:
SYSCALL(close)
 7f6:	b8 15 00 00 00       	mov    $0x15,%eax
 7fb:	cd 40                	int    $0x40
 7fd:	c3                   	retq   

00000000000007fe <kill>:
SYSCALL(kill)
 7fe:	b8 06 00 00 00       	mov    $0x6,%eax
 803:	cd 40                	int    $0x40
 805:	c3                   	retq   

0000000000000806 <exec>:
SYSCALL(exec)
 806:	b8 07 00 00 00       	mov    $0x7,%eax
 80b:	cd 40                	int    $0x40
 80d:	c3                   	retq   

000000000000080e <open>:
SYSCALL(open)
 80e:	b8 0f 00 00 00       	mov    $0xf,%eax
 813:	cd 40                	int    $0x40
 815:	c3                   	retq   

0000000000000816 <mknod>:
SYSCALL(mknod)
 816:	b8 11 00 00 00       	mov    $0x11,%eax
 81b:	cd 40                	int    $0x40
 81d:	c3                   	retq   

000000000000081e <unlink>:
SYSCALL(unlink)
 81e:	b8 12 00 00 00       	mov    $0x12,%eax
 823:	cd 40                	int    $0x40
 825:	c3                   	retq   

0000000000000826 <fstat>:
SYSCALL(fstat)
 826:	b8 08 00 00 00       	mov    $0x8,%eax
 82b:	cd 40                	int    $0x40
 82d:	c3                   	retq   

000000000000082e <link>:
SYSCALL(link)
 82e:	b8 13 00 00 00       	mov    $0x13,%eax
 833:	cd 40                	int    $0x40
 835:	c3                   	retq   

0000000000000836 <mkdir>:
SYSCALL(mkdir)
 836:	b8 14 00 00 00       	mov    $0x14,%eax
 83b:	cd 40                	int    $0x40
 83d:	c3                   	retq   

000000000000083e <chdir>:
SYSCALL(chdir)
 83e:	b8 09 00 00 00       	mov    $0x9,%eax
 843:	cd 40                	int    $0x40
 845:	c3                   	retq   

0000000000000846 <dup>:
SYSCALL(dup)
 846:	b8 0a 00 00 00       	mov    $0xa,%eax
 84b:	cd 40                	int    $0x40
 84d:	c3                   	retq   

000000000000084e <getpid>:
SYSCALL(getpid)
 84e:	b8 0b 00 00 00       	mov    $0xb,%eax
 853:	cd 40                	int    $0x40
 855:	c3                   	retq   

0000000000000856 <sbrk>:
SYSCALL(sbrk)
 856:	b8 0c 00 00 00       	mov    $0xc,%eax
 85b:	cd 40                	int    $0x40
 85d:	c3                   	retq   

000000000000085e <sleep>:
SYSCALL(sleep)
 85e:	b8 0d 00 00 00       	mov    $0xd,%eax
 863:	cd 40                	int    $0x40
 865:	c3                   	retq   

0000000000000866 <uptime>:
SYSCALL(uptime)
 866:	b8 0e 00 00 00       	mov    $0xe,%eax
 86b:	cd 40                	int    $0x40
 86d:	c3                   	retq   

000000000000086e <sysinfo>:
SYSCALL(sysinfo)
 86e:	b8 16 00 00 00       	mov    $0x16,%eax
 873:	cd 40                	int    $0x40
 875:	c3                   	retq   

0000000000000876 <crashn>:
SYSCALL(crashn)
 876:	b8 17 00 00 00       	mov    $0x17,%eax
 87b:	cd 40                	int    $0x40
 87d:	c3                   	retq   

000000000000087e <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 87e:	55                   	push   %rbp
 87f:	48 89 e5             	mov    %rsp,%rbp
 882:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 886:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 88a:	48 83 e8 10          	sub    $0x10,%rax
 88e:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 892:	48 8b 05 37 05 00 00 	mov    0x537(%rip),%rax        # dd0 <freep>
 899:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 89d:	eb 2f                	jmp    8ce <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 89f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8a3:	48 8b 00             	mov    (%rax),%rax
 8a6:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8aa:	77 17                	ja     8c3 <free+0x45>
 8ac:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8b0:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8b4:	77 2f                	ja     8e5 <free+0x67>
 8b6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8ba:	48 8b 00             	mov    (%rax),%rax
 8bd:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8c1:	77 22                	ja     8e5 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8c3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8c7:	48 8b 00             	mov    (%rax),%rax
 8ca:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8ce:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8d2:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8d6:	76 c7                	jbe    89f <free+0x21>
 8d8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8dc:	48 8b 00             	mov    (%rax),%rax
 8df:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8e3:	76 ba                	jbe    89f <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 8e5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8e9:	8b 40 08             	mov    0x8(%rax),%eax
 8ec:	89 c0                	mov    %eax,%eax
 8ee:	48 c1 e0 04          	shl    $0x4,%rax
 8f2:	48 89 c2             	mov    %rax,%rdx
 8f5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8f9:	48 01 c2             	add    %rax,%rdx
 8fc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 900:	48 8b 00             	mov    (%rax),%rax
 903:	48 39 c2             	cmp    %rax,%rdx
 906:	75 2d                	jne    935 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 908:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 90c:	8b 50 08             	mov    0x8(%rax),%edx
 90f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 913:	48 8b 00             	mov    (%rax),%rax
 916:	8b 40 08             	mov    0x8(%rax),%eax
 919:	01 c2                	add    %eax,%edx
 91b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 91f:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 922:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 926:	48 8b 00             	mov    (%rax),%rax
 929:	48 8b 10             	mov    (%rax),%rdx
 92c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 930:	48 89 10             	mov    %rdx,(%rax)
 933:	eb 0e                	jmp    943 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 935:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 939:	48 8b 10             	mov    (%rax),%rdx
 93c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 940:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 943:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 947:	8b 40 08             	mov    0x8(%rax),%eax
 94a:	89 c0                	mov    %eax,%eax
 94c:	48 c1 e0 04          	shl    $0x4,%rax
 950:	48 89 c2             	mov    %rax,%rdx
 953:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 957:	48 01 d0             	add    %rdx,%rax
 95a:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 95e:	75 27                	jne    987 <free+0x109>
    p->s.size += bp->s.size;
 960:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 964:	8b 50 08             	mov    0x8(%rax),%edx
 967:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 96b:	8b 40 08             	mov    0x8(%rax),%eax
 96e:	01 c2                	add    %eax,%edx
 970:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 974:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 977:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 97b:	48 8b 10             	mov    (%rax),%rdx
 97e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 982:	48 89 10             	mov    %rdx,(%rax)
 985:	eb 0b                	jmp    992 <free+0x114>
  } else
    p->s.ptr = bp;
 987:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 98b:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 98f:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 992:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 996:	48 89 05 33 04 00 00 	mov    %rax,0x433(%rip)        # dd0 <freep>
}
 99d:	90                   	nop
 99e:	5d                   	pop    %rbp
 99f:	c3                   	retq   

00000000000009a0 <morecore>:

static Header *morecore(uint nu) {
 9a0:	55                   	push   %rbp
 9a1:	48 89 e5             	mov    %rsp,%rbp
 9a4:	48 83 ec 20          	sub    $0x20,%rsp
 9a8:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 9ab:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 9b2:	77 07                	ja     9bb <morecore+0x1b>
    nu = 4096;
 9b4:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 9bb:	8b 45 ec             	mov    -0x14(%rbp),%eax
 9be:	c1 e0 04             	shl    $0x4,%eax
 9c1:	89 c7                	mov    %eax,%edi
 9c3:	e8 8e fe ff ff       	callq  856 <sbrk>
 9c8:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 9cc:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 9d1:	75 07                	jne    9da <morecore+0x3a>
    return 0;
 9d3:	b8 00 00 00 00       	mov    $0x0,%eax
 9d8:	eb 29                	jmp    a03 <morecore+0x63>
  hp = (Header *)p;
 9da:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9de:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 9e2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9e6:	8b 55 ec             	mov    -0x14(%rbp),%edx
 9e9:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 9ec:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9f0:	48 83 c0 10          	add    $0x10,%rax
 9f4:	48 89 c7             	mov    %rax,%rdi
 9f7:	e8 82 fe ff ff       	callq  87e <free>
  return freep;
 9fc:	48 8b 05 cd 03 00 00 	mov    0x3cd(%rip),%rax        # dd0 <freep>
}
 a03:	c9                   	leaveq 
 a04:	c3                   	retq   

0000000000000a05 <malloc>:

void *malloc(uint nbytes) {
 a05:	55                   	push   %rbp
 a06:	48 89 e5             	mov    %rsp,%rbp
 a09:	48 83 ec 30          	sub    $0x30,%rsp
 a0d:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a10:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a13:	48 83 c0 0f          	add    $0xf,%rax
 a17:	48 c1 e8 04          	shr    $0x4,%rax
 a1b:	83 c0 01             	add    $0x1,%eax
 a1e:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a21:	48 8b 05 a8 03 00 00 	mov    0x3a8(%rip),%rax        # dd0 <freep>
 a28:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 a2c:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 a31:	75 2b                	jne    a5e <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 a33:	48 c7 45 f0 c0 0d 00 	movq   $0xdc0,-0x10(%rbp)
 a3a:	00 
 a3b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a3f:	48 89 05 8a 03 00 00 	mov    %rax,0x38a(%rip)        # dd0 <freep>
 a46:	48 8b 05 83 03 00 00 	mov    0x383(%rip),%rax        # dd0 <freep>
 a4d:	48 89 05 6c 03 00 00 	mov    %rax,0x36c(%rip)        # dc0 <base>
    base.s.size = 0;
 a54:	c7 05 6a 03 00 00 00 	movl   $0x0,0x36a(%rip)        # dc8 <base+0x8>
 a5b:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a5e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a62:	48 8b 00             	mov    (%rax),%rax
 a65:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a69:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a6d:	8b 40 08             	mov    0x8(%rax),%eax
 a70:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a73:	72 5f                	jb     ad4 <malloc+0xcf>
      if (p->s.size == nunits)
 a75:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a79:	8b 40 08             	mov    0x8(%rax),%eax
 a7c:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a7f:	75 10                	jne    a91 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 a81:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a85:	48 8b 10             	mov    (%rax),%rdx
 a88:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a8c:	48 89 10             	mov    %rdx,(%rax)
 a8f:	eb 2e                	jmp    abf <malloc+0xba>
      else {
        p->s.size -= nunits;
 a91:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a95:	8b 40 08             	mov    0x8(%rax),%eax
 a98:	2b 45 ec             	sub    -0x14(%rbp),%eax
 a9b:	89 c2                	mov    %eax,%edx
 a9d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa1:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 aa4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa8:	8b 40 08             	mov    0x8(%rax),%eax
 aab:	89 c0                	mov    %eax,%eax
 aad:	48 c1 e0 04          	shl    $0x4,%rax
 ab1:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 ab5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ab9:	8b 55 ec             	mov    -0x14(%rbp),%edx
 abc:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 abf:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ac3:	48 89 05 06 03 00 00 	mov    %rax,0x306(%rip)        # dd0 <freep>
      return (void *)(p + 1);
 aca:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ace:	48 83 c0 10          	add    $0x10,%rax
 ad2:	eb 41                	jmp    b15 <malloc+0x110>
    }
    if (p == freep)
 ad4:	48 8b 05 f5 02 00 00 	mov    0x2f5(%rip),%rax        # dd0 <freep>
 adb:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 adf:	75 1c                	jne    afd <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 ae1:	8b 45 ec             	mov    -0x14(%rbp),%eax
 ae4:	89 c7                	mov    %eax,%edi
 ae6:	e8 b5 fe ff ff       	callq  9a0 <morecore>
 aeb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 aef:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 af4:	75 07                	jne    afd <malloc+0xf8>
        return 0;
 af6:	b8 00 00 00 00       	mov    $0x0,%eax
 afb:	eb 18                	jmp    b15 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 afd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b01:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b05:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b09:	48 8b 00             	mov    (%rax),%rax
 b0c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b10:	e9 54 ff ff ff       	jmpq   a69 <malloc+0x64>
 b15:	c9                   	leaveq 
 b16:	c3                   	retq   
