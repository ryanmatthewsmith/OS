
out/user/_ln:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <cdefs.h>
#include <stat.h>
#include <user.h>

int main(int argc, char *argv[]) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 10          	sub    $0x10,%rsp
   8:	89 7d fc             	mov    %edi,-0x4(%rbp)
   b:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  if (argc != 3) {
   f:	83 7d fc 03          	cmpl   $0x3,-0x4(%rbp)
  13:	74 19                	je     2e <main+0x2e>
    printf(2, "Usage: ln old new\n");
  15:	be 2e 0b 00 00       	mov    $0xb2e,%esi
  1a:	bf 02 00 00 00       	mov    $0x2,%edi
  1f:	b8 00 00 00 00       	mov    $0x0,%eax
  24:	e8 19 02 00 00       	callq  242 <printf>
    exit();
  29:	e8 b7 07 00 00       	callq  7e5 <exit>
  }
  if (link(argv[1], argv[2]) < 0)
  2e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  32:	48 83 c0 10          	add    $0x10,%rax
  36:	48 8b 10             	mov    (%rax),%rdx
  39:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  3d:	48 83 c0 08          	add    $0x8,%rax
  41:	48 8b 00             	mov    (%rax),%rax
  44:	48 89 d6             	mov    %rdx,%rsi
  47:	48 89 c7             	mov    %rax,%rdi
  4a:	e8 f6 07 00 00       	callq  845 <link>
  4f:	85 c0                	test   %eax,%eax
  51:	79 30                	jns    83 <main+0x83>
    printf(2, "link %s %s: failed\n", argv[1], argv[2]);
  53:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  57:	48 83 c0 10          	add    $0x10,%rax
  5b:	48 8b 10             	mov    (%rax),%rdx
  5e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
  62:	48 83 c0 08          	add    $0x8,%rax
  66:	48 8b 00             	mov    (%rax),%rax
  69:	48 89 d1             	mov    %rdx,%rcx
  6c:	48 89 c2             	mov    %rax,%rdx
  6f:	be 41 0b 00 00       	mov    $0xb41,%esi
  74:	bf 02 00 00 00       	mov    $0x2,%edi
  79:	b8 00 00 00 00       	mov    $0x0,%eax
  7e:	e8 bf 01 00 00       	callq  242 <printf>
  exit();
  83:	e8 5d 07 00 00       	callq  7e5 <exit>

0000000000000088 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  88:	55                   	push   %rbp
  89:	48 89 e5             	mov    %rsp,%rbp
  8c:	48 83 ec 10          	sub    $0x10,%rsp
  90:	89 7d fc             	mov    %edi,-0x4(%rbp)
  93:	89 f0                	mov    %esi,%eax
  95:	88 45 f8             	mov    %al,-0x8(%rbp)
  98:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
  9c:	8b 45 fc             	mov    -0x4(%rbp),%eax
  9f:	ba 01 00 00 00       	mov    $0x1,%edx
  a4:	48 89 ce             	mov    %rcx,%rsi
  a7:	89 c7                	mov    %eax,%edi
  a9:	e8 57 07 00 00       	callq  805 <write>
  ae:	90                   	nop
  af:	c9                   	leaveq 
  b0:	c3                   	retq   

00000000000000b1 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
  b1:	55                   	push   %rbp
  b2:	48 89 e5             	mov    %rsp,%rbp
  b5:	48 83 ec 40          	sub    $0x40,%rsp
  b9:	89 7d cc             	mov    %edi,-0x34(%rbp)
  bc:	89 75 c8             	mov    %esi,-0x38(%rbp)
  bf:	89 55 c4             	mov    %edx,-0x3c(%rbp)
  c2:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
  c5:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  c9:	74 1f                	je     ea <printint64+0x39>
  cb:	8b 45 c8             	mov    -0x38(%rbp),%eax
  ce:	c1 e8 1f             	shr    $0x1f,%eax
  d1:	0f b6 c0             	movzbl %al,%eax
  d4:	89 45 c0             	mov    %eax,-0x40(%rbp)
  d7:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
  db:	74 0d                	je     ea <printint64+0x39>
    x = -xx;
  dd:	8b 45 c8             	mov    -0x38(%rbp),%eax
  e0:	f7 d8                	neg    %eax
  e2:	48 98                	cltq   
  e4:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  e8:	eb 09                	jmp    f3 <printint64+0x42>
  else
    x = xx;
  ea:	8b 45 c8             	mov    -0x38(%rbp),%eax
  ed:	48 98                	cltq   
  ef:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
  f3:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
  fa:	8b 4d fc             	mov    -0x4(%rbp),%ecx
  fd:	8d 41 01             	lea    0x1(%rcx),%eax
 100:	89 45 fc             	mov    %eax,-0x4(%rbp)
 103:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 106:	48 63 f0             	movslq %eax,%rsi
 109:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 10d:	ba 00 00 00 00       	mov    $0x0,%edx
 112:	48 f7 f6             	div    %rsi
 115:	48 89 d0             	mov    %rdx,%rax
 118:	0f b6 90 c0 0d 00 00 	movzbl 0xdc0(%rax),%edx
 11f:	48 63 c1             	movslq %ecx,%rax
 122:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 126:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 129:	48 63 f8             	movslq %eax,%rdi
 12c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 130:	ba 00 00 00 00       	mov    $0x0,%edx
 135:	48 f7 f7             	div    %rdi
 138:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 13c:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 141:	75 b7                	jne    fa <printint64+0x49>

  if (sgn)
 143:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 147:	74 2b                	je     174 <printint64+0xc3>
    buf[i++] = '-';
 149:	8b 45 fc             	mov    -0x4(%rbp),%eax
 14c:	8d 50 01             	lea    0x1(%rax),%edx
 14f:	89 55 fc             	mov    %edx,-0x4(%rbp)
 152:	48 98                	cltq   
 154:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 159:	eb 19                	jmp    174 <printint64+0xc3>
    putc(fd, buf[i]);
 15b:	8b 45 fc             	mov    -0x4(%rbp),%eax
 15e:	48 98                	cltq   
 160:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 165:	0f be d0             	movsbl %al,%edx
 168:	8b 45 cc             	mov    -0x34(%rbp),%eax
 16b:	89 d6                	mov    %edx,%esi
 16d:	89 c7                	mov    %eax,%edi
 16f:	e8 14 ff ff ff       	callq  88 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 174:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 178:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 17c:	79 dd                	jns    15b <printint64+0xaa>
    putc(fd, buf[i]);
}
 17e:	90                   	nop
 17f:	c9                   	leaveq 
 180:	c3                   	retq   

0000000000000181 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 181:	55                   	push   %rbp
 182:	48 89 e5             	mov    %rsp,%rbp
 185:	48 83 ec 30          	sub    $0x30,%rsp
 189:	89 7d dc             	mov    %edi,-0x24(%rbp)
 18c:	89 75 d8             	mov    %esi,-0x28(%rbp)
 18f:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 192:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 195:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 19c:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 1a0:	74 17                	je     1b9 <printint+0x38>
 1a2:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 1a6:	79 11                	jns    1b9 <printint+0x38>
    neg = 1;
 1a8:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 1af:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1b2:	f7 d8                	neg    %eax
 1b4:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1b7:	eb 06                	jmp    1bf <printint+0x3e>
  } else {
    x = xx;
 1b9:	8b 45 d8             	mov    -0x28(%rbp),%eax
 1bc:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 1bf:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1c6:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1c9:	8d 41 01             	lea    0x1(%rcx),%eax
 1cc:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1cf:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 1d2:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1d5:	ba 00 00 00 00       	mov    $0x0,%edx
 1da:	f7 f6                	div    %esi
 1dc:	89 d0                	mov    %edx,%eax
 1de:	89 c0                	mov    %eax,%eax
 1e0:	0f b6 90 e0 0d 00 00 	movzbl 0xde0(%rax),%edx
 1e7:	48 63 c1             	movslq %ecx,%rax
 1ea:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 1ee:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 1f1:	8b 45 f4             	mov    -0xc(%rbp),%eax
 1f4:	ba 00 00 00 00       	mov    $0x0,%edx
 1f9:	f7 f7                	div    %edi
 1fb:	89 45 f4             	mov    %eax,-0xc(%rbp)
 1fe:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 202:	75 c2                	jne    1c6 <printint+0x45>
  if (neg)
 204:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 208:	74 2b                	je     235 <printint+0xb4>
    buf[i++] = '-';
 20a:	8b 45 fc             	mov    -0x4(%rbp),%eax
 20d:	8d 50 01             	lea    0x1(%rax),%edx
 210:	89 55 fc             	mov    %edx,-0x4(%rbp)
 213:	48 98                	cltq   
 215:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 21a:	eb 19                	jmp    235 <printint+0xb4>
    putc(fd, buf[i]);
 21c:	8b 45 fc             	mov    -0x4(%rbp),%eax
 21f:	48 98                	cltq   
 221:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 226:	0f be d0             	movsbl %al,%edx
 229:	8b 45 dc             	mov    -0x24(%rbp),%eax
 22c:	89 d6                	mov    %edx,%esi
 22e:	89 c7                	mov    %eax,%edi
 230:	e8 53 fe ff ff       	callq  88 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 235:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 239:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 23d:	79 dd                	jns    21c <printint+0x9b>
    putc(fd, buf[i]);
}
 23f:	90                   	nop
 240:	c9                   	leaveq 
 241:	c3                   	retq   

0000000000000242 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 242:	55                   	push   %rbp
 243:	48 89 e5             	mov    %rsp,%rbp
 246:	48 83 ec 70          	sub    $0x70,%rsp
 24a:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 24d:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 251:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 255:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 259:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 25d:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 261:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 268:	48 8d 45 10          	lea    0x10(%rbp),%rax
 26c:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 270:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 274:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 278:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 27f:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 286:	e9 68 02 00 00       	jmpq   4f3 <printf+0x2b1>
    c = fmt[i] & 0xff;
 28b:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 28e:	48 63 d0             	movslq %eax,%rdx
 291:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 295:	48 01 d0             	add    %rdx,%rax
 298:	0f b6 00             	movzbl (%rax),%eax
 29b:	0f be c0             	movsbl %al,%eax
 29e:	25 ff 00 00 00       	and    $0xff,%eax
 2a3:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 2a6:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 2aa:	75 30                	jne    2dc <printf+0x9a>
      if (c == '%') {
 2ac:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 2b0:	75 13                	jne    2c5 <printf+0x83>
        state = '%';
 2b2:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 2b9:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 2c0:	e9 2a 02 00 00       	jmpq   4ef <printf+0x2ad>
      } else {
        putc(fd, c);
 2c5:	8b 45 b8             	mov    -0x48(%rbp),%eax
 2c8:	0f be d0             	movsbl %al,%edx
 2cb:	8b 45 9c             	mov    -0x64(%rbp),%eax
 2ce:	89 d6                	mov    %edx,%esi
 2d0:	89 c7                	mov    %eax,%edi
 2d2:	e8 b1 fd ff ff       	callq  88 <putc>
 2d7:	e9 13 02 00 00       	jmpq   4ef <printf+0x2ad>
      }
    } else if (state == '%') {
 2dc:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 2e0:	0f 85 09 02 00 00    	jne    4ef <printf+0x2ad>
      if (c == 'l') {
 2e6:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 2ea:	75 0c                	jne    2f8 <printf+0xb6>
        lflag = 1;
 2ec:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 2f3:	e9 f7 01 00 00       	jmpq   4ef <printf+0x2ad>
      } else if (c == 'd') {
 2f8:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 2fc:	0f 85 95 00 00 00    	jne    397 <printf+0x155>
        if (lflag == 1)
 302:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 306:	75 49                	jne    351 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 308:	8b 45 a0             	mov    -0x60(%rbp),%eax
 30b:	83 f8 30             	cmp    $0x30,%eax
 30e:	73 17                	jae    327 <printf+0xe5>
 310:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 314:	8b 55 a0             	mov    -0x60(%rbp),%edx
 317:	89 d2                	mov    %edx,%edx
 319:	48 01 d0             	add    %rdx,%rax
 31c:	8b 55 a0             	mov    -0x60(%rbp),%edx
 31f:	83 c2 08             	add    $0x8,%edx
 322:	89 55 a0             	mov    %edx,-0x60(%rbp)
 325:	eb 0c                	jmp    333 <printf+0xf1>
 327:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 32b:	48 8d 50 08          	lea    0x8(%rax),%rdx
 32f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 333:	48 8b 00             	mov    (%rax),%rax
 336:	89 c6                	mov    %eax,%esi
 338:	8b 45 9c             	mov    -0x64(%rbp),%eax
 33b:	b9 01 00 00 00       	mov    $0x1,%ecx
 340:	ba 0a 00 00 00       	mov    $0xa,%edx
 345:	89 c7                	mov    %eax,%edi
 347:	e8 65 fd ff ff       	callq  b1 <printint64>
 34c:	e9 97 01 00 00       	jmpq   4e8 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 351:	8b 45 a0             	mov    -0x60(%rbp),%eax
 354:	83 f8 30             	cmp    $0x30,%eax
 357:	73 17                	jae    370 <printf+0x12e>
 359:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 35d:	8b 55 a0             	mov    -0x60(%rbp),%edx
 360:	89 d2                	mov    %edx,%edx
 362:	48 01 d0             	add    %rdx,%rax
 365:	8b 55 a0             	mov    -0x60(%rbp),%edx
 368:	83 c2 08             	add    $0x8,%edx
 36b:	89 55 a0             	mov    %edx,-0x60(%rbp)
 36e:	eb 0c                	jmp    37c <printf+0x13a>
 370:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 374:	48 8d 50 08          	lea    0x8(%rax),%rdx
 378:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 37c:	8b 30                	mov    (%rax),%esi
 37e:	8b 45 9c             	mov    -0x64(%rbp),%eax
 381:	b9 01 00 00 00       	mov    $0x1,%ecx
 386:	ba 0a 00 00 00       	mov    $0xa,%edx
 38b:	89 c7                	mov    %eax,%edi
 38d:	e8 ef fd ff ff       	callq  181 <printint>
 392:	e9 51 01 00 00       	jmpq   4e8 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 397:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 39b:	74 0a                	je     3a7 <printf+0x165>
 39d:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 3a1:	0f 85 95 00 00 00    	jne    43c <printf+0x1fa>
        if (lflag == 1)
 3a7:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 3ab:	75 49                	jne    3f6 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 3ad:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3b0:	83 f8 30             	cmp    $0x30,%eax
 3b3:	73 17                	jae    3cc <printf+0x18a>
 3b5:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3b9:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3bc:	89 d2                	mov    %edx,%edx
 3be:	48 01 d0             	add    %rdx,%rax
 3c1:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3c4:	83 c2 08             	add    $0x8,%edx
 3c7:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3ca:	eb 0c                	jmp    3d8 <printf+0x196>
 3cc:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3d0:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3d4:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3d8:	48 8b 00             	mov    (%rax),%rax
 3db:	89 c6                	mov    %eax,%esi
 3dd:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3e0:	b9 00 00 00 00       	mov    $0x0,%ecx
 3e5:	ba 10 00 00 00       	mov    $0x10,%edx
 3ea:	89 c7                	mov    %eax,%edi
 3ec:	e8 c0 fc ff ff       	callq  b1 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 3f1:	e9 f2 00 00 00       	jmpq   4e8 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 3f6:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3f9:	83 f8 30             	cmp    $0x30,%eax
 3fc:	73 17                	jae    415 <printf+0x1d3>
 3fe:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 402:	8b 55 a0             	mov    -0x60(%rbp),%edx
 405:	89 d2                	mov    %edx,%edx
 407:	48 01 d0             	add    %rdx,%rax
 40a:	8b 55 a0             	mov    -0x60(%rbp),%edx
 40d:	83 c2 08             	add    $0x8,%edx
 410:	89 55 a0             	mov    %edx,-0x60(%rbp)
 413:	eb 0c                	jmp    421 <printf+0x1df>
 415:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 419:	48 8d 50 08          	lea    0x8(%rax),%rdx
 41d:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 421:	8b 30                	mov    (%rax),%esi
 423:	8b 45 9c             	mov    -0x64(%rbp),%eax
 426:	b9 00 00 00 00       	mov    $0x0,%ecx
 42b:	ba 10 00 00 00       	mov    $0x10,%edx
 430:	89 c7                	mov    %eax,%edi
 432:	e8 4a fd ff ff       	callq  181 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 437:	e9 ac 00 00 00       	jmpq   4e8 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 43c:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 440:	75 6b                	jne    4ad <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 442:	8b 45 a0             	mov    -0x60(%rbp),%eax
 445:	83 f8 30             	cmp    $0x30,%eax
 448:	73 17                	jae    461 <printf+0x21f>
 44a:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 44e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 451:	89 d2                	mov    %edx,%edx
 453:	48 01 d0             	add    %rdx,%rax
 456:	8b 55 a0             	mov    -0x60(%rbp),%edx
 459:	83 c2 08             	add    $0x8,%edx
 45c:	89 55 a0             	mov    %edx,-0x60(%rbp)
 45f:	eb 0c                	jmp    46d <printf+0x22b>
 461:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 465:	48 8d 50 08          	lea    0x8(%rax),%rdx
 469:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 46d:	48 8b 00             	mov    (%rax),%rax
 470:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 474:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 479:	75 25                	jne    4a0 <printf+0x25e>
          s = "(null)";
 47b:	48 c7 45 c8 55 0b 00 	movq   $0xb55,-0x38(%rbp)
 482:	00 
        for (; *s; s++)
 483:	eb 1b                	jmp    4a0 <printf+0x25e>
          putc(fd, *s);
 485:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 489:	0f b6 00             	movzbl (%rax),%eax
 48c:	0f be d0             	movsbl %al,%edx
 48f:	8b 45 9c             	mov    -0x64(%rbp),%eax
 492:	89 d6                	mov    %edx,%esi
 494:	89 c7                	mov    %eax,%edi
 496:	e8 ed fb ff ff       	callq  88 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 49b:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 4a0:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 4a4:	0f b6 00             	movzbl (%rax),%eax
 4a7:	84 c0                	test   %al,%al
 4a9:	75 da                	jne    485 <printf+0x243>
 4ab:	eb 3b                	jmp    4e8 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 4ad:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 4b1:	75 14                	jne    4c7 <printf+0x285>
        putc(fd, c);
 4b3:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4b6:	0f be d0             	movsbl %al,%edx
 4b9:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4bc:	89 d6                	mov    %edx,%esi
 4be:	89 c7                	mov    %eax,%edi
 4c0:	e8 c3 fb ff ff       	callq  88 <putc>
 4c5:	eb 21                	jmp    4e8 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 4c7:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4ca:	be 25 00 00 00       	mov    $0x25,%esi
 4cf:	89 c7                	mov    %eax,%edi
 4d1:	e8 b2 fb ff ff       	callq  88 <putc>
        putc(fd, c);
 4d6:	8b 45 b8             	mov    -0x48(%rbp),%eax
 4d9:	0f be d0             	movsbl %al,%edx
 4dc:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4df:	89 d6                	mov    %edx,%esi
 4e1:	89 c7                	mov    %eax,%edi
 4e3:	e8 a0 fb ff ff       	callq  88 <putc>
      }
      state = 0;
 4e8:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 4ef:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 4f3:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 4f6:	48 63 d0             	movslq %eax,%rdx
 4f9:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 4fd:	48 01 d0             	add    %rdx,%rax
 500:	0f b6 00             	movzbl (%rax),%eax
 503:	84 c0                	test   %al,%al
 505:	0f 85 80 fd ff ff    	jne    28b <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 50b:	90                   	nop
 50c:	c9                   	leaveq 
 50d:	c3                   	retq   

000000000000050e <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 50e:	55                   	push   %rbp
 50f:	48 89 e5             	mov    %rsp,%rbp
 512:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 516:	89 75 f4             	mov    %esi,-0xc(%rbp)
 519:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 51c:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 520:	8b 55 f0             	mov    -0x10(%rbp),%edx
 523:	8b 45 f4             	mov    -0xc(%rbp),%eax
 526:	48 89 ce             	mov    %rcx,%rsi
 529:	48 89 f7             	mov    %rsi,%rdi
 52c:	89 d1                	mov    %edx,%ecx
 52e:	fc                   	cld    
 52f:	f3 aa                	rep stos %al,%es:(%rdi)
 531:	89 ca                	mov    %ecx,%edx
 533:	48 89 fe             	mov    %rdi,%rsi
 536:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 53a:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 53d:	90                   	nop
 53e:	5d                   	pop    %rbp
 53f:	c3                   	retq   

0000000000000540 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 540:	55                   	push   %rbp
 541:	48 89 e5             	mov    %rsp,%rbp
 544:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 548:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 54c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 550:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 554:	90                   	nop
 555:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 559:	48 8d 50 01          	lea    0x1(%rax),%rdx
 55d:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 561:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 565:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 569:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 56d:	0f b6 12             	movzbl (%rdx),%edx
 570:	88 10                	mov    %dl,(%rax)
 572:	0f b6 00             	movzbl (%rax),%eax
 575:	84 c0                	test   %al,%al
 577:	75 dc                	jne    555 <strcpy+0x15>
    ;
  return os;
 579:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 57d:	5d                   	pop    %rbp
 57e:	c3                   	retq   

000000000000057f <strcmp>:

int strcmp(const char *p, const char *q) {
 57f:	55                   	push   %rbp
 580:	48 89 e5             	mov    %rsp,%rbp
 583:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 587:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 58b:	eb 0a                	jmp    597 <strcmp+0x18>
    p++, q++;
 58d:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 592:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 597:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 59b:	0f b6 00             	movzbl (%rax),%eax
 59e:	84 c0                	test   %al,%al
 5a0:	74 12                	je     5b4 <strcmp+0x35>
 5a2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5a6:	0f b6 10             	movzbl (%rax),%edx
 5a9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5ad:	0f b6 00             	movzbl (%rax),%eax
 5b0:	38 c2                	cmp    %al,%dl
 5b2:	74 d9                	je     58d <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 5b4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 5b8:	0f b6 00             	movzbl (%rax),%eax
 5bb:	0f b6 d0             	movzbl %al,%edx
 5be:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 5c2:	0f b6 00             	movzbl (%rax),%eax
 5c5:	0f b6 c0             	movzbl %al,%eax
 5c8:	29 c2                	sub    %eax,%edx
 5ca:	89 d0                	mov    %edx,%eax
}
 5cc:	5d                   	pop    %rbp
 5cd:	c3                   	retq   

00000000000005ce <strlen>:

uint strlen(char *s) {
 5ce:	55                   	push   %rbp
 5cf:	48 89 e5             	mov    %rsp,%rbp
 5d2:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 5d6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 5dd:	eb 04                	jmp    5e3 <strlen+0x15>
 5df:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 5e3:	8b 45 fc             	mov    -0x4(%rbp),%eax
 5e6:	48 63 d0             	movslq %eax,%rdx
 5e9:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5ed:	48 01 d0             	add    %rdx,%rax
 5f0:	0f b6 00             	movzbl (%rax),%eax
 5f3:	84 c0                	test   %al,%al
 5f5:	75 e8                	jne    5df <strlen+0x11>
    ;
  return n;
 5f7:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 5fa:	5d                   	pop    %rbp
 5fb:	c3                   	retq   

00000000000005fc <memset>:

void *memset(void *dst, int c, uint n) {
 5fc:	55                   	push   %rbp
 5fd:	48 89 e5             	mov    %rsp,%rbp
 600:	48 83 ec 10          	sub    $0x10,%rsp
 604:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 608:	89 75 f4             	mov    %esi,-0xc(%rbp)
 60b:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 60e:	8b 55 f0             	mov    -0x10(%rbp),%edx
 611:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 614:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 618:	89 ce                	mov    %ecx,%esi
 61a:	48 89 c7             	mov    %rax,%rdi
 61d:	e8 ec fe ff ff       	callq  50e <stosb>
  return dst;
 622:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 626:	c9                   	leaveq 
 627:	c3                   	retq   

0000000000000628 <strchr>:

char *strchr(const char *s, char c) {
 628:	55                   	push   %rbp
 629:	48 89 e5             	mov    %rsp,%rbp
 62c:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 630:	89 f0                	mov    %esi,%eax
 632:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 635:	eb 17                	jmp    64e <strchr+0x26>
    if (*s == c)
 637:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 63b:	0f b6 00             	movzbl (%rax),%eax
 63e:	3a 45 f4             	cmp    -0xc(%rbp),%al
 641:	75 06                	jne    649 <strchr+0x21>
      return (char *)s;
 643:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 647:	eb 15                	jmp    65e <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 649:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 64e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 652:	0f b6 00             	movzbl (%rax),%eax
 655:	84 c0                	test   %al,%al
 657:	75 de                	jne    637 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 659:	b8 00 00 00 00       	mov    $0x0,%eax
}
 65e:	5d                   	pop    %rbp
 65f:	c3                   	retq   

0000000000000660 <gets>:

char *gets(char *buf, int max) {
 660:	55                   	push   %rbp
 661:	48 89 e5             	mov    %rsp,%rbp
 664:	48 83 ec 20          	sub    $0x20,%rsp
 668:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 66c:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 66f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 676:	eb 48                	jmp    6c0 <gets+0x60>
    cc = read(0, &c, 1);
 678:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 67c:	ba 01 00 00 00       	mov    $0x1,%edx
 681:	48 89 c6             	mov    %rax,%rsi
 684:	bf 00 00 00 00       	mov    $0x0,%edi
 689:	e8 6f 01 00 00       	callq  7fd <read>
 68e:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 691:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 695:	7e 36                	jle    6cd <gets+0x6d>
      break;
    buf[i++] = c;
 697:	8b 45 fc             	mov    -0x4(%rbp),%eax
 69a:	8d 50 01             	lea    0x1(%rax),%edx
 69d:	89 55 fc             	mov    %edx,-0x4(%rbp)
 6a0:	48 63 d0             	movslq %eax,%rdx
 6a3:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6a7:	48 01 c2             	add    %rax,%rdx
 6aa:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6ae:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 6b0:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6b4:	3c 0a                	cmp    $0xa,%al
 6b6:	74 16                	je     6ce <gets+0x6e>
 6b8:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 6bc:	3c 0d                	cmp    $0xd,%al
 6be:	74 0e                	je     6ce <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6c0:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6c3:	83 c0 01             	add    $0x1,%eax
 6c6:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 6c9:	7c ad                	jl     678 <gets+0x18>
 6cb:	eb 01                	jmp    6ce <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 6cd:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 6ce:	8b 45 fc             	mov    -0x4(%rbp),%eax
 6d1:	48 63 d0             	movslq %eax,%rdx
 6d4:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6d8:	48 01 d0             	add    %rdx,%rax
 6db:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 6de:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 6e2:	c9                   	leaveq 
 6e3:	c3                   	retq   

00000000000006e4 <stat>:

int stat(char *n, struct stat *st) {
 6e4:	55                   	push   %rbp
 6e5:	48 89 e5             	mov    %rsp,%rbp
 6e8:	48 83 ec 20          	sub    $0x20,%rsp
 6ec:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 6f0:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 6f4:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6f8:	be 00 00 00 00       	mov    $0x0,%esi
 6fd:	48 89 c7             	mov    %rax,%rdi
 700:	e8 20 01 00 00       	callq  825 <open>
 705:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 708:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 70c:	79 07                	jns    715 <stat+0x31>
    return -1;
 70e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 713:	eb 21                	jmp    736 <stat+0x52>
  r = fstat(fd, st);
 715:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 719:	8b 45 fc             	mov    -0x4(%rbp),%eax
 71c:	48 89 d6             	mov    %rdx,%rsi
 71f:	89 c7                	mov    %eax,%edi
 721:	e8 17 01 00 00       	callq  83d <fstat>
 726:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 729:	8b 45 fc             	mov    -0x4(%rbp),%eax
 72c:	89 c7                	mov    %eax,%edi
 72e:	e8 da 00 00 00       	callq  80d <close>
  return r;
 733:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 736:	c9                   	leaveq 
 737:	c3                   	retq   

0000000000000738 <atoi>:

int atoi(const char *s) {
 738:	55                   	push   %rbp
 739:	48 89 e5             	mov    %rsp,%rbp
 73c:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 740:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 747:	eb 28                	jmp    771 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 749:	8b 55 fc             	mov    -0x4(%rbp),%edx
 74c:	89 d0                	mov    %edx,%eax
 74e:	c1 e0 02             	shl    $0x2,%eax
 751:	01 d0                	add    %edx,%eax
 753:	01 c0                	add    %eax,%eax
 755:	89 c1                	mov    %eax,%ecx
 757:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 75b:	48 8d 50 01          	lea    0x1(%rax),%rdx
 75f:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 763:	0f b6 00             	movzbl (%rax),%eax
 766:	0f be c0             	movsbl %al,%eax
 769:	01 c8                	add    %ecx,%eax
 76b:	83 e8 30             	sub    $0x30,%eax
 76e:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 771:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 775:	0f b6 00             	movzbl (%rax),%eax
 778:	3c 2f                	cmp    $0x2f,%al
 77a:	7e 0b                	jle    787 <atoi+0x4f>
 77c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 780:	0f b6 00             	movzbl (%rax),%eax
 783:	3c 39                	cmp    $0x39,%al
 785:	7e c2                	jle    749 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 787:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 78a:	5d                   	pop    %rbp
 78b:	c3                   	retq   

000000000000078c <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 78c:	55                   	push   %rbp
 78d:	48 89 e5             	mov    %rsp,%rbp
 790:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 794:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 798:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 79b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 79f:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 7a3:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 7a7:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 7ab:	eb 1d                	jmp    7ca <memmove+0x3e>
    *dst++ = *src++;
 7ad:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 7b1:	48 8d 50 01          	lea    0x1(%rax),%rdx
 7b5:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 7b9:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 7bd:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 7c1:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 7c5:	0f b6 12             	movzbl (%rdx),%edx
 7c8:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 7ca:	8b 45 dc             	mov    -0x24(%rbp),%eax
 7cd:	8d 50 ff             	lea    -0x1(%rax),%edx
 7d0:	89 55 dc             	mov    %edx,-0x24(%rbp)
 7d3:	85 c0                	test   %eax,%eax
 7d5:	7f d6                	jg     7ad <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 7d7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7db:	5d                   	pop    %rbp
 7dc:	c3                   	retq   

00000000000007dd <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 7dd:	b8 01 00 00 00       	mov    $0x1,%eax
 7e2:	cd 40                	int    $0x40
 7e4:	c3                   	retq   

00000000000007e5 <exit>:
SYSCALL(exit)
 7e5:	b8 02 00 00 00       	mov    $0x2,%eax
 7ea:	cd 40                	int    $0x40
 7ec:	c3                   	retq   

00000000000007ed <wait>:
SYSCALL(wait)
 7ed:	b8 03 00 00 00       	mov    $0x3,%eax
 7f2:	cd 40                	int    $0x40
 7f4:	c3                   	retq   

00000000000007f5 <pipe>:
SYSCALL(pipe)
 7f5:	b8 04 00 00 00       	mov    $0x4,%eax
 7fa:	cd 40                	int    $0x40
 7fc:	c3                   	retq   

00000000000007fd <read>:
SYSCALL(read)
 7fd:	b8 05 00 00 00       	mov    $0x5,%eax
 802:	cd 40                	int    $0x40
 804:	c3                   	retq   

0000000000000805 <write>:
SYSCALL(write)
 805:	b8 10 00 00 00       	mov    $0x10,%eax
 80a:	cd 40                	int    $0x40
 80c:	c3                   	retq   

000000000000080d <close>:
SYSCALL(close)
 80d:	b8 15 00 00 00       	mov    $0x15,%eax
 812:	cd 40                	int    $0x40
 814:	c3                   	retq   

0000000000000815 <kill>:
SYSCALL(kill)
 815:	b8 06 00 00 00       	mov    $0x6,%eax
 81a:	cd 40                	int    $0x40
 81c:	c3                   	retq   

000000000000081d <exec>:
SYSCALL(exec)
 81d:	b8 07 00 00 00       	mov    $0x7,%eax
 822:	cd 40                	int    $0x40
 824:	c3                   	retq   

0000000000000825 <open>:
SYSCALL(open)
 825:	b8 0f 00 00 00       	mov    $0xf,%eax
 82a:	cd 40                	int    $0x40
 82c:	c3                   	retq   

000000000000082d <mknod>:
SYSCALL(mknod)
 82d:	b8 11 00 00 00       	mov    $0x11,%eax
 832:	cd 40                	int    $0x40
 834:	c3                   	retq   

0000000000000835 <unlink>:
SYSCALL(unlink)
 835:	b8 12 00 00 00       	mov    $0x12,%eax
 83a:	cd 40                	int    $0x40
 83c:	c3                   	retq   

000000000000083d <fstat>:
SYSCALL(fstat)
 83d:	b8 08 00 00 00       	mov    $0x8,%eax
 842:	cd 40                	int    $0x40
 844:	c3                   	retq   

0000000000000845 <link>:
SYSCALL(link)
 845:	b8 13 00 00 00       	mov    $0x13,%eax
 84a:	cd 40                	int    $0x40
 84c:	c3                   	retq   

000000000000084d <mkdir>:
SYSCALL(mkdir)
 84d:	b8 14 00 00 00       	mov    $0x14,%eax
 852:	cd 40                	int    $0x40
 854:	c3                   	retq   

0000000000000855 <chdir>:
SYSCALL(chdir)
 855:	b8 09 00 00 00       	mov    $0x9,%eax
 85a:	cd 40                	int    $0x40
 85c:	c3                   	retq   

000000000000085d <dup>:
SYSCALL(dup)
 85d:	b8 0a 00 00 00       	mov    $0xa,%eax
 862:	cd 40                	int    $0x40
 864:	c3                   	retq   

0000000000000865 <getpid>:
SYSCALL(getpid)
 865:	b8 0b 00 00 00       	mov    $0xb,%eax
 86a:	cd 40                	int    $0x40
 86c:	c3                   	retq   

000000000000086d <sbrk>:
SYSCALL(sbrk)
 86d:	b8 0c 00 00 00       	mov    $0xc,%eax
 872:	cd 40                	int    $0x40
 874:	c3                   	retq   

0000000000000875 <sleep>:
SYSCALL(sleep)
 875:	b8 0d 00 00 00       	mov    $0xd,%eax
 87a:	cd 40                	int    $0x40
 87c:	c3                   	retq   

000000000000087d <uptime>:
SYSCALL(uptime)
 87d:	b8 0e 00 00 00       	mov    $0xe,%eax
 882:	cd 40                	int    $0x40
 884:	c3                   	retq   

0000000000000885 <sysinfo>:
SYSCALL(sysinfo)
 885:	b8 16 00 00 00       	mov    $0x16,%eax
 88a:	cd 40                	int    $0x40
 88c:	c3                   	retq   

000000000000088d <crashn>:
SYSCALL(crashn)
 88d:	b8 17 00 00 00       	mov    $0x17,%eax
 892:	cd 40                	int    $0x40
 894:	c3                   	retq   

0000000000000895 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 895:	55                   	push   %rbp
 896:	48 89 e5             	mov    %rsp,%rbp
 899:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 89d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8a1:	48 83 e8 10          	sub    $0x10,%rax
 8a5:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8a9:	48 8b 05 60 05 00 00 	mov    0x560(%rip),%rax        # e10 <freep>
 8b0:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8b4:	eb 2f                	jmp    8e5 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 8b6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8ba:	48 8b 00             	mov    (%rax),%rax
 8bd:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8c1:	77 17                	ja     8da <free+0x45>
 8c3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8c7:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8cb:	77 2f                	ja     8fc <free+0x67>
 8cd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8d1:	48 8b 00             	mov    (%rax),%rax
 8d4:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8d8:	77 22                	ja     8fc <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 8da:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8de:	48 8b 00             	mov    (%rax),%rax
 8e1:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 8e5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8e9:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 8ed:	76 c7                	jbe    8b6 <free+0x21>
 8ef:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8f3:	48 8b 00             	mov    (%rax),%rax
 8f6:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 8fa:	76 ba                	jbe    8b6 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 8fc:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 900:	8b 40 08             	mov    0x8(%rax),%eax
 903:	89 c0                	mov    %eax,%eax
 905:	48 c1 e0 04          	shl    $0x4,%rax
 909:	48 89 c2             	mov    %rax,%rdx
 90c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 910:	48 01 c2             	add    %rax,%rdx
 913:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 917:	48 8b 00             	mov    (%rax),%rax
 91a:	48 39 c2             	cmp    %rax,%rdx
 91d:	75 2d                	jne    94c <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 91f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 923:	8b 50 08             	mov    0x8(%rax),%edx
 926:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 92a:	48 8b 00             	mov    (%rax),%rax
 92d:	8b 40 08             	mov    0x8(%rax),%eax
 930:	01 c2                	add    %eax,%edx
 932:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 936:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 939:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 93d:	48 8b 00             	mov    (%rax),%rax
 940:	48 8b 10             	mov    (%rax),%rdx
 943:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 947:	48 89 10             	mov    %rdx,(%rax)
 94a:	eb 0e                	jmp    95a <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 94c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 950:	48 8b 10             	mov    (%rax),%rdx
 953:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 957:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 95a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 95e:	8b 40 08             	mov    0x8(%rax),%eax
 961:	89 c0                	mov    %eax,%eax
 963:	48 c1 e0 04          	shl    $0x4,%rax
 967:	48 89 c2             	mov    %rax,%rdx
 96a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 96e:	48 01 d0             	add    %rdx,%rax
 971:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 975:	75 27                	jne    99e <free+0x109>
    p->s.size += bp->s.size;
 977:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 97b:	8b 50 08             	mov    0x8(%rax),%edx
 97e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 982:	8b 40 08             	mov    0x8(%rax),%eax
 985:	01 c2                	add    %eax,%edx
 987:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 98b:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 98e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 992:	48 8b 10             	mov    (%rax),%rdx
 995:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 999:	48 89 10             	mov    %rdx,(%rax)
 99c:	eb 0b                	jmp    9a9 <free+0x114>
  } else
    p->s.ptr = bp;
 99e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9a2:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 9a6:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 9a9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9ad:	48 89 05 5c 04 00 00 	mov    %rax,0x45c(%rip)        # e10 <freep>
}
 9b4:	90                   	nop
 9b5:	5d                   	pop    %rbp
 9b6:	c3                   	retq   

00000000000009b7 <morecore>:

static Header *morecore(uint nu) {
 9b7:	55                   	push   %rbp
 9b8:	48 89 e5             	mov    %rsp,%rbp
 9bb:	48 83 ec 20          	sub    $0x20,%rsp
 9bf:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 9c2:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 9c9:	77 07                	ja     9d2 <morecore+0x1b>
    nu = 4096;
 9cb:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 9d2:	8b 45 ec             	mov    -0x14(%rbp),%eax
 9d5:	c1 e0 04             	shl    $0x4,%eax
 9d8:	89 c7                	mov    %eax,%edi
 9da:	e8 8e fe ff ff       	callq  86d <sbrk>
 9df:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 9e3:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 9e8:	75 07                	jne    9f1 <morecore+0x3a>
    return 0;
 9ea:	b8 00 00 00 00       	mov    $0x0,%eax
 9ef:	eb 29                	jmp    a1a <morecore+0x63>
  hp = (Header *)p;
 9f1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9f5:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 9f9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9fd:	8b 55 ec             	mov    -0x14(%rbp),%edx
 a00:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 a03:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a07:	48 83 c0 10          	add    $0x10,%rax
 a0b:	48 89 c7             	mov    %rax,%rdi
 a0e:	e8 82 fe ff ff       	callq  895 <free>
  return freep;
 a13:	48 8b 05 f6 03 00 00 	mov    0x3f6(%rip),%rax        # e10 <freep>
}
 a1a:	c9                   	leaveq 
 a1b:	c3                   	retq   

0000000000000a1c <malloc>:

void *malloc(uint nbytes) {
 a1c:	55                   	push   %rbp
 a1d:	48 89 e5             	mov    %rsp,%rbp
 a20:	48 83 ec 30          	sub    $0x30,%rsp
 a24:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a27:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a2a:	48 83 c0 0f          	add    $0xf,%rax
 a2e:	48 c1 e8 04          	shr    $0x4,%rax
 a32:	83 c0 01             	add    $0x1,%eax
 a35:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a38:	48 8b 05 d1 03 00 00 	mov    0x3d1(%rip),%rax        # e10 <freep>
 a3f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 a43:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 a48:	75 2b                	jne    a75 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 a4a:	48 c7 45 f0 00 0e 00 	movq   $0xe00,-0x10(%rbp)
 a51:	00 
 a52:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a56:	48 89 05 b3 03 00 00 	mov    %rax,0x3b3(%rip)        # e10 <freep>
 a5d:	48 8b 05 ac 03 00 00 	mov    0x3ac(%rip),%rax        # e10 <freep>
 a64:	48 89 05 95 03 00 00 	mov    %rax,0x395(%rip)        # e00 <base>
    base.s.size = 0;
 a6b:	c7 05 93 03 00 00 00 	movl   $0x0,0x393(%rip)        # e08 <base+0x8>
 a72:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 a75:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a79:	48 8b 00             	mov    (%rax),%rax
 a7c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 a80:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a84:	8b 40 08             	mov    0x8(%rax),%eax
 a87:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a8a:	72 5f                	jb     aeb <malloc+0xcf>
      if (p->s.size == nunits)
 a8c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a90:	8b 40 08             	mov    0x8(%rax),%eax
 a93:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 a96:	75 10                	jne    aa8 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 a98:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a9c:	48 8b 10             	mov    (%rax),%rdx
 a9f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 aa3:	48 89 10             	mov    %rdx,(%rax)
 aa6:	eb 2e                	jmp    ad6 <malloc+0xba>
      else {
        p->s.size -= nunits;
 aa8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aac:	8b 40 08             	mov    0x8(%rax),%eax
 aaf:	2b 45 ec             	sub    -0x14(%rbp),%eax
 ab2:	89 c2                	mov    %eax,%edx
 ab4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ab8:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 abb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 abf:	8b 40 08             	mov    0x8(%rax),%eax
 ac2:	89 c0                	mov    %eax,%eax
 ac4:	48 c1 e0 04          	shl    $0x4,%rax
 ac8:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 acc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ad0:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ad3:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 ad6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ada:	48 89 05 2f 03 00 00 	mov    %rax,0x32f(%rip)        # e10 <freep>
      return (void *)(p + 1);
 ae1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ae5:	48 83 c0 10          	add    $0x10,%rax
 ae9:	eb 41                	jmp    b2c <malloc+0x110>
    }
    if (p == freep)
 aeb:	48 8b 05 1e 03 00 00 	mov    0x31e(%rip),%rax        # e10 <freep>
 af2:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 af6:	75 1c                	jne    b14 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 af8:	8b 45 ec             	mov    -0x14(%rbp),%eax
 afb:	89 c7                	mov    %eax,%edi
 afd:	e8 b5 fe ff ff       	callq  9b7 <morecore>
 b02:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 b06:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 b0b:	75 07                	jne    b14 <malloc+0xf8>
        return 0;
 b0d:	b8 00 00 00 00       	mov    $0x0,%eax
 b12:	eb 18                	jmp    b2c <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b14:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b18:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b1c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b20:	48 8b 00             	mov    (%rax),%rax
 b23:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b27:	e9 54 ff ff ff       	jmpq   a80 <malloc+0x64>
 b2c:	c9                   	leaveq 
 b2d:	c3                   	retq   
