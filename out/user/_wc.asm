
out/user/_wc:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <wc>:
#include <stat.h>
#include <user.h>

char buf[512];

void wc(int fd, char *name) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 30          	sub    $0x30,%rsp
   8:	89 7d dc             	mov    %edi,-0x24(%rbp)
   b:	48 89 75 d0          	mov    %rsi,-0x30(%rbp)
  int i, n;
  int l, w, c, inword;

  l = w = c = 0;
   f:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%rbp)
  16:	8b 45 f0             	mov    -0x10(%rbp),%eax
  19:	89 45 f4             	mov    %eax,-0xc(%rbp)
  1c:	8b 45 f4             	mov    -0xc(%rbp),%eax
  1f:	89 45 f8             	mov    %eax,-0x8(%rbp)
  inword = 0;
  22:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%rbp)
  while ((n = read(fd, buf, sizeof(buf))) > 0) {
  29:	eb 67                	jmp    92 <wc+0x92>
    for (i = 0; i < n; i++) {
  2b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  32:	eb 56                	jmp    8a <wc+0x8a>
      c++;
  34:	83 45 f0 01          	addl   $0x1,-0x10(%rbp)
      if (buf[i] == '\n')
  38:	8b 45 fc             	mov    -0x4(%rbp),%eax
  3b:	48 98                	cltq   
  3d:	0f b6 80 a0 0f 00 00 	movzbl 0xfa0(%rax),%eax
  44:	3c 0a                	cmp    $0xa,%al
  46:	75 04                	jne    4c <wc+0x4c>
        l++;
  48:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
      if (strchr(" \r\t\n\v", buf[i]))
  4c:	8b 45 fc             	mov    -0x4(%rbp),%eax
  4f:	48 98                	cltq   
  51:	0f b6 80 a0 0f 00 00 	movzbl 0xfa0(%rax),%eax
  58:	0f be c0             	movsbl %al,%eax
  5b:	89 c6                	mov    %eax,%esi
  5d:	bf 7e 0c 00 00       	mov    $0xc7e,%edi
  62:	e8 11 07 00 00       	callq  778 <strchr>
  67:	48 85 c0             	test   %rax,%rax
  6a:	74 09                	je     75 <wc+0x75>
        inword = 0;
  6c:	c7 45 ec 00 00 00 00 	movl   $0x0,-0x14(%rbp)
  73:	eb 11                	jmp    86 <wc+0x86>
      else if (!inword) {
  75:	83 7d ec 00          	cmpl   $0x0,-0x14(%rbp)
  79:	75 0b                	jne    86 <wc+0x86>
        w++;
  7b:	83 45 f4 01          	addl   $0x1,-0xc(%rbp)
        inword = 1;
  7f:	c7 45 ec 01 00 00 00 	movl   $0x1,-0x14(%rbp)
  int l, w, c, inword;

  l = w = c = 0;
  inword = 0;
  while ((n = read(fd, buf, sizeof(buf))) > 0) {
    for (i = 0; i < n; i++) {
  86:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
  8a:	8b 45 fc             	mov    -0x4(%rbp),%eax
  8d:	3b 45 e8             	cmp    -0x18(%rbp),%eax
  90:	7c a2                	jl     34 <wc+0x34>
  int i, n;
  int l, w, c, inword;

  l = w = c = 0;
  inword = 0;
  while ((n = read(fd, buf, sizeof(buf))) > 0) {
  92:	8b 45 dc             	mov    -0x24(%rbp),%eax
  95:	ba 00 02 00 00       	mov    $0x200,%edx
  9a:	be a0 0f 00 00       	mov    $0xfa0,%esi
  9f:	89 c7                	mov    %eax,%edi
  a1:	e8 a7 08 00 00       	callq  94d <read>
  a6:	89 45 e8             	mov    %eax,-0x18(%rbp)
  a9:	83 7d e8 00          	cmpl   $0x0,-0x18(%rbp)
  ad:	0f 8f 78 ff ff ff    	jg     2b <wc+0x2b>
        w++;
        inword = 1;
      }
    }
  }
  if (n < 0) {
  b3:	83 7d e8 00          	cmpl   $0x0,-0x18(%rbp)
  b7:	79 19                	jns    d2 <wc+0xd2>
    printf(1, "wc: read error\n");
  b9:	be 84 0c 00 00       	mov    $0xc84,%esi
  be:	bf 01 00 00 00       	mov    $0x1,%edi
  c3:	b8 00 00 00 00       	mov    $0x0,%eax
  c8:	e8 c5 02 00 00       	callq  392 <printf>
    exit();
  cd:	e8 63 08 00 00       	callq  935 <exit>
  }
  printf(1, "%d %d %d %s\n", l, w, c, name);
  d2:	48 8b 75 d0          	mov    -0x30(%rbp),%rsi
  d6:	8b 4d f0             	mov    -0x10(%rbp),%ecx
  d9:	8b 55 f4             	mov    -0xc(%rbp),%edx
  dc:	8b 45 f8             	mov    -0x8(%rbp),%eax
  df:	49 89 f1             	mov    %rsi,%r9
  e2:	41 89 c8             	mov    %ecx,%r8d
  e5:	89 d1                	mov    %edx,%ecx
  e7:	89 c2                	mov    %eax,%edx
  e9:	be 94 0c 00 00       	mov    $0xc94,%esi
  ee:	bf 01 00 00 00       	mov    $0x1,%edi
  f3:	b8 00 00 00 00       	mov    $0x0,%eax
  f8:	e8 95 02 00 00       	callq  392 <printf>
}
  fd:	90                   	nop
  fe:	c9                   	leaveq 
  ff:	c3                   	retq   

0000000000000100 <main>:

int main(int argc, char *argv[]) {
 100:	55                   	push   %rbp
 101:	48 89 e5             	mov    %rsp,%rbp
 104:	48 83 ec 20          	sub    $0x20,%rsp
 108:	89 7d ec             	mov    %edi,-0x14(%rbp)
 10b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd, i;

  if (argc <= 1) {
 10f:	83 7d ec 01          	cmpl   $0x1,-0x14(%rbp)
 113:	7f 14                	jg     129 <main+0x29>
    wc(0, "");
 115:	be a1 0c 00 00       	mov    $0xca1,%esi
 11a:	bf 00 00 00 00       	mov    $0x0,%edi
 11f:	e8 dc fe ff ff       	callq  0 <wc>
    exit();
 124:	e8 0c 08 00 00       	callq  935 <exit>
  }

  for (i = 1; i < argc; i++) {
 129:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%rbp)
 130:	e9 92 00 00 00       	jmpq   1c7 <main+0xc7>
    if ((fd = open(argv[i], 0)) < 0) {
 135:	8b 45 fc             	mov    -0x4(%rbp),%eax
 138:	48 98                	cltq   
 13a:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
 141:	00 
 142:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 146:	48 01 d0             	add    %rdx,%rax
 149:	48 8b 00             	mov    (%rax),%rax
 14c:	be 00 00 00 00       	mov    $0x0,%esi
 151:	48 89 c7             	mov    %rax,%rdi
 154:	e8 1c 08 00 00       	callq  975 <open>
 159:	89 45 f8             	mov    %eax,-0x8(%rbp)
 15c:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 160:	79 33                	jns    195 <main+0x95>
      printf(1, "wc: cannot open %s\n", argv[i]);
 162:	8b 45 fc             	mov    -0x4(%rbp),%eax
 165:	48 98                	cltq   
 167:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
 16e:	00 
 16f:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 173:	48 01 d0             	add    %rdx,%rax
 176:	48 8b 00             	mov    (%rax),%rax
 179:	48 89 c2             	mov    %rax,%rdx
 17c:	be a2 0c 00 00       	mov    $0xca2,%esi
 181:	bf 01 00 00 00       	mov    $0x1,%edi
 186:	b8 00 00 00 00       	mov    $0x0,%eax
 18b:	e8 02 02 00 00       	callq  392 <printf>
      exit();
 190:	e8 a0 07 00 00       	callq  935 <exit>
    }
    wc(fd, argv[i]);
 195:	8b 45 fc             	mov    -0x4(%rbp),%eax
 198:	48 98                	cltq   
 19a:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
 1a1:	00 
 1a2:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 1a6:	48 01 d0             	add    %rdx,%rax
 1a9:	48 8b 10             	mov    (%rax),%rdx
 1ac:	8b 45 f8             	mov    -0x8(%rbp),%eax
 1af:	48 89 d6             	mov    %rdx,%rsi
 1b2:	89 c7                	mov    %eax,%edi
 1b4:	e8 47 fe ff ff       	callq  0 <wc>
    close(fd);
 1b9:	8b 45 f8             	mov    -0x8(%rbp),%eax
 1bc:	89 c7                	mov    %eax,%edi
 1be:	e8 9a 07 00 00       	callq  95d <close>
  if (argc <= 1) {
    wc(0, "");
    exit();
  }

  for (i = 1; i < argc; i++) {
 1c3:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 1c7:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1ca:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 1cd:	0f 8c 62 ff ff ff    	jl     135 <main+0x35>
      exit();
    }
    wc(fd, argv[i]);
    close(fd);
  }
  exit();
 1d3:	e8 5d 07 00 00       	callq  935 <exit>

00000000000001d8 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
 1d8:	55                   	push   %rbp
 1d9:	48 89 e5             	mov    %rsp,%rbp
 1dc:	48 83 ec 10          	sub    $0x10,%rsp
 1e0:	89 7d fc             	mov    %edi,-0x4(%rbp)
 1e3:	89 f0                	mov    %esi,%eax
 1e5:	88 45 f8             	mov    %al,-0x8(%rbp)
 1e8:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
 1ec:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1ef:	ba 01 00 00 00       	mov    $0x1,%edx
 1f4:	48 89 ce             	mov    %rcx,%rsi
 1f7:	89 c7                	mov    %eax,%edi
 1f9:	e8 57 07 00 00       	callq  955 <write>
 1fe:	90                   	nop
 1ff:	c9                   	leaveq 
 200:	c3                   	retq   

0000000000000201 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
 201:	55                   	push   %rbp
 202:	48 89 e5             	mov    %rsp,%rbp
 205:	48 83 ec 40          	sub    $0x40,%rsp
 209:	89 7d cc             	mov    %edi,-0x34(%rbp)
 20c:	89 75 c8             	mov    %esi,-0x38(%rbp)
 20f:	89 55 c4             	mov    %edx,-0x3c(%rbp)
 212:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
 215:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 219:	74 1f                	je     23a <printint64+0x39>
 21b:	8b 45 c8             	mov    -0x38(%rbp),%eax
 21e:	c1 e8 1f             	shr    $0x1f,%eax
 221:	0f b6 c0             	movzbl %al,%eax
 224:	89 45 c0             	mov    %eax,-0x40(%rbp)
 227:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 22b:	74 0d                	je     23a <printint64+0x39>
    x = -xx;
 22d:	8b 45 c8             	mov    -0x38(%rbp),%eax
 230:	f7 d8                	neg    %eax
 232:	48 98                	cltq   
 234:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 238:	eb 09                	jmp    243 <printint64+0x42>
  else
    x = xx;
 23a:	8b 45 c8             	mov    -0x38(%rbp),%eax
 23d:	48 98                	cltq   
 23f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 243:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 24a:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 24d:	8d 41 01             	lea    0x1(%rcx),%eax
 250:	89 45 fc             	mov    %eax,-0x4(%rbp)
 253:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 256:	48 63 f0             	movslq %eax,%rsi
 259:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 25d:	ba 00 00 00 00       	mov    $0x0,%edx
 262:	48 f7 f6             	div    %rsi
 265:	48 89 d0             	mov    %rdx,%rax
 268:	0f b6 90 40 0f 00 00 	movzbl 0xf40(%rax),%edx
 26f:	48 63 c1             	movslq %ecx,%rax
 272:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 276:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 279:	48 63 f8             	movslq %eax,%rdi
 27c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 280:	ba 00 00 00 00       	mov    $0x0,%edx
 285:	48 f7 f7             	div    %rdi
 288:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 28c:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 291:	75 b7                	jne    24a <printint64+0x49>

  if (sgn)
 293:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 297:	74 2b                	je     2c4 <printint64+0xc3>
    buf[i++] = '-';
 299:	8b 45 fc             	mov    -0x4(%rbp),%eax
 29c:	8d 50 01             	lea    0x1(%rax),%edx
 29f:	89 55 fc             	mov    %edx,-0x4(%rbp)
 2a2:	48 98                	cltq   
 2a4:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 2a9:	eb 19                	jmp    2c4 <printint64+0xc3>
    putc(fd, buf[i]);
 2ab:	8b 45 fc             	mov    -0x4(%rbp),%eax
 2ae:	48 98                	cltq   
 2b0:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 2b5:	0f be d0             	movsbl %al,%edx
 2b8:	8b 45 cc             	mov    -0x34(%rbp),%eax
 2bb:	89 d6                	mov    %edx,%esi
 2bd:	89 c7                	mov    %eax,%edi
 2bf:	e8 14 ff ff ff       	callq  1d8 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 2c4:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 2c8:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 2cc:	79 dd                	jns    2ab <printint64+0xaa>
    putc(fd, buf[i]);
}
 2ce:	90                   	nop
 2cf:	c9                   	leaveq 
 2d0:	c3                   	retq   

00000000000002d1 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 2d1:	55                   	push   %rbp
 2d2:	48 89 e5             	mov    %rsp,%rbp
 2d5:	48 83 ec 30          	sub    $0x30,%rsp
 2d9:	89 7d dc             	mov    %edi,-0x24(%rbp)
 2dc:	89 75 d8             	mov    %esi,-0x28(%rbp)
 2df:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 2e2:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 2e5:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 2ec:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 2f0:	74 17                	je     309 <printint+0x38>
 2f2:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 2f6:	79 11                	jns    309 <printint+0x38>
    neg = 1;
 2f8:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 2ff:	8b 45 d8             	mov    -0x28(%rbp),%eax
 302:	f7 d8                	neg    %eax
 304:	89 45 f4             	mov    %eax,-0xc(%rbp)
 307:	eb 06                	jmp    30f <printint+0x3e>
  } else {
    x = xx;
 309:	8b 45 d8             	mov    -0x28(%rbp),%eax
 30c:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 30f:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 316:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 319:	8d 41 01             	lea    0x1(%rcx),%eax
 31c:	89 45 fc             	mov    %eax,-0x4(%rbp)
 31f:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 322:	8b 45 f4             	mov    -0xc(%rbp),%eax
 325:	ba 00 00 00 00       	mov    $0x0,%edx
 32a:	f7 f6                	div    %esi
 32c:	89 d0                	mov    %edx,%eax
 32e:	89 c0                	mov    %eax,%eax
 330:	0f b6 90 60 0f 00 00 	movzbl 0xf60(%rax),%edx
 337:	48 63 c1             	movslq %ecx,%rax
 33a:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 33e:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 341:	8b 45 f4             	mov    -0xc(%rbp),%eax
 344:	ba 00 00 00 00       	mov    $0x0,%edx
 349:	f7 f7                	div    %edi
 34b:	89 45 f4             	mov    %eax,-0xc(%rbp)
 34e:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 352:	75 c2                	jne    316 <printint+0x45>
  if (neg)
 354:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 358:	74 2b                	je     385 <printint+0xb4>
    buf[i++] = '-';
 35a:	8b 45 fc             	mov    -0x4(%rbp),%eax
 35d:	8d 50 01             	lea    0x1(%rax),%edx
 360:	89 55 fc             	mov    %edx,-0x4(%rbp)
 363:	48 98                	cltq   
 365:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 36a:	eb 19                	jmp    385 <printint+0xb4>
    putc(fd, buf[i]);
 36c:	8b 45 fc             	mov    -0x4(%rbp),%eax
 36f:	48 98                	cltq   
 371:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 376:	0f be d0             	movsbl %al,%edx
 379:	8b 45 dc             	mov    -0x24(%rbp),%eax
 37c:	89 d6                	mov    %edx,%esi
 37e:	89 c7                	mov    %eax,%edi
 380:	e8 53 fe ff ff       	callq  1d8 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 385:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 389:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 38d:	79 dd                	jns    36c <printint+0x9b>
    putc(fd, buf[i]);
}
 38f:	90                   	nop
 390:	c9                   	leaveq 
 391:	c3                   	retq   

0000000000000392 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 392:	55                   	push   %rbp
 393:	48 89 e5             	mov    %rsp,%rbp
 396:	48 83 ec 70          	sub    $0x70,%rsp
 39a:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 39d:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 3a1:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 3a5:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 3a9:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 3ad:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 3b1:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 3b8:	48 8d 45 10          	lea    0x10(%rbp),%rax
 3bc:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 3c0:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 3c4:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 3c8:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 3cf:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 3d6:	e9 68 02 00 00       	jmpq   643 <printf+0x2b1>
    c = fmt[i] & 0xff;
 3db:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 3de:	48 63 d0             	movslq %eax,%rdx
 3e1:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 3e5:	48 01 d0             	add    %rdx,%rax
 3e8:	0f b6 00             	movzbl (%rax),%eax
 3eb:	0f be c0             	movsbl %al,%eax
 3ee:	25 ff 00 00 00       	and    $0xff,%eax
 3f3:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 3f6:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 3fa:	75 30                	jne    42c <printf+0x9a>
      if (c == '%') {
 3fc:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 400:	75 13                	jne    415 <printf+0x83>
        state = '%';
 402:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 409:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 410:	e9 2a 02 00 00       	jmpq   63f <printf+0x2ad>
      } else {
        putc(fd, c);
 415:	8b 45 b8             	mov    -0x48(%rbp),%eax
 418:	0f be d0             	movsbl %al,%edx
 41b:	8b 45 9c             	mov    -0x64(%rbp),%eax
 41e:	89 d6                	mov    %edx,%esi
 420:	89 c7                	mov    %eax,%edi
 422:	e8 b1 fd ff ff       	callq  1d8 <putc>
 427:	e9 13 02 00 00       	jmpq   63f <printf+0x2ad>
      }
    } else if (state == '%') {
 42c:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 430:	0f 85 09 02 00 00    	jne    63f <printf+0x2ad>
      if (c == 'l') {
 436:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 43a:	75 0c                	jne    448 <printf+0xb6>
        lflag = 1;
 43c:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 443:	e9 f7 01 00 00       	jmpq   63f <printf+0x2ad>
      } else if (c == 'd') {
 448:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 44c:	0f 85 95 00 00 00    	jne    4e7 <printf+0x155>
        if (lflag == 1)
 452:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 456:	75 49                	jne    4a1 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 458:	8b 45 a0             	mov    -0x60(%rbp),%eax
 45b:	83 f8 30             	cmp    $0x30,%eax
 45e:	73 17                	jae    477 <printf+0xe5>
 460:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 464:	8b 55 a0             	mov    -0x60(%rbp),%edx
 467:	89 d2                	mov    %edx,%edx
 469:	48 01 d0             	add    %rdx,%rax
 46c:	8b 55 a0             	mov    -0x60(%rbp),%edx
 46f:	83 c2 08             	add    $0x8,%edx
 472:	89 55 a0             	mov    %edx,-0x60(%rbp)
 475:	eb 0c                	jmp    483 <printf+0xf1>
 477:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 47b:	48 8d 50 08          	lea    0x8(%rax),%rdx
 47f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 483:	48 8b 00             	mov    (%rax),%rax
 486:	89 c6                	mov    %eax,%esi
 488:	8b 45 9c             	mov    -0x64(%rbp),%eax
 48b:	b9 01 00 00 00       	mov    $0x1,%ecx
 490:	ba 0a 00 00 00       	mov    $0xa,%edx
 495:	89 c7                	mov    %eax,%edi
 497:	e8 65 fd ff ff       	callq  201 <printint64>
 49c:	e9 97 01 00 00       	jmpq   638 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 4a1:	8b 45 a0             	mov    -0x60(%rbp),%eax
 4a4:	83 f8 30             	cmp    $0x30,%eax
 4a7:	73 17                	jae    4c0 <printf+0x12e>
 4a9:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 4ad:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4b0:	89 d2                	mov    %edx,%edx
 4b2:	48 01 d0             	add    %rdx,%rax
 4b5:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4b8:	83 c2 08             	add    $0x8,%edx
 4bb:	89 55 a0             	mov    %edx,-0x60(%rbp)
 4be:	eb 0c                	jmp    4cc <printf+0x13a>
 4c0:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 4c4:	48 8d 50 08          	lea    0x8(%rax),%rdx
 4c8:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 4cc:	8b 30                	mov    (%rax),%esi
 4ce:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4d1:	b9 01 00 00 00       	mov    $0x1,%ecx
 4d6:	ba 0a 00 00 00       	mov    $0xa,%edx
 4db:	89 c7                	mov    %eax,%edi
 4dd:	e8 ef fd ff ff       	callq  2d1 <printint>
 4e2:	e9 51 01 00 00       	jmpq   638 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 4e7:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 4eb:	74 0a                	je     4f7 <printf+0x165>
 4ed:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 4f1:	0f 85 95 00 00 00    	jne    58c <printf+0x1fa>
        if (lflag == 1)
 4f7:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 4fb:	75 49                	jne    546 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 4fd:	8b 45 a0             	mov    -0x60(%rbp),%eax
 500:	83 f8 30             	cmp    $0x30,%eax
 503:	73 17                	jae    51c <printf+0x18a>
 505:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 509:	8b 55 a0             	mov    -0x60(%rbp),%edx
 50c:	89 d2                	mov    %edx,%edx
 50e:	48 01 d0             	add    %rdx,%rax
 511:	8b 55 a0             	mov    -0x60(%rbp),%edx
 514:	83 c2 08             	add    $0x8,%edx
 517:	89 55 a0             	mov    %edx,-0x60(%rbp)
 51a:	eb 0c                	jmp    528 <printf+0x196>
 51c:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 520:	48 8d 50 08          	lea    0x8(%rax),%rdx
 524:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 528:	48 8b 00             	mov    (%rax),%rax
 52b:	89 c6                	mov    %eax,%esi
 52d:	8b 45 9c             	mov    -0x64(%rbp),%eax
 530:	b9 00 00 00 00       	mov    $0x0,%ecx
 535:	ba 10 00 00 00       	mov    $0x10,%edx
 53a:	89 c7                	mov    %eax,%edi
 53c:	e8 c0 fc ff ff       	callq  201 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 541:	e9 f2 00 00 00       	jmpq   638 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 546:	8b 45 a0             	mov    -0x60(%rbp),%eax
 549:	83 f8 30             	cmp    $0x30,%eax
 54c:	73 17                	jae    565 <printf+0x1d3>
 54e:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 552:	8b 55 a0             	mov    -0x60(%rbp),%edx
 555:	89 d2                	mov    %edx,%edx
 557:	48 01 d0             	add    %rdx,%rax
 55a:	8b 55 a0             	mov    -0x60(%rbp),%edx
 55d:	83 c2 08             	add    $0x8,%edx
 560:	89 55 a0             	mov    %edx,-0x60(%rbp)
 563:	eb 0c                	jmp    571 <printf+0x1df>
 565:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 569:	48 8d 50 08          	lea    0x8(%rax),%rdx
 56d:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 571:	8b 30                	mov    (%rax),%esi
 573:	8b 45 9c             	mov    -0x64(%rbp),%eax
 576:	b9 00 00 00 00       	mov    $0x0,%ecx
 57b:	ba 10 00 00 00       	mov    $0x10,%edx
 580:	89 c7                	mov    %eax,%edi
 582:	e8 4a fd ff ff       	callq  2d1 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 587:	e9 ac 00 00 00       	jmpq   638 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 58c:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 590:	75 6b                	jne    5fd <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 592:	8b 45 a0             	mov    -0x60(%rbp),%eax
 595:	83 f8 30             	cmp    $0x30,%eax
 598:	73 17                	jae    5b1 <printf+0x21f>
 59a:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 59e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 5a1:	89 d2                	mov    %edx,%edx
 5a3:	48 01 d0             	add    %rdx,%rax
 5a6:	8b 55 a0             	mov    -0x60(%rbp),%edx
 5a9:	83 c2 08             	add    $0x8,%edx
 5ac:	89 55 a0             	mov    %edx,-0x60(%rbp)
 5af:	eb 0c                	jmp    5bd <printf+0x22b>
 5b1:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 5b5:	48 8d 50 08          	lea    0x8(%rax),%rdx
 5b9:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 5bd:	48 8b 00             	mov    (%rax),%rax
 5c0:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 5c4:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 5c9:	75 25                	jne    5f0 <printf+0x25e>
          s = "(null)";
 5cb:	48 c7 45 c8 b6 0c 00 	movq   $0xcb6,-0x38(%rbp)
 5d2:	00 
        for (; *s; s++)
 5d3:	eb 1b                	jmp    5f0 <printf+0x25e>
          putc(fd, *s);
 5d5:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 5d9:	0f b6 00             	movzbl (%rax),%eax
 5dc:	0f be d0             	movsbl %al,%edx
 5df:	8b 45 9c             	mov    -0x64(%rbp),%eax
 5e2:	89 d6                	mov    %edx,%esi
 5e4:	89 c7                	mov    %eax,%edi
 5e6:	e8 ed fb ff ff       	callq  1d8 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 5eb:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 5f0:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 5f4:	0f b6 00             	movzbl (%rax),%eax
 5f7:	84 c0                	test   %al,%al
 5f9:	75 da                	jne    5d5 <printf+0x243>
 5fb:	eb 3b                	jmp    638 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 5fd:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 601:	75 14                	jne    617 <printf+0x285>
        putc(fd, c);
 603:	8b 45 b8             	mov    -0x48(%rbp),%eax
 606:	0f be d0             	movsbl %al,%edx
 609:	8b 45 9c             	mov    -0x64(%rbp),%eax
 60c:	89 d6                	mov    %edx,%esi
 60e:	89 c7                	mov    %eax,%edi
 610:	e8 c3 fb ff ff       	callq  1d8 <putc>
 615:	eb 21                	jmp    638 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 617:	8b 45 9c             	mov    -0x64(%rbp),%eax
 61a:	be 25 00 00 00       	mov    $0x25,%esi
 61f:	89 c7                	mov    %eax,%edi
 621:	e8 b2 fb ff ff       	callq  1d8 <putc>
        putc(fd, c);
 626:	8b 45 b8             	mov    -0x48(%rbp),%eax
 629:	0f be d0             	movsbl %al,%edx
 62c:	8b 45 9c             	mov    -0x64(%rbp),%eax
 62f:	89 d6                	mov    %edx,%esi
 631:	89 c7                	mov    %eax,%edi
 633:	e8 a0 fb ff ff       	callq  1d8 <putc>
      }
      state = 0;
 638:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 63f:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 643:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 646:	48 63 d0             	movslq %eax,%rdx
 649:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 64d:	48 01 d0             	add    %rdx,%rax
 650:	0f b6 00             	movzbl (%rax),%eax
 653:	84 c0                	test   %al,%al
 655:	0f 85 80 fd ff ff    	jne    3db <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 65b:	90                   	nop
 65c:	c9                   	leaveq 
 65d:	c3                   	retq   

000000000000065e <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 65e:	55                   	push   %rbp
 65f:	48 89 e5             	mov    %rsp,%rbp
 662:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 666:	89 75 f4             	mov    %esi,-0xc(%rbp)
 669:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 66c:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 670:	8b 55 f0             	mov    -0x10(%rbp),%edx
 673:	8b 45 f4             	mov    -0xc(%rbp),%eax
 676:	48 89 ce             	mov    %rcx,%rsi
 679:	48 89 f7             	mov    %rsi,%rdi
 67c:	89 d1                	mov    %edx,%ecx
 67e:	fc                   	cld    
 67f:	f3 aa                	rep stos %al,%es:(%rdi)
 681:	89 ca                	mov    %ecx,%edx
 683:	48 89 fe             	mov    %rdi,%rsi
 686:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 68a:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 68d:	90                   	nop
 68e:	5d                   	pop    %rbp
 68f:	c3                   	retq   

0000000000000690 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 690:	55                   	push   %rbp
 691:	48 89 e5             	mov    %rsp,%rbp
 694:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 698:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 69c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6a0:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 6a4:	90                   	nop
 6a5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 6a9:	48 8d 50 01          	lea    0x1(%rax),%rdx
 6ad:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 6b1:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 6b5:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 6b9:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 6bd:	0f b6 12             	movzbl (%rdx),%edx
 6c0:	88 10                	mov    %dl,(%rax)
 6c2:	0f b6 00             	movzbl (%rax),%eax
 6c5:	84 c0                	test   %al,%al
 6c7:	75 dc                	jne    6a5 <strcpy+0x15>
    ;
  return os;
 6c9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 6cd:	5d                   	pop    %rbp
 6ce:	c3                   	retq   

00000000000006cf <strcmp>:

int strcmp(const char *p, const char *q) {
 6cf:	55                   	push   %rbp
 6d0:	48 89 e5             	mov    %rsp,%rbp
 6d3:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 6d7:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 6db:	eb 0a                	jmp    6e7 <strcmp+0x18>
    p++, q++;
 6dd:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 6e2:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 6e7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6eb:	0f b6 00             	movzbl (%rax),%eax
 6ee:	84 c0                	test   %al,%al
 6f0:	74 12                	je     704 <strcmp+0x35>
 6f2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6f6:	0f b6 10             	movzbl (%rax),%edx
 6f9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 6fd:	0f b6 00             	movzbl (%rax),%eax
 700:	38 c2                	cmp    %al,%dl
 702:	74 d9                	je     6dd <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 704:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 708:	0f b6 00             	movzbl (%rax),%eax
 70b:	0f b6 d0             	movzbl %al,%edx
 70e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 712:	0f b6 00             	movzbl (%rax),%eax
 715:	0f b6 c0             	movzbl %al,%eax
 718:	29 c2                	sub    %eax,%edx
 71a:	89 d0                	mov    %edx,%eax
}
 71c:	5d                   	pop    %rbp
 71d:	c3                   	retq   

000000000000071e <strlen>:

uint strlen(char *s) {
 71e:	55                   	push   %rbp
 71f:	48 89 e5             	mov    %rsp,%rbp
 722:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 726:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 72d:	eb 04                	jmp    733 <strlen+0x15>
 72f:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 733:	8b 45 fc             	mov    -0x4(%rbp),%eax
 736:	48 63 d0             	movslq %eax,%rdx
 739:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 73d:	48 01 d0             	add    %rdx,%rax
 740:	0f b6 00             	movzbl (%rax),%eax
 743:	84 c0                	test   %al,%al
 745:	75 e8                	jne    72f <strlen+0x11>
    ;
  return n;
 747:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 74a:	5d                   	pop    %rbp
 74b:	c3                   	retq   

000000000000074c <memset>:

void *memset(void *dst, int c, uint n) {
 74c:	55                   	push   %rbp
 74d:	48 89 e5             	mov    %rsp,%rbp
 750:	48 83 ec 10          	sub    $0x10,%rsp
 754:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 758:	89 75 f4             	mov    %esi,-0xc(%rbp)
 75b:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 75e:	8b 55 f0             	mov    -0x10(%rbp),%edx
 761:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 764:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 768:	89 ce                	mov    %ecx,%esi
 76a:	48 89 c7             	mov    %rax,%rdi
 76d:	e8 ec fe ff ff       	callq  65e <stosb>
  return dst;
 772:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 776:	c9                   	leaveq 
 777:	c3                   	retq   

0000000000000778 <strchr>:

char *strchr(const char *s, char c) {
 778:	55                   	push   %rbp
 779:	48 89 e5             	mov    %rsp,%rbp
 77c:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 780:	89 f0                	mov    %esi,%eax
 782:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 785:	eb 17                	jmp    79e <strchr+0x26>
    if (*s == c)
 787:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 78b:	0f b6 00             	movzbl (%rax),%eax
 78e:	3a 45 f4             	cmp    -0xc(%rbp),%al
 791:	75 06                	jne    799 <strchr+0x21>
      return (char *)s;
 793:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 797:	eb 15                	jmp    7ae <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 799:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 79e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 7a2:	0f b6 00             	movzbl (%rax),%eax
 7a5:	84 c0                	test   %al,%al
 7a7:	75 de                	jne    787 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 7a9:	b8 00 00 00 00       	mov    $0x0,%eax
}
 7ae:	5d                   	pop    %rbp
 7af:	c3                   	retq   

00000000000007b0 <gets>:

char *gets(char *buf, int max) {
 7b0:	55                   	push   %rbp
 7b1:	48 89 e5             	mov    %rsp,%rbp
 7b4:	48 83 ec 20          	sub    $0x20,%rsp
 7b8:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 7bc:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 7bf:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 7c6:	eb 48                	jmp    810 <gets+0x60>
    cc = read(0, &c, 1);
 7c8:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 7cc:	ba 01 00 00 00       	mov    $0x1,%edx
 7d1:	48 89 c6             	mov    %rax,%rsi
 7d4:	bf 00 00 00 00       	mov    $0x0,%edi
 7d9:	e8 6f 01 00 00       	callq  94d <read>
 7de:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 7e1:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 7e5:	7e 36                	jle    81d <gets+0x6d>
      break;
    buf[i++] = c;
 7e7:	8b 45 fc             	mov    -0x4(%rbp),%eax
 7ea:	8d 50 01             	lea    0x1(%rax),%edx
 7ed:	89 55 fc             	mov    %edx,-0x4(%rbp)
 7f0:	48 63 d0             	movslq %eax,%rdx
 7f3:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7f7:	48 01 c2             	add    %rax,%rdx
 7fa:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 7fe:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 800:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 804:	3c 0a                	cmp    $0xa,%al
 806:	74 16                	je     81e <gets+0x6e>
 808:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 80c:	3c 0d                	cmp    $0xd,%al
 80e:	74 0e                	je     81e <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 810:	8b 45 fc             	mov    -0x4(%rbp),%eax
 813:	83 c0 01             	add    $0x1,%eax
 816:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 819:	7c ad                	jl     7c8 <gets+0x18>
 81b:	eb 01                	jmp    81e <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 81d:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 81e:	8b 45 fc             	mov    -0x4(%rbp),%eax
 821:	48 63 d0             	movslq %eax,%rdx
 824:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 828:	48 01 d0             	add    %rdx,%rax
 82b:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 82e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 832:	c9                   	leaveq 
 833:	c3                   	retq   

0000000000000834 <stat>:

int stat(char *n, struct stat *st) {
 834:	55                   	push   %rbp
 835:	48 89 e5             	mov    %rsp,%rbp
 838:	48 83 ec 20          	sub    $0x20,%rsp
 83c:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 840:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 844:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 848:	be 00 00 00 00       	mov    $0x0,%esi
 84d:	48 89 c7             	mov    %rax,%rdi
 850:	e8 20 01 00 00       	callq  975 <open>
 855:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 858:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 85c:	79 07                	jns    865 <stat+0x31>
    return -1;
 85e:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 863:	eb 21                	jmp    886 <stat+0x52>
  r = fstat(fd, st);
 865:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 869:	8b 45 fc             	mov    -0x4(%rbp),%eax
 86c:	48 89 d6             	mov    %rdx,%rsi
 86f:	89 c7                	mov    %eax,%edi
 871:	e8 17 01 00 00       	callq  98d <fstat>
 876:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 879:	8b 45 fc             	mov    -0x4(%rbp),%eax
 87c:	89 c7                	mov    %eax,%edi
 87e:	e8 da 00 00 00       	callq  95d <close>
  return r;
 883:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 886:	c9                   	leaveq 
 887:	c3                   	retq   

0000000000000888 <atoi>:

int atoi(const char *s) {
 888:	55                   	push   %rbp
 889:	48 89 e5             	mov    %rsp,%rbp
 88c:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 890:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 897:	eb 28                	jmp    8c1 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 899:	8b 55 fc             	mov    -0x4(%rbp),%edx
 89c:	89 d0                	mov    %edx,%eax
 89e:	c1 e0 02             	shl    $0x2,%eax
 8a1:	01 d0                	add    %edx,%eax
 8a3:	01 c0                	add    %eax,%eax
 8a5:	89 c1                	mov    %eax,%ecx
 8a7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8ab:	48 8d 50 01          	lea    0x1(%rax),%rdx
 8af:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 8b3:	0f b6 00             	movzbl (%rax),%eax
 8b6:	0f be c0             	movsbl %al,%eax
 8b9:	01 c8                	add    %ecx,%eax
 8bb:	83 e8 30             	sub    $0x30,%eax
 8be:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 8c1:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8c5:	0f b6 00             	movzbl (%rax),%eax
 8c8:	3c 2f                	cmp    $0x2f,%al
 8ca:	7e 0b                	jle    8d7 <atoi+0x4f>
 8cc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8d0:	0f b6 00             	movzbl (%rax),%eax
 8d3:	3c 39                	cmp    $0x39,%al
 8d5:	7e c2                	jle    899 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 8d7:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 8da:	5d                   	pop    %rbp
 8db:	c3                   	retq   

00000000000008dc <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 8dc:	55                   	push   %rbp
 8dd:	48 89 e5             	mov    %rsp,%rbp
 8e0:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 8e4:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 8e8:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 8eb:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8ef:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 8f3:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 8f7:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 8fb:	eb 1d                	jmp    91a <memmove+0x3e>
    *dst++ = *src++;
 8fd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 901:	48 8d 50 01          	lea    0x1(%rax),%rdx
 905:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 909:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 90d:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 911:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 915:	0f b6 12             	movzbl (%rdx),%edx
 918:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 91a:	8b 45 dc             	mov    -0x24(%rbp),%eax
 91d:	8d 50 ff             	lea    -0x1(%rax),%edx
 920:	89 55 dc             	mov    %edx,-0x24(%rbp)
 923:	85 c0                	test   %eax,%eax
 925:	7f d6                	jg     8fd <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 927:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 92b:	5d                   	pop    %rbp
 92c:	c3                   	retq   

000000000000092d <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 92d:	b8 01 00 00 00       	mov    $0x1,%eax
 932:	cd 40                	int    $0x40
 934:	c3                   	retq   

0000000000000935 <exit>:
SYSCALL(exit)
 935:	b8 02 00 00 00       	mov    $0x2,%eax
 93a:	cd 40                	int    $0x40
 93c:	c3                   	retq   

000000000000093d <wait>:
SYSCALL(wait)
 93d:	b8 03 00 00 00       	mov    $0x3,%eax
 942:	cd 40                	int    $0x40
 944:	c3                   	retq   

0000000000000945 <pipe>:
SYSCALL(pipe)
 945:	b8 04 00 00 00       	mov    $0x4,%eax
 94a:	cd 40                	int    $0x40
 94c:	c3                   	retq   

000000000000094d <read>:
SYSCALL(read)
 94d:	b8 05 00 00 00       	mov    $0x5,%eax
 952:	cd 40                	int    $0x40
 954:	c3                   	retq   

0000000000000955 <write>:
SYSCALL(write)
 955:	b8 10 00 00 00       	mov    $0x10,%eax
 95a:	cd 40                	int    $0x40
 95c:	c3                   	retq   

000000000000095d <close>:
SYSCALL(close)
 95d:	b8 15 00 00 00       	mov    $0x15,%eax
 962:	cd 40                	int    $0x40
 964:	c3                   	retq   

0000000000000965 <kill>:
SYSCALL(kill)
 965:	b8 06 00 00 00       	mov    $0x6,%eax
 96a:	cd 40                	int    $0x40
 96c:	c3                   	retq   

000000000000096d <exec>:
SYSCALL(exec)
 96d:	b8 07 00 00 00       	mov    $0x7,%eax
 972:	cd 40                	int    $0x40
 974:	c3                   	retq   

0000000000000975 <open>:
SYSCALL(open)
 975:	b8 0f 00 00 00       	mov    $0xf,%eax
 97a:	cd 40                	int    $0x40
 97c:	c3                   	retq   

000000000000097d <mknod>:
SYSCALL(mknod)
 97d:	b8 11 00 00 00       	mov    $0x11,%eax
 982:	cd 40                	int    $0x40
 984:	c3                   	retq   

0000000000000985 <unlink>:
SYSCALL(unlink)
 985:	b8 12 00 00 00       	mov    $0x12,%eax
 98a:	cd 40                	int    $0x40
 98c:	c3                   	retq   

000000000000098d <fstat>:
SYSCALL(fstat)
 98d:	b8 08 00 00 00       	mov    $0x8,%eax
 992:	cd 40                	int    $0x40
 994:	c3                   	retq   

0000000000000995 <link>:
SYSCALL(link)
 995:	b8 13 00 00 00       	mov    $0x13,%eax
 99a:	cd 40                	int    $0x40
 99c:	c3                   	retq   

000000000000099d <mkdir>:
SYSCALL(mkdir)
 99d:	b8 14 00 00 00       	mov    $0x14,%eax
 9a2:	cd 40                	int    $0x40
 9a4:	c3                   	retq   

00000000000009a5 <chdir>:
SYSCALL(chdir)
 9a5:	b8 09 00 00 00       	mov    $0x9,%eax
 9aa:	cd 40                	int    $0x40
 9ac:	c3                   	retq   

00000000000009ad <dup>:
SYSCALL(dup)
 9ad:	b8 0a 00 00 00       	mov    $0xa,%eax
 9b2:	cd 40                	int    $0x40
 9b4:	c3                   	retq   

00000000000009b5 <getpid>:
SYSCALL(getpid)
 9b5:	b8 0b 00 00 00       	mov    $0xb,%eax
 9ba:	cd 40                	int    $0x40
 9bc:	c3                   	retq   

00000000000009bd <sbrk>:
SYSCALL(sbrk)
 9bd:	b8 0c 00 00 00       	mov    $0xc,%eax
 9c2:	cd 40                	int    $0x40
 9c4:	c3                   	retq   

00000000000009c5 <sleep>:
SYSCALL(sleep)
 9c5:	b8 0d 00 00 00       	mov    $0xd,%eax
 9ca:	cd 40                	int    $0x40
 9cc:	c3                   	retq   

00000000000009cd <uptime>:
SYSCALL(uptime)
 9cd:	b8 0e 00 00 00       	mov    $0xe,%eax
 9d2:	cd 40                	int    $0x40
 9d4:	c3                   	retq   

00000000000009d5 <sysinfo>:
SYSCALL(sysinfo)
 9d5:	b8 16 00 00 00       	mov    $0x16,%eax
 9da:	cd 40                	int    $0x40
 9dc:	c3                   	retq   

00000000000009dd <crashn>:
SYSCALL(crashn)
 9dd:	b8 17 00 00 00       	mov    $0x17,%eax
 9e2:	cd 40                	int    $0x40
 9e4:	c3                   	retq   

00000000000009e5 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 9e5:	55                   	push   %rbp
 9e6:	48 89 e5             	mov    %rsp,%rbp
 9e9:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 9ed:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 9f1:	48 83 e8 10          	sub    $0x10,%rax
 9f5:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 9f9:	48 8b 05 90 05 00 00 	mov    0x590(%rip),%rax        # f90 <freep>
 a00:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 a04:	eb 2f                	jmp    a35 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 a06:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a0a:	48 8b 00             	mov    (%rax),%rax
 a0d:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 a11:	77 17                	ja     a2a <free+0x45>
 a13:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a17:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 a1b:	77 2f                	ja     a4c <free+0x67>
 a1d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a21:	48 8b 00             	mov    (%rax),%rax
 a24:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 a28:	77 22                	ja     a4c <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 a2a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a2e:	48 8b 00             	mov    (%rax),%rax
 a31:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 a35:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a39:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 a3d:	76 c7                	jbe    a06 <free+0x21>
 a3f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a43:	48 8b 00             	mov    (%rax),%rax
 a46:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 a4a:	76 ba                	jbe    a06 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 a4c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a50:	8b 40 08             	mov    0x8(%rax),%eax
 a53:	89 c0                	mov    %eax,%eax
 a55:	48 c1 e0 04          	shl    $0x4,%rax
 a59:	48 89 c2             	mov    %rax,%rdx
 a5c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a60:	48 01 c2             	add    %rax,%rdx
 a63:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a67:	48 8b 00             	mov    (%rax),%rax
 a6a:	48 39 c2             	cmp    %rax,%rdx
 a6d:	75 2d                	jne    a9c <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 a6f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a73:	8b 50 08             	mov    0x8(%rax),%edx
 a76:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a7a:	48 8b 00             	mov    (%rax),%rax
 a7d:	8b 40 08             	mov    0x8(%rax),%eax
 a80:	01 c2                	add    %eax,%edx
 a82:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a86:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 a89:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a8d:	48 8b 00             	mov    (%rax),%rax
 a90:	48 8b 10             	mov    (%rax),%rdx
 a93:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a97:	48 89 10             	mov    %rdx,(%rax)
 a9a:	eb 0e                	jmp    aaa <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 a9c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aa0:	48 8b 10             	mov    (%rax),%rdx
 aa3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 aa7:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 aaa:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aae:	8b 40 08             	mov    0x8(%rax),%eax
 ab1:	89 c0                	mov    %eax,%eax
 ab3:	48 c1 e0 04          	shl    $0x4,%rax
 ab7:	48 89 c2             	mov    %rax,%rdx
 aba:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 abe:	48 01 d0             	add    %rdx,%rax
 ac1:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 ac5:	75 27                	jne    aee <free+0x109>
    p->s.size += bp->s.size;
 ac7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 acb:	8b 50 08             	mov    0x8(%rax),%edx
 ace:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ad2:	8b 40 08             	mov    0x8(%rax),%eax
 ad5:	01 c2                	add    %eax,%edx
 ad7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 adb:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 ade:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ae2:	48 8b 10             	mov    (%rax),%rdx
 ae5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ae9:	48 89 10             	mov    %rdx,(%rax)
 aec:	eb 0b                	jmp    af9 <free+0x114>
  } else
    p->s.ptr = bp;
 aee:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 af2:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 af6:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 af9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 afd:	48 89 05 8c 04 00 00 	mov    %rax,0x48c(%rip)        # f90 <freep>
}
 b04:	90                   	nop
 b05:	5d                   	pop    %rbp
 b06:	c3                   	retq   

0000000000000b07 <morecore>:

static Header *morecore(uint nu) {
 b07:	55                   	push   %rbp
 b08:	48 89 e5             	mov    %rsp,%rbp
 b0b:	48 83 ec 20          	sub    $0x20,%rsp
 b0f:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 b12:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 b19:	77 07                	ja     b22 <morecore+0x1b>
    nu = 4096;
 b1b:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 b22:	8b 45 ec             	mov    -0x14(%rbp),%eax
 b25:	c1 e0 04             	shl    $0x4,%eax
 b28:	89 c7                	mov    %eax,%edi
 b2a:	e8 8e fe ff ff       	callq  9bd <sbrk>
 b2f:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 b33:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 b38:	75 07                	jne    b41 <morecore+0x3a>
    return 0;
 b3a:	b8 00 00 00 00       	mov    $0x0,%eax
 b3f:	eb 29                	jmp    b6a <morecore+0x63>
  hp = (Header *)p;
 b41:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b45:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 b49:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b4d:	8b 55 ec             	mov    -0x14(%rbp),%edx
 b50:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 b53:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b57:	48 83 c0 10          	add    $0x10,%rax
 b5b:	48 89 c7             	mov    %rax,%rdi
 b5e:	e8 82 fe ff ff       	callq  9e5 <free>
  return freep;
 b63:	48 8b 05 26 04 00 00 	mov    0x426(%rip),%rax        # f90 <freep>
}
 b6a:	c9                   	leaveq 
 b6b:	c3                   	retq   

0000000000000b6c <malloc>:

void *malloc(uint nbytes) {
 b6c:	55                   	push   %rbp
 b6d:	48 89 e5             	mov    %rsp,%rbp
 b70:	48 83 ec 30          	sub    $0x30,%rsp
 b74:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 b77:	8b 45 dc             	mov    -0x24(%rbp),%eax
 b7a:	48 83 c0 0f          	add    $0xf,%rax
 b7e:	48 c1 e8 04          	shr    $0x4,%rax
 b82:	83 c0 01             	add    $0x1,%eax
 b85:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 b88:	48 8b 05 01 04 00 00 	mov    0x401(%rip),%rax        # f90 <freep>
 b8f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b93:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 b98:	75 2b                	jne    bc5 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 b9a:	48 c7 45 f0 80 0f 00 	movq   $0xf80,-0x10(%rbp)
 ba1:	00 
 ba2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 ba6:	48 89 05 e3 03 00 00 	mov    %rax,0x3e3(%rip)        # f90 <freep>
 bad:	48 8b 05 dc 03 00 00 	mov    0x3dc(%rip),%rax        # f90 <freep>
 bb4:	48 89 05 c5 03 00 00 	mov    %rax,0x3c5(%rip)        # f80 <base>
    base.s.size = 0;
 bbb:	c7 05 c3 03 00 00 00 	movl   $0x0,0x3c3(%rip)        # f88 <base+0x8>
 bc2:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 bc5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 bc9:	48 8b 00             	mov    (%rax),%rax
 bcc:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 bd0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bd4:	8b 40 08             	mov    0x8(%rax),%eax
 bd7:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 bda:	72 5f                	jb     c3b <malloc+0xcf>
      if (p->s.size == nunits)
 bdc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 be0:	8b 40 08             	mov    0x8(%rax),%eax
 be3:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 be6:	75 10                	jne    bf8 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 be8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bec:	48 8b 10             	mov    (%rax),%rdx
 bef:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 bf3:	48 89 10             	mov    %rdx,(%rax)
 bf6:	eb 2e                	jmp    c26 <malloc+0xba>
      else {
        p->s.size -= nunits;
 bf8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bfc:	8b 40 08             	mov    0x8(%rax),%eax
 bff:	2b 45 ec             	sub    -0x14(%rbp),%eax
 c02:	89 c2                	mov    %eax,%edx
 c04:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c08:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 c0b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c0f:	8b 40 08             	mov    0x8(%rax),%eax
 c12:	89 c0                	mov    %eax,%eax
 c14:	48 c1 e0 04          	shl    $0x4,%rax
 c18:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 c1c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c20:	8b 55 ec             	mov    -0x14(%rbp),%edx
 c23:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 c26:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c2a:	48 89 05 5f 03 00 00 	mov    %rax,0x35f(%rip)        # f90 <freep>
      return (void *)(p + 1);
 c31:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c35:	48 83 c0 10          	add    $0x10,%rax
 c39:	eb 41                	jmp    c7c <malloc+0x110>
    }
    if (p == freep)
 c3b:	48 8b 05 4e 03 00 00 	mov    0x34e(%rip),%rax        # f90 <freep>
 c42:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 c46:	75 1c                	jne    c64 <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 c48:	8b 45 ec             	mov    -0x14(%rbp),%eax
 c4b:	89 c7                	mov    %eax,%edi
 c4d:	e8 b5 fe ff ff       	callq  b07 <morecore>
 c52:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 c56:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 c5b:	75 07                	jne    c64 <malloc+0xf8>
        return 0;
 c5d:	b8 00 00 00 00       	mov    $0x0,%eax
 c62:	eb 18                	jmp    c7c <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 c64:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c68:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 c6c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c70:	48 8b 00             	mov    (%rax),%rax
 c73:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 c77:	e9 54 ff ff ff       	jmpq   bd0 <malloc+0x64>
 c7c:	c9                   	leaveq 
 c7d:	c3                   	retq   
