
out/user/_lab5test_a:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <modification>:
      while (1)                                                                \
        ;                                                                      \
    }                                                                          \
  } while (0)

void modification(void) {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
       4:	48 83 ec 10          	sub    $0x10,%rsp
  int fd;

  printf(stdout, "modification test starting\n");
       8:	8b 05 92 16 00 00    	mov    0x1692(%rip),%eax        # 16a0 <stdout>
       e:	be c0 11 00 00       	mov    $0x11c0,%esi
      13:	89 c7                	mov    %eax,%edi
      15:	b8 00 00 00 00       	mov    $0x0,%eax
      1a:	e8 ae 08 00 00       	callq  8cd <printf>
  strcpy(buf, "lab5 is 451's last lab.\n");
      1f:	be dc 11 00 00       	mov    $0x11dc,%esi
      24:	bf 20 17 00 00       	mov    $0x1720,%edi
      29:	e8 9d 0b 00 00       	callq  bcb <strcpy>
  fd = open("small.txt", O_RDWR);
      2e:	be 02 00 00 00       	mov    $0x2,%esi
      33:	bf f5 11 00 00       	mov    $0x11f5,%edi
      38:	e8 73 0e 00 00       	callq  eb0 <open>
      3d:	89 45 fc             	mov    %eax,-0x4(%rbp)
  write(fd, buf, 50);
      40:	8b 45 fc             	mov    -0x4(%rbp),%eax
      43:	ba 32 00 00 00       	mov    $0x32,%edx
      48:	be 20 17 00 00       	mov    $0x1720,%esi
      4d:	89 c7                	mov    %eax,%edi
      4f:	e8 3c 0e 00 00       	callq  e90 <write>
  close(fd);
      54:	8b 45 fc             	mov    -0x4(%rbp),%eax
      57:	89 c7                	mov    %eax,%edi
      59:	e8 3a 0e 00 00       	callq  e98 <close>

  fd = open("small.txt", O_RDONLY);
      5e:	be 00 00 00 00       	mov    $0x0,%esi
      63:	bf f5 11 00 00       	mov    $0x11f5,%edi
      68:	e8 43 0e 00 00       	callq  eb0 <open>
      6d:	89 45 fc             	mov    %eax,-0x4(%rbp)
  read(fd, buf, 50);
      70:	8b 45 fc             	mov    -0x4(%rbp),%eax
      73:	ba 32 00 00 00       	mov    $0x32,%edx
      78:	be 20 17 00 00       	mov    $0x1720,%esi
      7d:	89 c7                	mov    %eax,%edi
      7f:	e8 04 0e 00 00       	callq  e88 <read>

  if (strcmp(buf, "lab5 is 451's last lab.\n") != 0)
      84:	be dc 11 00 00       	mov    $0x11dc,%esi
      89:	bf 20 17 00 00       	mov    $0x1720,%edi
      8e:	e8 77 0b 00 00       	callq  c0a <strcmp>
      93:	85 c0                	test   %eax,%eax
      95:	74 54                	je     eb <modification+0xeb>
    error("file content was not lab5 is 451's last lab., was: '%s'", buf);
      97:	8b 05 03 16 00 00    	mov    0x1603(%rip),%eax        # 16a0 <stdout>
      9d:	ba 2e 00 00 00       	mov    $0x2e,%edx
      a2:	be ff 11 00 00       	mov    $0x11ff,%esi
      a7:	89 c7                	mov    %eax,%edi
      a9:	b8 00 00 00 00       	mov    $0x0,%eax
      ae:	e8 1a 08 00 00       	callq  8cd <printf>
      b3:	8b 05 e7 15 00 00    	mov    0x15e7(%rip),%eax        # 16a0 <stdout>
      b9:	ba 20 17 00 00       	mov    $0x1720,%edx
      be:	be 18 12 00 00       	mov    $0x1218,%esi
      c3:	89 c7                	mov    %eax,%edi
      c5:	b8 00 00 00 00       	mov    $0x0,%eax
      ca:	e8 fe 07 00 00       	callq  8cd <printf>
      cf:	8b 05 cb 15 00 00    	mov    0x15cb(%rip),%eax        # 16a0 <stdout>
      d5:	be 50 12 00 00       	mov    $0x1250,%esi
      da:	89 c7                	mov    %eax,%edi
      dc:	b8 00 00 00 00       	mov    $0x0,%eax
      e1:	e8 e7 07 00 00       	callq  8cd <printf>
      e6:	e8 85 0d 00 00       	callq  e70 <exit>

  close(fd);
      eb:	8b 45 fc             	mov    -0x4(%rbp),%eax
      ee:	89 c7                	mov    %eax,%edi
      f0:	e8 a3 0d 00 00       	callq  e98 <close>

  printf(stdout, "modification test ok!\n");
      f5:	8b 05 a5 15 00 00    	mov    0x15a5(%rip),%eax        # 16a0 <stdout>
      fb:	be 52 12 00 00       	mov    $0x1252,%esi
     100:	89 c7                	mov    %eax,%edi
     102:	b8 00 00 00 00       	mov    $0x0,%eax
     107:	e8 c1 07 00 00       	callq  8cd <printf>
}
     10c:	90                   	nop
     10d:	c9                   	leaveq 
     10e:	c3                   	retq   

000000000000010f <onefile>:

void onefile(void) {
     10f:	55                   	push   %rbp
     110:	48 89 e5             	mov    %rsp,%rbp
     113:	48 83 ec 10          	sub    $0x10,%rsp
  int fd, i, j;
  printf(1, "one file test\n");
     117:	be 69 12 00 00       	mov    $0x1269,%esi
     11c:	bf 01 00 00 00       	mov    $0x1,%edi
     121:	b8 00 00 00 00       	mov    $0x0,%eax
     126:	e8 a2 07 00 00       	callq  8cd <printf>

  if ((fd = open("onefile.txt", O_CREATE|O_RDWR)) < 0)
     12b:	be 02 02 00 00       	mov    $0x202,%esi
     130:	bf 78 12 00 00       	mov    $0x1278,%edi
     135:	e8 76 0d 00 00       	callq  eb0 <open>
     13a:	89 45 f4             	mov    %eax,-0xc(%rbp)
     13d:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     141:	79 4f                	jns    192 <onefile+0x83>
    error("create 'onefile.txt' failed");
     143:	8b 05 57 15 00 00    	mov    0x1557(%rip),%eax        # 16a0 <stdout>
     149:	ba 3a 00 00 00       	mov    $0x3a,%edx
     14e:	be ff 11 00 00       	mov    $0x11ff,%esi
     153:	89 c7                	mov    %eax,%edi
     155:	b8 00 00 00 00       	mov    $0x0,%eax
     15a:	e8 6e 07 00 00       	callq  8cd <printf>
     15f:	8b 05 3b 15 00 00    	mov    0x153b(%rip),%eax        # 16a0 <stdout>
     165:	be 84 12 00 00       	mov    $0x1284,%esi
     16a:	89 c7                	mov    %eax,%edi
     16c:	b8 00 00 00 00       	mov    $0x0,%eax
     171:	e8 57 07 00 00       	callq  8cd <printf>
     176:	8b 05 24 15 00 00    	mov    0x1524(%rip),%eax        # 16a0 <stdout>
     17c:	be 50 12 00 00       	mov    $0x1250,%esi
     181:	89 c7                	mov    %eax,%edi
     183:	b8 00 00 00 00       	mov    $0x0,%eax
     188:	e8 40 07 00 00       	callq  8cd <printf>
     18d:	e8 de 0c 00 00       	callq  e70 <exit>

  memset(buf, 0, sizeof(buf));
     192:	ba 00 20 00 00       	mov    $0x2000,%edx
     197:	be 00 00 00 00       	mov    $0x0,%esi
     19c:	bf 20 17 00 00       	mov    $0x1720,%edi
     1a1:	e8 e1 0a 00 00       	callq  c87 <memset>
  for (i = 0; i < 10; i++) {
     1a6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     1ad:	eb 2c                	jmp    1db <onefile+0xcc>
    memset(buf, i, 512);
     1af:	8b 45 fc             	mov    -0x4(%rbp),%eax
     1b2:	ba 00 02 00 00       	mov    $0x200,%edx
     1b7:	89 c6                	mov    %eax,%esi
     1b9:	bf 20 17 00 00       	mov    $0x1720,%edi
     1be:	e8 c4 0a 00 00       	callq  c87 <memset>
    write(fd, buf, 512);
     1c3:	8b 45 f4             	mov    -0xc(%rbp),%eax
     1c6:	ba 00 02 00 00       	mov    $0x200,%edx
     1cb:	be 20 17 00 00       	mov    $0x1720,%esi
     1d0:	89 c7                	mov    %eax,%edi
     1d2:	e8 b9 0c 00 00       	callq  e90 <write>

  if ((fd = open("onefile.txt", O_CREATE|O_RDWR)) < 0)
    error("create 'onefile.txt' failed");

  memset(buf, 0, sizeof(buf));
  for (i = 0; i < 10; i++) {
     1d7:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     1db:	83 7d fc 09          	cmpl   $0x9,-0x4(%rbp)
     1df:	7e ce                	jle    1af <onefile+0xa0>
    memset(buf, i, 512);
    write(fd, buf, 512);
  }
 
  close(fd);
     1e1:	8b 45 f4             	mov    -0xc(%rbp),%eax
     1e4:	89 c7                	mov    %eax,%edi
     1e6:	e8 ad 0c 00 00       	callq  e98 <close>

  if ((fd = open("onefile.txt", O_RDONLY)) < 0)
     1eb:	be 00 00 00 00       	mov    $0x0,%esi
     1f0:	bf 78 12 00 00       	mov    $0x1278,%edi
     1f5:	e8 b6 0c 00 00       	callq  eb0 <open>
     1fa:	89 45 f4             	mov    %eax,-0xc(%rbp)
     1fd:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     201:	79 4f                	jns    252 <onefile+0x143>
    error("couldn't reopen 'onefile.txt'");
     203:	8b 05 97 14 00 00    	mov    0x1497(%rip),%eax        # 16a0 <stdout>
     209:	ba 45 00 00 00       	mov    $0x45,%edx
     20e:	be ff 11 00 00       	mov    $0x11ff,%esi
     213:	89 c7                	mov    %eax,%edi
     215:	b8 00 00 00 00       	mov    $0x0,%eax
     21a:	e8 ae 06 00 00       	callq  8cd <printf>
     21f:	8b 05 7b 14 00 00    	mov    0x147b(%rip),%eax        # 16a0 <stdout>
     225:	be a0 12 00 00       	mov    $0x12a0,%esi
     22a:	89 c7                	mov    %eax,%edi
     22c:	b8 00 00 00 00       	mov    $0x0,%eax
     231:	e8 97 06 00 00       	callq  8cd <printf>
     236:	8b 05 64 14 00 00    	mov    0x1464(%rip),%eax        # 16a0 <stdout>
     23c:	be 50 12 00 00       	mov    $0x1250,%esi
     241:	89 c7                	mov    %eax,%edi
     243:	b8 00 00 00 00       	mov    $0x0,%eax
     248:	e8 80 06 00 00       	callq  8cd <printf>
     24d:	e8 1e 0c 00 00       	callq  e70 <exit>

  memset(buf, 0, sizeof(buf));
     252:	ba 00 20 00 00       	mov    $0x2000,%edx
     257:	be 00 00 00 00       	mov    $0x0,%esi
     25c:	bf 20 17 00 00       	mov    $0x1720,%edi
     261:	e8 21 0a 00 00       	callq  c87 <memset>
  for (i = 0; i < 10; i++) {
     266:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     26d:	e9 be 00 00 00       	jmpq   330 <onefile+0x221>
    if (read(fd, buf, 512) != 512)
     272:	8b 45 f4             	mov    -0xc(%rbp),%eax
     275:	ba 00 02 00 00       	mov    $0x200,%edx
     27a:	be 20 17 00 00       	mov    $0x1720,%esi
     27f:	89 c7                	mov    %eax,%edi
     281:	e8 02 0c 00 00       	callq  e88 <read>
     286:	3d 00 02 00 00       	cmp    $0x200,%eax
     28b:	74 52                	je     2df <onefile+0x1d0>
      error("couldn't read the bytes for iteration %d", i);
     28d:	8b 05 0d 14 00 00    	mov    0x140d(%rip),%eax        # 16a0 <stdout>
     293:	ba 4a 00 00 00       	mov    $0x4a,%edx
     298:	be ff 11 00 00       	mov    $0x11ff,%esi
     29d:	89 c7                	mov    %eax,%edi
     29f:	b8 00 00 00 00       	mov    $0x0,%eax
     2a4:	e8 24 06 00 00       	callq  8cd <printf>
     2a9:	8b 05 f1 13 00 00    	mov    0x13f1(%rip),%eax        # 16a0 <stdout>
     2af:	8b 55 fc             	mov    -0x4(%rbp),%edx
     2b2:	be c0 12 00 00       	mov    $0x12c0,%esi
     2b7:	89 c7                	mov    %eax,%edi
     2b9:	b8 00 00 00 00       	mov    $0x0,%eax
     2be:	e8 0a 06 00 00       	callq  8cd <printf>
     2c3:	8b 05 d7 13 00 00    	mov    0x13d7(%rip),%eax        # 16a0 <stdout>
     2c9:	be 50 12 00 00       	mov    $0x1250,%esi
     2ce:	89 c7                	mov    %eax,%edi
     2d0:	b8 00 00 00 00       	mov    $0x0,%eax
     2d5:	e8 f3 05 00 00       	callq  8cd <printf>
     2da:	e8 91 0b 00 00       	callq  e70 <exit>
    for (j = 0; j < 512; j++)
     2df:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     2e6:	eb 3b                	jmp    323 <onefile+0x214>
      assert(i == buf[j]);
     2e8:	8b 45 f8             	mov    -0x8(%rbp),%eax
     2eb:	48 98                	cltq   
     2ed:	0f b6 80 20 17 00 00 	movzbl 0x1720(%rax),%eax
     2f4:	0f be c0             	movsbl %al,%eax
     2f7:	3b 45 fc             	cmp    -0x4(%rbp),%eax
     2fa:	74 23                	je     31f <onefile+0x210>
     2fc:	8b 05 9e 13 00 00    	mov    0x139e(%rip),%eax        # 16a0 <stdout>
     302:	b9 e9 12 00 00       	mov    $0x12e9,%ecx
     307:	ba 4c 00 00 00       	mov    $0x4c,%edx
     30c:	be f8 12 00 00       	mov    $0x12f8,%esi
     311:	89 c7                	mov    %eax,%edi
     313:	b8 00 00 00 00       	mov    $0x0,%eax
     318:	e8 b0 05 00 00       	callq  8cd <printf>
     31d:	eb fe                	jmp    31d <onefile+0x20e>

  memset(buf, 0, sizeof(buf));
  for (i = 0; i < 10; i++) {
    if (read(fd, buf, 512) != 512)
      error("couldn't read the bytes for iteration %d", i);
    for (j = 0; j < 512; j++)
     31f:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     323:	81 7d f8 ff 01 00 00 	cmpl   $0x1ff,-0x8(%rbp)
     32a:	7e bc                	jle    2e8 <onefile+0x1d9>

  if ((fd = open("onefile.txt", O_RDONLY)) < 0)
    error("couldn't reopen 'onefile.txt'");

  memset(buf, 0, sizeof(buf));
  for (i = 0; i < 10; i++) {
     32c:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     330:	83 7d fc 09          	cmpl   $0x9,-0x4(%rbp)
     334:	0f 8e 38 ff ff ff    	jle    272 <onefile+0x163>
      error("couldn't read the bytes for iteration %d", i);
    for (j = 0; j < 512; j++)
      assert(i == buf[j]);
  }

  printf(1, "one file test passed\n");
     33a:	be 18 13 00 00       	mov    $0x1318,%esi
     33f:	bf 01 00 00 00       	mov    $0x1,%edi
     344:	b8 00 00 00 00       	mov    $0x0,%eax
     349:	e8 7f 05 00 00       	callq  8cd <printf>
}
     34e:	90                   	nop
     34f:	c9                   	leaveq 
     350:	c3                   	retq   

0000000000000351 <fourfiles>:

// four processes write different files at the same
// time, to test block allocation.
void fourfiles(void) {
     351:	55                   	push   %rbp
     352:	48 89 e5             	mov    %rsp,%rbp
     355:	48 83 ec 50          	sub    $0x50,%rsp
  int fd, pid, i, j, n, total, pi;
  char *names[] = {"f0", "f1", "f2", "f3"};
     359:	48 c7 45 b8 2e 13 00 	movq   $0x132e,-0x48(%rbp)
     360:	00 
     361:	48 c7 45 c0 31 13 00 	movq   $0x1331,-0x40(%rbp)
     368:	00 
     369:	48 c7 45 c8 34 13 00 	movq   $0x1334,-0x38(%rbp)
     370:	00 
     371:	48 c7 45 d0 37 13 00 	movq   $0x1337,-0x30(%rbp)
     378:	00 
  char *fname;

  printf(1, "fourfiles test\n");
     379:	be 3a 13 00 00       	mov    $0x133a,%esi
     37e:	bf 01 00 00 00       	mov    $0x1,%edi
     383:	b8 00 00 00 00       	mov    $0x0,%eax
     388:	e8 40 05 00 00       	callq  8cd <printf>

  for (pi = 0; pi < 4; pi++) {
     38d:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%rbp)
     394:	e9 83 01 00 00       	jmpq   51c <fourfiles+0x1cb>
    fname = names[pi];
     399:	8b 45 f0             	mov    -0x10(%rbp),%eax
     39c:	48 98                	cltq   
     39e:	48 8b 44 c5 b8       	mov    -0x48(%rbp,%rax,8),%rax
     3a3:	48 89 45 e8          	mov    %rax,-0x18(%rbp)

    pid = fork();
     3a7:	e8 bc 0a 00 00       	callq  e68 <fork>
     3ac:	89 45 e4             	mov    %eax,-0x1c(%rbp)
    if (pid < 0) {
     3af:	83 7d e4 00          	cmpl   $0x0,-0x1c(%rbp)
     3b3:	79 4f                	jns    404 <fourfiles+0xb3>
      error("fork failed\n");
     3b5:	8b 05 e5 12 00 00    	mov    0x12e5(%rip),%eax        # 16a0 <stdout>
     3bb:	ba 60 00 00 00       	mov    $0x60,%edx
     3c0:	be ff 11 00 00       	mov    $0x11ff,%esi
     3c5:	89 c7                	mov    %eax,%edi
     3c7:	b8 00 00 00 00       	mov    $0x0,%eax
     3cc:	e8 fc 04 00 00       	callq  8cd <printf>
     3d1:	8b 05 c9 12 00 00    	mov    0x12c9(%rip),%eax        # 16a0 <stdout>
     3d7:	be 4a 13 00 00       	mov    $0x134a,%esi
     3dc:	89 c7                	mov    %eax,%edi
     3de:	b8 00 00 00 00       	mov    $0x0,%eax
     3e3:	e8 e5 04 00 00       	callq  8cd <printf>
     3e8:	8b 05 b2 12 00 00    	mov    0x12b2(%rip),%eax        # 16a0 <stdout>
     3ee:	be 50 12 00 00       	mov    $0x1250,%esi
     3f3:	89 c7                	mov    %eax,%edi
     3f5:	b8 00 00 00 00       	mov    $0x0,%eax
     3fa:	e8 ce 04 00 00       	callq  8cd <printf>
     3ff:	e8 6c 0a 00 00       	callq  e70 <exit>
    }

    if (pid == 0) {
     404:	83 7d e4 00          	cmpl   $0x0,-0x1c(%rbp)
     408:	0f 85 0a 01 00 00    	jne    518 <fourfiles+0x1c7>
      fd = open(fname, O_CREATE | O_RDWR);
     40e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     412:	be 02 02 00 00       	mov    $0x202,%esi
     417:	48 89 c7             	mov    %rax,%rdi
     41a:	e8 91 0a 00 00       	callq  eb0 <open>
     41f:	89 45 e0             	mov    %eax,-0x20(%rbp)
      if (fd < 0) {
     422:	83 7d e0 00          	cmpl   $0x0,-0x20(%rbp)
     426:	79 4f                	jns    477 <fourfiles+0x126>
        error("create failed\n");
     428:	8b 05 72 12 00 00    	mov    0x1272(%rip),%eax        # 16a0 <stdout>
     42e:	ba 66 00 00 00       	mov    $0x66,%edx
     433:	be ff 11 00 00       	mov    $0x11ff,%esi
     438:	89 c7                	mov    %eax,%edi
     43a:	b8 00 00 00 00       	mov    $0x0,%eax
     43f:	e8 89 04 00 00       	callq  8cd <printf>
     444:	8b 05 56 12 00 00    	mov    0x1256(%rip),%eax        # 16a0 <stdout>
     44a:	be 57 13 00 00       	mov    $0x1357,%esi
     44f:	89 c7                	mov    %eax,%edi
     451:	b8 00 00 00 00       	mov    $0x0,%eax
     456:	e8 72 04 00 00       	callq  8cd <printf>
     45b:	8b 05 3f 12 00 00    	mov    0x123f(%rip),%eax        # 16a0 <stdout>
     461:	be 50 12 00 00       	mov    $0x1250,%esi
     466:	89 c7                	mov    %eax,%edi
     468:	b8 00 00 00 00       	mov    $0x0,%eax
     46d:	e8 5b 04 00 00       	callq  8cd <printf>
     472:	e8 f9 09 00 00       	callq  e70 <exit>
      }

      memset(buf, '0' + pi, 512);
     477:	8b 45 f0             	mov    -0x10(%rbp),%eax
     47a:	83 c0 30             	add    $0x30,%eax
     47d:	ba 00 02 00 00       	mov    $0x200,%edx
     482:	89 c6                	mov    %eax,%esi
     484:	bf 20 17 00 00       	mov    $0x1720,%edi
     489:	e8 f9 07 00 00       	callq  c87 <memset>
      for (i = 0; i < 12; i++) {
     48e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     495:	eb 76                	jmp    50d <fourfiles+0x1bc>
        if ((n = write(fd, buf, 500)) != 500) {
     497:	8b 45 e0             	mov    -0x20(%rbp),%eax
     49a:	ba f4 01 00 00       	mov    $0x1f4,%edx
     49f:	be 20 17 00 00       	mov    $0x1720,%esi
     4a4:	89 c7                	mov    %eax,%edi
     4a6:	e8 e5 09 00 00       	callq  e90 <write>
     4ab:	89 45 dc             	mov    %eax,-0x24(%rbp)
     4ae:	81 7d dc f4 01 00 00 	cmpl   $0x1f4,-0x24(%rbp)
     4b5:	74 52                	je     509 <fourfiles+0x1b8>
          error("write failed %d\n", n);
     4b7:	8b 05 e3 11 00 00    	mov    0x11e3(%rip),%eax        # 16a0 <stdout>
     4bd:	ba 6c 00 00 00       	mov    $0x6c,%edx
     4c2:	be ff 11 00 00       	mov    $0x11ff,%esi
     4c7:	89 c7                	mov    %eax,%edi
     4c9:	b8 00 00 00 00       	mov    $0x0,%eax
     4ce:	e8 fa 03 00 00       	callq  8cd <printf>
     4d3:	8b 05 c7 11 00 00    	mov    0x11c7(%rip),%eax        # 16a0 <stdout>
     4d9:	8b 55 dc             	mov    -0x24(%rbp),%edx
     4dc:	be 66 13 00 00       	mov    $0x1366,%esi
     4e1:	89 c7                	mov    %eax,%edi
     4e3:	b8 00 00 00 00       	mov    $0x0,%eax
     4e8:	e8 e0 03 00 00       	callq  8cd <printf>
     4ed:	8b 05 ad 11 00 00    	mov    0x11ad(%rip),%eax        # 16a0 <stdout>
     4f3:	be 50 12 00 00       	mov    $0x1250,%esi
     4f8:	89 c7                	mov    %eax,%edi
     4fa:	b8 00 00 00 00       	mov    $0x0,%eax
     4ff:	e8 c9 03 00 00       	callq  8cd <printf>
     504:	e8 67 09 00 00       	callq  e70 <exit>
      if (fd < 0) {
        error("create failed\n");
      }

      memset(buf, '0' + pi, 512);
      for (i = 0; i < 12; i++) {
     509:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     50d:	83 7d fc 0b          	cmpl   $0xb,-0x4(%rbp)
     511:	7e 84                	jle    497 <fourfiles+0x146>
        if ((n = write(fd, buf, 500)) != 500) {
          error("write failed %d\n", n);
        }
      }
      exit();
     513:	e8 58 09 00 00       	callq  e70 <exit>
  char *names[] = {"f0", "f1", "f2", "f3"};
  char *fname;

  printf(1, "fourfiles test\n");

  for (pi = 0; pi < 4; pi++) {
     518:	83 45 f0 01          	addl   $0x1,-0x10(%rbp)
     51c:	83 7d f0 03          	cmpl   $0x3,-0x10(%rbp)
     520:	0f 8e 73 fe ff ff    	jle    399 <fourfiles+0x48>
      }
      exit();
    }
  }

  for (pi = 0; pi < 4; pi++) {
     526:	c7 45 f0 00 00 00 00 	movl   $0x0,-0x10(%rbp)
     52d:	eb 09                	jmp    538 <fourfiles+0x1e7>
    wait();
     52f:	e8 44 09 00 00       	callq  e78 <wait>
      }
      exit();
    }
  }

  for (pi = 0; pi < 4; pi++) {
     534:	83 45 f0 01          	addl   $0x1,-0x10(%rbp)
     538:	83 7d f0 03          	cmpl   $0x3,-0x10(%rbp)
     53c:	7e f1                	jle    52f <fourfiles+0x1de>
    wait();
  }

  for (i = 0; i < 4; i++) {
     53e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     545:	e9 57 01 00 00       	jmpq   6a1 <fourfiles+0x350>
    fname = names[i];
     54a:	8b 45 fc             	mov    -0x4(%rbp),%eax
     54d:	48 98                	cltq   
     54f:	48 8b 44 c5 b8       	mov    -0x48(%rbp,%rax,8),%rax
     554:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
    fd = open(fname, 0);
     558:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     55c:	be 00 00 00 00       	mov    $0x0,%esi
     561:	48 89 c7             	mov    %rax,%rdi
     564:	e8 47 09 00 00       	callq  eb0 <open>
     569:	89 45 e0             	mov    %eax,-0x20(%rbp)
    total = 0;
     56c:	c7 45 f4 00 00 00 00 	movl   $0x0,-0xc(%rbp)
    while ((n = read(fd, buf, sizeof(buf))) > 0) {
     573:	e9 9f 00 00 00       	jmpq   617 <fourfiles+0x2c6>
      for (j = 0; j < n; j++) {
     578:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
     57f:	e9 81 00 00 00       	jmpq   605 <fourfiles+0x2b4>
        if (buf[j] != '0' + i) {
     584:	8b 45 f8             	mov    -0x8(%rbp),%eax
     587:	48 98                	cltq   
     589:	0f b6 80 20 17 00 00 	movzbl 0x1720(%rax),%eax
     590:	0f be c0             	movsbl %al,%eax
     593:	8b 55 fc             	mov    -0x4(%rbp),%edx
     596:	83 c2 30             	add    $0x30,%edx
     599:	39 d0                	cmp    %edx,%eax
     59b:	74 64                	je     601 <fourfiles+0x2b0>
          error("wrong char, was %d should be %d\n", buf[j], '0' + i);
     59d:	8b 05 fd 10 00 00    	mov    0x10fd(%rip),%eax        # 16a0 <stdout>
     5a3:	ba 7e 00 00 00       	mov    $0x7e,%edx
     5a8:	be ff 11 00 00       	mov    $0x11ff,%esi
     5ad:	89 c7                	mov    %eax,%edi
     5af:	b8 00 00 00 00       	mov    $0x0,%eax
     5b4:	e8 14 03 00 00       	callq  8cd <printf>
     5b9:	8b 45 fc             	mov    -0x4(%rbp),%eax
     5bc:	8d 48 30             	lea    0x30(%rax),%ecx
     5bf:	8b 45 f8             	mov    -0x8(%rbp),%eax
     5c2:	48 98                	cltq   
     5c4:	0f b6 80 20 17 00 00 	movzbl 0x1720(%rax),%eax
     5cb:	0f be d0             	movsbl %al,%edx
     5ce:	8b 05 cc 10 00 00    	mov    0x10cc(%rip),%eax        # 16a0 <stdout>
     5d4:	be 78 13 00 00       	mov    $0x1378,%esi
     5d9:	89 c7                	mov    %eax,%edi
     5db:	b8 00 00 00 00       	mov    $0x0,%eax
     5e0:	e8 e8 02 00 00       	callq  8cd <printf>
     5e5:	8b 05 b5 10 00 00    	mov    0x10b5(%rip),%eax        # 16a0 <stdout>
     5eb:	be 50 12 00 00       	mov    $0x1250,%esi
     5f0:	89 c7                	mov    %eax,%edi
     5f2:	b8 00 00 00 00       	mov    $0x0,%eax
     5f7:	e8 d1 02 00 00       	callq  8cd <printf>
     5fc:	e8 6f 08 00 00       	callq  e70 <exit>
  for (i = 0; i < 4; i++) {
    fname = names[i];
    fd = open(fname, 0);
    total = 0;
    while ((n = read(fd, buf, sizeof(buf))) > 0) {
      for (j = 0; j < n; j++) {
     601:	83 45 f8 01          	addl   $0x1,-0x8(%rbp)
     605:	8b 45 f8             	mov    -0x8(%rbp),%eax
     608:	3b 45 dc             	cmp    -0x24(%rbp),%eax
     60b:	0f 8c 73 ff ff ff    	jl     584 <fourfiles+0x233>
        if (buf[j] != '0' + i) {
          error("wrong char, was %d should be %d\n", buf[j], '0' + i);
        }
      }
      total += n;
     611:	8b 45 dc             	mov    -0x24(%rbp),%eax
     614:	01 45 f4             	add    %eax,-0xc(%rbp)

  for (i = 0; i < 4; i++) {
    fname = names[i];
    fd = open(fname, 0);
    total = 0;
    while ((n = read(fd, buf, sizeof(buf))) > 0) {
     617:	8b 45 e0             	mov    -0x20(%rbp),%eax
     61a:	ba 00 20 00 00       	mov    $0x2000,%edx
     61f:	be 20 17 00 00       	mov    $0x1720,%esi
     624:	89 c7                	mov    %eax,%edi
     626:	e8 5d 08 00 00       	callq  e88 <read>
     62b:	89 45 dc             	mov    %eax,-0x24(%rbp)
     62e:	83 7d dc 00          	cmpl   $0x0,-0x24(%rbp)
     632:	0f 8f 40 ff ff ff    	jg     578 <fourfiles+0x227>
          error("wrong char, was %d should be %d\n", buf[j], '0' + i);
        }
      }
      total += n;
    }
    close(fd);
     638:	8b 45 e0             	mov    -0x20(%rbp),%eax
     63b:	89 c7                	mov    %eax,%edi
     63d:	e8 56 08 00 00       	callq  e98 <close>
    if (total != 12 * 500) {
     642:	81 7d f4 70 17 00 00 	cmpl   $0x1770,-0xc(%rbp)
     649:	74 52                	je     69d <fourfiles+0x34c>
      error("wrong length %d\n", total);
     64b:	8b 05 4f 10 00 00    	mov    0x104f(%rip),%eax        # 16a0 <stdout>
     651:	ba 85 00 00 00       	mov    $0x85,%edx
     656:	be ff 11 00 00       	mov    $0x11ff,%esi
     65b:	89 c7                	mov    %eax,%edi
     65d:	b8 00 00 00 00       	mov    $0x0,%eax
     662:	e8 66 02 00 00       	callq  8cd <printf>
     667:	8b 05 33 10 00 00    	mov    0x1033(%rip),%eax        # 16a0 <stdout>
     66d:	8b 55 f4             	mov    -0xc(%rbp),%edx
     670:	be 99 13 00 00       	mov    $0x1399,%esi
     675:	89 c7                	mov    %eax,%edi
     677:	b8 00 00 00 00       	mov    $0x0,%eax
     67c:	e8 4c 02 00 00       	callq  8cd <printf>
     681:	8b 05 19 10 00 00    	mov    0x1019(%rip),%eax        # 16a0 <stdout>
     687:	be 50 12 00 00       	mov    $0x1250,%esi
     68c:	89 c7                	mov    %eax,%edi
     68e:	b8 00 00 00 00       	mov    $0x0,%eax
     693:	e8 35 02 00 00       	callq  8cd <printf>
     698:	e8 d3 07 00 00       	callq  e70 <exit>

  for (pi = 0; pi < 4; pi++) {
    wait();
  }

  for (i = 0; i < 4; i++) {
     69d:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     6a1:	83 7d fc 03          	cmpl   $0x3,-0x4(%rbp)
     6a5:	0f 8e 9f fe ff ff    	jle    54a <fourfiles+0x1f9>
    if (total != 12 * 500) {
      error("wrong length %d\n", total);
    }
  }

  printf(1, "fourfiles ok\n");
     6ab:	be aa 13 00 00       	mov    $0x13aa,%esi
     6b0:	bf 01 00 00 00       	mov    $0x1,%edi
     6b5:	b8 00 00 00 00       	mov    $0x0,%eax
     6ba:	e8 0e 02 00 00       	callq  8cd <printf>
}
     6bf:	90                   	nop
     6c0:	c9                   	leaveq 
     6c1:	c3                   	retq   

00000000000006c2 <main>:

int main(int argc, char *argv[]) {
     6c2:	55                   	push   %rbp
     6c3:	48 89 e5             	mov    %rsp,%rbp
     6c6:	48 83 ec 10          	sub    $0x10,%rsp
     6ca:	89 7d fc             	mov    %edi,-0x4(%rbp)
     6cd:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  printf(stdout, "lab5test_a starting\n");
     6d1:	8b 05 c9 0f 00 00    	mov    0xfc9(%rip),%eax        # 16a0 <stdout>
     6d7:	be b8 13 00 00       	mov    $0x13b8,%esi
     6dc:	89 c7                	mov    %eax,%edi
     6de:	b8 00 00 00 00       	mov    $0x0,%eax
     6e3:	e8 e5 01 00 00       	callq  8cd <printf>
  modification();
     6e8:	e8 13 f9 ff ff       	callq  0 <modification>
  onefile();
     6ed:	e8 1d fa ff ff       	callq  10f <onefile>
  fourfiles();
     6f2:	e8 5a fc ff ff       	callq  351 <fourfiles>

  printf(stdout, "lab5test_a passed!\n");
     6f7:	8b 05 a3 0f 00 00    	mov    0xfa3(%rip),%eax        # 16a0 <stdout>
     6fd:	be cd 13 00 00       	mov    $0x13cd,%esi
     702:	89 c7                	mov    %eax,%edi
     704:	b8 00 00 00 00       	mov    $0x0,%eax
     709:	e8 bf 01 00 00       	callq  8cd <printf>
  exit();
     70e:	e8 5d 07 00 00       	callq  e70 <exit>

0000000000000713 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
     713:	55                   	push   %rbp
     714:	48 89 e5             	mov    %rsp,%rbp
     717:	48 83 ec 10          	sub    $0x10,%rsp
     71b:	89 7d fc             	mov    %edi,-0x4(%rbp)
     71e:	89 f0                	mov    %esi,%eax
     720:	88 45 f8             	mov    %al,-0x8(%rbp)
     723:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
     727:	8b 45 fc             	mov    -0x4(%rbp),%eax
     72a:	ba 01 00 00 00       	mov    $0x1,%edx
     72f:	48 89 ce             	mov    %rcx,%rsi
     732:	89 c7                	mov    %eax,%edi
     734:	e8 57 07 00 00       	callq  e90 <write>
     739:	90                   	nop
     73a:	c9                   	leaveq 
     73b:	c3                   	retq   

000000000000073c <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
     73c:	55                   	push   %rbp
     73d:	48 89 e5             	mov    %rsp,%rbp
     740:	48 83 ec 40          	sub    $0x40,%rsp
     744:	89 7d cc             	mov    %edi,-0x34(%rbp)
     747:	89 75 c8             	mov    %esi,-0x38(%rbp)
     74a:	89 55 c4             	mov    %edx,-0x3c(%rbp)
     74d:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
     750:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     754:	74 1f                	je     775 <printint64+0x39>
     756:	8b 45 c8             	mov    -0x38(%rbp),%eax
     759:	c1 e8 1f             	shr    $0x1f,%eax
     75c:	0f b6 c0             	movzbl %al,%eax
     75f:	89 45 c0             	mov    %eax,-0x40(%rbp)
     762:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     766:	74 0d                	je     775 <printint64+0x39>
    x = -xx;
     768:	8b 45 c8             	mov    -0x38(%rbp),%eax
     76b:	f7 d8                	neg    %eax
     76d:	48 98                	cltq   
     76f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     773:	eb 09                	jmp    77e <printint64+0x42>
  else
    x = xx;
     775:	8b 45 c8             	mov    -0x38(%rbp),%eax
     778:	48 98                	cltq   
     77a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
     77e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     785:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     788:	8d 41 01             	lea    0x1(%rcx),%eax
     78b:	89 45 fc             	mov    %eax,-0x4(%rbp)
     78e:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     791:	48 63 f0             	movslq %eax,%rsi
     794:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     798:	ba 00 00 00 00       	mov    $0x0,%edx
     79d:	48 f7 f6             	div    %rsi
     7a0:	48 89 d0             	mov    %rdx,%rax
     7a3:	0f b6 90 b0 16 00 00 	movzbl 0x16b0(%rax),%edx
     7aa:	48 63 c1             	movslq %ecx,%rax
     7ad:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
     7b1:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     7b4:	48 63 f8             	movslq %eax,%rdi
     7b7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     7bb:	ba 00 00 00 00       	mov    $0x0,%edx
     7c0:	48 f7 f7             	div    %rdi
     7c3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     7c7:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     7cc:	75 b7                	jne    785 <printint64+0x49>

  if (sgn)
     7ce:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     7d2:	74 2b                	je     7ff <printint64+0xc3>
    buf[i++] = '-';
     7d4:	8b 45 fc             	mov    -0x4(%rbp),%eax
     7d7:	8d 50 01             	lea    0x1(%rax),%edx
     7da:	89 55 fc             	mov    %edx,-0x4(%rbp)
     7dd:	48 98                	cltq   
     7df:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
     7e4:	eb 19                	jmp    7ff <printint64+0xc3>
    putc(fd, buf[i]);
     7e6:	8b 45 fc             	mov    -0x4(%rbp),%eax
     7e9:	48 98                	cltq   
     7eb:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
     7f0:	0f be d0             	movsbl %al,%edx
     7f3:	8b 45 cc             	mov    -0x34(%rbp),%eax
     7f6:	89 d6                	mov    %edx,%esi
     7f8:	89 c7                	mov    %eax,%edi
     7fa:	e8 14 ff ff ff       	callq  713 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
     7ff:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     803:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     807:	79 dd                	jns    7e6 <printint64+0xaa>
    putc(fd, buf[i]);
}
     809:	90                   	nop
     80a:	c9                   	leaveq 
     80b:	c3                   	retq   

000000000000080c <printint>:

static void printint(int fd, int xx, int base, int sgn) {
     80c:	55                   	push   %rbp
     80d:	48 89 e5             	mov    %rsp,%rbp
     810:	48 83 ec 30          	sub    $0x30,%rsp
     814:	89 7d dc             	mov    %edi,-0x24(%rbp)
     817:	89 75 d8             	mov    %esi,-0x28(%rbp)
     81a:	89 55 d4             	mov    %edx,-0x2c(%rbp)
     81d:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
     820:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
     827:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
     82b:	74 17                	je     844 <printint+0x38>
     82d:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
     831:	79 11                	jns    844 <printint+0x38>
    neg = 1;
     833:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
     83a:	8b 45 d8             	mov    -0x28(%rbp),%eax
     83d:	f7 d8                	neg    %eax
     83f:	89 45 f4             	mov    %eax,-0xc(%rbp)
     842:	eb 06                	jmp    84a <printint+0x3e>
  } else {
    x = xx;
     844:	8b 45 d8             	mov    -0x28(%rbp),%eax
     847:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
     84a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     851:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     854:	8d 41 01             	lea    0x1(%rcx),%eax
     857:	89 45 fc             	mov    %eax,-0x4(%rbp)
     85a:	8b 75 d4             	mov    -0x2c(%rbp),%esi
     85d:	8b 45 f4             	mov    -0xc(%rbp),%eax
     860:	ba 00 00 00 00       	mov    $0x0,%edx
     865:	f7 f6                	div    %esi
     867:	89 d0                	mov    %edx,%eax
     869:	89 c0                	mov    %eax,%eax
     86b:	0f b6 90 d0 16 00 00 	movzbl 0x16d0(%rax),%edx
     872:	48 63 c1             	movslq %ecx,%rax
     875:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
     879:	8b 7d d4             	mov    -0x2c(%rbp),%edi
     87c:	8b 45 f4             	mov    -0xc(%rbp),%eax
     87f:	ba 00 00 00 00       	mov    $0x0,%edx
     884:	f7 f7                	div    %edi
     886:	89 45 f4             	mov    %eax,-0xc(%rbp)
     889:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     88d:	75 c2                	jne    851 <printint+0x45>
  if (neg)
     88f:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     893:	74 2b                	je     8c0 <printint+0xb4>
    buf[i++] = '-';
     895:	8b 45 fc             	mov    -0x4(%rbp),%eax
     898:	8d 50 01             	lea    0x1(%rax),%edx
     89b:	89 55 fc             	mov    %edx,-0x4(%rbp)
     89e:	48 98                	cltq   
     8a0:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
     8a5:	eb 19                	jmp    8c0 <printint+0xb4>
    putc(fd, buf[i]);
     8a7:	8b 45 fc             	mov    -0x4(%rbp),%eax
     8aa:	48 98                	cltq   
     8ac:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
     8b1:	0f be d0             	movsbl %al,%edx
     8b4:	8b 45 dc             	mov    -0x24(%rbp),%eax
     8b7:	89 d6                	mov    %edx,%esi
     8b9:	89 c7                	mov    %eax,%edi
     8bb:	e8 53 fe ff ff       	callq  713 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
     8c0:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     8c4:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     8c8:	79 dd                	jns    8a7 <printint+0x9b>
    putc(fd, buf[i]);
}
     8ca:	90                   	nop
     8cb:	c9                   	leaveq 
     8cc:	c3                   	retq   

00000000000008cd <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
     8cd:	55                   	push   %rbp
     8ce:	48 89 e5             	mov    %rsp,%rbp
     8d1:	48 83 ec 70          	sub    $0x70,%rsp
     8d5:	89 7d 9c             	mov    %edi,-0x64(%rbp)
     8d8:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
     8dc:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
     8e0:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
     8e4:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
     8e8:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
     8ec:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
     8f3:	48 8d 45 10          	lea    0x10(%rbp),%rax
     8f7:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
     8fb:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
     8ff:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
     903:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
     90a:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
     911:	e9 68 02 00 00       	jmpq   b7e <printf+0x2b1>
    c = fmt[i] & 0xff;
     916:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     919:	48 63 d0             	movslq %eax,%rdx
     91c:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     920:	48 01 d0             	add    %rdx,%rax
     923:	0f b6 00             	movzbl (%rax),%eax
     926:	0f be c0             	movsbl %al,%eax
     929:	25 ff 00 00 00       	and    $0xff,%eax
     92e:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
     931:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     935:	75 30                	jne    967 <printf+0x9a>
      if (c == '%') {
     937:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     93b:	75 13                	jne    950 <printf+0x83>
        state = '%';
     93d:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
     944:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
     94b:	e9 2a 02 00 00       	jmpq   b7a <printf+0x2ad>
      } else {
        putc(fd, c);
     950:	8b 45 b8             	mov    -0x48(%rbp),%eax
     953:	0f be d0             	movsbl %al,%edx
     956:	8b 45 9c             	mov    -0x64(%rbp),%eax
     959:	89 d6                	mov    %edx,%esi
     95b:	89 c7                	mov    %eax,%edi
     95d:	e8 b1 fd ff ff       	callq  713 <putc>
     962:	e9 13 02 00 00       	jmpq   b7a <printf+0x2ad>
      }
    } else if (state == '%') {
     967:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
     96b:	0f 85 09 02 00 00    	jne    b7a <printf+0x2ad>
      if (c == 'l') {
     971:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
     975:	75 0c                	jne    983 <printf+0xb6>
        lflag = 1;
     977:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
     97e:	e9 f7 01 00 00       	jmpq   b7a <printf+0x2ad>
      } else if (c == 'd') {
     983:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
     987:	0f 85 95 00 00 00    	jne    a22 <printf+0x155>
        if (lflag == 1)
     98d:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
     991:	75 49                	jne    9dc <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
     993:	8b 45 a0             	mov    -0x60(%rbp),%eax
     996:	83 f8 30             	cmp    $0x30,%eax
     999:	73 17                	jae    9b2 <printf+0xe5>
     99b:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     99f:	8b 55 a0             	mov    -0x60(%rbp),%edx
     9a2:	89 d2                	mov    %edx,%edx
     9a4:	48 01 d0             	add    %rdx,%rax
     9a7:	8b 55 a0             	mov    -0x60(%rbp),%edx
     9aa:	83 c2 08             	add    $0x8,%edx
     9ad:	89 55 a0             	mov    %edx,-0x60(%rbp)
     9b0:	eb 0c                	jmp    9be <printf+0xf1>
     9b2:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     9b6:	48 8d 50 08          	lea    0x8(%rax),%rdx
     9ba:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     9be:	48 8b 00             	mov    (%rax),%rax
     9c1:	89 c6                	mov    %eax,%esi
     9c3:	8b 45 9c             	mov    -0x64(%rbp),%eax
     9c6:	b9 01 00 00 00       	mov    $0x1,%ecx
     9cb:	ba 0a 00 00 00       	mov    $0xa,%edx
     9d0:	89 c7                	mov    %eax,%edi
     9d2:	e8 65 fd ff ff       	callq  73c <printint64>
     9d7:	e9 97 01 00 00       	jmpq   b73 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
     9dc:	8b 45 a0             	mov    -0x60(%rbp),%eax
     9df:	83 f8 30             	cmp    $0x30,%eax
     9e2:	73 17                	jae    9fb <printf+0x12e>
     9e4:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     9e8:	8b 55 a0             	mov    -0x60(%rbp),%edx
     9eb:	89 d2                	mov    %edx,%edx
     9ed:	48 01 d0             	add    %rdx,%rax
     9f0:	8b 55 a0             	mov    -0x60(%rbp),%edx
     9f3:	83 c2 08             	add    $0x8,%edx
     9f6:	89 55 a0             	mov    %edx,-0x60(%rbp)
     9f9:	eb 0c                	jmp    a07 <printf+0x13a>
     9fb:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     9ff:	48 8d 50 08          	lea    0x8(%rax),%rdx
     a03:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     a07:	8b 30                	mov    (%rax),%esi
     a09:	8b 45 9c             	mov    -0x64(%rbp),%eax
     a0c:	b9 01 00 00 00       	mov    $0x1,%ecx
     a11:	ba 0a 00 00 00       	mov    $0xa,%edx
     a16:	89 c7                	mov    %eax,%edi
     a18:	e8 ef fd ff ff       	callq  80c <printint>
     a1d:	e9 51 01 00 00       	jmpq   b73 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
     a22:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
     a26:	74 0a                	je     a32 <printf+0x165>
     a28:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
     a2c:	0f 85 95 00 00 00    	jne    ac7 <printf+0x1fa>
        if (lflag == 1)
     a32:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
     a36:	75 49                	jne    a81 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
     a38:	8b 45 a0             	mov    -0x60(%rbp),%eax
     a3b:	83 f8 30             	cmp    $0x30,%eax
     a3e:	73 17                	jae    a57 <printf+0x18a>
     a40:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     a44:	8b 55 a0             	mov    -0x60(%rbp),%edx
     a47:	89 d2                	mov    %edx,%edx
     a49:	48 01 d0             	add    %rdx,%rax
     a4c:	8b 55 a0             	mov    -0x60(%rbp),%edx
     a4f:	83 c2 08             	add    $0x8,%edx
     a52:	89 55 a0             	mov    %edx,-0x60(%rbp)
     a55:	eb 0c                	jmp    a63 <printf+0x196>
     a57:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     a5b:	48 8d 50 08          	lea    0x8(%rax),%rdx
     a5f:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     a63:	48 8b 00             	mov    (%rax),%rax
     a66:	89 c6                	mov    %eax,%esi
     a68:	8b 45 9c             	mov    -0x64(%rbp),%eax
     a6b:	b9 00 00 00 00       	mov    $0x0,%ecx
     a70:	ba 10 00 00 00       	mov    $0x10,%edx
     a75:	89 c7                	mov    %eax,%edi
     a77:	e8 c0 fc ff ff       	callq  73c <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
     a7c:	e9 f2 00 00 00       	jmpq   b73 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
     a81:	8b 45 a0             	mov    -0x60(%rbp),%eax
     a84:	83 f8 30             	cmp    $0x30,%eax
     a87:	73 17                	jae    aa0 <printf+0x1d3>
     a89:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     a8d:	8b 55 a0             	mov    -0x60(%rbp),%edx
     a90:	89 d2                	mov    %edx,%edx
     a92:	48 01 d0             	add    %rdx,%rax
     a95:	8b 55 a0             	mov    -0x60(%rbp),%edx
     a98:	83 c2 08             	add    $0x8,%edx
     a9b:	89 55 a0             	mov    %edx,-0x60(%rbp)
     a9e:	eb 0c                	jmp    aac <printf+0x1df>
     aa0:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     aa4:	48 8d 50 08          	lea    0x8(%rax),%rdx
     aa8:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     aac:	8b 30                	mov    (%rax),%esi
     aae:	8b 45 9c             	mov    -0x64(%rbp),%eax
     ab1:	b9 00 00 00 00       	mov    $0x0,%ecx
     ab6:	ba 10 00 00 00       	mov    $0x10,%edx
     abb:	89 c7                	mov    %eax,%edi
     abd:	e8 4a fd ff ff       	callq  80c <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
     ac2:	e9 ac 00 00 00       	jmpq   b73 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
     ac7:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
     acb:	75 6b                	jne    b38 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
     acd:	8b 45 a0             	mov    -0x60(%rbp),%eax
     ad0:	83 f8 30             	cmp    $0x30,%eax
     ad3:	73 17                	jae    aec <printf+0x21f>
     ad5:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
     ad9:	8b 55 a0             	mov    -0x60(%rbp),%edx
     adc:	89 d2                	mov    %edx,%edx
     ade:	48 01 d0             	add    %rdx,%rax
     ae1:	8b 55 a0             	mov    -0x60(%rbp),%edx
     ae4:	83 c2 08             	add    $0x8,%edx
     ae7:	89 55 a0             	mov    %edx,-0x60(%rbp)
     aea:	eb 0c                	jmp    af8 <printf+0x22b>
     aec:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
     af0:	48 8d 50 08          	lea    0x8(%rax),%rdx
     af4:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
     af8:	48 8b 00             	mov    (%rax),%rax
     afb:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
     aff:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
     b04:	75 25                	jne    b2b <printf+0x25e>
          s = "(null)";
     b06:	48 c7 45 c8 e1 13 00 	movq   $0x13e1,-0x38(%rbp)
     b0d:	00 
        for (; *s; s++)
     b0e:	eb 1b                	jmp    b2b <printf+0x25e>
          putc(fd, *s);
     b10:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     b14:	0f b6 00             	movzbl (%rax),%eax
     b17:	0f be d0             	movsbl %al,%edx
     b1a:	8b 45 9c             	mov    -0x64(%rbp),%eax
     b1d:	89 d6                	mov    %edx,%esi
     b1f:	89 c7                	mov    %eax,%edi
     b21:	e8 ed fb ff ff       	callq  713 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
     b26:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
     b2b:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     b2f:	0f b6 00             	movzbl (%rax),%eax
     b32:	84 c0                	test   %al,%al
     b34:	75 da                	jne    b10 <printf+0x243>
     b36:	eb 3b                	jmp    b73 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
     b38:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     b3c:	75 14                	jne    b52 <printf+0x285>
        putc(fd, c);
     b3e:	8b 45 b8             	mov    -0x48(%rbp),%eax
     b41:	0f be d0             	movsbl %al,%edx
     b44:	8b 45 9c             	mov    -0x64(%rbp),%eax
     b47:	89 d6                	mov    %edx,%esi
     b49:	89 c7                	mov    %eax,%edi
     b4b:	e8 c3 fb ff ff       	callq  713 <putc>
     b50:	eb 21                	jmp    b73 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
     b52:	8b 45 9c             	mov    -0x64(%rbp),%eax
     b55:	be 25 00 00 00       	mov    $0x25,%esi
     b5a:	89 c7                	mov    %eax,%edi
     b5c:	e8 b2 fb ff ff       	callq  713 <putc>
        putc(fd, c);
     b61:	8b 45 b8             	mov    -0x48(%rbp),%eax
     b64:	0f be d0             	movsbl %al,%edx
     b67:	8b 45 9c             	mov    -0x64(%rbp),%eax
     b6a:	89 d6                	mov    %edx,%esi
     b6c:	89 c7                	mov    %eax,%edi
     b6e:	e8 a0 fb ff ff       	callq  713 <putc>
      }
      state = 0;
     b73:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
     b7a:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
     b7e:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     b81:	48 63 d0             	movslq %eax,%rdx
     b84:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     b88:	48 01 d0             	add    %rdx,%rax
     b8b:	0f b6 00             	movzbl (%rax),%eax
     b8e:	84 c0                	test   %al,%al
     b90:	0f 85 80 fd ff ff    	jne    916 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
     b96:	90                   	nop
     b97:	c9                   	leaveq 
     b98:	c3                   	retq   

0000000000000b99 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
     b99:	55                   	push   %rbp
     b9a:	48 89 e5             	mov    %rsp,%rbp
     b9d:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     ba1:	89 75 f4             	mov    %esi,-0xc(%rbp)
     ba4:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
     ba7:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
     bab:	8b 55 f0             	mov    -0x10(%rbp),%edx
     bae:	8b 45 f4             	mov    -0xc(%rbp),%eax
     bb1:	48 89 ce             	mov    %rcx,%rsi
     bb4:	48 89 f7             	mov    %rsi,%rdi
     bb7:	89 d1                	mov    %edx,%ecx
     bb9:	fc                   	cld    
     bba:	f3 aa                	rep stos %al,%es:(%rdi)
     bbc:	89 ca                	mov    %ecx,%edx
     bbe:	48 89 fe             	mov    %rdi,%rsi
     bc1:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
     bc5:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
     bc8:	90                   	nop
     bc9:	5d                   	pop    %rbp
     bca:	c3                   	retq   

0000000000000bcb <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
     bcb:	55                   	push   %rbp
     bcc:	48 89 e5             	mov    %rsp,%rbp
     bcf:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     bd3:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
     bd7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     bdb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
     bdf:	90                   	nop
     be0:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     be4:	48 8d 50 01          	lea    0x1(%rax),%rdx
     be8:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
     bec:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     bf0:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
     bf4:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
     bf8:	0f b6 12             	movzbl (%rdx),%edx
     bfb:	88 10                	mov    %dl,(%rax)
     bfd:	0f b6 00             	movzbl (%rax),%eax
     c00:	84 c0                	test   %al,%al
     c02:	75 dc                	jne    be0 <strcpy+0x15>
    ;
  return os;
     c04:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     c08:	5d                   	pop    %rbp
     c09:	c3                   	retq   

0000000000000c0a <strcmp>:

int strcmp(const char *p, const char *q) {
     c0a:	55                   	push   %rbp
     c0b:	48 89 e5             	mov    %rsp,%rbp
     c0e:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     c12:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
     c16:	eb 0a                	jmp    c22 <strcmp+0x18>
    p++, q++;
     c18:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
     c1d:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
     c22:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     c26:	0f b6 00             	movzbl (%rax),%eax
     c29:	84 c0                	test   %al,%al
     c2b:	74 12                	je     c3f <strcmp+0x35>
     c2d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     c31:	0f b6 10             	movzbl (%rax),%edx
     c34:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     c38:	0f b6 00             	movzbl (%rax),%eax
     c3b:	38 c2                	cmp    %al,%dl
     c3d:	74 d9                	je     c18 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
     c3f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     c43:	0f b6 00             	movzbl (%rax),%eax
     c46:	0f b6 d0             	movzbl %al,%edx
     c49:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     c4d:	0f b6 00             	movzbl (%rax),%eax
     c50:	0f b6 c0             	movzbl %al,%eax
     c53:	29 c2                	sub    %eax,%edx
     c55:	89 d0                	mov    %edx,%eax
}
     c57:	5d                   	pop    %rbp
     c58:	c3                   	retq   

0000000000000c59 <strlen>:

uint strlen(char *s) {
     c59:	55                   	push   %rbp
     c5a:	48 89 e5             	mov    %rsp,%rbp
     c5d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
     c61:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     c68:	eb 04                	jmp    c6e <strlen+0x15>
     c6a:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     c6e:	8b 45 fc             	mov    -0x4(%rbp),%eax
     c71:	48 63 d0             	movslq %eax,%rdx
     c74:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c78:	48 01 d0             	add    %rdx,%rax
     c7b:	0f b6 00             	movzbl (%rax),%eax
     c7e:	84 c0                	test   %al,%al
     c80:	75 e8                	jne    c6a <strlen+0x11>
    ;
  return n;
     c82:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     c85:	5d                   	pop    %rbp
     c86:	c3                   	retq   

0000000000000c87 <memset>:

void *memset(void *dst, int c, uint n) {
     c87:	55                   	push   %rbp
     c88:	48 89 e5             	mov    %rsp,%rbp
     c8b:	48 83 ec 10          	sub    $0x10,%rsp
     c8f:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     c93:	89 75 f4             	mov    %esi,-0xc(%rbp)
     c96:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
     c99:	8b 55 f0             	mov    -0x10(%rbp),%edx
     c9c:	8b 4d f4             	mov    -0xc(%rbp),%ecx
     c9f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ca3:	89 ce                	mov    %ecx,%esi
     ca5:	48 89 c7             	mov    %rax,%rdi
     ca8:	e8 ec fe ff ff       	callq  b99 <stosb>
  return dst;
     cad:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     cb1:	c9                   	leaveq 
     cb2:	c3                   	retq   

0000000000000cb3 <strchr>:

char *strchr(const char *s, char c) {
     cb3:	55                   	push   %rbp
     cb4:	48 89 e5             	mov    %rsp,%rbp
     cb7:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     cbb:	89 f0                	mov    %esi,%eax
     cbd:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
     cc0:	eb 17                	jmp    cd9 <strchr+0x26>
    if (*s == c)
     cc2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     cc6:	0f b6 00             	movzbl (%rax),%eax
     cc9:	3a 45 f4             	cmp    -0xc(%rbp),%al
     ccc:	75 06                	jne    cd4 <strchr+0x21>
      return (char *)s;
     cce:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     cd2:	eb 15                	jmp    ce9 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
     cd4:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
     cd9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     cdd:	0f b6 00             	movzbl (%rax),%eax
     ce0:	84 c0                	test   %al,%al
     ce2:	75 de                	jne    cc2 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
     ce4:	b8 00 00 00 00       	mov    $0x0,%eax
}
     ce9:	5d                   	pop    %rbp
     cea:	c3                   	retq   

0000000000000ceb <gets>:

char *gets(char *buf, int max) {
     ceb:	55                   	push   %rbp
     cec:	48 89 e5             	mov    %rsp,%rbp
     cef:	48 83 ec 20          	sub    $0x20,%rsp
     cf3:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     cf7:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
     cfa:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     d01:	eb 48                	jmp    d4b <gets+0x60>
    cc = read(0, &c, 1);
     d03:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
     d07:	ba 01 00 00 00       	mov    $0x1,%edx
     d0c:	48 89 c6             	mov    %rax,%rsi
     d0f:	bf 00 00 00 00       	mov    $0x0,%edi
     d14:	e8 6f 01 00 00       	callq  e88 <read>
     d19:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
     d1c:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     d20:	7e 36                	jle    d58 <gets+0x6d>
      break;
    buf[i++] = c;
     d22:	8b 45 fc             	mov    -0x4(%rbp),%eax
     d25:	8d 50 01             	lea    0x1(%rax),%edx
     d28:	89 55 fc             	mov    %edx,-0x4(%rbp)
     d2b:	48 63 d0             	movslq %eax,%rdx
     d2e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d32:	48 01 c2             	add    %rax,%rdx
     d35:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     d39:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
     d3b:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     d3f:	3c 0a                	cmp    $0xa,%al
     d41:	74 16                	je     d59 <gets+0x6e>
     d43:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
     d47:	3c 0d                	cmp    $0xd,%al
     d49:	74 0e                	je     d59 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
     d4b:	8b 45 fc             	mov    -0x4(%rbp),%eax
     d4e:	83 c0 01             	add    $0x1,%eax
     d51:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
     d54:	7c ad                	jl     d03 <gets+0x18>
     d56:	eb 01                	jmp    d59 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
     d58:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
     d59:	8b 45 fc             	mov    -0x4(%rbp),%eax
     d5c:	48 63 d0             	movslq %eax,%rdx
     d5f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d63:	48 01 d0             	add    %rdx,%rax
     d66:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
     d69:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
     d6d:	c9                   	leaveq 
     d6e:	c3                   	retq   

0000000000000d6f <stat>:

int stat(char *n, struct stat *st) {
     d6f:	55                   	push   %rbp
     d70:	48 89 e5             	mov    %rsp,%rbp
     d73:	48 83 ec 20          	sub    $0x20,%rsp
     d77:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     d7b:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
     d7f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d83:	be 00 00 00 00       	mov    $0x0,%esi
     d88:	48 89 c7             	mov    %rax,%rdi
     d8b:	e8 20 01 00 00       	callq  eb0 <open>
     d90:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
     d93:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     d97:	79 07                	jns    da0 <stat+0x31>
    return -1;
     d99:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
     d9e:	eb 21                	jmp    dc1 <stat+0x52>
  r = fstat(fd, st);
     da0:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     da4:	8b 45 fc             	mov    -0x4(%rbp),%eax
     da7:	48 89 d6             	mov    %rdx,%rsi
     daa:	89 c7                	mov    %eax,%edi
     dac:	e8 17 01 00 00       	callq  ec8 <fstat>
     db1:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
     db4:	8b 45 fc             	mov    -0x4(%rbp),%eax
     db7:	89 c7                	mov    %eax,%edi
     db9:	e8 da 00 00 00       	callq  e98 <close>
  return r;
     dbe:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
     dc1:	c9                   	leaveq 
     dc2:	c3                   	retq   

0000000000000dc3 <atoi>:

int atoi(const char *s) {
     dc3:	55                   	push   %rbp
     dc4:	48 89 e5             	mov    %rsp,%rbp
     dc7:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
     dcb:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
     dd2:	eb 28                	jmp    dfc <atoi+0x39>
    n = n * 10 + *s++ - '0';
     dd4:	8b 55 fc             	mov    -0x4(%rbp),%edx
     dd7:	89 d0                	mov    %edx,%eax
     dd9:	c1 e0 02             	shl    $0x2,%eax
     ddc:	01 d0                	add    %edx,%eax
     dde:	01 c0                	add    %eax,%eax
     de0:	89 c1                	mov    %eax,%ecx
     de2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     de6:	48 8d 50 01          	lea    0x1(%rax),%rdx
     dea:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
     dee:	0f b6 00             	movzbl (%rax),%eax
     df1:	0f be c0             	movsbl %al,%eax
     df4:	01 c8                	add    %ecx,%eax
     df6:	83 e8 30             	sub    $0x30,%eax
     df9:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
     dfc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     e00:	0f b6 00             	movzbl (%rax),%eax
     e03:	3c 2f                	cmp    $0x2f,%al
     e05:	7e 0b                	jle    e12 <atoi+0x4f>
     e07:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     e0b:	0f b6 00             	movzbl (%rax),%eax
     e0e:	3c 39                	cmp    $0x39,%al
     e10:	7e c2                	jle    dd4 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
     e12:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     e15:	5d                   	pop    %rbp
     e16:	c3                   	retq   

0000000000000e17 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
     e17:	55                   	push   %rbp
     e18:	48 89 e5             	mov    %rsp,%rbp
     e1b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     e1f:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
     e23:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
     e26:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     e2a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
     e2e:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     e32:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
     e36:	eb 1d                	jmp    e55 <memmove+0x3e>
    *dst++ = *src++;
     e38:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     e3c:	48 8d 50 01          	lea    0x1(%rax),%rdx
     e40:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
     e44:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
     e48:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
     e4c:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
     e50:	0f b6 12             	movzbl (%rdx),%edx
     e53:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
     e55:	8b 45 dc             	mov    -0x24(%rbp),%eax
     e58:	8d 50 ff             	lea    -0x1(%rax),%edx
     e5b:	89 55 dc             	mov    %edx,-0x24(%rbp)
     e5e:	85 c0                	test   %eax,%eax
     e60:	7f d6                	jg     e38 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
     e62:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     e66:	5d                   	pop    %rbp
     e67:	c3                   	retq   

0000000000000e68 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
     e68:	b8 01 00 00 00       	mov    $0x1,%eax
     e6d:	cd 40                	int    $0x40
     e6f:	c3                   	retq   

0000000000000e70 <exit>:
SYSCALL(exit)
     e70:	b8 02 00 00 00       	mov    $0x2,%eax
     e75:	cd 40                	int    $0x40
     e77:	c3                   	retq   

0000000000000e78 <wait>:
SYSCALL(wait)
     e78:	b8 03 00 00 00       	mov    $0x3,%eax
     e7d:	cd 40                	int    $0x40
     e7f:	c3                   	retq   

0000000000000e80 <pipe>:
SYSCALL(pipe)
     e80:	b8 04 00 00 00       	mov    $0x4,%eax
     e85:	cd 40                	int    $0x40
     e87:	c3                   	retq   

0000000000000e88 <read>:
SYSCALL(read)
     e88:	b8 05 00 00 00       	mov    $0x5,%eax
     e8d:	cd 40                	int    $0x40
     e8f:	c3                   	retq   

0000000000000e90 <write>:
SYSCALL(write)
     e90:	b8 10 00 00 00       	mov    $0x10,%eax
     e95:	cd 40                	int    $0x40
     e97:	c3                   	retq   

0000000000000e98 <close>:
SYSCALL(close)
     e98:	b8 15 00 00 00       	mov    $0x15,%eax
     e9d:	cd 40                	int    $0x40
     e9f:	c3                   	retq   

0000000000000ea0 <kill>:
SYSCALL(kill)
     ea0:	b8 06 00 00 00       	mov    $0x6,%eax
     ea5:	cd 40                	int    $0x40
     ea7:	c3                   	retq   

0000000000000ea8 <exec>:
SYSCALL(exec)
     ea8:	b8 07 00 00 00       	mov    $0x7,%eax
     ead:	cd 40                	int    $0x40
     eaf:	c3                   	retq   

0000000000000eb0 <open>:
SYSCALL(open)
     eb0:	b8 0f 00 00 00       	mov    $0xf,%eax
     eb5:	cd 40                	int    $0x40
     eb7:	c3                   	retq   

0000000000000eb8 <mknod>:
SYSCALL(mknod)
     eb8:	b8 11 00 00 00       	mov    $0x11,%eax
     ebd:	cd 40                	int    $0x40
     ebf:	c3                   	retq   

0000000000000ec0 <unlink>:
SYSCALL(unlink)
     ec0:	b8 12 00 00 00       	mov    $0x12,%eax
     ec5:	cd 40                	int    $0x40
     ec7:	c3                   	retq   

0000000000000ec8 <fstat>:
SYSCALL(fstat)
     ec8:	b8 08 00 00 00       	mov    $0x8,%eax
     ecd:	cd 40                	int    $0x40
     ecf:	c3                   	retq   

0000000000000ed0 <link>:
SYSCALL(link)
     ed0:	b8 13 00 00 00       	mov    $0x13,%eax
     ed5:	cd 40                	int    $0x40
     ed7:	c3                   	retq   

0000000000000ed8 <mkdir>:
SYSCALL(mkdir)
     ed8:	b8 14 00 00 00       	mov    $0x14,%eax
     edd:	cd 40                	int    $0x40
     edf:	c3                   	retq   

0000000000000ee0 <chdir>:
SYSCALL(chdir)
     ee0:	b8 09 00 00 00       	mov    $0x9,%eax
     ee5:	cd 40                	int    $0x40
     ee7:	c3                   	retq   

0000000000000ee8 <dup>:
SYSCALL(dup)
     ee8:	b8 0a 00 00 00       	mov    $0xa,%eax
     eed:	cd 40                	int    $0x40
     eef:	c3                   	retq   

0000000000000ef0 <getpid>:
SYSCALL(getpid)
     ef0:	b8 0b 00 00 00       	mov    $0xb,%eax
     ef5:	cd 40                	int    $0x40
     ef7:	c3                   	retq   

0000000000000ef8 <sbrk>:
SYSCALL(sbrk)
     ef8:	b8 0c 00 00 00       	mov    $0xc,%eax
     efd:	cd 40                	int    $0x40
     eff:	c3                   	retq   

0000000000000f00 <sleep>:
SYSCALL(sleep)
     f00:	b8 0d 00 00 00       	mov    $0xd,%eax
     f05:	cd 40                	int    $0x40
     f07:	c3                   	retq   

0000000000000f08 <uptime>:
SYSCALL(uptime)
     f08:	b8 0e 00 00 00       	mov    $0xe,%eax
     f0d:	cd 40                	int    $0x40
     f0f:	c3                   	retq   

0000000000000f10 <sysinfo>:
SYSCALL(sysinfo)
     f10:	b8 16 00 00 00       	mov    $0x16,%eax
     f15:	cd 40                	int    $0x40
     f17:	c3                   	retq   

0000000000000f18 <crashn>:
SYSCALL(crashn)
     f18:	b8 17 00 00 00       	mov    $0x17,%eax
     f1d:	cd 40                	int    $0x40
     f1f:	c3                   	retq   

0000000000000f20 <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
     f20:	55                   	push   %rbp
     f21:	48 89 e5             	mov    %rsp,%rbp
     f24:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
     f28:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     f2c:	48 83 e8 10          	sub    $0x10,%rax
     f30:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
     f34:	48 8b 05 d5 07 00 00 	mov    0x7d5(%rip),%rax        # 1710 <freep>
     f3b:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
     f3f:	eb 2f                	jmp    f70 <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
     f41:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f45:	48 8b 00             	mov    (%rax),%rax
     f48:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     f4c:	77 17                	ja     f65 <free+0x45>
     f4e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f52:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     f56:	77 2f                	ja     f87 <free+0x67>
     f58:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f5c:	48 8b 00             	mov    (%rax),%rax
     f5f:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     f63:	77 22                	ja     f87 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
     f65:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f69:	48 8b 00             	mov    (%rax),%rax
     f6c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
     f70:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f74:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
     f78:	76 c7                	jbe    f41 <free+0x21>
     f7a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     f7e:	48 8b 00             	mov    (%rax),%rax
     f81:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
     f85:	76 ba                	jbe    f41 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
     f87:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f8b:	8b 40 08             	mov    0x8(%rax),%eax
     f8e:	89 c0                	mov    %eax,%eax
     f90:	48 c1 e0 04          	shl    $0x4,%rax
     f94:	48 89 c2             	mov    %rax,%rdx
     f97:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     f9b:	48 01 c2             	add    %rax,%rdx
     f9e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fa2:	48 8b 00             	mov    (%rax),%rax
     fa5:	48 39 c2             	cmp    %rax,%rdx
     fa8:	75 2d                	jne    fd7 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
     faa:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     fae:	8b 50 08             	mov    0x8(%rax),%edx
     fb1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fb5:	48 8b 00             	mov    (%rax),%rax
     fb8:	8b 40 08             	mov    0x8(%rax),%eax
     fbb:	01 c2                	add    %eax,%edx
     fbd:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     fc1:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
     fc4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fc8:	48 8b 00             	mov    (%rax),%rax
     fcb:	48 8b 10             	mov    (%rax),%rdx
     fce:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     fd2:	48 89 10             	mov    %rdx,(%rax)
     fd5:	eb 0e                	jmp    fe5 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
     fd7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fdb:	48 8b 10             	mov    (%rax),%rdx
     fde:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     fe2:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
     fe5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     fe9:	8b 40 08             	mov    0x8(%rax),%eax
     fec:	89 c0                	mov    %eax,%eax
     fee:	48 c1 e0 04          	shl    $0x4,%rax
     ff2:	48 89 c2             	mov    %rax,%rdx
     ff5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     ff9:	48 01 d0             	add    %rdx,%rax
     ffc:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1000:	75 27                	jne    1029 <free+0x109>
    p->s.size += bp->s.size;
    1002:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1006:	8b 50 08             	mov    0x8(%rax),%edx
    1009:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    100d:	8b 40 08             	mov    0x8(%rax),%eax
    1010:	01 c2                	add    %eax,%edx
    1012:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1016:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
    1019:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    101d:	48 8b 10             	mov    (%rax),%rdx
    1020:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1024:	48 89 10             	mov    %rdx,(%rax)
    1027:	eb 0b                	jmp    1034 <free+0x114>
  } else
    p->s.ptr = bp;
    1029:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    102d:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    1031:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
    1034:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1038:	48 89 05 d1 06 00 00 	mov    %rax,0x6d1(%rip)        # 1710 <freep>
}
    103f:	90                   	nop
    1040:	5d                   	pop    %rbp
    1041:	c3                   	retq   

0000000000001042 <morecore>:

static Header *morecore(uint nu) {
    1042:	55                   	push   %rbp
    1043:	48 89 e5             	mov    %rsp,%rbp
    1046:	48 83 ec 20          	sub    $0x20,%rsp
    104a:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
    104d:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
    1054:	77 07                	ja     105d <morecore+0x1b>
    nu = 4096;
    1056:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
    105d:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1060:	c1 e0 04             	shl    $0x4,%eax
    1063:	89 c7                	mov    %eax,%edi
    1065:	e8 8e fe ff ff       	callq  ef8 <sbrk>
    106a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
    106e:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
    1073:	75 07                	jne    107c <morecore+0x3a>
    return 0;
    1075:	b8 00 00 00 00       	mov    $0x0,%eax
    107a:	eb 29                	jmp    10a5 <morecore+0x63>
  hp = (Header *)p;
    107c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1080:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
    1084:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1088:	8b 55 ec             	mov    -0x14(%rbp),%edx
    108b:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
    108e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1092:	48 83 c0 10          	add    $0x10,%rax
    1096:	48 89 c7             	mov    %rax,%rdi
    1099:	e8 82 fe ff ff       	callq  f20 <free>
  return freep;
    109e:	48 8b 05 6b 06 00 00 	mov    0x66b(%rip),%rax        # 1710 <freep>
}
    10a5:	c9                   	leaveq 
    10a6:	c3                   	retq   

00000000000010a7 <malloc>:

void *malloc(uint nbytes) {
    10a7:	55                   	push   %rbp
    10a8:	48 89 e5             	mov    %rsp,%rbp
    10ab:	48 83 ec 30          	sub    $0x30,%rsp
    10af:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
    10b2:	8b 45 dc             	mov    -0x24(%rbp),%eax
    10b5:	48 83 c0 0f          	add    $0xf,%rax
    10b9:	48 c1 e8 04          	shr    $0x4,%rax
    10bd:	83 c0 01             	add    $0x1,%eax
    10c0:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
    10c3:	48 8b 05 46 06 00 00 	mov    0x646(%rip),%rax        # 1710 <freep>
    10ca:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    10ce:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    10d3:	75 2b                	jne    1100 <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
    10d5:	48 c7 45 f0 00 17 00 	movq   $0x1700,-0x10(%rbp)
    10dc:	00 
    10dd:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    10e1:	48 89 05 28 06 00 00 	mov    %rax,0x628(%rip)        # 1710 <freep>
    10e8:	48 8b 05 21 06 00 00 	mov    0x621(%rip),%rax        # 1710 <freep>
    10ef:	48 89 05 0a 06 00 00 	mov    %rax,0x60a(%rip)        # 1700 <base>
    base.s.size = 0;
    10f6:	c7 05 08 06 00 00 00 	movl   $0x0,0x608(%rip)        # 1708 <base+0x8>
    10fd:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    1100:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1104:	48 8b 00             	mov    (%rax),%rax
    1107:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
    110b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    110f:	8b 40 08             	mov    0x8(%rax),%eax
    1112:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1115:	72 5f                	jb     1176 <malloc+0xcf>
      if (p->s.size == nunits)
    1117:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    111b:	8b 40 08             	mov    0x8(%rax),%eax
    111e:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    1121:	75 10                	jne    1133 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
    1123:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1127:	48 8b 10             	mov    (%rax),%rdx
    112a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    112e:	48 89 10             	mov    %rdx,(%rax)
    1131:	eb 2e                	jmp    1161 <malloc+0xba>
      else {
        p->s.size -= nunits;
    1133:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1137:	8b 40 08             	mov    0x8(%rax),%eax
    113a:	2b 45 ec             	sub    -0x14(%rbp),%eax
    113d:	89 c2                	mov    %eax,%edx
    113f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1143:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
    1146:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    114a:	8b 40 08             	mov    0x8(%rax),%eax
    114d:	89 c0                	mov    %eax,%eax
    114f:	48 c1 e0 04          	shl    $0x4,%rax
    1153:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
    1157:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    115b:	8b 55 ec             	mov    -0x14(%rbp),%edx
    115e:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
    1161:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1165:	48 89 05 a4 05 00 00 	mov    %rax,0x5a4(%rip)        # 1710 <freep>
      return (void *)(p + 1);
    116c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1170:	48 83 c0 10          	add    $0x10,%rax
    1174:	eb 41                	jmp    11b7 <malloc+0x110>
    }
    if (p == freep)
    1176:	48 8b 05 93 05 00 00 	mov    0x593(%rip),%rax        # 1710 <freep>
    117d:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    1181:	75 1c                	jne    119f <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    1183:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1186:	89 c7                	mov    %eax,%edi
    1188:	e8 b5 fe ff ff       	callq  1042 <morecore>
    118d:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    1191:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    1196:	75 07                	jne    119f <malloc+0xf8>
        return 0;
    1198:	b8 00 00 00 00       	mov    $0x0,%eax
    119d:	eb 18                	jmp    11b7 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    119f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    11a3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    11a7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    11ab:	48 8b 00             	mov    (%rax),%rax
    11ae:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    11b2:	e9 54 ff ff ff       	jmpq   110b <malloc+0x64>
    11b7:	c9                   	leaveq 
    11b8:	c3                   	retq   
