
out/user/_sh:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <runcmd>:
int fork1(void); // Fork but panics on failure.
void panic(char *);
struct cmd *parsecmd(char *);

// Execute cmd.  Never returns.
void runcmd(struct cmd *cmd) {
       0:	55                   	push   %rbp
       1:	48 89 e5             	mov    %rsp,%rbp
       4:	48 83 ec 40          	sub    $0x40,%rsp
       8:	48 89 7d c8          	mov    %rdi,-0x38(%rbp)
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if (cmd == 0)
       c:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
      11:	75 05                	jne    18 <runcmd+0x18>
    exit();
      13:	e8 f6 14 00 00       	callq  150e <exit>

  switch (cmd->type) {
      18:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
      1c:	8b 00                	mov    (%rax),%eax
      1e:	83 f8 05             	cmp    $0x5,%eax
      21:	77 0c                	ja     2f <runcmd+0x2f>
      23:	89 c0                	mov    %eax,%eax
      25:	48 8b 04 c5 88 18 00 	mov    0x1888(,%rax,8),%rax
      2c:	00 
      2d:	ff e0                	jmpq   *%rax
  default:
    panic("runcmd");
      2f:	bf 58 18 00 00       	mov    $0x1858,%edi
      34:	e8 27 03 00 00       	callq  360 <panic>

  case EXEC:
    ecmd = (struct execcmd *)cmd;
      39:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
      3d:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (ecmd->argv[0] == 0)
      41:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
      45:	48 8b 40 08          	mov    0x8(%rax),%rax
      49:	48 85 c0             	test   %rax,%rax
      4c:	75 05                	jne    53 <runcmd+0x53>
      exit();
      4e:	e8 bb 14 00 00       	callq  150e <exit>
    exec(ecmd->argv[0], ecmd->argv);
      53:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
      57:	48 8d 50 08          	lea    0x8(%rax),%rdx
      5b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
      5f:	48 8b 40 08          	mov    0x8(%rax),%rax
      63:	48 89 d6             	mov    %rdx,%rsi
      66:	48 89 c7             	mov    %rax,%rdi
      69:	e8 d8 14 00 00       	callq  1546 <exec>
    printf(2, "exec %s failed\n", ecmd->argv[0]);
      6e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
      72:	48 8b 40 08          	mov    0x8(%rax),%rax
      76:	48 89 c2             	mov    %rax,%rdx
      79:	be 5f 18 00 00       	mov    $0x185f,%esi
      7e:	bf 02 00 00 00       	mov    $0x2,%edi
      83:	b8 00 00 00 00       	mov    $0x0,%eax
      88:	e8 de 0e 00 00       	callq  f6b <printf>
    break;
      8d:	e9 8d 01 00 00       	jmpq   21f <runcmd+0x21f>

  case REDIR:
    rcmd = (struct redircmd *)cmd;
      92:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
      96:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    close(rcmd->fd);
      9a:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
      9e:	8b 40 24             	mov    0x24(%rax),%eax
      a1:	89 c7                	mov    %eax,%edi
      a3:	e8 8e 14 00 00       	callq  1536 <close>
    if (open(rcmd->file, rcmd->mode) < 0) {
      a8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
      ac:	8b 50 20             	mov    0x20(%rax),%edx
      af:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
      b3:	48 8b 40 10          	mov    0x10(%rax),%rax
      b7:	89 d6                	mov    %edx,%esi
      b9:	48 89 c7             	mov    %rax,%rdi
      bc:	e8 8d 14 00 00       	callq  154e <open>
      c1:	85 c0                	test   %eax,%eax
      c3:	79 24                	jns    e9 <runcmd+0xe9>
      printf(2, "open %s failed\n", rcmd->file);
      c5:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
      c9:	48 8b 40 10          	mov    0x10(%rax),%rax
      cd:	48 89 c2             	mov    %rax,%rdx
      d0:	be 6f 18 00 00       	mov    $0x186f,%esi
      d5:	bf 02 00 00 00       	mov    $0x2,%edi
      da:	b8 00 00 00 00       	mov    $0x0,%eax
      df:	e8 87 0e 00 00       	callq  f6b <printf>
      exit();
      e4:	e8 25 14 00 00       	callq  150e <exit>
    }
    runcmd(rcmd->cmd);
      e9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
      ed:	48 8b 40 08          	mov    0x8(%rax),%rax
      f1:	48 89 c7             	mov    %rax,%rdi
      f4:	e8 07 ff ff ff       	callq  0 <runcmd>
    break;
      f9:	e9 21 01 00 00       	jmpq   21f <runcmd+0x21f>

  case LIST:
    lcmd = (struct listcmd *)cmd;
      fe:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     102:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
    if (fork1() == 0)
     106:	e8 81 02 00 00       	callq  38c <fork1>
     10b:	85 c0                	test   %eax,%eax
     10d:	75 10                	jne    11f <runcmd+0x11f>
      runcmd(lcmd->left);
     10f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     113:	48 8b 40 08          	mov    0x8(%rax),%rax
     117:	48 89 c7             	mov    %rax,%rdi
     11a:	e8 e1 fe ff ff       	callq  0 <runcmd>
    wait();
     11f:	e8 f2 13 00 00       	callq  1516 <wait>
    runcmd(lcmd->right);
     124:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     128:	48 8b 40 10          	mov    0x10(%rax),%rax
     12c:	48 89 c7             	mov    %rax,%rdi
     12f:	e8 cc fe ff ff       	callq  0 <runcmd>
    break;
     134:	e9 e6 00 00 00       	jmpq   21f <runcmd+0x21f>

  case PIPE:
    pcmd = (struct pipecmd *)cmd;
     139:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     13d:	48 89 45 e0          	mov    %rax,-0x20(%rbp)
    if (pipe(p) < 0)
     141:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
     145:	48 89 c7             	mov    %rax,%rdi
     148:	e8 d1 13 00 00       	callq  151e <pipe>
     14d:	85 c0                	test   %eax,%eax
     14f:	79 0a                	jns    15b <runcmd+0x15b>
      panic("pipe");
     151:	bf 7f 18 00 00       	mov    $0x187f,%edi
     156:	e8 05 02 00 00       	callq  360 <panic>
    if (fork1() == 0) {
     15b:	e8 2c 02 00 00       	callq  38c <fork1>
     160:	85 c0                	test   %eax,%eax
     162:	75 38                	jne    19c <runcmd+0x19c>
      close(1);
     164:	bf 01 00 00 00       	mov    $0x1,%edi
     169:	e8 c8 13 00 00       	callq  1536 <close>
      dup(p[1]);
     16e:	8b 45 d4             	mov    -0x2c(%rbp),%eax
     171:	89 c7                	mov    %eax,%edi
     173:	e8 0e 14 00 00       	callq  1586 <dup>
      close(p[0]);
     178:	8b 45 d0             	mov    -0x30(%rbp),%eax
     17b:	89 c7                	mov    %eax,%edi
     17d:	e8 b4 13 00 00       	callq  1536 <close>
      close(p[1]);
     182:	8b 45 d4             	mov    -0x2c(%rbp),%eax
     185:	89 c7                	mov    %eax,%edi
     187:	e8 aa 13 00 00       	callq  1536 <close>
      runcmd(pcmd->left);
     18c:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     190:	48 8b 40 08          	mov    0x8(%rax),%rax
     194:	48 89 c7             	mov    %rax,%rdi
     197:	e8 64 fe ff ff       	callq  0 <runcmd>
    }
    if (fork1() == 0) {
     19c:	e8 eb 01 00 00       	callq  38c <fork1>
     1a1:	85 c0                	test   %eax,%eax
     1a3:	75 38                	jne    1dd <runcmd+0x1dd>
      close(0);
     1a5:	bf 00 00 00 00       	mov    $0x0,%edi
     1aa:	e8 87 13 00 00       	callq  1536 <close>
      dup(p[0]);
     1af:	8b 45 d0             	mov    -0x30(%rbp),%eax
     1b2:	89 c7                	mov    %eax,%edi
     1b4:	e8 cd 13 00 00       	callq  1586 <dup>
      close(p[0]);
     1b9:	8b 45 d0             	mov    -0x30(%rbp),%eax
     1bc:	89 c7                	mov    %eax,%edi
     1be:	e8 73 13 00 00       	callq  1536 <close>
      close(p[1]);
     1c3:	8b 45 d4             	mov    -0x2c(%rbp),%eax
     1c6:	89 c7                	mov    %eax,%edi
     1c8:	e8 69 13 00 00       	callq  1536 <close>
      runcmd(pcmd->right);
     1cd:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     1d1:	48 8b 40 10          	mov    0x10(%rax),%rax
     1d5:	48 89 c7             	mov    %rax,%rdi
     1d8:	e8 23 fe ff ff       	callq  0 <runcmd>
    }
    close(p[0]);
     1dd:	8b 45 d0             	mov    -0x30(%rbp),%eax
     1e0:	89 c7                	mov    %eax,%edi
     1e2:	e8 4f 13 00 00       	callq  1536 <close>
    close(p[1]);
     1e7:	8b 45 d4             	mov    -0x2c(%rbp),%eax
     1ea:	89 c7                	mov    %eax,%edi
     1ec:	e8 45 13 00 00       	callq  1536 <close>
    wait();
     1f1:	e8 20 13 00 00       	callq  1516 <wait>
    wait();
     1f6:	e8 1b 13 00 00       	callq  1516 <wait>
    break;
     1fb:	eb 22                	jmp    21f <runcmd+0x21f>

  case BACK:
    bcmd = (struct backcmd *)cmd;
     1fd:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     201:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
    if (fork1() == 0)
     205:	e8 82 01 00 00       	callq  38c <fork1>
     20a:	85 c0                	test   %eax,%eax
     20c:	75 10                	jne    21e <runcmd+0x21e>
      runcmd(bcmd->cmd);
     20e:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     212:	48 8b 40 08          	mov    0x8(%rax),%rax
     216:	48 89 c7             	mov    %rax,%rdi
     219:	e8 e2 fd ff ff       	callq  0 <runcmd>
    break;
     21e:	90                   	nop
  }
  exit();
     21f:	e8 ea 12 00 00       	callq  150e <exit>

0000000000000224 <getcmd>:
}

int getcmd(char *buf, int nbuf) {
     224:	55                   	push   %rbp
     225:	48 89 e5             	mov    %rsp,%rbp
     228:	48 83 ec 10          	sub    $0x10,%rsp
     22c:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
     230:	89 75 f4             	mov    %esi,-0xc(%rbp)
  printf(2, "$ ");
     233:	be b8 18 00 00       	mov    $0x18b8,%esi
     238:	bf 02 00 00 00       	mov    $0x2,%edi
     23d:	b8 00 00 00 00       	mov    $0x0,%eax
     242:	e8 24 0d 00 00       	callq  f6b <printf>
  memset(buf, 0, nbuf);
     247:	8b 55 f4             	mov    -0xc(%rbp),%edx
     24a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     24e:	be 00 00 00 00       	mov    $0x0,%esi
     253:	48 89 c7             	mov    %rax,%rdi
     256:	e8 ca 10 00 00       	callq  1325 <memset>
  gets(buf, nbuf);
     25b:	8b 55 f4             	mov    -0xc(%rbp),%edx
     25e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     262:	89 d6                	mov    %edx,%esi
     264:	48 89 c7             	mov    %rax,%rdi
     267:	e8 1d 11 00 00       	callq  1389 <gets>
  if (buf[0] == 0) // EOF
     26c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     270:	0f b6 00             	movzbl (%rax),%eax
     273:	84 c0                	test   %al,%al
     275:	75 07                	jne    27e <getcmd+0x5a>
    return -1;
     277:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
     27c:	eb 05                	jmp    283 <getcmd+0x5f>
  return 0;
     27e:	b8 00 00 00 00       	mov    $0x0,%eax
}
     283:	c9                   	leaveq 
     284:	c3                   	retq   

0000000000000285 <main>:

int main(void) {
     285:	55                   	push   %rbp
     286:	48 89 e5             	mov    %rsp,%rbp
     289:	48 83 ec 10          	sub    $0x10,%rsp
  static char buf[100];
  int fd;

  // Ensure that three file descriptors are open.
  while ((fd = open("console", O_RDWR)) >= 0) {
     28d:	eb 12                	jmp    2a1 <main+0x1c>
    if (fd >= 3) {
     28f:	83 7d fc 02          	cmpl   $0x2,-0x4(%rbp)
     293:	7e 0c                	jle    2a1 <main+0x1c>
      close(fd);
     295:	8b 45 fc             	mov    -0x4(%rbp),%eax
     298:	89 c7                	mov    %eax,%edi
     29a:	e8 97 12 00 00       	callq  1536 <close>
      break;
     29f:	eb 18                	jmp    2b9 <main+0x34>
int main(void) {
  static char buf[100];
  int fd;

  // Ensure that three file descriptors are open.
  while ((fd = open("console", O_RDWR)) >= 0) {
     2a1:	be 02 00 00 00       	mov    $0x2,%esi
     2a6:	bf bb 18 00 00       	mov    $0x18bb,%edi
     2ab:	e8 9e 12 00 00       	callq  154e <open>
     2b0:	89 45 fc             	mov    %eax,-0x4(%rbp)
     2b3:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     2b7:	79 d6                	jns    28f <main+0xa>
      break;
    }
  }

  // Read and run input commands.
  while (getcmd(buf, sizeof(buf)) >= 0) {
     2b9:	e9 86 00 00 00       	jmpq   344 <main+0xbf>
    if (buf[0] == 'c' && buf[1] == 'd' && buf[2] == ' ') {
     2be:	0f b6 05 bb 1b 00 00 	movzbl 0x1bbb(%rip),%eax        # 1e80 <buf.1255>
     2c5:	3c 63                	cmp    $0x63,%al
     2c7:	75 5b                	jne    324 <main+0x9f>
     2c9:	0f b6 05 b1 1b 00 00 	movzbl 0x1bb1(%rip),%eax        # 1e81 <buf.1255+0x1>
     2d0:	3c 64                	cmp    $0x64,%al
     2d2:	75 50                	jne    324 <main+0x9f>
     2d4:	0f b6 05 a7 1b 00 00 	movzbl 0x1ba7(%rip),%eax        # 1e82 <buf.1255+0x2>
     2db:	3c 20                	cmp    $0x20,%al
     2dd:	75 45                	jne    324 <main+0x9f>
      // Chdir must be called by the parent, not the child.
      buf[strlen(buf) - 1] = 0; // chop \n
     2df:	bf 80 1e 00 00       	mov    $0x1e80,%edi
     2e4:	e8 0e 10 00 00       	callq  12f7 <strlen>
     2e9:	83 e8 01             	sub    $0x1,%eax
     2ec:	89 c0                	mov    %eax,%eax
     2ee:	c6 80 80 1e 00 00 00 	movb   $0x0,0x1e80(%rax)
      if (chdir(buf + 3) < 0)
     2f5:	b8 83 1e 00 00       	mov    $0x1e83,%eax
     2fa:	48 89 c7             	mov    %rax,%rdi
     2fd:	e8 7c 12 00 00       	callq  157e <chdir>
     302:	85 c0                	test   %eax,%eax
     304:	79 3e                	jns    344 <main+0xbf>
        printf(2, "cannot cd %s\n", buf + 3);
     306:	b8 83 1e 00 00       	mov    $0x1e83,%eax
     30b:	48 89 c2             	mov    %rax,%rdx
     30e:	be c3 18 00 00       	mov    $0x18c3,%esi
     313:	bf 02 00 00 00       	mov    $0x2,%edi
     318:	b8 00 00 00 00       	mov    $0x0,%eax
     31d:	e8 49 0c 00 00       	callq  f6b <printf>
      continue;
     322:	eb 20                	jmp    344 <main+0xbf>
    }
    if (fork1() == 0)
     324:	e8 63 00 00 00       	callq  38c <fork1>
     329:	85 c0                	test   %eax,%eax
     32b:	75 12                	jne    33f <main+0xba>
      runcmd(parsecmd(buf));
     32d:	bf 80 1e 00 00       	mov    $0x1e80,%edi
     332:	e8 41 04 00 00       	callq  778 <parsecmd>
     337:	48 89 c7             	mov    %rax,%rdi
     33a:	e8 c1 fc ff ff       	callq  0 <runcmd>
    wait();
     33f:	e8 d2 11 00 00       	callq  1516 <wait>
      break;
    }
  }

  // Read and run input commands.
  while (getcmd(buf, sizeof(buf)) >= 0) {
     344:	be 64 00 00 00       	mov    $0x64,%esi
     349:	bf 80 1e 00 00       	mov    $0x1e80,%edi
     34e:	e8 d1 fe ff ff       	callq  224 <getcmd>
     353:	85 c0                	test   %eax,%eax
     355:	0f 89 63 ff ff ff    	jns    2be <main+0x39>
    }
    if (fork1() == 0)
      runcmd(parsecmd(buf));
    wait();
  }
  exit();
     35b:	e8 ae 11 00 00       	callq  150e <exit>

0000000000000360 <panic>:
}

void panic(char *s) {
     360:	55                   	push   %rbp
     361:	48 89 e5             	mov    %rsp,%rbp
     364:	48 83 ec 10          	sub    $0x10,%rsp
     368:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
  printf(2, "%s\n", s);
     36c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     370:	48 89 c2             	mov    %rax,%rdx
     373:	be d1 18 00 00       	mov    $0x18d1,%esi
     378:	bf 02 00 00 00       	mov    $0x2,%edi
     37d:	b8 00 00 00 00       	mov    $0x0,%eax
     382:	e8 e4 0b 00 00       	callq  f6b <printf>
  exit();
     387:	e8 82 11 00 00       	callq  150e <exit>

000000000000038c <fork1>:
}

int fork1(void) {
     38c:	55                   	push   %rbp
     38d:	48 89 e5             	mov    %rsp,%rbp
     390:	48 83 ec 10          	sub    $0x10,%rsp
  int pid;

  pid = fork();
     394:	e8 6d 11 00 00       	callq  1506 <fork>
     399:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (pid == -1)
     39c:	83 7d fc ff          	cmpl   $0xffffffff,-0x4(%rbp)
     3a0:	75 0a                	jne    3ac <fork1+0x20>
    panic("fork");
     3a2:	bf d5 18 00 00       	mov    $0x18d5,%edi
     3a7:	e8 b4 ff ff ff       	callq  360 <panic>
  return pid;
     3ac:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
     3af:	c9                   	leaveq 
     3b0:	c3                   	retq   

00000000000003b1 <execcmd>:

// Constructors

struct cmd *execcmd(void) {
     3b1:	55                   	push   %rbp
     3b2:	48 89 e5             	mov    %rsp,%rbp
     3b5:	48 83 ec 10          	sub    $0x10,%rsp
  struct execcmd *cmd;

  cmd = malloc(sizeof(*cmd));
     3b9:	bf a8 00 00 00       	mov    $0xa8,%edi
     3be:	e8 82 13 00 00       	callq  1745 <malloc>
     3c3:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  memset(cmd, 0, sizeof(*cmd));
     3c7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     3cb:	ba a8 00 00 00       	mov    $0xa8,%edx
     3d0:	be 00 00 00 00       	mov    $0x0,%esi
     3d5:	48 89 c7             	mov    %rax,%rdi
     3d8:	e8 48 0f 00 00       	callq  1325 <memset>
  cmd->type = EXEC;
     3dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     3e1:	c7 00 01 00 00 00    	movl   $0x1,(%rax)
  return (struct cmd *)cmd;
     3e7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     3eb:	c9                   	leaveq 
     3ec:	c3                   	retq   

00000000000003ed <redircmd>:

struct cmd *redircmd(struct cmd *subcmd, char *file, char *efile, int mode,
                     int fd) {
     3ed:	55                   	push   %rbp
     3ee:	48 89 e5             	mov    %rsp,%rbp
     3f1:	48 83 ec 30          	sub    $0x30,%rsp
     3f5:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     3f9:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
     3fd:	48 89 55 d8          	mov    %rdx,-0x28(%rbp)
     401:	89 4d d4             	mov    %ecx,-0x2c(%rbp)
     404:	44 89 45 d0          	mov    %r8d,-0x30(%rbp)
  struct redircmd *cmd;

  cmd = malloc(sizeof(*cmd));
     408:	bf 28 00 00 00       	mov    $0x28,%edi
     40d:	e8 33 13 00 00       	callq  1745 <malloc>
     412:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  memset(cmd, 0, sizeof(*cmd));
     416:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     41a:	ba 28 00 00 00       	mov    $0x28,%edx
     41f:	be 00 00 00 00       	mov    $0x0,%esi
     424:	48 89 c7             	mov    %rax,%rdi
     427:	e8 f9 0e 00 00       	callq  1325 <memset>
  cmd->type = REDIR;
     42c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     430:	c7 00 02 00 00 00    	movl   $0x2,(%rax)
  cmd->cmd = subcmd;
     436:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     43a:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     43e:	48 89 50 08          	mov    %rdx,0x8(%rax)
  cmd->file = file;
     442:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     446:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     44a:	48 89 50 10          	mov    %rdx,0x10(%rax)
  cmd->efile = efile;
     44e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     452:	48 8b 55 d8          	mov    -0x28(%rbp),%rdx
     456:	48 89 50 18          	mov    %rdx,0x18(%rax)
  cmd->mode = mode;
     45a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     45e:	8b 55 d4             	mov    -0x2c(%rbp),%edx
     461:	89 50 20             	mov    %edx,0x20(%rax)
  cmd->fd = fd;
     464:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     468:	8b 55 d0             	mov    -0x30(%rbp),%edx
     46b:	89 50 24             	mov    %edx,0x24(%rax)
  return (struct cmd *)cmd;
     46e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     472:	c9                   	leaveq 
     473:	c3                   	retq   

0000000000000474 <pipecmd>:

struct cmd *pipecmd(struct cmd *left, struct cmd *right) {
     474:	55                   	push   %rbp
     475:	48 89 e5             	mov    %rsp,%rbp
     478:	48 83 ec 20          	sub    $0x20,%rsp
     47c:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     480:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  struct pipecmd *cmd;

  cmd = malloc(sizeof(*cmd));
     484:	bf 18 00 00 00       	mov    $0x18,%edi
     489:	e8 b7 12 00 00       	callq  1745 <malloc>
     48e:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  memset(cmd, 0, sizeof(*cmd));
     492:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     496:	ba 18 00 00 00       	mov    $0x18,%edx
     49b:	be 00 00 00 00       	mov    $0x0,%esi
     4a0:	48 89 c7             	mov    %rax,%rdi
     4a3:	e8 7d 0e 00 00       	callq  1325 <memset>
  cmd->type = PIPE;
     4a8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     4ac:	c7 00 03 00 00 00    	movl   $0x3,(%rax)
  cmd->left = left;
     4b2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     4b6:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     4ba:	48 89 50 08          	mov    %rdx,0x8(%rax)
  cmd->right = right;
     4be:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     4c2:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     4c6:	48 89 50 10          	mov    %rdx,0x10(%rax)
  return (struct cmd *)cmd;
     4ca:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     4ce:	c9                   	leaveq 
     4cf:	c3                   	retq   

00000000000004d0 <listcmd>:

struct cmd *listcmd(struct cmd *left, struct cmd *right) {
     4d0:	55                   	push   %rbp
     4d1:	48 89 e5             	mov    %rsp,%rbp
     4d4:	48 83 ec 20          	sub    $0x20,%rsp
     4d8:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     4dc:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  struct listcmd *cmd;

  cmd = malloc(sizeof(*cmd));
     4e0:	bf 18 00 00 00       	mov    $0x18,%edi
     4e5:	e8 5b 12 00 00       	callq  1745 <malloc>
     4ea:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  memset(cmd, 0, sizeof(*cmd));
     4ee:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     4f2:	ba 18 00 00 00       	mov    $0x18,%edx
     4f7:	be 00 00 00 00       	mov    $0x0,%esi
     4fc:	48 89 c7             	mov    %rax,%rdi
     4ff:	e8 21 0e 00 00       	callq  1325 <memset>
  cmd->type = LIST;
     504:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     508:	c7 00 04 00 00 00    	movl   $0x4,(%rax)
  cmd->left = left;
     50e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     512:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     516:	48 89 50 08          	mov    %rdx,0x8(%rax)
  cmd->right = right;
     51a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     51e:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     522:	48 89 50 10          	mov    %rdx,0x10(%rax)
  return (struct cmd *)cmd;
     526:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     52a:	c9                   	leaveq 
     52b:	c3                   	retq   

000000000000052c <backcmd>:

struct cmd *backcmd(struct cmd *subcmd) {
     52c:	55                   	push   %rbp
     52d:	48 89 e5             	mov    %rsp,%rbp
     530:	48 83 ec 20          	sub    $0x20,%rsp
     534:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  struct backcmd *cmd;

  cmd = malloc(sizeof(*cmd));
     538:	bf 10 00 00 00       	mov    $0x10,%edi
     53d:	e8 03 12 00 00       	callq  1745 <malloc>
     542:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  memset(cmd, 0, sizeof(*cmd));
     546:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     54a:	ba 10 00 00 00       	mov    $0x10,%edx
     54f:	be 00 00 00 00       	mov    $0x0,%esi
     554:	48 89 c7             	mov    %rax,%rdi
     557:	e8 c9 0d 00 00       	callq  1325 <memset>
  cmd->type = BACK;
     55c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     560:	c7 00 05 00 00 00    	movl   $0x5,(%rax)
  cmd->cmd = subcmd;
     566:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     56a:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     56e:	48 89 50 08          	mov    %rdx,0x8(%rax)
  return (struct cmd *)cmd;
     572:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     576:	c9                   	leaveq 
     577:	c3                   	retq   

0000000000000578 <gettoken>:
// Parsing

char whitespace[] = " \t\r\n\v";
char symbols[] = "<|>&;()";

int gettoken(char **ps, char *es, char **q, char **eq) {
     578:	55                   	push   %rbp
     579:	48 89 e5             	mov    %rsp,%rbp
     57c:	48 83 ec 30          	sub    $0x30,%rsp
     580:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     584:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
     588:	48 89 55 d8          	mov    %rdx,-0x28(%rbp)
     58c:	48 89 4d d0          	mov    %rcx,-0x30(%rbp)
  char *s;
  int ret;

  s = *ps;
     590:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     594:	48 8b 00             	mov    (%rax),%rax
     597:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while (s < es && strchr(whitespace, *s))
     59b:	eb 05                	jmp    5a2 <gettoken+0x2a>
    s++;
     59d:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
int gettoken(char **ps, char *es, char **q, char **eq) {
  char *s;
  int ret;

  s = *ps;
  while (s < es && strchr(whitespace, *s))
     5a2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     5a6:	48 3b 45 e0          	cmp    -0x20(%rbp),%rax
     5aa:	73 1b                	jae    5c7 <gettoken+0x4f>
     5ac:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     5b0:	0f b6 00             	movzbl (%rax),%eax
     5b3:	0f be c0             	movsbl %al,%eax
     5b6:	89 c6                	mov    %eax,%esi
     5b8:	bf 30 1e 00 00       	mov    $0x1e30,%edi
     5bd:	e8 8f 0d 00 00       	callq  1351 <strchr>
     5c2:	48 85 c0             	test   %rax,%rax
     5c5:	75 d6                	jne    59d <gettoken+0x25>
    s++;
  if (q)
     5c7:	48 83 7d d8 00       	cmpq   $0x0,-0x28(%rbp)
     5cc:	74 0b                	je     5d9 <gettoken+0x61>
    *q = s;
     5ce:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     5d2:	48 8b 55 f8          	mov    -0x8(%rbp),%rdx
     5d6:	48 89 10             	mov    %rdx,(%rax)
  ret = *s;
     5d9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     5dd:	0f b6 00             	movzbl (%rax),%eax
     5e0:	0f be c0             	movsbl %al,%eax
     5e3:	89 45 f4             	mov    %eax,-0xc(%rbp)
  switch (*s) {
     5e6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     5ea:	0f b6 00             	movzbl (%rax),%eax
     5ed:	0f be c0             	movsbl %al,%eax
     5f0:	83 f8 29             	cmp    $0x29,%eax
     5f3:	7f 14                	jg     609 <gettoken+0x91>
     5f5:	83 f8 28             	cmp    $0x28,%eax
     5f8:	7d 28                	jge    622 <gettoken+0xaa>
     5fa:	85 c0                	test   %eax,%eax
     5fc:	0f 84 95 00 00 00    	je     697 <gettoken+0x11f>
     602:	83 f8 26             	cmp    $0x26,%eax
     605:	74 1b                	je     622 <gettoken+0xaa>
     607:	eb 3e                	jmp    647 <gettoken+0xcf>
     609:	83 f8 3e             	cmp    $0x3e,%eax
     60c:	74 1b                	je     629 <gettoken+0xb1>
     60e:	83 f8 3e             	cmp    $0x3e,%eax
     611:	7f 0a                	jg     61d <gettoken+0xa5>
     613:	83 e8 3b             	sub    $0x3b,%eax
     616:	83 f8 01             	cmp    $0x1,%eax
     619:	77 2c                	ja     647 <gettoken+0xcf>
     61b:	eb 05                	jmp    622 <gettoken+0xaa>
     61d:	83 f8 7c             	cmp    $0x7c,%eax
     620:	75 25                	jne    647 <gettoken+0xcf>
  case '(':
  case ')':
  case ';':
  case '&':
  case '<':
    s++;
     622:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    break;
     627:	eb 75                	jmp    69e <gettoken+0x126>
  case '>':
    s++;
     629:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    if (*s == '>') {
     62e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     632:	0f b6 00             	movzbl (%rax),%eax
     635:	3c 3e                	cmp    $0x3e,%al
     637:	75 61                	jne    69a <gettoken+0x122>
      ret = '+';
     639:	c7 45 f4 2b 00 00 00 	movl   $0x2b,-0xc(%rbp)
      s++;
     640:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    }
    break;
     645:	eb 53                	jmp    69a <gettoken+0x122>
  default:
    ret = 'a';
     647:	c7 45 f4 61 00 00 00 	movl   $0x61,-0xc(%rbp)
    while (s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
     64e:	eb 05                	jmp    655 <gettoken+0xdd>
      s++;
     650:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
      s++;
    }
    break;
  default:
    ret = 'a';
    while (s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
     655:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     659:	48 3b 45 e0          	cmp    -0x20(%rbp),%rax
     65d:	73 3e                	jae    69d <gettoken+0x125>
     65f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     663:	0f b6 00             	movzbl (%rax),%eax
     666:	0f be c0             	movsbl %al,%eax
     669:	89 c6                	mov    %eax,%esi
     66b:	bf 30 1e 00 00       	mov    $0x1e30,%edi
     670:	e8 dc 0c 00 00       	callq  1351 <strchr>
     675:	48 85 c0             	test   %rax,%rax
     678:	75 23                	jne    69d <gettoken+0x125>
     67a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     67e:	0f b6 00             	movzbl (%rax),%eax
     681:	0f be c0             	movsbl %al,%eax
     684:	89 c6                	mov    %eax,%esi
     686:	bf 38 1e 00 00       	mov    $0x1e38,%edi
     68b:	e8 c1 0c 00 00       	callq  1351 <strchr>
     690:	48 85 c0             	test   %rax,%rax
     693:	74 bb                	je     650 <gettoken+0xd8>
      s++;
    break;
     695:	eb 06                	jmp    69d <gettoken+0x125>
  if (q)
    *q = s;
  ret = *s;
  switch (*s) {
  case 0:
    break;
     697:	90                   	nop
     698:	eb 04                	jmp    69e <gettoken+0x126>
    s++;
    if (*s == '>') {
      ret = '+';
      s++;
    }
    break;
     69a:	90                   	nop
     69b:	eb 01                	jmp    69e <gettoken+0x126>
  default:
    ret = 'a';
    while (s < es && !strchr(whitespace, *s) && !strchr(symbols, *s))
      s++;
    break;
     69d:	90                   	nop
  }
  if (eq)
     69e:	48 83 7d d0 00       	cmpq   $0x0,-0x30(%rbp)
     6a3:	74 12                	je     6b7 <gettoken+0x13f>
    *eq = s;
     6a5:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     6a9:	48 8b 55 f8          	mov    -0x8(%rbp),%rdx
     6ad:	48 89 10             	mov    %rdx,(%rax)

  while (s < es && strchr(whitespace, *s))
     6b0:	eb 05                	jmp    6b7 <gettoken+0x13f>
    s++;
     6b2:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    break;
  }
  if (eq)
    *eq = s;

  while (s < es && strchr(whitespace, *s))
     6b7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     6bb:	48 3b 45 e0          	cmp    -0x20(%rbp),%rax
     6bf:	73 1b                	jae    6dc <gettoken+0x164>
     6c1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     6c5:	0f b6 00             	movzbl (%rax),%eax
     6c8:	0f be c0             	movsbl %al,%eax
     6cb:	89 c6                	mov    %eax,%esi
     6cd:	bf 30 1e 00 00       	mov    $0x1e30,%edi
     6d2:	e8 7a 0c 00 00       	callq  1351 <strchr>
     6d7:	48 85 c0             	test   %rax,%rax
     6da:	75 d6                	jne    6b2 <gettoken+0x13a>
    s++;
  *ps = s;
     6dc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     6e0:	48 8b 55 f8          	mov    -0x8(%rbp),%rdx
     6e4:	48 89 10             	mov    %rdx,(%rax)
  return ret;
     6e7:	8b 45 f4             	mov    -0xc(%rbp),%eax
}
     6ea:	c9                   	leaveq 
     6eb:	c3                   	retq   

00000000000006ec <peek>:

int peek(char **ps, char *es, char *toks) {
     6ec:	55                   	push   %rbp
     6ed:	48 89 e5             	mov    %rsp,%rbp
     6f0:	48 83 ec 30          	sub    $0x30,%rsp
     6f4:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     6f8:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
     6fc:	48 89 55 d8          	mov    %rdx,-0x28(%rbp)
  char *s;

  s = *ps;
     700:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     704:	48 8b 00             	mov    (%rax),%rax
     707:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while (s < es && strchr(whitespace, *s))
     70b:	eb 05                	jmp    712 <peek+0x26>
    s++;
     70d:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)

int peek(char **ps, char *es, char *toks) {
  char *s;

  s = *ps;
  while (s < es && strchr(whitespace, *s))
     712:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     716:	48 3b 45 e0          	cmp    -0x20(%rbp),%rax
     71a:	73 1b                	jae    737 <peek+0x4b>
     71c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     720:	0f b6 00             	movzbl (%rax),%eax
     723:	0f be c0             	movsbl %al,%eax
     726:	89 c6                	mov    %eax,%esi
     728:	bf 30 1e 00 00       	mov    $0x1e30,%edi
     72d:	e8 1f 0c 00 00       	callq  1351 <strchr>
     732:	48 85 c0             	test   %rax,%rax
     735:	75 d6                	jne    70d <peek+0x21>
    s++;
  *ps = s;
     737:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     73b:	48 8b 55 f8          	mov    -0x8(%rbp),%rdx
     73f:	48 89 10             	mov    %rdx,(%rax)
  return *s && strchr(toks, *s);
     742:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     746:	0f b6 00             	movzbl (%rax),%eax
     749:	84 c0                	test   %al,%al
     74b:	74 24                	je     771 <peek+0x85>
     74d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     751:	0f b6 00             	movzbl (%rax),%eax
     754:	0f be d0             	movsbl %al,%edx
     757:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     75b:	89 d6                	mov    %edx,%esi
     75d:	48 89 c7             	mov    %rax,%rdi
     760:	e8 ec 0b 00 00       	callq  1351 <strchr>
     765:	48 85 c0             	test   %rax,%rax
     768:	74 07                	je     771 <peek+0x85>
     76a:	b8 01 00 00 00       	mov    $0x1,%eax
     76f:	eb 05                	jmp    776 <peek+0x8a>
     771:	b8 00 00 00 00       	mov    $0x0,%eax
}
     776:	c9                   	leaveq 
     777:	c3                   	retq   

0000000000000778 <parsecmd>:
struct cmd *parseline(char **, char *);
struct cmd *parsepipe(char **, char *);
struct cmd *parseexec(char **, char *);
struct cmd *nulterminate(struct cmd *);

struct cmd *parsecmd(char *s) {
     778:	55                   	push   %rbp
     779:	48 89 e5             	mov    %rsp,%rbp
     77c:	53                   	push   %rbx
     77d:	48 83 ec 28          	sub    $0x28,%rsp
     781:	48 89 7d d8          	mov    %rdi,-0x28(%rbp)
  char *es;
  struct cmd *cmd;

  es = s + strlen(s);
     785:	48 8b 5d d8          	mov    -0x28(%rbp),%rbx
     789:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     78d:	48 89 c7             	mov    %rax,%rdi
     790:	e8 62 0b 00 00       	callq  12f7 <strlen>
     795:	89 c0                	mov    %eax,%eax
     797:	48 01 d8             	add    %rbx,%rax
     79a:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  cmd = parseline(&s, es);
     79e:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     7a2:	48 8d 45 d8          	lea    -0x28(%rbp),%rax
     7a6:	48 89 d6             	mov    %rdx,%rsi
     7a9:	48 89 c7             	mov    %rax,%rdi
     7ac:	e8 62 00 00 00       	callq  813 <parseline>
     7b1:	48 89 45 e0          	mov    %rax,-0x20(%rbp)
  peek(&s, es, "");
     7b5:	48 8b 4d e8          	mov    -0x18(%rbp),%rcx
     7b9:	48 8d 45 d8          	lea    -0x28(%rbp),%rax
     7bd:	ba da 18 00 00       	mov    $0x18da,%edx
     7c2:	48 89 ce             	mov    %rcx,%rsi
     7c5:	48 89 c7             	mov    %rax,%rdi
     7c8:	e8 1f ff ff ff       	callq  6ec <peek>
  if (s != es) {
     7cd:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     7d1:	48 3b 45 e8          	cmp    -0x18(%rbp),%rax
     7d5:	74 25                	je     7fc <parsecmd+0x84>
    printf(2, "leftovers: %s\n", s);
     7d7:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     7db:	48 89 c2             	mov    %rax,%rdx
     7de:	be db 18 00 00       	mov    $0x18db,%esi
     7e3:	bf 02 00 00 00       	mov    $0x2,%edi
     7e8:	b8 00 00 00 00       	mov    $0x0,%eax
     7ed:	e8 79 07 00 00       	callq  f6b <printf>
    panic("syntax");
     7f2:	bf ea 18 00 00       	mov    $0x18ea,%edi
     7f7:	e8 64 fb ff ff       	callq  360 <panic>
  }
  nulterminate(cmd);
     7fc:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     800:	48 89 c7             	mov    %rax,%rdi
     803:	e8 95 04 00 00       	callq  c9d <nulterminate>
  return cmd;
     808:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
}
     80c:	48 83 c4 28          	add    $0x28,%rsp
     810:	5b                   	pop    %rbx
     811:	5d                   	pop    %rbp
     812:	c3                   	retq   

0000000000000813 <parseline>:

struct cmd *parseline(char **ps, char *es) {
     813:	55                   	push   %rbp
     814:	48 89 e5             	mov    %rsp,%rbp
     817:	48 83 ec 20          	sub    $0x20,%rsp
     81b:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     81f:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  struct cmd *cmd;

  cmd = parsepipe(ps, es);
     823:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     827:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     82b:	48 89 d6             	mov    %rdx,%rsi
     82e:	48 89 c7             	mov    %rax,%rdi
     831:	e8 b1 00 00 00       	callq  8e7 <parsepipe>
     836:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while (peek(ps, es, "&")) {
     83a:	eb 2a                	jmp    866 <parseline+0x53>
    gettoken(ps, es, 0, 0);
     83c:	48 8b 75 e0          	mov    -0x20(%rbp),%rsi
     840:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     844:	b9 00 00 00 00       	mov    $0x0,%ecx
     849:	ba 00 00 00 00       	mov    $0x0,%edx
     84e:	48 89 c7             	mov    %rax,%rdi
     851:	e8 22 fd ff ff       	callq  578 <gettoken>
    cmd = backcmd(cmd);
     856:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     85a:	48 89 c7             	mov    %rax,%rdi
     85d:	e8 ca fc ff ff       	callq  52c <backcmd>
     862:	48 89 45 f8          	mov    %rax,-0x8(%rbp)

struct cmd *parseline(char **ps, char *es) {
  struct cmd *cmd;

  cmd = parsepipe(ps, es);
  while (peek(ps, es, "&")) {
     866:	48 8b 4d e0          	mov    -0x20(%rbp),%rcx
     86a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     86e:	ba f1 18 00 00       	mov    $0x18f1,%edx
     873:	48 89 ce             	mov    %rcx,%rsi
     876:	48 89 c7             	mov    %rax,%rdi
     879:	e8 6e fe ff ff       	callq  6ec <peek>
     87e:	85 c0                	test   %eax,%eax
     880:	75 ba                	jne    83c <parseline+0x29>
    gettoken(ps, es, 0, 0);
    cmd = backcmd(cmd);
  }
  if (peek(ps, es, ";")) {
     882:	48 8b 4d e0          	mov    -0x20(%rbp),%rcx
     886:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     88a:	ba f3 18 00 00       	mov    $0x18f3,%edx
     88f:	48 89 ce             	mov    %rcx,%rsi
     892:	48 89 c7             	mov    %rax,%rdi
     895:	e8 52 fe ff ff       	callq  6ec <peek>
     89a:	85 c0                	test   %eax,%eax
     89c:	74 43                	je     8e1 <parseline+0xce>
    gettoken(ps, es, 0, 0);
     89e:	48 8b 75 e0          	mov    -0x20(%rbp),%rsi
     8a2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     8a6:	b9 00 00 00 00       	mov    $0x0,%ecx
     8ab:	ba 00 00 00 00       	mov    $0x0,%edx
     8b0:	48 89 c7             	mov    %rax,%rdi
     8b3:	e8 c0 fc ff ff       	callq  578 <gettoken>
    cmd = listcmd(cmd, parseline(ps, es));
     8b8:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     8bc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     8c0:	48 89 d6             	mov    %rdx,%rsi
     8c3:	48 89 c7             	mov    %rax,%rdi
     8c6:	e8 48 ff ff ff       	callq  813 <parseline>
     8cb:	48 89 c2             	mov    %rax,%rdx
     8ce:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     8d2:	48 89 d6             	mov    %rdx,%rsi
     8d5:	48 89 c7             	mov    %rax,%rdi
     8d8:	e8 f3 fb ff ff       	callq  4d0 <listcmd>
     8dd:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  }
  return cmd;
     8e1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     8e5:	c9                   	leaveq 
     8e6:	c3                   	retq   

00000000000008e7 <parsepipe>:

struct cmd *parsepipe(char **ps, char *es) {
     8e7:	55                   	push   %rbp
     8e8:	48 89 e5             	mov    %rsp,%rbp
     8eb:	48 83 ec 20          	sub    $0x20,%rsp
     8ef:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     8f3:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  struct cmd *cmd;

  cmd = parseexec(ps, es);
     8f7:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     8fb:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     8ff:	48 89 d6             	mov    %rdx,%rsi
     902:	48 89 c7             	mov    %rax,%rdi
     905:	e8 36 02 00 00       	callq  b40 <parseexec>
     90a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (peek(ps, es, "|")) {
     90e:	48 8b 4d e0          	mov    -0x20(%rbp),%rcx
     912:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     916:	ba f5 18 00 00       	mov    $0x18f5,%edx
     91b:	48 89 ce             	mov    %rcx,%rsi
     91e:	48 89 c7             	mov    %rax,%rdi
     921:	e8 c6 fd ff ff       	callq  6ec <peek>
     926:	85 c0                	test   %eax,%eax
     928:	74 43                	je     96d <parsepipe+0x86>
    gettoken(ps, es, 0, 0);
     92a:	48 8b 75 e0          	mov    -0x20(%rbp),%rsi
     92e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     932:	b9 00 00 00 00       	mov    $0x0,%ecx
     937:	ba 00 00 00 00       	mov    $0x0,%edx
     93c:	48 89 c7             	mov    %rax,%rdi
     93f:	e8 34 fc ff ff       	callq  578 <gettoken>
    cmd = pipecmd(cmd, parsepipe(ps, es));
     944:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     948:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     94c:	48 89 d6             	mov    %rdx,%rsi
     94f:	48 89 c7             	mov    %rax,%rdi
     952:	e8 90 ff ff ff       	callq  8e7 <parsepipe>
     957:	48 89 c2             	mov    %rax,%rdx
     95a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     95e:	48 89 d6             	mov    %rdx,%rsi
     961:	48 89 c7             	mov    %rax,%rdi
     964:	e8 0b fb ff ff       	callq  474 <pipecmd>
     969:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  }
  return cmd;
     96d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     971:	c9                   	leaveq 
     972:	c3                   	retq   

0000000000000973 <parseredirs>:

struct cmd *parseredirs(struct cmd *cmd, char **ps, char *es) {
     973:	55                   	push   %rbp
     974:	48 89 e5             	mov    %rsp,%rbp
     977:	48 83 ec 40          	sub    $0x40,%rsp
     97b:	48 89 7d d8          	mov    %rdi,-0x28(%rbp)
     97f:	48 89 75 d0          	mov    %rsi,-0x30(%rbp)
     983:	48 89 55 c8          	mov    %rdx,-0x38(%rbp)
  int tok;
  char *q, *eq;

  while (peek(ps, es, "<>")) {
     987:	e9 c6 00 00 00       	jmpq   a52 <parseredirs+0xdf>
    tok = gettoken(ps, es, 0, 0);
     98c:	48 8b 75 c8          	mov    -0x38(%rbp),%rsi
     990:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     994:	b9 00 00 00 00       	mov    $0x0,%ecx
     999:	ba 00 00 00 00       	mov    $0x0,%edx
     99e:	48 89 c7             	mov    %rax,%rdi
     9a1:	e8 d2 fb ff ff       	callq  578 <gettoken>
     9a6:	89 45 fc             	mov    %eax,-0x4(%rbp)
    if (gettoken(ps, es, &q, &eq) != 'a')
     9a9:	48 8d 4d e8          	lea    -0x18(%rbp),%rcx
     9ad:	48 8d 55 f0          	lea    -0x10(%rbp),%rdx
     9b1:	48 8b 75 c8          	mov    -0x38(%rbp),%rsi
     9b5:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     9b9:	48 89 c7             	mov    %rax,%rdi
     9bc:	e8 b7 fb ff ff       	callq  578 <gettoken>
     9c1:	83 f8 61             	cmp    $0x61,%eax
     9c4:	74 0a                	je     9d0 <parseredirs+0x5d>
      panic("missing file for redirection");
     9c6:	bf f7 18 00 00       	mov    $0x18f7,%edi
     9cb:	e8 90 f9 ff ff       	callq  360 <panic>
    switch (tok) {
     9d0:	8b 45 fc             	mov    -0x4(%rbp),%eax
     9d3:	83 f8 3c             	cmp    $0x3c,%eax
     9d6:	74 0c                	je     9e4 <parseredirs+0x71>
     9d8:	83 f8 3e             	cmp    $0x3e,%eax
     9db:	74 2c                	je     a09 <parseredirs+0x96>
     9dd:	83 f8 2b             	cmp    $0x2b,%eax
     9e0:	74 4c                	je     a2e <parseredirs+0xbb>
     9e2:	eb 6e                	jmp    a52 <parseredirs+0xdf>
    case '<':
      cmd = redircmd(cmd, q, eq, O_RDONLY, 0);
     9e4:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     9e8:	48 8b 75 f0          	mov    -0x10(%rbp),%rsi
     9ec:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     9f0:	41 b8 00 00 00 00    	mov    $0x0,%r8d
     9f6:	b9 00 00 00 00       	mov    $0x0,%ecx
     9fb:	48 89 c7             	mov    %rax,%rdi
     9fe:	e8 ea f9 ff ff       	callq  3ed <redircmd>
     a03:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
      break;
     a07:	eb 49                	jmp    a52 <parseredirs+0xdf>
    case '>':
      cmd = redircmd(cmd, q, eq, O_WRONLY | O_CREATE, 1);
     a09:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     a0d:	48 8b 75 f0          	mov    -0x10(%rbp),%rsi
     a11:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     a15:	41 b8 01 00 00 00    	mov    $0x1,%r8d
     a1b:	b9 01 02 00 00       	mov    $0x201,%ecx
     a20:	48 89 c7             	mov    %rax,%rdi
     a23:	e8 c5 f9 ff ff       	callq  3ed <redircmd>
     a28:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
      break;
     a2c:	eb 24                	jmp    a52 <parseredirs+0xdf>
    case '+': // >>
      cmd = redircmd(cmd, q, eq, O_WRONLY | O_CREATE, 1);
     a2e:	48 8b 55 e8          	mov    -0x18(%rbp),%rdx
     a32:	48 8b 75 f0          	mov    -0x10(%rbp),%rsi
     a36:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     a3a:	41 b8 01 00 00 00    	mov    $0x1,%r8d
     a40:	b9 01 02 00 00       	mov    $0x201,%ecx
     a45:	48 89 c7             	mov    %rax,%rdi
     a48:	e8 a0 f9 ff ff       	callq  3ed <redircmd>
     a4d:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
      break;
     a51:	90                   	nop

struct cmd *parseredirs(struct cmd *cmd, char **ps, char *es) {
  int tok;
  char *q, *eq;

  while (peek(ps, es, "<>")) {
     a52:	48 8b 4d c8          	mov    -0x38(%rbp),%rcx
     a56:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     a5a:	ba 14 19 00 00       	mov    $0x1914,%edx
     a5f:	48 89 ce             	mov    %rcx,%rsi
     a62:	48 89 c7             	mov    %rax,%rdi
     a65:	e8 82 fc ff ff       	callq  6ec <peek>
     a6a:	85 c0                	test   %eax,%eax
     a6c:	0f 85 1a ff ff ff    	jne    98c <parseredirs+0x19>
    case '+': // >>
      cmd = redircmd(cmd, q, eq, O_WRONLY | O_CREATE, 1);
      break;
    }
  }
  return cmd;
     a72:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
}
     a76:	c9                   	leaveq 
     a77:	c3                   	retq   

0000000000000a78 <parseblock>:

struct cmd *parseblock(char **ps, char *es) {
     a78:	55                   	push   %rbp
     a79:	48 89 e5             	mov    %rsp,%rbp
     a7c:	48 83 ec 20          	sub    $0x20,%rsp
     a80:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
     a84:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  struct cmd *cmd;

  if (!peek(ps, es, "("))
     a88:	48 8b 4d e0          	mov    -0x20(%rbp),%rcx
     a8c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     a90:	ba 17 19 00 00       	mov    $0x1917,%edx
     a95:	48 89 ce             	mov    %rcx,%rsi
     a98:	48 89 c7             	mov    %rax,%rdi
     a9b:	e8 4c fc ff ff       	callq  6ec <peek>
     aa0:	85 c0                	test   %eax,%eax
     aa2:	75 0a                	jne    aae <parseblock+0x36>
    panic("parseblock");
     aa4:	bf 19 19 00 00       	mov    $0x1919,%edi
     aa9:	e8 b2 f8 ff ff       	callq  360 <panic>
  gettoken(ps, es, 0, 0);
     aae:	48 8b 75 e0          	mov    -0x20(%rbp),%rsi
     ab2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     ab6:	b9 00 00 00 00       	mov    $0x0,%ecx
     abb:	ba 00 00 00 00       	mov    $0x0,%edx
     ac0:	48 89 c7             	mov    %rax,%rdi
     ac3:	e8 b0 fa ff ff       	callq  578 <gettoken>
  cmd = parseline(ps, es);
     ac8:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     acc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     ad0:	48 89 d6             	mov    %rdx,%rsi
     ad3:	48 89 c7             	mov    %rax,%rdi
     ad6:	e8 38 fd ff ff       	callq  813 <parseline>
     adb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (!peek(ps, es, ")"))
     adf:	48 8b 4d e0          	mov    -0x20(%rbp),%rcx
     ae3:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     ae7:	ba 24 19 00 00       	mov    $0x1924,%edx
     aec:	48 89 ce             	mov    %rcx,%rsi
     aef:	48 89 c7             	mov    %rax,%rdi
     af2:	e8 f5 fb ff ff       	callq  6ec <peek>
     af7:	85 c0                	test   %eax,%eax
     af9:	75 0a                	jne    b05 <parseblock+0x8d>
    panic("syntax - missing )");
     afb:	bf 26 19 00 00       	mov    $0x1926,%edi
     b00:	e8 5b f8 ff ff       	callq  360 <panic>
  gettoken(ps, es, 0, 0);
     b05:	48 8b 75 e0          	mov    -0x20(%rbp),%rsi
     b09:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     b0d:	b9 00 00 00 00       	mov    $0x0,%ecx
     b12:	ba 00 00 00 00       	mov    $0x0,%edx
     b17:	48 89 c7             	mov    %rax,%rdi
     b1a:	e8 59 fa ff ff       	callq  578 <gettoken>
  cmd = parseredirs(cmd, ps, es);
     b1f:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
     b23:	48 8b 4d e8          	mov    -0x18(%rbp),%rcx
     b27:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
     b2b:	48 89 ce             	mov    %rcx,%rsi
     b2e:	48 89 c7             	mov    %rax,%rdi
     b31:	e8 3d fe ff ff       	callq  973 <parseredirs>
     b36:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  return cmd;
     b3a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
     b3e:	c9                   	leaveq 
     b3f:	c3                   	retq   

0000000000000b40 <parseexec>:

struct cmd *parseexec(char **ps, char *es) {
     b40:	55                   	push   %rbp
     b41:	48 89 e5             	mov    %rsp,%rbp
     b44:	48 83 ec 40          	sub    $0x40,%rsp
     b48:	48 89 7d c8          	mov    %rdi,-0x38(%rbp)
     b4c:	48 89 75 c0          	mov    %rsi,-0x40(%rbp)
  char *q, *eq;
  int tok, argc;
  struct execcmd *cmd;
  struct cmd *ret;

  if (peek(ps, es, "("))
     b50:	48 8b 4d c0          	mov    -0x40(%rbp),%rcx
     b54:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     b58:	ba 17 19 00 00       	mov    $0x1917,%edx
     b5d:	48 89 ce             	mov    %rcx,%rsi
     b60:	48 89 c7             	mov    %rax,%rdi
     b63:	e8 84 fb ff ff       	callq  6ec <peek>
     b68:	85 c0                	test   %eax,%eax
     b6a:	74 18                	je     b84 <parseexec+0x44>
    return parseblock(ps, es);
     b6c:	48 8b 55 c0          	mov    -0x40(%rbp),%rdx
     b70:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     b74:	48 89 d6             	mov    %rdx,%rsi
     b77:	48 89 c7             	mov    %rax,%rdi
     b7a:	e8 f9 fe ff ff       	callq  a78 <parseblock>
     b7f:	e9 17 01 00 00       	jmpq   c9b <parseexec+0x15b>

  ret = execcmd();
     b84:	e8 28 f8 ff ff       	callq  3b1 <execcmd>
     b89:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  cmd = (struct execcmd *)ret;
     b8d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     b91:	48 89 45 e8          	mov    %rax,-0x18(%rbp)

  argc = 0;
     b95:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  ret = parseredirs(ret, ps, es);
     b9c:	48 8b 55 c0          	mov    -0x40(%rbp),%rdx
     ba0:	48 8b 4d c8          	mov    -0x38(%rbp),%rcx
     ba4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     ba8:	48 89 ce             	mov    %rcx,%rsi
     bab:	48 89 c7             	mov    %rax,%rdi
     bae:	e8 c0 fd ff ff       	callq  973 <parseredirs>
     bb3:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (!peek(ps, es, "|)&;")) {
     bb7:	e9 8e 00 00 00       	jmpq   c4a <parseexec+0x10a>
    if ((tok = gettoken(ps, es, &q, &eq)) == 0)
     bbc:	48 8d 4d d0          	lea    -0x30(%rbp),%rcx
     bc0:	48 8d 55 d8          	lea    -0x28(%rbp),%rdx
     bc4:	48 8b 75 c0          	mov    -0x40(%rbp),%rsi
     bc8:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     bcc:	48 89 c7             	mov    %rax,%rdi
     bcf:	e8 a4 f9 ff ff       	callq  578 <gettoken>
     bd4:	89 45 e4             	mov    %eax,-0x1c(%rbp)
     bd7:	83 7d e4 00          	cmpl   $0x0,-0x1c(%rbp)
     bdb:	0f 84 8b 00 00 00    	je     c6c <parseexec+0x12c>
      break;
    if (tok != 'a')
     be1:	83 7d e4 61          	cmpl   $0x61,-0x1c(%rbp)
     be5:	74 0a                	je     bf1 <parseexec+0xb1>
      panic("syntax");
     be7:	bf ea 18 00 00       	mov    $0x18ea,%edi
     bec:	e8 6f f7 ff ff       	callq  360 <panic>
    cmd->argv[argc] = q;
     bf1:	48 8b 4d d8          	mov    -0x28(%rbp),%rcx
     bf5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     bf9:	8b 55 fc             	mov    -0x4(%rbp),%edx
     bfc:	48 63 d2             	movslq %edx,%rdx
     bff:	48 89 4c d0 08       	mov    %rcx,0x8(%rax,%rdx,8)
    cmd->eargv[argc] = eq;
     c04:	48 8b 55 d0          	mov    -0x30(%rbp),%rdx
     c08:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c0c:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     c0f:	48 63 c9             	movslq %ecx,%rcx
     c12:	48 83 c1 0a          	add    $0xa,%rcx
     c16:	48 89 54 c8 08       	mov    %rdx,0x8(%rax,%rcx,8)
    argc++;
     c1b:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    if (argc >= MAXARGS)
     c1f:	83 7d fc 09          	cmpl   $0x9,-0x4(%rbp)
     c23:	7e 0a                	jle    c2f <parseexec+0xef>
      panic("too many args");
     c25:	bf 39 19 00 00       	mov    $0x1939,%edi
     c2a:	e8 31 f7 ff ff       	callq  360 <panic>
    ret = parseredirs(ret, ps, es);
     c2f:	48 8b 55 c0          	mov    -0x40(%rbp),%rdx
     c33:	48 8b 4d c8          	mov    -0x38(%rbp),%rcx
     c37:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     c3b:	48 89 ce             	mov    %rcx,%rsi
     c3e:	48 89 c7             	mov    %rax,%rdi
     c41:	e8 2d fd ff ff       	callq  973 <parseredirs>
     c46:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  ret = execcmd();
  cmd = (struct execcmd *)ret;

  argc = 0;
  ret = parseredirs(ret, ps, es);
  while (!peek(ps, es, "|)&;")) {
     c4a:	48 8b 4d c0          	mov    -0x40(%rbp),%rcx
     c4e:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     c52:	ba 47 19 00 00       	mov    $0x1947,%edx
     c57:	48 89 ce             	mov    %rcx,%rsi
     c5a:	48 89 c7             	mov    %rax,%rdi
     c5d:	e8 8a fa ff ff       	callq  6ec <peek>
     c62:	85 c0                	test   %eax,%eax
     c64:	0f 84 52 ff ff ff    	je     bbc <parseexec+0x7c>
     c6a:	eb 01                	jmp    c6d <parseexec+0x12d>
    if ((tok = gettoken(ps, es, &q, &eq)) == 0)
      break;
     c6c:	90                   	nop
    argc++;
    if (argc >= MAXARGS)
      panic("too many args");
    ret = parseredirs(ret, ps, es);
  }
  cmd->argv[argc] = 0;
     c6d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c71:	8b 55 fc             	mov    -0x4(%rbp),%edx
     c74:	48 63 d2             	movslq %edx,%rdx
     c77:	48 c7 44 d0 08 00 00 	movq   $0x0,0x8(%rax,%rdx,8)
     c7e:	00 00 
  cmd->eargv[argc] = 0;
     c80:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     c84:	8b 55 fc             	mov    -0x4(%rbp),%edx
     c87:	48 63 d2             	movslq %edx,%rdx
     c8a:	48 83 c2 0a          	add    $0xa,%rdx
     c8e:	48 c7 44 d0 08 00 00 	movq   $0x0,0x8(%rax,%rdx,8)
     c95:	00 00 
  return ret;
     c97:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
}
     c9b:	c9                   	leaveq 
     c9c:	c3                   	retq   

0000000000000c9d <nulterminate>:

// NUL-terminate all the counted strings.
struct cmd *nulterminate(struct cmd *cmd) {
     c9d:	55                   	push   %rbp
     c9e:	48 89 e5             	mov    %rsp,%rbp
     ca1:	48 83 ec 40          	sub    $0x40,%rsp
     ca5:	48 89 7d c8          	mov    %rdi,-0x38(%rbp)
  struct execcmd *ecmd;
  struct listcmd *lcmd;
  struct pipecmd *pcmd;
  struct redircmd *rcmd;

  if (cmd == 0)
     ca9:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
     cae:	75 0a                	jne    cba <nulterminate+0x1d>
    return 0;
     cb0:	b8 00 00 00 00       	mov    $0x0,%eax
     cb5:	e9 f5 00 00 00       	jmpq   daf <nulterminate+0x112>

  switch (cmd->type) {
     cba:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     cbe:	8b 00                	mov    (%rax),%eax
     cc0:	83 f8 05             	cmp    $0x5,%eax
     cc3:	0f 87 e2 00 00 00    	ja     dab <nulterminate+0x10e>
     cc9:	89 c0                	mov    %eax,%eax
     ccb:	48 8b 04 c5 50 19 00 	mov    0x1950(,%rax,8),%rax
     cd2:	00 
     cd3:	ff e0                	jmpq   *%rax
  case EXEC:
    ecmd = (struct execcmd *)cmd;
     cd5:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     cd9:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    for (i = 0; ecmd->argv[i]; i++)
     cdd:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
     ce4:	eb 1a                	jmp    d00 <nulterminate+0x63>
      *ecmd->eargv[i] = 0;
     ce6:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     cea:	8b 55 fc             	mov    -0x4(%rbp),%edx
     ced:	48 63 d2             	movslq %edx,%rdx
     cf0:	48 83 c2 0a          	add    $0xa,%rdx
     cf4:	48 8b 44 d0 08       	mov    0x8(%rax,%rdx,8),%rax
     cf9:	c6 00 00             	movb   $0x0,(%rax)
    return 0;

  switch (cmd->type) {
  case EXEC:
    ecmd = (struct execcmd *)cmd;
    for (i = 0; ecmd->argv[i]; i++)
     cfc:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
     d00:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     d04:	8b 55 fc             	mov    -0x4(%rbp),%edx
     d07:	48 63 d2             	movslq %edx,%rdx
     d0a:	48 8b 44 d0 08       	mov    0x8(%rax,%rdx,8),%rax
     d0f:	48 85 c0             	test   %rax,%rax
     d12:	75 d2                	jne    ce6 <nulterminate+0x49>
      *ecmd->eargv[i] = 0;
    break;
     d14:	e9 92 00 00 00       	jmpq   dab <nulterminate+0x10e>

  case REDIR:
    rcmd = (struct redircmd *)cmd;
     d19:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     d1d:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
    nulterminate(rcmd->cmd);
     d21:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d25:	48 8b 40 08          	mov    0x8(%rax),%rax
     d29:	48 89 c7             	mov    %rax,%rdi
     d2c:	e8 6c ff ff ff       	callq  c9d <nulterminate>
    *rcmd->efile = 0;
     d31:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
     d35:	48 8b 40 18          	mov    0x18(%rax),%rax
     d39:	c6 00 00             	movb   $0x0,(%rax)
    break;
     d3c:	eb 6d                	jmp    dab <nulterminate+0x10e>

  case PIPE:
    pcmd = (struct pipecmd *)cmd;
     d3e:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     d42:	48 89 45 e0          	mov    %rax,-0x20(%rbp)
    nulterminate(pcmd->left);
     d46:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     d4a:	48 8b 40 08          	mov    0x8(%rax),%rax
     d4e:	48 89 c7             	mov    %rax,%rdi
     d51:	e8 47 ff ff ff       	callq  c9d <nulterminate>
    nulterminate(pcmd->right);
     d56:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
     d5a:	48 8b 40 10          	mov    0x10(%rax),%rax
     d5e:	48 89 c7             	mov    %rax,%rdi
     d61:	e8 37 ff ff ff       	callq  c9d <nulterminate>
    break;
     d66:	eb 43                	jmp    dab <nulterminate+0x10e>

  case LIST:
    lcmd = (struct listcmd *)cmd;
     d68:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     d6c:	48 89 45 d8          	mov    %rax,-0x28(%rbp)
    nulterminate(lcmd->left);
     d70:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     d74:	48 8b 40 08          	mov    0x8(%rax),%rax
     d78:	48 89 c7             	mov    %rax,%rdi
     d7b:	e8 1d ff ff ff       	callq  c9d <nulterminate>
    nulterminate(lcmd->right);
     d80:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
     d84:	48 8b 40 10          	mov    0x10(%rax),%rax
     d88:	48 89 c7             	mov    %rax,%rdi
     d8b:	e8 0d ff ff ff       	callq  c9d <nulterminate>
    break;
     d90:	eb 19                	jmp    dab <nulterminate+0x10e>

  case BACK:
    bcmd = (struct backcmd *)cmd;
     d92:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
     d96:	48 89 45 d0          	mov    %rax,-0x30(%rbp)
    nulterminate(bcmd->cmd);
     d9a:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
     d9e:	48 8b 40 08          	mov    0x8(%rax),%rax
     da2:	48 89 c7             	mov    %rax,%rdi
     da5:	e8 f3 fe ff ff       	callq  c9d <nulterminate>
    break;
     daa:	90                   	nop
  }
  return cmd;
     dab:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
}
     daf:	c9                   	leaveq 
     db0:	c3                   	retq   

0000000000000db1 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
     db1:	55                   	push   %rbp
     db2:	48 89 e5             	mov    %rsp,%rbp
     db5:	48 83 ec 10          	sub    $0x10,%rsp
     db9:	89 7d fc             	mov    %edi,-0x4(%rbp)
     dbc:	89 f0                	mov    %esi,%eax
     dbe:	88 45 f8             	mov    %al,-0x8(%rbp)
     dc1:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
     dc5:	8b 45 fc             	mov    -0x4(%rbp),%eax
     dc8:	ba 01 00 00 00       	mov    $0x1,%edx
     dcd:	48 89 ce             	mov    %rcx,%rsi
     dd0:	89 c7                	mov    %eax,%edi
     dd2:	e8 57 07 00 00       	callq  152e <write>
     dd7:	90                   	nop
     dd8:	c9                   	leaveq 
     dd9:	c3                   	retq   

0000000000000dda <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
     dda:	55                   	push   %rbp
     ddb:	48 89 e5             	mov    %rsp,%rbp
     dde:	48 83 ec 40          	sub    $0x40,%rsp
     de2:	89 7d cc             	mov    %edi,-0x34(%rbp)
     de5:	89 75 c8             	mov    %esi,-0x38(%rbp)
     de8:	89 55 c4             	mov    %edx,-0x3c(%rbp)
     deb:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
     dee:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     df2:	74 1f                	je     e13 <printint64+0x39>
     df4:	8b 45 c8             	mov    -0x38(%rbp),%eax
     df7:	c1 e8 1f             	shr    $0x1f,%eax
     dfa:	0f b6 c0             	movzbl %al,%eax
     dfd:	89 45 c0             	mov    %eax,-0x40(%rbp)
     e00:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     e04:	74 0d                	je     e13 <printint64+0x39>
    x = -xx;
     e06:	8b 45 c8             	mov    -0x38(%rbp),%eax
     e09:	f7 d8                	neg    %eax
     e0b:	48 98                	cltq   
     e0d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     e11:	eb 09                	jmp    e1c <printint64+0x42>
  else
    x = xx;
     e13:	8b 45 c8             	mov    -0x38(%rbp),%eax
     e16:	48 98                	cltq   
     e18:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
     e1c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     e23:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     e26:	8d 41 01             	lea    0x1(%rcx),%eax
     e29:	89 45 fc             	mov    %eax,-0x4(%rbp)
     e2c:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     e2f:	48 63 f0             	movslq %eax,%rsi
     e32:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e36:	ba 00 00 00 00       	mov    $0x0,%edx
     e3b:	48 f7 f6             	div    %rsi
     e3e:	48 89 d0             	mov    %rdx,%rax
     e41:	0f b6 90 40 1e 00 00 	movzbl 0x1e40(%rax),%edx
     e48:	48 63 c1             	movslq %ecx,%rax
     e4b:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
     e4f:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     e52:	48 63 f8             	movslq %eax,%rdi
     e55:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
     e59:	ba 00 00 00 00       	mov    $0x0,%edx
     e5e:	48 f7 f7             	div    %rdi
     e61:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
     e65:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
     e6a:	75 b7                	jne    e23 <printint64+0x49>

  if (sgn)
     e6c:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     e70:	74 2b                	je     e9d <printint64+0xc3>
    buf[i++] = '-';
     e72:	8b 45 fc             	mov    -0x4(%rbp),%eax
     e75:	8d 50 01             	lea    0x1(%rax),%edx
     e78:	89 55 fc             	mov    %edx,-0x4(%rbp)
     e7b:	48 98                	cltq   
     e7d:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
     e82:	eb 19                	jmp    e9d <printint64+0xc3>
    putc(fd, buf[i]);
     e84:	8b 45 fc             	mov    -0x4(%rbp),%eax
     e87:	48 98                	cltq   
     e89:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
     e8e:	0f be d0             	movsbl %al,%edx
     e91:	8b 45 cc             	mov    -0x34(%rbp),%eax
     e94:	89 d6                	mov    %edx,%esi
     e96:	89 c7                	mov    %eax,%edi
     e98:	e8 14 ff ff ff       	callq  db1 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
     e9d:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     ea1:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     ea5:	79 dd                	jns    e84 <printint64+0xaa>
    putc(fd, buf[i]);
}
     ea7:	90                   	nop
     ea8:	c9                   	leaveq 
     ea9:	c3                   	retq   

0000000000000eaa <printint>:

static void printint(int fd, int xx, int base, int sgn) {
     eaa:	55                   	push   %rbp
     eab:	48 89 e5             	mov    %rsp,%rbp
     eae:	48 83 ec 30          	sub    $0x30,%rsp
     eb2:	89 7d dc             	mov    %edi,-0x24(%rbp)
     eb5:	89 75 d8             	mov    %esi,-0x28(%rbp)
     eb8:	89 55 d4             	mov    %edx,-0x2c(%rbp)
     ebb:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
     ebe:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
     ec5:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
     ec9:	74 17                	je     ee2 <printint+0x38>
     ecb:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
     ecf:	79 11                	jns    ee2 <printint+0x38>
    neg = 1;
     ed1:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
     ed8:	8b 45 d8             	mov    -0x28(%rbp),%eax
     edb:	f7 d8                	neg    %eax
     edd:	89 45 f4             	mov    %eax,-0xc(%rbp)
     ee0:	eb 06                	jmp    ee8 <printint+0x3e>
  } else {
    x = xx;
     ee2:	8b 45 d8             	mov    -0x28(%rbp),%eax
     ee5:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
     ee8:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
     eef:	8b 4d fc             	mov    -0x4(%rbp),%ecx
     ef2:	8d 41 01             	lea    0x1(%rcx),%eax
     ef5:	89 45 fc             	mov    %eax,-0x4(%rbp)
     ef8:	8b 75 d4             	mov    -0x2c(%rbp),%esi
     efb:	8b 45 f4             	mov    -0xc(%rbp),%eax
     efe:	ba 00 00 00 00       	mov    $0x0,%edx
     f03:	f7 f6                	div    %esi
     f05:	89 d0                	mov    %edx,%eax
     f07:	89 c0                	mov    %eax,%eax
     f09:	0f b6 90 60 1e 00 00 	movzbl 0x1e60(%rax),%edx
     f10:	48 63 c1             	movslq %ecx,%rax
     f13:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
     f17:	8b 7d d4             	mov    -0x2c(%rbp),%edi
     f1a:	8b 45 f4             	mov    -0xc(%rbp),%eax
     f1d:	ba 00 00 00 00       	mov    $0x0,%edx
     f22:	f7 f7                	div    %edi
     f24:	89 45 f4             	mov    %eax,-0xc(%rbp)
     f27:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
     f2b:	75 c2                	jne    eef <printint+0x45>
  if (neg)
     f2d:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
     f31:	74 2b                	je     f5e <printint+0xb4>
    buf[i++] = '-';
     f33:	8b 45 fc             	mov    -0x4(%rbp),%eax
     f36:	8d 50 01             	lea    0x1(%rax),%edx
     f39:	89 55 fc             	mov    %edx,-0x4(%rbp)
     f3c:	48 98                	cltq   
     f3e:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
     f43:	eb 19                	jmp    f5e <printint+0xb4>
    putc(fd, buf[i]);
     f45:	8b 45 fc             	mov    -0x4(%rbp),%eax
     f48:	48 98                	cltq   
     f4a:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
     f4f:	0f be d0             	movsbl %al,%edx
     f52:	8b 45 dc             	mov    -0x24(%rbp),%eax
     f55:	89 d6                	mov    %edx,%esi
     f57:	89 c7                	mov    %eax,%edi
     f59:	e8 53 fe ff ff       	callq  db1 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
     f5e:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
     f62:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
     f66:	79 dd                	jns    f45 <printint+0x9b>
    putc(fd, buf[i]);
}
     f68:	90                   	nop
     f69:	c9                   	leaveq 
     f6a:	c3                   	retq   

0000000000000f6b <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
     f6b:	55                   	push   %rbp
     f6c:	48 89 e5             	mov    %rsp,%rbp
     f6f:	48 83 ec 70          	sub    $0x70,%rsp
     f73:	89 7d 9c             	mov    %edi,-0x64(%rbp)
     f76:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
     f7a:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
     f7e:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
     f82:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
     f86:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
     f8a:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
     f91:	48 8d 45 10          	lea    0x10(%rbp),%rax
     f95:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
     f99:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
     f9d:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
     fa1:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
     fa8:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
     faf:	e9 68 02 00 00       	jmpq   121c <printf+0x2b1>
    c = fmt[i] & 0xff;
     fb4:	8b 45 c4             	mov    -0x3c(%rbp),%eax
     fb7:	48 63 d0             	movslq %eax,%rdx
     fba:	48 8b 45 90          	mov    -0x70(%rbp),%rax
     fbe:	48 01 d0             	add    %rdx,%rax
     fc1:	0f b6 00             	movzbl (%rax),%eax
     fc4:	0f be c0             	movsbl %al,%eax
     fc7:	25 ff 00 00 00       	and    $0xff,%eax
     fcc:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
     fcf:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
     fd3:	75 30                	jne    1005 <printf+0x9a>
      if (c == '%') {
     fd5:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
     fd9:	75 13                	jne    fee <printf+0x83>
        state = '%';
     fdb:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
     fe2:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
     fe9:	e9 2a 02 00 00       	jmpq   1218 <printf+0x2ad>
      } else {
        putc(fd, c);
     fee:	8b 45 b8             	mov    -0x48(%rbp),%eax
     ff1:	0f be d0             	movsbl %al,%edx
     ff4:	8b 45 9c             	mov    -0x64(%rbp),%eax
     ff7:	89 d6                	mov    %edx,%esi
     ff9:	89 c7                	mov    %eax,%edi
     ffb:	e8 b1 fd ff ff       	callq  db1 <putc>
    1000:	e9 13 02 00 00       	jmpq   1218 <printf+0x2ad>
      }
    } else if (state == '%') {
    1005:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
    1009:	0f 85 09 02 00 00    	jne    1218 <printf+0x2ad>
      if (c == 'l') {
    100f:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
    1013:	75 0c                	jne    1021 <printf+0xb6>
        lflag = 1;
    1015:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
    101c:	e9 f7 01 00 00       	jmpq   1218 <printf+0x2ad>
      } else if (c == 'd') {
    1021:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
    1025:	0f 85 95 00 00 00    	jne    10c0 <printf+0x155>
        if (lflag == 1)
    102b:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    102f:	75 49                	jne    107a <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
    1031:	8b 45 a0             	mov    -0x60(%rbp),%eax
    1034:	83 f8 30             	cmp    $0x30,%eax
    1037:	73 17                	jae    1050 <printf+0xe5>
    1039:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    103d:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1040:	89 d2                	mov    %edx,%edx
    1042:	48 01 d0             	add    %rdx,%rax
    1045:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1048:	83 c2 08             	add    $0x8,%edx
    104b:	89 55 a0             	mov    %edx,-0x60(%rbp)
    104e:	eb 0c                	jmp    105c <printf+0xf1>
    1050:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1054:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1058:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    105c:	48 8b 00             	mov    (%rax),%rax
    105f:	89 c6                	mov    %eax,%esi
    1061:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1064:	b9 01 00 00 00       	mov    $0x1,%ecx
    1069:	ba 0a 00 00 00       	mov    $0xa,%edx
    106e:	89 c7                	mov    %eax,%edi
    1070:	e8 65 fd ff ff       	callq  dda <printint64>
    1075:	e9 97 01 00 00       	jmpq   1211 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
    107a:	8b 45 a0             	mov    -0x60(%rbp),%eax
    107d:	83 f8 30             	cmp    $0x30,%eax
    1080:	73 17                	jae    1099 <printf+0x12e>
    1082:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1086:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1089:	89 d2                	mov    %edx,%edx
    108b:	48 01 d0             	add    %rdx,%rax
    108e:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1091:	83 c2 08             	add    $0x8,%edx
    1094:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1097:	eb 0c                	jmp    10a5 <printf+0x13a>
    1099:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    109d:	48 8d 50 08          	lea    0x8(%rax),%rdx
    10a1:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    10a5:	8b 30                	mov    (%rax),%esi
    10a7:	8b 45 9c             	mov    -0x64(%rbp),%eax
    10aa:	b9 01 00 00 00       	mov    $0x1,%ecx
    10af:	ba 0a 00 00 00       	mov    $0xa,%edx
    10b4:	89 c7                	mov    %eax,%edi
    10b6:	e8 ef fd ff ff       	callq  eaa <printint>
    10bb:	e9 51 01 00 00       	jmpq   1211 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
    10c0:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
    10c4:	74 0a                	je     10d0 <printf+0x165>
    10c6:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
    10ca:	0f 85 95 00 00 00    	jne    1165 <printf+0x1fa>
        if (lflag == 1)
    10d0:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
    10d4:	75 49                	jne    111f <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
    10d6:	8b 45 a0             	mov    -0x60(%rbp),%eax
    10d9:	83 f8 30             	cmp    $0x30,%eax
    10dc:	73 17                	jae    10f5 <printf+0x18a>
    10de:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    10e2:	8b 55 a0             	mov    -0x60(%rbp),%edx
    10e5:	89 d2                	mov    %edx,%edx
    10e7:	48 01 d0             	add    %rdx,%rax
    10ea:	8b 55 a0             	mov    -0x60(%rbp),%edx
    10ed:	83 c2 08             	add    $0x8,%edx
    10f0:	89 55 a0             	mov    %edx,-0x60(%rbp)
    10f3:	eb 0c                	jmp    1101 <printf+0x196>
    10f5:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    10f9:	48 8d 50 08          	lea    0x8(%rax),%rdx
    10fd:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1101:	48 8b 00             	mov    (%rax),%rax
    1104:	89 c6                	mov    %eax,%esi
    1106:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1109:	b9 00 00 00 00       	mov    $0x0,%ecx
    110e:	ba 10 00 00 00       	mov    $0x10,%edx
    1113:	89 c7                	mov    %eax,%edi
    1115:	e8 c0 fc ff ff       	callq  dda <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    111a:	e9 f2 00 00 00       	jmpq   1211 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
    111f:	8b 45 a0             	mov    -0x60(%rbp),%eax
    1122:	83 f8 30             	cmp    $0x30,%eax
    1125:	73 17                	jae    113e <printf+0x1d3>
    1127:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    112b:	8b 55 a0             	mov    -0x60(%rbp),%edx
    112e:	89 d2                	mov    %edx,%edx
    1130:	48 01 d0             	add    %rdx,%rax
    1133:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1136:	83 c2 08             	add    $0x8,%edx
    1139:	89 55 a0             	mov    %edx,-0x60(%rbp)
    113c:	eb 0c                	jmp    114a <printf+0x1df>
    113e:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    1142:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1146:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    114a:	8b 30                	mov    (%rax),%esi
    114c:	8b 45 9c             	mov    -0x64(%rbp),%eax
    114f:	b9 00 00 00 00       	mov    $0x0,%ecx
    1154:	ba 10 00 00 00       	mov    $0x10,%edx
    1159:	89 c7                	mov    %eax,%edi
    115b:	e8 4a fd ff ff       	callq  eaa <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
    1160:	e9 ac 00 00 00       	jmpq   1211 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
    1165:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
    1169:	75 6b                	jne    11d6 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
    116b:	8b 45 a0             	mov    -0x60(%rbp),%eax
    116e:	83 f8 30             	cmp    $0x30,%eax
    1171:	73 17                	jae    118a <printf+0x21f>
    1173:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
    1177:	8b 55 a0             	mov    -0x60(%rbp),%edx
    117a:	89 d2                	mov    %edx,%edx
    117c:	48 01 d0             	add    %rdx,%rax
    117f:	8b 55 a0             	mov    -0x60(%rbp),%edx
    1182:	83 c2 08             	add    $0x8,%edx
    1185:	89 55 a0             	mov    %edx,-0x60(%rbp)
    1188:	eb 0c                	jmp    1196 <printf+0x22b>
    118a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
    118e:	48 8d 50 08          	lea    0x8(%rax),%rdx
    1192:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
    1196:	48 8b 00             	mov    (%rax),%rax
    1199:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
    119d:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
    11a2:	75 25                	jne    11c9 <printf+0x25e>
          s = "(null)";
    11a4:	48 c7 45 c8 80 19 00 	movq   $0x1980,-0x38(%rbp)
    11ab:	00 
        for (; *s; s++)
    11ac:	eb 1b                	jmp    11c9 <printf+0x25e>
          putc(fd, *s);
    11ae:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    11b2:	0f b6 00             	movzbl (%rax),%eax
    11b5:	0f be d0             	movsbl %al,%edx
    11b8:	8b 45 9c             	mov    -0x64(%rbp),%eax
    11bb:	89 d6                	mov    %edx,%esi
    11bd:	89 c7                	mov    %eax,%edi
    11bf:	e8 ed fb ff ff       	callq  db1 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
    11c4:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
    11c9:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
    11cd:	0f b6 00             	movzbl (%rax),%eax
    11d0:	84 c0                	test   %al,%al
    11d2:	75 da                	jne    11ae <printf+0x243>
    11d4:	eb 3b                	jmp    1211 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
    11d6:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
    11da:	75 14                	jne    11f0 <printf+0x285>
        putc(fd, c);
    11dc:	8b 45 b8             	mov    -0x48(%rbp),%eax
    11df:	0f be d0             	movsbl %al,%edx
    11e2:	8b 45 9c             	mov    -0x64(%rbp),%eax
    11e5:	89 d6                	mov    %edx,%esi
    11e7:	89 c7                	mov    %eax,%edi
    11e9:	e8 c3 fb ff ff       	callq  db1 <putc>
    11ee:	eb 21                	jmp    1211 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
    11f0:	8b 45 9c             	mov    -0x64(%rbp),%eax
    11f3:	be 25 00 00 00       	mov    $0x25,%esi
    11f8:	89 c7                	mov    %eax,%edi
    11fa:	e8 b2 fb ff ff       	callq  db1 <putc>
        putc(fd, c);
    11ff:	8b 45 b8             	mov    -0x48(%rbp),%eax
    1202:	0f be d0             	movsbl %al,%edx
    1205:	8b 45 9c             	mov    -0x64(%rbp),%eax
    1208:	89 d6                	mov    %edx,%esi
    120a:	89 c7                	mov    %eax,%edi
    120c:	e8 a0 fb ff ff       	callq  db1 <putc>
      }
      state = 0;
    1211:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
    1218:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
    121c:	8b 45 c4             	mov    -0x3c(%rbp),%eax
    121f:	48 63 d0             	movslq %eax,%rdx
    1222:	48 8b 45 90          	mov    -0x70(%rbp),%rax
    1226:	48 01 d0             	add    %rdx,%rax
    1229:	0f b6 00             	movzbl (%rax),%eax
    122c:	84 c0                	test   %al,%al
    122e:	0f 85 80 fd ff ff    	jne    fb4 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
    1234:	90                   	nop
    1235:	c9                   	leaveq 
    1236:	c3                   	retq   

0000000000001237 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
    1237:	55                   	push   %rbp
    1238:	48 89 e5             	mov    %rsp,%rbp
    123b:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    123f:	89 75 f4             	mov    %esi,-0xc(%rbp)
    1242:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
    1245:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
    1249:	8b 55 f0             	mov    -0x10(%rbp),%edx
    124c:	8b 45 f4             	mov    -0xc(%rbp),%eax
    124f:	48 89 ce             	mov    %rcx,%rsi
    1252:	48 89 f7             	mov    %rsi,%rdi
    1255:	89 d1                	mov    %edx,%ecx
    1257:	fc                   	cld    
    1258:	f3 aa                	rep stos %al,%es:(%rdi)
    125a:	89 ca                	mov    %ecx,%edx
    125c:	48 89 fe             	mov    %rdi,%rsi
    125f:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
    1263:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
    1266:	90                   	nop
    1267:	5d                   	pop    %rbp
    1268:	c3                   	retq   

0000000000001269 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
    1269:	55                   	push   %rbp
    126a:	48 89 e5             	mov    %rsp,%rbp
    126d:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1271:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
    1275:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1279:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
    127d:	90                   	nop
    127e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1282:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1286:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    128a:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    128e:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    1292:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
    1296:	0f b6 12             	movzbl (%rdx),%edx
    1299:	88 10                	mov    %dl,(%rax)
    129b:	0f b6 00             	movzbl (%rax),%eax
    129e:	84 c0                	test   %al,%al
    12a0:	75 dc                	jne    127e <strcpy+0x15>
    ;
  return os;
    12a2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    12a6:	5d                   	pop    %rbp
    12a7:	c3                   	retq   

00000000000012a8 <strcmp>:

int strcmp(const char *p, const char *q) {
    12a8:	55                   	push   %rbp
    12a9:	48 89 e5             	mov    %rsp,%rbp
    12ac:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    12b0:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
    12b4:	eb 0a                	jmp    12c0 <strcmp+0x18>
    p++, q++;
    12b6:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    12bb:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
    12c0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    12c4:	0f b6 00             	movzbl (%rax),%eax
    12c7:	84 c0                	test   %al,%al
    12c9:	74 12                	je     12dd <strcmp+0x35>
    12cb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    12cf:	0f b6 10             	movzbl (%rax),%edx
    12d2:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    12d6:	0f b6 00             	movzbl (%rax),%eax
    12d9:	38 c2                	cmp    %al,%dl
    12db:	74 d9                	je     12b6 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
    12dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    12e1:	0f b6 00             	movzbl (%rax),%eax
    12e4:	0f b6 d0             	movzbl %al,%edx
    12e7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    12eb:	0f b6 00             	movzbl (%rax),%eax
    12ee:	0f b6 c0             	movzbl %al,%eax
    12f1:	29 c2                	sub    %eax,%edx
    12f3:	89 d0                	mov    %edx,%eax
}
    12f5:	5d                   	pop    %rbp
    12f6:	c3                   	retq   

00000000000012f7 <strlen>:

uint strlen(char *s) {
    12f7:	55                   	push   %rbp
    12f8:	48 89 e5             	mov    %rsp,%rbp
    12fb:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
    12ff:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    1306:	eb 04                	jmp    130c <strlen+0x15>
    1308:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
    130c:	8b 45 fc             	mov    -0x4(%rbp),%eax
    130f:	48 63 d0             	movslq %eax,%rdx
    1312:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1316:	48 01 d0             	add    %rdx,%rax
    1319:	0f b6 00             	movzbl (%rax),%eax
    131c:	84 c0                	test   %al,%al
    131e:	75 e8                	jne    1308 <strlen+0x11>
    ;
  return n;
    1320:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    1323:	5d                   	pop    %rbp
    1324:	c3                   	retq   

0000000000001325 <memset>:

void *memset(void *dst, int c, uint n) {
    1325:	55                   	push   %rbp
    1326:	48 89 e5             	mov    %rsp,%rbp
    1329:	48 83 ec 10          	sub    $0x10,%rsp
    132d:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    1331:	89 75 f4             	mov    %esi,-0xc(%rbp)
    1334:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
    1337:	8b 55 f0             	mov    -0x10(%rbp),%edx
    133a:	8b 4d f4             	mov    -0xc(%rbp),%ecx
    133d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1341:	89 ce                	mov    %ecx,%esi
    1343:	48 89 c7             	mov    %rax,%rdi
    1346:	e8 ec fe ff ff       	callq  1237 <stosb>
  return dst;
    134b:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
    134f:	c9                   	leaveq 
    1350:	c3                   	retq   

0000000000001351 <strchr>:

char *strchr(const char *s, char c) {
    1351:	55                   	push   %rbp
    1352:	48 89 e5             	mov    %rsp,%rbp
    1355:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
    1359:	89 f0                	mov    %esi,%eax
    135b:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
    135e:	eb 17                	jmp    1377 <strchr+0x26>
    if (*s == c)
    1360:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1364:	0f b6 00             	movzbl (%rax),%eax
    1367:	3a 45 f4             	cmp    -0xc(%rbp),%al
    136a:	75 06                	jne    1372 <strchr+0x21>
      return (char *)s;
    136c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1370:	eb 15                	jmp    1387 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
    1372:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
    1377:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    137b:	0f b6 00             	movzbl (%rax),%eax
    137e:	84 c0                	test   %al,%al
    1380:	75 de                	jne    1360 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
    1382:	b8 00 00 00 00       	mov    $0x0,%eax
}
    1387:	5d                   	pop    %rbp
    1388:	c3                   	retq   

0000000000001389 <gets>:

char *gets(char *buf, int max) {
    1389:	55                   	push   %rbp
    138a:	48 89 e5             	mov    %rsp,%rbp
    138d:	48 83 ec 20          	sub    $0x20,%rsp
    1391:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1395:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    1398:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
    139f:	eb 48                	jmp    13e9 <gets+0x60>
    cc = read(0, &c, 1);
    13a1:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
    13a5:	ba 01 00 00 00       	mov    $0x1,%edx
    13aa:	48 89 c6             	mov    %rax,%rsi
    13ad:	bf 00 00 00 00       	mov    $0x0,%edi
    13b2:	e8 6f 01 00 00       	callq  1526 <read>
    13b7:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
    13ba:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
    13be:	7e 36                	jle    13f6 <gets+0x6d>
      break;
    buf[i++] = c;
    13c0:	8b 45 fc             	mov    -0x4(%rbp),%eax
    13c3:	8d 50 01             	lea    0x1(%rax),%edx
    13c6:	89 55 fc             	mov    %edx,-0x4(%rbp)
    13c9:	48 63 d0             	movslq %eax,%rdx
    13cc:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    13d0:	48 01 c2             	add    %rax,%rdx
    13d3:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    13d7:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
    13d9:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    13dd:	3c 0a                	cmp    $0xa,%al
    13df:	74 16                	je     13f7 <gets+0x6e>
    13e1:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
    13e5:	3c 0d                	cmp    $0xd,%al
    13e7:	74 0e                	je     13f7 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
    13e9:	8b 45 fc             	mov    -0x4(%rbp),%eax
    13ec:	83 c0 01             	add    $0x1,%eax
    13ef:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
    13f2:	7c ad                	jl     13a1 <gets+0x18>
    13f4:	eb 01                	jmp    13f7 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
    13f6:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
    13f7:	8b 45 fc             	mov    -0x4(%rbp),%eax
    13fa:	48 63 d0             	movslq %eax,%rdx
    13fd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1401:	48 01 d0             	add    %rdx,%rax
    1404:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
    1407:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
    140b:	c9                   	leaveq 
    140c:	c3                   	retq   

000000000000140d <stat>:

int stat(char *n, struct stat *st) {
    140d:	55                   	push   %rbp
    140e:	48 89 e5             	mov    %rsp,%rbp
    1411:	48 83 ec 20          	sub    $0x20,%rsp
    1415:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    1419:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
    141d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1421:	be 00 00 00 00       	mov    $0x0,%esi
    1426:	48 89 c7             	mov    %rax,%rdi
    1429:	e8 20 01 00 00       	callq  154e <open>
    142e:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
    1431:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
    1435:	79 07                	jns    143e <stat+0x31>
    return -1;
    1437:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
    143c:	eb 21                	jmp    145f <stat+0x52>
  r = fstat(fd, st);
    143e:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
    1442:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1445:	48 89 d6             	mov    %rdx,%rsi
    1448:	89 c7                	mov    %eax,%edi
    144a:	e8 17 01 00 00       	callq  1566 <fstat>
    144f:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
    1452:	8b 45 fc             	mov    -0x4(%rbp),%eax
    1455:	89 c7                	mov    %eax,%edi
    1457:	e8 da 00 00 00       	callq  1536 <close>
  return r;
    145c:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
    145f:	c9                   	leaveq 
    1460:	c3                   	retq   

0000000000001461 <atoi>:

int atoi(const char *s) {
    1461:	55                   	push   %rbp
    1462:	48 89 e5             	mov    %rsp,%rbp
    1465:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
    1469:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
    1470:	eb 28                	jmp    149a <atoi+0x39>
    n = n * 10 + *s++ - '0';
    1472:	8b 55 fc             	mov    -0x4(%rbp),%edx
    1475:	89 d0                	mov    %edx,%eax
    1477:	c1 e0 02             	shl    $0x2,%eax
    147a:	01 d0                	add    %edx,%eax
    147c:	01 c0                	add    %eax,%eax
    147e:	89 c1                	mov    %eax,%ecx
    1480:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1484:	48 8d 50 01          	lea    0x1(%rax),%rdx
    1488:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
    148c:	0f b6 00             	movzbl (%rax),%eax
    148f:	0f be c0             	movsbl %al,%eax
    1492:	01 c8                	add    %ecx,%eax
    1494:	83 e8 30             	sub    $0x30,%eax
    1497:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
    149a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    149e:	0f b6 00             	movzbl (%rax),%eax
    14a1:	3c 2f                	cmp    $0x2f,%al
    14a3:	7e 0b                	jle    14b0 <atoi+0x4f>
    14a5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    14a9:	0f b6 00             	movzbl (%rax),%eax
    14ac:	3c 39                	cmp    $0x39,%al
    14ae:	7e c2                	jle    1472 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
    14b0:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
    14b3:	5d                   	pop    %rbp
    14b4:	c3                   	retq   

00000000000014b5 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
    14b5:	55                   	push   %rbp
    14b6:	48 89 e5             	mov    %rsp,%rbp
    14b9:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
    14bd:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
    14c1:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
    14c4:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    14c8:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
    14cc:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
    14d0:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
    14d4:	eb 1d                	jmp    14f3 <memmove+0x3e>
    *dst++ = *src++;
    14d6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    14da:	48 8d 50 01          	lea    0x1(%rax),%rdx
    14de:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
    14e2:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    14e6:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
    14ea:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
    14ee:	0f b6 12             	movzbl (%rdx),%edx
    14f1:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
    14f3:	8b 45 dc             	mov    -0x24(%rbp),%eax
    14f6:	8d 50 ff             	lea    -0x1(%rax),%edx
    14f9:	89 55 dc             	mov    %edx,-0x24(%rbp)
    14fc:	85 c0                	test   %eax,%eax
    14fe:	7f d6                	jg     14d6 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
    1500:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    1504:	5d                   	pop    %rbp
    1505:	c3                   	retq   

0000000000001506 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
    1506:	b8 01 00 00 00       	mov    $0x1,%eax
    150b:	cd 40                	int    $0x40
    150d:	c3                   	retq   

000000000000150e <exit>:
SYSCALL(exit)
    150e:	b8 02 00 00 00       	mov    $0x2,%eax
    1513:	cd 40                	int    $0x40
    1515:	c3                   	retq   

0000000000001516 <wait>:
SYSCALL(wait)
    1516:	b8 03 00 00 00       	mov    $0x3,%eax
    151b:	cd 40                	int    $0x40
    151d:	c3                   	retq   

000000000000151e <pipe>:
SYSCALL(pipe)
    151e:	b8 04 00 00 00       	mov    $0x4,%eax
    1523:	cd 40                	int    $0x40
    1525:	c3                   	retq   

0000000000001526 <read>:
SYSCALL(read)
    1526:	b8 05 00 00 00       	mov    $0x5,%eax
    152b:	cd 40                	int    $0x40
    152d:	c3                   	retq   

000000000000152e <write>:
SYSCALL(write)
    152e:	b8 10 00 00 00       	mov    $0x10,%eax
    1533:	cd 40                	int    $0x40
    1535:	c3                   	retq   

0000000000001536 <close>:
SYSCALL(close)
    1536:	b8 15 00 00 00       	mov    $0x15,%eax
    153b:	cd 40                	int    $0x40
    153d:	c3                   	retq   

000000000000153e <kill>:
SYSCALL(kill)
    153e:	b8 06 00 00 00       	mov    $0x6,%eax
    1543:	cd 40                	int    $0x40
    1545:	c3                   	retq   

0000000000001546 <exec>:
SYSCALL(exec)
    1546:	b8 07 00 00 00       	mov    $0x7,%eax
    154b:	cd 40                	int    $0x40
    154d:	c3                   	retq   

000000000000154e <open>:
SYSCALL(open)
    154e:	b8 0f 00 00 00       	mov    $0xf,%eax
    1553:	cd 40                	int    $0x40
    1555:	c3                   	retq   

0000000000001556 <mknod>:
SYSCALL(mknod)
    1556:	b8 11 00 00 00       	mov    $0x11,%eax
    155b:	cd 40                	int    $0x40
    155d:	c3                   	retq   

000000000000155e <unlink>:
SYSCALL(unlink)
    155e:	b8 12 00 00 00       	mov    $0x12,%eax
    1563:	cd 40                	int    $0x40
    1565:	c3                   	retq   

0000000000001566 <fstat>:
SYSCALL(fstat)
    1566:	b8 08 00 00 00       	mov    $0x8,%eax
    156b:	cd 40                	int    $0x40
    156d:	c3                   	retq   

000000000000156e <link>:
SYSCALL(link)
    156e:	b8 13 00 00 00       	mov    $0x13,%eax
    1573:	cd 40                	int    $0x40
    1575:	c3                   	retq   

0000000000001576 <mkdir>:
SYSCALL(mkdir)
    1576:	b8 14 00 00 00       	mov    $0x14,%eax
    157b:	cd 40                	int    $0x40
    157d:	c3                   	retq   

000000000000157e <chdir>:
SYSCALL(chdir)
    157e:	b8 09 00 00 00       	mov    $0x9,%eax
    1583:	cd 40                	int    $0x40
    1585:	c3                   	retq   

0000000000001586 <dup>:
SYSCALL(dup)
    1586:	b8 0a 00 00 00       	mov    $0xa,%eax
    158b:	cd 40                	int    $0x40
    158d:	c3                   	retq   

000000000000158e <getpid>:
SYSCALL(getpid)
    158e:	b8 0b 00 00 00       	mov    $0xb,%eax
    1593:	cd 40                	int    $0x40
    1595:	c3                   	retq   

0000000000001596 <sbrk>:
SYSCALL(sbrk)
    1596:	b8 0c 00 00 00       	mov    $0xc,%eax
    159b:	cd 40                	int    $0x40
    159d:	c3                   	retq   

000000000000159e <sleep>:
SYSCALL(sleep)
    159e:	b8 0d 00 00 00       	mov    $0xd,%eax
    15a3:	cd 40                	int    $0x40
    15a5:	c3                   	retq   

00000000000015a6 <uptime>:
SYSCALL(uptime)
    15a6:	b8 0e 00 00 00       	mov    $0xe,%eax
    15ab:	cd 40                	int    $0x40
    15ad:	c3                   	retq   

00000000000015ae <sysinfo>:
SYSCALL(sysinfo)
    15ae:	b8 16 00 00 00       	mov    $0x16,%eax
    15b3:	cd 40                	int    $0x40
    15b5:	c3                   	retq   

00000000000015b6 <crashn>:
SYSCALL(crashn)
    15b6:	b8 17 00 00 00       	mov    $0x17,%eax
    15bb:	cd 40                	int    $0x40
    15bd:	c3                   	retq   

00000000000015be <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
    15be:	55                   	push   %rbp
    15bf:	48 89 e5             	mov    %rsp,%rbp
    15c2:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
    15c6:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
    15ca:	48 83 e8 10          	sub    $0x10,%rax
    15ce:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    15d2:	48 8b 05 27 09 00 00 	mov    0x927(%rip),%rax        # 1f00 <freep>
    15d9:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    15dd:	eb 2f                	jmp    160e <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
    15df:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    15e3:	48 8b 00             	mov    (%rax),%rax
    15e6:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    15ea:	77 17                	ja     1603 <free+0x45>
    15ec:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    15f0:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    15f4:	77 2f                	ja     1625 <free+0x67>
    15f6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    15fa:	48 8b 00             	mov    (%rax),%rax
    15fd:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1601:	77 22                	ja     1625 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
    1603:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1607:	48 8b 00             	mov    (%rax),%rax
    160a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    160e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1612:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
    1616:	76 c7                	jbe    15df <free+0x21>
    1618:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    161c:	48 8b 00             	mov    (%rax),%rax
    161f:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    1623:	76 ba                	jbe    15df <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
    1625:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1629:	8b 40 08             	mov    0x8(%rax),%eax
    162c:	89 c0                	mov    %eax,%eax
    162e:	48 c1 e0 04          	shl    $0x4,%rax
    1632:	48 89 c2             	mov    %rax,%rdx
    1635:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1639:	48 01 c2             	add    %rax,%rdx
    163c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1640:	48 8b 00             	mov    (%rax),%rax
    1643:	48 39 c2             	cmp    %rax,%rdx
    1646:	75 2d                	jne    1675 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
    1648:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    164c:	8b 50 08             	mov    0x8(%rax),%edx
    164f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1653:	48 8b 00             	mov    (%rax),%rax
    1656:	8b 40 08             	mov    0x8(%rax),%eax
    1659:	01 c2                	add    %eax,%edx
    165b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    165f:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
    1662:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1666:	48 8b 00             	mov    (%rax),%rax
    1669:	48 8b 10             	mov    (%rax),%rdx
    166c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1670:	48 89 10             	mov    %rdx,(%rax)
    1673:	eb 0e                	jmp    1683 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
    1675:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1679:	48 8b 10             	mov    (%rax),%rdx
    167c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1680:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
    1683:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1687:	8b 40 08             	mov    0x8(%rax),%eax
    168a:	89 c0                	mov    %eax,%eax
    168c:	48 c1 e0 04          	shl    $0x4,%rax
    1690:	48 89 c2             	mov    %rax,%rdx
    1693:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1697:	48 01 d0             	add    %rdx,%rax
    169a:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
    169e:	75 27                	jne    16c7 <free+0x109>
    p->s.size += bp->s.size;
    16a0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16a4:	8b 50 08             	mov    0x8(%rax),%edx
    16a7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16ab:	8b 40 08             	mov    0x8(%rax),%eax
    16ae:	01 c2                	add    %eax,%edx
    16b0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16b4:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
    16b7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    16bb:	48 8b 10             	mov    (%rax),%rdx
    16be:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16c2:	48 89 10             	mov    %rdx,(%rax)
    16c5:	eb 0b                	jmp    16d2 <free+0x114>
  } else
    p->s.ptr = bp;
    16c7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16cb:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
    16cf:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
    16d2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    16d6:	48 89 05 23 08 00 00 	mov    %rax,0x823(%rip)        # 1f00 <freep>
}
    16dd:	90                   	nop
    16de:	5d                   	pop    %rbp
    16df:	c3                   	retq   

00000000000016e0 <morecore>:

static Header *morecore(uint nu) {
    16e0:	55                   	push   %rbp
    16e1:	48 89 e5             	mov    %rsp,%rbp
    16e4:	48 83 ec 20          	sub    $0x20,%rsp
    16e8:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
    16eb:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
    16f2:	77 07                	ja     16fb <morecore+0x1b>
    nu = 4096;
    16f4:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
    16fb:	8b 45 ec             	mov    -0x14(%rbp),%eax
    16fe:	c1 e0 04             	shl    $0x4,%eax
    1701:	89 c7                	mov    %eax,%edi
    1703:	e8 8e fe ff ff       	callq  1596 <sbrk>
    1708:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
    170c:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
    1711:	75 07                	jne    171a <morecore+0x3a>
    return 0;
    1713:	b8 00 00 00 00       	mov    $0x0,%eax
    1718:	eb 29                	jmp    1743 <morecore+0x63>
  hp = (Header *)p;
    171a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    171e:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
    1722:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1726:	8b 55 ec             	mov    -0x14(%rbp),%edx
    1729:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
    172c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1730:	48 83 c0 10          	add    $0x10,%rax
    1734:	48 89 c7             	mov    %rax,%rdi
    1737:	e8 82 fe ff ff       	callq  15be <free>
  return freep;
    173c:	48 8b 05 bd 07 00 00 	mov    0x7bd(%rip),%rax        # 1f00 <freep>
}
    1743:	c9                   	leaveq 
    1744:	c3                   	retq   

0000000000001745 <malloc>:

void *malloc(uint nbytes) {
    1745:	55                   	push   %rbp
    1746:	48 89 e5             	mov    %rsp,%rbp
    1749:	48 83 ec 30          	sub    $0x30,%rsp
    174d:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
    1750:	8b 45 dc             	mov    -0x24(%rbp),%eax
    1753:	48 83 c0 0f          	add    $0xf,%rax
    1757:	48 c1 e8 04          	shr    $0x4,%rax
    175b:	83 c0 01             	add    $0x1,%eax
    175e:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
    1761:	48 8b 05 98 07 00 00 	mov    0x798(%rip),%rax        # 1f00 <freep>
    1768:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    176c:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
    1771:	75 2b                	jne    179e <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
    1773:	48 c7 45 f0 f0 1e 00 	movq   $0x1ef0,-0x10(%rbp)
    177a:	00 
    177b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    177f:	48 89 05 7a 07 00 00 	mov    %rax,0x77a(%rip)        # 1f00 <freep>
    1786:	48 8b 05 73 07 00 00 	mov    0x773(%rip),%rax        # 1f00 <freep>
    178d:	48 89 05 5c 07 00 00 	mov    %rax,0x75c(%rip)        # 1ef0 <base>
    base.s.size = 0;
    1794:	c7 05 5a 07 00 00 00 	movl   $0x0,0x75a(%rip)        # 1ef8 <base+0x8>
    179b:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    179e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    17a2:	48 8b 00             	mov    (%rax),%rax
    17a5:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
    17a9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17ad:	8b 40 08             	mov    0x8(%rax),%eax
    17b0:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    17b3:	72 5f                	jb     1814 <malloc+0xcf>
      if (p->s.size == nunits)
    17b5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17b9:	8b 40 08             	mov    0x8(%rax),%eax
    17bc:	3b 45 ec             	cmp    -0x14(%rbp),%eax
    17bf:	75 10                	jne    17d1 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
    17c1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17c5:	48 8b 10             	mov    (%rax),%rdx
    17c8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    17cc:	48 89 10             	mov    %rdx,(%rax)
    17cf:	eb 2e                	jmp    17ff <malloc+0xba>
      else {
        p->s.size -= nunits;
    17d1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17d5:	8b 40 08             	mov    0x8(%rax),%eax
    17d8:	2b 45 ec             	sub    -0x14(%rbp),%eax
    17db:	89 c2                	mov    %eax,%edx
    17dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17e1:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
    17e4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17e8:	8b 40 08             	mov    0x8(%rax),%eax
    17eb:	89 c0                	mov    %eax,%eax
    17ed:	48 c1 e0 04          	shl    $0x4,%rax
    17f1:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
    17f5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    17f9:	8b 55 ec             	mov    -0x14(%rbp),%edx
    17fc:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
    17ff:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
    1803:	48 89 05 f6 06 00 00 	mov    %rax,0x6f6(%rip)        # 1f00 <freep>
      return (void *)(p + 1);
    180a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    180e:	48 83 c0 10          	add    $0x10,%rax
    1812:	eb 41                	jmp    1855 <malloc+0x110>
    }
    if (p == freep)
    1814:	48 8b 05 e5 06 00 00 	mov    0x6e5(%rip),%rax        # 1f00 <freep>
    181b:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
    181f:	75 1c                	jne    183d <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
    1821:	8b 45 ec             	mov    -0x14(%rbp),%eax
    1824:	89 c7                	mov    %eax,%edi
    1826:	e8 b5 fe ff ff       	callq  16e0 <morecore>
    182b:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    182f:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
    1834:	75 07                	jne    183d <malloc+0xf8>
        return 0;
    1836:	b8 00 00 00 00       	mov    $0x0,%eax
    183b:	eb 18                	jmp    1855 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
    183d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1841:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
    1845:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
    1849:	48 8b 00             	mov    (%rax),%rax
    184c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
    1850:	e9 54 ff ff ff       	jmpq   17a9 <malloc+0x64>
    1855:	c9                   	leaveq 
    1856:	c3                   	retq   
