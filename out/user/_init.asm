
out/user/_init:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <main>:
#include <stat.h>
#include <user.h>

char *argv[] = {"sh", 0};

int main(void) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 10          	sub    $0x10,%rsp
  int pid, wpid;

  if (open("console", O_RDWR) < 0) {
   8:	be 02 00 00 00       	mov    $0x2,%esi
   d:	bf 97 0b 00 00       	mov    $0xb97,%edi
  12:	e8 74 08 00 00       	callq  88b <open>
  17:	85 c0                	test   %eax,%eax
  19:	79 23                	jns    3e <main+0x3e>
    mknod("console", 1, 1);
  1b:	ba 01 00 00 00       	mov    $0x1,%edx
  20:	be 01 00 00 00       	mov    $0x1,%esi
  25:	bf 97 0b 00 00       	mov    $0xb97,%edi
  2a:	e8 64 08 00 00       	callq  893 <mknod>
    open("console", O_RDWR);
  2f:	be 02 00 00 00       	mov    $0x2,%esi
  34:	bf 97 0b 00 00       	mov    $0xb97,%edi
  39:	e8 4d 08 00 00       	callq  88b <open>
  }
  dup(0); // stdout
  3e:	bf 00 00 00 00       	mov    $0x0,%edi
  43:	e8 7b 08 00 00       	callq  8c3 <dup>
  dup(0); // stderr
  48:	bf 00 00 00 00       	mov    $0x0,%edi
  4d:	e8 71 08 00 00       	callq  8c3 <dup>

  for (;;) {
    printf(1, "init: starting sh\n");
  52:	be 9f 0b 00 00       	mov    $0xb9f,%esi
  57:	bf 01 00 00 00       	mov    $0x1,%edi
  5c:	b8 00 00 00 00       	mov    $0x0,%eax
  61:	e8 42 02 00 00       	callq  2a8 <printf>
    pid = fork();
  66:	e8 d8 07 00 00       	callq  843 <fork>
  6b:	89 45 fc             	mov    %eax,-0x4(%rbp)
    if (pid < 0) {
  6e:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
  72:	79 19                	jns    8d <main+0x8d>
      printf(1, "init: fork failed\n");
  74:	be b2 0b 00 00       	mov    $0xbb2,%esi
  79:	bf 01 00 00 00       	mov    $0x1,%edi
  7e:	b8 00 00 00 00       	mov    $0x0,%eax
  83:	e8 20 02 00 00       	callq  2a8 <printf>
      exit();
  88:	e8 be 07 00 00       	callq  84b <exit>
    }
    if (pid == 0) {
  8d:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
  91:	75 3c                	jne    cf <main+0xcf>
      exec("sh", argv);
  93:	be 50 0e 00 00       	mov    $0xe50,%esi
  98:	bf 94 0b 00 00       	mov    $0xb94,%edi
  9d:	e8 e1 07 00 00       	callq  883 <exec>
      printf(1, "init: exec sh failed\n");
  a2:	be c5 0b 00 00       	mov    $0xbc5,%esi
  a7:	bf 01 00 00 00       	mov    $0x1,%edi
  ac:	b8 00 00 00 00       	mov    $0x0,%eax
  b1:	e8 f2 01 00 00       	callq  2a8 <printf>
      exit();
  b6:	e8 90 07 00 00       	callq  84b <exit>
    }
    while ((wpid = wait()) >= 0 && wpid != pid)
      printf(1, "zombie!\n");
  bb:	be db 0b 00 00       	mov    $0xbdb,%esi
  c0:	bf 01 00 00 00       	mov    $0x1,%edi
  c5:	b8 00 00 00 00       	mov    $0x0,%eax
  ca:	e8 d9 01 00 00       	callq  2a8 <printf>
    if (pid == 0) {
      exec("sh", argv);
      printf(1, "init: exec sh failed\n");
      exit();
    }
    while ((wpid = wait()) >= 0 && wpid != pid)
  cf:	e8 7f 07 00 00       	callq  853 <wait>
  d4:	89 45 f8             	mov    %eax,-0x8(%rbp)
  d7:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
  db:	0f 88 71 ff ff ff    	js     52 <main+0x52>
  e1:	8b 45 f8             	mov    -0x8(%rbp),%eax
  e4:	3b 45 fc             	cmp    -0x4(%rbp),%eax
  e7:	75 d2                	jne    bb <main+0xbb>
      printf(1, "zombie!\n");
  }
  e9:	e9 64 ff ff ff       	jmpq   52 <main+0x52>

00000000000000ee <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
  ee:	55                   	push   %rbp
  ef:	48 89 e5             	mov    %rsp,%rbp
  f2:	48 83 ec 10          	sub    $0x10,%rsp
  f6:	89 7d fc             	mov    %edi,-0x4(%rbp)
  f9:	89 f0                	mov    %esi,%eax
  fb:	88 45 f8             	mov    %al,-0x8(%rbp)
  fe:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
 102:	8b 45 fc             	mov    -0x4(%rbp),%eax
 105:	ba 01 00 00 00       	mov    $0x1,%edx
 10a:	48 89 ce             	mov    %rcx,%rsi
 10d:	89 c7                	mov    %eax,%edi
 10f:	e8 57 07 00 00       	callq  86b <write>
 114:	90                   	nop
 115:	c9                   	leaveq 
 116:	c3                   	retq   

0000000000000117 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
 117:	55                   	push   %rbp
 118:	48 89 e5             	mov    %rsp,%rbp
 11b:	48 83 ec 40          	sub    $0x40,%rsp
 11f:	89 7d cc             	mov    %edi,-0x34(%rbp)
 122:	89 75 c8             	mov    %esi,-0x38(%rbp)
 125:	89 55 c4             	mov    %edx,-0x3c(%rbp)
 128:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
 12b:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 12f:	74 1f                	je     150 <printint64+0x39>
 131:	8b 45 c8             	mov    -0x38(%rbp),%eax
 134:	c1 e8 1f             	shr    $0x1f,%eax
 137:	0f b6 c0             	movzbl %al,%eax
 13a:	89 45 c0             	mov    %eax,-0x40(%rbp)
 13d:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 141:	74 0d                	je     150 <printint64+0x39>
    x = -xx;
 143:	8b 45 c8             	mov    -0x38(%rbp),%eax
 146:	f7 d8                	neg    %eax
 148:	48 98                	cltq   
 14a:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 14e:	eb 09                	jmp    159 <printint64+0x42>
  else
    x = xx;
 150:	8b 45 c8             	mov    -0x38(%rbp),%eax
 153:	48 98                	cltq   
 155:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 159:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 160:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 163:	8d 41 01             	lea    0x1(%rcx),%eax
 166:	89 45 fc             	mov    %eax,-0x4(%rbp)
 169:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 16c:	48 63 f0             	movslq %eax,%rsi
 16f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 173:	ba 00 00 00 00       	mov    $0x0,%edx
 178:	48 f7 f6             	div    %rsi
 17b:	48 89 d0             	mov    %rdx,%rax
 17e:	0f b6 90 60 0e 00 00 	movzbl 0xe60(%rax),%edx
 185:	48 63 c1             	movslq %ecx,%rax
 188:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 18c:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 18f:	48 63 f8             	movslq %eax,%rdi
 192:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 196:	ba 00 00 00 00       	mov    $0x0,%edx
 19b:	48 f7 f7             	div    %rdi
 19e:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 1a2:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 1a7:	75 b7                	jne    160 <printint64+0x49>

  if (sgn)
 1a9:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 1ad:	74 2b                	je     1da <printint64+0xc3>
    buf[i++] = '-';
 1af:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1b2:	8d 50 01             	lea    0x1(%rax),%edx
 1b5:	89 55 fc             	mov    %edx,-0x4(%rbp)
 1b8:	48 98                	cltq   
 1ba:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 1bf:	eb 19                	jmp    1da <printint64+0xc3>
    putc(fd, buf[i]);
 1c1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1c4:	48 98                	cltq   
 1c6:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 1cb:	0f be d0             	movsbl %al,%edx
 1ce:	8b 45 cc             	mov    -0x34(%rbp),%eax
 1d1:	89 d6                	mov    %edx,%esi
 1d3:	89 c7                	mov    %eax,%edi
 1d5:	e8 14 ff ff ff       	callq  ee <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 1da:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 1de:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 1e2:	79 dd                	jns    1c1 <printint64+0xaa>
    putc(fd, buf[i]);
}
 1e4:	90                   	nop
 1e5:	c9                   	leaveq 
 1e6:	c3                   	retq   

00000000000001e7 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 1e7:	55                   	push   %rbp
 1e8:	48 89 e5             	mov    %rsp,%rbp
 1eb:	48 83 ec 30          	sub    $0x30,%rsp
 1ef:	89 7d dc             	mov    %edi,-0x24(%rbp)
 1f2:	89 75 d8             	mov    %esi,-0x28(%rbp)
 1f5:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 1f8:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 1fb:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 202:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 206:	74 17                	je     21f <printint+0x38>
 208:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 20c:	79 11                	jns    21f <printint+0x38>
    neg = 1;
 20e:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 215:	8b 45 d8             	mov    -0x28(%rbp),%eax
 218:	f7 d8                	neg    %eax
 21a:	89 45 f4             	mov    %eax,-0xc(%rbp)
 21d:	eb 06                	jmp    225 <printint+0x3e>
  } else {
    x = xx;
 21f:	8b 45 d8             	mov    -0x28(%rbp),%eax
 222:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 225:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 22c:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 22f:	8d 41 01             	lea    0x1(%rcx),%eax
 232:	89 45 fc             	mov    %eax,-0x4(%rbp)
 235:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 238:	8b 45 f4             	mov    -0xc(%rbp),%eax
 23b:	ba 00 00 00 00       	mov    $0x0,%edx
 240:	f7 f6                	div    %esi
 242:	89 d0                	mov    %edx,%eax
 244:	89 c0                	mov    %eax,%eax
 246:	0f b6 90 80 0e 00 00 	movzbl 0xe80(%rax),%edx
 24d:	48 63 c1             	movslq %ecx,%rax
 250:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 254:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 257:	8b 45 f4             	mov    -0xc(%rbp),%eax
 25a:	ba 00 00 00 00       	mov    $0x0,%edx
 25f:	f7 f7                	div    %edi
 261:	89 45 f4             	mov    %eax,-0xc(%rbp)
 264:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 268:	75 c2                	jne    22c <printint+0x45>
  if (neg)
 26a:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 26e:	74 2b                	je     29b <printint+0xb4>
    buf[i++] = '-';
 270:	8b 45 fc             	mov    -0x4(%rbp),%eax
 273:	8d 50 01             	lea    0x1(%rax),%edx
 276:	89 55 fc             	mov    %edx,-0x4(%rbp)
 279:	48 98                	cltq   
 27b:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 280:	eb 19                	jmp    29b <printint+0xb4>
    putc(fd, buf[i]);
 282:	8b 45 fc             	mov    -0x4(%rbp),%eax
 285:	48 98                	cltq   
 287:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 28c:	0f be d0             	movsbl %al,%edx
 28f:	8b 45 dc             	mov    -0x24(%rbp),%eax
 292:	89 d6                	mov    %edx,%esi
 294:	89 c7                	mov    %eax,%edi
 296:	e8 53 fe ff ff       	callq  ee <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 29b:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 29f:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 2a3:	79 dd                	jns    282 <printint+0x9b>
    putc(fd, buf[i]);
}
 2a5:	90                   	nop
 2a6:	c9                   	leaveq 
 2a7:	c3                   	retq   

00000000000002a8 <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 2a8:	55                   	push   %rbp
 2a9:	48 89 e5             	mov    %rsp,%rbp
 2ac:	48 83 ec 70          	sub    $0x70,%rsp
 2b0:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 2b3:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 2b7:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 2bb:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 2bf:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 2c3:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 2c7:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 2ce:	48 8d 45 10          	lea    0x10(%rbp),%rax
 2d2:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 2d6:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 2da:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 2de:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 2e5:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 2ec:	e9 68 02 00 00       	jmpq   559 <printf+0x2b1>
    c = fmt[i] & 0xff;
 2f1:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 2f4:	48 63 d0             	movslq %eax,%rdx
 2f7:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 2fb:	48 01 d0             	add    %rdx,%rax
 2fe:	0f b6 00             	movzbl (%rax),%eax
 301:	0f be c0             	movsbl %al,%eax
 304:	25 ff 00 00 00       	and    $0xff,%eax
 309:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 30c:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 310:	75 30                	jne    342 <printf+0x9a>
      if (c == '%') {
 312:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 316:	75 13                	jne    32b <printf+0x83>
        state = '%';
 318:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 31f:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 326:	e9 2a 02 00 00       	jmpq   555 <printf+0x2ad>
      } else {
        putc(fd, c);
 32b:	8b 45 b8             	mov    -0x48(%rbp),%eax
 32e:	0f be d0             	movsbl %al,%edx
 331:	8b 45 9c             	mov    -0x64(%rbp),%eax
 334:	89 d6                	mov    %edx,%esi
 336:	89 c7                	mov    %eax,%edi
 338:	e8 b1 fd ff ff       	callq  ee <putc>
 33d:	e9 13 02 00 00       	jmpq   555 <printf+0x2ad>
      }
    } else if (state == '%') {
 342:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 346:	0f 85 09 02 00 00    	jne    555 <printf+0x2ad>
      if (c == 'l') {
 34c:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 350:	75 0c                	jne    35e <printf+0xb6>
        lflag = 1;
 352:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 359:	e9 f7 01 00 00       	jmpq   555 <printf+0x2ad>
      } else if (c == 'd') {
 35e:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 362:	0f 85 95 00 00 00    	jne    3fd <printf+0x155>
        if (lflag == 1)
 368:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 36c:	75 49                	jne    3b7 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 36e:	8b 45 a0             	mov    -0x60(%rbp),%eax
 371:	83 f8 30             	cmp    $0x30,%eax
 374:	73 17                	jae    38d <printf+0xe5>
 376:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 37a:	8b 55 a0             	mov    -0x60(%rbp),%edx
 37d:	89 d2                	mov    %edx,%edx
 37f:	48 01 d0             	add    %rdx,%rax
 382:	8b 55 a0             	mov    -0x60(%rbp),%edx
 385:	83 c2 08             	add    $0x8,%edx
 388:	89 55 a0             	mov    %edx,-0x60(%rbp)
 38b:	eb 0c                	jmp    399 <printf+0xf1>
 38d:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 391:	48 8d 50 08          	lea    0x8(%rax),%rdx
 395:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 399:	48 8b 00             	mov    (%rax),%rax
 39c:	89 c6                	mov    %eax,%esi
 39e:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3a1:	b9 01 00 00 00       	mov    $0x1,%ecx
 3a6:	ba 0a 00 00 00       	mov    $0xa,%edx
 3ab:	89 c7                	mov    %eax,%edi
 3ad:	e8 65 fd ff ff       	callq  117 <printint64>
 3b2:	e9 97 01 00 00       	jmpq   54e <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 3b7:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3ba:	83 f8 30             	cmp    $0x30,%eax
 3bd:	73 17                	jae    3d6 <printf+0x12e>
 3bf:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3c3:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3c6:	89 d2                	mov    %edx,%edx
 3c8:	48 01 d0             	add    %rdx,%rax
 3cb:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3ce:	83 c2 08             	add    $0x8,%edx
 3d1:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3d4:	eb 0c                	jmp    3e2 <printf+0x13a>
 3d6:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3da:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3de:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3e2:	8b 30                	mov    (%rax),%esi
 3e4:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3e7:	b9 01 00 00 00       	mov    $0x1,%ecx
 3ec:	ba 0a 00 00 00       	mov    $0xa,%edx
 3f1:	89 c7                	mov    %eax,%edi
 3f3:	e8 ef fd ff ff       	callq  1e7 <printint>
 3f8:	e9 51 01 00 00       	jmpq   54e <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 3fd:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 401:	74 0a                	je     40d <printf+0x165>
 403:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 407:	0f 85 95 00 00 00    	jne    4a2 <printf+0x1fa>
        if (lflag == 1)
 40d:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 411:	75 49                	jne    45c <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 413:	8b 45 a0             	mov    -0x60(%rbp),%eax
 416:	83 f8 30             	cmp    $0x30,%eax
 419:	73 17                	jae    432 <printf+0x18a>
 41b:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 41f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 422:	89 d2                	mov    %edx,%edx
 424:	48 01 d0             	add    %rdx,%rax
 427:	8b 55 a0             	mov    -0x60(%rbp),%edx
 42a:	83 c2 08             	add    $0x8,%edx
 42d:	89 55 a0             	mov    %edx,-0x60(%rbp)
 430:	eb 0c                	jmp    43e <printf+0x196>
 432:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 436:	48 8d 50 08          	lea    0x8(%rax),%rdx
 43a:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 43e:	48 8b 00             	mov    (%rax),%rax
 441:	89 c6                	mov    %eax,%esi
 443:	8b 45 9c             	mov    -0x64(%rbp),%eax
 446:	b9 00 00 00 00       	mov    $0x0,%ecx
 44b:	ba 10 00 00 00       	mov    $0x10,%edx
 450:	89 c7                	mov    %eax,%edi
 452:	e8 c0 fc ff ff       	callq  117 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 457:	e9 f2 00 00 00       	jmpq   54e <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 45c:	8b 45 a0             	mov    -0x60(%rbp),%eax
 45f:	83 f8 30             	cmp    $0x30,%eax
 462:	73 17                	jae    47b <printf+0x1d3>
 464:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 468:	8b 55 a0             	mov    -0x60(%rbp),%edx
 46b:	89 d2                	mov    %edx,%edx
 46d:	48 01 d0             	add    %rdx,%rax
 470:	8b 55 a0             	mov    -0x60(%rbp),%edx
 473:	83 c2 08             	add    $0x8,%edx
 476:	89 55 a0             	mov    %edx,-0x60(%rbp)
 479:	eb 0c                	jmp    487 <printf+0x1df>
 47b:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 47f:	48 8d 50 08          	lea    0x8(%rax),%rdx
 483:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 487:	8b 30                	mov    (%rax),%esi
 489:	8b 45 9c             	mov    -0x64(%rbp),%eax
 48c:	b9 00 00 00 00       	mov    $0x0,%ecx
 491:	ba 10 00 00 00       	mov    $0x10,%edx
 496:	89 c7                	mov    %eax,%edi
 498:	e8 4a fd ff ff       	callq  1e7 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 49d:	e9 ac 00 00 00       	jmpq   54e <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 4a2:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 4a6:	75 6b                	jne    513 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 4a8:	8b 45 a0             	mov    -0x60(%rbp),%eax
 4ab:	83 f8 30             	cmp    $0x30,%eax
 4ae:	73 17                	jae    4c7 <printf+0x21f>
 4b0:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 4b4:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4b7:	89 d2                	mov    %edx,%edx
 4b9:	48 01 d0             	add    %rdx,%rax
 4bc:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4bf:	83 c2 08             	add    $0x8,%edx
 4c2:	89 55 a0             	mov    %edx,-0x60(%rbp)
 4c5:	eb 0c                	jmp    4d3 <printf+0x22b>
 4c7:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 4cb:	48 8d 50 08          	lea    0x8(%rax),%rdx
 4cf:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 4d3:	48 8b 00             	mov    (%rax),%rax
 4d6:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 4da:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 4df:	75 25                	jne    506 <printf+0x25e>
          s = "(null)";
 4e1:	48 c7 45 c8 e4 0b 00 	movq   $0xbe4,-0x38(%rbp)
 4e8:	00 
        for (; *s; s++)
 4e9:	eb 1b                	jmp    506 <printf+0x25e>
          putc(fd, *s);
 4eb:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 4ef:	0f b6 00             	movzbl (%rax),%eax
 4f2:	0f be d0             	movsbl %al,%edx
 4f5:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4f8:	89 d6                	mov    %edx,%esi
 4fa:	89 c7                	mov    %eax,%edi
 4fc:	e8 ed fb ff ff       	callq  ee <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 501:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 506:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 50a:	0f b6 00             	movzbl (%rax),%eax
 50d:	84 c0                	test   %al,%al
 50f:	75 da                	jne    4eb <printf+0x243>
 511:	eb 3b                	jmp    54e <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 513:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 517:	75 14                	jne    52d <printf+0x285>
        putc(fd, c);
 519:	8b 45 b8             	mov    -0x48(%rbp),%eax
 51c:	0f be d0             	movsbl %al,%edx
 51f:	8b 45 9c             	mov    -0x64(%rbp),%eax
 522:	89 d6                	mov    %edx,%esi
 524:	89 c7                	mov    %eax,%edi
 526:	e8 c3 fb ff ff       	callq  ee <putc>
 52b:	eb 21                	jmp    54e <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 52d:	8b 45 9c             	mov    -0x64(%rbp),%eax
 530:	be 25 00 00 00       	mov    $0x25,%esi
 535:	89 c7                	mov    %eax,%edi
 537:	e8 b2 fb ff ff       	callq  ee <putc>
        putc(fd, c);
 53c:	8b 45 b8             	mov    -0x48(%rbp),%eax
 53f:	0f be d0             	movsbl %al,%edx
 542:	8b 45 9c             	mov    -0x64(%rbp),%eax
 545:	89 d6                	mov    %edx,%esi
 547:	89 c7                	mov    %eax,%edi
 549:	e8 a0 fb ff ff       	callq  ee <putc>
      }
      state = 0;
 54e:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 555:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 559:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 55c:	48 63 d0             	movslq %eax,%rdx
 55f:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 563:	48 01 d0             	add    %rdx,%rax
 566:	0f b6 00             	movzbl (%rax),%eax
 569:	84 c0                	test   %al,%al
 56b:	0f 85 80 fd ff ff    	jne    2f1 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 571:	90                   	nop
 572:	c9                   	leaveq 
 573:	c3                   	retq   

0000000000000574 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 574:	55                   	push   %rbp
 575:	48 89 e5             	mov    %rsp,%rbp
 578:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 57c:	89 75 f4             	mov    %esi,-0xc(%rbp)
 57f:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 582:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 586:	8b 55 f0             	mov    -0x10(%rbp),%edx
 589:	8b 45 f4             	mov    -0xc(%rbp),%eax
 58c:	48 89 ce             	mov    %rcx,%rsi
 58f:	48 89 f7             	mov    %rsi,%rdi
 592:	89 d1                	mov    %edx,%ecx
 594:	fc                   	cld    
 595:	f3 aa                	rep stos %al,%es:(%rdi)
 597:	89 ca                	mov    %ecx,%edx
 599:	48 89 fe             	mov    %rdi,%rsi
 59c:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 5a0:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 5a3:	90                   	nop
 5a4:	5d                   	pop    %rbp
 5a5:	c3                   	retq   

00000000000005a6 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 5a6:	55                   	push   %rbp
 5a7:	48 89 e5             	mov    %rsp,%rbp
 5aa:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 5ae:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 5b2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5b6:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 5ba:	90                   	nop
 5bb:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5bf:	48 8d 50 01          	lea    0x1(%rax),%rdx
 5c3:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 5c7:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 5cb:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 5cf:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 5d3:	0f b6 12             	movzbl (%rdx),%edx
 5d6:	88 10                	mov    %dl,(%rax)
 5d8:	0f b6 00             	movzbl (%rax),%eax
 5db:	84 c0                	test   %al,%al
 5dd:	75 dc                	jne    5bb <strcpy+0x15>
    ;
  return os;
 5df:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 5e3:	5d                   	pop    %rbp
 5e4:	c3                   	retq   

00000000000005e5 <strcmp>:

int strcmp(const char *p, const char *q) {
 5e5:	55                   	push   %rbp
 5e6:	48 89 e5             	mov    %rsp,%rbp
 5e9:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5ed:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 5f1:	eb 0a                	jmp    5fd <strcmp+0x18>
    p++, q++;
 5f3:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 5f8:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 5fd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 601:	0f b6 00             	movzbl (%rax),%eax
 604:	84 c0                	test   %al,%al
 606:	74 12                	je     61a <strcmp+0x35>
 608:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 60c:	0f b6 10             	movzbl (%rax),%edx
 60f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 613:	0f b6 00             	movzbl (%rax),%eax
 616:	38 c2                	cmp    %al,%dl
 618:	74 d9                	je     5f3 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 61a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 61e:	0f b6 00             	movzbl (%rax),%eax
 621:	0f b6 d0             	movzbl %al,%edx
 624:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 628:	0f b6 00             	movzbl (%rax),%eax
 62b:	0f b6 c0             	movzbl %al,%eax
 62e:	29 c2                	sub    %eax,%edx
 630:	89 d0                	mov    %edx,%eax
}
 632:	5d                   	pop    %rbp
 633:	c3                   	retq   

0000000000000634 <strlen>:

uint strlen(char *s) {
 634:	55                   	push   %rbp
 635:	48 89 e5             	mov    %rsp,%rbp
 638:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 63c:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 643:	eb 04                	jmp    649 <strlen+0x15>
 645:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 649:	8b 45 fc             	mov    -0x4(%rbp),%eax
 64c:	48 63 d0             	movslq %eax,%rdx
 64f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 653:	48 01 d0             	add    %rdx,%rax
 656:	0f b6 00             	movzbl (%rax),%eax
 659:	84 c0                	test   %al,%al
 65b:	75 e8                	jne    645 <strlen+0x11>
    ;
  return n;
 65d:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 660:	5d                   	pop    %rbp
 661:	c3                   	retq   

0000000000000662 <memset>:

void *memset(void *dst, int c, uint n) {
 662:	55                   	push   %rbp
 663:	48 89 e5             	mov    %rsp,%rbp
 666:	48 83 ec 10          	sub    $0x10,%rsp
 66a:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 66e:	89 75 f4             	mov    %esi,-0xc(%rbp)
 671:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 674:	8b 55 f0             	mov    -0x10(%rbp),%edx
 677:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 67a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 67e:	89 ce                	mov    %ecx,%esi
 680:	48 89 c7             	mov    %rax,%rdi
 683:	e8 ec fe ff ff       	callq  574 <stosb>
  return dst;
 688:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 68c:	c9                   	leaveq 
 68d:	c3                   	retq   

000000000000068e <strchr>:

char *strchr(const char *s, char c) {
 68e:	55                   	push   %rbp
 68f:	48 89 e5             	mov    %rsp,%rbp
 692:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 696:	89 f0                	mov    %esi,%eax
 698:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 69b:	eb 17                	jmp    6b4 <strchr+0x26>
    if (*s == c)
 69d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6a1:	0f b6 00             	movzbl (%rax),%eax
 6a4:	3a 45 f4             	cmp    -0xc(%rbp),%al
 6a7:	75 06                	jne    6af <strchr+0x21>
      return (char *)s;
 6a9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6ad:	eb 15                	jmp    6c4 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 6af:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 6b4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6b8:	0f b6 00             	movzbl (%rax),%eax
 6bb:	84 c0                	test   %al,%al
 6bd:	75 de                	jne    69d <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 6bf:	b8 00 00 00 00       	mov    $0x0,%eax
}
 6c4:	5d                   	pop    %rbp
 6c5:	c3                   	retq   

00000000000006c6 <gets>:

char *gets(char *buf, int max) {
 6c6:	55                   	push   %rbp
 6c7:	48 89 e5             	mov    %rsp,%rbp
 6ca:	48 83 ec 20          	sub    $0x20,%rsp
 6ce:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 6d2:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 6d5:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 6dc:	eb 48                	jmp    726 <gets+0x60>
    cc = read(0, &c, 1);
 6de:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 6e2:	ba 01 00 00 00       	mov    $0x1,%edx
 6e7:	48 89 c6             	mov    %rax,%rsi
 6ea:	bf 00 00 00 00       	mov    $0x0,%edi
 6ef:	e8 6f 01 00 00       	callq  863 <read>
 6f4:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 6f7:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 6fb:	7e 36                	jle    733 <gets+0x6d>
      break;
    buf[i++] = c;
 6fd:	8b 45 fc             	mov    -0x4(%rbp),%eax
 700:	8d 50 01             	lea    0x1(%rax),%edx
 703:	89 55 fc             	mov    %edx,-0x4(%rbp)
 706:	48 63 d0             	movslq %eax,%rdx
 709:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 70d:	48 01 c2             	add    %rax,%rdx
 710:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 714:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 716:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 71a:	3c 0a                	cmp    $0xa,%al
 71c:	74 16                	je     734 <gets+0x6e>
 71e:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 722:	3c 0d                	cmp    $0xd,%al
 724:	74 0e                	je     734 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 726:	8b 45 fc             	mov    -0x4(%rbp),%eax
 729:	83 c0 01             	add    $0x1,%eax
 72c:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 72f:	7c ad                	jl     6de <gets+0x18>
 731:	eb 01                	jmp    734 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 733:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 734:	8b 45 fc             	mov    -0x4(%rbp),%eax
 737:	48 63 d0             	movslq %eax,%rdx
 73a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 73e:	48 01 d0             	add    %rdx,%rax
 741:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 744:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 748:	c9                   	leaveq 
 749:	c3                   	retq   

000000000000074a <stat>:

int stat(char *n, struct stat *st) {
 74a:	55                   	push   %rbp
 74b:	48 89 e5             	mov    %rsp,%rbp
 74e:	48 83 ec 20          	sub    $0x20,%rsp
 752:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 756:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 75a:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 75e:	be 00 00 00 00       	mov    $0x0,%esi
 763:	48 89 c7             	mov    %rax,%rdi
 766:	e8 20 01 00 00       	callq  88b <open>
 76b:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 76e:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 772:	79 07                	jns    77b <stat+0x31>
    return -1;
 774:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 779:	eb 21                	jmp    79c <stat+0x52>
  r = fstat(fd, st);
 77b:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 77f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 782:	48 89 d6             	mov    %rdx,%rsi
 785:	89 c7                	mov    %eax,%edi
 787:	e8 17 01 00 00       	callq  8a3 <fstat>
 78c:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 78f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 792:	89 c7                	mov    %eax,%edi
 794:	e8 da 00 00 00       	callq  873 <close>
  return r;
 799:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 79c:	c9                   	leaveq 
 79d:	c3                   	retq   

000000000000079e <atoi>:

int atoi(const char *s) {
 79e:	55                   	push   %rbp
 79f:	48 89 e5             	mov    %rsp,%rbp
 7a2:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 7a6:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 7ad:	eb 28                	jmp    7d7 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 7af:	8b 55 fc             	mov    -0x4(%rbp),%edx
 7b2:	89 d0                	mov    %edx,%eax
 7b4:	c1 e0 02             	shl    $0x2,%eax
 7b7:	01 d0                	add    %edx,%eax
 7b9:	01 c0                	add    %eax,%eax
 7bb:	89 c1                	mov    %eax,%ecx
 7bd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7c1:	48 8d 50 01          	lea    0x1(%rax),%rdx
 7c5:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 7c9:	0f b6 00             	movzbl (%rax),%eax
 7cc:	0f be c0             	movsbl %al,%eax
 7cf:	01 c8                	add    %ecx,%eax
 7d1:	83 e8 30             	sub    $0x30,%eax
 7d4:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 7d7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7db:	0f b6 00             	movzbl (%rax),%eax
 7de:	3c 2f                	cmp    $0x2f,%al
 7e0:	7e 0b                	jle    7ed <atoi+0x4f>
 7e2:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7e6:	0f b6 00             	movzbl (%rax),%eax
 7e9:	3c 39                	cmp    $0x39,%al
 7eb:	7e c2                	jle    7af <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 7ed:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 7f0:	5d                   	pop    %rbp
 7f1:	c3                   	retq   

00000000000007f2 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 7f2:	55                   	push   %rbp
 7f3:	48 89 e5             	mov    %rsp,%rbp
 7f6:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 7fa:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 7fe:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 801:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 805:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 809:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 80d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 811:	eb 1d                	jmp    830 <memmove+0x3e>
    *dst++ = *src++;
 813:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 817:	48 8d 50 01          	lea    0x1(%rax),%rdx
 81b:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 81f:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 823:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 827:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 82b:	0f b6 12             	movzbl (%rdx),%edx
 82e:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 830:	8b 45 dc             	mov    -0x24(%rbp),%eax
 833:	8d 50 ff             	lea    -0x1(%rax),%edx
 836:	89 55 dc             	mov    %edx,-0x24(%rbp)
 839:	85 c0                	test   %eax,%eax
 83b:	7f d6                	jg     813 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 83d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 841:	5d                   	pop    %rbp
 842:	c3                   	retq   

0000000000000843 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 843:	b8 01 00 00 00       	mov    $0x1,%eax
 848:	cd 40                	int    $0x40
 84a:	c3                   	retq   

000000000000084b <exit>:
SYSCALL(exit)
 84b:	b8 02 00 00 00       	mov    $0x2,%eax
 850:	cd 40                	int    $0x40
 852:	c3                   	retq   

0000000000000853 <wait>:
SYSCALL(wait)
 853:	b8 03 00 00 00       	mov    $0x3,%eax
 858:	cd 40                	int    $0x40
 85a:	c3                   	retq   

000000000000085b <pipe>:
SYSCALL(pipe)
 85b:	b8 04 00 00 00       	mov    $0x4,%eax
 860:	cd 40                	int    $0x40
 862:	c3                   	retq   

0000000000000863 <read>:
SYSCALL(read)
 863:	b8 05 00 00 00       	mov    $0x5,%eax
 868:	cd 40                	int    $0x40
 86a:	c3                   	retq   

000000000000086b <write>:
SYSCALL(write)
 86b:	b8 10 00 00 00       	mov    $0x10,%eax
 870:	cd 40                	int    $0x40
 872:	c3                   	retq   

0000000000000873 <close>:
SYSCALL(close)
 873:	b8 15 00 00 00       	mov    $0x15,%eax
 878:	cd 40                	int    $0x40
 87a:	c3                   	retq   

000000000000087b <kill>:
SYSCALL(kill)
 87b:	b8 06 00 00 00       	mov    $0x6,%eax
 880:	cd 40                	int    $0x40
 882:	c3                   	retq   

0000000000000883 <exec>:
SYSCALL(exec)
 883:	b8 07 00 00 00       	mov    $0x7,%eax
 888:	cd 40                	int    $0x40
 88a:	c3                   	retq   

000000000000088b <open>:
SYSCALL(open)
 88b:	b8 0f 00 00 00       	mov    $0xf,%eax
 890:	cd 40                	int    $0x40
 892:	c3                   	retq   

0000000000000893 <mknod>:
SYSCALL(mknod)
 893:	b8 11 00 00 00       	mov    $0x11,%eax
 898:	cd 40                	int    $0x40
 89a:	c3                   	retq   

000000000000089b <unlink>:
SYSCALL(unlink)
 89b:	b8 12 00 00 00       	mov    $0x12,%eax
 8a0:	cd 40                	int    $0x40
 8a2:	c3                   	retq   

00000000000008a3 <fstat>:
SYSCALL(fstat)
 8a3:	b8 08 00 00 00       	mov    $0x8,%eax
 8a8:	cd 40                	int    $0x40
 8aa:	c3                   	retq   

00000000000008ab <link>:
SYSCALL(link)
 8ab:	b8 13 00 00 00       	mov    $0x13,%eax
 8b0:	cd 40                	int    $0x40
 8b2:	c3                   	retq   

00000000000008b3 <mkdir>:
SYSCALL(mkdir)
 8b3:	b8 14 00 00 00       	mov    $0x14,%eax
 8b8:	cd 40                	int    $0x40
 8ba:	c3                   	retq   

00000000000008bb <chdir>:
SYSCALL(chdir)
 8bb:	b8 09 00 00 00       	mov    $0x9,%eax
 8c0:	cd 40                	int    $0x40
 8c2:	c3                   	retq   

00000000000008c3 <dup>:
SYSCALL(dup)
 8c3:	b8 0a 00 00 00       	mov    $0xa,%eax
 8c8:	cd 40                	int    $0x40
 8ca:	c3                   	retq   

00000000000008cb <getpid>:
SYSCALL(getpid)
 8cb:	b8 0b 00 00 00       	mov    $0xb,%eax
 8d0:	cd 40                	int    $0x40
 8d2:	c3                   	retq   

00000000000008d3 <sbrk>:
SYSCALL(sbrk)
 8d3:	b8 0c 00 00 00       	mov    $0xc,%eax
 8d8:	cd 40                	int    $0x40
 8da:	c3                   	retq   

00000000000008db <sleep>:
SYSCALL(sleep)
 8db:	b8 0d 00 00 00       	mov    $0xd,%eax
 8e0:	cd 40                	int    $0x40
 8e2:	c3                   	retq   

00000000000008e3 <uptime>:
SYSCALL(uptime)
 8e3:	b8 0e 00 00 00       	mov    $0xe,%eax
 8e8:	cd 40                	int    $0x40
 8ea:	c3                   	retq   

00000000000008eb <sysinfo>:
SYSCALL(sysinfo)
 8eb:	b8 16 00 00 00       	mov    $0x16,%eax
 8f0:	cd 40                	int    $0x40
 8f2:	c3                   	retq   

00000000000008f3 <crashn>:
SYSCALL(crashn)
 8f3:	b8 17 00 00 00       	mov    $0x17,%eax
 8f8:	cd 40                	int    $0x40
 8fa:	c3                   	retq   

00000000000008fb <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 8fb:	55                   	push   %rbp
 8fc:	48 89 e5             	mov    %rsp,%rbp
 8ff:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 903:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 907:	48 83 e8 10          	sub    $0x10,%rax
 90b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 90f:	48 8b 05 9a 05 00 00 	mov    0x59a(%rip),%rax        # eb0 <freep>
 916:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 91a:	eb 2f                	jmp    94b <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 91c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 920:	48 8b 00             	mov    (%rax),%rax
 923:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 927:	77 17                	ja     940 <free+0x45>
 929:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 92d:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 931:	77 2f                	ja     962 <free+0x67>
 933:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 937:	48 8b 00             	mov    (%rax),%rax
 93a:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 93e:	77 22                	ja     962 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 940:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 944:	48 8b 00             	mov    (%rax),%rax
 947:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 94b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 94f:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 953:	76 c7                	jbe    91c <free+0x21>
 955:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 959:	48 8b 00             	mov    (%rax),%rax
 95c:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 960:	76 ba                	jbe    91c <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 962:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 966:	8b 40 08             	mov    0x8(%rax),%eax
 969:	89 c0                	mov    %eax,%eax
 96b:	48 c1 e0 04          	shl    $0x4,%rax
 96f:	48 89 c2             	mov    %rax,%rdx
 972:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 976:	48 01 c2             	add    %rax,%rdx
 979:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 97d:	48 8b 00             	mov    (%rax),%rax
 980:	48 39 c2             	cmp    %rax,%rdx
 983:	75 2d                	jne    9b2 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 985:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 989:	8b 50 08             	mov    0x8(%rax),%edx
 98c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 990:	48 8b 00             	mov    (%rax),%rax
 993:	8b 40 08             	mov    0x8(%rax),%eax
 996:	01 c2                	add    %eax,%edx
 998:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 99c:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 99f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9a3:	48 8b 00             	mov    (%rax),%rax
 9a6:	48 8b 10             	mov    (%rax),%rdx
 9a9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ad:	48 89 10             	mov    %rdx,(%rax)
 9b0:	eb 0e                	jmp    9c0 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 9b2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9b6:	48 8b 10             	mov    (%rax),%rdx
 9b9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9bd:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 9c0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9c4:	8b 40 08             	mov    0x8(%rax),%eax
 9c7:	89 c0                	mov    %eax,%eax
 9c9:	48 c1 e0 04          	shl    $0x4,%rax
 9cd:	48 89 c2             	mov    %rax,%rdx
 9d0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9d4:	48 01 d0             	add    %rdx,%rax
 9d7:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 9db:	75 27                	jne    a04 <free+0x109>
    p->s.size += bp->s.size;
 9dd:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9e1:	8b 50 08             	mov    0x8(%rax),%edx
 9e4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9e8:	8b 40 08             	mov    0x8(%rax),%eax
 9eb:	01 c2                	add    %eax,%edx
 9ed:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9f1:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 9f4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9f8:	48 8b 10             	mov    (%rax),%rdx
 9fb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9ff:	48 89 10             	mov    %rdx,(%rax)
 a02:	eb 0b                	jmp    a0f <free+0x114>
  } else
    p->s.ptr = bp;
 a04:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a08:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 a0c:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 a0f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a13:	48 89 05 96 04 00 00 	mov    %rax,0x496(%rip)        # eb0 <freep>
}
 a1a:	90                   	nop
 a1b:	5d                   	pop    %rbp
 a1c:	c3                   	retq   

0000000000000a1d <morecore>:

static Header *morecore(uint nu) {
 a1d:	55                   	push   %rbp
 a1e:	48 89 e5             	mov    %rsp,%rbp
 a21:	48 83 ec 20          	sub    $0x20,%rsp
 a25:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 a28:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 a2f:	77 07                	ja     a38 <morecore+0x1b>
    nu = 4096;
 a31:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 a38:	8b 45 ec             	mov    -0x14(%rbp),%eax
 a3b:	c1 e0 04             	shl    $0x4,%eax
 a3e:	89 c7                	mov    %eax,%edi
 a40:	e8 8e fe ff ff       	callq  8d3 <sbrk>
 a45:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 a49:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 a4e:	75 07                	jne    a57 <morecore+0x3a>
    return 0;
 a50:	b8 00 00 00 00       	mov    $0x0,%eax
 a55:	eb 29                	jmp    a80 <morecore+0x63>
  hp = (Header *)p;
 a57:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a5b:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 a5f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a63:	8b 55 ec             	mov    -0x14(%rbp),%edx
 a66:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 a69:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a6d:	48 83 c0 10          	add    $0x10,%rax
 a71:	48 89 c7             	mov    %rax,%rdi
 a74:	e8 82 fe ff ff       	callq  8fb <free>
  return freep;
 a79:	48 8b 05 30 04 00 00 	mov    0x430(%rip),%rax        # eb0 <freep>
}
 a80:	c9                   	leaveq 
 a81:	c3                   	retq   

0000000000000a82 <malloc>:

void *malloc(uint nbytes) {
 a82:	55                   	push   %rbp
 a83:	48 89 e5             	mov    %rsp,%rbp
 a86:	48 83 ec 30          	sub    $0x30,%rsp
 a8a:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 a8d:	8b 45 dc             	mov    -0x24(%rbp),%eax
 a90:	48 83 c0 0f          	add    $0xf,%rax
 a94:	48 c1 e8 04          	shr    $0x4,%rax
 a98:	83 c0 01             	add    $0x1,%eax
 a9b:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 a9e:	48 8b 05 0b 04 00 00 	mov    0x40b(%rip),%rax        # eb0 <freep>
 aa5:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 aa9:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 aae:	75 2b                	jne    adb <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 ab0:	48 c7 45 f0 a0 0e 00 	movq   $0xea0,-0x10(%rbp)
 ab7:	00 
 ab8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 abc:	48 89 05 ed 03 00 00 	mov    %rax,0x3ed(%rip)        # eb0 <freep>
 ac3:	48 8b 05 e6 03 00 00 	mov    0x3e6(%rip),%rax        # eb0 <freep>
 aca:	48 89 05 cf 03 00 00 	mov    %rax,0x3cf(%rip)        # ea0 <base>
    base.s.size = 0;
 ad1:	c7 05 cd 03 00 00 00 	movl   $0x0,0x3cd(%rip)        # ea8 <base+0x8>
 ad8:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 adb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 adf:	48 8b 00             	mov    (%rax),%rax
 ae2:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 ae6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 aea:	8b 40 08             	mov    0x8(%rax),%eax
 aed:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 af0:	72 5f                	jb     b51 <malloc+0xcf>
      if (p->s.size == nunits)
 af2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 af6:	8b 40 08             	mov    0x8(%rax),%eax
 af9:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 afc:	75 10                	jne    b0e <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 afe:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b02:	48 8b 10             	mov    (%rax),%rdx
 b05:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b09:	48 89 10             	mov    %rdx,(%rax)
 b0c:	eb 2e                	jmp    b3c <malloc+0xba>
      else {
        p->s.size -= nunits;
 b0e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b12:	8b 40 08             	mov    0x8(%rax),%eax
 b15:	2b 45 ec             	sub    -0x14(%rbp),%eax
 b18:	89 c2                	mov    %eax,%edx
 b1a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b1e:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 b21:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b25:	8b 40 08             	mov    0x8(%rax),%eax
 b28:	89 c0                	mov    %eax,%eax
 b2a:	48 c1 e0 04          	shl    $0x4,%rax
 b2e:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 b32:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b36:	8b 55 ec             	mov    -0x14(%rbp),%edx
 b39:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 b3c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b40:	48 89 05 69 03 00 00 	mov    %rax,0x369(%rip)        # eb0 <freep>
      return (void *)(p + 1);
 b47:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b4b:	48 83 c0 10          	add    $0x10,%rax
 b4f:	eb 41                	jmp    b92 <malloc+0x110>
    }
    if (p == freep)
 b51:	48 8b 05 58 03 00 00 	mov    0x358(%rip),%rax        # eb0 <freep>
 b58:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 b5c:	75 1c                	jne    b7a <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 b5e:	8b 45 ec             	mov    -0x14(%rbp),%eax
 b61:	89 c7                	mov    %eax,%edi
 b63:	e8 b5 fe ff ff       	callq  a1d <morecore>
 b68:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 b6c:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 b71:	75 07                	jne    b7a <malloc+0xf8>
        return 0;
 b73:	b8 00 00 00 00       	mov    $0x0,%eax
 b78:	eb 18                	jmp    b92 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b7a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b7e:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 b82:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b86:	48 8b 00             	mov    (%rax),%rax
 b89:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 b8d:	e9 54 ff ff ff       	jmpq   ae6 <malloc+0x64>
 b92:	c9                   	leaveq 
 b93:	c3                   	retq   
