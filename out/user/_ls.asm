
out/user/_ls:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <fmtname>:
#include <cdefs.h>
#include <fs.h>
#include <stat.h>
#include <user.h>

char *fmtname(char *path) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	53                   	push   %rbx
   5:	48 83 ec 28          	sub    $0x28,%rsp
   9:	48 89 7d d8          	mov    %rdi,-0x28(%rbp)
  static char buf[DIRSIZ + 1];
  char *p;

  // Find first character after last slash.
  for (p = path + strlen(path); p >= path && *p != '/'; p--)
   d:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
  11:	48 89 c7             	mov    %rax,%rdi
  14:	e8 bf 08 00 00       	callq  8d8 <strlen>
  19:	89 c2                	mov    %eax,%edx
  1b:	48 8b 45 d8          	mov    -0x28(%rbp),%rax
  1f:	48 01 d0             	add    %rdx,%rax
  22:	48 89 45 e8          	mov    %rax,-0x18(%rbp)
  26:	eb 05                	jmp    2d <fmtname+0x2d>
  28:	48 83 6d e8 01       	subq   $0x1,-0x18(%rbp)
  2d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  31:	48 3b 45 d8          	cmp    -0x28(%rbp),%rax
  35:	72 0b                	jb     42 <fmtname+0x42>
  37:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  3b:	0f b6 00             	movzbl (%rax),%eax
  3e:	3c 2f                	cmp    $0x2f,%al
  40:	75 e6                	jne    28 <fmtname+0x28>
    ;
  p++;
  42:	48 83 45 e8 01       	addq   $0x1,-0x18(%rbp)

  // Return blank-padded name.
  if (strlen(p) >= DIRSIZ)
  47:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  4b:	48 89 c7             	mov    %rax,%rdi
  4e:	e8 85 08 00 00       	callq  8d8 <strlen>
  53:	83 f8 0d             	cmp    $0xd,%eax
  56:	76 06                	jbe    5e <fmtname+0x5e>
    return p;
  58:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  5c:	eb 5c                	jmp    ba <fmtname+0xba>
  memmove(buf, p, strlen(p));
  5e:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  62:	48 89 c7             	mov    %rax,%rdi
  65:	e8 6e 08 00 00       	callq  8d8 <strlen>
  6a:	89 c2                	mov    %eax,%edx
  6c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  70:	48 89 c6             	mov    %rax,%rsi
  73:	bf 80 11 00 00       	mov    $0x1180,%edi
  78:	e8 19 0a 00 00       	callq  a96 <memmove>
  memset(buf + strlen(p), ' ', DIRSIZ - strlen(p));
  7d:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  81:	48 89 c7             	mov    %rax,%rdi
  84:	e8 4f 08 00 00       	callq  8d8 <strlen>
  89:	ba 0e 00 00 00       	mov    $0xe,%edx
  8e:	89 d3                	mov    %edx,%ebx
  90:	29 c3                	sub    %eax,%ebx
  92:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
  96:	48 89 c7             	mov    %rax,%rdi
  99:	e8 3a 08 00 00       	callq  8d8 <strlen>
  9e:	89 c0                	mov    %eax,%eax
  a0:	48 05 80 11 00 00    	add    $0x1180,%rax
  a6:	89 da                	mov    %ebx,%edx
  a8:	be 20 00 00 00       	mov    $0x20,%esi
  ad:	48 89 c7             	mov    %rax,%rdi
  b0:	e8 51 08 00 00       	callq  906 <memset>
  return buf;
  b5:	b8 80 11 00 00       	mov    $0x1180,%eax
}
  ba:	48 83 c4 28          	add    $0x28,%rsp
  be:	5b                   	pop    %rbx
  bf:	5d                   	pop    %rbp
  c0:	c3                   	retq   

00000000000000c1 <ls>:

void ls(char *path) {
  c1:	55                   	push   %rbp
  c2:	48 89 e5             	mov    %rsp,%rbp
  c5:	41 55                	push   %r13
  c7:	41 54                	push   %r12
  c9:	53                   	push   %rbx
  ca:	48 81 ec 48 02 00 00 	sub    $0x248,%rsp
  d1:	48 89 bd a8 fd ff ff 	mov    %rdi,-0x258(%rbp)
  char buf[512], *p;
  int fd;
  struct dirent de;
  struct stat st;

  if ((fd = open(path, 0)) < 0) {
  d8:	48 8b 85 a8 fd ff ff 	mov    -0x258(%rbp),%rax
  df:	be 00 00 00 00       	mov    $0x0,%esi
  e4:	48 89 c7             	mov    %rax,%rdi
  e7:	e8 43 0a 00 00       	callq  b2f <open>
  ec:	89 45 dc             	mov    %eax,-0x24(%rbp)
  ef:	83 7d dc 00          	cmpl   $0x0,-0x24(%rbp)
  f3:	79 23                	jns    118 <ls+0x57>
    printf(2, "ls: cannot open %s\n", path);
  f5:	48 8b 85 a8 fd ff ff 	mov    -0x258(%rbp),%rax
  fc:	48 89 c2             	mov    %rax,%rdx
  ff:	be 38 0e 00 00       	mov    $0xe38,%esi
 104:	bf 02 00 00 00       	mov    $0x2,%edi
 109:	b8 00 00 00 00       	mov    $0x0,%eax
 10e:	e8 39 04 00 00       	callq  54c <printf>
    return;
 113:	e9 0f 02 00 00       	jmpq   327 <ls+0x266>
  }

  if (fstat(fd, &st) < 0) {
 118:	48 8d 95 b0 fd ff ff 	lea    -0x250(%rbp),%rdx
 11f:	8b 45 dc             	mov    -0x24(%rbp),%eax
 122:	48 89 d6             	mov    %rdx,%rsi
 125:	89 c7                	mov    %eax,%edi
 127:	e8 1b 0a 00 00       	callq  b47 <fstat>
 12c:	85 c0                	test   %eax,%eax
 12e:	79 2d                	jns    15d <ls+0x9c>
    printf(2, "ls: cannot stat %s\n", path);
 130:	48 8b 85 a8 fd ff ff 	mov    -0x258(%rbp),%rax
 137:	48 89 c2             	mov    %rax,%rdx
 13a:	be 4c 0e 00 00       	mov    $0xe4c,%esi
 13f:	bf 02 00 00 00       	mov    $0x2,%edi
 144:	b8 00 00 00 00       	mov    $0x0,%eax
 149:	e8 fe 03 00 00       	callq  54c <printf>
    close(fd);
 14e:	8b 45 dc             	mov    -0x24(%rbp),%eax
 151:	89 c7                	mov    %eax,%edi
 153:	e8 bf 09 00 00       	callq  b17 <close>
    return;
 158:	e9 ca 01 00 00       	jmpq   327 <ls+0x266>
  }

  switch (st.type) {
 15d:	0f b7 85 b0 fd ff ff 	movzwl -0x250(%rbp),%eax
 164:	98                   	cwtl   
 165:	83 f8 01             	cmp    $0x1,%eax
 168:	74 54                	je     1be <ls+0xfd>
 16a:	83 f8 02             	cmp    $0x2,%eax
 16d:	0f 85 aa 01 00 00    	jne    31d <ls+0x25c>
  case T_FILE:
    printf(1, "%s %d %d %d\n", fmtname(path), st.type, st.ino, st.size);
 173:	44 8b ad bc fd ff ff 	mov    -0x244(%rbp),%r13d
 17a:	44 8b a5 b8 fd ff ff 	mov    -0x248(%rbp),%r12d
 181:	0f b7 85 b0 fd ff ff 	movzwl -0x250(%rbp),%eax
 188:	0f bf d8             	movswl %ax,%ebx
 18b:	48 8b 85 a8 fd ff ff 	mov    -0x258(%rbp),%rax
 192:	48 89 c7             	mov    %rax,%rdi
 195:	e8 66 fe ff ff       	callq  0 <fmtname>
 19a:	45 89 e9             	mov    %r13d,%r9d
 19d:	45 89 e0             	mov    %r12d,%r8d
 1a0:	89 d9                	mov    %ebx,%ecx
 1a2:	48 89 c2             	mov    %rax,%rdx
 1a5:	be 60 0e 00 00       	mov    $0xe60,%esi
 1aa:	bf 01 00 00 00       	mov    $0x1,%edi
 1af:	b8 00 00 00 00       	mov    $0x0,%eax
 1b4:	e8 93 03 00 00       	callq  54c <printf>
    break;
 1b9:	e9 5f 01 00 00       	jmpq   31d <ls+0x25c>

  case T_DIR:
    if (strlen(path) + 1 + DIRSIZ + 1 > sizeof buf) {
 1be:	48 8b 85 a8 fd ff ff 	mov    -0x258(%rbp),%rax
 1c5:	48 89 c7             	mov    %rax,%rdi
 1c8:	e8 0b 07 00 00       	callq  8d8 <strlen>
 1cd:	83 c0 10             	add    $0x10,%eax
 1d0:	3d 00 02 00 00       	cmp    $0x200,%eax
 1d5:	76 19                	jbe    1f0 <ls+0x12f>
      printf(1, "ls: path too long\n");
 1d7:	be 6d 0e 00 00       	mov    $0xe6d,%esi
 1dc:	bf 01 00 00 00       	mov    $0x1,%edi
 1e1:	b8 00 00 00 00       	mov    $0x0,%eax
 1e6:	e8 61 03 00 00       	callq  54c <printf>
      break;
 1eb:	e9 2d 01 00 00       	jmpq   31d <ls+0x25c>
    }
    strcpy(buf, path);
 1f0:	48 8b 95 a8 fd ff ff 	mov    -0x258(%rbp),%rdx
 1f7:	48 8d 85 d0 fd ff ff 	lea    -0x230(%rbp),%rax
 1fe:	48 89 d6             	mov    %rdx,%rsi
 201:	48 89 c7             	mov    %rax,%rdi
 204:	e8 41 06 00 00       	callq  84a <strcpy>
    p = buf + strlen(buf);
 209:	48 8d 85 d0 fd ff ff 	lea    -0x230(%rbp),%rax
 210:	48 89 c7             	mov    %rax,%rdi
 213:	e8 c0 06 00 00       	callq  8d8 <strlen>
 218:	89 c2                	mov    %eax,%edx
 21a:	48 8d 85 d0 fd ff ff 	lea    -0x230(%rbp),%rax
 221:	48 01 d0             	add    %rdx,%rax
 224:	48 89 45 d0          	mov    %rax,-0x30(%rbp)
    *p++ = '/';
 228:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
 22c:	48 8d 50 01          	lea    0x1(%rax),%rdx
 230:	48 89 55 d0          	mov    %rdx,-0x30(%rbp)
 234:	c6 00 2f             	movb   $0x2f,(%rax)
    while (read(fd, &de, sizeof(de)) == sizeof(de)) {
 237:	e9 be 00 00 00       	jmpq   2fa <ls+0x239>
      if (de.inum == 0)
 23c:	0f b7 85 c0 fd ff ff 	movzwl -0x240(%rbp),%eax
 243:	66 85 c0             	test   %ax,%ax
 246:	75 05                	jne    24d <ls+0x18c>
        continue;
 248:	e9 ad 00 00 00       	jmpq   2fa <ls+0x239>
      memmove(p, de.name, DIRSIZ);
 24d:	48 8d 85 c0 fd ff ff 	lea    -0x240(%rbp),%rax
 254:	48 8d 48 02          	lea    0x2(%rax),%rcx
 258:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
 25c:	ba 0e 00 00 00       	mov    $0xe,%edx
 261:	48 89 ce             	mov    %rcx,%rsi
 264:	48 89 c7             	mov    %rax,%rdi
 267:	e8 2a 08 00 00       	callq  a96 <memmove>
      p[DIRSIZ] = 0;
 26c:	48 8b 45 d0          	mov    -0x30(%rbp),%rax
 270:	48 83 c0 0e          	add    $0xe,%rax
 274:	c6 00 00             	movb   $0x0,(%rax)
      if (stat(buf, &st) < 0) {
 277:	48 8d 95 b0 fd ff ff 	lea    -0x250(%rbp),%rdx
 27e:	48 8d 85 d0 fd ff ff 	lea    -0x230(%rbp),%rax
 285:	48 89 d6             	mov    %rdx,%rsi
 288:	48 89 c7             	mov    %rax,%rdi
 28b:	e8 5e 07 00 00       	callq  9ee <stat>
 290:	85 c0                	test   %eax,%eax
 292:	79 20                	jns    2b4 <ls+0x1f3>
        printf(1, "ls: cannot stat %s\n", buf);
 294:	48 8d 85 d0 fd ff ff 	lea    -0x230(%rbp),%rax
 29b:	48 89 c2             	mov    %rax,%rdx
 29e:	be 4c 0e 00 00       	mov    $0xe4c,%esi
 2a3:	bf 01 00 00 00       	mov    $0x1,%edi
 2a8:	b8 00 00 00 00       	mov    $0x0,%eax
 2ad:	e8 9a 02 00 00       	callq  54c <printf>
        continue;
 2b2:	eb 46                	jmp    2fa <ls+0x239>
      }
      printf(1, "%s %d %d %d\n", fmtname(buf), st.type, st.ino, st.size);
 2b4:	44 8b ad bc fd ff ff 	mov    -0x244(%rbp),%r13d
 2bb:	44 8b a5 b8 fd ff ff 	mov    -0x248(%rbp),%r12d
 2c2:	0f b7 85 b0 fd ff ff 	movzwl -0x250(%rbp),%eax
 2c9:	0f bf d8             	movswl %ax,%ebx
 2cc:	48 8d 85 d0 fd ff ff 	lea    -0x230(%rbp),%rax
 2d3:	48 89 c7             	mov    %rax,%rdi
 2d6:	e8 25 fd ff ff       	callq  0 <fmtname>
 2db:	45 89 e9             	mov    %r13d,%r9d
 2de:	45 89 e0             	mov    %r12d,%r8d
 2e1:	89 d9                	mov    %ebx,%ecx
 2e3:	48 89 c2             	mov    %rax,%rdx
 2e6:	be 60 0e 00 00       	mov    $0xe60,%esi
 2eb:	bf 01 00 00 00       	mov    $0x1,%edi
 2f0:	b8 00 00 00 00       	mov    $0x0,%eax
 2f5:	e8 52 02 00 00       	callq  54c <printf>
      break;
    }
    strcpy(buf, path);
    p = buf + strlen(buf);
    *p++ = '/';
    while (read(fd, &de, sizeof(de)) == sizeof(de)) {
 2fa:	48 8d 8d c0 fd ff ff 	lea    -0x240(%rbp),%rcx
 301:	8b 45 dc             	mov    -0x24(%rbp),%eax
 304:	ba 10 00 00 00       	mov    $0x10,%edx
 309:	48 89 ce             	mov    %rcx,%rsi
 30c:	89 c7                	mov    %eax,%edi
 30e:	e8 f4 07 00 00       	callq  b07 <read>
 313:	83 f8 10             	cmp    $0x10,%eax
 316:	0f 84 20 ff ff ff    	je     23c <ls+0x17b>
        printf(1, "ls: cannot stat %s\n", buf);
        continue;
      }
      printf(1, "%s %d %d %d\n", fmtname(buf), st.type, st.ino, st.size);
    }
    break;
 31c:	90                   	nop
  }
  close(fd);
 31d:	8b 45 dc             	mov    -0x24(%rbp),%eax
 320:	89 c7                	mov    %eax,%edi
 322:	e8 f0 07 00 00       	callq  b17 <close>
}
 327:	48 81 c4 48 02 00 00 	add    $0x248,%rsp
 32e:	5b                   	pop    %rbx
 32f:	41 5c                	pop    %r12
 331:	41 5d                	pop    %r13
 333:	5d                   	pop    %rbp
 334:	c3                   	retq   

0000000000000335 <main>:

int main(int argc, char *argv[]) {
 335:	55                   	push   %rbp
 336:	48 89 e5             	mov    %rsp,%rbp
 339:	48 83 ec 20          	sub    $0x20,%rsp
 33d:	89 7d ec             	mov    %edi,-0x14(%rbp)
 340:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int i;

  if (argc < 2) {
 344:	83 7d ec 01          	cmpl   $0x1,-0x14(%rbp)
 348:	7f 0f                	jg     359 <main+0x24>
    ls(".");
 34a:	bf 80 0e 00 00       	mov    $0xe80,%edi
 34f:	e8 6d fd ff ff       	callq  c1 <ls>
    exit();
 354:	e8 96 07 00 00       	callq  aef <exit>
  }
  for (i = 1; i < argc; i++)
 359:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%rbp)
 360:	eb 23                	jmp    385 <main+0x50>
    ls(argv[i]);
 362:	8b 45 fc             	mov    -0x4(%rbp),%eax
 365:	48 98                	cltq   
 367:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
 36e:	00 
 36f:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 373:	48 01 d0             	add    %rdx,%rax
 376:	48 8b 00             	mov    (%rax),%rax
 379:	48 89 c7             	mov    %rax,%rdi
 37c:	e8 40 fd ff ff       	callq  c1 <ls>

  if (argc < 2) {
    ls(".");
    exit();
  }
  for (i = 1; i < argc; i++)
 381:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 385:	8b 45 fc             	mov    -0x4(%rbp),%eax
 388:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 38b:	7c d5                	jl     362 <main+0x2d>
    ls(argv[i]);
  exit();
 38d:	e8 5d 07 00 00       	callq  aef <exit>

0000000000000392 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
 392:	55                   	push   %rbp
 393:	48 89 e5             	mov    %rsp,%rbp
 396:	48 83 ec 10          	sub    $0x10,%rsp
 39a:	89 7d fc             	mov    %edi,-0x4(%rbp)
 39d:	89 f0                	mov    %esi,%eax
 39f:	88 45 f8             	mov    %al,-0x8(%rbp)
 3a2:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
 3a6:	8b 45 fc             	mov    -0x4(%rbp),%eax
 3a9:	ba 01 00 00 00       	mov    $0x1,%edx
 3ae:	48 89 ce             	mov    %rcx,%rsi
 3b1:	89 c7                	mov    %eax,%edi
 3b3:	e8 57 07 00 00       	callq  b0f <write>
 3b8:	90                   	nop
 3b9:	c9                   	leaveq 
 3ba:	c3                   	retq   

00000000000003bb <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
 3bb:	55                   	push   %rbp
 3bc:	48 89 e5             	mov    %rsp,%rbp
 3bf:	48 83 ec 40          	sub    $0x40,%rsp
 3c3:	89 7d cc             	mov    %edi,-0x34(%rbp)
 3c6:	89 75 c8             	mov    %esi,-0x38(%rbp)
 3c9:	89 55 c4             	mov    %edx,-0x3c(%rbp)
 3cc:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
 3cf:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 3d3:	74 1f                	je     3f4 <printint64+0x39>
 3d5:	8b 45 c8             	mov    -0x38(%rbp),%eax
 3d8:	c1 e8 1f             	shr    $0x1f,%eax
 3db:	0f b6 c0             	movzbl %al,%eax
 3de:	89 45 c0             	mov    %eax,-0x40(%rbp)
 3e1:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 3e5:	74 0d                	je     3f4 <printint64+0x39>
    x = -xx;
 3e7:	8b 45 c8             	mov    -0x38(%rbp),%eax
 3ea:	f7 d8                	neg    %eax
 3ec:	48 98                	cltq   
 3ee:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 3f2:	eb 09                	jmp    3fd <printint64+0x42>
  else
    x = xx;
 3f4:	8b 45 c8             	mov    -0x38(%rbp),%eax
 3f7:	48 98                	cltq   
 3f9:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 3fd:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 404:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 407:	8d 41 01             	lea    0x1(%rcx),%eax
 40a:	89 45 fc             	mov    %eax,-0x4(%rbp)
 40d:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 410:	48 63 f0             	movslq %eax,%rsi
 413:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 417:	ba 00 00 00 00       	mov    $0x0,%edx
 41c:	48 f7 f6             	div    %rsi
 41f:	48 89 d0             	mov    %rdx,%rax
 422:	0f b6 90 40 11 00 00 	movzbl 0x1140(%rax),%edx
 429:	48 63 c1             	movslq %ecx,%rax
 42c:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 430:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 433:	48 63 f8             	movslq %eax,%rdi
 436:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 43a:	ba 00 00 00 00       	mov    $0x0,%edx
 43f:	48 f7 f7             	div    %rdi
 442:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 446:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 44b:	75 b7                	jne    404 <printint64+0x49>

  if (sgn)
 44d:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 451:	74 2b                	je     47e <printint64+0xc3>
    buf[i++] = '-';
 453:	8b 45 fc             	mov    -0x4(%rbp),%eax
 456:	8d 50 01             	lea    0x1(%rax),%edx
 459:	89 55 fc             	mov    %edx,-0x4(%rbp)
 45c:	48 98                	cltq   
 45e:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 463:	eb 19                	jmp    47e <printint64+0xc3>
    putc(fd, buf[i]);
 465:	8b 45 fc             	mov    -0x4(%rbp),%eax
 468:	48 98                	cltq   
 46a:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 46f:	0f be d0             	movsbl %al,%edx
 472:	8b 45 cc             	mov    -0x34(%rbp),%eax
 475:	89 d6                	mov    %edx,%esi
 477:	89 c7                	mov    %eax,%edi
 479:	e8 14 ff ff ff       	callq  392 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 47e:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 482:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 486:	79 dd                	jns    465 <printint64+0xaa>
    putc(fd, buf[i]);
}
 488:	90                   	nop
 489:	c9                   	leaveq 
 48a:	c3                   	retq   

000000000000048b <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 48b:	55                   	push   %rbp
 48c:	48 89 e5             	mov    %rsp,%rbp
 48f:	48 83 ec 30          	sub    $0x30,%rsp
 493:	89 7d dc             	mov    %edi,-0x24(%rbp)
 496:	89 75 d8             	mov    %esi,-0x28(%rbp)
 499:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 49c:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 49f:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 4a6:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 4aa:	74 17                	je     4c3 <printint+0x38>
 4ac:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 4b0:	79 11                	jns    4c3 <printint+0x38>
    neg = 1;
 4b2:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 4b9:	8b 45 d8             	mov    -0x28(%rbp),%eax
 4bc:	f7 d8                	neg    %eax
 4be:	89 45 f4             	mov    %eax,-0xc(%rbp)
 4c1:	eb 06                	jmp    4c9 <printint+0x3e>
  } else {
    x = xx;
 4c3:	8b 45 d8             	mov    -0x28(%rbp),%eax
 4c6:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 4c9:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 4d0:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 4d3:	8d 41 01             	lea    0x1(%rcx),%eax
 4d6:	89 45 fc             	mov    %eax,-0x4(%rbp)
 4d9:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 4dc:	8b 45 f4             	mov    -0xc(%rbp),%eax
 4df:	ba 00 00 00 00       	mov    $0x0,%edx
 4e4:	f7 f6                	div    %esi
 4e6:	89 d0                	mov    %edx,%eax
 4e8:	89 c0                	mov    %eax,%eax
 4ea:	0f b6 90 60 11 00 00 	movzbl 0x1160(%rax),%edx
 4f1:	48 63 c1             	movslq %ecx,%rax
 4f4:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 4f8:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 4fb:	8b 45 f4             	mov    -0xc(%rbp),%eax
 4fe:	ba 00 00 00 00       	mov    $0x0,%edx
 503:	f7 f7                	div    %edi
 505:	89 45 f4             	mov    %eax,-0xc(%rbp)
 508:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 50c:	75 c2                	jne    4d0 <printint+0x45>
  if (neg)
 50e:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 512:	74 2b                	je     53f <printint+0xb4>
    buf[i++] = '-';
 514:	8b 45 fc             	mov    -0x4(%rbp),%eax
 517:	8d 50 01             	lea    0x1(%rax),%edx
 51a:	89 55 fc             	mov    %edx,-0x4(%rbp)
 51d:	48 98                	cltq   
 51f:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 524:	eb 19                	jmp    53f <printint+0xb4>
    putc(fd, buf[i]);
 526:	8b 45 fc             	mov    -0x4(%rbp),%eax
 529:	48 98                	cltq   
 52b:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 530:	0f be d0             	movsbl %al,%edx
 533:	8b 45 dc             	mov    -0x24(%rbp),%eax
 536:	89 d6                	mov    %edx,%esi
 538:	89 c7                	mov    %eax,%edi
 53a:	e8 53 fe ff ff       	callq  392 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 53f:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 543:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 547:	79 dd                	jns    526 <printint+0x9b>
    putc(fd, buf[i]);
}
 549:	90                   	nop
 54a:	c9                   	leaveq 
 54b:	c3                   	retq   

000000000000054c <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 54c:	55                   	push   %rbp
 54d:	48 89 e5             	mov    %rsp,%rbp
 550:	48 83 ec 70          	sub    $0x70,%rsp
 554:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 557:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 55b:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 55f:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 563:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 567:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 56b:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 572:	48 8d 45 10          	lea    0x10(%rbp),%rax
 576:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 57a:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 57e:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 582:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 589:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 590:	e9 68 02 00 00       	jmpq   7fd <printf+0x2b1>
    c = fmt[i] & 0xff;
 595:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 598:	48 63 d0             	movslq %eax,%rdx
 59b:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 59f:	48 01 d0             	add    %rdx,%rax
 5a2:	0f b6 00             	movzbl (%rax),%eax
 5a5:	0f be c0             	movsbl %al,%eax
 5a8:	25 ff 00 00 00       	and    $0xff,%eax
 5ad:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 5b0:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 5b4:	75 30                	jne    5e6 <printf+0x9a>
      if (c == '%') {
 5b6:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 5ba:	75 13                	jne    5cf <printf+0x83>
        state = '%';
 5bc:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 5c3:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 5ca:	e9 2a 02 00 00       	jmpq   7f9 <printf+0x2ad>
      } else {
        putc(fd, c);
 5cf:	8b 45 b8             	mov    -0x48(%rbp),%eax
 5d2:	0f be d0             	movsbl %al,%edx
 5d5:	8b 45 9c             	mov    -0x64(%rbp),%eax
 5d8:	89 d6                	mov    %edx,%esi
 5da:	89 c7                	mov    %eax,%edi
 5dc:	e8 b1 fd ff ff       	callq  392 <putc>
 5e1:	e9 13 02 00 00       	jmpq   7f9 <printf+0x2ad>
      }
    } else if (state == '%') {
 5e6:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 5ea:	0f 85 09 02 00 00    	jne    7f9 <printf+0x2ad>
      if (c == 'l') {
 5f0:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 5f4:	75 0c                	jne    602 <printf+0xb6>
        lflag = 1;
 5f6:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 5fd:	e9 f7 01 00 00       	jmpq   7f9 <printf+0x2ad>
      } else if (c == 'd') {
 602:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 606:	0f 85 95 00 00 00    	jne    6a1 <printf+0x155>
        if (lflag == 1)
 60c:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 610:	75 49                	jne    65b <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 612:	8b 45 a0             	mov    -0x60(%rbp),%eax
 615:	83 f8 30             	cmp    $0x30,%eax
 618:	73 17                	jae    631 <printf+0xe5>
 61a:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 61e:	8b 55 a0             	mov    -0x60(%rbp),%edx
 621:	89 d2                	mov    %edx,%edx
 623:	48 01 d0             	add    %rdx,%rax
 626:	8b 55 a0             	mov    -0x60(%rbp),%edx
 629:	83 c2 08             	add    $0x8,%edx
 62c:	89 55 a0             	mov    %edx,-0x60(%rbp)
 62f:	eb 0c                	jmp    63d <printf+0xf1>
 631:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 635:	48 8d 50 08          	lea    0x8(%rax),%rdx
 639:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 63d:	48 8b 00             	mov    (%rax),%rax
 640:	89 c6                	mov    %eax,%esi
 642:	8b 45 9c             	mov    -0x64(%rbp),%eax
 645:	b9 01 00 00 00       	mov    $0x1,%ecx
 64a:	ba 0a 00 00 00       	mov    $0xa,%edx
 64f:	89 c7                	mov    %eax,%edi
 651:	e8 65 fd ff ff       	callq  3bb <printint64>
 656:	e9 97 01 00 00       	jmpq   7f2 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 65b:	8b 45 a0             	mov    -0x60(%rbp),%eax
 65e:	83 f8 30             	cmp    $0x30,%eax
 661:	73 17                	jae    67a <printf+0x12e>
 663:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 667:	8b 55 a0             	mov    -0x60(%rbp),%edx
 66a:	89 d2                	mov    %edx,%edx
 66c:	48 01 d0             	add    %rdx,%rax
 66f:	8b 55 a0             	mov    -0x60(%rbp),%edx
 672:	83 c2 08             	add    $0x8,%edx
 675:	89 55 a0             	mov    %edx,-0x60(%rbp)
 678:	eb 0c                	jmp    686 <printf+0x13a>
 67a:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 67e:	48 8d 50 08          	lea    0x8(%rax),%rdx
 682:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 686:	8b 30                	mov    (%rax),%esi
 688:	8b 45 9c             	mov    -0x64(%rbp),%eax
 68b:	b9 01 00 00 00       	mov    $0x1,%ecx
 690:	ba 0a 00 00 00       	mov    $0xa,%edx
 695:	89 c7                	mov    %eax,%edi
 697:	e8 ef fd ff ff       	callq  48b <printint>
 69c:	e9 51 01 00 00       	jmpq   7f2 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 6a1:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 6a5:	74 0a                	je     6b1 <printf+0x165>
 6a7:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 6ab:	0f 85 95 00 00 00    	jne    746 <printf+0x1fa>
        if (lflag == 1)
 6b1:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 6b5:	75 49                	jne    700 <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 6b7:	8b 45 a0             	mov    -0x60(%rbp),%eax
 6ba:	83 f8 30             	cmp    $0x30,%eax
 6bd:	73 17                	jae    6d6 <printf+0x18a>
 6bf:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 6c3:	8b 55 a0             	mov    -0x60(%rbp),%edx
 6c6:	89 d2                	mov    %edx,%edx
 6c8:	48 01 d0             	add    %rdx,%rax
 6cb:	8b 55 a0             	mov    -0x60(%rbp),%edx
 6ce:	83 c2 08             	add    $0x8,%edx
 6d1:	89 55 a0             	mov    %edx,-0x60(%rbp)
 6d4:	eb 0c                	jmp    6e2 <printf+0x196>
 6d6:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 6da:	48 8d 50 08          	lea    0x8(%rax),%rdx
 6de:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 6e2:	48 8b 00             	mov    (%rax),%rax
 6e5:	89 c6                	mov    %eax,%esi
 6e7:	8b 45 9c             	mov    -0x64(%rbp),%eax
 6ea:	b9 00 00 00 00       	mov    $0x0,%ecx
 6ef:	ba 10 00 00 00       	mov    $0x10,%edx
 6f4:	89 c7                	mov    %eax,%edi
 6f6:	e8 c0 fc ff ff       	callq  3bb <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 6fb:	e9 f2 00 00 00       	jmpq   7f2 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 700:	8b 45 a0             	mov    -0x60(%rbp),%eax
 703:	83 f8 30             	cmp    $0x30,%eax
 706:	73 17                	jae    71f <printf+0x1d3>
 708:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 70c:	8b 55 a0             	mov    -0x60(%rbp),%edx
 70f:	89 d2                	mov    %edx,%edx
 711:	48 01 d0             	add    %rdx,%rax
 714:	8b 55 a0             	mov    -0x60(%rbp),%edx
 717:	83 c2 08             	add    $0x8,%edx
 71a:	89 55 a0             	mov    %edx,-0x60(%rbp)
 71d:	eb 0c                	jmp    72b <printf+0x1df>
 71f:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 723:	48 8d 50 08          	lea    0x8(%rax),%rdx
 727:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 72b:	8b 30                	mov    (%rax),%esi
 72d:	8b 45 9c             	mov    -0x64(%rbp),%eax
 730:	b9 00 00 00 00       	mov    $0x0,%ecx
 735:	ba 10 00 00 00       	mov    $0x10,%edx
 73a:	89 c7                	mov    %eax,%edi
 73c:	e8 4a fd ff ff       	callq  48b <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 741:	e9 ac 00 00 00       	jmpq   7f2 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 746:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 74a:	75 6b                	jne    7b7 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 74c:	8b 45 a0             	mov    -0x60(%rbp),%eax
 74f:	83 f8 30             	cmp    $0x30,%eax
 752:	73 17                	jae    76b <printf+0x21f>
 754:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 758:	8b 55 a0             	mov    -0x60(%rbp),%edx
 75b:	89 d2                	mov    %edx,%edx
 75d:	48 01 d0             	add    %rdx,%rax
 760:	8b 55 a0             	mov    -0x60(%rbp),%edx
 763:	83 c2 08             	add    $0x8,%edx
 766:	89 55 a0             	mov    %edx,-0x60(%rbp)
 769:	eb 0c                	jmp    777 <printf+0x22b>
 76b:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 76f:	48 8d 50 08          	lea    0x8(%rax),%rdx
 773:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 777:	48 8b 00             	mov    (%rax),%rax
 77a:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 77e:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 783:	75 25                	jne    7aa <printf+0x25e>
          s = "(null)";
 785:	48 c7 45 c8 82 0e 00 	movq   $0xe82,-0x38(%rbp)
 78c:	00 
        for (; *s; s++)
 78d:	eb 1b                	jmp    7aa <printf+0x25e>
          putc(fd, *s);
 78f:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 793:	0f b6 00             	movzbl (%rax),%eax
 796:	0f be d0             	movsbl %al,%edx
 799:	8b 45 9c             	mov    -0x64(%rbp),%eax
 79c:	89 d6                	mov    %edx,%esi
 79e:	89 c7                	mov    %eax,%edi
 7a0:	e8 ed fb ff ff       	callq  392 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 7a5:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 7aa:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 7ae:	0f b6 00             	movzbl (%rax),%eax
 7b1:	84 c0                	test   %al,%al
 7b3:	75 da                	jne    78f <printf+0x243>
 7b5:	eb 3b                	jmp    7f2 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 7b7:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 7bb:	75 14                	jne    7d1 <printf+0x285>
        putc(fd, c);
 7bd:	8b 45 b8             	mov    -0x48(%rbp),%eax
 7c0:	0f be d0             	movsbl %al,%edx
 7c3:	8b 45 9c             	mov    -0x64(%rbp),%eax
 7c6:	89 d6                	mov    %edx,%esi
 7c8:	89 c7                	mov    %eax,%edi
 7ca:	e8 c3 fb ff ff       	callq  392 <putc>
 7cf:	eb 21                	jmp    7f2 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 7d1:	8b 45 9c             	mov    -0x64(%rbp),%eax
 7d4:	be 25 00 00 00       	mov    $0x25,%esi
 7d9:	89 c7                	mov    %eax,%edi
 7db:	e8 b2 fb ff ff       	callq  392 <putc>
        putc(fd, c);
 7e0:	8b 45 b8             	mov    -0x48(%rbp),%eax
 7e3:	0f be d0             	movsbl %al,%edx
 7e6:	8b 45 9c             	mov    -0x64(%rbp),%eax
 7e9:	89 d6                	mov    %edx,%esi
 7eb:	89 c7                	mov    %eax,%edi
 7ed:	e8 a0 fb ff ff       	callq  392 <putc>
      }
      state = 0;
 7f2:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 7f9:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 7fd:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 800:	48 63 d0             	movslq %eax,%rdx
 803:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 807:	48 01 d0             	add    %rdx,%rax
 80a:	0f b6 00             	movzbl (%rax),%eax
 80d:	84 c0                	test   %al,%al
 80f:	0f 85 80 fd ff ff    	jne    595 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 815:	90                   	nop
 816:	c9                   	leaveq 
 817:	c3                   	retq   

0000000000000818 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 818:	55                   	push   %rbp
 819:	48 89 e5             	mov    %rsp,%rbp
 81c:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 820:	89 75 f4             	mov    %esi,-0xc(%rbp)
 823:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 826:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 82a:	8b 55 f0             	mov    -0x10(%rbp),%edx
 82d:	8b 45 f4             	mov    -0xc(%rbp),%eax
 830:	48 89 ce             	mov    %rcx,%rsi
 833:	48 89 f7             	mov    %rsi,%rdi
 836:	89 d1                	mov    %edx,%ecx
 838:	fc                   	cld    
 839:	f3 aa                	rep stos %al,%es:(%rdi)
 83b:	89 ca                	mov    %ecx,%edx
 83d:	48 89 fe             	mov    %rdi,%rsi
 840:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 844:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 847:	90                   	nop
 848:	5d                   	pop    %rbp
 849:	c3                   	retq   

000000000000084a <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 84a:	55                   	push   %rbp
 84b:	48 89 e5             	mov    %rsp,%rbp
 84e:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 852:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 856:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 85a:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 85e:	90                   	nop
 85f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 863:	48 8d 50 01          	lea    0x1(%rax),%rdx
 867:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 86b:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 86f:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 873:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 877:	0f b6 12             	movzbl (%rdx),%edx
 87a:	88 10                	mov    %dl,(%rax)
 87c:	0f b6 00             	movzbl (%rax),%eax
 87f:	84 c0                	test   %al,%al
 881:	75 dc                	jne    85f <strcpy+0x15>
    ;
  return os;
 883:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 887:	5d                   	pop    %rbp
 888:	c3                   	retq   

0000000000000889 <strcmp>:

int strcmp(const char *p, const char *q) {
 889:	55                   	push   %rbp
 88a:	48 89 e5             	mov    %rsp,%rbp
 88d:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 891:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 895:	eb 0a                	jmp    8a1 <strcmp+0x18>
    p++, q++;
 897:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 89c:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 8a1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8a5:	0f b6 00             	movzbl (%rax),%eax
 8a8:	84 c0                	test   %al,%al
 8aa:	74 12                	je     8be <strcmp+0x35>
 8ac:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8b0:	0f b6 10             	movzbl (%rax),%edx
 8b3:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8b7:	0f b6 00             	movzbl (%rax),%eax
 8ba:	38 c2                	cmp    %al,%dl
 8bc:	74 d9                	je     897 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 8be:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 8c2:	0f b6 00             	movzbl (%rax),%eax
 8c5:	0f b6 d0             	movzbl %al,%edx
 8c8:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 8cc:	0f b6 00             	movzbl (%rax),%eax
 8cf:	0f b6 c0             	movzbl %al,%eax
 8d2:	29 c2                	sub    %eax,%edx
 8d4:	89 d0                	mov    %edx,%eax
}
 8d6:	5d                   	pop    %rbp
 8d7:	c3                   	retq   

00000000000008d8 <strlen>:

uint strlen(char *s) {
 8d8:	55                   	push   %rbp
 8d9:	48 89 e5             	mov    %rsp,%rbp
 8dc:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 8e0:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 8e7:	eb 04                	jmp    8ed <strlen+0x15>
 8e9:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 8ed:	8b 45 fc             	mov    -0x4(%rbp),%eax
 8f0:	48 63 d0             	movslq %eax,%rdx
 8f3:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 8f7:	48 01 d0             	add    %rdx,%rax
 8fa:	0f b6 00             	movzbl (%rax),%eax
 8fd:	84 c0                	test   %al,%al
 8ff:	75 e8                	jne    8e9 <strlen+0x11>
    ;
  return n;
 901:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 904:	5d                   	pop    %rbp
 905:	c3                   	retq   

0000000000000906 <memset>:

void *memset(void *dst, int c, uint n) {
 906:	55                   	push   %rbp
 907:	48 89 e5             	mov    %rsp,%rbp
 90a:	48 83 ec 10          	sub    $0x10,%rsp
 90e:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 912:	89 75 f4             	mov    %esi,-0xc(%rbp)
 915:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 918:	8b 55 f0             	mov    -0x10(%rbp),%edx
 91b:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 91e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 922:	89 ce                	mov    %ecx,%esi
 924:	48 89 c7             	mov    %rax,%rdi
 927:	e8 ec fe ff ff       	callq  818 <stosb>
  return dst;
 92c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 930:	c9                   	leaveq 
 931:	c3                   	retq   

0000000000000932 <strchr>:

char *strchr(const char *s, char c) {
 932:	55                   	push   %rbp
 933:	48 89 e5             	mov    %rsp,%rbp
 936:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 93a:	89 f0                	mov    %esi,%eax
 93c:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 93f:	eb 17                	jmp    958 <strchr+0x26>
    if (*s == c)
 941:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 945:	0f b6 00             	movzbl (%rax),%eax
 948:	3a 45 f4             	cmp    -0xc(%rbp),%al
 94b:	75 06                	jne    953 <strchr+0x21>
      return (char *)s;
 94d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 951:	eb 15                	jmp    968 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 953:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 958:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 95c:	0f b6 00             	movzbl (%rax),%eax
 95f:	84 c0                	test   %al,%al
 961:	75 de                	jne    941 <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 963:	b8 00 00 00 00       	mov    $0x0,%eax
}
 968:	5d                   	pop    %rbp
 969:	c3                   	retq   

000000000000096a <gets>:

char *gets(char *buf, int max) {
 96a:	55                   	push   %rbp
 96b:	48 89 e5             	mov    %rsp,%rbp
 96e:	48 83 ec 20          	sub    $0x20,%rsp
 972:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 976:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 979:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 980:	eb 48                	jmp    9ca <gets+0x60>
    cc = read(0, &c, 1);
 982:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 986:	ba 01 00 00 00       	mov    $0x1,%edx
 98b:	48 89 c6             	mov    %rax,%rsi
 98e:	bf 00 00 00 00       	mov    $0x0,%edi
 993:	e8 6f 01 00 00       	callq  b07 <read>
 998:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 99b:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 99f:	7e 36                	jle    9d7 <gets+0x6d>
      break;
    buf[i++] = c;
 9a1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 9a4:	8d 50 01             	lea    0x1(%rax),%edx
 9a7:	89 55 fc             	mov    %edx,-0x4(%rbp)
 9aa:	48 63 d0             	movslq %eax,%rdx
 9ad:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 9b1:	48 01 c2             	add    %rax,%rdx
 9b4:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 9b8:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 9ba:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 9be:	3c 0a                	cmp    $0xa,%al
 9c0:	74 16                	je     9d8 <gets+0x6e>
 9c2:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 9c6:	3c 0d                	cmp    $0xd,%al
 9c8:	74 0e                	je     9d8 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 9ca:	8b 45 fc             	mov    -0x4(%rbp),%eax
 9cd:	83 c0 01             	add    $0x1,%eax
 9d0:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 9d3:	7c ad                	jl     982 <gets+0x18>
 9d5:	eb 01                	jmp    9d8 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 9d7:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 9d8:	8b 45 fc             	mov    -0x4(%rbp),%eax
 9db:	48 63 d0             	movslq %eax,%rdx
 9de:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 9e2:	48 01 d0             	add    %rdx,%rax
 9e5:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 9e8:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 9ec:	c9                   	leaveq 
 9ed:	c3                   	retq   

00000000000009ee <stat>:

int stat(char *n, struct stat *st) {
 9ee:	55                   	push   %rbp
 9ef:	48 89 e5             	mov    %rsp,%rbp
 9f2:	48 83 ec 20          	sub    $0x20,%rsp
 9f6:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 9fa:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 9fe:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a02:	be 00 00 00 00       	mov    $0x0,%esi
 a07:	48 89 c7             	mov    %rax,%rdi
 a0a:	e8 20 01 00 00       	callq  b2f <open>
 a0f:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 a12:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 a16:	79 07                	jns    a1f <stat+0x31>
    return -1;
 a18:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 a1d:	eb 21                	jmp    a40 <stat+0x52>
  r = fstat(fd, st);
 a1f:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 a23:	8b 45 fc             	mov    -0x4(%rbp),%eax
 a26:	48 89 d6             	mov    %rdx,%rsi
 a29:	89 c7                	mov    %eax,%edi
 a2b:	e8 17 01 00 00       	callq  b47 <fstat>
 a30:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 a33:	8b 45 fc             	mov    -0x4(%rbp),%eax
 a36:	89 c7                	mov    %eax,%edi
 a38:	e8 da 00 00 00       	callq  b17 <close>
  return r;
 a3d:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 a40:	c9                   	leaveq 
 a41:	c3                   	retq   

0000000000000a42 <atoi>:

int atoi(const char *s) {
 a42:	55                   	push   %rbp
 a43:	48 89 e5             	mov    %rsp,%rbp
 a46:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 a4a:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 a51:	eb 28                	jmp    a7b <atoi+0x39>
    n = n * 10 + *s++ - '0';
 a53:	8b 55 fc             	mov    -0x4(%rbp),%edx
 a56:	89 d0                	mov    %edx,%eax
 a58:	c1 e0 02             	shl    $0x2,%eax
 a5b:	01 d0                	add    %edx,%eax
 a5d:	01 c0                	add    %eax,%eax
 a5f:	89 c1                	mov    %eax,%ecx
 a61:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a65:	48 8d 50 01          	lea    0x1(%rax),%rdx
 a69:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 a6d:	0f b6 00             	movzbl (%rax),%eax
 a70:	0f be c0             	movsbl %al,%eax
 a73:	01 c8                	add    %ecx,%eax
 a75:	83 e8 30             	sub    $0x30,%eax
 a78:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 a7b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a7f:	0f b6 00             	movzbl (%rax),%eax
 a82:	3c 2f                	cmp    $0x2f,%al
 a84:	7e 0b                	jle    a91 <atoi+0x4f>
 a86:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 a8a:	0f b6 00             	movzbl (%rax),%eax
 a8d:	3c 39                	cmp    $0x39,%al
 a8f:	7e c2                	jle    a53 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 a91:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 a94:	5d                   	pop    %rbp
 a95:	c3                   	retq   

0000000000000a96 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 a96:	55                   	push   %rbp
 a97:	48 89 e5             	mov    %rsp,%rbp
 a9a:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 a9e:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 aa2:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 aa5:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 aa9:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 aad:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 ab1:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 ab5:	eb 1d                	jmp    ad4 <memmove+0x3e>
    *dst++ = *src++;
 ab7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 abb:	48 8d 50 01          	lea    0x1(%rax),%rdx
 abf:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 ac3:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 ac7:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 acb:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 acf:	0f b6 12             	movzbl (%rdx),%edx
 ad2:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 ad4:	8b 45 dc             	mov    -0x24(%rbp),%eax
 ad7:	8d 50 ff             	lea    -0x1(%rax),%edx
 ada:	89 55 dc             	mov    %edx,-0x24(%rbp)
 add:	85 c0                	test   %eax,%eax
 adf:	7f d6                	jg     ab7 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 ae1:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 ae5:	5d                   	pop    %rbp
 ae6:	c3                   	retq   

0000000000000ae7 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 ae7:	b8 01 00 00 00       	mov    $0x1,%eax
 aec:	cd 40                	int    $0x40
 aee:	c3                   	retq   

0000000000000aef <exit>:
SYSCALL(exit)
 aef:	b8 02 00 00 00       	mov    $0x2,%eax
 af4:	cd 40                	int    $0x40
 af6:	c3                   	retq   

0000000000000af7 <wait>:
SYSCALL(wait)
 af7:	b8 03 00 00 00       	mov    $0x3,%eax
 afc:	cd 40                	int    $0x40
 afe:	c3                   	retq   

0000000000000aff <pipe>:
SYSCALL(pipe)
 aff:	b8 04 00 00 00       	mov    $0x4,%eax
 b04:	cd 40                	int    $0x40
 b06:	c3                   	retq   

0000000000000b07 <read>:
SYSCALL(read)
 b07:	b8 05 00 00 00       	mov    $0x5,%eax
 b0c:	cd 40                	int    $0x40
 b0e:	c3                   	retq   

0000000000000b0f <write>:
SYSCALL(write)
 b0f:	b8 10 00 00 00       	mov    $0x10,%eax
 b14:	cd 40                	int    $0x40
 b16:	c3                   	retq   

0000000000000b17 <close>:
SYSCALL(close)
 b17:	b8 15 00 00 00       	mov    $0x15,%eax
 b1c:	cd 40                	int    $0x40
 b1e:	c3                   	retq   

0000000000000b1f <kill>:
SYSCALL(kill)
 b1f:	b8 06 00 00 00       	mov    $0x6,%eax
 b24:	cd 40                	int    $0x40
 b26:	c3                   	retq   

0000000000000b27 <exec>:
SYSCALL(exec)
 b27:	b8 07 00 00 00       	mov    $0x7,%eax
 b2c:	cd 40                	int    $0x40
 b2e:	c3                   	retq   

0000000000000b2f <open>:
SYSCALL(open)
 b2f:	b8 0f 00 00 00       	mov    $0xf,%eax
 b34:	cd 40                	int    $0x40
 b36:	c3                   	retq   

0000000000000b37 <mknod>:
SYSCALL(mknod)
 b37:	b8 11 00 00 00       	mov    $0x11,%eax
 b3c:	cd 40                	int    $0x40
 b3e:	c3                   	retq   

0000000000000b3f <unlink>:
SYSCALL(unlink)
 b3f:	b8 12 00 00 00       	mov    $0x12,%eax
 b44:	cd 40                	int    $0x40
 b46:	c3                   	retq   

0000000000000b47 <fstat>:
SYSCALL(fstat)
 b47:	b8 08 00 00 00       	mov    $0x8,%eax
 b4c:	cd 40                	int    $0x40
 b4e:	c3                   	retq   

0000000000000b4f <link>:
SYSCALL(link)
 b4f:	b8 13 00 00 00       	mov    $0x13,%eax
 b54:	cd 40                	int    $0x40
 b56:	c3                   	retq   

0000000000000b57 <mkdir>:
SYSCALL(mkdir)
 b57:	b8 14 00 00 00       	mov    $0x14,%eax
 b5c:	cd 40                	int    $0x40
 b5e:	c3                   	retq   

0000000000000b5f <chdir>:
SYSCALL(chdir)
 b5f:	b8 09 00 00 00       	mov    $0x9,%eax
 b64:	cd 40                	int    $0x40
 b66:	c3                   	retq   

0000000000000b67 <dup>:
SYSCALL(dup)
 b67:	b8 0a 00 00 00       	mov    $0xa,%eax
 b6c:	cd 40                	int    $0x40
 b6e:	c3                   	retq   

0000000000000b6f <getpid>:
SYSCALL(getpid)
 b6f:	b8 0b 00 00 00       	mov    $0xb,%eax
 b74:	cd 40                	int    $0x40
 b76:	c3                   	retq   

0000000000000b77 <sbrk>:
SYSCALL(sbrk)
 b77:	b8 0c 00 00 00       	mov    $0xc,%eax
 b7c:	cd 40                	int    $0x40
 b7e:	c3                   	retq   

0000000000000b7f <sleep>:
SYSCALL(sleep)
 b7f:	b8 0d 00 00 00       	mov    $0xd,%eax
 b84:	cd 40                	int    $0x40
 b86:	c3                   	retq   

0000000000000b87 <uptime>:
SYSCALL(uptime)
 b87:	b8 0e 00 00 00       	mov    $0xe,%eax
 b8c:	cd 40                	int    $0x40
 b8e:	c3                   	retq   

0000000000000b8f <sysinfo>:
SYSCALL(sysinfo)
 b8f:	b8 16 00 00 00       	mov    $0x16,%eax
 b94:	cd 40                	int    $0x40
 b96:	c3                   	retq   

0000000000000b97 <crashn>:
SYSCALL(crashn)
 b97:	b8 17 00 00 00       	mov    $0x17,%eax
 b9c:	cd 40                	int    $0x40
 b9e:	c3                   	retq   

0000000000000b9f <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 b9f:	55                   	push   %rbp
 ba0:	48 89 e5             	mov    %rsp,%rbp
 ba3:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 ba7:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 bab:	48 83 e8 10          	sub    $0x10,%rax
 baf:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 bb3:	48 8b 05 e6 05 00 00 	mov    0x5e6(%rip),%rax        # 11a0 <freep>
 bba:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 bbe:	eb 2f                	jmp    bef <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 bc0:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bc4:	48 8b 00             	mov    (%rax),%rax
 bc7:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 bcb:	77 17                	ja     be4 <free+0x45>
 bcd:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 bd1:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 bd5:	77 2f                	ja     c06 <free+0x67>
 bd7:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bdb:	48 8b 00             	mov    (%rax),%rax
 bde:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 be2:	77 22                	ja     c06 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 be4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 be8:	48 8b 00             	mov    (%rax),%rax
 beb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 bef:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 bf3:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 bf7:	76 c7                	jbe    bc0 <free+0x21>
 bf9:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bfd:	48 8b 00             	mov    (%rax),%rax
 c00:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 c04:	76 ba                	jbe    bc0 <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 c06:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c0a:	8b 40 08             	mov    0x8(%rax),%eax
 c0d:	89 c0                	mov    %eax,%eax
 c0f:	48 c1 e0 04          	shl    $0x4,%rax
 c13:	48 89 c2             	mov    %rax,%rdx
 c16:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c1a:	48 01 c2             	add    %rax,%rdx
 c1d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c21:	48 8b 00             	mov    (%rax),%rax
 c24:	48 39 c2             	cmp    %rax,%rdx
 c27:	75 2d                	jne    c56 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 c29:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c2d:	8b 50 08             	mov    0x8(%rax),%edx
 c30:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c34:	48 8b 00             	mov    (%rax),%rax
 c37:	8b 40 08             	mov    0x8(%rax),%eax
 c3a:	01 c2                	add    %eax,%edx
 c3c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c40:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 c43:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c47:	48 8b 00             	mov    (%rax),%rax
 c4a:	48 8b 10             	mov    (%rax),%rdx
 c4d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c51:	48 89 10             	mov    %rdx,(%rax)
 c54:	eb 0e                	jmp    c64 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 c56:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c5a:	48 8b 10             	mov    (%rax),%rdx
 c5d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c61:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 c64:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c68:	8b 40 08             	mov    0x8(%rax),%eax
 c6b:	89 c0                	mov    %eax,%eax
 c6d:	48 c1 e0 04          	shl    $0x4,%rax
 c71:	48 89 c2             	mov    %rax,%rdx
 c74:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c78:	48 01 d0             	add    %rdx,%rax
 c7b:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 c7f:	75 27                	jne    ca8 <free+0x109>
    p->s.size += bp->s.size;
 c81:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c85:	8b 50 08             	mov    0x8(%rax),%edx
 c88:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c8c:	8b 40 08             	mov    0x8(%rax),%eax
 c8f:	01 c2                	add    %eax,%edx
 c91:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 c95:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 c98:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 c9c:	48 8b 10             	mov    (%rax),%rdx
 c9f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 ca3:	48 89 10             	mov    %rdx,(%rax)
 ca6:	eb 0b                	jmp    cb3 <free+0x114>
  } else
    p->s.ptr = bp;
 ca8:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 cac:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 cb0:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 cb3:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 cb7:	48 89 05 e2 04 00 00 	mov    %rax,0x4e2(%rip)        # 11a0 <freep>
}
 cbe:	90                   	nop
 cbf:	5d                   	pop    %rbp
 cc0:	c3                   	retq   

0000000000000cc1 <morecore>:

static Header *morecore(uint nu) {
 cc1:	55                   	push   %rbp
 cc2:	48 89 e5             	mov    %rsp,%rbp
 cc5:	48 83 ec 20          	sub    $0x20,%rsp
 cc9:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 ccc:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 cd3:	77 07                	ja     cdc <morecore+0x1b>
    nu = 4096;
 cd5:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 cdc:	8b 45 ec             	mov    -0x14(%rbp),%eax
 cdf:	c1 e0 04             	shl    $0x4,%eax
 ce2:	89 c7                	mov    %eax,%edi
 ce4:	e8 8e fe ff ff       	callq  b77 <sbrk>
 ce9:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 ced:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 cf2:	75 07                	jne    cfb <morecore+0x3a>
    return 0;
 cf4:	b8 00 00 00 00       	mov    $0x0,%eax
 cf9:	eb 29                	jmp    d24 <morecore+0x63>
  hp = (Header *)p;
 cfb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 cff:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 d03:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d07:	8b 55 ec             	mov    -0x14(%rbp),%edx
 d0a:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 d0d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d11:	48 83 c0 10          	add    $0x10,%rax
 d15:	48 89 c7             	mov    %rax,%rdi
 d18:	e8 82 fe ff ff       	callq  b9f <free>
  return freep;
 d1d:	48 8b 05 7c 04 00 00 	mov    0x47c(%rip),%rax        # 11a0 <freep>
}
 d24:	c9                   	leaveq 
 d25:	c3                   	retq   

0000000000000d26 <malloc>:

void *malloc(uint nbytes) {
 d26:	55                   	push   %rbp
 d27:	48 89 e5             	mov    %rsp,%rbp
 d2a:	48 83 ec 30          	sub    $0x30,%rsp
 d2e:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 d31:	8b 45 dc             	mov    -0x24(%rbp),%eax
 d34:	48 83 c0 0f          	add    $0xf,%rax
 d38:	48 c1 e8 04          	shr    $0x4,%rax
 d3c:	83 c0 01             	add    $0x1,%eax
 d3f:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 d42:	48 8b 05 57 04 00 00 	mov    0x457(%rip),%rax        # 11a0 <freep>
 d49:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 d4d:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 d52:	75 2b                	jne    d7f <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 d54:	48 c7 45 f0 90 11 00 	movq   $0x1190,-0x10(%rbp)
 d5b:	00 
 d5c:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d60:	48 89 05 39 04 00 00 	mov    %rax,0x439(%rip)        # 11a0 <freep>
 d67:	48 8b 05 32 04 00 00 	mov    0x432(%rip),%rax        # 11a0 <freep>
 d6e:	48 89 05 1b 04 00 00 	mov    %rax,0x41b(%rip)        # 1190 <base>
    base.s.size = 0;
 d75:	c7 05 19 04 00 00 00 	movl   $0x0,0x419(%rip)        # 1198 <base+0x8>
 d7c:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 d7f:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 d83:	48 8b 00             	mov    (%rax),%rax
 d86:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 d8a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 d8e:	8b 40 08             	mov    0x8(%rax),%eax
 d91:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 d94:	72 5f                	jb     df5 <malloc+0xcf>
      if (p->s.size == nunits)
 d96:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 d9a:	8b 40 08             	mov    0x8(%rax),%eax
 d9d:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 da0:	75 10                	jne    db2 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 da2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 da6:	48 8b 10             	mov    (%rax),%rdx
 da9:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 dad:	48 89 10             	mov    %rdx,(%rax)
 db0:	eb 2e                	jmp    de0 <malloc+0xba>
      else {
        p->s.size -= nunits;
 db2:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 db6:	8b 40 08             	mov    0x8(%rax),%eax
 db9:	2b 45 ec             	sub    -0x14(%rbp),%eax
 dbc:	89 c2                	mov    %eax,%edx
 dbe:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 dc2:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 dc5:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 dc9:	8b 40 08             	mov    0x8(%rax),%eax
 dcc:	89 c0                	mov    %eax,%eax
 dce:	48 c1 e0 04          	shl    $0x4,%rax
 dd2:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 dd6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 dda:	8b 55 ec             	mov    -0x14(%rbp),%edx
 ddd:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 de0:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 de4:	48 89 05 b5 03 00 00 	mov    %rax,0x3b5(%rip)        # 11a0 <freep>
      return (void *)(p + 1);
 deb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 def:	48 83 c0 10          	add    $0x10,%rax
 df3:	eb 41                	jmp    e36 <malloc+0x110>
    }
    if (p == freep)
 df5:	48 8b 05 a4 03 00 00 	mov    0x3a4(%rip),%rax        # 11a0 <freep>
 dfc:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 e00:	75 1c                	jne    e1e <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 e02:	8b 45 ec             	mov    -0x14(%rbp),%eax
 e05:	89 c7                	mov    %eax,%edi
 e07:	e8 b5 fe ff ff       	callq  cc1 <morecore>
 e0c:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 e10:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 e15:	75 07                	jne    e1e <malloc+0xf8>
        return 0;
 e17:	b8 00 00 00 00       	mov    $0x0,%eax
 e1c:	eb 18                	jmp    e36 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 e1e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 e22:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 e26:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 e2a:	48 8b 00             	mov    (%rax),%rax
 e2d:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 e31:	e9 54 ff ff ff       	jmpq   d8a <malloc+0x64>
 e36:	c9                   	leaveq 
 e37:	c3                   	retq   
