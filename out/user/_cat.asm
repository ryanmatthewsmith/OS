
out/user/_cat:     file format elf64-x86-64


Disassembly of section .text:

0000000000000000 <cat>:
#include <stat.h>
#include <user.h>

char buf[512];

void cat(int fd) {
   0:	55                   	push   %rbp
   1:	48 89 e5             	mov    %rsp,%rbp
   4:	48 83 ec 20          	sub    $0x20,%rsp
   8:	89 7d ec             	mov    %edi,-0x14(%rbp)
  int n;

  while ((n = read(fd, buf, sizeof(buf))) > 0) {
   b:	eb 32                	jmp    3f <cat+0x3f>
    if (write(1, buf, n) != n) {
   d:	8b 45 fc             	mov    -0x4(%rbp),%eax
  10:	89 c2                	mov    %eax,%edx
  12:	be 00 0f 00 00       	mov    $0xf00,%esi
  17:	bf 01 00 00 00       	mov    $0x1,%edi
  1c:	e8 8c 08 00 00       	callq  8ad <write>
  21:	3b 45 fc             	cmp    -0x4(%rbp),%eax
  24:	74 19                	je     3f <cat+0x3f>
      printf(1, "cat: write error\n");
  26:	be d6 0b 00 00       	mov    $0xbd6,%esi
  2b:	bf 01 00 00 00       	mov    $0x1,%edi
  30:	b8 00 00 00 00       	mov    $0x0,%eax
  35:	e8 b0 02 00 00       	callq  2ea <printf>
      exit();
  3a:	e8 4e 08 00 00       	callq  88d <exit>
char buf[512];

void cat(int fd) {
  int n;

  while ((n = read(fd, buf, sizeof(buf))) > 0) {
  3f:	8b 45 ec             	mov    -0x14(%rbp),%eax
  42:	ba 00 02 00 00       	mov    $0x200,%edx
  47:	be 00 0f 00 00       	mov    $0xf00,%esi
  4c:	89 c7                	mov    %eax,%edi
  4e:	e8 52 08 00 00       	callq  8a5 <read>
  53:	89 45 fc             	mov    %eax,-0x4(%rbp)
  56:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
  5a:	7f b1                	jg     d <cat+0xd>
    if (write(1, buf, n) != n) {
      printf(1, "cat: write error\n");
      exit();
    }
  }
  if (n < 0) {
  5c:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
  60:	79 19                	jns    7b <cat+0x7b>
    printf(1, "cat: read error\n");
  62:	be e8 0b 00 00       	mov    $0xbe8,%esi
  67:	bf 01 00 00 00       	mov    $0x1,%edi
  6c:	b8 00 00 00 00       	mov    $0x0,%eax
  71:	e8 74 02 00 00       	callq  2ea <printf>
    exit();
  76:	e8 12 08 00 00       	callq  88d <exit>
  }
}
  7b:	90                   	nop
  7c:	c9                   	leaveq 
  7d:	c3                   	retq   

000000000000007e <main>:

int main(int argc, char *argv[]) {
  7e:	55                   	push   %rbp
  7f:	48 89 e5             	mov    %rsp,%rbp
  82:	48 83 ec 20          	sub    $0x20,%rsp
  86:	89 7d ec             	mov    %edi,-0x14(%rbp)
  89:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd, i;

  if (argc <= 1) {
  8d:	83 7d ec 01          	cmpl   $0x1,-0x14(%rbp)
  91:	7f 0f                	jg     a2 <main+0x24>
    cat(0);
  93:	bf 00 00 00 00       	mov    $0x0,%edi
  98:	e8 63 ff ff ff       	callq  0 <cat>
    exit();
  9d:	e8 eb 07 00 00       	callq  88d <exit>
  }

  for (i = 1; i < argc; i++) {
  a2:	c7 45 fc 01 00 00 00 	movl   $0x1,-0x4(%rbp)
  a9:	eb 78                	jmp    123 <main+0xa5>
    if ((fd = open(argv[i], 0)) < 0) {
  ab:	8b 45 fc             	mov    -0x4(%rbp),%eax
  ae:	48 98                	cltq   
  b0:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
  b7:	00 
  b8:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  bc:	48 01 d0             	add    %rdx,%rax
  bf:	48 8b 00             	mov    (%rax),%rax
  c2:	be 00 00 00 00       	mov    $0x0,%esi
  c7:	48 89 c7             	mov    %rax,%rdi
  ca:	e8 fe 07 00 00       	callq  8cd <open>
  cf:	89 45 f8             	mov    %eax,-0x8(%rbp)
  d2:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
  d6:	79 33                	jns    10b <main+0x8d>
      printf(1, "cat: cannot open %s\n", argv[i]);
  d8:	8b 45 fc             	mov    -0x4(%rbp),%eax
  db:	48 98                	cltq   
  dd:	48 8d 14 c5 00 00 00 	lea    0x0(,%rax,8),%rdx
  e4:	00 
  e5:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
  e9:	48 01 d0             	add    %rdx,%rax
  ec:	48 8b 00             	mov    (%rax),%rax
  ef:	48 89 c2             	mov    %rax,%rdx
  f2:	be f9 0b 00 00       	mov    $0xbf9,%esi
  f7:	bf 01 00 00 00       	mov    $0x1,%edi
  fc:	b8 00 00 00 00       	mov    $0x0,%eax
 101:	e8 e4 01 00 00       	callq  2ea <printf>
      exit();
 106:	e8 82 07 00 00       	callq  88d <exit>
    }
    cat(fd);
 10b:	8b 45 f8             	mov    -0x8(%rbp),%eax
 10e:	89 c7                	mov    %eax,%edi
 110:	e8 eb fe ff ff       	callq  0 <cat>
    close(fd);
 115:	8b 45 f8             	mov    -0x8(%rbp),%eax
 118:	89 c7                	mov    %eax,%edi
 11a:	e8 96 07 00 00       	callq  8b5 <close>
  if (argc <= 1) {
    cat(0);
    exit();
  }

  for (i = 1; i < argc; i++) {
 11f:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 123:	8b 45 fc             	mov    -0x4(%rbp),%eax
 126:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 129:	7c 80                	jl     ab <main+0x2d>
      exit();
    }
    cat(fd);
    close(fd);
  }
  exit();
 12b:	e8 5d 07 00 00       	callq  88d <exit>

0000000000000130 <putc>:
#include <cdefs.h>
#include <stat.h>
#include <stdarg.h>
#include <user.h>

static void putc(int fd, char c) { write(fd, &c, 1); }
 130:	55                   	push   %rbp
 131:	48 89 e5             	mov    %rsp,%rbp
 134:	48 83 ec 10          	sub    $0x10,%rsp
 138:	89 7d fc             	mov    %edi,-0x4(%rbp)
 13b:	89 f0                	mov    %esi,%eax
 13d:	88 45 f8             	mov    %al,-0x8(%rbp)
 140:	48 8d 4d f8          	lea    -0x8(%rbp),%rcx
 144:	8b 45 fc             	mov    -0x4(%rbp),%eax
 147:	ba 01 00 00 00       	mov    $0x1,%edx
 14c:	48 89 ce             	mov    %rcx,%rsi
 14f:	89 c7                	mov    %eax,%edi
 151:	e8 57 07 00 00       	callq  8ad <write>
 156:	90                   	nop
 157:	c9                   	leaveq 
 158:	c3                   	retq   

0000000000000159 <printint64>:

static void printint64(int fd, int xx, int base, int sgn) {
 159:	55                   	push   %rbp
 15a:	48 89 e5             	mov    %rsp,%rbp
 15d:	48 83 ec 40          	sub    $0x40,%rsp
 161:	89 7d cc             	mov    %edi,-0x34(%rbp)
 164:	89 75 c8             	mov    %esi,-0x38(%rbp)
 167:	89 55 c4             	mov    %edx,-0x3c(%rbp)
 16a:	89 4d c0             	mov    %ecx,-0x40(%rbp)
  static char digits[] = "0123456789abcdef";
  char buf[32];
  int i;
  uint64_t x;

  if (sgn && (sgn = xx < 0))
 16d:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 171:	74 1f                	je     192 <printint64+0x39>
 173:	8b 45 c8             	mov    -0x38(%rbp),%eax
 176:	c1 e8 1f             	shr    $0x1f,%eax
 179:	0f b6 c0             	movzbl %al,%eax
 17c:	89 45 c0             	mov    %eax,-0x40(%rbp)
 17f:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 183:	74 0d                	je     192 <printint64+0x39>
    x = -xx;
 185:	8b 45 c8             	mov    -0x38(%rbp),%eax
 188:	f7 d8                	neg    %eax
 18a:	48 98                	cltq   
 18c:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 190:	eb 09                	jmp    19b <printint64+0x42>
  else
    x = xx;
 192:	8b 45 c8             	mov    -0x38(%rbp),%eax
 195:	48 98                	cltq   
 197:	48 89 45 f0          	mov    %rax,-0x10(%rbp)

  i = 0;
 19b:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 1a2:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 1a5:	8d 41 01             	lea    0x1(%rcx),%eax
 1a8:	89 45 fc             	mov    %eax,-0x4(%rbp)
 1ab:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 1ae:	48 63 f0             	movslq %eax,%rsi
 1b1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 1b5:	ba 00 00 00 00       	mov    $0x0,%edx
 1ba:	48 f7 f6             	div    %rsi
 1bd:	48 89 d0             	mov    %rdx,%rax
 1c0:	0f b6 90 90 0e 00 00 	movzbl 0xe90(%rax),%edx
 1c7:	48 63 c1             	movslq %ecx,%rax
 1ca:	88 54 05 d0          	mov    %dl,-0x30(%rbp,%rax,1)
  } while ((x /= base) != 0);
 1ce:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 1d1:	48 63 f8             	movslq %eax,%rdi
 1d4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 1d8:	ba 00 00 00 00       	mov    $0x0,%edx
 1dd:	48 f7 f7             	div    %rdi
 1e0:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 1e4:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 1e9:	75 b7                	jne    1a2 <printint64+0x49>

  if (sgn)
 1eb:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 1ef:	74 2b                	je     21c <printint64+0xc3>
    buf[i++] = '-';
 1f1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 1f4:	8d 50 01             	lea    0x1(%rax),%edx
 1f7:	89 55 fc             	mov    %edx,-0x4(%rbp)
 1fa:	48 98                	cltq   
 1fc:	c6 44 05 d0 2d       	movb   $0x2d,-0x30(%rbp,%rax,1)

  while (--i >= 0)
 201:	eb 19                	jmp    21c <printint64+0xc3>
    putc(fd, buf[i]);
 203:	8b 45 fc             	mov    -0x4(%rbp),%eax
 206:	48 98                	cltq   
 208:	0f b6 44 05 d0       	movzbl -0x30(%rbp,%rax,1),%eax
 20d:	0f be d0             	movsbl %al,%edx
 210:	8b 45 cc             	mov    -0x34(%rbp),%eax
 213:	89 d6                	mov    %edx,%esi
 215:	89 c7                	mov    %eax,%edi
 217:	e8 14 ff ff ff       	callq  130 <putc>
  } while ((x /= base) != 0);

  if (sgn)
    buf[i++] = '-';

  while (--i >= 0)
 21c:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 220:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 224:	79 dd                	jns    203 <printint64+0xaa>
    putc(fd, buf[i]);
}
 226:	90                   	nop
 227:	c9                   	leaveq 
 228:	c3                   	retq   

0000000000000229 <printint>:

static void printint(int fd, int xx, int base, int sgn) {
 229:	55                   	push   %rbp
 22a:	48 89 e5             	mov    %rsp,%rbp
 22d:	48 83 ec 30          	sub    $0x30,%rsp
 231:	89 7d dc             	mov    %edi,-0x24(%rbp)
 234:	89 75 d8             	mov    %esi,-0x28(%rbp)
 237:	89 55 d4             	mov    %edx,-0x2c(%rbp)
 23a:	89 4d d0             	mov    %ecx,-0x30(%rbp)
  static char digits[] = "0123456789ABCDEF";
  char buf[16];
  int i, neg;
  uint x;

  neg = 0;
 23d:	c7 45 f8 00 00 00 00 	movl   $0x0,-0x8(%rbp)
  if (sgn && xx < 0) {
 244:	83 7d d0 00          	cmpl   $0x0,-0x30(%rbp)
 248:	74 17                	je     261 <printint+0x38>
 24a:	83 7d d8 00          	cmpl   $0x0,-0x28(%rbp)
 24e:	79 11                	jns    261 <printint+0x38>
    neg = 1;
 250:	c7 45 f8 01 00 00 00 	movl   $0x1,-0x8(%rbp)
    x = -xx;
 257:	8b 45 d8             	mov    -0x28(%rbp),%eax
 25a:	f7 d8                	neg    %eax
 25c:	89 45 f4             	mov    %eax,-0xc(%rbp)
 25f:	eb 06                	jmp    267 <printint+0x3e>
  } else {
    x = xx;
 261:	8b 45 d8             	mov    -0x28(%rbp),%eax
 264:	89 45 f4             	mov    %eax,-0xc(%rbp)
  }

  i = 0;
 267:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  do {
    buf[i++] = digits[x % base];
 26e:	8b 4d fc             	mov    -0x4(%rbp),%ecx
 271:	8d 41 01             	lea    0x1(%rcx),%eax
 274:	89 45 fc             	mov    %eax,-0x4(%rbp)
 277:	8b 75 d4             	mov    -0x2c(%rbp),%esi
 27a:	8b 45 f4             	mov    -0xc(%rbp),%eax
 27d:	ba 00 00 00 00       	mov    $0x0,%edx
 282:	f7 f6                	div    %esi
 284:	89 d0                	mov    %edx,%eax
 286:	89 c0                	mov    %eax,%eax
 288:	0f b6 90 b0 0e 00 00 	movzbl 0xeb0(%rax),%edx
 28f:	48 63 c1             	movslq %ecx,%rax
 292:	88 54 05 e4          	mov    %dl,-0x1c(%rbp,%rax,1)
  } while ((x /= base) != 0);
 296:	8b 7d d4             	mov    -0x2c(%rbp),%edi
 299:	8b 45 f4             	mov    -0xc(%rbp),%eax
 29c:	ba 00 00 00 00       	mov    $0x0,%edx
 2a1:	f7 f7                	div    %edi
 2a3:	89 45 f4             	mov    %eax,-0xc(%rbp)
 2a6:	83 7d f4 00          	cmpl   $0x0,-0xc(%rbp)
 2aa:	75 c2                	jne    26e <printint+0x45>
  if (neg)
 2ac:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 2b0:	74 2b                	je     2dd <printint+0xb4>
    buf[i++] = '-';
 2b2:	8b 45 fc             	mov    -0x4(%rbp),%eax
 2b5:	8d 50 01             	lea    0x1(%rax),%edx
 2b8:	89 55 fc             	mov    %edx,-0x4(%rbp)
 2bb:	48 98                	cltq   
 2bd:	c6 44 05 e4 2d       	movb   $0x2d,-0x1c(%rbp,%rax,1)

  while (--i >= 0)
 2c2:	eb 19                	jmp    2dd <printint+0xb4>
    putc(fd, buf[i]);
 2c4:	8b 45 fc             	mov    -0x4(%rbp),%eax
 2c7:	48 98                	cltq   
 2c9:	0f b6 44 05 e4       	movzbl -0x1c(%rbp,%rax,1),%eax
 2ce:	0f be d0             	movsbl %al,%edx
 2d1:	8b 45 dc             	mov    -0x24(%rbp),%eax
 2d4:	89 d6                	mov    %edx,%esi
 2d6:	89 c7                	mov    %eax,%edi
 2d8:	e8 53 fe ff ff       	callq  130 <putc>
    buf[i++] = digits[x % base];
  } while ((x /= base) != 0);
  if (neg)
    buf[i++] = '-';

  while (--i >= 0)
 2dd:	83 6d fc 01          	subl   $0x1,-0x4(%rbp)
 2e1:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 2e5:	79 dd                	jns    2c4 <printint+0x9b>
    putc(fd, buf[i]);
}
 2e7:	90                   	nop
 2e8:	c9                   	leaveq 
 2e9:	c3                   	retq   

00000000000002ea <printf>:

// Print to the given fd. Only understands %d, %x, %p, %s.
void printf(int fd, char *fmt, ...) {
 2ea:	55                   	push   %rbp
 2eb:	48 89 e5             	mov    %rsp,%rbp
 2ee:	48 83 ec 70          	sub    $0x70,%rsp
 2f2:	89 7d 9c             	mov    %edi,-0x64(%rbp)
 2f5:	48 89 75 90          	mov    %rsi,-0x70(%rbp)
 2f9:	48 89 55 e0          	mov    %rdx,-0x20(%rbp)
 2fd:	48 89 4d e8          	mov    %rcx,-0x18(%rbp)
 301:	4c 89 45 f0          	mov    %r8,-0x10(%rbp)
 305:	4c 89 4d f8          	mov    %r9,-0x8(%rbp)
  char *s;
  int c, i, state;
  int lflag;
  va_list valist;
  va_start(valist, fmt);
 309:	c7 45 a0 10 00 00 00 	movl   $0x10,-0x60(%rbp)
 310:	48 8d 45 10          	lea    0x10(%rbp),%rax
 314:	48 89 45 a8          	mov    %rax,-0x58(%rbp)
 318:	48 8d 45 d0          	lea    -0x30(%rbp),%rax
 31c:	48 89 45 b0          	mov    %rax,-0x50(%rbp)

  state = 0;
 320:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  for (i = 0; fmt[i]; i++) {
 327:	c7 45 c4 00 00 00 00 	movl   $0x0,-0x3c(%rbp)
 32e:	e9 68 02 00 00       	jmpq   59b <printf+0x2b1>
    c = fmt[i] & 0xff;
 333:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 336:	48 63 d0             	movslq %eax,%rdx
 339:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 33d:	48 01 d0             	add    %rdx,%rax
 340:	0f b6 00             	movzbl (%rax),%eax
 343:	0f be c0             	movsbl %al,%eax
 346:	25 ff 00 00 00       	and    $0xff,%eax
 34b:	89 45 b8             	mov    %eax,-0x48(%rbp)
    if (state == 0) {
 34e:	83 7d c0 00          	cmpl   $0x0,-0x40(%rbp)
 352:	75 30                	jne    384 <printf+0x9a>
      if (c == '%') {
 354:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 358:	75 13                	jne    36d <printf+0x83>
        state = '%';
 35a:	c7 45 c0 25 00 00 00 	movl   $0x25,-0x40(%rbp)
        lflag = 0;
 361:	c7 45 bc 00 00 00 00 	movl   $0x0,-0x44(%rbp)
 368:	e9 2a 02 00 00       	jmpq   597 <printf+0x2ad>
      } else {
        putc(fd, c);
 36d:	8b 45 b8             	mov    -0x48(%rbp),%eax
 370:	0f be d0             	movsbl %al,%edx
 373:	8b 45 9c             	mov    -0x64(%rbp),%eax
 376:	89 d6                	mov    %edx,%esi
 378:	89 c7                	mov    %eax,%edi
 37a:	e8 b1 fd ff ff       	callq  130 <putc>
 37f:	e9 13 02 00 00       	jmpq   597 <printf+0x2ad>
      }
    } else if (state == '%') {
 384:	83 7d c0 25          	cmpl   $0x25,-0x40(%rbp)
 388:	0f 85 09 02 00 00    	jne    597 <printf+0x2ad>
      if (c == 'l') {
 38e:	83 7d b8 6c          	cmpl   $0x6c,-0x48(%rbp)
 392:	75 0c                	jne    3a0 <printf+0xb6>
        lflag = 1;
 394:	c7 45 bc 01 00 00 00 	movl   $0x1,-0x44(%rbp)
        continue;
 39b:	e9 f7 01 00 00       	jmpq   597 <printf+0x2ad>
      } else if (c == 'd') {
 3a0:	83 7d b8 64          	cmpl   $0x64,-0x48(%rbp)
 3a4:	0f 85 95 00 00 00    	jne    43f <printf+0x155>
        if (lflag == 1)
 3aa:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 3ae:	75 49                	jne    3f9 <printf+0x10f>
          printint64(fd, va_arg(valist, int64_t), 10, 1);
 3b0:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3b3:	83 f8 30             	cmp    $0x30,%eax
 3b6:	73 17                	jae    3cf <printf+0xe5>
 3b8:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 3bc:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3bf:	89 d2                	mov    %edx,%edx
 3c1:	48 01 d0             	add    %rdx,%rax
 3c4:	8b 55 a0             	mov    -0x60(%rbp),%edx
 3c7:	83 c2 08             	add    $0x8,%edx
 3ca:	89 55 a0             	mov    %edx,-0x60(%rbp)
 3cd:	eb 0c                	jmp    3db <printf+0xf1>
 3cf:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 3d3:	48 8d 50 08          	lea    0x8(%rax),%rdx
 3d7:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 3db:	48 8b 00             	mov    (%rax),%rax
 3de:	89 c6                	mov    %eax,%esi
 3e0:	8b 45 9c             	mov    -0x64(%rbp),%eax
 3e3:	b9 01 00 00 00       	mov    $0x1,%ecx
 3e8:	ba 0a 00 00 00       	mov    $0xa,%edx
 3ed:	89 c7                	mov    %eax,%edi
 3ef:	e8 65 fd ff ff       	callq  159 <printint64>
 3f4:	e9 97 01 00 00       	jmpq   590 <printf+0x2a6>
        else
          printint(fd, va_arg(valist, int), 10, 1);
 3f9:	8b 45 a0             	mov    -0x60(%rbp),%eax
 3fc:	83 f8 30             	cmp    $0x30,%eax
 3ff:	73 17                	jae    418 <printf+0x12e>
 401:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 405:	8b 55 a0             	mov    -0x60(%rbp),%edx
 408:	89 d2                	mov    %edx,%edx
 40a:	48 01 d0             	add    %rdx,%rax
 40d:	8b 55 a0             	mov    -0x60(%rbp),%edx
 410:	83 c2 08             	add    $0x8,%edx
 413:	89 55 a0             	mov    %edx,-0x60(%rbp)
 416:	eb 0c                	jmp    424 <printf+0x13a>
 418:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 41c:	48 8d 50 08          	lea    0x8(%rax),%rdx
 420:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 424:	8b 30                	mov    (%rax),%esi
 426:	8b 45 9c             	mov    -0x64(%rbp),%eax
 429:	b9 01 00 00 00       	mov    $0x1,%ecx
 42e:	ba 0a 00 00 00       	mov    $0xa,%edx
 433:	89 c7                	mov    %eax,%edi
 435:	e8 ef fd ff ff       	callq  229 <printint>
 43a:	e9 51 01 00 00       	jmpq   590 <printf+0x2a6>
      } else if (c == 'x' || c == 'p') {
 43f:	83 7d b8 78          	cmpl   $0x78,-0x48(%rbp)
 443:	74 0a                	je     44f <printf+0x165>
 445:	83 7d b8 70          	cmpl   $0x70,-0x48(%rbp)
 449:	0f 85 95 00 00 00    	jne    4e4 <printf+0x1fa>
        if (lflag == 1)
 44f:	83 7d bc 01          	cmpl   $0x1,-0x44(%rbp)
 453:	75 49                	jne    49e <printf+0x1b4>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
 455:	8b 45 a0             	mov    -0x60(%rbp),%eax
 458:	83 f8 30             	cmp    $0x30,%eax
 45b:	73 17                	jae    474 <printf+0x18a>
 45d:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 461:	8b 55 a0             	mov    -0x60(%rbp),%edx
 464:	89 d2                	mov    %edx,%edx
 466:	48 01 d0             	add    %rdx,%rax
 469:	8b 55 a0             	mov    -0x60(%rbp),%edx
 46c:	83 c2 08             	add    $0x8,%edx
 46f:	89 55 a0             	mov    %edx,-0x60(%rbp)
 472:	eb 0c                	jmp    480 <printf+0x196>
 474:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 478:	48 8d 50 08          	lea    0x8(%rax),%rdx
 47c:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 480:	48 8b 00             	mov    (%rax),%rax
 483:	89 c6                	mov    %eax,%esi
 485:	8b 45 9c             	mov    -0x64(%rbp),%eax
 488:	b9 00 00 00 00       	mov    $0x0,%ecx
 48d:	ba 10 00 00 00       	mov    $0x10,%edx
 492:	89 c7                	mov    %eax,%edi
 494:	e8 c0 fc ff ff       	callq  159 <printint64>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 499:	e9 f2 00 00 00       	jmpq   590 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
 49e:	8b 45 a0             	mov    -0x60(%rbp),%eax
 4a1:	83 f8 30             	cmp    $0x30,%eax
 4a4:	73 17                	jae    4bd <printf+0x1d3>
 4a6:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 4aa:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4ad:	89 d2                	mov    %edx,%edx
 4af:	48 01 d0             	add    %rdx,%rax
 4b2:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4b5:	83 c2 08             	add    $0x8,%edx
 4b8:	89 55 a0             	mov    %edx,-0x60(%rbp)
 4bb:	eb 0c                	jmp    4c9 <printf+0x1df>
 4bd:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 4c1:	48 8d 50 08          	lea    0x8(%rax),%rdx
 4c5:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 4c9:	8b 30                	mov    (%rax),%esi
 4cb:	8b 45 9c             	mov    -0x64(%rbp),%eax
 4ce:	b9 00 00 00 00       	mov    $0x0,%ecx
 4d3:	ba 10 00 00 00       	mov    $0x10,%edx
 4d8:	89 c7                	mov    %eax,%edi
 4da:	e8 4a fd ff ff       	callq  229 <printint>
        if (lflag == 1)
          printint64(fd, va_arg(valist, int64_t), 10, 1);
        else
          printint(fd, va_arg(valist, int), 10, 1);
      } else if (c == 'x' || c == 'p') {
        if (lflag == 1)
 4df:	e9 ac 00 00 00       	jmpq   590 <printf+0x2a6>
          printint64(fd, va_arg(valist, int64_t), 16, 0);
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
 4e4:	83 7d b8 73          	cmpl   $0x73,-0x48(%rbp)
 4e8:	75 6b                	jne    555 <printf+0x26b>
        if ((s = (char *)va_arg(valist, char *)) == 0)
 4ea:	8b 45 a0             	mov    -0x60(%rbp),%eax
 4ed:	83 f8 30             	cmp    $0x30,%eax
 4f0:	73 17                	jae    509 <printf+0x21f>
 4f2:	48 8b 45 b0          	mov    -0x50(%rbp),%rax
 4f6:	8b 55 a0             	mov    -0x60(%rbp),%edx
 4f9:	89 d2                	mov    %edx,%edx
 4fb:	48 01 d0             	add    %rdx,%rax
 4fe:	8b 55 a0             	mov    -0x60(%rbp),%edx
 501:	83 c2 08             	add    $0x8,%edx
 504:	89 55 a0             	mov    %edx,-0x60(%rbp)
 507:	eb 0c                	jmp    515 <printf+0x22b>
 509:	48 8b 45 a8          	mov    -0x58(%rbp),%rax
 50d:	48 8d 50 08          	lea    0x8(%rax),%rdx
 511:	48 89 55 a8          	mov    %rdx,-0x58(%rbp)
 515:	48 8b 00             	mov    (%rax),%rax
 518:	48 89 45 c8          	mov    %rax,-0x38(%rbp)
 51c:	48 83 7d c8 00       	cmpq   $0x0,-0x38(%rbp)
 521:	75 25                	jne    548 <printf+0x25e>
          s = "(null)";
 523:	48 c7 45 c8 0e 0c 00 	movq   $0xc0e,-0x38(%rbp)
 52a:	00 
        for (; *s; s++)
 52b:	eb 1b                	jmp    548 <printf+0x25e>
          putc(fd, *s);
 52d:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 531:	0f b6 00             	movzbl (%rax),%eax
 534:	0f be d0             	movsbl %al,%edx
 537:	8b 45 9c             	mov    -0x64(%rbp),%eax
 53a:	89 d6                	mov    %edx,%esi
 53c:	89 c7                	mov    %eax,%edi
 53e:	e8 ed fb ff ff       	callq  130 <putc>
        else
          printint(fd, va_arg(valist, int), 16, 0);
      } else if (c == 's') {
        if ((s = (char *)va_arg(valist, char *)) == 0)
          s = "(null)";
        for (; *s; s++)
 543:	48 83 45 c8 01       	addq   $0x1,-0x38(%rbp)
 548:	48 8b 45 c8          	mov    -0x38(%rbp),%rax
 54c:	0f b6 00             	movzbl (%rax),%eax
 54f:	84 c0                	test   %al,%al
 551:	75 da                	jne    52d <printf+0x243>
 553:	eb 3b                	jmp    590 <printf+0x2a6>
          putc(fd, *s);
      } else if (c == '%') {
 555:	83 7d b8 25          	cmpl   $0x25,-0x48(%rbp)
 559:	75 14                	jne    56f <printf+0x285>
        putc(fd, c);
 55b:	8b 45 b8             	mov    -0x48(%rbp),%eax
 55e:	0f be d0             	movsbl %al,%edx
 561:	8b 45 9c             	mov    -0x64(%rbp),%eax
 564:	89 d6                	mov    %edx,%esi
 566:	89 c7                	mov    %eax,%edi
 568:	e8 c3 fb ff ff       	callq  130 <putc>
 56d:	eb 21                	jmp    590 <printf+0x2a6>
      } else {
        // Unknown % sequence.  Print it to draw attention.
        putc(fd, '%');
 56f:	8b 45 9c             	mov    -0x64(%rbp),%eax
 572:	be 25 00 00 00       	mov    $0x25,%esi
 577:	89 c7                	mov    %eax,%edi
 579:	e8 b2 fb ff ff       	callq  130 <putc>
        putc(fd, c);
 57e:	8b 45 b8             	mov    -0x48(%rbp),%eax
 581:	0f be d0             	movsbl %al,%edx
 584:	8b 45 9c             	mov    -0x64(%rbp),%eax
 587:	89 d6                	mov    %edx,%esi
 589:	89 c7                	mov    %eax,%edi
 58b:	e8 a0 fb ff ff       	callq  130 <putc>
      }
      state = 0;
 590:	c7 45 c0 00 00 00 00 	movl   $0x0,-0x40(%rbp)
  int lflag;
  va_list valist;
  va_start(valist, fmt);

  state = 0;
  for (i = 0; fmt[i]; i++) {
 597:	83 45 c4 01          	addl   $0x1,-0x3c(%rbp)
 59b:	8b 45 c4             	mov    -0x3c(%rbp),%eax
 59e:	48 63 d0             	movslq %eax,%rdx
 5a1:	48 8b 45 90          	mov    -0x70(%rbp),%rax
 5a5:	48 01 d0             	add    %rdx,%rax
 5a8:	0f b6 00             	movzbl (%rax),%eax
 5ab:	84 c0                	test   %al,%al
 5ad:	0f 85 80 fd ff ff    	jne    333 <printf+0x49>
      state = 0;
    }
  }

  va_end(valist);
}
 5b3:	90                   	nop
 5b4:	c9                   	leaveq 
 5b5:	c3                   	retq   

00000000000005b6 <stosb>:
               : "=S"(addr), "=c"(cnt)
               : "d"(port), "0"(addr), "1"(cnt)
               : "cc");
}

static inline void stosb(void *addr, int data, int cnt) {
 5b6:	55                   	push   %rbp
 5b7:	48 89 e5             	mov    %rsp,%rbp
 5ba:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 5be:	89 75 f4             	mov    %esi,-0xc(%rbp)
 5c1:	89 55 f0             	mov    %edx,-0x10(%rbp)
  asm volatile("cld; rep stosb"
 5c4:	48 8b 4d f8          	mov    -0x8(%rbp),%rcx
 5c8:	8b 55 f0             	mov    -0x10(%rbp),%edx
 5cb:	8b 45 f4             	mov    -0xc(%rbp),%eax
 5ce:	48 89 ce             	mov    %rcx,%rsi
 5d1:	48 89 f7             	mov    %rsi,%rdi
 5d4:	89 d1                	mov    %edx,%ecx
 5d6:	fc                   	cld    
 5d7:	f3 aa                	rep stos %al,%es:(%rdi)
 5d9:	89 ca                	mov    %ecx,%edx
 5db:	48 89 fe             	mov    %rdi,%rsi
 5de:	48 89 75 f8          	mov    %rsi,-0x8(%rbp)
 5e2:	89 55 f0             	mov    %edx,-0x10(%rbp)
               : "=D"(addr), "=c"(cnt)
               : "0"(addr), "1"(cnt), "a"(data)
               : "memory", "cc");
}
 5e5:	90                   	nop
 5e6:	5d                   	pop    %rbp
 5e7:	c3                   	retq   

00000000000005e8 <strcpy>:
#include <fcntl.h>
#include <stat.h>
#include <user.h>
#include <x86_64.h>

char *strcpy(char *s, char *t) {
 5e8:	55                   	push   %rbp
 5e9:	48 89 e5             	mov    %rsp,%rbp
 5ec:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 5f0:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  char *os;

  os = s;
 5f4:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 5f8:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  while ((*s++ = *t++) != 0)
 5fc:	90                   	nop
 5fd:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 601:	48 8d 50 01          	lea    0x1(%rax),%rdx
 605:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 609:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 60d:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 611:	48 89 4d e0          	mov    %rcx,-0x20(%rbp)
 615:	0f b6 12             	movzbl (%rdx),%edx
 618:	88 10                	mov    %dl,(%rax)
 61a:	0f b6 00             	movzbl (%rax),%eax
 61d:	84 c0                	test   %al,%al
 61f:	75 dc                	jne    5fd <strcpy+0x15>
    ;
  return os;
 621:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 625:	5d                   	pop    %rbp
 626:	c3                   	retq   

0000000000000627 <strcmp>:

int strcmp(const char *p, const char *q) {
 627:	55                   	push   %rbp
 628:	48 89 e5             	mov    %rsp,%rbp
 62b:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 62f:	48 89 75 f0          	mov    %rsi,-0x10(%rbp)
  while (*p && *p == *q)
 633:	eb 0a                	jmp    63f <strcmp+0x18>
    p++, q++;
 635:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 63a:	48 83 45 f0 01       	addq   $0x1,-0x10(%rbp)
    ;
  return os;
}

int strcmp(const char *p, const char *q) {
  while (*p && *p == *q)
 63f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 643:	0f b6 00             	movzbl (%rax),%eax
 646:	84 c0                	test   %al,%al
 648:	74 12                	je     65c <strcmp+0x35>
 64a:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 64e:	0f b6 10             	movzbl (%rax),%edx
 651:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 655:	0f b6 00             	movzbl (%rax),%eax
 658:	38 c2                	cmp    %al,%dl
 65a:	74 d9                	je     635 <strcmp+0xe>
    p++, q++;
  return (uchar)*p - (uchar)*q;
 65c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 660:	0f b6 00             	movzbl (%rax),%eax
 663:	0f b6 d0             	movzbl %al,%edx
 666:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 66a:	0f b6 00             	movzbl (%rax),%eax
 66d:	0f b6 c0             	movzbl %al,%eax
 670:	29 c2                	sub    %eax,%edx
 672:	89 d0                	mov    %edx,%eax
}
 674:	5d                   	pop    %rbp
 675:	c3                   	retq   

0000000000000676 <strlen>:

uint strlen(char *s) {
 676:	55                   	push   %rbp
 677:	48 89 e5             	mov    %rsp,%rbp
 67a:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  for (n = 0; s[n]; n++)
 67e:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 685:	eb 04                	jmp    68b <strlen+0x15>
 687:	83 45 fc 01          	addl   $0x1,-0x4(%rbp)
 68b:	8b 45 fc             	mov    -0x4(%rbp),%eax
 68e:	48 63 d0             	movslq %eax,%rdx
 691:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 695:	48 01 d0             	add    %rdx,%rax
 698:	0f b6 00             	movzbl (%rax),%eax
 69b:	84 c0                	test   %al,%al
 69d:	75 e8                	jne    687 <strlen+0x11>
    ;
  return n;
 69f:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 6a2:	5d                   	pop    %rbp
 6a3:	c3                   	retq   

00000000000006a4 <memset>:

void *memset(void *dst, int c, uint n) {
 6a4:	55                   	push   %rbp
 6a5:	48 89 e5             	mov    %rsp,%rbp
 6a8:	48 83 ec 10          	sub    $0x10,%rsp
 6ac:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 6b0:	89 75 f4             	mov    %esi,-0xc(%rbp)
 6b3:	89 55 f0             	mov    %edx,-0x10(%rbp)
  stosb(dst, c, n);
 6b6:	8b 55 f0             	mov    -0x10(%rbp),%edx
 6b9:	8b 4d f4             	mov    -0xc(%rbp),%ecx
 6bc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6c0:	89 ce                	mov    %ecx,%esi
 6c2:	48 89 c7             	mov    %rax,%rdi
 6c5:	e8 ec fe ff ff       	callq  5b6 <stosb>
  return dst;
 6ca:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
}
 6ce:	c9                   	leaveq 
 6cf:	c3                   	retq   

00000000000006d0 <strchr>:

char *strchr(const char *s, char c) {
 6d0:	55                   	push   %rbp
 6d1:	48 89 e5             	mov    %rsp,%rbp
 6d4:	48 89 7d f8          	mov    %rdi,-0x8(%rbp)
 6d8:	89 f0                	mov    %esi,%eax
 6da:	88 45 f4             	mov    %al,-0xc(%rbp)
  for (; *s; s++)
 6dd:	eb 17                	jmp    6f6 <strchr+0x26>
    if (*s == c)
 6df:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6e3:	0f b6 00             	movzbl (%rax),%eax
 6e6:	3a 45 f4             	cmp    -0xc(%rbp),%al
 6e9:	75 06                	jne    6f1 <strchr+0x21>
      return (char *)s;
 6eb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6ef:	eb 15                	jmp    706 <strchr+0x36>
  stosb(dst, c, n);
  return dst;
}

char *strchr(const char *s, char c) {
  for (; *s; s++)
 6f1:	48 83 45 f8 01       	addq   $0x1,-0x8(%rbp)
 6f6:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 6fa:	0f b6 00             	movzbl (%rax),%eax
 6fd:	84 c0                	test   %al,%al
 6ff:	75 de                	jne    6df <strchr+0xf>
    if (*s == c)
      return (char *)s;
  return 0;
 701:	b8 00 00 00 00       	mov    $0x0,%eax
}
 706:	5d                   	pop    %rbp
 707:	c3                   	retq   

0000000000000708 <gets>:

char *gets(char *buf, int max) {
 708:	55                   	push   %rbp
 709:	48 89 e5             	mov    %rsp,%rbp
 70c:	48 83 ec 20          	sub    $0x20,%rsp
 710:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 714:	89 75 e4             	mov    %esi,-0x1c(%rbp)
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 717:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
 71e:	eb 48                	jmp    768 <gets+0x60>
    cc = read(0, &c, 1);
 720:	48 8d 45 f7          	lea    -0x9(%rbp),%rax
 724:	ba 01 00 00 00       	mov    $0x1,%edx
 729:	48 89 c6             	mov    %rax,%rsi
 72c:	bf 00 00 00 00       	mov    $0x0,%edi
 731:	e8 6f 01 00 00       	callq  8a5 <read>
 736:	89 45 f8             	mov    %eax,-0x8(%rbp)
    if (cc < 1)
 739:	83 7d f8 00          	cmpl   $0x0,-0x8(%rbp)
 73d:	7e 36                	jle    775 <gets+0x6d>
      break;
    buf[i++] = c;
 73f:	8b 45 fc             	mov    -0x4(%rbp),%eax
 742:	8d 50 01             	lea    0x1(%rax),%edx
 745:	89 55 fc             	mov    %edx,-0x4(%rbp)
 748:	48 63 d0             	movslq %eax,%rdx
 74b:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 74f:	48 01 c2             	add    %rax,%rdx
 752:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 756:	88 02                	mov    %al,(%rdx)
    if (c == '\n' || c == '\r')
 758:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 75c:	3c 0a                	cmp    $0xa,%al
 75e:	74 16                	je     776 <gets+0x6e>
 760:	0f b6 45 f7          	movzbl -0x9(%rbp),%eax
 764:	3c 0d                	cmp    $0xd,%al
 766:	74 0e                	je     776 <gets+0x6e>

char *gets(char *buf, int max) {
  int i, cc;
  char c;

  for (i = 0; i + 1 < max;) {
 768:	8b 45 fc             	mov    -0x4(%rbp),%eax
 76b:	83 c0 01             	add    $0x1,%eax
 76e:	3b 45 e4             	cmp    -0x1c(%rbp),%eax
 771:	7c ad                	jl     720 <gets+0x18>
 773:	eb 01                	jmp    776 <gets+0x6e>
    cc = read(0, &c, 1);
    if (cc < 1)
      break;
 775:	90                   	nop
    buf[i++] = c;
    if (c == '\n' || c == '\r')
      break;
  }
  buf[i] = '\0';
 776:	8b 45 fc             	mov    -0x4(%rbp),%eax
 779:	48 63 d0             	movslq %eax,%rdx
 77c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 780:	48 01 d0             	add    %rdx,%rax
 783:	c6 00 00             	movb   $0x0,(%rax)
  return buf;
 786:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
}
 78a:	c9                   	leaveq 
 78b:	c3                   	retq   

000000000000078c <stat>:

int stat(char *n, struct stat *st) {
 78c:	55                   	push   %rbp
 78d:	48 89 e5             	mov    %rsp,%rbp
 790:	48 83 ec 20          	sub    $0x20,%rsp
 794:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 798:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
  int fd;
  int r;

  fd = open(n, O_RDONLY);
 79c:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 7a0:	be 00 00 00 00       	mov    $0x0,%esi
 7a5:	48 89 c7             	mov    %rax,%rdi
 7a8:	e8 20 01 00 00       	callq  8cd <open>
 7ad:	89 45 fc             	mov    %eax,-0x4(%rbp)
  if (fd < 0)
 7b0:	83 7d fc 00          	cmpl   $0x0,-0x4(%rbp)
 7b4:	79 07                	jns    7bd <stat+0x31>
    return -1;
 7b6:	b8 ff ff ff ff       	mov    $0xffffffff,%eax
 7bb:	eb 21                	jmp    7de <stat+0x52>
  r = fstat(fd, st);
 7bd:	48 8b 55 e0          	mov    -0x20(%rbp),%rdx
 7c1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 7c4:	48 89 d6             	mov    %rdx,%rsi
 7c7:	89 c7                	mov    %eax,%edi
 7c9:	e8 17 01 00 00       	callq  8e5 <fstat>
 7ce:	89 45 f8             	mov    %eax,-0x8(%rbp)
  close(fd);
 7d1:	8b 45 fc             	mov    -0x4(%rbp),%eax
 7d4:	89 c7                	mov    %eax,%edi
 7d6:	e8 da 00 00 00       	callq  8b5 <close>
  return r;
 7db:	8b 45 f8             	mov    -0x8(%rbp),%eax
}
 7de:	c9                   	leaveq 
 7df:	c3                   	retq   

00000000000007e0 <atoi>:

int atoi(const char *s) {
 7e0:	55                   	push   %rbp
 7e1:	48 89 e5             	mov    %rsp,%rbp
 7e4:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  int n;

  n = 0;
 7e8:	c7 45 fc 00 00 00 00 	movl   $0x0,-0x4(%rbp)
  while ('0' <= *s && *s <= '9')
 7ef:	eb 28                	jmp    819 <atoi+0x39>
    n = n * 10 + *s++ - '0';
 7f1:	8b 55 fc             	mov    -0x4(%rbp),%edx
 7f4:	89 d0                	mov    %edx,%eax
 7f6:	c1 e0 02             	shl    $0x2,%eax
 7f9:	01 d0                	add    %edx,%eax
 7fb:	01 c0                	add    %eax,%eax
 7fd:	89 c1                	mov    %eax,%ecx
 7ff:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 803:	48 8d 50 01          	lea    0x1(%rax),%rdx
 807:	48 89 55 e8          	mov    %rdx,-0x18(%rbp)
 80b:	0f b6 00             	movzbl (%rax),%eax
 80e:	0f be c0             	movsbl %al,%eax
 811:	01 c8                	add    %ecx,%eax
 813:	83 e8 30             	sub    $0x30,%eax
 816:	89 45 fc             	mov    %eax,-0x4(%rbp)

int atoi(const char *s) {
  int n;

  n = 0;
  while ('0' <= *s && *s <= '9')
 819:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 81d:	0f b6 00             	movzbl (%rax),%eax
 820:	3c 2f                	cmp    $0x2f,%al
 822:	7e 0b                	jle    82f <atoi+0x4f>
 824:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 828:	0f b6 00             	movzbl (%rax),%eax
 82b:	3c 39                	cmp    $0x39,%al
 82d:	7e c2                	jle    7f1 <atoi+0x11>
    n = n * 10 + *s++ - '0';
  return n;
 82f:	8b 45 fc             	mov    -0x4(%rbp),%eax
}
 832:	5d                   	pop    %rbp
 833:	c3                   	retq   

0000000000000834 <memmove>:

void *memmove(void *vdst, void *vsrc, int n) {
 834:	55                   	push   %rbp
 835:	48 89 e5             	mov    %rsp,%rbp
 838:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
 83c:	48 89 75 e0          	mov    %rsi,-0x20(%rbp)
 840:	89 55 dc             	mov    %edx,-0x24(%rbp)
  char *dst, *src;

  dst = vdst;
 843:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 847:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  src = vsrc;
 84b:	48 8b 45 e0          	mov    -0x20(%rbp),%rax
 84f:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  while (n-- > 0)
 853:	eb 1d                	jmp    872 <memmove+0x3e>
    *dst++ = *src++;
 855:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 859:	48 8d 50 01          	lea    0x1(%rax),%rdx
 85d:	48 89 55 f8          	mov    %rdx,-0x8(%rbp)
 861:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 865:	48 8d 4a 01          	lea    0x1(%rdx),%rcx
 869:	48 89 4d f0          	mov    %rcx,-0x10(%rbp)
 86d:	0f b6 12             	movzbl (%rdx),%edx
 870:	88 10                	mov    %dl,(%rax)
void *memmove(void *vdst, void *vsrc, int n) {
  char *dst, *src;

  dst = vdst;
  src = vsrc;
  while (n-- > 0)
 872:	8b 45 dc             	mov    -0x24(%rbp),%eax
 875:	8d 50 ff             	lea    -0x1(%rax),%edx
 878:	89 55 dc             	mov    %edx,-0x24(%rbp)
 87b:	85 c0                	test   %eax,%eax
 87d:	7f d6                	jg     855 <memmove+0x21>
    *dst++ = *src++;
  return vdst;
 87f:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 883:	5d                   	pop    %rbp
 884:	c3                   	retq   

0000000000000885 <fork>:
  name:                                                                        \
  movl $SYS_##name, % eax;                                                     \
  int $TRAP_SYSCALL;                                                           \
  ret

SYSCALL(fork)
 885:	b8 01 00 00 00       	mov    $0x1,%eax
 88a:	cd 40                	int    $0x40
 88c:	c3                   	retq   

000000000000088d <exit>:
SYSCALL(exit)
 88d:	b8 02 00 00 00       	mov    $0x2,%eax
 892:	cd 40                	int    $0x40
 894:	c3                   	retq   

0000000000000895 <wait>:
SYSCALL(wait)
 895:	b8 03 00 00 00       	mov    $0x3,%eax
 89a:	cd 40                	int    $0x40
 89c:	c3                   	retq   

000000000000089d <pipe>:
SYSCALL(pipe)
 89d:	b8 04 00 00 00       	mov    $0x4,%eax
 8a2:	cd 40                	int    $0x40
 8a4:	c3                   	retq   

00000000000008a5 <read>:
SYSCALL(read)
 8a5:	b8 05 00 00 00       	mov    $0x5,%eax
 8aa:	cd 40                	int    $0x40
 8ac:	c3                   	retq   

00000000000008ad <write>:
SYSCALL(write)
 8ad:	b8 10 00 00 00       	mov    $0x10,%eax
 8b2:	cd 40                	int    $0x40
 8b4:	c3                   	retq   

00000000000008b5 <close>:
SYSCALL(close)
 8b5:	b8 15 00 00 00       	mov    $0x15,%eax
 8ba:	cd 40                	int    $0x40
 8bc:	c3                   	retq   

00000000000008bd <kill>:
SYSCALL(kill)
 8bd:	b8 06 00 00 00       	mov    $0x6,%eax
 8c2:	cd 40                	int    $0x40
 8c4:	c3                   	retq   

00000000000008c5 <exec>:
SYSCALL(exec)
 8c5:	b8 07 00 00 00       	mov    $0x7,%eax
 8ca:	cd 40                	int    $0x40
 8cc:	c3                   	retq   

00000000000008cd <open>:
SYSCALL(open)
 8cd:	b8 0f 00 00 00       	mov    $0xf,%eax
 8d2:	cd 40                	int    $0x40
 8d4:	c3                   	retq   

00000000000008d5 <mknod>:
SYSCALL(mknod)
 8d5:	b8 11 00 00 00       	mov    $0x11,%eax
 8da:	cd 40                	int    $0x40
 8dc:	c3                   	retq   

00000000000008dd <unlink>:
SYSCALL(unlink)
 8dd:	b8 12 00 00 00       	mov    $0x12,%eax
 8e2:	cd 40                	int    $0x40
 8e4:	c3                   	retq   

00000000000008e5 <fstat>:
SYSCALL(fstat)
 8e5:	b8 08 00 00 00       	mov    $0x8,%eax
 8ea:	cd 40                	int    $0x40
 8ec:	c3                   	retq   

00000000000008ed <link>:
SYSCALL(link)
 8ed:	b8 13 00 00 00       	mov    $0x13,%eax
 8f2:	cd 40                	int    $0x40
 8f4:	c3                   	retq   

00000000000008f5 <mkdir>:
SYSCALL(mkdir)
 8f5:	b8 14 00 00 00       	mov    $0x14,%eax
 8fa:	cd 40                	int    $0x40
 8fc:	c3                   	retq   

00000000000008fd <chdir>:
SYSCALL(chdir)
 8fd:	b8 09 00 00 00       	mov    $0x9,%eax
 902:	cd 40                	int    $0x40
 904:	c3                   	retq   

0000000000000905 <dup>:
SYSCALL(dup)
 905:	b8 0a 00 00 00       	mov    $0xa,%eax
 90a:	cd 40                	int    $0x40
 90c:	c3                   	retq   

000000000000090d <getpid>:
SYSCALL(getpid)
 90d:	b8 0b 00 00 00       	mov    $0xb,%eax
 912:	cd 40                	int    $0x40
 914:	c3                   	retq   

0000000000000915 <sbrk>:
SYSCALL(sbrk)
 915:	b8 0c 00 00 00       	mov    $0xc,%eax
 91a:	cd 40                	int    $0x40
 91c:	c3                   	retq   

000000000000091d <sleep>:
SYSCALL(sleep)
 91d:	b8 0d 00 00 00       	mov    $0xd,%eax
 922:	cd 40                	int    $0x40
 924:	c3                   	retq   

0000000000000925 <uptime>:
SYSCALL(uptime)
 925:	b8 0e 00 00 00       	mov    $0xe,%eax
 92a:	cd 40                	int    $0x40
 92c:	c3                   	retq   

000000000000092d <sysinfo>:
SYSCALL(sysinfo)
 92d:	b8 16 00 00 00       	mov    $0x16,%eax
 932:	cd 40                	int    $0x40
 934:	c3                   	retq   

0000000000000935 <crashn>:
SYSCALL(crashn)
 935:	b8 17 00 00 00       	mov    $0x17,%eax
 93a:	cd 40                	int    $0x40
 93c:	c3                   	retq   

000000000000093d <free>:
typedef union header Header;

static Header base;
static Header *freep;

void free(void *ap) {
 93d:	55                   	push   %rbp
 93e:	48 89 e5             	mov    %rsp,%rbp
 941:	48 89 7d e8          	mov    %rdi,-0x18(%rbp)
  Header *bp, *p;

  bp = (Header *)ap - 1;
 945:	48 8b 45 e8          	mov    -0x18(%rbp),%rax
 949:	48 83 e8 10          	sub    $0x10,%rax
 94d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 951:	48 8b 05 98 05 00 00 	mov    0x598(%rip),%rax        # ef0 <freep>
 958:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 95c:	eb 2f                	jmp    98d <free+0x50>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
 95e:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 962:	48 8b 00             	mov    (%rax),%rax
 965:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 969:	77 17                	ja     982 <free+0x45>
 96b:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 96f:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 973:	77 2f                	ja     9a4 <free+0x67>
 975:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 979:	48 8b 00             	mov    (%rax),%rax
 97c:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 980:	77 22                	ja     9a4 <free+0x67>

void free(void *ap) {
  Header *bp, *p;

  bp = (Header *)ap - 1;
  for (p = freep; !(bp > p && bp < p->s.ptr); p = p->s.ptr)
 982:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 986:	48 8b 00             	mov    (%rax),%rax
 989:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 98d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 991:	48 3b 45 f8          	cmp    -0x8(%rbp),%rax
 995:	76 c7                	jbe    95e <free+0x21>
 997:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 99b:	48 8b 00             	mov    (%rax),%rax
 99e:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 9a2:	76 ba                	jbe    95e <free+0x21>
    if (p >= p->s.ptr && (bp > p || bp < p->s.ptr))
      break;
  if (bp + bp->s.size == p->s.ptr) {
 9a4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9a8:	8b 40 08             	mov    0x8(%rax),%eax
 9ab:	89 c0                	mov    %eax,%eax
 9ad:	48 c1 e0 04          	shl    $0x4,%rax
 9b1:	48 89 c2             	mov    %rax,%rdx
 9b4:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9b8:	48 01 c2             	add    %rax,%rdx
 9bb:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9bf:	48 8b 00             	mov    (%rax),%rax
 9c2:	48 39 c2             	cmp    %rax,%rdx
 9c5:	75 2d                	jne    9f4 <free+0xb7>
    bp->s.size += p->s.ptr->s.size;
 9c7:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9cb:	8b 50 08             	mov    0x8(%rax),%edx
 9ce:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9d2:	48 8b 00             	mov    (%rax),%rax
 9d5:	8b 40 08             	mov    0x8(%rax),%eax
 9d8:	01 c2                	add    %eax,%edx
 9da:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9de:	89 50 08             	mov    %edx,0x8(%rax)
    bp->s.ptr = p->s.ptr->s.ptr;
 9e1:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9e5:	48 8b 00             	mov    (%rax),%rax
 9e8:	48 8b 10             	mov    (%rax),%rdx
 9eb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ef:	48 89 10             	mov    %rdx,(%rax)
 9f2:	eb 0e                	jmp    a02 <free+0xc5>
  } else
    bp->s.ptr = p->s.ptr;
 9f4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 9f8:	48 8b 10             	mov    (%rax),%rdx
 9fb:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 9ff:	48 89 10             	mov    %rdx,(%rax)
  if (p + p->s.size == bp) {
 a02:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a06:	8b 40 08             	mov    0x8(%rax),%eax
 a09:	89 c0                	mov    %eax,%eax
 a0b:	48 c1 e0 04          	shl    $0x4,%rax
 a0f:	48 89 c2             	mov    %rax,%rdx
 a12:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a16:	48 01 d0             	add    %rdx,%rax
 a19:	48 3b 45 f0          	cmp    -0x10(%rbp),%rax
 a1d:	75 27                	jne    a46 <free+0x109>
    p->s.size += bp->s.size;
 a1f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a23:	8b 50 08             	mov    0x8(%rax),%edx
 a26:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a2a:	8b 40 08             	mov    0x8(%rax),%eax
 a2d:	01 c2                	add    %eax,%edx
 a2f:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a33:	89 50 08             	mov    %edx,0x8(%rax)
    p->s.ptr = bp->s.ptr;
 a36:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 a3a:	48 8b 10             	mov    (%rax),%rdx
 a3d:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a41:	48 89 10             	mov    %rdx,(%rax)
 a44:	eb 0b                	jmp    a51 <free+0x114>
  } else
    p->s.ptr = bp;
 a46:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a4a:	48 8b 55 f0          	mov    -0x10(%rbp),%rdx
 a4e:	48 89 10             	mov    %rdx,(%rax)
  freep = p;
 a51:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a55:	48 89 05 94 04 00 00 	mov    %rax,0x494(%rip)        # ef0 <freep>
}
 a5c:	90                   	nop
 a5d:	5d                   	pop    %rbp
 a5e:	c3                   	retq   

0000000000000a5f <morecore>:

static Header *morecore(uint nu) {
 a5f:	55                   	push   %rbp
 a60:	48 89 e5             	mov    %rsp,%rbp
 a63:	48 83 ec 20          	sub    $0x20,%rsp
 a67:	89 7d ec             	mov    %edi,-0x14(%rbp)
  char *p;
  Header *hp;

  if (nu < 4096)
 a6a:	81 7d ec ff 0f 00 00 	cmpl   $0xfff,-0x14(%rbp)
 a71:	77 07                	ja     a7a <morecore+0x1b>
    nu = 4096;
 a73:	c7 45 ec 00 10 00 00 	movl   $0x1000,-0x14(%rbp)
  p = sbrk(nu * sizeof(Header));
 a7a:	8b 45 ec             	mov    -0x14(%rbp),%eax
 a7d:	c1 e0 04             	shl    $0x4,%eax
 a80:	89 c7                	mov    %eax,%edi
 a82:	e8 8e fe ff ff       	callq  915 <sbrk>
 a87:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
  if (p == (char *)-1)
 a8b:	48 83 7d f8 ff       	cmpq   $0xffffffffffffffff,-0x8(%rbp)
 a90:	75 07                	jne    a99 <morecore+0x3a>
    return 0;
 a92:	b8 00 00 00 00       	mov    $0x0,%eax
 a97:	eb 29                	jmp    ac2 <morecore+0x63>
  hp = (Header *)p;
 a99:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 a9d:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
  hp->s.size = nu;
 aa1:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 aa5:	8b 55 ec             	mov    -0x14(%rbp),%edx
 aa8:	89 50 08             	mov    %edx,0x8(%rax)
  free((void *)(hp + 1));
 aab:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 aaf:	48 83 c0 10          	add    $0x10,%rax
 ab3:	48 89 c7             	mov    %rax,%rdi
 ab6:	e8 82 fe ff ff       	callq  93d <free>
  return freep;
 abb:	48 8b 05 2e 04 00 00 	mov    0x42e(%rip),%rax        # ef0 <freep>
}
 ac2:	c9                   	leaveq 
 ac3:	c3                   	retq   

0000000000000ac4 <malloc>:

void *malloc(uint nbytes) {
 ac4:	55                   	push   %rbp
 ac5:	48 89 e5             	mov    %rsp,%rbp
 ac8:	48 83 ec 30          	sub    $0x30,%rsp
 acc:	89 7d dc             	mov    %edi,-0x24(%rbp)
  Header *p, *prevp;
  uint nunits;

  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
 acf:	8b 45 dc             	mov    -0x24(%rbp),%eax
 ad2:	48 83 c0 0f          	add    $0xf,%rax
 ad6:	48 c1 e8 04          	shr    $0x4,%rax
 ada:	83 c0 01             	add    $0x1,%eax
 add:	89 45 ec             	mov    %eax,-0x14(%rbp)
  if ((prevp = freep) == 0) {
 ae0:	48 8b 05 09 04 00 00 	mov    0x409(%rip),%rax        # ef0 <freep>
 ae7:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 aeb:	48 83 7d f0 00       	cmpq   $0x0,-0x10(%rbp)
 af0:	75 2b                	jne    b1d <malloc+0x59>
    base.s.ptr = freep = prevp = &base;
 af2:	48 c7 45 f0 e0 0e 00 	movq   $0xee0,-0x10(%rbp)
 af9:	00 
 afa:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 afe:	48 89 05 eb 03 00 00 	mov    %rax,0x3eb(%rip)        # ef0 <freep>
 b05:	48 8b 05 e4 03 00 00 	mov    0x3e4(%rip),%rax        # ef0 <freep>
 b0c:	48 89 05 cd 03 00 00 	mov    %rax,0x3cd(%rip)        # ee0 <base>
    base.s.size = 0;
 b13:	c7 05 cb 03 00 00 00 	movl   $0x0,0x3cb(%rip)        # ee8 <base+0x8>
 b1a:	00 00 00 
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 b1d:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b21:	48 8b 00             	mov    (%rax),%rax
 b24:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
    if (p->s.size >= nunits) {
 b28:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b2c:	8b 40 08             	mov    0x8(%rax),%eax
 b2f:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 b32:	72 5f                	jb     b93 <malloc+0xcf>
      if (p->s.size == nunits)
 b34:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b38:	8b 40 08             	mov    0x8(%rax),%eax
 b3b:	3b 45 ec             	cmp    -0x14(%rbp),%eax
 b3e:	75 10                	jne    b50 <malloc+0x8c>
        prevp->s.ptr = p->s.ptr;
 b40:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b44:	48 8b 10             	mov    (%rax),%rdx
 b47:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b4b:	48 89 10             	mov    %rdx,(%rax)
 b4e:	eb 2e                	jmp    b7e <malloc+0xba>
      else {
        p->s.size -= nunits;
 b50:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b54:	8b 40 08             	mov    0x8(%rax),%eax
 b57:	2b 45 ec             	sub    -0x14(%rbp),%eax
 b5a:	89 c2                	mov    %eax,%edx
 b5c:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b60:	89 50 08             	mov    %edx,0x8(%rax)
        p += p->s.size;
 b63:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b67:	8b 40 08             	mov    0x8(%rax),%eax
 b6a:	89 c0                	mov    %eax,%eax
 b6c:	48 c1 e0 04          	shl    $0x4,%rax
 b70:	48 01 45 f8          	add    %rax,-0x8(%rbp)
        p->s.size = nunits;
 b74:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b78:	8b 55 ec             	mov    -0x14(%rbp),%edx
 b7b:	89 50 08             	mov    %edx,0x8(%rax)
      }
      freep = prevp;
 b7e:	48 8b 45 f0          	mov    -0x10(%rbp),%rax
 b82:	48 89 05 67 03 00 00 	mov    %rax,0x367(%rip)        # ef0 <freep>
      return (void *)(p + 1);
 b89:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 b8d:	48 83 c0 10          	add    $0x10,%rax
 b91:	eb 41                	jmp    bd4 <malloc+0x110>
    }
    if (p == freep)
 b93:	48 8b 05 56 03 00 00 	mov    0x356(%rip),%rax        # ef0 <freep>
 b9a:	48 39 45 f8          	cmp    %rax,-0x8(%rbp)
 b9e:	75 1c                	jne    bbc <malloc+0xf8>
      if ((p = morecore(nunits)) == 0)
 ba0:	8b 45 ec             	mov    -0x14(%rbp),%eax
 ba3:	89 c7                	mov    %eax,%edi
 ba5:	e8 b5 fe ff ff       	callq  a5f <morecore>
 baa:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
 bae:	48 83 7d f8 00       	cmpq   $0x0,-0x8(%rbp)
 bb3:	75 07                	jne    bbc <malloc+0xf8>
        return 0;
 bb5:	b8 00 00 00 00       	mov    $0x0,%eax
 bba:	eb 18                	jmp    bd4 <malloc+0x110>
  nunits = (nbytes + sizeof(Header) - 1) / sizeof(Header) + 1;
  if ((prevp = freep) == 0) {
    base.s.ptr = freep = prevp = &base;
    base.s.size = 0;
  }
  for (p = prevp->s.ptr;; prevp = p, p = p->s.ptr) {
 bbc:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bc0:	48 89 45 f0          	mov    %rax,-0x10(%rbp)
 bc4:	48 8b 45 f8          	mov    -0x8(%rbp),%rax
 bc8:	48 8b 00             	mov    (%rax),%rax
 bcb:	48 89 45 f8          	mov    %rax,-0x8(%rbp)
      return (void *)(p + 1);
    }
    if (p == freep)
      if ((p = morecore(nunits)) == 0)
        return 0;
  }
 bcf:	e9 54 ff ff ff       	jmpq   b28 <malloc+0x64>
 bd4:	c9                   	leaveq 
 bd5:	c3                   	retq   
