
out/bootblock.o:     file format elf32-i386


Disassembly of section .text:

00007c00 <start>:
    7c00:	fa                   	cli    
    7c01:	31 c0                	xor    %eax,%eax
    7c03:	8e d8                	mov    %eax,%ds
    7c05:	8e c0                	mov    %eax,%es
    7c07:	8e d0                	mov    %eax,%ss

00007c09 <seta20.1>:
    7c09:	e4 64                	in     $0x64,%al
    7c0b:	a8 02                	test   $0x2,%al
    7c0d:	75 fa                	jne    7c09 <seta20.1>
    7c0f:	b0 d1                	mov    $0xd1,%al
    7c11:	e6 64                	out    %al,$0x64

00007c13 <seta20.2>:
    7c13:	e4 64                	in     $0x64,%al
    7c15:	a8 02                	test   $0x2,%al
    7c17:	75 fa                	jne    7c13 <seta20.2>
    7c19:	b0 df                	mov    $0xdf,%al
    7c1b:	e6 60                	out    %al,$0x60

00007c1d <e820_start>:
    7c1d:	66 31 db             	xor    %bx,%bx
    7c20:	bf 00 90 66 c7       	mov    $0xc7669000,%edi

00007c23 <e820_loop>:
    7c23:	66 c7 05 14 00 00 00 	movw   $0xc783,0x14
    7c2a:	83 c7 
    7c2c:	04 66                	add    $0x66,%al
    7c2e:	ba 50 41 4d 53       	mov    $0x534d4150,%edx
    7c33:	66 b8 20 e8          	mov    $0xe820,%ax
    7c37:	00 00                	add    %al,(%eax)
    7c39:	b9 14 00 cd 15       	mov    $0x15cd0014,%ecx
    7c3e:	72 0d                	jb     7c4d <e820_end>
    7c40:	83 f9 14             	cmp    $0x14,%ecx
    7c43:	7f 03                	jg     7c48 <e820_skip>

00007c45 <e820_next>:
    7c45:	83 c7 14             	add    $0x14,%edi

00007c48 <e820_skip>:
    7c48:	66 85 db             	test   %bx,%bx
    7c4b:	75 d6                	jne    7c23 <e820_loop>

00007c4d <e820_end>:
    7c4d:	66 89 3e             	mov    %di,(%esi)
    7c50:	44                   	inc    %esp
    7c51:	7e 0f                	jle    7c62 <e820_end+0x15>
    7c53:	01 16                	add    %edx,(%esi)
    7c55:	ac                   	lods   %ds:(%esi),%al
    7c56:	7c 0f                	jl     7c67 <start32+0x1>
    7c58:	20 c0                	and    %al,%al
    7c5a:	66 83 c8 01          	or     $0x1,%ax
    7c5e:	0f 22 c0             	mov    %eax,%cr0
    7c61:	ea 66 7c 08 00 66 b8 	ljmp   $0xb866,$0x87c66

00007c66 <start32>:
    7c66:	66 b8 10 00          	mov    $0x10,%ax
    7c6a:	8e d8                	mov    %eax,%ds
    7c6c:	8e c0                	mov    %eax,%es
    7c6e:	8e d0                	mov    %eax,%ss
    7c70:	66 b8 00 00          	mov    $0x0,%ax
    7c74:	8e e0                	mov    %eax,%fs
    7c76:	8e e8                	mov    %eax,%gs
    7c78:	bc 00 7c 00 00       	mov    $0x7c00,%esp
    7c7d:	e8 9b 00 00 00       	call   7d1d <bootmain>
    7c82:	66 b8 00 8a          	mov    $0x8a00,%ax
    7c86:	66 89 c2             	mov    %ax,%dx
    7c89:	66 ef                	out    %ax,(%dx)
    7c8b:	66 b8 e0 8a          	mov    $0x8ae0,%ax
    7c8f:	66 ef                	out    %ax,(%dx)

00007c91 <spin>:
    7c91:	eb fe                	jmp    7c91 <spin>
    7c93:	90                   	nop

00007c94 <gdt>:
	...
    7c9c:	ff                   	(bad)  
    7c9d:	ff 00                	incl   (%eax)
    7c9f:	00 00                	add    %al,(%eax)
    7ca1:	9a cf 00 ff ff 00 00 	lcall  $0x0,$0xffff00cf
    7ca8:	00 92 cf 00 17 00    	add    %dl,0x1700cf(%edx)

00007cac <gdtdesc>:
    7cac:	17                   	pop    %ss
    7cad:	00 94 7c 00 00 55 ba 	add    %dl,-0x45ab0000(%esp,%edi,2)

00007cb2 <readsect>:
    7cb2:	55                   	push   %ebp
    7cb3:	ba f7 01 00 00       	mov    $0x1f7,%edx
    7cb8:	89 e5                	mov    %esp,%ebp
    7cba:	57                   	push   %edi
    7cbb:	8b 4d 0c             	mov    0xc(%ebp),%ecx
    7cbe:	ec                   	in     (%dx),%al
    7cbf:	83 e0 c0             	and    $0xffffffc0,%eax
    7cc2:	3c 40                	cmp    $0x40,%al
    7cc4:	75 f8                	jne    7cbe <readsect+0xc>
    7cc6:	ba f2 01 00 00       	mov    $0x1f2,%edx
    7ccb:	b0 01                	mov    $0x1,%al
    7ccd:	ee                   	out    %al,(%dx)
    7cce:	ba f3 01 00 00       	mov    $0x1f3,%edx
    7cd3:	88 c8                	mov    %cl,%al
    7cd5:	ee                   	out    %al,(%dx)
    7cd6:	89 c8                	mov    %ecx,%eax
    7cd8:	ba f4 01 00 00       	mov    $0x1f4,%edx
    7cdd:	c1 e8 08             	shr    $0x8,%eax
    7ce0:	ee                   	out    %al,(%dx)
    7ce1:	89 c8                	mov    %ecx,%eax
    7ce3:	ba f5 01 00 00       	mov    $0x1f5,%edx
    7ce8:	c1 e8 10             	shr    $0x10,%eax
    7ceb:	ee                   	out    %al,(%dx)
    7cec:	89 c8                	mov    %ecx,%eax
    7cee:	ba f6 01 00 00       	mov    $0x1f6,%edx
    7cf3:	c1 e8 18             	shr    $0x18,%eax
    7cf6:	83 c8 e0             	or     $0xffffffe0,%eax
    7cf9:	ee                   	out    %al,(%dx)
    7cfa:	ba f7 01 00 00       	mov    $0x1f7,%edx
    7cff:	b0 20                	mov    $0x20,%al
    7d01:	ee                   	out    %al,(%dx)
    7d02:	ec                   	in     (%dx),%al
    7d03:	83 e0 c0             	and    $0xffffffc0,%eax
    7d06:	3c 40                	cmp    $0x40,%al
    7d08:	75 f8                	jne    7d02 <readsect+0x50>
    7d0a:	8b 7d 08             	mov    0x8(%ebp),%edi
    7d0d:	b9 80 00 00 00       	mov    $0x80,%ecx
    7d12:	ba f0 01 00 00       	mov    $0x1f0,%edx
    7d17:	fc                   	cld    
    7d18:	f3 6d                	rep insl (%dx),%es:(%edi)
    7d1a:	5f                   	pop    %edi
    7d1b:	5d                   	pop    %ebp
    7d1c:	c3                   	ret    

00007d1d <bootmain>:
    7d1d:	55                   	push   %ebp
    7d1e:	89 e5                	mov    %esp,%ebp
    7d20:	57                   	push   %edi
    7d21:	56                   	push   %esi
    7d22:	53                   	push   %ebx
    7d23:	57                   	push   %edi
    7d24:	bb 01 00 00 00       	mov    $0x1,%ebx
    7d29:	8d 43 7f             	lea    0x7f(%ebx),%eax
    7d2c:	53                   	push   %ebx
    7d2d:	43                   	inc    %ebx
    7d2e:	c1 e0 09             	shl    $0x9,%eax
    7d31:	50                   	push   %eax
    7d32:	e8 7b ff ff ff       	call   7cb2 <readsect>
    7d37:	83 fb 11             	cmp    $0x11,%ebx
    7d3a:	59                   	pop    %ecx
    7d3b:	5e                   	pop    %esi
    7d3c:	75 eb                	jne    7d29 <bootmain+0xc>
    7d3e:	bb 00 00 01 00       	mov    $0x10000,%ebx
    7d43:	81 3b 02 b0 ad 1b    	cmpl   $0x1badb002,(%ebx)
    7d49:	8d 83 00 00 ff ff    	lea    -0x10000(%ebx),%eax
    7d4f:	74 0d                	je     7d5e <bootmain+0x41>
    7d51:	83 c3 04             	add    $0x4,%ebx
    7d54:	81 fb 00 20 01 00    	cmp    $0x12000,%ebx
    7d5a:	75 e7                	jne    7d43 <bootmain+0x26>
    7d5c:	eb 72                	jmp    7dd0 <bootmain+0xb3>
    7d5e:	8b 53 10             	mov    0x10(%ebx),%edx
    7d61:	89 d6                	mov    %edx,%esi
    7d63:	2b 73 0c             	sub    0xc(%ebx),%esi
    7d66:	01 c6                	add    %eax,%esi
    7d68:	8b 43 14             	mov    0x14(%ebx),%eax
    7d6b:	89 45 f0             	mov    %eax,-0x10(%ebp)
    7d6e:	89 f0                	mov    %esi,%eax
    7d70:	c1 ee 09             	shr    $0x9,%esi
    7d73:	25 ff 01 00 00       	and    $0x1ff,%eax
    7d78:	46                   	inc    %esi
    7d79:	29 c2                	sub    %eax,%edx
    7d7b:	89 d7                	mov    %edx,%edi
    7d7d:	39 7d f0             	cmp    %edi,-0x10(%ebp)
    7d80:	76 12                	jbe    7d94 <bootmain+0x77>
    7d82:	56                   	push   %esi
    7d83:	57                   	push   %edi
    7d84:	46                   	inc    %esi
    7d85:	81 c7 00 02 00 00    	add    $0x200,%edi
    7d8b:	e8 22 ff ff ff       	call   7cb2 <readsect>
    7d90:	58                   	pop    %eax
    7d91:	5a                   	pop    %edx
    7d92:	eb e9                	jmp    7d7d <bootmain+0x60>
    7d94:	8b 4b 18             	mov    0x18(%ebx),%ecx
    7d97:	8b 7b 14             	mov    0x14(%ebx),%edi
    7d9a:	39 f9                	cmp    %edi,%ecx
    7d9c:	76 07                	jbe    7da5 <bootmain+0x88>
    7d9e:	29 f9                	sub    %edi,%ecx
    7da0:	31 c0                	xor    %eax,%eax
    7da2:	fc                   	cld    
    7da3:	f3 aa                	rep stos %al,%es:(%edi)
    7da5:	8b 15 44 7e 00 00    	mov    0x7e44,%edx
    7dab:	8b 73 1c             	mov    0x1c(%ebx),%esi
    7dae:	89 d0                	mov    %edx,%eax
    7db0:	89 d1                	mov    %edx,%ecx
    7db2:	c7 02 40 00 00 00    	movl   $0x40,(%edx)
    7db8:	25 ff 0f 00 00       	and    $0xfff,%eax
    7dbd:	29 c1                	sub    %eax,%ecx
    7dbf:	89 42 2c             	mov    %eax,0x2c(%edx)
    7dc2:	89 4a 30             	mov    %ecx,0x30(%edx)
    7dc5:	b9 02 b0 ad 2b       	mov    $0x2badb002,%ecx
    7dca:	89 c8                	mov    %ecx,%eax
    7dcc:	89 d3                	mov    %edx,%ebx
    7dce:	56                   	push   %esi
    7dcf:	c3                   	ret    
    7dd0:	8d 65 f4             	lea    -0xc(%ebp),%esp
    7dd3:	5b                   	pop    %ebx
    7dd4:	5e                   	pop    %esi
    7dd5:	5f                   	pop    %edi
    7dd6:	5d                   	pop    %ebp
    7dd7:	c3                   	ret    
