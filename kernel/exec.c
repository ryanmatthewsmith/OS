#include <cdefs.h>
#include <defs.h>
#include <elf.h>
#include <memlayout.h>
#include <mmu.h>
#include <param.h>
#include <proc.h>
#include <trap.h>
#include <x86_64.h>

int exec(char *path, char **argv) {
  struct vspace vsp;
  uint64_t rip, rsp;
  uint64_t argc;

  //args + stack setup + load code
  if (vspaceinit(&vsp) < 0)
    return -1;
  
  if (vspaceloadcode(&vsp, path, &rip) == 0) {
    vspacefree(&vsp);
    return -1;
  }

  rsp = SZ_2G; 
  vspaceinitstack(&vsp, rsp);

  for(argc = 0; argc < MAXARG; argc++){
    if (argv[argc] == 0) break;
  }

  uint64_t mockPC = 0xCAFEBEEF;
  uint64_t stackBuild[argc+2];

  for (int i = argc + 1; i > 0; i--){
    int arglength = strlen(argv[i-1]) + 1;
    rsp -= arglength;
    vspacewritetova(&vsp, rsp, argv[i-1], arglength);
    stackBuild[i] = rsp;
  }

  stackBuild[0] = mockPC;
 
  rsp -= rsp % 8;

  rsp -=  sizeof(stackBuild);
  vspacewritetova(&vsp, rsp, stackBuild, sizeof(stackBuild));

  //update trapframe
  myproc()->tf->rsi = rsp+8;
  myproc()->tf->rsp = rsp;
  myproc()->tf->rip = rip;
  myproc()->tf->rdi = argc;
  
  struct vspace v = myproc()->vspace;

  myproc()->vspace = vsp;
  vspaceinvalidate(&myproc()->vspace);
  vspaceinstall(myproc());
  vspacefree(&v);

  return 0;
}
