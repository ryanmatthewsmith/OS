// File system implementation.  Five layers:
//   + Blocks: allocator for raw disk blocks.
//   + Files: inode allocator, reading, writing, metadata.
//   + Directories: inode with special contents (list of other inodes!)
//   + Names: paths like /usr/rtm/xk/fs.c for convenient naming.
//
// This file contains the low-level file system manipulation
// routines.  The (higher-level) system call implementations
// are in sysfile.c.

#include <cdefs.h>
#include <defs.h>
#include <file.h>
#include <fs.h>
#include <mmu.h>
#include <param.h>
#include <proc.h>
#include <sleeplock.h>
#include <spinlock.h>
#include <stat.h>
#include <memlayout.h>
#include <buf.h>

// there should be one superblock per disk device, but we run with
// only one device
struct superblock sb;
struct logdisk{
  int committed;
  int size;
  int blocks[50];
};
struct{
  uint startblock;
  uint blocksinuse;
  int blocks[50];
  struct sleeplock lock;
  struct logdisk ld;
} logmemory;

//global next free block, starts as the first free
uint nextfree;

// Read the super block.
void readsb(int dev, struct superblock *sb) {
  struct buf *bp;

  bp = bread(dev, 1);
  memmove(sb, bp->data, sizeof(*sb));
  brelse(bp);
}

// Inodes.
//
// An inode describes a single unnamed file.
// The inode disk structure holds metadata: the file's type,
// its size, the number of links referring to it, and the
// range of blocks holding the file's content.
//
// The inodes themselves are contained in a file known as the
// inodefile. This allows the number of inodes to grow dynamically
// appending to the end of the inode file. The inodefile has an
// inum of 1 and starts at sb.startinode.
//
// The kernel keeps a cache of in-use inodes in memory
// to provide a place for synchronizing access
// to inodes used by multiple processes. The cached
// inodes include book-keeping information that is
// not stored on disk: ip->ref and ip->flags.
//
// Since there is no writing to the file system there is no need
// for the callers to worry about coherence between the disk
// and the in memory copy, although that will become important
// if writing to the disk is introduced.
//
// Clients use iload() to populate an inode with valid information
// from the disk. idup() can be used to add an in memory reference
// to and inode. iput() will decrement the in memory reference count
// and will free the inode if there are no more references to it,
// freeing up space in the cache for the inode to be used again.

struct {
  struct spinlock lock;
  struct inode inode[NINODE];
  struct inode inodefile;
} icache;

// Find the inode file on the disk and load it into memory
// should only be called once, but is idempotent.
static void init_inodefile(int dev) {
  struct buf *b;
  struct dinode di;

  b = bread(dev, sb.inodestart);
  memmove(&di, b->data, sizeof(struct dinode));

  icache.inodefile.inum = INODEFILEINO;
  icache.inodefile.dev = dev;
  icache.inodefile.type = di.type;
  icache.inodefile.devid = di.devid;
  icache.inodefile.size = di.size;
  icache.inodefile.data = di.data;
  memmove(icache.inodefile.overflowBlocks, di.overflowBlocks, sizeof(di.overflowBlocks));

  brelse(b);

  initsleeplock(&logmemory.lock, "logmemory");
  initlog();
}

void iinit(int dev) {
  int i;

  initlock(&icache.lock, "icache");
  for (i = 0; i < NINODE; i++) {
    initsleeplock(&icache.inode[i].lock, "inode");
  }
  initsleeplock(&icache.inodefile.lock, "inodefile");

  readsb(dev, &sb);
  cprintf("sb: size %d nblocks %d swap start %d logstart %d bmap start %d inodestart %d\n", sb.size,
          sb.nblocks, sb.swapstart, sb.logstart, sb.bmapstart, sb.inodestart);

  init_inodefile(dev);

  nextfree = sb.extentfirstfree;
}

static void read_dinode(uint inum, struct dinode *dip) {
  readi(&icache.inodefile, (char *)dip, INODEOFF(inum), sizeof(*dip));
}

struct inode* get_inode_from_inum(int inum){
  return &icache.inode[inum];
}

void updateDinodeSize(int sizeAdded,int inum){
  struct dinode dp;
  struct dinode inodefile;
  uint i = INODEFILEINO;

  read_dinode(0, &inodefile);
  read_dinode(inum, &dp);

  int inodeblock = inodefile.data.startblkno + inum/8;
  int offset = (inum*64) % 512;

  dp.size += sizeAdded;

  //cprintf("blocknum: %d\n", inodeblock);
  struct buf *buf = bread(ROOTDEV, inodeblock);
  memmove(buf->data + offset, &dp, 64);
  logwrite(inodeblock, buf);
  //bwrite(buf);
  brelse(buf);
  logcommit();
  //cprintf("LBL blocknum: %d\n", inodeblock);
}



struct inode *ialloc(){
  struct dinode ifile_dinode;

  struct dinode newdinode;
  struct inode *inodefile;
  
  read_dinode(INODEFILEINO, &ifile_dinode);

  acquiresleep(&logmemory.lock);
  newdinode.devid = 1;
  newdinode.type = T_FILE;
  newdinode.size = 0;
  newdinode.data.startblkno = nextfree;
  nextfree += 15;
  newdinode.data.nblocks = 15;
  releasesleep(&logmemory.lock);

  inodefile = iget(ROOTDEV, INODEFILEINO);
  acquiresleep(&inodefile->lock);
  int size = inodefile->size;
  releasesleep(&inodefile->lock);
  writei(inodefile, &newdinode, size, 64);
  acquire(&icache.lock);
  icache.inodefile.size += 64;
  release(&icache.lock);

  return iget(ROOTDEV, size / 64);
}



// Find the inode with number inum on device dev
// and return the in-memory copy. Does not read
// the inode from from disk.
static struct inode *iget(uint dev, uint inum) {
  struct inode *ip, *empty;
  struct dinode dip;

  acquire(&icache.lock);

  // Is the inode already cached?
  empty = 0;
  for (ip = &icache.inode[0]; ip < &icache.inode[NINODE]; ip++) {
    if (ip->ref > 0 && ip->dev == dev && ip->inum == inum) {
      ip->ref++;
      release(&icache.lock);
      return ip;
    }
    if (empty == 0 && ip->ref == 0) // Remember empty slot.
      empty = ip;
  }

  // Recycle an inode cache entry.
  if (empty == 0)
    panic("iget: no inodes");

  ip = empty;
  ip->ref = 1;
  ip->dev = dev;
  ip->inum = inum;

  release(&icache.lock);

  read_dinode(ip->inum, &dip);
  ip->type = dip.type;
  ip->devid = dip.devid;
  ip->size = dip.size;
  ip->data = dip.data;
  memmove(ip->overflowBlocks, dip.overflowBlocks, sizeof(dip.overflowBlocks));

  if (ip->type == 0)
    panic("iget: no type");

  return ip;
}

// Increment reference count for ip.
// Returns ip to enable ip = idup(ip1) idiom.
struct inode *idup(struct inode *ip) {
  acquire(&icache.lock);
  ip->ref++;
  release(&icache.lock);
  return ip;
}

// Drop a reference to an in-memory inode.
// If that was the last reference, the inode cache entry can
// be recycled.
// If that was the last reference and the inode has no links
// to it, free the inode (and its content) on disk.
void irelease(struct inode *ip) {
  acquire(&icache.lock);
  // inode has no links and no other references release
  if (ip->ref == 1)
    ip->type = 0;
  ip->ref--;
  release(&icache.lock);
}

// Copy stat information from inode.
void stati(struct inode *ip, struct stat *st) {
  st->dev = ip->dev;
  st->ino = ip->inum;
  st->type = ip->type;
  st->size = ip->size;
}

int readi(struct inode *ip, char *dst, uint off, uint n) {
  uint tot, m;
  struct buf *bp;

  if (ip->type == T_DEV) {
    if (ip->devid < 0 || ip->devid >= NDEV || !devsw[ip->devid].read)
      return -1;
    return devsw[ip->devid].read(ip, dst, n);
  }

  if (off > ip->size || off + n < off)
    return -1;
  if (off + n > ip->size)
    n = ip->size - off;

  for (tot = 0; tot < n; tot += m, off += m, dst += m) {
    bp = bread(ip->dev, ip->data.startblkno + off / BSIZE);
    m = min(n - tot, BSIZE - off % BSIZE);
    memmove(dst, bp->data + off % BSIZE, m);
    brelse(bp);
  }
  return n;
}


// Write data to inode.
int writei(struct inode *ip, char *src, uint off, uint n) {
  if (ip->type == T_DEV) {
    if (ip->devid < 0 || ip->devid >= NDEV || !devsw[ip->devid].write)
      return -1;
    return devsw[ip->devid].write(ip, src, n);
  }

  int increase = 0;

  if((off + n) > ip->size){
    increase = off + n - ip->size;
  }

  uint startingBlock = off / BSIZE;
  // adjust the offset to be correct for the block that it is writing
  // in
  while (off >= BSIZE) {
    off -= BSIZE;
  }

  struct extent data = ip->data;
  uint blockNum = data.startblkno + startingBlock;

  // case 1: offset + n < 512 ie our write does not span
  // beyond 1 block
  // case 2: offset + n >= 512 ie we are writing across
  // multiple blocks

  uint amtWritten = 0;
  acquiresleep(&logmemory.lock);
  while (n > 0) {
    // avoid copying garbage from beyond the bounds of src
    uint amtToWrite = ((BSIZE - off) > n) ? n : (BSIZE - off);

    struct buf *buf = bread(ROOTDEV, blockNum);
    memmove((buf->data) + off, (src + amtWritten), amtToWrite);
    logwrite(blockNum, buf);
    //bwrite(buf);
    brelse(buf);
    //logcommit();
    // if n goes to 0 after this write we are starting
    // from the beginning of the next block
    off = 0;
    n -= amtToWrite;

    if(n < 0)
       panic("writei issue, off by 1?");

    amtWritten += amtToWrite;
    // assumes that the user isn't writing too much
    blockNum += 1;
  }
  ip->size += increase;
  updateDinodeSize(increase, ip->inum);

  logcommit();
  releasesleep(&logmemory.lock);
  return amtWritten;
}

// Directories

int namecmp(const char *s, const char *t) { return strncmp(s, t, DIRSIZ); }

struct inode *rootlookup(char *name) {
  return dirlookup(namei("/"), name, 0);
}

// Look for a directory entry in a directory.
// If found, set *poff to byte offset of entry.
struct inode *dirlookup(struct inode *dp, char *name, uint *poff) {
  uint off, inum;
  struct dirent de;

  if (dp->type != T_DIR)
    panic("dirlookup not DIR");

  for (off = 0; off < dp->size; off += sizeof(de)) {
    if (readi(dp, (char *)&de, off, sizeof(de)) != sizeof(de))
      panic("dirlink read");
    if (de.inum == 0)
      continue;
    if (namecmp(name, de.name) == 0) {
      // entry matches path element
      if (poff)
        *poff = off;
      inum = de.inum;
      return iget(dp->dev, inum);
    }
  }

  return 0;
}

// Paths

// Copy the next path element from path into name.
// Return a pointer to the element following the copied one.
// The returned path has no leading slashes,
// so the caller can check *path=='\0' to see if the name is the last one.
// If no name to remove, return 0.
//
// Examples:
//   skipelem("a/bb/c", name) = "bb/c", setting name = "a"
//   skipelem("///a//bb", name) = "bb", setting name = "a"
//   skipelem("a", name) = "", setting name = "a"
//   skipelem("", name) = skipelem("////", name) = 0
//
static char *skipelem(char *path, char *name) {
  char *s;
  int len;

  while (*path == '/')
    path++;
  if (*path == 0)
    return 0;
  s = path;
  while (*path != '/' && *path != 0)
    path++;
  len = path - s;
  if (len >= DIRSIZ)
    memmove(name, s, DIRSIZ);
  else {
    memmove(name, s, len);
    name[len] = 0;
  }
  while (*path == '/')
    path++;
  return path;
}

// Look up and return the inode for a path name.
// If parent != 0, return the inode for the parent and copy the final
// path element into name, which must have room for DIRSIZ bytes.
// Must be called inside a transaction since it calls iput().
static struct inode *namex(char *path, int nameiparent, char *name) {
  struct inode *ip, *next;

  if (*path == '/')
    ip = iget(ROOTDEV, ROOTINO);
  else
    ip = idup(namei("/"));

  while ((path = skipelem(path, name)) != 0) {
    if (ip->type != T_DIR)
      goto notfound;

    // Stop one level early.
    if (nameiparent && *path == '\0')
      return ip;

    if ((next = dirlookup(ip, name, 0)) == 0)
      goto notfound;

    irelease(ip);
    ip = next;
  }
  if (nameiparent)
    goto notfound;

  return ip;

notfound:
  irelease(ip);
  return 0;
}

struct inode *namei(char *path) {
  char name[DIRSIZ];
  return namex(path, 0, name);
}

struct inode *nameiparent(char *path, char *name) {
  return namex(path, 1, name);
}

void swapread(int spn, uint64_t mem){
  int blocknum = spn*8 + sb.swapstart;

  //cprintf("SWAPREAD blocknum: %d swapstart: %d spn: %d addr: %d\n", blocknum, sb.swapstart, spn, mem);

  for (int i = 0; i < 8; i++){
    struct buf *buf = bread(ROOTDEV, blocknum + i);
    memmove(P2V(mem + i*BSIZE), buf->data, BSIZE);
    brelse(buf);
  }
}

void swapwrite(int spn, int ppn){
  int blocknum = spn*8 + sb.swapstart;
  uint64_t physAddr = ppn << PT_SHIFT;
  
  //cprintf("SWAPWRITE blocknum: %d swapstart: %d spn: %d addr: %d\n", blocknum, sb.swapstart, spn, physAddr);
  for (int i = 0; i < 8; i++){
    struct buf *buf = bread(ROOTDEV, blocknum + i);
    memmove(buf->data, P2V(physAddr + i*BSIZE), BSIZE);
    bwrite(buf);
    brelse(buf);
  }
}

//--LOG--//
void updatefreeblock(int highblock){
  int trackhighblock = 2;
  struct buf *buf = bread(ROOTDEV, trackhighblock);
  memmove(buf->data, &highblock, sizeof(int));
  bwrite(buf);
  brelse(buf);
}

void checkfreeblock(){
  int trackhighblock = 2;
  int highblock;
  struct buf *buf = bread(ROOTDEV, trackhighblock);
  memmove(&highblock, buf->data, sizeof(int));
  brelse(buf);

  if (highblock >= nextfree){
    nextfree = highblock+1;
  }
}

void initlog(){  
  checkfreeblock();
  logmemory.startblock = 16386;
  logmemory.blocksinuse = 0; 
  recovery();
}

void flushlog(){
  int blocknum = logmemory.startblock;
  struct buf *buf = bread(ROOTDEV, blocknum);
  memmove(buf->data, &logmemory.ld, sizeof(struct logdisk));
  bwrite(buf);
  brelse(buf);
}


void readlog_disk(){
  int blocknum = logmemory.startblock;
  struct buf *buf = bread(ROOTDEV, blocknum);
  memmove(&logmemory.ld, buf->data, sizeof(struct logdisk));
  brelse(buf);
}

void recovery(){
  readlog_disk();

  if (!logmemory.ld.committed){
    //zerolog();
  }
  else{
    //int numblockwrites = logmemory.ld.size;

    logBlocksToDisk();
    //log is now reset, zero header and continue normal book
    zerolog();
  }
}

void logwrite(int extentblocknum, struct buf* srcbuf){
  srcbuf->flags |= B_DIRTY;
  logmemory.blocks[logmemory.blocksinuse] = extentblocknum;
  logmemory.blocksinuse+=1;
}

void logcommit(){
  logBlocksToLog();
  logmemory.ld.committed = 1;
  flushlog();

  logBlocksToDisk();
  logmemory.ld.committed = 0;
  flushlog();

  initlog();
}

void zerolog(){
  //memset(&logmemory.ld, 0, sizeof(struct logdisk));
  logmemory.blocksinuse = 0;
  logmemory.ld.size = 0;
  logmemory.ld.committed = 0;
  flushlog();
}

void logBlocksToLog(){
  int highblock = 0;
  logmemory.ld.size = logmemory.blocksinuse;
  for (int i = 0; i < logmemory.blocksinuse; i++){
    //cprintf("LBL blocknum: %d\n", logmemory.blocks[i]);
    //cprintf("LBL blocknum: %d\n", (logmemory.startblock + i + 1));
    struct buf *buf = bread(ROOTDEV, logmemory.blocks[i]);
    struct buf *bufTo = bread(ROOTDEV, (logmemory.startblock + i + 1));
    memmove(bufTo->data, buf->data, BSIZE);
    bwrite(bufTo);
    brelse(buf);
    brelse(bufTo);
    logmemory.ld.blocks[i] = logmemory.blocks[i];
    if (logmemory.blocks[i] > highblock){
      highblock = logmemory.blocks[i];
    }
  } 
  if (highblock >= nextfree){
    updatefreeblock(highblock);
  }
}

void logBlocksToDisk(){
  int numblockwrites = logmemory.ld.size;
  int highblock;
  //read from logregion then write to intended extent
  for (int i = 0; i < numblockwrites ; i++){
    //cprintf("B2D blocknum: %d\n", (logmemory.startblock + i + 1));
    //cprintf("B2D blocknum: %d\n", (logmemory.ld.blocks[i]));
    struct buf *srcblock = bread(ROOTDEV, logmemory.startblock + i + 1); //plus 1 skips the logdisk/header
    struct buf *dstblock = bread(ROOTDEV, logmemory.ld.blocks[i]);       //block from extent

    memmove(dstblock->data, srcblock->data, BSIZE);
    bwrite(dstblock);
    brelse(srcblock);
    brelse(dstblock);
    
    if (logmemory.ld.blocks[i] > highblock){
      highblock = logmemory.ld.blocks[i];
    }
  }
  
  if (highblock >= nextfree){
    updatefreeblock(highblock);
  }

}

