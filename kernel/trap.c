#include <cdefs.h>
#include <defs.h>
#include <memlayout.h>
#include <mmu.h>
#include <param.h>
#include <proc.h>
#include <spinlock.h>
#include <trap.h>
#include <x86_64.h>

// Interrupt descriptor table (shared by all CPUs).
struct gate_desc idt[256];
extern void *vectors[]; // in vectors.S: array of 256 entry pointers
struct spinlock tickslock;
uint ticks;

int num_page_faults = 0;

void tvinit(void) {
  int i;

  for (i = 0; i < 256; i++)
    set_gate_desc(&idt[i], 0, SEG_KCODE << 3, vectors[i], KERNEL_PL);
  set_gate_desc(&idt[TRAP_SYSCALL], 1, SEG_KCODE << 3, vectors[TRAP_SYSCALL],
                USER_PL);

  initlock(&tickslock, "time");
}

void idtinit(void) { lidt((void *)idt, sizeof(idt)); }

void trap(struct trap_frame *tf) {
  uint64_t addr;
  uint64_t error;
  struct vregion *stack; 

  if (tf->trapno == TRAP_SYSCALL) {
    if (myproc()->killed)
      exit();
    myproc()->tf = tf;
    syscall();
    if (myproc()->killed)
      exit();
    return;
  }

  switch (tf->trapno) {
  case TRAP_IRQ0 + IRQ_TIMER:
    if (cpunum() == 0) {
      acquire(&tickslock);
      ticks++;
      wakeup(&ticks);
      release(&tickslock);
    }
    lapiceoi();
    break;
  case TRAP_IRQ0 + IRQ_IDE:
    ideintr();
    lapiceoi();
    break;
  case TRAP_IRQ0 + IRQ_IDE + 1:
    // Bochs generates spurious IDE1 interrupts.
    break;
  case TRAP_IRQ0 + IRQ_KBD:
    kbdintr();
    lapiceoi();
    break;
  case TRAP_IRQ0 + IRQ_COM1:
    uartintr();
    lapiceoi();
    break;
  case TRAP_IRQ0 + 7:
  case TRAP_IRQ0 + IRQ_SPURIOUS:
    cprintf("cpu%d: spurious interrupt at %x:%x\n", cpunum(), tf->cs, tf->rip);
    lapiceoi();
    break;

  //stack growth
  case TRAP_PF:

    addr = rcr2();
    error = myproc()->tf->err;
    
    struct vregion *vr;
    struct vpage_info *vpi = NULL;
    struct proc *thisproc = myproc(); 
    stack = &myproc()->vspace.regions[VR_USTACK];
    uint64_t stackBottom = stack->va_base - stack->size;
    uint64_t lowStack = SZ_STACK_LIMIT;
  
    num_page_faults += 1;

    if (myproc() == 0 || (tf->cs & 3) == 0) {
      // In kernel, it must be our mistake.
      cprintf("unexpected trap %d from cpu %d rip %lx (cr2=0x%x)\n",
              tf->trapno, cpunum(), tf->rip, addr);
      panic("trap");
    }

    vr = va2vregion(&myproc()->vspace, addr);
    if (vr > 0){
        vpi = va2vpage_info(vr, addr);
    }

    //cprintf("error: %d addr: %d ppn: %d spn: %d used: %d\n", error, addr, vpi->ppn, vpi->spn, vpi->used); 

    // page is in swap
    // non present page, could be 4 (user, non present), 6 (user, read
    // write, non present)
    if ((error == 4 || error == 6) && vpi && vpi->used && !vpi->present) {
      // evict physical memory
      // write out to swap
      //int ppn;
      //if ((ppn = evictmem()) < 0) {
      //  panic("no more space in swap");
      //}

      char* page; 
      page = kalloc();

      vpi->ppn = (PGNUM(V2P(page)));      
//      vpi->present = 1;

      int checkppnaddr = vpi->ppn << PT_SHIFT;

      struct core_map_entry *entry = pa2page(vpi->ppn << PT_SHIFT);
      //cprintf("prePageIn rc: %d ppn: %d spn: %d avail: %d\n", entry->refcount, vpi->ppn, vpi->spn, entry->available);

      //vspaceinvalidate(&myproc()->vspace);
      //vspaceinstall(myproc());
      // bring in the page and updated any other processes that
      // are referencing the same swap page, does not handle
      // copy on write errors, this will be handled by the
      // next fault
      addr = PGROUNDDOWN(addr);
      entry->va = addr;
      
      pagein(vpi->ppn, vpi, addr);
      vspaceinvalidate(&myproc()->vspace);
      vspaceinstall(myproc());
      //cprintf("postPageIn rc: %d ppn: %d spn: %d entryavail: %d present: %d cow: %d\n", entry->refcount, vpi->ppn, vpi->spn, entry->available, vpi->present, vpi->copyonwrite);
      break;
    }

    //if (error == 6){
    //  growheap(1000);
    //  break;
    //}

    //cow fault = 0b111
    if (error == 7) {//(PTE_U|PTE_W)){
      
      //check if addr is in valid region
      vr = va2vregion(&myproc()->vspace, addr);
      if(vr > 0){
        //get pageinfo
        vpi = va2vpage_info(vr, addr);
        if(vpi->copyonwrite){
          //dec ref
          vpi->writable = 1;
          vpi->copyonwrite = 0;
        }
           
        //create new page/add to vspace
            
        if (getphysicalrefcount(vpi->ppn) > 1) {
          decrefcount(vpi->ppn);
          char *temp;
          temp = kalloc();

          addr = PGROUNDDOWN(addr);
          memmove(temp, (void *) addr, PGSIZE);
          vpi->ppn = PGNUM(V2P(temp));
        }

        vspaceinvalidate(&myproc()->vspace);
        vspaceinstall(myproc());
        break;
      }
    }

    //stack growth = 0b101
    if ((error == (PTE_U|PTE_P) || (error == 6 && (addr >= lowStack && addr < stackBottom)))){ 
      if (addr >= lowStack && addr < stackBottom){
        uint64_t roundAddr = PGROUNDDOWN(addr);
        uint64_t size = stackBottom - roundAddr;
     
        stack->size += size;
        vregionaddmap(stack, roundAddr, size, 1, 1);
        vspaceinvalidate(&myproc()->vspace);
        break;
      } 
    }
    
      

    // Assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
          "rip 0x%lx addr 0x%x--kill proc\n",
          myproc()->pid, myproc()->name, tf->trapno, tf->err, cpunum(),
          tf->rip, addr);
    myproc()->killed = 1;
    break;

  default:
    addr = rcr2();

    if (tf->trapno == TRAP_PF) {
      num_page_faults += 1;

      if (myproc() == 0 || (tf->cs & 3) == 0) {
        // In kernel, it must be our mistake.
        cprintf("unexpected trap %d from cpu %d rip %lx (cr2=0x%x)\n",
                tf->trapno, cpunum(), tf->rip, addr);
        panic("trap");
      }
    }

    // Assume process misbehaved.
    cprintf("pid %d %s: trap %d err %d on cpu %d "
            "rip 0x%lx addr 0x%x--kill proc\n",
            myproc()->pid, myproc()->name, tf->trapno, tf->err, cpunum(),
            tf->rip, addr);
    myproc()->killed = 1;
  }

  // Force process exit if it has been killed and is in user space.
  // (If it is still executing in the kernel, let it keep running
  // until it gets to the regular system call return.)
  if (myproc() && myproc()->killed && (tf->cs & 3) == DPL_USER)
    exit();

  // Force process to give up CPU on clock tick.
  // If interrupts were on while locks held, would need to check nlock.
  if (myproc() && myproc()->state == RUNNING &&
      tf->trapno == TRAP_IRQ0 + IRQ_TIMER)
    yield();

  // Check if the process has been killed since we yielded
  if (myproc() && myproc()->killed && (tf->cs & 3) == DPL_USER)
    exit();
}
