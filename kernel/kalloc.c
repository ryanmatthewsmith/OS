// Physical memory allocator, intended to allocate
// memory for user processes, kernel stacks, page table pages,
// and pipe buffers. Allocates 4096-byte pages.

#include <cdefs.h>
#include <defs.h>
#include <e820.h>
#include <memlayout.h>
#include <mmu.h>
#include <param.h>
#include <spinlock.h>
#include <proc.h>

int npages = 0;
uint64_t last_evicted = 0;
int pages_in_use;
int pages_in_swap;
int free_pages;
int NSWAP = 2048;

struct core_map_entry *core_map = NULL;

//struct swapinfo {
//  short inswap;
//  short refcount;
//  uint64_t va;
//};

struct swapinfo swaptable[2048];

struct core_map_entry *pa2page(uint64_t pa) {
  if (PGNUM(pa) >= (npages)) {
    int ppn = PGNUM(pa);
    cprintf("%x\n", pa);
    cprintf("ppn: %d pa: %d\n", ppn, pa);  
    panic("pa2page called with invalid pa");
  }
  return &core_map[PGNUM(pa)];
}

uint64_t page2pa(struct core_map_entry *pp) {
  return (pp - core_map) << PT_SHIFT;
}

// --------------------------------------------------------------
// Detect machine's physical memory setup.
// --------------------------------------------------------------

void detect_memory(void) {
  uint32_t i;
  struct e820_entry *e;
  size_t mem = 0, mem_max = -KERNBASE;

  e = e820_map.entries;
  for (i = 0; i != e820_map.nr; ++i, ++e) {
    if (e->addr >= mem_max)
      continue;
    mem = max(mem, (size_t)(e->addr + e->len));
  }

  // Limit memory to 256MB.
  mem = min(mem, mem_max);
  npages = mem / PGSIZE;
  cprintf("E820: physical memory %dMB\n", mem / 1024 / 1024);
}

void freerange(void *vstart, void *vend);
extern char end[]; // first address after kernel loaded from ELF file

struct {
  struct spinlock lock;
  int use_lock;
} kmem;

// Initialization happens in two phases.
// 1. main() calls kinit1() while still using entrypgdir to place just
// the pages mapped by entrypgdir on free list.
// 2. main() calls kinit2() with the rest of the physical pages
// after installing a full page table that maps them on all cores.
void mem_init(void *vstart) {
  void *vend;

  core_map = vstart;
  memset(vstart, 0, PGROUNDUP(npages * sizeof(struct core_map_entry)));
  vstart += PGROUNDUP(npages * sizeof(struct core_map_entry));

  initlock(&kmem.lock, "kmem");
  kmem.use_lock = 0;

  vend = (void *)P2V((uint64_t)((npages) * PGSIZE));
  freerange(vstart, vend);
  free_pages = (vend - vstart) >> PT_SHIFT;
  pages_in_use = 0;
  pages_in_swap = 0;
  kmem.use_lock = 1;
}

void freerange(void *vstart, void *vend) {
  char *p;
  p = (char *)PGROUNDUP((uint64_t)vstart);
  for (; p + PGSIZE <= (char *)vend; p += PGSIZE)
    kfree(p);
}

// Free the page of physical memory pointed at by v,
// which normally should have been returned by a
// call to kalloc().  (The exception is when
// initializing the allocator; see kinit above.)
void kfree(char *v) {
  struct core_map_entry *r;
  int t = (uint64_t)v % PGSIZE;
  int u = v < _end;
  int x = V2P(v) >= (uint64_t)((npages) * PGSIZE);

  if ((uint64_t)v % PGSIZE || v < _end || V2P(v) >= (uint64_t)((npages) * PGSIZE))
    panic("kfree");

  if (kmem.use_lock) {
    acquire(&kmem.lock);
  }
  //if (!r->available){
    int check = V2P(v);
    //cprintf("in kfree, pa: %d\n", check);
    r = (struct core_map_entry *)pa2page(V2P(v));

    if (kmem.use_lock) {
      release(&kmem.lock);
      struct vregion *vr = va2vregion(&(myproc()->vspace), r->va);
      if (vr != NULL) {
         struct vpage_info *info = va2vpage_info(vr, r->va);
         if (!info->present) {
            struct swapinfo *sinfo = &swaptable[info->spn];
            decref_swapinfo(sinfo);
            return;
         }
      }
      acquire(&kmem.lock);
    }

    if (kmem.use_lock && r->refcount > 1) {
      r->refcount -= 1;
      release(&kmem.lock);
      return;
    }

    pages_in_use--;
    free_pages++;

    // Fill with junk to catch dangling refs.
    memset(v, 2, PGSIZE);

    r->available = 1;
    r->user = 0;
    r->va = 0;
    r->refcount = 0;
    if (kmem.use_lock)
      release(&kmem.lock);

  //}
}

void
mark_user_mem(uint64_t pa, uint64_t va)
{
  if (pa > 4200000){
    cprintf("in markuser, pa: %d\n", pa);
    //return;
  }
  // for user mem, add an mapping to proc_info
  struct core_map_entry *r = pa2page(pa);

  r->user = 1;
  r->va = va;
}

void
mark_kernel_mem(uint64_t pa)
{
  //cprintf("in markkernel, pa: %d\n", pa);
  // for user mem, add an mapping to proc_info
  struct core_map_entry *r = pa2page(pa);

  r->user = 0;
  r->va = 0;
}

char *kalloc(void) {
  pages_in_use++;
  free_pages--;

  int i;

  if (kmem.use_lock)
    acquire(&kmem.lock);

  for (i = 0; i < npages; i++) {
    if (core_map[i].available == 1) {
      core_map[i].available = 0;
      core_map[i].refcount = 1;
      if (kmem.use_lock)
        release(&kmem.lock);
      return P2V(page2pa(&core_map[i]));
    }
  }
  free_pages++;
  pages_in_use--;

  uint64_t ppn;
  if (kmem.use_lock)
    release(&kmem.lock);
  if ((ppn = evictmem()) < 0) {
    return -1;
  }

  if (kmem.use_lock)
    acquire(&kmem.lock);

  core_map[ppn].available = 0;
  core_map[ppn].refcount = 1;

  if (kmem.use_lock)
    release(&kmem.lock);

  return P2V(page2pa(&core_map[ppn]));
}

void bumpref(struct core_map_entry *entry) {
  if (kmem.use_lock)
    acquire(&kmem.lock);
  entry->refcount += 1;
  if (kmem.use_lock)
    release(&kmem.lock);
}

void decref(struct core_map_entry *entry) {
  if (kmem.use_lock)
    acquire(&kmem.lock);
  entry->refcount -= 1;
  if (kmem.use_lock)
    release(&kmem.lock);
}

void bumpref_swapinfo(struct swapinfo *swap) {
  if (kmem.use_lock)
    acquire(&kmem.lock);
  swap->refcount += 1;
  if (kmem.use_lock)
    release(&kmem.lock);
}

void decref_swapinfo(struct swapinfo *swap) {
  if (kmem.use_lock)
    acquire(&kmem.lock);
  swap->refcount -= 1;
  if (swap->refcount == 0 && swap->inswap) {
    swap->inswap = 0;
    pages_in_swap--;
  }
  if (kmem.use_lock)
    release(&kmem.lock);
}


void bumprefcount(uint64_t ppn) {
  struct core_map_entry *entry = &core_map[ppn];
  if (kmem.use_lock)
    acquire(&kmem.lock);
  entry->refcount += 1;
  if (kmem.use_lock)
    release(&kmem.lock);
}
void decrefcount(uint64_t ppn) {
  struct core_map_entry *entry = &core_map[ppn];
  if (kmem.use_lock)
    acquire(&kmem.lock);
  entry->refcount -= 1;
  if (kmem.use_lock)
    release(&kmem.lock);
}
int getphysicalrefcount(uint64_t ppn) {
  struct core_map_entry *entry = &core_map[ppn];
  return entry->refcount;
}

void swapfree(struct swapinfo *swp){
  swp->inswap = 0;
  swp->refcount = 0;
}

void bumpswapbyspn(int swapindex){
  struct swapinfo *swap = &swaptable[swapindex];
  bumpref_swapinfo(swap);
}

void decswapbyspn(int swapindex) {
   struct swapinfo *swap = &swaptable[swapindex];
   decref_swapinfo(swap);
}

uint64_t evictmem() {
  struct core_map_entry *entry;
  pages_in_swap++;

  // get from core map array value of next evicted memory
  uint64_t evicted_ppn = lruApprox();
  int spn;
  spn = getOpenSwap();
  if (spn == -1){
    return -1;
  }
  
  entry = &core_map[evicted_ppn];
  swaptable[spn].va = entry->va;


  entry->available = 1;
  // if ppn referenced by other vspace
  // iterate through proc
  // boot that entry
  swapwrite(spn, evicted_ppn);  
  updateproc_eviction(&swaptable[spn], entry, evicted_ppn, spn);
  //entry->available = 1;
    
  
  if (kmem.use_lock) 
    acquire(&kmem.lock);
  
  //entry->available = 1;
  entry->va = 0;
  entry->user = 0;
  
  if (kmem.use_lock)
    release(&kmem.lock);
    
  return evicted_ppn;
}

// ppn has just been evicted and is OK to be overwritten
void pagein(int ppn, struct vpage_info *info, uint64_t addr) {
  // zero out the ppn info
  // just worrying about refcount right now because we need to
  // make sure that refcounts are consistent, not too concerned about
  // security yet
  struct core_map_entry *entry = &core_map[ppn];
  struct swapinfo *sentry = &swaptable[info->spn];
  //assert(entry->refcount == 0);
  //assert(sentry->refcount > 0);
  if (kmem.use_lock)
    acquire(&kmem.lock);
  entry->available = 0;
  entry->refcount = 0;
  entry->va = PGROUNDDOWN(addr);

  //cprintf("pagein va addr: %d\n", addr);

  if (kmem.use_lock)
    release(&kmem.lock);
  
  // multiple references to this swap page make sure that these
  // pages' ppn is updated to point to this recently evicted ppn
  updateproc_pagein(sentry, entry, info->spn, ppn, info);
  swapread(info->spn, ppn << PT_SHIFT);
}

uint64_t lruApprox(){
    uint64_t next_evicted = last_evicted;
    uint64_t backToStart = last_evicted;
    struct core_map_entry *entry;

    while(1){
      next_evicted++;
      
      if (next_evicted >= npages){
        next_evicted = 0;
      }

      entry = &core_map[next_evicted];
      if (!entry->user) {
        continue;
      }
      
      uint64_t addr = entry->va;
      
      int accessed = 0;

      accessed = was_va_accessed(addr);

      if(!accessed){
        last_evicted = next_evicted;
        //cprintf("ppn evicted: %d\n", next_evicted);
        return next_evicted;
      }
    }
}

int getOpenSwap(){    
    int spn; 
    for (int i = 0; i < NSWAP; i++) {
      spn = i;

      struct swapinfo *sentry = &swaptable[i];
      if (!sentry->inswap) {
        //assert(sentry->refcount == 0);
        sentry->inswap = 1;
        break;
      }
    }

    if (spn == NSWAP) {
      cprintf("HIT NSWAP, swap full \n");
      return -1;
    }

    //cprintf("spn: %d\n", spn);
    return spn;
}
