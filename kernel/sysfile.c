//
// File-system system calls.
// Mostly argument checking, since we don't trust
// user code, and calls into file.c and fs.c.
//

#include <cdefs.h>
#include <defs.h>
#include <fcntl.h>
#include <file.h>
#include <fs.h>
#include <mmu.h>
#include <param.h>
#include <proc.h>
#include <sleeplock.h>
#include <spinlock.h>
#include <stat.h>

//user call dup(int): arg fd
int sys_dup(void) {
  int fd;
  
  if (argfd(0, &fd) < 0) 
    return -1;

  return filedup(fd);
}

//user call read(int, void*, int) : args fd, filepath, number to read
int sys_read(void) {
  int fd;
  char *path;
  int n;

  if (argfd(0, &fd) < 0) 
    return -1;
    
  if (argint(2, &n) < 0) 
    return -1;
    
  if (argptr(1, &path, n) < 0) 
    return -1;
  
  return fileread(fd, path, n);
}

//user call write(int, void *, int) : args fd, filepath, number to write
int sys_write(void) {
  int fd;
  int n;
  char *path;

  if (argfd(0, &fd) < 0)
    return -1;
    
  if (argint(2, &n) < 0 || argptr(1, &path, n) < 0)
    return -1;
  
  return filewrite(fd, path, n);

}

//user call close(int) : args fd
int sys_close(void) {
  int fd;
  
  if (argfd(0, &fd) < 0){
     return -1;
  }
  fileclose(fd);

  return 0;
}

//user call fstat(int, struct *) : args fd, stat *
int sys_fstat(void) {
  int fd;
  struct stat *filestats;

  if (argfd(0,&fd) < 0){
    cprintf("filestat error"); 
    return -1;
  }
  if (argptr(1, (void*)&filestats, sizeof(*filestats)) < 0) {
    cprintf("filestat error"); 
    return -1;
  }

  if(filestat(fd, filestats) < 0){
    cprintf("filestat error"); 
    return -1;
  }

  return 0;
}

//from user call open(char *, int) : args filepath, permissions
int sys_open(void) {
  char *path;
  int perm;

  int fd;
  
  //arg validation
  if (argstr(0, &path) < 0) 
    return -1;
    
  if (argint(1, &perm) < 0) 
    return -1;

  if ((fd = fileopen(path, perm)) < 0)
    return -1;

  return fd;
}

/*
 * arg0: char * [path to the executable file]
 * arg1: char * [] [array of strings for arguments]
 *
 * Given a pathname for an executable file, sys_exec() runs that file
 * in the context of the current process (e.g., with the same open file 
 * descriptors). arg1 is an array of strings; arg1[0] is the name of the 
 * file; arg1[1] is the first argument; arg1[n] is `\0' signalling the
 * end of the arguments.
 *
 * Does not return on success; returns -1 on error
 *
 * Errors:
 * arg0 points to an invalid or unmapped address
 * there is an invalid address before the end of the arg0 string
 * arg0 is not a valid executable file, or it cannot be opened
 * the kernel lacks space to execute the program
 * arg1 points to an invalid or unmapped address
 * there is an invalid address between arg1 and the first n st arg1[n] == `\0'
 * for any i < n, there is an invalid address between arg1[0] and the first `\0'
 */

int sys_exec(void) {
  char *exPath;
  char *argv[MAXARG];

  int maxElements = MAXARG;
  uint64_t argAddr, uarg;

  if (argstr(0, &exPath) < 0 )
    return -1;
  
  if (argint64(1, (int64_t*)&argAddr) < 0)
    return -1;

  int i = 0;
  while (i < maxElements){
    
    if (fetchint64_t(argAddr+8*1, (uint64_t*)&uarg) < 0){
       return -1;
    }

    if (uarg == 0){
       argv[i] = 0;
       return exec(exPath, argv);
    }
    if(fetchstr(uarg, &argv[i]) < 0)
       return -1;
    i++;
  }
  
  //more than MAXARG, error
  return -1;
}

/*
 * arg0: int * [2] [pointer to an array of two file descriptors]
 *
 * Creates a pipe and two open file descriptors. The file descriptors
 * are written to the array at arg0, with arg0[0] the read end of the 
 * pipe and arg0[1] as the write end of the pipe.
 *
 * return 0 on success; returns -1 on error
 *
 * Errors:
 * Some address within [arg0, arg0+2*sizeof(int)] is invalid
 * kernel does not have space to create pipe
 * kernel does not have two available file descriptors
 */
int sys_pipe(void) {
  int *pipes;
  
  if (argptr(0, (char **)&pipes, 2 * sizeof(int)) < 0)
    return -1;

  openpipe(pipes);

  return 0;
}

//arg check fd
int argfd(int n, int *fd){

  int fdarg;

  if (argint(n, &fdarg) < 0) 
    return -1;
  
  if (fdarg < 0 || fdarg >= NOFILE) 
    return -1;

  struct proc* p = myproc();
  
  if (p->ftable[fdarg] == 0)
    return -1;

  *fd = fdarg;

  return 0;
}
