#include <cdefs.h>
#include <defs.h>
#include <file.h>
#include <fs.h>
#include <memlayout.h>
#include <mmu.h>
#include <param.h>
#include <proc.h>
#include <spinlock.h>
#include <trap.h>
#include <x86_64.h>
#include <fs.h>
#include <file.h>
#include <vspace.h>

// process table
struct {
  struct spinlock lock;
  struct proc proc[NPROC];
} ptable;

static struct proc *initproc;

int nextpid = 1;
extern void forkret(void);
extern void trapret(void);

static void wakeup1(void *chan);

// to test crash safety in lab5, 
// we trigger restarts in the middle of file operations
void reboot(void) {
  uint8_t good = 0x02;
  while (good & 0x02)
    good = inb(0x64);
  outb(0x64, 0xFE);
loop:
  asm volatile("hlt");
  goto loop;
}

void pinit(void) { initlock(&ptable.lock, "ptable"); }

// Look in the process table for an UNUSED proc.
// If found, change state to EMBRYO and initialize
// state required to run in the kernel.
// Otherwise return 0.
static struct proc *allocproc(void) {
  struct proc *p;
  char *sp;

  acquire(&ptable.lock);

  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if (p->state == UNUSED)
      goto found;

  release(&ptable.lock);
  return 0;

found:
  p->state = EMBRYO;
  p->pid = nextpid++;

  release(&ptable.lock);

  // Allocate kernel stack.
  if ((p->kstack = kalloc()) == 0) {
    p->state = UNUSED;
    return 0;
  }
  sp = p->kstack + KSTACKSIZE;

  // Leave room for trap frame.
  sp -= sizeof *p->tf;
  p->tf = (struct trap_frame *)sp;

  // Set up new context to start executing at forkret,
  // which returns to trapret.
  sp -= 8;
  *(uint64_t *)sp = (uint64_t)trapret;

  sp -= sizeof *p->context;
  p->context = (struct context *)sp;
  memset(p->context, 0, sizeof *p->context);
  p->context->rip = (uint64_t)forkret;

  return p;
}

// Set up first user process.
void userinit(void) {
  struct proc *p;
  extern char _binary_out_initcode_start[], _binary_out_initcode_size[];

  p = allocproc();

  initproc = p;
  assertm(vspaceinit(&p->vspace) == 0, "error initializing process's virtual address descriptor");
  vspaceinitcode(&p->vspace, _binary_out_initcode_start, (int64_t)_binary_out_initcode_size);
  memset(p->tf, 0, sizeof(*p->tf));
  p->tf->cs = (SEG_UCODE << 3) | DPL_USER;
  p->tf->ss = (SEG_UDATA << 3) | DPL_USER;
  p->tf->rflags = FLAGS_IF;
  p->tf->rip = VRBOT(&p->vspace.regions[VR_CODE]);  // beginning of initcode.S
  p->tf->rsp = VRTOP(&p->vspace.regions[VR_USTACK]);

  safestrcpy(p->name, "initcode", sizeof(p->name));

  // this assignment to p->state lets other cores
  // run this process. the acquire forces the above
  // writes to be visible, and the lock is also needed
  // because the assignment might not be atomic.
  acquire(&ptable.lock);
  p->state = RUNNABLE;
  release(&ptable.lock);
}

// Create a new process copying p as the parent.
// Sets up stack to return as if from system call.
// Caller must set state of returned proc to RUNNABLE.
int fork(void) {
  struct proc *newProc;
 

  newProc = allocproc();
  struct proc *oldProc = myproc();
  
  if(newProc == 0){
    return -1;  
  }
  
  vspaceinit(&newProc->vspace);

  // to be used by COW-fork
  if (vspacecopy(&newProc->vspace, &oldProc->vspace) < 0){
    kfree(&newProc->vspace);
  }
  //if (vspacecopy(&newProc->vspace, &oldProc->vspace) < 0){
  //  kfree((char *)newProc);
  //  return -1; 
  //}

  for (int i = 0; i < NOFILE ; i++){
    struct file *f = oldProc->ftable[i];
    newProc->ftable[i] = f;
    if(f != 0 && f->ref > 0){
       filedupf(f);
    }
  }

  newProc->parent = oldProc;
  *newProc->tf = *oldProc->tf;
  newProc->tf->rax = 0;
  newProc->killed = 0;

  acquire(&ptable.lock);
  int retPid = newProc->pid;
  newProc->state = RUNNABLE;

  vspaceinvalidate(&myproc()->vspace);
  vspaceinstall(myproc());

  release(&ptable.lock);
 
  return retPid;
}

// Exit the current process.  Does not return.
// An exited process remains in the zombie state
// until its parent calls wait() to find out it exited.
void exit(void) {
  struct proc *exitChild;
 
  for(int fd = 0; fd < NOFILE; fd++){
    if(myproc()->ftable[fd] != 0){
      fileclose(fd);
    }
  }

  acquire(&ptable.lock);
  wakeup1(myproc()->parent);

  //before reschedule reparent all exiting process's children, pass to initproc
  for(exitChild = ptable.proc; exitChild < &ptable.proc[NPROC]; exitChild++){
    if(exitChild->parent == myproc()){
      exitChild->parent = initproc;
    }
  }

  myproc()->state = ZOMBIE;
  
  sched();
}

// Wait for a child process to exit and return its pid.
// Return -1 if this process has no children.
int wait(void) {
  struct proc *child;      
  bool hasChild = false;
            
  acquire(&ptable.lock);
  while(1){ //do we need a condition var here? the hasChild should be sufficient?
    hasChild = false;
    for (child = ptable.proc; child < &ptable.proc[NPROC]; child++){
      if (child->parent == myproc() && child->state != UNUSED){
        
        hasChild = true;
        if(child->state == ZOMBIE){
          int pid = child->pid;
          struct vspace vsp = child->vspace; 
          freeallswap(&vsp);
          vspacefree(&vsp);
          void *stk = child->kstack;
          child->parent = 0;
          child->pid = 0;
          child->state = UNUSED;
          child->killed = 0; 
          child->kstack = 0;

          kfree(stk);
          release(&ptable.lock);
          return pid;
        }
      }
    }
    
    if(!hasChild){
      release(&ptable.lock);
      return -1;
    }

    sleep(myproc(), &ptable.lock); 
  }
}

// Per-CPU process scheduler.
// Each CPU calls scheduler() after setting itself up.
// Scheduler never returns.  It loops, doing:
//  - choose a process to run
//  - swtch to start running that process
//  - eventually that process transfers control
//      via swtch back to the scheduler.
void scheduler(void) {
  struct proc *p;

  for (;;) {
    // Enable interrupts on this processor.
    sti();

    // Loop over process table looking for process to run.
    acquire(&ptable.lock);
    for (p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
      if (p->state != RUNNABLE)
        continue;

      // Switch to chosen process.  It is the process's job
      // to release ptable.lock and then reacquire it
      // before jumping back to us.
      mycpu()->proc = p;
      vspaceinstall(p);
      p->state = RUNNING;
      swtch(&mycpu()->scheduler, p->context);
      vspaceinstallkern();

      // Process is done running for now.
      // It should have changed its p->state before coming back.
      mycpu()->proc = 0;
    }
    release(&ptable.lock);
  }
}

// Enter scheduler.  Must hold only ptable.lock
// and have changed proc->state. Saves and restores
// intena because intena is a property of this
// kernel thread, not this CPU. It should
// be proc->intena and proc->ncli, but that would
// break in the few places where a lock is held but
// there's no process.
void sched(void) {
  int intena;

  if (!holding(&ptable.lock))
    panic("sched ptable.lock");
  if (mycpu()->ncli != 1) {
    cprintf("pid : %d\n", myproc()->pid);
    cprintf("ncli : %d\n", mycpu()->ncli);
    cprintf("intena : %d\n", mycpu()->intena);

    panic("sched locks");
  }
  if (myproc()->state == RUNNING)
    panic("sched running");
  if (readeflags() & FLAGS_IF)
    panic("sched interruptible");

  intena = mycpu()->intena;
  swtch(&myproc()->context, mycpu()->scheduler);
  mycpu()->intena = intena;
}

// Give up the CPU for one scheduling round.
void yield(void) {
  acquire(&ptable.lock); // DOC: yieldlock
  myproc()->state = RUNNABLE;
  sched();
  release(&ptable.lock);
}

// A fork child's very first scheduling by scheduler()
// will swtch here.  "Return" to user space.
void forkret(void) {
  static int first = 1;
  // Still holding ptable.lock from scheduler.
  release(&ptable.lock);

  if (first) {
    // Some initialization functions must be run in the context
    // of a regular process (e.g., they call sleep), and thus cannot
    // be run from main().
    first = 0;
    iinit(ROOTDEV);
  }

  // Return to "caller", actually trapret (see allocproc).
}

// Atomically release lock and sleep on chan.
// Reacquires lock when awakened.
void sleep(void *chan, struct spinlock *lk) {
  if (myproc() == 0)
    panic("sleep");

  if (lk == 0)
    panic("sleep without lk");

  // Must acquire ptable.lock in order to
  // change p->state and then call sched.
  // Once we hold ptable.lock, we can be
  // guaranteed that we won't miss any wakeup
  // (wakeup runs with ptable.lock locked),
  // so it's okay to release lk.
  if (lk != &ptable.lock) { // DOC: sleeplock0
    acquire(&ptable.lock);  // DOC: sleeplock1
    release(lk);
  }

  // Go to sleep.
  myproc()->chan = chan;
  myproc()->state = SLEEPING;
  sched();

  // Tidy up.
  myproc()->chan = 0;

  // Reacquire original lock.
  if (lk != &ptable.lock) { // DOC: sleeplock2
    release(&ptable.lock);
    acquire(lk);
  }
}

// Wake up all processes sleeping on chan.
// The ptable lock must be held.
static void wakeup1(void *chan) {
  struct proc *p;

  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++)
    if (p->state == SLEEPING && p->chan == chan)
      p->state = RUNNABLE;
}

// Wake up all processes sleeping on chan.
void wakeup(void *chan) {
  acquire(&ptable.lock);
  wakeup1(chan);
  release(&ptable.lock);
}

// Kill the process with the given pid.
// Process won't exit until it returns
// to user space (see trap in trap.c).
int kill(int pid) {
  struct proc *p;

  acquire(&ptable.lock);
  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
    if (p->pid == pid) {
      p->killed = 1;
      // Wake process from sleep if necessary.
      if (p->state == SLEEPING)
        p->state = RUNNABLE;
      release(&ptable.lock);
      return 0;
    }
  }
  release(&ptable.lock);
  return -1;
}

// Print a process listing to console.  For debugging.
// Runs when user types ^P on console.
// No lock to avoid wedging a stuck machine further.
void procdump(void) {
  static char *states[] = {[UNUSED] = "unused",   [EMBRYO] = "embryo",
                           [SLEEPING] = "sleep ", [RUNNABLE] = "runble",
                           [RUNNING] = "run   ",  [ZOMBIE] = "zombie"};
  int i;
  struct proc *p;
  char *state;
  uint64_t pc[10];

  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
    if (p->state == UNUSED)
      continue;
    if (p->state != 0 && p->state < NELEM(states) && states[p->state])
      state = states[p->state];
    else
      state = "???";
    cprintf("%d %s %s", p->pid, state, p->name);
    if (p->state == SLEEPING) {
      getcallerpcs((uint64_t *)p->context->rbp, pc);
      for (i = 0; i < 10 && pc[i] != 0; i++)
        cprintf(" %p", pc[i]);
    }
    cprintf("\n");
  }
}

struct proc *findproc(int pid) {
  struct proc *p;
  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
    if (p->pid == pid)
      return p;
  }
  return 0;
}

int growheap(int amt){
  struct vregion *heapP = &myproc()->vspace.regions[VR_HEAP];
  int heapSize = heapP->size;
  int startAddr = heapP->va_base + heapSize;

  if (amt < 0){
    //startAddr = startAddr + amt;
    return -1;
  } 
  
  if ( (startAddr + amt) < SZ_HEAP_MAX){
    if (vregionaddmap(&myproc()->vspace.regions[VR_HEAP], startAddr, amt, 1, 1) < 0){
      return -1;
    }
    heapP->size += amt;
    vspaceinvalidate(&myproc()->vspace);
    vspaceinstall(myproc());
    return startAddr;
  }
  //greater than heap max
  return -1;
}

void updateproc_eviction(struct swapinfo *sentry, struct core_map_entry *entry,
                int evicted, int spn) {

  acquire(&ptable.lock);
  struct proc *p;
  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
    // check if real process (!unused)
    if (p->state == RUNNING || p->state == RUNNABLE || 
        p->state == ZOMBIE || p->state == SLEEPING) {
      // get vpage_info struct of the address -> this ppn
      // if used, check = ppn, set to not present, set spn, bump ref count
      struct vregion *vr = va2vregion(&p->vspace, entry->va);
      if (vr != NULL) {
        struct vpage_info *info = va2vpage_info(vr, entry->va);
        // could have multiple pages with virtual addresses that would
        // map to this ppn, when they are evicted their ppn will no
        // longer be valid accurate (set to -1)
        if (info->used && info->ppn == evicted && info->present) {
          //cprintf("IN EVICT SAME PPN");
          assert(info->present);
          info->present = 0;
          //info->ppn = -1;
          info->spn = spn;
          bumpref_swapinfo(sentry);
          decref(entry);
          vspaceinvalidate(&p->vspace);
          if (p->state == RUNNING) {
            vspaceinstall(myproc());
          }
        }
      }
    }
  }
  release(&ptable.lock);
}

void updateproc_pagein(struct swapinfo *sentry, struct core_map_entry *entry,
                 int spn, int ppn) {

  acquire(&ptable.lock);
  struct proc *p;
  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++) {
    // check if real process (!unused)
    if (p->state == RUNNING || p->state == RUNNABLE || p->state == ZOMBIE || p->state == SLEEPING) {
      // get vpage_info struct of the address -> this ppn
      // if used, check = ppn, set to not present, set spn, bump ref count
      struct vregion *vr = va2vregion(&p->vspace, sentry->va);
      if (vr != NULL) {
      struct vpage_info *info = va2vpage_info(vr, sentry->va);
      // could have multiple pages with virtual addresses that would
      // map to this ppn, when they are evicted their ppn will no
      // longer be valid accurate (set to -1)
      if (info->used && !info->present && info->spn == spn) {
        //cprintf("PageIn ppn: %d spn: %d\n", ppn, spn);
        //assert(!info->present);
        info->present = 1;
        info->ppn = ppn;
        bumpref(entry);
        decref_swapinfo(sentry);
        vspaceinvalidate(&p->vspace);
        if (p->state == RUNNING) {
          vspaceinstall(myproc());
        }
      }
      }
    }
  }
  release(&ptable.lock);
  //swapfree(sentry);
}

int was_va_accessed(uint64_t addr){
  int accessed = 0;
  struct proc *p;
  for (p = ptable.proc; p < &ptable.proc[NPROC]; p++){
    if(p->state == ZOMBIE || p->state == RUNNABLE || p->state == SLEEPING || p->state == RUNNING){
      if ((vspacecontains(&p->vspace, addr, 0)) && (vawasaccessed(&p->vspace, addr)) ){
          accessed = 1;
          break;
      }
    }
  }

  return accessed;

}
