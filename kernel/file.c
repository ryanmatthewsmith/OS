#include <cdefs.h>
#include <defs.h>
#include <fcntl.h>
#include <file.h>
#include <fs.h>
#include <param.h>
#include <proc.h>
#include <sleeplock.h>
#include <spinlock.h>
#include <stat.h>

struct devsw devsw[NDEV];

// global open file table
struct {
  struct file file[NFILE];
  struct spinlock lock;
} ftable;

void initfiletable() {
  memset(ftable.file, 0, sizeof(struct file) * NFILE);
  initlock(&ftable.lock, "ftable");
}

int fileopen(char *path, int perm) {
  int fd, i;
  struct inode *ip;

  if (perm == O_CREATE || perm == (O_CREATE|O_RDWR) || perm == (O_WRONLY|O_CREATE)){
    if ((ip = createfile(path)) < 0){
      return -1;
    }
  }

  if ((ip = namei(path)) == 0) {
    return -1;
  }
 
  acquire(&ftable.lock);
  i = findnextfd();

  if (i == NFILE) {
    release(&ftable.lock);
    return -1;
  }

  struct proc *pr = myproc();

  for (fd = 0; fd < NOFILE; fd++) {
    if (pr->ftable[fd] == 0) {
      pr->ftable[fd] = &ftable.file[i];
      ftable.file[i].ref++;
      ftable.file[i].type = F_INODE;
      ftable.file[i].ip = ip;
      ftable.file[i].pp = 0;
      ftable.file[i].offset = 0;
      ftable.file[i].permissions = perm;
      release(&ftable.lock);
      return fd;
    }
  }

  release(&ftable.lock);
  return -1;
}

struct inode *createfile(char *path){
  struct inode *ip;
  struct dirent d;
  struct inode *rootdir;
  struct dinode rootdird;
  
  if ((ip = namei(path)) > 0) {
    return ip;
  }

  //new inode
  ip = ialloc();

  //dirent for this new inode
  memset(&d, 0, sizeof(struct dirent));
  strncpy(d.name, path, strlen(path));
  d.inum = ip->inum;

  //update the inode root dir, write to dinode
  // get dinode
  //read_dinode(1, &rootdird);
  rootdir = namei("/");
  int offset = rootdir->size;
  writei(rootdir, (char *)&d, offset, sizeof(struct dirent));

  return ip;
  //update the dinode size.. do this in writei for any writes
  //read_dinode(ROOTINO ,&rootdird);
}

int fileread(int fd, char *path, int n){
  struct proc *pr = myproc();
  struct file *f = pr->ftable[fd];

  int nRead;

  if (f->permissions != O_RDONLY && f->permissions != O_RDWR)
    return -1;

  if (f->type == F_INODE) {
    int bufStart = f->offset;
    nRead = readi(f->ip, path, bufStart, n);

    if (nRead < 0) {
      return -1;
    }

    else {
      f->offset += nRead;
      return nRead;
    }
  }

  if (f->type == F_PIPE){
    if (f->permissions != O_RDONLY){
      return -1;
    }
    return piperead(f->pp, path, n);  
  }
  
  return -1;
}

int filewrite(int fd, char *path, int n){
  struct proc *pr = myproc();
  struct file *f = pr->ftable[fd];

  int nWritten;


  if (f->type == F_INODE){
    int bufStart = f->offset;
    nWritten = writei(f->ip, path, bufStart, n);
    
    if (nWritten < 0)
      return -1;

    else{
       f->offset += nWritten;
       return nWritten;
    }
  }

  if (f->type == F_PIPE){
    if (f->permissions != O_WRONLY){
      return -1;
    }
    return pipewrite(f->pp, path, n);
  }

  return -1;

}

//returns fd for duped file
int filedup(int fd){
  struct file *f;
  struct proc *pr = myproc();

  int dupfd;

  f = pr->ftable[fd];

  if (f == 0) return -1;

  acquire(&ftable.lock);
  for (dupfd = 0; dupfd < NOFILE; dupfd++){
    if(pr->ftable[dupfd] == 0){
      pr->ftable[dupfd] = pr->ftable[fd];
      pr->ftable[fd]->ref++;
      release(&ftable.lock);
      return dupfd;
    }
  }

  //process out of file table space
  release(&ftable.lock);
  return -1;
}

//dupes just the file
int filedupf(struct file *f){
  acquire(&ftable.lock);
  f->ref++;
  release(&ftable.lock);
}

// copy stat information from the inode at the given fd into
// filestats, returns 0 on success, -1 on failure
int filestat(int fd, struct stat *filestats) {
  struct file *f;
  struct proc *pr = myproc();

  f = pr->ftable[fd];
  
  if (f->type != F_INODE){
    cprintf("filestat error"); 
    return 0;
  }

  stati(f->ip, filestats);
  return 0;
}

// closes the given fd
void fileclose(int fd) {
  struct file *f;
  struct file copy;
  struct proc *pr = myproc();

  acquire(&ftable.lock);
  f = pr->ftable[fd];

  // decrement the refcount of the file
  f->ref = f->ref - 1;
  copy = *f;
  pr->ftable[fd] = 0;
  release(&ftable.lock);
  if (copy.ref > 0) {
    return;
  }
  if (copy.type == F_INODE) {
    irelease(copy.ip);
  } else if (copy.type == F_PIPE) {
    prelease(copy.pp, copy.permissions);
  }
}

int findnextfd() {
  for (int i = 0; i < NFILE; i++) {
    if (ftable.file[i].ref == 0)
      return i;
  }
  return NFILE;
}

//PIPES
int openpipe(int *pipes) {
  int glhead, gltail, prhead, prtail;

  struct pipe *p = initpipe();

  acquire(&ftable.lock);
  if ((glhead = findnextfd()) == NFILE) {
    kfree((char *)p);
    release(&ftable.lock);
    return -1;
  }
  if ((prhead = procopenpipe(glhead, 1, p)) < 0) {
    kfree((char *)p);
    release(&ftable.lock);
    return -1;
  }

  if ((gltail = findnextfd()) == NFILE) {
    kfree((char *)p);
    release(&ftable.lock);
    return -1;
  }
  if ((prtail = procopenpipe(gltail, 0, p)) < 0) {
    kfree((char *)p);
    release(&ftable.lock);
    return -1;
  }

  pipes[1] = prhead;
  pipes[0] = prtail;

  release(&ftable.lock);
  return 0;
}

struct pipe *initpipe() {
  struct pipe *p;
  p = (struct pipe*)kalloc();
  p->readOffset = 0;
  p->writeOffset = 0;
  p->readEnabled = 1;
  p->writeEnabled = 1;
  initlock(&p->lock, "pipe");
  return p;
}

int procopenpipe(int location, int write, struct pipe *p) {
  struct proc *pr = myproc();
  for (int fd = 0; fd < NOFILE; fd++) {
    if (pr->ftable[fd] == 0) {
      pr->ftable[fd] = &ftable.file[location];
      ftable.file[location].ref++;
      ftable.file[location].type = F_PIPE;
      ftable.file[location].ip = 0;
      ftable.file[location].pp = p;
      ftable.file[location].offset = 0;
      if (write) {
        ftable.file[location].permissions = O_WRONLY;
      } else {
        ftable.file[location].permissions = O_RDONLY;
      }
      return fd;
    }
  }
  return -1;
}

//pipe read
int piperead(struct pipe *pp, char* dst, int n){
  
  //sleep if offsets are equal
  acquire(&pp->lock);
  while (pp->readOffset == pp->writeOffset && pp->writeEnabled == 1){
    if (myproc()->killed) {
      release(&pp->lock);
      return -1;
    }
    sleep(&pp->readOffset, &pp->lock);
  }
  int nRead = 0;
  //read 
  for (int i = 0; (i < n && pp->readOffset != pp->writeOffset); i++){
    int pipesize = PIPESIZE;
    dst[i] = pp->buffer[pp->readOffset % pipesize]; //allow wrapping
    pp->readOffset++;
    nRead += 1;
  }
  
  wakeup(&pp->writeOffset);
  release(&pp->lock);

  return nRead; //number of bytes read
}

int pipewrite(struct pipe *pp, char *src, int n){

  acquire(&pp->lock);
  
  if (pp->writeEnabled == 0 || pp->readEnabled == 0){ 
    release(&pp->lock);
    return -1;
  }
 
  int nWritten = 0;

  for (int i = 0; i < n; i++){
    while (pp->writeOffset == pp->readOffset + PIPESIZE){
      if (pp->readEnabled == 0 || myproc()->killed) {
        release(&pp->lock);
        return -1;
      }
      wakeup(&pp->readOffset);
      sleep(&pp->writeOffset, &pp->lock);
    }
    int offset = pp->writeOffset;
    int pipesize = PIPESIZE;
    int adjustedOffset = offset % pipesize;
    char *buffer = pp->buffer;
    buffer[adjustedOffset] = src[i];
    //(pp->buffer)[(pp->writeOffset) % PIPESIZE] = src[i];
    pp->writeOffset++;
    nWritten += 1;
  }

  wakeup(&pp->readOffset);
  release(&pp->lock);
  
  return nWritten;
}

void prelease(struct pipe *pipe, int permissions) {
  // this is the last reference to the given pipe
  acquire(&pipe->lock);
  if (permissions == O_RDONLY) {
    pipe->readEnabled = 0;
    wakeup(&pipe->writeOffset);
  } else {
    pipe->writeEnabled = 0;
    wakeup(&pipe->readOffset);
  }
  if (pipe->readEnabled == 0 && pipe->writeEnabled == 0) {
    release(&pipe->lock);
    kfree((char *)pipe);
  } else {
    release(&pipe->lock);
  }
}

