#!/bin/bash
#runs `make qemu` several times and checks to see the number of tests passing.
# To use, in a screen or tmux session window, run:
#    source testmuch.sh
#
# You cannot simply run this script in a separate shell due to the way
# the output of QEMU is written.

# CONSTANTS
# Feel free to mess with these variables
TRIALS=200    # Number of times to runs the test
TIMEOUT=3s     # Number of seconds to let `make qemu` run for.
LOGDIR=logs    # Where to store the test output
DELPASS=true   # Whether to delete outputs of successful runs.


# DO NOT MODIFY THE LINES BELOW UNLESS YOU KNOW SHELL SCRIPTING

function runtests() {
  pass=0
  fail=0
  echo Running the tests ${TRIALS} times.
  echo "+ means the tests passed. - means the tests failed."
  echo

  for i in `seq 1 $TRIALS`; do 
    log="$LOGDIR/$i.tmp"
    timeout $TIMEOUT make qemu > $log 2> /dev/null

    if grep "lab3 tests passed!!" $log > /dev/null; then
      if $DELPASS; then 
        rm -f $log
      fi
      echo -n +
      (( pass += 1 ))
    else
      echo -
      echo [Iteration $i failed. See ${log}.]
      (( fail += 1 ))
    fi
  done
  echo SUMMARY: ${TRIALS} tests run. $pass passed. $fail failed.
}

[[ $0 != "$BASH_SOURCE" ]] && sourced=1 || sourced=0

if [[ sourced -eq 0 ]]; then
  echo "Script was not sourced. In a separate tmux/screen window, run:"
  echo "  source $BASH_SOURCE"
  echo ""
else
  # Clear the logs directory and run the tests
  mkdir -p $LOGDIR
  rm -f $LOGDIR/*.tmp

  echo "*** NOTE ***"
  echo "You will not be able to kill this script using Ctrl-C."
  echo "To kill it, you'll have to close the screen/tmux window."
  runtests
fi
