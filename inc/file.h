#pragma once

#include <extent.h>
#include <sleeplock.h>

#define PIPESIZE 4000

//pipe funcs only used in file.c
int openpipe(int *);
struct pipe *initpipe();
void prelease(struct pipe *, int permissions);
int piperead(struct pipe *, char *, int);
int pipewrite(struct pipe *, char *, int);
int procopenpipe(int, int, struct pipe*);

// in-memory copy of an inode
struct inode {
  uint dev;  // Device number
  uint inum; // Inode number
  int ref;   // Reference count
  struct sleeplock lock;

  short type; // copy of disk inode
  short devid;
  uint size;
  struct extent data;
  uint overflowBlocks[12];
};

// table mapping device ID (devid) to device functions
struct devsw {
  int (*read)(struct inode *, char *, int);
  int (*write)(struct inode *, char *, int);
};

extern struct devsw devsw[];

// Device ids
enum{
  CONSOLE = 1,
  F_INODE = 2,
  F_PIPE = 3
};

struct file {
   int      ref;        //reference count
   short    type;       //type of file F_INODE, F_PIPE, etc
   struct   inode *ip;  //inode reference
   struct   pipe *pp;  //pipe reference
   int      offset;     //read offset
   short    permissions; // 0 = none, 1=Read, 2=Write
};

struct pipe{
  int    ref;
  char   buffer[PIPESIZE];
  int    readOffset;
  int    writeOffset;
  short  readEnabled;
  short  writeEnabled;
  struct spinlock lock;
};


